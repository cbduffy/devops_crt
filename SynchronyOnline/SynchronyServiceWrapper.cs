﻿using Newtonsoft.Json.Linq;
using SynchronyOnline.SynChronyService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace SynchronyOnline
{
    public class SynchronyServiceWrapper : SynchronyServiceInterface
    {
        public string ServiceURL { get; set; }

        public SynchronyServiceWrapper()
        {  
            this.ServiceURL = Convert.ToString(ConfigurationManager.AppSettings["SynchronyServiceUrl"]);
        }
        public List<SynchronyOptions> GetSynchronyOptionsJson(string TransactionID, bool activeSynchronyOptions)
        {
            
            SynchronyServiceInterfaceClient servClient = new SynchronyServiceInterfaceClient();
            
            List<SynchronyOptions> synchronyOptionsList = new List<SynchronyOptions>();
            servClient.Open();
            synchronyOptionsList = servClient.GetSynchronyOptionsJson(TransactionID).OfType<SynchronyOptions>().ToList<SynchronyOptions>();
            if (activeSynchronyOptions)
            {
                IEnumerable<SynchronyOptions> activeSynchrony = synchronyOptionsList.Where(t => t.Status = true);
                if (activeSynchrony != null && activeSynchrony.Count() > 0)
                    synchronyOptionsList = activeSynchrony.ToList<SynchronyOptions>();
            }
            servClient.Close();
            return synchronyOptionsList;

        }

        public List<SynchronyOptions> GetResults(string transactionId,bool activeSynchronyOptions)
        {
            SynchronyServiceInterfaceClient servClient = new SynchronyServiceInterfaceClient();
            List<SynchronyOptions> synchronyOptionsList = new List<SynchronyOptions>();
            servClient.Open();
            synchronyOptionsList = servClient.GetResults(transactionId).OfType<SynchronyOptions>().ToList<SynchronyOptions>();
            if (activeSynchronyOptions)
            {
                IEnumerable<SynchronyOptions> activeSynchrony = synchronyOptionsList.Where(t => t.Status = true);
                if (activeSynchrony != null && activeSynchrony.Count() > 0)
                    synchronyOptionsList = activeSynchrony.ToList<SynchronyOptions>();
            }
            servClient.Close();
            return synchronyOptionsList;
        }

    }
}
