﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SynchronyOnline.SynChronyService;

namespace SynchronyOnline
{
    public interface SynchronyServiceInterface
    {
        List<SynchronyOptions> GetSynchronyOptionsJson(string TransactionID,bool activeSynchronyOption);

        List<SynchronyOptions> GetResults(string transactionId,bool activeSynchronyOptions);
    }
}
