﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

using AFM.Commerce.Framework.Extensions.Utils;

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Services
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Web;

    using AFM.Commerce.Entities;

    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;

    /// <summary>
    /// Service for customer.
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CustomerService : CustomerServiceBase
    {
        private static readonly ISearchEngine searchEngine = new SearchEngine();
        private static readonly IProductValidator productValidator = new ProductValidator();
        private static readonly ServiceUtilityBase serviceProxy = new ServiceUtility();

        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <returns>Customer response.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override CustomerResponse GetCustomer()
        {
            return base.GetCustomer();
        }

        /// <summary>
        /// Creates the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>Customer response.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override CustomerResponse CreateCustomer(Customer customer)
        {
            return base.CreateCustomer(customer);
        }

        /// <summary>
        /// Creates the customer.
        /// </summary>
        /// <param name="emailAddress">Email address of the customer.</param>
        /// <param name="activationToken">Activation token.</param>
        /// <returns>Customer response.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override CustomerResponse CreateCustomerByEmail(string emailAddress, string activationToken)
        {
            return base.CreateCustomerByEmail(emailAddress, activationToken);
        }

        /// <summary>
        /// Search for a customer given the corresponding email address.
        /// </summary>
        /// <param name="emailAddress">Email address of the customer.</param>
        /// <returns>True if the customer exists, false otherwise.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override bool SearchCustomers(string emailAddress)
        {
            return base.SearchCustomers(emailAddress);
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override NullResponse UpdateCustomer(Customer customer)
        {
            return base.UpdateCustomer(customer);
        }

        /// <summary>
        /// Gets the addresses.
        /// </summary>
        /// <returns>An address collection response.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override AddressCollectionResponse GetAddresses()
        {
            return base.GetAddresses();
        }

        /// <summary>
        /// Updates the addresses.
        /// </summary>
        /// <param name="addresses">The addresses.</param>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override NullResponse UpdateAddresses(IEnumerable<Address> addresses)
        {
            return base.UpdateAddresses(addresses);
        }

        /// <summary>
        /// Associates an existing customer to the current credentials if the criteria matches.
        /// </summary>
        /// <param name="email">The customer's e-mail address.</param>
        /// <param name="givenName">Given name.</param>
        /// <param name="surname">Surname.</param>
        /// <param name="userId">The user identifier.</param>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Thrown when the customer cannot be found.</exception>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override StringResponse AssociateCustomer(string email, string givenName, string surname, string userId)
        {
            return base.AssociateCustomer(email, givenName, surname, userId);
        }



        /// <summary>
        /// Gets Demo implementation of ISearchEngine.
        /// </summary>
        protected override ISearchEngine SearchEngine
        {
            get { return searchEngine; }
        }

        /// <summary>
        /// Gets Demo implementation of IProductValidator.
        /// </summary>
        protected override IProductValidator ProductValidator
        {
            get { return productValidator; }
        }

        /// <summary>
        /// Gets Demo implementation of ServiceUtilityBase.
        /// </summary>
        protected override ServiceUtilityBase ServiceUtility
        {
            get { return serviceProxy; }
        }


        /// <summary>
        /// Gets Demo implementation of IConfiguration.
        /// </summary>
        protected override IConfiguration Configuration
        {
            get { return new SiteConfiguration(); }
        }


        /// <summary>
        /// Executes the given service action and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="action">Service action.</param>
        protected override void ExecuteAfterRequestValidation(ServiceResponse response, System.Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            AFMServiceResponse afmServiceResponse = null;
            if (response != null)
            {
                afmServiceResponse = new AFMServiceResponse();
                afmServiceResponse.CanRetry = response.CanRetry;
                foreach (var error in response.Errors)
                {
                    AFM.Commerce.Entities.ResponseError responseError = new AFM.Commerce.Entities.ResponseError();
                    responseError.ErrorCode = error.ErrorCode;
                    responseError.ErrorMessage = error.ErrorMessage;
                    responseError.ExtendedErrorMessage = error.ExtendedErrorMessage;
                    responseError.RedirectUrl = error.RedirectUrl;
                    responseError.ShowDebugInfo = error.ShowDebugInfo;
                    responseError.Source = error.Source;
                    responseError.StackTrace = error.StackTrace;
                    afmServiceResponse.Errors.Add(responseError);

                }
                afmServiceResponse.RedirectUrl = response.RedirectUrl;
            }
            AFMDataUtilities.ExecuteAndHandleExceptions(afmServiceResponse, action);
        }

        /// <summary>
        /// Executes handler and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void Execute(ServiceResponse response, Action action)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if user is authenticated and assosiated then calls <see cref="ExecuteAfterRequestValidation"/> with new execution handler.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="action">Service action.</param>
        protected override void ExecuteForAuthorizedUser(ServiceResponse response, Action<string> action)
        {
            string customerId = AFMDataUtilities.GetCustomerIdFromCookie();

            ExecuteAfterRequestValidation(response, () =>
            {
                action(customerId);
            });
        }
    }
}
