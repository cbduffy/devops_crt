﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Services
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Web;

    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Extensions.Utils;

    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;
    using Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront;
    using System.Diagnostics.CodeAnalysis;
    using AFM.Commerce.Logging;

    /// <summary>
    /// Service for shopping cart.
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ShoppingCartService : ShoppingCartServiceBase
    {
        private static readonly IProductValidator productValidator = new ProductValidator();
        private static readonly ISearchEngine searchEngine = new SearchEngine();
        private static readonly ServiceUtilityBase serviceProxy = new ServiceUtility();

        /// <summary>
        /// Adds items to the shopping cart associated with the given identifier.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="listings">The items to add to the cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the updated shopping cart.
        /// </returns>
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse AddItems(bool isCheckoutSession, IEnumerable<Listing> listings, ShoppingCartDataLevel dataLevel)
        {
            if (listings == null)
            {
                throw new ArgumentNullException("listings");
            }
            try
            {
                return base.AddItems(isCheckoutSession, listings, dataLevel);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                throw;
            }
        }

        /// <summary>
        /// Gets the shopping cart associated .
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether the request originated from the checkout page.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the shopping cart.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse GetShoppingCart(bool isCheckoutSession, ShoppingCartDataLevel dataLevel)
        {
            try
            {
                return base.GetShoppingCart(isCheckoutSession, dataLevel);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                throw;
            }
        }

        /// <summary>
        /// Removes items from the shopping cart associated with the given identifier.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="lineIds">The item line identifiers to be removed from the shopping cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the updated shopping cart.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse RemoveItems(bool isCheckoutSession, IEnumerable<string> lineIds, ShoppingCartDataLevel dataLevel)
        {
            if (lineIds == null)
            {
                throw new ArgumentNullException("lineIds");
            }
            try
            {
                return base.RemoveItems(isCheckoutSession, lineIds, dataLevel);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                throw;
            }
        }

        /// <summary>
        /// Updates items on the shopping cart associated with the given identifier.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="items">The items to be updated.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the updated shopping cart.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse UpdateItems(bool isCheckoutSession, IEnumerable<TransactionItem> items, ShoppingCartDataLevel dataLevel)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }
            try
            {
                return base.UpdateItems(isCheckoutSession, items, dataLevel);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                throw;
            }
        }

        /// <summary>
        /// Gets all the promotions for the shopping cart items.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the updated shopping cart.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse GetPromotions(bool isCheckoutSession, ShoppingCartDataLevel dataLevel)
        {
            try
            {
                return base.GetPromotions(isCheckoutSession, dataLevel);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                throw;
            }
        }

        /// <summary>
        /// Adds or Removes the discount codes from the cart.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="promotionCode">The promotion code to apply to the cart.</param>
        /// <param name="isAdd">Indicates whether the operation is addition or removal of discount codes.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse AddOrRemovePromotionCode(bool isCheckoutSession, string promotionCode, bool isAdd, ShoppingCartDataLevel dataLevel)
        {
            if (string.IsNullOrWhiteSpace(promotionCode))
            {
                throw new ArgumentNullException("promotionCode");
            }
            try
            {
                return base.AddOrRemovePromotionCode(isCheckoutSession, promotionCode, isAdd, dataLevel);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                throw;
            }
        }

        /// <summary>
        /// Gets information about the specific kit configuration that has the provided kit line values.
        /// </summary>
        /// <param name="kitProductMasterIdentifier">The product identifier of the kit product master.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="kitLines">Details of each component line that comprises the kit.</param>
        /// <returns>Specific kit configuration information.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override KitConfigurationResponse GetKitConfigurationInformation(long kitProductMasterIdentifier, long catalogId, IEnumerable<KitLine> kitLines)
        {
            try
            {
                return base.GetKitConfigurationInformation(kitProductMasterIdentifier, catalogId, kitLines);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                throw;
            }
        }

        /// <summary>
        /// Gets the applicable variants of a kit component.
        /// </summary>
        /// <param name="kitComponentProductId">Product identifier of the kit component.</param>
        /// <param name="kitComponentParentId">Product identifier of the kit component master.</param>
        /// <param name="parentKitId">Product identifier of the kit master product.</param>
        /// <param name="kitComponentLineId">The kit component line identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <returns>Returns a collection of kit component variants.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override KitComponentVariantResponse GetKitComponentVariants(long kitComponentProductId, long kitComponentParentId, long parentKitId, long kitComponentLineId, long catalogId)
        {
            try
            {
                return base.GetKitComponentVariants(kitComponentProductId, kitComponentParentId, parentKitId, kitComponentLineId, catalogId);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                throw;
            }
        }

        /// <summary>
        /// Starts the checkout process by creating a secure cart and returning it.
        /// </summary>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>A new cart with a random cart id that should be used during the secure checkout process.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse CommenceCheckout(ShoppingCartDataLevel dataLevel)
        {
            try
            {
                return base.CommenceCheckout(dataLevel);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                throw;
            }
        }

        /// <summary>
        /// Gets Demo implementation of ISearchEngine.
        /// </summary>
        protected override ISearchEngine SearchEngine
        {
            get { return searchEngine; }
        }

        /// <summary>
        /// Gets Demo implementation of IProductValidator.
        /// </summary>
        protected override IProductValidator ProductValidator
        {
            get { return productValidator; }
        }

        /// <summary>
        /// Gets Demo implementation of ServiceUtilityBase.
        /// </summary>
        protected override ServiceUtilityBase ServiceUtility
        {
            get { return serviceProxy; }
        }

        /// <summary>
        /// Gets Demo implementation of IConfiguration.
        /// </summary>
        protected override IConfiguration Configuration
        {
            get { return new SiteConfiguration(); }
        }

        /// <summary>
        /// Executes the given service action and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="action">Service action.</param>
        protected override void ExecuteAfterRequestValidation(ServiceResponse response, System.Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            AFMServiceResponse afmServiceResponse = null;
            if (response != null)
            {
                afmServiceResponse = new AFMServiceResponse();
                afmServiceResponse.CanRetry = response.CanRetry;
                foreach (var error in response.Errors)
                {
                    AFM.Commerce.Entities.ResponseError responseError = new AFM.Commerce.Entities.ResponseError();
                    responseError.ErrorCode = error.ErrorCode;
                    responseError.ErrorMessage = error.ErrorMessage;
                    responseError.ExtendedErrorMessage = error.ExtendedErrorMessage;
                    responseError.RedirectUrl = error.RedirectUrl;
                    responseError.ShowDebugInfo = error.ShowDebugInfo;
                    responseError.Source = error.Source;
                    responseError.StackTrace = error.StackTrace;
                    afmServiceResponse.Errors.Add(responseError);

                }
                afmServiceResponse.RedirectUrl = response.RedirectUrl;
            }
            AFMDataUtilities.ExecuteAndHandleExceptions(afmServiceResponse, action);
        }

        /// <summary>
        /// Executes handler and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void Execute(ServiceResponse response, Action action)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if user is authenticated and assosiated then calls <see cref="ExecuteAfterPostRequestValidation"/> with new execution handler.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void ExecuteForAuthorizedUser(ServiceResponse response, Action<string> action)
        {
            throw new NotImplementedException();
        }
    }
}
