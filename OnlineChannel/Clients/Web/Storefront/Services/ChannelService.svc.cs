﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Services
{
    using System;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Web;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;
    using AFM.Commerce.Framework.Extensions.Utils;
    using AFM.Commerce.Entities;

    /// <summary>
    /// Service for Channel.
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ChannelService : ChannelServiceBase
    {
        private static readonly ISearchEngine searchEngine = new SearchEngine();
        private static readonly IProductValidator productValidator = new ProductValidator();
        private static readonly ServiceUtilityBase serviceProxy = new ServiceUtility();

        /// <summary>
        /// Gets the channel configuration.
        /// </summary>
        /// <returns>
        /// A channel info response that contains the channel configuration.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ChannelInfoResponse GetChannelConfiguration()
        {
            return base.GetChannelConfiguration();
        }

        /// <summary>
        /// Gets channel tender types.
        /// </summary>
        /// <returns>Tender types response.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override TenderTypesResponse GetChannelTenderTypes()
        {
            return base.GetChannelTenderTypes();
        }

        /// <summary>
        /// Gets the channel country region info.
        /// </summary>
        /// <returns>Country info response.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override CountryInfoResponse GetChannelCountryRegionInfo()
        {
            return base.GetChannelCountryRegionInfo();
        }

        /// <summary>
        /// Gets the states/provinces data for the given country.
        /// </summary>
        /// <param name="countryCode">Country code.</param>
        /// <returns>The states/provinces for the given country.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override StateProvinceInfoResponse GetStateProvinceInfo(string countryCode)
        {
            return base.GetStateProvinceInfo(countryCode);
        }

        /// <summary>
        /// Gets Demo implementation of ISearchEngine.
        /// </summary>
        protected override ISearchEngine SearchEngine
        {
            get { return searchEngine; }
        }

        /// <summary>
        /// Gets Demo implementation of IProductValidator.
        /// </summary>
        protected override IProductValidator ProductValidator
        {
            get { return productValidator; }
        }

        /// <summary>
        /// Gets Demo implementation of ServiceUtilityBase.
        /// </summary>
        protected override ServiceUtilityBase ServiceUtility
        {
            get { return serviceProxy; }
        }

        /// <summary>
        /// Gets Demo implementation of IConfiguration.
        /// </summary>
        protected override IConfiguration Configuration
        {
            get { return new SiteConfiguration(); }
        }

        /// <summary>
        /// Executes the given service action and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="action">Service action.</param>
        protected override void ExecuteAfterRequestValidation(ServiceResponse response, System.Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            AFMServiceResponse afmServiceResponse = null;
            if (response != null)
            {
                afmServiceResponse = new AFMServiceResponse();
                afmServiceResponse.CanRetry = response.CanRetry;
                foreach (var error in response.Errors)
                {
                    AFM.Commerce.Entities.ResponseError responseError = new AFM.Commerce.Entities.ResponseError();
                    responseError.ErrorCode = error.ErrorCode;
                    responseError.ErrorMessage = error.ErrorMessage;
                    responseError.ExtendedErrorMessage = error.ExtendedErrorMessage;
                    responseError.RedirectUrl = error.RedirectUrl;
                    responseError.ShowDebugInfo = error.ShowDebugInfo;
                    responseError.Source = error.Source;
                    responseError.StackTrace = error.StackTrace;
                    afmServiceResponse.Errors.Add(responseError);

                }
                afmServiceResponse.RedirectUrl = response.RedirectUrl;
            }
            AFMDataUtilities.ExecuteAndHandleExceptions(afmServiceResponse, action);
        }

        /// <summary>
        /// Executes handler and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void Execute(ServiceResponse response, Action action)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if user is authenticated and assosiated then calls <see cref="ExecuteAfterPostRequestValidation"/> with new execution handler.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void ExecuteForAuthorizedUser(ServiceResponse response, Action<string> action)
        {
            throw new NotImplementedException();
        }
    }
}
