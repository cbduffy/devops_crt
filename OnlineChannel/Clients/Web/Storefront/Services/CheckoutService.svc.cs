﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

using AFM.Commerce.Framework.Extensions.Utils;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Services
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Web;

    using AFM.Commerce.Entities;

    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;
    using AFM.Commerce.Logging;

    /// <summary>
    /// Service for checkout.
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CheckoutService : CheckoutServiceBase
    {
        private static readonly ISearchEngine searchEngine = new SearchEngine();
        private static readonly IProductValidator productValidator = new ProductValidator();
        private static readonly ServiceUtilityBase serviceProxy = new ServiceUtility();

        /// <summary>
        /// Gets details of all the delivery options available for the current legal entity.
        /// </summary>
        /// <returns>
        /// The delivery options response object.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override DeliveryOptionsResponse GetDeliveryOptionsInfo()
        {
            try
            {
                return base.GetDeliveryOptionsInfo();
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }  
        }

        /// <summary>
        /// Gets delivery preferences applicable to the current checkout cart.
        /// </summary>
        /// <returns>
        /// The delivery preference response object.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override DeliveryPreferenceResponse GetDeliveryPreferences()
        {
            try
            {
                return base.GetDeliveryPreferences();
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }  
        }

        /// <summary>
        /// Gets the applicable delivery options when the user wants to 'ship' the entire order as a single entity.
        /// </summary>
        /// <param name="shipToAddress">The 'ship to' address.</param>
        /// <returns>
        /// The service response containing the available delivery options.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override DeliveryOptionsResponse GetOrderDeliveryOptionsForShipping(Address shipToAddress)
        {
            try
            {
                return base.GetOrderDeliveryOptionsForShipping(shipToAddress);
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }  
        }

        /// <summary>
        /// Gets the delivery options applicable per line when the user wants the items in the cart 'shipped' to them individually.
        /// </summary>
        /// <param name="selectedLineShippingInfo">The shipping information for the selected lines.</param>
        /// <returns>
        /// The service response containing the available delivery options for the specified lines.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override DeliveryOptionsResponse GetLineDeliveryOptionsForShipping(IEnumerable<SelectedLineShippingInfo> selectedLineShippingInfo)
        {
            try
            {
                if (selectedLineShippingInfo == null)
                {
                    throw new ArgumentNullException("selectedLineShippingInfo");
                }
                return base.GetLineDeliveryOptionsForShipping(selectedLineShippingInfo);
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }  
        }

        /// <summary>
        /// Commits the selected delivery option to the cart when entire order is being 'delivered' as a single entity.
        /// </summary>
        /// <param name="selectedDeliveryOption">The selected delivery option.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// The updated shopping cart.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse SetOrderDeliveryOption(SelectedDeliveryOption selectedDeliveryOption, ShoppingCartDataLevel dataLevel)
        {
            try
            {
                if (selectedDeliveryOption == null)
                {
                    throw new ArgumentNullException("selectedDeliveryOption");
                }
                return base.SetOrderDeliveryOption(selectedDeliveryOption, dataLevel);
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }  
        }

        /// <summary>
        /// Commits the selected delivery options per line when the sales line in the order are being 'delivered' individually.
        /// </summary>
        /// <param name="selectedLineDeliveryOptions">The line delivery options.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// The updated shopping cart.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse SetLineDeliveryOptions(IEnumerable<SelectedLineDeliveryOption> selectedLineDeliveryOptions, ShoppingCartDataLevel dataLevel)
        {
            if (selectedLineDeliveryOptions == null)
            {
                throw new ArgumentNullException("selectedLineDeliveryOptions");
            }
            try
            {
                return base.SetLineDeliveryOptions(selectedLineDeliveryOptions, dataLevel);
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }  
        }

        /// <summary>
        /// Processes a sales order.
        /// </summary>
        /// <param name="tenderDataLine">Tender lines data containing payments for the order.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <returns>
        /// The updated shopping cart item collection.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override CreateSalesOrderResponse CreateOrder(IEnumerable<TenderDataLine> tenderDataLine, string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }

            if (tenderDataLine == null)
            {
                throw new ArgumentNullException("tenderDataLine");
            }
            try
            {              
                return base.CreateOrder(tenderDataLine, emailAddress);
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }  
        }

        //TO DO spriya commented for upgrade
        /*
        /// <summary>
        /// Sets the shipping options per item.
        /// </summary>
        /// <param name="shippingOptions">The collection of shipping options per line id.</param>
        /// <returns>
        /// NullResponse object to indicate successful call.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse SetShippingOptionPerItem(IEnumerable<SelectedItemShippingOptions> shippingOptions)
        {
            try
            {
                return base.SetShippingOptionPerItem(shippingOptions);
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            } 
        }
         */

        /// <summary>
        /// Gets the payment card types.
        /// </summary>
        /// <returns>
        /// The valid card types accepted for payment.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override PaymentCardTypesResponse GetPaymentCardTypes()
        {
            try
            {
                return base.GetPaymentCardTypes();
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            } 
        }


        //TO DO spriya commented as part of upgrade
      /*
       /// <summary>
        /// Gets the payment card.
        /// </summary>
        /// <returns>
        /// The payment card.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override PaymentCardResponse GetPaymentCard()
        {
            try
            {
                return base.GetPaymentCard();
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            } 
        } */

        /// <summary>
        /// Gets the gift card balance.
        /// </summary>
        /// <returns>
        /// The gift card balance response.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override GiftCardResponse GetGiftCardInformation(string giftCardId)
        {
            try
            {
                return base.GetGiftCardInformation(giftCardId);
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            } 
        }

        //TO DO spriya merged for upgrade
        /// <summary>
        /// Gets boolean value indicating whether session is authenticated or not.
        /// </summary>
        /// <returns>True if session is authenticated.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override BooleanResponse IsAuthenticatedSession()
        {
            return base.IsAuthenticatedSession();
        }

        /// <summary>
        /// Gets customer email address.
        /// </summary>
        /// <returns>Email response.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override StringResponse GetUserEmail()
        {
            return base.GetUserEmail();
        }

        //NE spriya
        
        /// <summary>
        /// Gets Demo implementation of ISearchEngine.
        /// </summary>
        protected override ISearchEngine SearchEngine
        {
            get { return searchEngine; }
        }

        /// <summary>
        /// Gets Demo implementation of IProductValidator.
        /// </summary>
        protected override IProductValidator ProductValidator
        {
            get { return productValidator; }
        }

        /// <summary>
        /// Gets Demo implementation of ServiceUtilityBase.
        /// </summary>
        protected override ServiceUtilityBase ServiceUtility
        {
            get { return serviceProxy; }
        }
        /// <summary>
        /// Gets Demo implementation of IConfiguration.
        /// </summary>
        protected override IConfiguration Configuration
        {
            get { return new SiteConfiguration(); }
        }

        /// <summary>
        /// Executes the given service action and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="action">Service action.</param>
        protected override void ExecuteAfterRequestValidation(ServiceResponse response, System.Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            AFMServiceResponse afmServiceResponse = null;
            if (response != null)
            {
                afmServiceResponse = new AFMServiceResponse();
                afmServiceResponse.CanRetry = response.CanRetry;
                foreach (var error in response.Errors)
                {
                    AFM.Commerce.Entities.ResponseError responseError = new AFM.Commerce.Entities.ResponseError();
                    responseError.ErrorCode = error.ErrorCode;
                    responseError.ErrorMessage = error.ErrorMessage;
                    responseError.ExtendedErrorMessage = error.ExtendedErrorMessage;
                    responseError.RedirectUrl = error.RedirectUrl;
                    responseError.ShowDebugInfo = error.ShowDebugInfo;
                    responseError.Source = error.Source;
                    responseError.StackTrace = error.StackTrace;
                    afmServiceResponse.Errors.Add(responseError);

                }
                afmServiceResponse.RedirectUrl = response.RedirectUrl;
            }
            AFMDataUtilities.ExecuteAndHandleExceptions(afmServiceResponse, action);
        }

        /// <summary>
        /// Executes handler and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void Execute(ServiceResponse response, Action action)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if user is authenticated and assosiated then calls <see cref="ExecuteAfterPostRequestValidation"/> with new execution handler.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void ExecuteForAuthorizedUser(ServiceResponse response, Action<string> action)
        {
            throw new NotImplementedException();
        }
    }
}
