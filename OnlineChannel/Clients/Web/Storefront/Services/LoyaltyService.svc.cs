﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Services
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Web;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;
    using AFM.Commerce.Framework.Extensions.Utils;
    using AFM.Commerce.Entities;

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class LoyaltyService : LoyaltyServiceBase
    {
        private static readonly ISearchEngine searchEngine = new SearchEngine();
        private static readonly IProductValidator productValidator = new ProductValidator();
        private static readonly ServiceUtilityBase serviceProxy = new ServiceUtility();

        /// <summary>
        /// Issues a new loyalty card id for the customer.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse GenerateLoyaltyCardId(bool isCheckoutSession, ShoppingCartDataLevel dataLevel)
        {
            return base.GenerateLoyaltyCardId(isCheckoutSession, dataLevel);
        }

        /// <summary>
        /// Gets a read only collection of all loyalty card numbers
        /// </summary>
        /// 
        /// <returns>A loyaltyCardsResponse object</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override LoyaltyCardsResponse GetLoyaltyCards()
        {
            return base.GetLoyaltyCards();
        }

        /// <summary>
        /// Gets the status of a loyalty card given it's id
        /// </summary>
        /// 
        /// <param name="loyaltyCardNumber">The loyalty card Id</param>
        /// <returns>A loyaltyCardResponse object</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override LoyaltyCardResponse GetLoyaltyCardStatus(string loyaltyCardNumber)
        {
            return base.GetLoyaltyCardStatus(loyaltyCardNumber);
        }

        /// <summary>
        /// Gets a read only collection of all loyalty card with their status
        /// </summary>
        /// 
        /// <returns>A loyaltyCardsResponse object</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override LoyaltyCardsResponse GetAllLoyaltyCardsStatus()
        {
            return base.GetAllLoyaltyCardsStatus();
        }

        /// <summary>
        /// Gets a stream with all the transaction data specific to a a loyalty card number for a given points category
        /// </summary>
        /// 
        /// <param name="loyaltyCardNumber">The loyalty card Id</param>
        /// <param name="rewardPointId">The reward points Id</param>
        /// <param name="topRows">Number of entries that should appear per page</param>
        /// <returns>A LoyaltyCardTransactionsResponse object</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override LoyaltyCardTransactionsResponse GetLoyaltyCardTransactions(string loyaltyCardNumber, string rewardPointId, int topRows)
        {
            return base.GetLoyaltyCardTransactions(loyaltyCardNumber, rewardPointId, topRows);
        }

        /// <summary>
        /// Updates the loyalty card id on the shopping cart.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="loyaltyCardId">The loyalty card id (or empty string) to set on the cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse UpdateLoyaltyCardId(bool isCheckoutSession, string loyaltyCardId, ShoppingCartDataLevel dataLevel)
        {
            return base.UpdateLoyaltyCardId(isCheckoutSession, loyaltyCardId, dataLevel);
        }
        
        /// <summary>
        /// Gets Demo implementation of ISearchEngine.
        /// </summary>
        protected override ISearchEngine SearchEngine
        {
            get { return searchEngine; }
        }

        /// <summary>
        /// Gets Demo implementation of IProductValidator.
        /// </summary>
        protected override IProductValidator ProductValidator
        {
            get { return productValidator; }
        }

        /// <summary>
        /// Gets Demo implementation of ServiceUtilityBase.
        /// </summary>
        protected override ServiceUtilityBase ServiceUtility
        {
            get { return serviceProxy; }
        }

        /// <summary>
        /// Gets Demo implementation of IConfiguration.
        /// </summary>
        protected override IConfiguration Configuration
        {
            get { return new SiteConfiguration(); }
        }

        /// <summary>
        /// Executes the given service action and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="action">Service action.</param>
        protected override void ExecuteAfterRequestValidation(ServiceResponse response, System.Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            AFMServiceResponse afmServiceResponse = null;
            if (response != null)
            {
                afmServiceResponse = new AFMServiceResponse();
                afmServiceResponse.CanRetry = response.CanRetry;
                foreach (var error in response.Errors)
                {
                    AFM.Commerce.Entities.ResponseError responseError = new AFM.Commerce.Entities.ResponseError();
                    responseError.ErrorCode = error.ErrorCode;
                    responseError.ErrorMessage = error.ErrorMessage;
                    responseError.ExtendedErrorMessage = error.ExtendedErrorMessage;
                    responseError.RedirectUrl = error.RedirectUrl;
                    responseError.ShowDebugInfo = error.ShowDebugInfo;
                    responseError.Source = error.Source;
                    responseError.StackTrace = error.StackTrace;
                    afmServiceResponse.Errors.Add(responseError);

                }
                afmServiceResponse.RedirectUrl = response.RedirectUrl;
            }
            AFMDataUtilities.ExecuteAndHandleExceptions(afmServiceResponse, action);
        }

        /// <summary>
        /// Executes handler and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void Execute(ServiceResponse response, Action action)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if user is authenticated and assosiated then calls <see cref="ExecuteAfterPostRequestValidation"/> with new execution handler.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void ExecuteForAuthorizedUser(ServiceResponse response, Action<string> action)
        {
            throw new NotImplementedException();
        }
    }
}