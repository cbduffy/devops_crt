﻿using AFM.Commerce.Framework.Extensions.Controllers;
using AFM.Commerce.Framework.Extensions.Mappers;
using AFM.Commerce.Framework.Extensions.Utils;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Client;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace SynchronyService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class SynchronyServiceClass : SynchronyServiceBase
    {
        public override List<SynchronyOptions> GetSynchronyOptionsJson(SynchronyRequest req)
        {
            return base.GetSynchronyOptionsJson(req);   
        }

        public override List<SynchronyOptions> GetSynchronyOptionsTestJson()
        {
            return base.GetSynchronyOptionsTestJson();
        }

        public override List<SynchronyOptions> GetResults(string TransactionID)
        {
            return base.GetResults(TransactionID);
        }
    }
}
