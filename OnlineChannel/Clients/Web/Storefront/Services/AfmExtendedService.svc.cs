﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfmExtendedService.svc.cs" company="Microsoft">
//   
// </copyright>
// <summary>
//   The afm extended service.
// NS by muthait
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.ServiceModel;
    using System.ServiceModel.Activation;
    using System.ServiceModel.Web;
    using System.Text;

    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Framework.Extensions.Controllers;
    using AFM.Commerce.Logging;
    using AFM.Commerce.Services;
    using AFM.Ecommerce.Controls;

    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;
    using Microsoft.Dynamics.Retail.SDKManager;
    using AFM.Commerce.Framework.Extensions.Utils;

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AFMCustomService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AFMCustomService.svc or AFMCustomService.svc.cs at the Solution Explorer and start debugging.

    /// <summary>
    /// The afm extended service.
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class AfmExtendedService : AFMExtendedServiceBase
    {

        private static readonly ISearchEngine searchEngine = new SearchEngine();
        private static readonly IProductValidator productValidator = new ProductValidator();
        private static readonly ServiceUtilityBase ServiceProxy = new ServiceUtility();

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse SetZipCode(string zipCode, bool isCheckoutSession)
        {
            if (string.IsNullOrWhiteSpace(zipCode))
            {
                throw new ArgumentNullException("zipCode");
            }

            try
            {
                //DemoUtilities.SetAFMZipCodeInCookie(zipCode);
                return base.SetZipCode(zipCode, isCheckoutSession);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }
        }


        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override string SendOrderSummary(AFMOrderSummary orderSummary)
        {
            try
            {
                //DemoUtilities.SetAFMZipCodeInCookie(zipCode);
                return base.SendOrderSummary(orderSummary);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                return string.Empty;
            }
        }


        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override AFMZipCodeResponse GetZipCode()
        {
            try
            {
                //var zipCode = DemoUtilities.GetAFMZipCodeFromCookie();
                //return new AFMZipCodeResponse
                //{
                //    AFMZipCode = zipCode
                //};
                return base.GetZipCode();
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }


        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override string GetCultureId()
        {
            try
            {
                return base.GetCultureId();
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }


        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override AFMVendorAffiliation GetAffiliationByZipCode(string zipCode)
        {
            if (string.IsNullOrWhiteSpace(zipCode))
            {
                throw new ArgumentNullException("zipCode");
            }

            try
            {
                return base.GetAffiliationByZipCode(zipCode);
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ReadOnlyCollection<AfmAffiliationPrice> GetPriceByAffiliation(long affiliationId, List<Product> products)
        {
            if (affiliationId == 0)
            {
                throw new ArgumentException("affiliationId");
            }
            try
            {
                return base.GetPriceByAffiliation(affiliationId, products);
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }
        }


        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override List<AFMPrdtNAP> GetPrdtswithNAPInfo(List<long> productIds)
        {
            try
            {
                return base.GetPrdtswithNAPInfo(productIds);
            }
            catch (Exception ex)
            {

                LoggerUtilities.ProcessLogMessage(ex);
                return null;
            }
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override CreateSalesOrderResponse CreateOrder(IEnumerable<TenderDataLine> tenderDataLine, AFMSalesTransHeader headerValues, string emailAddress, string cardToken)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }
            if (tenderDataLine == null)
            {
                throw new ArgumentNullException("tenderDataLine");
            }
            // CR 117 - null check throws an exception if checkbox not clicked
            //if (headerValues == null)
            //{
            //    throw new ArgumentNullException("headerValues");
            //}


            return base.CreateOrder(tenderDataLine, headerValues, emailAddress, cardToken);
        }


        /// <summary>
        /// Starts the checkout process by creating a secure cart and returning it.
        /// </summary>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>A new cart with a random cart id that should be used during the secure checkout process.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse CommenceCheckout(ShoppingCartDataLevel dataLevel)
        {
            try
            {
                return base.CommenceCheckout(dataLevel);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                throw;
            }
        }


        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse SetShippingtoLineItems(SelectedLineDeliveryOption shippingOptions, ShoppingCartDataLevel dataLevel)
        {
            if (shippingOptions == null)
            {
                throw new ArgumentNullException("shippingOptions");
            }
            return base.SetShippingtoLineItems(shippingOptions, dataLevel);
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override AFMCardTokenData GetCardTokenData(string tranId)
        {
            return base.GetCardTokenData(tranId);
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override string SetUserCartInfo(AFMUserCartData userCartData)
        {
            if (userCartData == null)
            {
                throw new ArgumentNullException("userCartData");
            }
            return base.SetUserCartInfo(userCartData);
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override AFMUserPageMode GetUserPageMode()
        {
            return base.GetUserPageMode();
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override string SetUserPageMode(AFMUserPageMode userPageMode)
        {
            return base.SetUserPageMode(userPageMode);
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override AFMUserCartData GetUserCartInfo()
        {
            return base.GetUserCartInfo();
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override string ClearAllCookies()
        {
            return base.ClearAllCookies();
        }

        //Changed Return type to AFMAddressData
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override AFMAddressData ValidateAddress(Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Address address)
        {
            return base.ValidateAddress(address);
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override List<AFMAddressState> GetAFMAddressState(string countryRegionId)
        {
            return base.GetAFMAddressState(countryRegionId);
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse GetActiveShoppingCart(string customerId, ShoppingCartDataLevel dataLevel)
        {
            return base.GetActiveShoppingCart(customerId, dataLevel);
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override ShoppingCartResponse MoveItemsBetweenCarts(string sourceShoppingCartId, string destinationShoppingCartId, string customerId, ShoppingCartDataLevel dataLevel)
        {
            return base.MoveItemsBetweenCarts(sourceShoppingCartId, destinationShoppingCartId, customerId, dataLevel);
        }


        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override bool ValidateZipcode(string zipCode, string countryRegionId)
        {
            return base.ValidateZipcode(zipCode, countryRegionId);
        }


        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override IEnumerable<Listing> CheckAtpItemAvailability(string itemId, string zipCode, int Qty)
        {
            var productList = AFMProductsController.GetProductsByItemIds(new List<string> { itemId });
            var listing = new List<Listing>();
            dynamic dimensionData = new System.Dynamic.ExpandoObject();
            dynamic productListing = new System.Dynamic.ExpandoObject();
            foreach (var product in productList)
            {
                dimensionData.Color = "Black";
                dimensionData.Size = "12 inch";

                productListing.Name = product.ProductName;
                productListing.Description = product.Description;
                productListing.DimensionValues = dimensionData;
                productListing.ImageUrl = "/images/Sofa.png";
                productListing.ImageAlt = "";

                var productDetails = Newtonsoft.Json.JsonConvert.SerializeObject(productListing);

                var listingtemp = new Listing { ListingId = product.RecordId, ItemId = product.ProductNumber, Quantity = Qty, ProductDetails = productDetails };
                listing.Add(listingtemp);
            }
            string customerId = AFMDataUtilities.GetCustomerIdFromCookie();
            if (!Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["ByPassATP"])) {
                var ATPoutput = this.Controller.CheckAtpItemAvailability(customerId, listing.AsEnumerable(), zipCode);
                if (ATPoutput != null)
                {
                    if (!ATPoutput.ItemGroupings[0].Items[0].IsAvailable)
                    {
                        //listing[0].AFMCartLineATP = ATPoutput.ItemGroupings[0].Items[0];
                        listing[0].Comment = ATPoutput.ItemGroupings[0].Items[0].Message + " Available Qty : " + ATPoutput.ItemGroupings[0].Items[0].Quantity;
                        //listing.Clear();
                    }
                    else
                    {
                        listing[0].AFMCartLineATP = ATPoutput.ItemGroupings[0].Items[0];
                        listing[0].Comment = "";
                    }
                }
                else
                {
                    listing.Clear();
                }
            }
            else
            {
                listing[0].Comment = "";
            }
            
            return listing.AsEnumerable();
            //return base.CheckAtpItemAvailability(listing, ZipCode);
        }

        //CS Developed by spriya Dated 12/12/2014
        //Pick only shipping address or billing address for signed in customer based on IsShipping value
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override AddressCollectionResponse GetShippingAddresses(bool IsShipping)
        {
            return base.GetShippingAddresses(IsShipping);
        }
        //CE Developed by spriya

        /// <summary>
        /// Redirect to Payment Gateway
        /// </summary>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override string RedirectToPayment()
        {
            return base.RedirectToPayment();
        }

        /// <summary>
        /// Gets Demo implementation of ServiceUtilityBase.
        /// </summary>
        protected override ServiceUtilityBase ServiceUtility
        {
            get { return ServiceProxy; }
        }

        protected override IConfiguration Configuration
        {
            get
            {
                return new SiteConfiguration();
            }
        }

        /// <summary>
        /// Gets Demo implementation of ISearchEngine.
        /// </summary>
        protected override ISearchEngine SearchEngine
        {
            get { return searchEngine; }
        }

        /// <summary>
        /// Gets Demo implementation of IProductValidator.
        /// </summary>
        protected override IProductValidator ProductValidator
        {
            get { return productValidator; }
        }

        protected override void ExecuteAfterRequestValidation(ServiceResponse response, Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            AFMServiceResponse afmServiceResponse = null;
            if (response != null)
            {
                afmServiceResponse = new AFMServiceResponse();
                afmServiceResponse.CanRetry = response.CanRetry;
                foreach (var error in response.Errors)
                {
                    AFM.Commerce.Entities.ResponseError responseError = new AFM.Commerce.Entities.ResponseError();
                    responseError.ErrorCode = error.ErrorCode;
                    responseError.ErrorMessage = error.ErrorMessage;
                    responseError.ExtendedErrorMessage = error.ExtendedErrorMessage;
                    responseError.RedirectUrl = error.RedirectUrl;
                    responseError.ShowDebugInfo = error.ShowDebugInfo;
                    responseError.Source = error.Source;
                    responseError.StackTrace = error.StackTrace;
                    afmServiceResponse.Errors.Add(responseError);

                }
                afmServiceResponse.RedirectUrl = response.RedirectUrl;
            }

            AFMDataUtilities.ExecuteAndHandleExceptions(afmServiceResponse, action);
            if (response != null)
            {
                foreach (var error in afmServiceResponse.Errors)
                {
                    Sdk.Services.ResponseError responseError = new Sdk.Services.ResponseError();
                    responseError.ErrorCode = error.ErrorCode;
                    responseError.ErrorMessage = error.ErrorMessage;
                    responseError.ExtendedErrorMessage = error.ExtendedErrorMessage;
                    responseError.RedirectUrl = error.RedirectUrl;
                    responseError.ShowDebugInfo = error.ShowDebugInfo;
                    responseError.Source = error.Source;
                    responseError.StackTrace = error.StackTrace;
                    response.Errors.Add(responseError);

                }
                response.RedirectUrl = response.RedirectUrl;
            }
        }

        //CS Developed by spriya Dated 12/12/2014
        protected override void ExecuteForAuthorizedUser(ServiceResponse response, Action<string> action)
        {
            string customerId = AFMDataUtilities.GetCustomerIdFromCookie();

            ExecuteAfterRequestValidation(response, () =>
            {
                action(customerId);
            });
        }
        //CE Developed by spriya

        protected override void Execute(ServiceResponse response, Action action)
        {
            throw new NotImplementedException();
        }
    }

}
