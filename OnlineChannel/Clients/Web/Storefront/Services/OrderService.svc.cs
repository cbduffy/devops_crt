﻿using AFM.Commerce.Framework.Extensions.Utils;
using AFM.Commerce.Logging;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;
using System;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Services
{
    //[ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class OrderService : OrderServiceBase
    {
        /// <summary>
        /// Get sales orders for the logged in customer.
        /// </summary>
        /// <returns>Sales orders for the logged in customer.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public override SalesOrderCollectionResponse GetSalesOrdersByCustomer(bool includeSalesLines)
        {
            SalesOrderCollectionResponse response = null;
            try
            {
                response = base.GetSalesOrdersByCustomer(includeSalesLines);
            }
            catch (Exception ex)
            {
                LoggerUtilities.ProcessLogMessage(ex);
                response = new SalesOrderCollectionResponse();
                response.AddError("GETORDERERROR", "Error fetching order infomation. Please contact support team.");
            }
            return response;
        }

        //NS - RxL - FDD0258-ECS-Review Customer Profile History Wish List V3 Phased-signed
        /// <summary>
        /// Gets the committed Sales order matching the confirmationId.
        /// </summary>
        /// <param name="confirmationId">ConfirmationId of the order to return.</param>
        /// <returns>Sales Order response containing the matching sales order.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual SalesOrderResponse GetOrderByChannelReferenceId(string confirmationId, string billingEmail, string billingZipCode)
        {
            if (string.IsNullOrEmpty(confirmationId) || confirmationId.Length < 12)
            {
                throw new ArgumentException("Invalid confirmationId.");
            }

            if (string.IsNullOrEmpty(billingEmail))
            {
                throw new ArgumentException("Invalid billingEmail.");
            }

            if (string.IsNullOrEmpty(billingZipCode) || billingZipCode.Length < 5)
            {
                throw new ArgumentException("Invalid billingZipCode.");
            }

            confirmationId = confirmationId.Replace(" ", "");
            confirmationId = confirmationId.Substring(0, 12);
            billingZipCode = billingZipCode.Substring(0, 5);
            SalesOrderResponse response = null;
            try
            {
                response = base.GetCommittedSalesOrderByConfirmationId(confirmationId, true);
            }
            catch 
            {
                response = new SalesOrderResponse();
                response.AddError("GETORDERERROR", "Error fetching order information. Please contact support team.");
            }

            if (response != null && response.SalesOrder != null)
            {
                Address billingAddress = response.SalesOrder.BillingAddress;
                if (!billingAddress.Email.Equals(billingEmail, StringComparison.InvariantCultureIgnoreCase) || !billingAddress.ZipCode.Contains(billingZipCode))
                { 
                    response = new SalesOrderResponse();
                    response.AddError("GETORDERERROR", "Billing email address or zip code does not match. Please verify");
                }
            }
            return response;
        }
        //NE - RxL

        private static readonly ISearchEngine searchEngine = new SearchEngine();
        private static readonly IProductValidator productValidator = new ProductValidator();
        private static readonly ServiceUtilityBase serviceProxy = new ServiceUtility();
        /// <summary>
        /// Gets Demo implementation of ISearchEngine.
        /// </summary>
        protected override ISearchEngine SearchEngine
        {
            get { return searchEngine; }
        }

        /// <summary>
        /// Gets Demo implementation of IProductValidator.
        /// </summary>
        protected override IProductValidator ProductValidator
        {
            get { return productValidator; }
        }

        /// <summary>
        /// Gets Demo implementation of ServiceUtilityBase.
        /// </summary>
        protected override ServiceUtilityBase ServiceUtility
        {
            get { return serviceProxy; }
        }

        /// <summary>
        /// Validates POST request then executes handler with elevated privileges by calling <see cref="Execute"/>.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void ExecuteAfterRequestValidation(ServiceResponse response, System.Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            action();
        }

        /// <summary>
        /// Executes handler and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void Execute(ServiceResponse response, Action action)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if user is authenticated and assosiated then calls <see cref="ExecuteAfterPostRequestValidation"/> with new execution handler.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void ExecuteForAuthorizedUser(ServiceResponse response, Action<string> action)
        {
            string customerId = AFMDataUtilities.GetCustomerIdFromCookie();

            ExecuteAfterRequestValidation(response, () =>
            {
                action(customerId);
            });
        }

        /// <summary>
        /// Gets Demo implementation of IConfiguration.
        /// </summary>
        protected override IConfiguration Configuration
        {
            get { return new SiteConfiguration(); }
        }
    }
}
