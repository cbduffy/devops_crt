﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

// Proxy to make ajax calls.
// relativeUrl - the service relative location of the service example: "/Services/ShoppingCartService.svc/".
// methodName - the method in the service that will be invoked.
// data - the input to the method in json format.
// successCallBack - the function that will be invoked if request is successful.
// failureCallBack - the function that will be invoked if request fails.
function ajaxProxy(relativeUrl, methodName, data, successCallBack, failureCallBack) {
    var webServiceUrl = window.location.protocol + "//" + window.location.host + relativeUrl + methodName;

    $.ajax({
        url: webServiceUrl,
        data: JSON.stringify(data),
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            successCallBack(data);
        },
        error: function (error) {
            failureCallBack(error);
        }
    });
}