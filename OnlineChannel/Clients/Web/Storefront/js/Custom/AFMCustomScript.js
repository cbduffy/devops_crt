﻿
(function ($) {
    $.fn.extend({
        inputWatermark: function () {
            return this.each(function () {
                // retrieve the value of the ‘placeholder’ attribute
                var watermarkText = $(this).attr('placeholder');
                var $this = $(this);
                if ($this.val() === '') {
                    $this.val(watermarkText);
                    // give the watermark a translucent look
                    $this.css({ 'opacity': '0.65' });
                }



                $this.blur(function () {
                    if ($this.val() === '') {
                        // If the text is empty put the watermark
                        // back

                        $this.val(watermarkText);
                        // give the watermark a translucent look
                        $this.css({ 'opacity': '0.65' });
                    }
                });



                $this.focus(function () {
                    if ($this.val() === watermarkText) {
                        $this.val('');
                        $this.css({ 'opacity': '1.0' });
                    }
                });
            });
        }
    });

})(jQuery);
$(document).ready(function () {
    //$(':input').inputWatermark();
    $("#IsTermsAndConditionsCheckBox").change(function () {
        if ($("#IsTermsAndConditionsCheckBox").is(':checked') == true)
            msaxValues.msax_IsAcceptTermsAndConditions = true;
        else
            msaxValues.msax_IsAcceptTermsAndConditions = false;
        var currentdate =new Date();
        var verIsopttimein = "VER" + currentdate.getDate() + "" + (parseInt(currentdate.getMonth()) + 1) + "" + currentdate.getFullYear() + "" + currentdate.getHours() + "" + currentdate.getMinutes() + "" + currentdate.getSeconds();
        var UNIQIsopttimein = "UNI" + currentdate.getDate() + "" + (parseInt(currentdate.getMonth()) + 1) + "" + currentdate.getFullYear() + "" + currentdate.getHours() + "" + currentdate.getMinutes() + "" + currentdate.getSeconds();
        msaxValues.msax_SurveyTermsAndConditions = {
            'OkToTextUniqueId': '1', 'OkToTextVersionId': '1',
            'IsAcceptTermsAndConditions': msaxValues.msax_IsAcceptTermsAndConditions,
            'TermsAndConditionsUniqueId': '1', 'TermsAndConditionsVersionId': '1',
            'IsOptinDealsUniqueId': UNIQIsopttimein,
            'IsOptinDealsVersionId': verIsopttimein

        };
    });

});

$(document).on('GetResources', function (event, data) {
        AFM.Ecommerce.Controls.ExternalResourceStrings = externalResources[data];

        for (var i = 0; i < AFM.Ecommerce.Controls.ExternalResourceStrings.length; i++) {
            if (AFM.Ecommerce.Controls.Resources[AFM.Ecommerce.Controls.ExternalResourceStrings[i].afmcode] != undefined)
                AFM.Ecommerce.Controls.Resources[AFM.Ecommerce.Controls.ExternalResourceStrings[i].afmcode] = AFM.Ecommerce.Controls.ExternalResourceStrings[i].value;
        }
        $(document).trigger('CreateCartData', null);
});

$(document).on('AddtoWishlist', function (event, data) {
   
});
$(document).on('DeleteOnZipcodeCookieUpdate', function (event, data) {

});

$(document).on('SendPaymentEmail', function (event, data) {

});

$(document).on('UpdateDDO_orderDetail', function (event, data) {
    //if (typeof DDO != "undefined")
    //    DDO.orderDetail = data;
});

$(document).on('UpdateDDO_checkoutPages', function (event, data) {
    //if (typeof DDO != "undefined")
    //    DDO.checkoutPages = data;
});

$(document).on('UpdateCartCount', function (event, CartCount) {
    $("#CartCount").text(CartCount);
});

$(document).on('ShowPasswordAndTACInputs', function (event, data) {
    if (data == "Success")
        $("#TermsAncConditionBlock").show();
    else
        $("#TermsAncConditionBlock").hide();
});

// NS Added by AxL for popup change in synchrony FDD on 17/12/2015
$(document).on('ShowNavigatingAwayPopUp', function (event, data) {
    if (msaxValues.msax_PageMode == AFM.Ecommerce.Controls.PageModeOptions.ConfirmViewCompleted)
    {
        if (AFM.Ecommerce.Controls.showNavigatingAwayPopUp) {
            // sitecore code
            //
            // AFM.Ecommerce.Controls.navigateAway need to be assigned booleann value.
            // boolean value "true" will continue with navigation and "false" will cancel navigation. 
            //AFM.Ecommerce.Controls.navigateAway = window.confirm("Navigating back will lead to data loss. Are you sure you want to continue?");
        }
        else {
            AFM.Ecommerce.Controls.navigateAway = true;
        }
            
    }
});