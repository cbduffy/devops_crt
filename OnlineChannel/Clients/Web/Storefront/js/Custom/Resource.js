﻿var externalResources = {
    "en-us": [
        {
            "code": "String_1",
            "afmcode": "String_1",
            "value": "Shopping cart"
        },
        {
            "code": "String_2",
            "afmcode": "String_2",
            "value": "Item"
        },
        {
            "code": "String_3",
            "afmcode": "String_3",
            "value": "Item Price"
        },
        {
            "code": "String_4",
            "afmcode": "String_4",
            "value": "QTY"
        },
        {
            "code": "String_5",
            "afmcode": "String_5",
            "value": "Total"
        },
        {
            "code": "String_6",
            "afmcode": "String_6",
            "value": "Remove"
        },
        {
            "code": "String_7",
            "afmcode": "String_7",
            "value": "You Save"
        },
        {
            "code": "String_8",
            "afmcode": "String_8",
            "value": "Update quantity"
        },
        {
            "code": "String_9",
            "afmcode": "String_9",
            "value": "Subtotal"
        },
        {
            "code": "String_10",
            "afmcode": "String_10",
            "value": "Subtotal:"
        },
        {
            "code": "String_11",
            "afmcode": "String_11",
            "value": "Shipping and handling:"
        },
        {
            "code": "String_12",
            "afmcode": "String_12",
            "value": "Estimated Total:"
        },
        {
            "code": "String_13",
            "afmcode": "String_13",
            "value": "Total savings:"
        },
        {
            "code": "String_14",
            "afmcode": "String_14",
            "value": "CONTINUE"
        },
        {
            "code": "String_15",
            "afmcode": "String_15",
            "value": "There are no items in your cart."
        },
        {
            "code": "String_16",
            "afmcode": "String_16",
            "value": "Delivery information"
        },
        {
            "code": "String_17",
            "afmcode": "String_17",
            "value": "Delivery preference:"
        },
        {
            "code": "String_18",
            "afmcode": "String_18",
            "value": "Shipping Address"
        },
        {
            "code": "String_19",
            "afmcode": "String_19",
            "value": "First Name"
        },
        {
            "code": "String_1097",
            "afmcode": "String_1097",
            "value": "Last Name"
        },
        {
            "code": "String_20",
            "afmcode": "String_20",
            "value": "Country/region"
        },
        {
            "code": "String_21",
            "afmcode": "String_21",
            "value": "United States"
        },
        {
            "code": "String_22",
            "afmcode": "String_22",
            "value": "Address"
        },
        {
            "code": "String_23",
            "afmcode": "String_23",
            "value": "City"
        },
        {
            "code": "String_24",
            "afmcode": "String_24",
            "value": "State/province"
        },
        {
            "code": "String_25",
            "afmcode": "String_25",
            "value": "ZIP/postal code"
        },
        {
            "code": "String_26",
            "afmcode": "String_26",
            "value": "Shipping Method"
        },
        {
            "code": "String_27",
            "afmcode": "String_27",
            "value": "Get shipping options"
        },
        {
            "code": "String_28",
            "afmcode": "String_28",
            "value": "GO BACK"
        },
        {
            "code": "String_29",
            "afmcode": "String_29",
            "value": "Name"
        },
        {
            "code": "String_30",
            "afmcode": "String_30",
            "value": "Billing information"
        },
        {
            "code": "String_31",
            "afmcode": "String_31",
            "value": "Contact information"
        },
        {
            "code": "String_32",
            "afmcode": "String_32",
            "value": "Email address"
        },
        {
            "code": "String_33",
            "afmcode": "String_33",
            "value": "Confirm email address"
        },
        {
            "code": "String_34",
            "afmcode": "String_34",
            "value": "Payment method"
        },
        {
            "code": "String_35",
            "afmcode": "String_35",
            "value": "Card number"
        },
        {
            "code": "String_36",
            "afmcode": "String_36",
            "value": "Card type"
        },
        {
            "code": "String_37",
            "afmcode": "String_37",
            "value": "Expiration month"
        },
        {
            "code": "String_38",
            "afmcode": "String_38",
            "value": "Expiration year"
        },
        {
            "code": "String_39",
            "afmcode": "String_39",
            "value": "CCID"
        },
        {
            "code": "String_40",
            "afmcode": "String_40",
            "value": "What is this?"
        },
        {
            "code": "String_41",
            "afmcode": "String_41",
            "value": "Payment amount"
        },
        {
            "code": "String_42",
            "afmcode": "String_42",
            "value": "Billing Address"
        },
        {
            "code": "String_43",
            "afmcode": "String_43",
            "value": "Same as shipping address"
        },
        {
            "code": "String_44",
            "afmcode": "String_44",
            "value": "Address2"
        },
        {
            "code": "String_45",
            "afmcode": "String_45",
            "value": "Review and confirm"
        },
        {
            "code": "String_46",
            "afmcode": "String_46",
            "value": "Order information"
        },
        {
            "code": "String_47",
            "afmcode": "String_47",
            "value": "Edit"
        },
        {
            "code": "String_48",
            "afmcode": "String_48",
            "value": "Credit card"
        },
        {
            "code": "String_49",
            "afmcode": "String_49",
            "value": "Proceed to Checkout"
        },
        {
            "code": "String_50",
            "afmcode": "String_50",
            "value": "Apply Promo code"
        },
        {
            "code": "String_51",
            "afmcode": "String_51",
            "value": "Tax:"
        },
        {
            "code": "String_52",
            "afmcode": "String_52",
            "value": "Submit order"
        },
        {
            "code": "String_53",
            "afmcode": "String_53",
            "value": "Thank you for your order"
        },
        {
            "code": "String_54",
            "afmcode": "String_54",
            "value": "Your order confirmation number is "
        },
        {
            "code": "String_55",
            "afmcode": "String_55",
            "value": "Street"
        },
        {
            "code": "String_56",
            "afmcode": "String_56",
            "value": "State"
        },
        {
            "code": "String_57",
            "afmcode": "String_57",
            "value": "Zipcode"
        },
        {
            "code": "String_58",
            "afmcode": "String_58",
            "value": "Email"
        },
        {
            "code": "String_59",
            "afmcode": "String_59",
            "value": "Payment"
        },
        {
            "code": "String_60",
            "afmcode": "String_60",
            "value": "CardNumber"
        },
        {
            "code": "String_61",
            "afmcode": "String_61",
            "value": "Please select shipping method"
        },
        {
            "code": "String_62",
            "afmcode": "String_62",
            "value": "The confirm email address must match the email address."
        },
        {
            "code": "String_63",
            "afmcode": "String_63",
            "value": "Sorry, something went wrong. The shopping cart information couldn't be retrieved. Please refresh the page and try again."
        },
        {
            "code": "String_64",
            "afmcode": "String_64",
            "value": "Sorry, something went wrong. The product was not removed from the cart successfully. Please refresh the page and try again."
        },
        {
            "code": "String_65",
            "afmcode": "String_65",
            "value": "Sorry, something went wrong. The product quantity couldn't be updated. Please refresh the page and try again."
        },
        {
            "code": "String_66",
            "afmcode": "String_66",
            "value": "Sorry, something went wrong. Delivery methods could not be retrieved. Please refresh the page and try again."
        },
        {
            "code": "String_67",
            "afmcode": "String_67",
            "value": "Sorry, something went wrong. The shipping information was not stored successfully. Please refresh the page and try again."
        },
        {
            "code": "String_68",
            "afmcode": "String_68",
            "value": "Sorry, something went wrong. The payment card type information was not retrieved successfully. Please refresh the page and try again."
        },
        {
            "code": "String_69",
            "afmcode": "String_69",
            "value": "We are unable to process your request; Please re-submit your Order."
        },
        {
            "code": "String_70",
            "afmcode": "String_70",
            "value": "Invalid parameter"
        },
        {
            "code": "String_71",
            "afmcode": "String_71",
            "value": "validatorType attribute is not provided for validator binding."
        },
        {
            "code": "String_72",
            "afmcode": "String_72",
            "value": "Use text characters only. Numbers, spaces, and special characters are not allowed."
        },
        {
            "code": "String_73",
            "afmcode": "String_73",
            "value": "Use text characters only. Numbers, spaces, and special characters are not allowed."
        },
        {
            "code": "String_74",
            "afmcode": "String_74",
            "value": "The quantity field cannot be empty"
        },
        {
            "code": "String_75",
            "afmcode": "String_75",
            "value": "Select delivery method."
        },
        {
            "code": "String_76",
            "afmcode": "String_76",
            "value": "The email address is invalid."
        },
        {
            "code": "String_77",
            "afmcode": "String_77",
            "value": "Please enter the name."
        },
        {
            "code": "String_78",
            "afmcode": "String_78",
            "value": "The street number you entered may not be accurate. Please make corrections if needed, or use the address as entered."
        },
        {
            "code": "String_79",
            "afmcode": "String_79",
            "value": "Please enter your {addresstype} address."
        },
        {
            "code": "String_80",
            "afmcode": "String_80",
            "value": "Please enter your {addresstype} city."
        },
        {
            "code": "String_81",
            "afmcode": "String_81",
            "value": "Please enter your {addresstype} zip code."
        },
        {
            "code": "String_82",
            "afmcode": "String_82",
            "value": "Please enter your {addresstype} state."
        },
        {
            "code": "String_83",
            "afmcode": "String_83",
            "value": "Please enter the country."
        },
        {
            "code": "String_84",
            "afmcode": "String_84",
            "value": "Please specify a payment card name."
        },
        {
            "code": "String_85",
            "afmcode": "String_85",
            "value": "Please enter a valid card number."
        },
        {
            "code": "String_86",
            "afmcode": "String_86",
            "value": "Please enter a valid CCID."
        },
        {
            "code": "String_87",
            "afmcode": "String_87",
            "value": "Please specify a valid amount."
        },
        {
            "code": "String_88",
            "afmcode": "String_88",
            "value": "{0} PRODUCT(S)"
        },
        {
            "code": "String_89",
            "afmcode": "String_89",
            "value": "Included"
        },
        {
            "code": "String_90",
            "afmcode": "String_90",
            "value": "Color: {0}"
        },
        {
            "code": "String_91",
            "afmcode": "String_91",
            "value": "Size: {0}"
        },
        {
            "code": "String_92",
            "afmcode": "String_92",
            "value": "Style: {0}"
        },
        {
            "code": "String_93",
            "afmcode": "String_93",
            "value": "Sorry, something went wrong. The promotion code could not be added successfully. Please refresh the page and try again."
        },
        {
            "code": "String_94",
            "afmcode": "String_94",
            "value": "Sorry, something went wrong. The promotion code could not be removed successfully. Please refresh the page and try again."
        },
        {
            "code": "String_95",
            "afmcode": "String_95",
            "value": "Apply"
        },
        {
            "code": "String_96",
            "afmcode": "String_96",
            "value": "Promotion codes"
        },
        {
            "code": "String_97",
            "afmcode": "String_97",
            "value": "Please enter a promotion code"
        },
        {
            "code": "String_98",
            "afmcode": "String_98",
            "value": "Sorry, something went wrong. The channel configuration could not be retrieved successfully. Please refresh the page and try again."
        },
        {
            "code": "String_99",
            "afmcode": "String_99",
            "value": "Ship items"
        },
        {
            "code": "String_100",
            "afmcode": "String_100",
            "value": ""
        },
        {
            "code": "String_101",
            "afmcode": "String_101",
            "value": "Select delivery options by item"
        },
        {
            "code": "String_102",
            "afmcode": "String_102",
            "value": "Find a store"
        },
        {
            "code": "String_103",
            "afmcode": "String_103",
            "value": "miles"
        },
        {
            "code": "String_104",
            "afmcode": "String_104",
            "value": "Available stores"
        },
        {
            "code": "String_105",
            "afmcode": "String_105",
            "value": "Store"
        },
        {
            "code": "String_106",
            "afmcode": "String_106",
            "value": "Distance"
        },
        {
            "code": "String_107",
            "afmcode": "String_107",
            "value": "Products are not available for pick up in the stores around the location you searched. Please update your delivery preferences and try again."
        },
        {
            "code": "String_108",
            "afmcode": "String_108",
            "value": "Sorry, something went wrong. An error occurred while trying to get stores. Please refresh the page and try again."
        },
        {
            "code": "String_109",
            "afmcode": "String_109",
            "value": "Sorry, your address could not be validated. Please re-enter a valid address."
        },
        {
            "code": "String_110",
            "afmcode": "String_110",
            "value": "Sorry, something went wrong. An error has occured while looking up the address you provided. Please refresh the page and try again."
        },
        {
            "code": "String_111",
            "afmcode": "String_111",
            "value": "Products are not available in this store"
        },
        {
            "code": "String_112",
            "afmcode": "String_112",
            "value": "Product availability:"
        },
        {
            "code": "String_113",
            "afmcode": "String_113",
            "value": "Products are not available in the selected store, Please select a different store"
        },
        {
            "code": "String_114",
            "afmcode": "String_114",
            "value": "Please select a store for pick up"
        },
        {
            "code": "String_115",
            "afmcode": "String_115",
            "value": "Store address"
        },
        {
            "code": "String_116",
            "afmcode": "String_116",
            "value": "Send to me"
        },
        {
            "code": "String_117",
            "afmcode": "String_117",
            "value": "Optional note"
        },
        {
            "code": "String_118",
            "afmcode": "String_118",
            "value": "Please enter email address for gift card delivery"
        },
        {
            "code": "String_119",
            "afmcode": "String_119",
            "value": "Sorry, something went wrong. An error occurred while trying to get the email address. Please enter the email address in the text box below"
        },
        {
            "code": "String_120",
            "afmcode": "String_120",
            "value": "Enter the shipping address and then click get shipping options to view the shipping options that are available for your area."
        },
        {
            "code": "String_121",
            "afmcode": "String_121",
            "value": "Delivery method"
        },
        {
            "code": "String_122",
            "afmcode": "String_122",
            "value": "Select your delivery preference"
        },
        {
            "code": "String_123",
            "afmcode": "String_123",
            "value": "Cancel"
        },
        {
            "code": "String_124",
            "afmcode": "String_124",
            "value": "Done"
        },
        {
            "code": "String_125",
            "afmcode": "String_125",
            "value": "for product: {0}"
        },
        {
            "code": "String_126",
            "afmcode": "String_126",
            "value": "Please select delivery preference for product {0}"
        },
        {
            "code": "String_127",
            "afmcode": "String_127",
            "value": "Add credit card"
        },
        {
            "code": "String_128",
            "afmcode": "String_128",
            "value": "Gift card"
        },
        {
            "code": "String_129",
            "afmcode": "String_129",
            "value": "Add gift card"
        },
        {
            "code": "String_130",
            "afmcode": "String_130",
            "value": "Loyalty card"
        },
        {
            "code": "String_131",
            "afmcode": "String_131",
            "value": "Add loyalty card"
        },
        {
            "code": "String_132",
            "afmcode": "String_132",
            "value": "Payment information"
        },
        {
            "code": "String_133",
            "afmcode": "String_133",
            "value": "Payment total:"
        },
        {
            "code": "String_134",
            "afmcode": "String_134",
            "value": "Order total:"
        },
        {
            "code": "String_135",
            "afmcode": "String_135",
            "value": "Gift card does not exist"
        },
        {
            "code": "String_136",
            "afmcode": "String_136",
            "value": "Gift card balance"
        },
        {
            "code": "String_137",
            "afmcode": "String_137",
            "value": "Card details"
        },
        {
            "code": "String_138",
            "afmcode": "String_138",
            "value": "Sorry, something went wrong. An error occurred while trying to get payment methods supported by the store. Please refresh the page and try again."
        },
        {
            "code": "String_139",
            "afmcode": "String_139",
            "value": "Please select payment method"
        },
        {
            "code": "String_140",
            "afmcode": "String_140",
            "value": "The expiration date is not valid. Please select valid expiration month and year and then try again"
        },
        {
            "code": "String_141",
            "afmcode": "String_141",
            "value": "Please enter a valid gift card number"
        },
        {
            "code": "String_142",
            "afmcode": "String_142",
            "value": "Get gift card balance"
        },
        {
            "code": "String_143",
            "afmcode": "String_143",
            "value": "Apply full amount"
        },
        {
            "code": "String_144",
            "afmcode": "String_144",
            "value": "Please enter a gift card number"
        },
        {
            "code": "String_145",
            "afmcode": "String_145",
            "value": "Sorry, something went wrong. An error occurred while trying to get gift card balance. Please refresh the page and try again."
        },
        {
            "code": "String_146",
            "afmcode": "String_146",
            "value": "Gift card payment amount cannot be zero"
        },
        {
            "code": "String_147",
            "afmcode": "String_147",
            "value": "Gift card payment amount is more than order total"
        },
        {
            "code": "String_148",
            "afmcode": "String_148",
            "value": "Gift card does not have sufficient balance"
        },
        {
            "code": "String_149",
            "afmcode": "String_149",
            "value": "Payment amount is different from the order total"
        },
        {
            "code": "String_150",
            "afmcode": "String_150",
            "value": "Sorry, something went wrong. An error occurred while trying to get loyalty card information. Please refresh the page and try again."
        },
        {
            "code": "String_151",
            "afmcode": "String_151",
            "value": "Please enter a valid loyalty card number"
        },
        {
            "code": "String_152",
            "afmcode": "String_152",
            "value": "Loyalty card payment amount cannot be zero"
        },
        {
            "code": "String_153",
            "afmcode": "String_153",
            "value": "Loyalty card payment amount is more than order total"
        },
        {
            "code": "String_154",
            "afmcode": "String_154",
            "value": "The loyalty card is blocked"
        },
        {
            "code": "String_155",
            "afmcode": "String_155",
            "value": "The loyalty card is not allowed for payment"
        },
        {
            "code": "String_156",
            "afmcode": "String_156",
            "value": "The loyalty payment amount exceeds what is allowed for this loyalty card in this transaction"
        },
        {
            "code": "String_157",
            "afmcode": "String_157",
            "value": "The loyalty card number does not exist"
        },
        {
            "code": "String_158",
            "afmcode": "String_158",
            "value": "Please select delivery preference"
        },
        {
            "code": "String_159",
            "afmcode": "String_159",
            "value": "Please select a delivery preference..."
        },
        {
            "code": "String_160",
            "afmcode": "String_160",
            "value": "Sorry, something went wrong. An error occurred while trying to get delivery methods information. Please refresh the page and try again."
        },
        {
            "code": "String_161",
            "afmcode": "String_161",
            "value": "Select address..."
        },
        {
            "code": "String_162",
            "afmcode": "String_162",
            "value": "You have not added loyalty card number to your order"
        },
        {
            "code": "String_163",
            "afmcode": "String_163",
            "value": "Enter a reward card for the current order. You can include only one reward card per order"
        },
        {
            "code": "String_164",
            "afmcode": "String_164",
            "value": "Sorry, something went wrong. An error occurred while trying to update reward card id in cart. Please refresh the page and try again."
        },
        {
            "code": "String_165",
            "afmcode": "String_165",
            "value": "Sorry, something went wrong. An error occurred while retrieving the country region information. Please refresh the page and try again."
        },
        {
            "code": "String_166",
            "afmcode": "String_166",
            "value": "TBD"
        },
        {
            "code": "String_167",
            "afmcode": "String_167",
            "value": "Mini Cart"
        },
        {
            "code": "String_168",
            "afmcode": "String_168",
            "value": "Ordering FAQ"
        },
        {
            "code": "String_169",
            "afmcode": "String_169",
            "value": "Return policy"
        },
        {
            "code": "String_170",
            "afmcode": "String_170",
            "value": "Store locator tool"
        },
        {
            "code": "String_171",
            "afmcode": "String_171",
            "value": "Continue shopping"
        },
        {
            "code": "String_172",
            "afmcode": "String_172",
            "value": "Cart Order Total"
        },
        {
            "code": "String_173",
            "afmcode": "String_173",
            "value": "View full cart contents"
        },
        {
            "code": "String_174",
            "afmcode": "String_174",
            "value": "Quantity:"
        },
        {
            "code": "String_175",
            "afmcode": "String_175",
            "value": "Added to your cart:"
        },
        {
            "code": "String_176",
            "afmcode": "String_176",
            "value": "Loading ..."
        },
        {
            "code": "String_177",
            "afmcode": "String_177",
            "value": "Sorry, something went wrong. The cart's promotion information couldn't be retrieved. Please refresh the page and try again."
        },
        {
            "code": "String_178",
            "afmcode": "String_178",
            "value": "Delivery method"
        },
        {
            "code": "String_179",
            "afmcode": "String_179",
            "value": "Updating shopping cart ..."
        },
        {
            "code": "String_180",
            "afmcode": "String_180",
            "value": "Submitting order ..."
        },
        {
            "code": "String_181",
            "afmcode": "String_181",
            "value": "Discount code"
        },
        {
            "code": "String_182",
            "afmcode": "String_182",
            "value": "Add coupon code"
        },
        {
            "code": "String_183",
            "afmcode": "String_183",
            "value": "Enter a discount code"
        },
        {
            "code": "String_184",
            "afmcode": "String_184",
            "value": "Please enter a valid discount code"
        },
        {
            "code": "String_185",
            "afmcode": "String_185",
            "value": "Sorry, something went wrong. An error occurred while retrieving the state/province information. Please refresh the page and try again."
        },
        {
            "code": "String_1098",
            "afmcode": "String_1098",
            "value": "Shipping"
        },
        {
            "code": "String_1099",
            "afmcode": "String_1099",
            "value": "Shipping:"
        },
        {
            "code": "String_1101",
            "afmcode": "String_1101",
            "value": "Sale price and shipping are based on shipping to "
        },
        {
            "code": "String_1102",
            "afmcode": "String_1102",
            "value": "zip code"
        },
        {
            "code": "String_1103",
            "afmcode": "String_1103",
            "value": "(Update delivery zip code)"
        },
        {
            "code": "String_1104",
            "afmcode": "String_1104",
            "value": "Continue shopping"
        },
        {
            "code": "String_1105",
            "afmcode": "String_1105",
            "value": "Move to Wish List"
        },
        {
            "code": "String_1106",
            "afmcode": "String_1106",
            "value": "Est. Shipping:"
        },
        {
            "code": "String_1107",
            "afmcode": "String_1107",
            "value": "Est. Home Delivery:"
        },
        {
            "code": "String_1108",
            "afmcode": "String_1108",
            "value": "Fulfilled By : "
        },
        {
            "code": "String_1109",
            "afmcode": "String_1109",
            "value": "On Sale"
        },
        {
            "code": "String_1110",
            "afmcode": "String_1110",
            "value": "Update"
        },
        {
            "code": "String_1111",
            "afmcode": "String_1111",
            "value": "Sorry, your address could not be validated. Please re-enter a valid address."
        },
        {
            "code": "String_1112",
            "afmcode": "String_1112",
            "value": "Zipcode can be of maximum 5 numbers"
        },
        {
            "code": "String_1113",
            "afmcode": "String_1113",
            "value": "Error processing payment authorization, Please click 'Go back' button and re-enter payment details"
        },
        {
            "code": "String_1114",
            "afmcode": "String_1114",
            "value": "Error Updating Zipcode. Please try again"
        },
        {
            "code": "String_1115",
            "afmcode": "String_1115",
            "value": "Go back"
        },
        {
            "code": "String_1116",
            "afmcode": "String_1116",
            "value": "Opt-in to Deals & Offers"
        },
        {
            "code": "String_1117",
            "afmcode": "String_1117",
            "value": "USE THIS ADDRESS"
        },
        {
            "code": "String_1118",
            "afmcode": "String_1118",
            "value": "Enter Shipping Address"
        },
        {
            "code": "String_1119",
            "afmcode": "String_1119",
            "value": "Choose a Shipping Address"
        },
        {
            "code": "String_1120",
            "afmcode": "String_1120",
            "value": "Contact Information (for shipping & delivery scheduling purposes)"
        },
        {
            "code": "String_1121",
            "afmcode": "String_1121",
            "value": "Agree to receive text survey about your purchase"
        },
        {
            "code": "String_1122",
            "afmcode": "String_1122",
            "value": "Country"
        },
        {
            "code": "String_1123",
            "afmcode": "String_1123",
            "value": "Order Submission in process... please wait..."
        },
        {
            "code": "String_1124",
            "afmcode": "String_1124",
            "value": "Address 2: (Optional)"
        },
        {
            "code": "String_1125",
            "afmcode": "String_1125",
            "value": "Phone Number:"
        },
        {
            "code": "String_1126",
            "afmcode": "String_1126",
            "value": "Alternate Number (Optional)"
        },
        {
            "code": "String_1127",
            "afmcode": "String_1127",
            "value": "Contact Information (for shipping and delivery scheduling purposes)"
        },
        {
            "code": "String_1128",
            "afmcode": "String_1128",
            "value": "Alt : "
        },
        {
            "code": "String_1129",
            "afmcode": "String_1129",
            "value": "Email : "
        },
        {
            "code": "String_1130",
            "afmcode": "String_1130",
            "value": "Alt Email : "
        },
        {
            "code": "String_1131",
            "afmcode": "String_1131",
            "value": "Alt : "
        },
        {
            "code": "String_1132",
            "afmcode": "String_1132",
            "value": "Name on Card : "
        },
        {
            "code": "String_1133",
            "afmcode": "String_1133",
            "value": "Commencing your cart to checkout..."
        },
        {
            "code": "String_1134",
            "afmcode": "String_1134",
            "value": "No Payment Card Information provided"
        },
        {
            "code": "String_1135",
            "afmcode": "String_1135",
            "value": "No Shipping charges applied"
        },
        {
            "code": "String_1136",
            "afmcode": "String_1136",
            "value": "Alternate Email (Optional)"
        },
        {
            "code": "String_1137",
            "afmcode": "String_1137",
            "value": "Or "
        },
        {
            "code": "String_1138",
            "afmcode": "String_1138",
            "value": "Qty: "
        },
        {
            "code": "String_1139",
            "afmcode": "String_1139",
            "value": "Billing"
        },
        {
            "code": "String_1140",
            "afmcode": "String_1140",
            "value": "Confirm Your Order"
        },
        {
            "code": "String_1141",
            "afmcode": "String_1141",
            "value": "You applied promo code : "
        },
        {
            "code": "String_1142",
            "afmcode": "String_1142",
            "value": "Confirmation #"
        },
        {
            "code": "String_1143",
            "afmcode": "String_1143",
            "value": "Agree to receive a text survey on your purchase"
        },
        {
            "code": "String_1144",
            "afmcode": "String_1144",
            "value": "I Agree with the Terms and Conditions"
        },
        {
            "code": "String_1145",
            "afmcode": "String_1145",
            "value": " Please remove item having zero price."
        },
        {
            "code": "String_1146",
            "afmcode": "String_1146",
            "value": "Shipping:"
        },
        {
            "code": "String_1147",
            "afmcode": "String_1147",
            "value": "Home Delivery:"
        },
        {
            "code": "String_1148",
            "afmcode": "String_1148",
            "value": "Please enter a positive quantity"
        },
        {
            "code": "String_1149",
            "afmcode": "String_1149",
            "value": "Please accept Terms & Conditions to complete your order"
        },
        {
            "code": "String_1150",
            "afmcode": "String_1150",
            "value": "The Delivery Zip Code Entered is not valid, please enter a new Delivery Zip Code"
        },
        {
            "code": "String_1151",
            "afmcode": "String_1151",
            "value": "Sorry, something went wrong. Address Validation is not successful. Please refresh the page and try again."
        },
        {
            "code": "String_1152",
            "afmcode": "String_1152",
            "value": "USA"
        },
        {
            "code": "String_1153",
            "afmcode": "String_1153",
            "value": "Update Delivery Zip Code:"
        },
        {
            "code": "String_1154",
            "afmcode": "String_1154",
            "value": "Are you sure you want to remove this product from the shopping cart?"
        },
        {
            "code": "String_1155",
            "afmcode": "String_1155",
            "value": "Remove from Cart"
        },
        {
            "code": "String_1156",
            "afmcode": "String_1156",
            "value": "Your Shopping Cart Is Empty"
        },
        {
            "code": "String_1157",
            "afmcode": "String_1157",
            "value": "(Required)"
        },
        {
            "code": "String_1158",
            "afmcode": "String_1158",
            "value": "Verify Your Address"
        },
        {
            "code": "String_1159",
            "afmcode": "String_1159",
            "value": "We checked your address with the U.S. Postal service and found some differences. Please click cancel to edit the address you entered or click 'Use This Address' button to use the suggested address."
        },
        {
            "code": "String_1160",
            "afmcode": "String_1160",
            "value": "Set of "
        },
        {
            "code": "String_1161",
            "afmcode": "String_1161",
            "value": "Tax could not be calculated.Please try again after sometime"
        },
        {
            "code": "String_1162",
            "afmcode": "String_1162",
            "value": "Your shopping cart is currently empty. Please "
        },
        {
            "code": "String_1163",
            "afmcode": "String_1163",
            "value": "to retrieve any items placed in your cart from a previous visit or continue shopping."
        }
        ,
        {
            "code": "String_1164",
            "afmcode": "String_1164",
            "value": "Promotion Code is not applied to any item in cart."
        },
        {
            "code": "String_1165",
            "afmcode": "String_1165",
            "value": "Promotion Code is already applied."
        },

        {
            "code": "String_1166",
            "afmcode": "String_1166",
            "value": "Sorry, at this time, we cannot ship to p.o. box. Please enter your street address."

        },
         {
             "code": "String_1167",
             "afmcode": "String_1167",
             "value": "poerror"

         },
          {
              "code": "String_1168",
              "afmcode": "String_1168",
              "value": "+1"

          },
          {
              "code": "String_1169",
              "afmcode": "String_1169",
              "value": "Please enter 10 digit number."

          },
          {
              "code": "String_1170",
              "afmcode": "String_1170",
              "value": "Address First Name or Last Name can have only alphabets."

          },

          //=====================Phase 2 =====================================
          {
              "code": "String_1171",
              "afmcode": "String_1171",
              "value": "Free Shipping"
          },
          {
              "code": "String_1172",
              "afmcode": "String_1172",
              "value": "Free Home Delivery"
          }
          ,
          {
              "code": "String_1173",
              "afmcode": "String_1173",
              "value": "You have changed the delivery zip-code, price(s) in your cart may have changed."
          }
          ,
          {
              "code": "String_1174",
              "afmcode": "String_1174",
              "value": "You have entered a new delivery address, price(s) in your cart may have changed. Please confirm your order."
          }
          ,
          {
              "code": "String_1175",
              "afmcode": "String_1175",
              "value": "Phone Number:"
          }
          ,
          {
              "code": "String_1176",
              "afmcode": "String_1176",
              "value": "Phone Number:"
          }
          ,
          {
              "code": "String_1177",
              "afmcode": "String_1177",
              "value": "Phone Number:"
          }
          ,
          {
              "code": "String_1178",
              "afmcode": "String_1178",
              "value": "Phone Number:"
          }
          ,
          {
              "code": "String_1179",
              "afmcode": "String_1179",
              "value": "Tel : "
          }
          ,
          {
              "code": "String_1180",
              "afmcode": "String_1180",
              "value": "Tel : "
          }
          ,
          {
              "code": "String_1181",
              "afmcode": "String_1181",
              "value": "Tel : "
          }
          ,
          {
              "code": "String_1182",
              "afmcode": "String_1182",
              "value": "Alternate Number (Highly Recommended)"
          }
          ,
          {
              "code": "String_1183",
              "afmcode": "String_1183",
              "value": "Alternate Number (Optional)"
          }
          ,
          {
              "code": "String_1184",
              "afmcode": "String_1184",
              "value": "Contact Information (for shipping and delivery scheduling purposes)"
          }
          ,
          {
              "code": "String_1185",
              "afmcode": "String_1185",
              "value": "First Name"
          }
          ,
          {
              "code": "String_1186",
              "afmcode": "String_1186",
              "value": "First Name"
          }
          ,
          {
              "code": "String_1187",
              "afmcode": "String_1187",
              "value": "Last Name"
          }
          ,
          {
              "code": "String_1188",
              "afmcode": "String_1188",
              "value": "Last Name"
          }
          ,
          {
              "code": "String_1189",
              "afmcode": "String_1189",
              "value": "Address"
          }
          ,
          {
              "code": "String_1190",
              "afmcode": "String_1190",
              "value": "Address 2: (Optional)"
          }
           ,
          {
              "code": "String_1191",
              "afmcode": "String_1191",
              "value": "City"
          }
          ,
          {
              "code": "String_1192",
              "afmcode": "String_1192",
              "value": "State/province"
          }
          ,
          {
              "code": "String_1193",
              "afmcode": "String_1193",
              "value": "ZIP/postal code"
          }
          ,
          {
              "code": "String_1194",
              "afmcode": "String_1194",
              "value": "Country/region"
          }
          ,
          {
              "code": "String_1195",
              "afmcode": "String_1195",
              "value": "Email address"
          }
          ,
          {
              "code": "String_1196",
              "afmcode": "String_1196",
              "value": "Confirm email address"
          }
           ,
          {
              "code": "String_1197",
              "afmcode": "String_1197",
              "value": "GO BACK"
          }
         ,
          {

              "code": "String_1198",
              "afmcode": "String_1198",
              "value": "GO BACK"
          }
          ,
          {

              "code": "String_1199",
              "afmcode": "String_1199",
              "value": "Billing Address"
          }
          ,
           {

               "code": "String_1200",
               "afmcode": "String_1200",
               "value": "CONTINUE"
           }
           ,
           {

               "code": "String_1201",
               "afmcode": "String_1201",
               "value": "Continue shopping"
           }
           ,
           {

               "code": "String_1202",
               "afmcode": "String_1202",
               "value": "Continue shopping"
           }
            ,
           {

               "code": "String_1203",
               "afmcode": "String_1203",
               "value": "Proceed to Checkout"
           }
            ,
           {

               "code": "String_1204",
               "afmcode": "String_1204",
               "value": "Proceed to Checkout"
           }
           ,
           {

               "code": "String_1205",
               "afmcode": "String_1205",
               "value": "Shipping Address"
           }
           ,
           {

               "code": "String_1206",
               "afmcode": "String_1206",
               "value": "+1"
           }
           ,
           {

               "code": "String_1207",
               "afmcode": "String_1207",
               "value": "+1"
           }
           ,
           {

               "code": "String_1208",
               "afmcode": "String_1208",
               "value": "+1"
           }
           ,
           {

               "code": "String_1209",
               "afmcode": "String_1209",
               "value": "Email : "
           }
           ,
           {

               "code": "String_1210",
               "afmcode": "String_1210",
               "value": "Shipping"
           }
       ,
           {

               "code": "String_1211",
               "afmcode": "String_1211",
               "value": "Shipping:"
           }
           ,
           {

               "code": "String_1212",
               "afmcode": "String_1212",
               "value": "Shipping:"
           }
           ,
           {

               "code": "String_1213",
               "afmcode": "String_1213",
               "value": "Shipping:"
           }
           ,
           {

               "code": "String_1214",
               "afmcode": "String_1214",
               "value": "Free Shipping"
           }
            ,
           {

               "code": "String_1215",
               "afmcode": "String_1215",
               "value": "Home Delivery:"
           }
            ,
           {

               "code": "String_1216",
               "afmcode": "String_1216",
               "value": "Home Delivery:"
           }
            ,
           {

               "code": "String_1217",
               "afmcode": "String_1217",
               "value": "Home Delivery:"
           }
            ,
           {

               "code": "String_1218",
               "afmcode": "String_1218",
               "value": "Free Home Delivery"
           }
            ,
           {

               "code": "String_1219",
               "afmcode": "String_1219",
               "value": "Subtotal:"
           }
            ,
           {

               "code": "String_1220",
               "afmcode": "String_1220",
               "value": "Subtotal:"
           }
            ,
           {

               "code": "String_1221",
               "afmcode": "String_1221",
               "value": "Subtotal:"
           }
            ,
           {

               "code": "String_1222",
               "afmcode": "String_1222",
               "value": "Subtotal:"
           }
            ,
           {

               "code": "String_1223",
               "afmcode": "String_1223",
               "value": "Subtotal:"
           }
            ,
           {

               "code": "String_1224",
               "afmcode": "String_1224",
               "value": "Subtotal:"
           }
            ,
           {

               "code": "String_1225",
               "afmcode": "String_1225",
               "value": "Tax:"
           }
            ,
           {

               "code": "String_1226",
               "afmcode": "String_1226",
               "value": "You Save"
           }
           ,
           {

               "code": "String_1227",
               "afmcode": "String_1227",
               "value": "You Save"
           }
            ,
           {

               "code": "String_1228",
               "afmcode": "String_1228",
               "value": "You Save"
           }
            ,
           {

               "code": "String_1229",
               "afmcode": "String_1229",
               "value": "Apply Promo Code"
           }
           ,
           {

               "code": "String_1230",
               "afmcode": "String_1230",
               "value": "Apply"
           }
           ,
           {

               "code": "String_1231",
               "afmcode": "String_1231",
               "value": "You applied promo code : "
           }
           ,
           {

               "code": "String_1232",
               "afmcode": "String_1232",
               "value": "Item"
           }
          ,
          {

              "code": "String_1233",
              "afmcode": "String_1233",
              "value": "Confirmation #"
          }
          ,
          {

              "code": "String_1234",
              "afmcode": "String_1234",
              "value": "Set of "
          }
          ,
          {

              "code": "String_1235",
              "afmcode": "String_1235",
              "value": "On Sale"
          }
           ,
          {

              "code": "String_1236",
              "afmcode": "String_1236",
              "value": "Move to Wish List"
          }
           ,
          {

              "code": "String_1237",
              "afmcode": "String_1237",
              "value": "Remove"
          }
           ,
          {

              "code": "String_1238",
              "afmcode": "String_1238",
              "value": "Loyalty card"
          }
           ,
          {

              "code": "String_1239",
              "afmcode": "String_1239",
              "value": "Loyalty card"
          }
             ,
          {

              "code": "String_1240",
              "afmcode": "String_1240",
              "value": "Edit"
          }
            ,
          {

              "code": "String_1241",
              "afmcode": "String_1241",
              "value": "Done"
          }
            ,
          {

              "code": "String_1242",
              "afmcode": "String_1242",
              "value": "Your Shopping Cart Is Empty"
          }
             ,
          {

              "code": "String_1243",
              "afmcode": "String_1243",
              "value": "Update"
          },
           //========================== Phase 2 =======================
          {

              "code": "String_1244",
              "afmcode": "String_1244",
              "value": "Free Shipping"
          },
          {

              "code": "String_1245",
              "afmcode": "String_1245",
              "value": "What's this?"
          }
          ,
          {

              "code": "String_1246",
              "afmcode": "String_1246",
              "value": "Why do we recommend an alternate phone number?  To ensure we can contact you to schedule your delivery. Providing a second number give as a better chance of reaching you."
          },
          {

              "code": "String_1247",
              "afmcode": "String_1247",
              "value": "Recommended for Scheduling & Delivery"
          },
          {
              "code": "String_1248",
              "afmcode": "String_1248",
              "value": "Please Enter Zipcode"
          },
          {
              "code": "String_1249",
              "afmcode": "String_1249",
              "value": "Please Enter Loyalty Number"
          },
          {
              "code": "String_1250",
              "afmcode": "String_1250",
              "value": "Please Click To Edit Reward"
          },
          {
              "code": "String_1251",
              "afmcode": "String_1251",
              "value": "Please Enter Promotion Code"
          },
          {
              "code": "String_1252",
              "afmcode": "String_1252",
              "value": "Please Enter Quantity"
          },
          {
              "code": "String_1253",
              "afmcode": "String_1253",
              "value": "Please Select Quantity"
          },
          {
              "code": "String_1254",
              "afmcode": "String_1254",
              "value": "Please Click to SignIn"
          },
          {
              "code": "String_1255",
              "afmcode": "String_1255",
              "value": "Please Enter First Name"
          },
          {
              "code": "String_1256",
              "afmcode": "String_1256",
              "value": "Please Enter Last Name"
          },
          {
              "code": "String_1257",
              "afmcode": "String_1257",
              "value": "(Sales tax will be applied during checkout)"
          },
          {
              "code": "String_1258",
              "afmcode": "String_1258",
              "value": "There appears to be a problem with the city, state and zip combination. Please try again."
          },
          {
              "code": "String_1259",
              "afmcode": "String_1259",
              "value": "Invalid city, state or zip combination."
          },
          {
              "code": "String_1300",
              "afmcode": "String_1300",
              "value": "Apply Promo Code(s)"
          },
          {
              "code": "String_1301",
              "afmcode": "String_1301",
              "value": "Edit Promo Code(s)"
          },
          {
              "code": "String_1302",
              "afmcode": "String_1302",
              "value": "Sorry, this code is invalid"
          },
          {
              "code": "String_1303",
              "afmcode": "String_1303",
              "value": "Apply Promo Code"
          },
          {
              "code": "String_1304",
              "afmcode": "String_1304",
              "value": "Enter Code:"
          },
          {
              "code": "String_1305",
              "afmcode": "String_1305",
              "value": "Did a Sales Associate Help You?"
          },
          {
              "code": "String_1306",
              "afmcode": "String_1306",
              "value": "RSA Code:"
          },
          {
              "code": "String_1307",
              "afmcode": "String_1307",
              "value": "(edit)"
          },
          {
              "code": "String_1308",
              "afmcode": "String_1308",
              "value": "(remove)"
          },
          {
              "code": "String_1309",
              "afmcode": "String_1309",
              "value": "Sorry, this code is invalid"
          },
           {
               "code": "String_1310",
               "afmcode": "String_1310",
               "value": "Sorry, something went wrong. The RSA Code could not be applied successfully. Please refresh the page and try again."
           },
           {
               "code": "String_1311",
               "afmcode": "String_1311",
               "value": "Please enter a RSA ID"
           },
           {
               "code": "String_1312",
               "afmcode": "String_1312",
               "value": "Enter the code provided by our sales associate"
           },
           {
               "code": "String_1313",
               "afmcode": "String_1313",
               "value": "This RSA Code has already been applied"
           },
           {
                "code": "String_1400",
                "afmcode": "String_1400",
                "value": "Back To Payment"
           },
           {
                "code": "String_1401",
                "afmcode": "String_1401",
                "value": "Secure Checkout"
           },
           {
                 "code": "String_1402",
                 "afmcode": "String_1402",
                 "value": "Shopping is always safe and secure."
           },
           {
               "code": "String_1403",
               "afmcode": "String_1403",
               "value": "Summary",
           },
           {
               "code": "String_1404",
               "afmcode": "String_1404",
               "value": "Synchrony"
           },
           {
               "code": "String_1405",
               "afmcode": "String_1405",
               "value": "Based on the payment type selected, Remove or Move to Wishlist options are not available"
           },
            {
                "code": "String_1406",
                "afmcode": "String_1406",
                "value": "Your cart total is below the minimum amount required for the financing option. Please go back to payment page and select another financing option."
            },
           
           {
               "code": "String_1407",
               "afmcode": "String_1407",
               "value": "BILLING"
           },
           {
               "code": "String_1408",
               "afmcode": "String_1408",
               "value": "PAYMENT"
           },
           {
               "code": "String_1409",
               "afmcode": "String_1409",
               "value": "CONFIRM"
           },
           {
               "code": "String_1410",
               "afmcode": "String_1410",
               "value": "Order Summary"
           },
           {
               "code": "String_1411",
               "afmcode": "String_1411",
               "value": "Shipping to : "
           },
           {
               "code": "String_1412",
               "afmcode": "String_1412",
               "value": "Receipt : "
           },
            {
                "code": "String_1413",
                "afmcode": "String_1413",
                "value": "Your Order"
            },
            {
                "code": "String_1414",
                "afmcode": "String_1414",
                "value": "Payment Type"
            },
            {
                "code": "String_1415",
                "afmcode": "String_1415",
                "value": "1"
            },
            {
                "code": "String_1416",
                "afmcode": "String_1416",
                "value": "2"
            },
            {
                "code": "String_1417",
                "afmcode": "String_1417",
                "value": "3"
            },
            {
                "code": "String_1418",
                "afmcode": "String_1418",
                "value": "4"
            },
            {
                "code": "String_1419",
                "afmcode": "String_1419",
                "value": "Use This Address"
            },
           {
               "code": "String_1420",
               "afmcode": "String_1420",
               "value": "SHIPPING"
           },
           {
               "code": "String_1421",
               "afmcode": "String_1421",
               "value": " each"
           },
    ]
}
