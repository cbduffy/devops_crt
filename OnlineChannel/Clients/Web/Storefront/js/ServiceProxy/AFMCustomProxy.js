﻿/*
NS by muthait dated 22Sep2014
*/

var afmCustomServiceRelativeUrl = "/Services/AfmExtendedService.svc/";
var methodName;
var data;

// Gets the shipping options for the entire shopping cart.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function GetZipCode(successCallBack, errorCallBack) {
    methodName = "GetZipCode";

    ajaxProxy(afmCustomServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}

// Gets the shipping options for each item on the shopping cart.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function SetZipCode(zipCode,isCheckoutSession, successCallBack, errorCallBack) {
    methodName = "SetZipCode";
    data = {
        "zipCode": zipCode,
        "isCheckoutSession": isCheckoutSession
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function SendOrderSummary(orderSummary, successCallBack, errorCallBack) {
    methodName = "SendOrderSummary";
    data = {
        "orderSummary": orderSummary
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function GetAffiliationDetailsForZipCode(successCallBack, errorCallBack) {
    methodName = "GetAffiliationDetailsForZipCode";
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}


function AFMGetPriceByAffiliation(successCallBack, errorCallBack) {
    methodName = "AFMGetPriceByAffiliation";
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}

function GetAllPrices(successCallBack, errorCallBack) {
    methodName = "GetAllPrices";
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}

function CreateOrder(tenderLineData, headerValues, email, cardToken, successCallBack, errorCallBack) {
    methodName = "CreateOrder";
    data = {
        "tenderDataLine": tenderLineData,
        "headerValues": headerValues,
        "emailAddress": email,
        "cardToken": cardToken
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function SetShippingtoLineItems(successCallBack, errorCallBack) {
    methodName = "SetShippingtoLineItems";
    data = {
        "shippingOptions": shippingOptions
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}
function GetCardTokenData(tranid, successCallBack, errorCallBack) {
    methodName = "GetCardTokenData";
    data = {
        "tranid": tranid
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function GetUserCartInfo(successCallBack, errorCallBack) {
    methodName = "GetUserCartInfo";
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}

function SetUserCartInfo(userCartData, successCallBack, errorCallBack) {
    methodName = "SetUserCartInfo";
    data = {
        "userCartData": userCartData
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function GetUserPageMode(successCallBack, errorCallBack) {
    methodName = "GetUserPageMode";
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}

function SetUserPageMode(userPageMode, successCallBack, errorCallBack) {
    data = {
        "userPageMode": userPageMode
    };
    methodName = "SetUserPageMode";
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function ClearAllCookies(successCallBack, errorCallBack) {
    methodName = "ClearAllCookies";
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}

function ValidateAddress(address, successCallBack, errorCallBack) {
    methodName = "ValidateAddress";
    data = {
        "address": address
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function GetAFMAddressState(countryRegionId, successCallBack, errorCallBack) {
    methodName = "GetAFMAddressState";
    data = {
        "countryRegionId": countryRegionId
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function ValidateZipcode(countryRegionId, zipCode, successCallBack, errorCallBack) {
    methodName = "ValidateZipcode";
    data = {
        "countryRegionId": countryRegionId,
        "zipCode": zipCode
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function ValidatePromocode(promoCode, successCallBack, errorCallBack) {
    methodName = "ValidatePromocode";
    data = {
        "promoCode": promoCode
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function CheckAtpItemAvailability(itemId, zipCode, Qty, successCallBack, errorCallBack) {
    methodName = "CheckAtpItemAvailability";
    data = {
        "itemId": itemId,
        "zipCode": zipCode,
        "Qty": Qty
    };
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

function GetShippingAddresses(itemId, zipCode, successCallBack, errorCallBack) {
    methodName = "GetShippingAddresses";
    ajaxProxy(afmCustomServiceRelativeUrl, methodName,  null, successCallBack, errorCallBack);
}

function GetCultureId(successCallBack, errorCallBack) {
    methodName = "GetCultureId";
    ajaxProxy(afmCustomServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}