﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

var checkoutServiceRelativeUrl = "/Services/CheckoutService.svc/";
var methodName;
var data;

// Gets the shipping options for the entire shopping cart.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function getShippingOptions(successCallBack, errorCallBack) {
    methodName = "GetShippingOptions";

    ajaxProxy(checkoutServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}

// Gets the shipping options for each item on the shopping cart.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function getShippingOptionsPerItem(successCallBack, errorCallBack) {
    methodName = "GetShippingOptionsPerItem";

    ajaxProxy(checkoutServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}

// Sets the shipping option for the whole shopping cart.
// shippingOptions - The order shipping options.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function setShippingOption(shippingOptions, successCallBack, errorCallBack) {
    methodName = "SetShippingOption";
    data = {
        "shippingOptions": shippingOptions
    };

    ajaxProxy(checkoutServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

// Gets the delivery methods for the order.
// shippingOptions - The order shipping options.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function getDeliveryMethods(shippingOptions, successCallBack, errorCallBack) {
    methodName = "GetDeliveryMethods";
    data = {
        "shippingOptions": shippingOptions
    };

    ajaxProxy(checkoutServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

//  Gets all delivery methods available in current channel.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function getAllDeliveryMethods(successCallBack, errorCallBack) {
    methodName = "GetAllDeliveryMethods";

    ajaxProxy(checkoutServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}

// Gets the delivery methods for the line items.
// shippingOptions - The order shipping options.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function getDeliveryMethodsPerItem(shippingOptions, successCallBack, errorCallBack) {
    methodName = "GetDeliveryMethodsPerItem";
    data = {
        "shippingOptions": shippingOptions
    };

    ajaxProxy(checkoutServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

// Processes a sales order.
// tenderDataLine - Tender lines data containing payments for the order.
// emailAddress - The email address.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function createOrder(tenderDataLine, emailAddress, successCallBack, errorCallBack) {
    methodName = "CreateOrder";
    data = {
        "tenderDataLine": tenderDataLine,
        "emailAddress": emailAddress
    };

    ajaxProxy(checkoutServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

// Sets the shipping options per item.
// shippingOptions - The order shipping options.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function setShippingOptionPerItem(shippingOptions, successCallBack, errorCallBack) {
    methodName = "SetShippingOptionPerItem";
    data = {
        "shippingOptions": shippingOptions
    };

    ajaxProxy(checkoutServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

// Gets the payment card types.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function getPaymentCardTypes(successCallBack, errorCallBack) {
    methodName = "GetPaymentCardTypes";

    ajaxProxy(checkoutServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}

// Gets the payment card.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails..
function getPaymentCard(successCallBack, errorCallBack) {
    methodName = "GetPaymentCard";

    ajaxProxy(checkoutServiceRelativeUrl, methodName, null, successCallBack, errorCallBack);
}