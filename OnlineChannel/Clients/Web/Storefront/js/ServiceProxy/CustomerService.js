﻿
var customerServiceUrl="/Services/CustomerService.svc/";
var methodName;
var data;

// Gets the shipping options for the entire shopping cart.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function UpdateShippingAddressfromAvailableAddresses(addresses,successCallBack, errorCallBack) {
    methodName = "UpdateAddresses";
    data = {
        "addresses": addresses
    };
    ajaxProxy(customerServiceUrl, methodName, null, successCallBack, errorCallBack);
}

function GetAddressCollection(successCallBack, errorCallBack) {
    methodName = "GetAddresses";

    ajaxProxy(customerServiceUrl, methodName, data, successCallBack, errorCallBack);
}

