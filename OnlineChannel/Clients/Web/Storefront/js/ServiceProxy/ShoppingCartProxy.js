﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

var shoppingCartServiceRelativeUrl = "/Services/ShoppingCartService.svc/";
var methodName;
var data;

// Add the given item to the shopping cart.
// listings - The items to be added to cart.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function addItemsToCart(listings, successCallBack, errorCallBack) {
    methodName = "AddItems";
    data = {
        "isCheckoutSession": false,
        "listings": listings,
        "dataLevel": 0
    };

    ajaxProxy(shoppingCartServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

// Gets shopping cart.
// isCheckoutSession - Indicates whether the request originated from the checkout page.
// dataLevel - Represents the data level of the cart entity that is required by the caller.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function getShoppingCart(isCheckoutSession, dataLevel, successCallBack, errorCallBack) {
    methodName = "GetShoppingCart";
    data = {
        "isCheckoutSession": isCheckoutSession,
        "dataLevel": dataLevel
    };

    ajaxProxy(shoppingCartServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

// Removes items from the shopping cart.
// isCheckoutSession - Indicates whether the request originated from the checkout page.
// lineIds - The item line identifiers to be removed from the shopping cart.
// dataLevel - Represents the data level of the cart entity that is required by the caller.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function removeItemsFromCart(isCheckoutSession, lineIds, dataLevel, successCallBack, errorCallBack) {
    methodName = "RemoveItems";
    data = {
        "isCheckoutSession": isCheckoutSession,
        "lineIds": lineIds,
        "dataLevel": dataLevel
    };

    ajaxProxy(shoppingCartServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

// Updates items in the shopping cart.
// isCheckoutSession - Indicates whether the request originated from the checkout page.
// items - The items to be updated.
// dataLevel - Represents the data level of the cart entity that is required by the caller.
// successCallBack - The function that is triggered when ajax call is successful.
// errorCallBack - The function that is triggered when ajax call fails.
function updateItemsInCart(isCheckoutSession, items, dataLevel, successCallBack, errorCallBack) {
    methodName = "UpdateItems";
    data = {
        "isCheckoutSession": isCheckoutSession,
        "items": items,
        "dataLevel": dataLevel
    };

    ajaxProxy(shoppingCartServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}

// Gets promotions
function GetPromotions(isCheckoutSession, dataLevel, successCallBack, errorCallBack) {
    methodName = "GetPromotions";
    data = {
        "isCheckoutSession": isCheckoutSession,
        "dataLevel": dataLevel
    };

    ajaxProxy(shoppingCartServiceRelativeUrl, methodName, data, successCallBack, errorCallBack);
}