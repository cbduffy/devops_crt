﻿/* 
 * NS Developed by muthait for AFM_TFS_40686 dated 06/30/2014 
 */

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    public partial class AshleyWebStore : WebStorefrontPageBase
    {
        
        protected override void Page_Load(object sender, EventArgs e)
        {
            base.Page_Load(sender,e);
            CartCount.Text = numberOfItemsInCart ?? "0";
            CustomerId.Text = !string.IsNullOrWhiteSpace(customerId) ? customerId : "-";
        }


    }
}