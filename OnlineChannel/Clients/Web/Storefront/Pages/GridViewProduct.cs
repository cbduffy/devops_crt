/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages
{
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Representation of a product object as it appears on the grid in product gallery page.
    /// </summary>
    public class GridViewProduct
    {
        /// <summary>
        /// Gets/Sets the ItemId property.
        /// </summary>
        public string ItemId { get; set; }

        /// <summary>
        /// Gets/Sets the ProductNumber property.
        /// </summary>
        public string ProductNumber { get; set; }

        /// <summary>
        /// Gets/Sets the ProductName property.
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets/Sets the Description property.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets the BasePrice property.
        /// </summary>
        public string BasePrice { get; set; }

        /// <summary>
        /// Gets/Sets the AdjustedPrice property.
        /// </summary>
        public string AdjustedPrice { get; set; }

        /// <summary>
        /// Gets/Sets the CompositionInformation property.
        /// </summary>
        public ProductCompositionInformation CompositionInformation { get; set; }

        /// <summary>
        /// Gets/Sets the IsMasterProduct property.
        /// </summary>
        public bool IsMasterProduct { get; set; }
    }
}
