﻿<%--NS Developed by muthait for AFM_TFS_40686 dated 06/26/2014--%>

<%@ page language="C#" masterpagefile="AshleyWebStore.Master" autoeventwireup="true" codebehind="ProductGallery.aspx.cs" inherits="Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages.ProductGalleryPage" %>

<asp:content id="head" contentplaceholderid="head" runat="server">
    <title>Product Gallery - Dynamics Retail Ecommerce Web Storefront</title>
    <script src="../../Common/Scripts/External/jquery-1.7.2.js"></script>
    <script src="../../Common/Scripts/Core.js"></script>
    <script src="../../js/ServiceProxy/ShoppingCartProxy.js"></script>

    <script src="../../js/ServiceProxy/AFMCustomProxy.js"></script>
    <link rel="stylesheet" href="../../css/MasterStyles.css" type="text/css" />
    <link rel="stylesheet" href="../../css/CheckOutStyle.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../css/Controls/ZipCode.css" />
    <link rel="stylesheet" type="text/css" href="../../css/Controls/Loader.css" />
    <link rel="stylesheet" type="text/css" href="../../css/External/jquery-ui-1.10.4.css" />
    <link rel="stylesheet" type="text/css" href="../../css/Controls/Loader.css" /> 

     <script type="text/javascript">
         var dialog;
         var dialogAddItem;
         var loadingDialog;
         var afmZipcode;
         var ItemidValue;
         var ItemQty;
         var LoadingTextBox;
         var listingdataafteratp;
         $("#dialog-form").css('display', 'none');

         $(document).ready(function () {
             LoadingTextBox = $("#LoadingText");
             loadingDialog = $(".loading-overlay").dialog({
                 modal: true,
                 autoOpen: false,
                 draggable: true,
                 resizable: false,
                 position: ['top', 100],
                 show: { effect: "fadeIn", duration: 500 },
                 hide: { effect: "fadeOut", duration: 500 },
                 width: 400,
                 height: 200
             });
             dialog = $("#dialog-form").dialog({
                 autoOpen: false,
                 height: 300,
                 width: 600,
                 modal: true,
                 title: "Add Item",
                 buttons: {
                     Cancel: function () {
                         dialog.dialog("close");
                     }
                 },
                 close: function () {
                     dialog.dialog("close");
                 }
             });
             dialogAddItem = $("#dialog-AfterAdd").dialog({
                 autoOpen: false,
                 height: 300,
                 width: 600,
                 modal: true,
                 title: "Item Added",
                 buttons: {
                     "Continue Shopping": function () {
                         dialogAddItem.dialog("close");
                     },
                     "Proceed to Checkout": function () {
                         dialogAddItem.dialog("close");
                         window.location = "cart.aspx";
                     }
                 },
                 close: function () {
                     dialogAddItem.dialog("close");
                 }
             });
             $('.ui-dialog').addClass('msax-Control');
             $('.ui-button-text').css({ 'font-size': '15pt' });
         });
         function ClickAddItem(itemid, btnid, dropdwonid, textboxid) {
             ItemidValue = itemid;
             $("#lblError").text("");
             var reg = new RegExp("^(0?[0-9]{1,3}|[0-4][0-9][0-9][0-9]|5000)$");
             if ($(dropdwonid).is(':visible'))
                 ItemQty = $(dropdwonid).val()
             if ($(textboxid).is(':visible')) {
                 if (reg.test($(textboxid).val())) {
                     ItemQty = $(textboxid).val();
                 }
                 else {
                     openErrorDialog("Please enter Qty number");
                     return false;
                 }
             }
             //openErrorDialog(ItemQty);
             //return false;
             loadingDialog.dialog("open");
             //loadingDialog.dialog("close");
             LoadingTextBox.text("Checking ZipCode Value");
             GetZipCode(getZipCodeSuccess, getZipCodeFailer);
             return false;
         }
         function getZipCodeSuccess(data) {
             $("#lblError").text("");
             if (data.AFMZipCode != "") {
                 afmZipcode = data.AFMZipCode;
                 //ATP Call
                 //alert("ATP Call Shoud done Get Zip code");
                 LoadingTextBox.text("Checking Atp Item Availability");
                 CheckAtpItemAvailability(ItemidValue, afmZipcode, ItemQty, ATPCallSuccess, ATPCallError);
             }
             else {
                 loadingDialog.dialog("close");
                 $(".msax-UpdateZipCode").css('visibility', 'visible');
                 dialog.dialog("open");
             }
         }

         function getZipCodeFailer(error) {
             openErrorDialog("Some thing went Wrong, Please contact system Administrator.Error : " + Error[0].ErrorMessage)
         }

         function ClickOupdateZipCode() {
             var zipCodeTextBox = $("#zipcodeTextBox");
             loadingDialog.dialog("open");
             LoadingTextBox.text("Validating Zip code");
             ValidateZipcode("USA", zipCodeTextBox.val(), validateZipCodeSuccess, validateZipCodeFail);
         }

         function validateZipCodeSuccess(data) {
             if (data) {
                 var zipCodeTextBox = $("#zipcodeTextBox");
                 loadingDialog.dialog("open");
                 LoadingTextBox.text("Updating ZipCode");
                 SetZipCode(zipCodeTextBox.val(), false, CallSetZipcodeSuccess, CallSetZipcodeFailer);
             }
             else {
                 openErrorDialog("Please Enter Valid zipcode");
                 $(".msax-UpdateZipCode").css('visibility', 'visible');
             }
         }

         function validateZipCodeFail(error) {
             openErrorDialog("Some thing went Wrong, Please contact system Administrator.Error : " + Error[0].ErrorMessage);
         }

         function CallSetZipcodeSuccess(data) {
             if (data.Errors[0] != null) {
                 openErrorDialog("Error : " + data.Errors[0].ErrorMessage)
                 //openErrorDialog("Erro occured while Zipcode");
                 $(".msax-UpdateZipCode").css('visibility', 'visible');
             }
             else {
                 var zipCodeTextBox = $("#zipcodeTextBox");
                 afmZipcode = zipCodeTextBox.val();
                 //alert("ATP Call Shoud done Set Zip code");
                 //ATP Call
                 dialog.dialog("close");
                 LoadingTextBox.text("Checking Atp Item Availability");
                 CheckAtpItemAvailability(ItemidValue, afmZipcode, ItemQty, ATPCallSuccess, ATPCallError);
             }

         }
         function CallSetZipcodeFailer(error) {
             openErrorDialog("Some thing went Wrong, Please contact system Administrator.Error : " + Error[0].ErrorMessage)
         }

         function ATPCallSuccess(data) {
             $("#lblError").text("");
             if (data[0] != null) {
                 if (data[0].Comment == "") {
                     //Add Item call          
                     LoadingTextBox.text("Adding item to Cart");
                     listingdataafteratp = data;
                     addItemsToCart(data, AddItemSuccess, AddItemFail);
                 }
                 else {
                     openErrorDialog(data[0].Comment)
                 }
             }
             else {
                 openErrorDialog("Some thing went Wrong, Please contact system Administrator. while check ATP")
             }
         }

         function ATPCallError(error) {
             if (error.ErrorMessage != undefined)
                 openErrorDialog("Some thing went Wrong, Please contact system Administrator.Error : " + error.ErrorMessage)
             else
                 openErrorDialog("Some thing went Wrong, Please contact system Administrator.Error : " + error.responseText)
         }

         function AddItemSuccess(data) {
             loadingDialog.dialog("close");
             $(".msax-UpdateZipCode").css('visibility', 'hidden');
             $("#divError").css('visibility', 'visible');
             if (data.Errors[0] != null) {
                 //$("#lblError").text("Error : " + data.Errors[0].ErrorMessage);
                 openErrorDialog("Error : " + data.Errors[0].ErrorMessage)
             }
             else {
                 var itemDetailToDisplay = "";
                 if(listingdataafteratp[0].AFMCartLineATP!=null)
                     itemDetailToDisplay = "Item Id: " + listingdataafteratp[0].ItemId + "</br>Qty:" + listingdataafteratp[0].Quantity + "</br>" + listingdataafteratp[0].AFMCartLineATP.Message + "</br>";
                 else
                     itemDetailToDisplay = "Item Id: " + listingdataafteratp[0].ItemId + "</br>Qty:" + listingdataafteratp[0].Quantity + "</br> ATP is bypassed  </br>";
                 //for (i = 0; i < data.ShoppingCart.Items.length; i++) {
                 //    if (data.ShoppingCart.Items[i].AFMKitItemId != "" && data.ShoppingCart.Items[i].AFMKitItemId != null && data.ShoppingCart.Items[i].AFMKitItemId != undefined) {
                 //        if (data.ShoppingCart.Items[i].AFMKitItemId == ItemidValue)
                 //            itemDetailToDisplay = "Item Id: " + data.ShoppingCart.Items[i].AFMKitItemId + "</br>Qty:" + data.ShoppingCart.Items[i].Quantity + "</br>" + listingdataafteratp[0].AFMCartLineATP.ShippingSpan + "</br>";
                 //        //$("#atpDetail").html(data.ShoppingCart.Items[i].ProductDetailsExpanded.Name + "</br>Qty:" + data.ShoppingCart.Items[i].Quantity + "</br>" + data.ShoppingCart.Items[i].AFMCartLineATP.ShippingSpan);
                 //        //$("#atpDetail").text(data.ShoppingCart.Items[i].AFMCartLineATP.ShippingSpan);
                 //    }
                 //    else {
                 //        if (data.ShoppingCart.Items[i].ItemId == ItemidValue) {
                 //            itemDetailToDisplay = data.ShoppingCart.Items[i].ItemId + "</br>Qty:" + data.ShoppingCart.Items[i].Quantity + "</br>" + listingdataafteratp[0].AFMCartLineATP.ShippingSpan;
                 //            //$("#atpDetail").html(data.ShoppingCart.Items[i].ProductDetailsExpanded.Name + "</br>Qty:" + data.ShoppingCart.Items[i].Quantity + "</br>" + data.ShoppingCart.Items[i].AFMCartLineATP.ShippingSpan);
                 //        }

                 //    }
                 //}
                 $("#atpDetail").html(itemDetailToDisplay);
                 $("#CartCount").text("" + data.ShoppingCart.CartCount);
                 dialogAddItem.dialog("open");
                 //$("#lblError").text("Item " + ItemidValue + " Added Successfully.");               

             }
         }

         function AddItemFail(Error) {
             openErrorDialog("Some thing went Wrong, Please contact system Administrator.Error : " + Error[0].ErrorMessage)
         }

         function openErrorDialog(ErrorMgs) {
             loadingDialog.dialog("close");
             $(".msax-UpdateZipCode").css('visibility', 'hidden');
             $("#divError").css('visibility', 'visible');
             $("#lblError").text(ErrorMgs);
             dialog.dialog("open");
         }

         function ClickonContinueeShopping() {
             dialogAddItem.dialog("close");
         }
         function ClickonProceedtoCheckOut() {
             dialogAddItem.dialog("close");
             window.location = "cart.aspx";
         }
    </script>

</asp:content>
<asp:content id="body" contentplaceholderid="WebContentWrapper" runat="server">     
        <div style="width: 100%; margin: 0 auto;">
            <br/>
             <!--<div id="searcharea">-->
                <input type="text" id="txtSearch" runat="server" name="txtSearch" maxlength="250" placeholder="Enter comma separated item ids..."  style="width:380px; height:30px; border:1px solid gray; padding-left:6px" />
                <asp:Button runat="server" Text="Search" OnClick="Search_OnClick" />
            <!--</div>-->
            <br />
            <asp:Label id="errorMessage" runat="server" BackColor="PINK" ForeColor="BLACK" BorderColor="BLACK" BorderWidth="1" Width = "100%" Visible="False"></asp:Label>
            <br />
            <br/>
            <asp:ListView ID="ListView1" Visible="false" runat="server">
                <LayoutTemplate>
                    <table ID="itemPlaceholderContainer" > 
                        <tr>
                            <td>
                                Product Number
                            </td>
                            <td>
                                Description
                            </td>
                            <td>
                                Color
                            </td>
                            <td>
                                Base Price
                            </td>
                            <td>
                                Adjusted Price
                            </td>
                            <td>
                                Add to Cart
                            </td>
                        </tr>
                       <tr runat="server" ID="itemPlaceholder">

                       </tr>
                    </table>

                </LayoutTemplate>
              <%--  <ItemTemplate>
                    <td>
                        <asp:Label runat="server" ID="ProductNumber" Text="<%# Eval("ProductNumber") %>" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="ProductName" Text="<%# Eval("ProductName") %>" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="Description" Text="<%# Eval("Description")%>" />
                    </td>
                    <td>
                        <asp:Label runat="server" ID="Label3" Text="<%# Eval("ProductNumber")%>" />
                    </td>
                </ItemTemplate>--%>
            </asp:ListView>
            
             <asp:GridView ID="ProductsGrid" runat="server" 
                AllowPaging="true" 
                PageSize="30" 
                OnPageIndexChanging="ProductsGrid_PageIndexChanging" 
                OnPageIndexChanged="ProductsGrid_PageIndexChanged"
                AutoGenerateColumns="false" 
                OnRowDataBound="ProductsGrid_RowDataBound">
                 <Columns>
                    <asp:BoundField DataField="ProductNumber" HeaderText="Product Number" />
                    <asp:BoundField DataField="ProductName" HeaderText="Product Name" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:TemplateField HeaderText="Color" Visible="false">
                        <ItemTemplate>
                            <asp:DropDownList ID="ColorDropDown" runat="server" AutoPostBack="true"><asp:ListItem>N/A</asp:ListItem></asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Style" Visible="false">
                        <ItemTemplate>
                            <asp:DropDownList ID="StyleDropDown" runat="server" AutoPostBack="true"><asp:ListItem>N/A</asp:ListItem></asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Size" Visible="false">
                        <ItemTemplate>
                             <asp:DropDownList ID="SizeDropDown" runat="server" AutoPostBack="true"><asp:ListItem>N/A</asp:ListItem></asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Configuration" Visible="false">
                        <ItemTemplate>
                             <asp:DropDownList ID="ConfigurationDropDown" runat="server" AutoPostBack="true"><asp:ListItem>N/A</asp:ListItem></asp:DropDownList>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="BasePrice" HeaderText="Base Price" />
                    <asp:BoundField DataField="AdjustedPrice" HeaderText="Adjusted Price" />
                       <asp:TemplateField HeaderText="Qty" >
                        <ItemTemplate>
                             <asp:DropDownList ID="qtyDropDwon" runat="server" AutoPostBack="false" Visible="false"></asp:DropDownList>
                            <asp:Label runat="server" Text="" id="lbldropCaption" Visible="false"></asp:Label>
                            <asp:TextBox runat="server" ID="txtQty" width="50px" Text="1"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Add to cart">
                        <ItemTemplate>
                             <asp:LinkButton ID="AddItem" runat="server"
                            Text="Add to cart"
                            >
                        </asp:LinkButton>                           
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>                       
        </div>      
        <footer>
            <div class="msax-CartId">
                <span>Shopping Cart Id: </span>
                <asp:Label id="CartId" runat="server" Text="<%# currentMaster.shoppingCartId%>"></asp:Label><br />
            </div>
            <div class="msax-NoOfItems">
                <span>Number of items in the cart: </span>
                <asp:Label id="NoOfItems" runat="server" Text="<%# currentMaster.numberOfItemsInCart %>"></asp:Label><br />
            </div>
            <div class="msax-CustomerId">
                <span>Customer Id: </span>
                <asp:Label id="CustomerId"  runat="server" Text="<%# currentMaster.customerId%>"></asp:Label><br />
            </div>
        </footer>
      <div id="dialog-form">                      
                <div id="popup_content" style="font-size:15pt;">
                    <div id="divError" class="msax-ErrorPanel" style="visibility:hidden;">
                        <label  id="lblError"></label>
                    </div>                    
                    <ul class="msax-UpdateZipCode">
                        <li>ZipCode&nbsp;&nbsp;:&nbsp;&nbsp;</li>
                        <li>
                            <input type="text" id="zipcodeTextBox" class="msax-UpdateZipCodeTextBox" data-bind="validator: { validatorType: 'ZipCodeValidator', validatorField: 'ZipCode', field: 'ZipCodeValue', data: $root }, event: { keypress: $root.preventKeyPress }" />
                            &nbsp;&nbsp;
                        </li>
                        <li>
                            <button class="msax-Delete16 msax-WithText2" onclick="ClickOupdateZipCode();">Update</button>
                        </li>
                    </ul>                    
                </div>
            </div>  
            <div id="dialog-AfterAdd">       
                <div id="popup_content">            
                    <div class="msax-ErrorPanel">
                        <label class="msax-LoadingText msax-MarginTop20" id="atpDetail"></label>
                    </div>                       
                 </div>                
            </div> 
    <div class="msax-Control">
      <div class="loading-overlay">
        <div class="msax-Loader" >
                <div class="msax-Loading">
                    <!--<img class="msax-Spinner msax-MarginTop50">-->
                    <div class="msax-LoadingText msax-MarginTop20"><span id="LoadingText"></span></div>
                </div>
            </div>
      </div>
    </div>
</asp:content>
