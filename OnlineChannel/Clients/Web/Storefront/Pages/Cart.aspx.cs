﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AFM.Commerce.Entities;
using AFM.Commerce.Framework.Extensions.Utils;

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages
{
    public partial class Cart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(AFMDataUtilities.GetCustomerIdFromCookie()))
                ShoppingCart.LoginMode = false;
            else
                ShoppingCart.LoginMode = true;

            AFMDataUtilities.SetUserPageMode(new AFMUserPageMode {PageMode = PageModeOptions.CartView});
        }
    }
}