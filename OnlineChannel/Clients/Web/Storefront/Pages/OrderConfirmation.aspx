﻿<%--NS Developed by muthait for AFM_TFS_40686 dated 06/26/2014--%>

<%@ Page AutoEventWireup="true" MasterPageFile="AshleyWebStore.Master" CodeBehind="OrderConfirmation.aspx.cs" Inherits="Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages.OrderConfirmationPage" Language="C#" %>

<%@ Register Assembly="AFM.Ecommerce.Controls" Namespace="AFM.Ecommerce.Controls" TagPrefix="msax" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <title>Order Confirmation</title>
    <link href="/css/CheckOutStyle.css" rel="stylesheet" type="text/css" />
    <link href="/css/OrderConfirmationStyle.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="body" ContentPlaceHolderID="WebContentWrapper" runat="server">
    <div class="msax-ErrorPanel msax-DisplayNone"></div>
    <div id="msax-OrderConfirmation">
        <section>
            <div id="mainwrapper">
                <div>
                    <div>
                        <div>
                            <h1><b>Thank You!</b></h1>
                            <div>
                                Your order has been successfully processed. See below for your confirmation number(s).<br />
                                Thank you for your business!<br />
                                Your Order Number : <span id="OrderId" runat="server" style="font-weight: bold"></span>
                            </div>
                            <table class="tableinfo" style="border-bottom: 1px solid #ebebeb">
                                <tr>
                                    <td>
                                        <h3><b>Check your email</b></h3>
                                        An email copy of your receipt will be sent<br />
                                        to <a href="mailto:email@domain.com">email@domain.com</a>
                                    </td>
                                    <td>
                                        <h3><b>Delivery</b></h3>
                                        A company representative will call you at the phone<br />
                                        number(s) provided to schedule a date for our delivery.
                                    </td>
                                </tr>
                            </table>
                            <div>
                                <h3><b>Have home delivery questions?</b></h3>
                                Contact Hill Country LLC, (DBA Ashley Furniture<br />
                                HomeStore) at <b>1-800-555-3827</b><br />
                                <br />
                                Mon-Fri: 9AM - 5PM<br />
                                Sat: 10AM - 5PM<br />
                                Sun: 10AM - 5PM 
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <br />
                <msax:shoppingcart id="ShoppingCartView" runat="server"
                    cartdisplaypromotionbanner="False"
                    supportdiscountcodes="True"
                    supportloyaltyreward="False" isreadonly="True" />
                <div class="continueShoppingBtn">
                    <a class="msax-PaddingRight5"
                        href="/Pages/ProductGallery.aspx">Continue Shopping</a>
                </div>

            </div>
            <div id="rightwrapper">
                <msax:ordersummary id="OrderSummary" runat="server" viewdiscount="True" viewtax="True" />
                <msax:orderdetail id="OrderDetailView" runat="server" iseditable="false" />
            </div>
        </section>
        
    <msax:loader runat="server" id="LoaderControl" />
    </div>
</asp:Content>

