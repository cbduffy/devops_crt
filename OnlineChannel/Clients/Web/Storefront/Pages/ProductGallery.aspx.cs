﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

using AFM.Commerce.Framework.Extensions.Utils;
using AFM.Commerce.Services;

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Web.UI.WebControls;

    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using ViewModel = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Framework.Extensions.Controllers;
    using AFM.Commerce.Entities;
    using System.Configuration;
    using System.Diagnostics;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using System.Dynamic;
    /// <summary>
    /// Code-behind for the home page.
    /// </summary>
    public partial class ProductGalleryPage : System.Web.UI.Page
    {
        private static ReadOnlyCollection<Product> products = null;
        private static ReadOnlyCollection<Product> kitproducts = null;
        public AshleyWebStore currentMaster;
        public List<AFMPrdtNAP> prdtNAPList = new List<AFMPrdtNAP>();
        /// <summary>
        /// The load event for the page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            currentMaster = this.Master as AshleyWebStore;
            currentMaster.GetAFMZipCode();
            var channelController = new ChannelController();
            channelController.SetConfiguration(new SiteConfiguration());
            if (!IsPostBack) //Fix for performance issue in Add Item to Cart
            //if (products == null || ProductsGrid.DataSource == null)  //Added for performance issue using the product gallery page.
            {
                BindProducts(true);
            }
            CartId.Text = currentMaster.shoppingCartId ?? "-";
            NoOfItems.Text = currentMaster.numberOfItemsInCart ?? "0";
            CustomerId.Text = !string.IsNullOrWhiteSpace(currentMaster.customerId) ? currentMaster.customerId : "-";
        }

        /// <summary>
        /// The click event for the add to cart link.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void AddToCartLink_OnClick(object sender, EventArgs e)
        {
            errorMessage.Visible = false;
            errorMessage.Text = string.Empty;
            //bool IskitHavingAnyProductPriceZero = false;
            if (products != null)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                LinkButton link = (LinkButton)sender;
                GridViewRow row = (GridViewRow)link.NamingContainer;
                string productNumber = row.Cells[0].Text;
                string notAvailableProduct = null;
                Collection<ViewModel.Listing> listings = null;
                ViewModel.Listing listing = null;
                Product selectedProduct = products.Where(p => p.ProductNumber.Equals(productNumber, StringComparison.InvariantCultureIgnoreCase)).SingleOrDefault();

                //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
                //If current product is Kit, check ATP for kit components. If ATP is not available for any component then do not add item to cart.
                List<Product> productList = new List<Product>();
                AFMShoppingCartController afmShoppingCartController = new AFMShoppingCartController();
                afmShoppingCartController.SetConfiguration(new SiteConfiguration());

                try
                {
                    listing = AFMGetProductListing(selectedProduct, row);
                    listings = new Collection<ViewModel.Listing>() { listing };


                    AFMAtpOutputContainer atpOC = null;

                    string zipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();

                    if (!Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassATP"]))
                    {
                        atpOC = afmShoppingCartController.CheckAtpItemAvailability(currentMaster.customerId, new List<ViewModel.Listing>() { listing }, zipCode);
                    }

                    if (!Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassATP"]))
                    {
                        if (atpOC != null)
                        {
                            if (string.IsNullOrEmpty(atpOC.ErrorMsg))
                            {
                                Services.ShoppingCartService shoppingCartSVC = new Services.ShoppingCartService();
                                ViewModel.ShoppingCart cart = shoppingCartSVC.AddItems(false, listings, ViewModel.ShoppingCartDataLevel.Minimal).ShoppingCart;

                                //ISearchEngine searchEngine = new SearchEngine();
                                //IProductValidator productValidator = new ProductValidator();
                                //ViewModel.ShoppingCart cart = afmShoppingCartController.AddItems(currentMaster.shoppingCartId,
                                //    currentMaster.customerId, listings, ViewModel.ShoppingCartDataLevel.Minimal,
                                //    productValidator, searchEngine, false);
                                ViewModel.SessionType sessionType = AFMDataUtilities.GetSessionType();
                                AFMDataUtilities.SetShoppingCartIdInCookie(sessionType, cart.CartId);


                                currentMaster.shoppingCartId = cart.CartId;
                                currentMaster.numberOfItemsInCart = cart.Items.Count.ToString();
                                Label CartCount = (Label)currentMaster.FindControl("CartCount");
                                CartCount.Text = currentMaster.numberOfItemsInCart;
                                CartId.Text = currentMaster.shoppingCartId;
                                NoOfItems.Text = currentMaster.numberOfItemsInCart;
                            }
                            else
                            {
                                errorMessage.Visible = true;
                                errorMessage.Text = "Currently Product : " + notAvailableProduct + " is not available. " + atpOC.ErrorMsg;
                            }
                        }
                        else
                        {
                            errorMessage.Visible = true;
                            errorMessage.Text = "Currently Product : " + notAvailableProduct + " is not available. ";
                        }
                    }
                    else
                    {
                        // N CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
                        listings = afmShoppingCartController.GetListingsOrderSettings(listings);

                        ISearchEngine searchEngine = new SearchEngine();
                        IProductValidator productValidator = new ProductValidator();
                        ViewModel.ShoppingCart cart = afmShoppingCartController.AddItems(currentMaster.shoppingCartId,
                            currentMaster.customerId, listings, ViewModel.ShoppingCartDataLevel.Minimal,
                            productValidator, searchEngine, false);
                        ViewModel.SessionType sessionType = AFMDataUtilities.GetSessionType();
                        AFMDataUtilities.SetShoppingCartIdInCookie(sessionType, cart.CartId);

                        currentMaster.shoppingCartId = cart.CartId;
                        currentMaster.numberOfItemsInCart = cart.CartCount.ToString();
                        Label CartCount = (Label)currentMaster.FindControl("CartCount");
                        CartCount.Text = currentMaster.numberOfItemsInCart;
                        CartId.Text = currentMaster.shoppingCartId;
                        NoOfItems.Text = currentMaster.numberOfItemsInCart;
                    }
                }
                catch
                {
                    errorMessage.Visible = true;
                    errorMessage.Text = "Something went wrong!! Try again.";
                }
                //NE - RxL
                sw.Stop();
            }
        }


        private ViewModel.Listing AFMGetProductListing(Product product, GridViewRow row)
        {
            ViewModel.Listing listing = null;
            DropDownList list = null;
            string productDetails;

            dynamic dimensionData = new ExpandoObject();
            dynamic productListing = new ExpandoObject();

            if (product.IsMasterProduct)
            {
                ICollection<ProductDimensionSet> dimensionSet = product.CompositionInformation.VariantInformation.Dimensions;
                List<List<long>> lists = new List<List<long>>();
                string aggregatedDimension = string.Empty;
                bool isFirstDimensionToBeAdded = true;

                foreach (ProductDimensionSet dimension in dimensionSet)
                {
                    ProductDimensionValueSet dimentionValueSet = null;

                    switch (dimension.DimensionKey)
                    {
                        case "Color":
                            list = (DropDownList)row.Cells[3].FindControl("ColorDropDown");
                            string color = list.Text;
                            if (!isFirstDimensionToBeAdded)
                            {
                                aggregatedDimension += ",";
                            }
                            aggregatedDimension += String.Format("{0} ", color);
                            isFirstDimensionToBeAdded = false;
                            dimentionValueSet = dimension.DimensionValues.Where(dv => dv.DimensionValue.Equals(color, StringComparison.InvariantCultureIgnoreCase)).Single();
                            break;
                        case "Style":
                            list = (DropDownList)row.Cells[3].FindControl("StyleDropDown");
                            string style = list.Text;
                            if (!isFirstDimensionToBeAdded)
                            {
                                aggregatedDimension += ",";
                            }
                            aggregatedDimension += String.Format("{0} ", style);
                            isFirstDimensionToBeAdded = false;
                            dimentionValueSet = dimension.DimensionValues.Where(dv => dv.DimensionValue.Equals(style, StringComparison.InvariantCultureIgnoreCase)).Single();
                            break;
                        case "Size":
                            list = (DropDownList)row.Cells[3].FindControl("SizeDropDown");
                            string size = list.Text;
                            if (!isFirstDimensionToBeAdded)
                            {
                                aggregatedDimension += ",";
                            }
                            aggregatedDimension += String.Format("{0} ", size);
                            isFirstDimensionToBeAdded = false;
                            dimentionValueSet = dimension.DimensionValues.Where(dv => dv.DimensionValue.Equals(size, StringComparison.InvariantCultureIgnoreCase)).Single();
                            break;
                        case "Configuration":
                            list = (DropDownList)row.Cells[3].FindControl("ConfigurationDropDown");
                            string configuration = list.Text;
                            if (!isFirstDimensionToBeAdded)
                            {
                                aggregatedDimension += ",";
                            }
                            aggregatedDimension += String.Format("{0} ", configuration);
                            isFirstDimensionToBeAdded = false;
                            dimentionValueSet = dimension.DimensionValues.Where(dv => dv.DimensionValue.Equals(configuration, StringComparison.InvariantCultureIgnoreCase)).Single();
                            break;
                    }

                    if (dimentionValueSet != null)
                    {
                        lists.Add(dimentionValueSet.VariantSet.ToList());
                    }
                }

                long variantId;
                if (lists.Count > 0)
                    variantId = lists.Aggregate((l1, l2) => l1.Intersect(l2).ToList()).Single();
                else
                {
                    ProductVariant variant = product.CompositionInformation.VariantInformation.Variants.FirstOrDefault();
                    if (variant.Color != string.Empty)
                        dimensionData.Color = variant.Color;
                    if (variant.Style != string.Empty)
                        dimensionData.Style = variant.Style;
                    if (variant.Size != string.Empty)
                        dimensionData.Size = variant.Size;
                    if (variant.Configuration != string.Empty)
                        dimensionData.Configuration = variant.Configuration;

                    //aggregatedDimension += string.IsNullOrEmpty(variant.Color) ? string.Empty : String.Format("Color: {0} ", variant.Color);
                    //aggregatedDimension += string.IsNullOrEmpty(variant.Style) ? string.Empty : String.Format("Style: {0} ", variant.Style);
                    //aggregatedDimension += string.IsNullOrEmpty(variant.Size) ? string.Empty : String.Format("Size: {0} ", variant.Size);
                    //aggregatedDimension += string.IsNullOrEmpty(variant.Configuration) ? string.Empty : String.Format("Configuration: {0} ", variant.Configuration);


                    variantId = variant.DistinctProductVariantId;

                    dimensionData.Color = "Black";
                    dimensionData.Size = "12 inch";

                    productListing.Name = product.ProductName;
                    productListing.Description = product.Description;
                    productListing.DimensionValues = dimensionData;
                    productListing.ImageUrl = "/images/chair.png";
                    productListing.ImageAlt = "";

                }

                productDetails = Newtonsoft.Json.JsonConvert.SerializeObject(productListing);
                //productDetails = String.Format("{{ \"Name\": \"{0}\", \"Description\": \"{1}\",  \"DimensionValues\": \"{2}\" , \"ImageUrl\":\"/images/chair.png\",\"ImageAlt\":\"\"}}", product.ProductName, product.Description, aggregatedDimension);

                listing = new ViewModel.Listing { ListingId = variantId, Quantity = 1, ItemId = product.ProductNumber, ProductDetails = productDetails };
            }
            else // product is a stand alone product.
            {
                dimensionData.Color = "Black";
                dimensionData.Size = "12 inch";

                productListing.Name = product.ProductName;
                productListing.Description = product.Description;
                productListing.DimensionValues = dimensionData;
                productListing.ImageUrl = "/images/Sofa.png";
                productListing.ImageAlt = "";


                productDetails = Newtonsoft.Json.JsonConvert.SerializeObject(productListing);

                //productDetails = String.Format("{{ \"Name\": \"{0}\", \"Description\": \"{1}\" ,  \"DimensionValues\": \"{2}\" , \"ImageUrl\":\"/images/Sofa.png\",\"ImageAlt\":\"\"}}", product.ProductName, product.Description, dimensionValues);
                //CS Developed by  ysrini for "0230-FDD Catalogue availability based on ATP" dated 8/18/2014 
                listing = new ViewModel.Listing { ListingId = product.RecordId, ItemId = product.ProductNumber, Quantity = 1, ProductDetails = productDetails };
            }

            return listing;
        }

        /// <summary>
        /// Gets the localized name and description for a given product.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <param name="name">The localized name of the product.</param>
        /// <param name="description">The localized description of the product.</param>
        protected void GetLocalizedProductInfo(Product product, out string name, out string description)
        {
            name = string.Empty;
            description = string.Empty;

            if (product != null)
            {
                var translatedProperties = product.ProductProperties.Where(i =>
                    (i.TranslationLanguage.Equals(currentMaster.selectedLanguage, StringComparison.InvariantCultureIgnoreCase))).SingleOrDefault();

                if (translatedProperties != null)
                {
                    name = translatedProperties.TranslatedProperties.Where(i => i.KeyName.Equals("ProductName")).Single().Translation;
                    description = translatedProperties.TranslatedProperties.Where(i => i.KeyName.Equals("Description")).Single().Translation;
                }
                else
                {
                    name = product.ProductName;
                    description = product.Description;
                }
            }
        }

        /// <summary>
        /// The event that updates the grid page index.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void ProductsGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (e != null)
            {
                ProductsGrid.PageIndex = e.NewPageIndex;
            }
        }

        /// <summary> 
        /// The event that is triggered when page index is changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void ProductsGrid_PageIndexChanged(object sender, EventArgs e)
        {
            BindProducts();
        }

        /// <summary>
        /// This event is raised when a data row is bound to data in the GridView control.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void ProductsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e != null && e.Row.RowType == DataControlRowType.DataRow)
            {
                //Product product = (Product)e.Row.DataItem;
                GridViewProduct product = (GridViewProduct)e.Row.DataItem;
                //Label lbl  = (Label)e.Row.FindControl("IsNAPField");
                //bool isNAP = prdtNAPList.First(p => p.ProductId == product.RecordId).IsNAP;
                //lbl.Text = isNAP ? isNAP.ToString() : "false";
                //e.Row.Cells[7].Text = Convert.ToString((bool)product["ISNAP"]);
                if (product.IsMasterProduct)
                {
                    ICollection<ProductDimensionSet> dimensionSet = product.CompositionInformation.VariantInformation.Dimensions;

                    foreach (ProductDimensionSet dimension in dimensionSet)
                    {
                        DropDownList list = null;

                        switch (dimension.DimensionKey)
                        {
                            case "Color": list = (DropDownList)e.Row.FindControl("ColorDropDown");
                                ProductsGrid.Columns[3].Visible = true;
                                break;
                            case "Style": list = (DropDownList)e.Row.FindControl("StyleDropDown");
                                ProductsGrid.Columns[4].Visible = true;
                                break;
                            case "Size": list = (DropDownList)e.Row.FindControl("SizeDropDown");
                                ProductsGrid.Columns[5].Visible = true;
                                break;
                            case "Configuration": list = (DropDownList)e.Row.FindControl("ConfigurationDropDown");
                                ProductsGrid.Columns[6].Visible = true;
                                break;
                        }

                        if (list != null)
                        {
                            list.DataSource = dimension.DimensionValues;
                            list.DataTextField = "DimensionValue";
                            list.DataBind();
                        }
                    }
                }
                //Add Qty Logic
                AFMShoppingCartController controller = new AFMShoppingCartController();
                var listItem = new List<string>();
                listItem.Add(product.ItemId);
                List<AFMProductOrderSettings> orderSettings = controller.GetOrderSettingsForItemIds(listItem);
                var itemSetting = orderSettings.FirstOrDefault(temp => temp.ItemId == product.ItemId);
                DropDownList listQty = (DropDownList)e.Row.FindControl("qtyDropDwon");//
                TextBox txtQty = (TextBox)e.Row.FindControl("txtQty");
                Label listQtyLable = (Label)e.Row.FindControl("lbldropCaption");
                var qtydropdownsouce = new List<decimal>();
                var QuantityPerBoxUpperBound = Convert.ToInt32(ConfigurationManager.AppSettings["QuantityPerBoxUpperBound"]);
                if (itemSetting != null)
                {

                    if ((itemSetting.LowestQty > 1 || itemSetting.MultipleQty > 1))
                    {
                        decimal m = 0;
                        for (int i = 1; m < QuantityPerBoxUpperBound; i++)
                        {
                            if (itemSetting.LowestQty != 0)
                            {
                                if ((itemSetting.MultipleQty * i) >= itemSetting.LowestQty)
                                {
                                    m = (itemSetting.MultipleQty * i);
                                    if (m < QuantityPerBoxUpperBound)
                                        listQty.Items.Add(new ListItem() { Text = m.ToString(), Value = m.ToString() });
                                }
                            }
                            else
                            {
                                m = (itemSetting.MultipleQty * i);
                                if (m < QuantityPerBoxUpperBound)
                                    listQty.Items.Add(new ListItem() { Text = m.ToString(), Value = m.ToString() });
                            }
                        }
                        listQty.Visible = true;
                        if (itemSetting.MultipleQty < 1 && itemSetting.QtyPerBox > 1)
                            listQtyLable.Text = "Set of " + itemSetting.MultipleQty;
                        listQtyLable.Visible = true;
                        txtQty.Visible = false;
                    }
                }
                //End Add Qty Logic
                LinkButton btnLink = (LinkButton)e.Row.FindControl("AddItem");
                btnLink.OnClientClick = "return ClickAddItem('" + product.ProductNumber + "','#" +
                    btnLink.ClientID + "','#" + listQty.ClientID + "','#" + txtQty.ClientID + "')";
            }
        }

        /// <summary>
        /// The function that binds the product details data to the grid on the page.
        /// </summary>
        private void BindProducts(bool searchById = false)
        {
            try
            {
                List<string> itemIds = null;
                if (!string.IsNullOrEmpty(txtSearch.Value) && searchById)
                {
                    itemIds = txtSearch.Value.Split(',').Select(s => s.Trim()).ToList();


                    products = AFMProductsController.GetProductsByItemIds(itemIds);
                }
                else if (searchById)
                {
                    itemIds = new List<string>();
                    itemIds.Add("A1000030T");
                    itemIds.Add("A1000029T");
                    products = AFMProductsController.GetProductsByItemIds(itemIds);
                }
                else
                {
                    products = AFMProductsController.GetProducts();
                }


                Collection<GridViewProduct> gridViewProducts = new Collection<GridViewProduct>();

                foreach (var p in products)
                {
                    string productName;
                    string productDescription;
                    decimal basePrice = p.BasePrice;
                    decimal adjustedPrice = p.AdjustedPrice;

                    GetLocalizedProductInfo(p, out productName, out productDescription);

                    GridViewProduct gridViewProduct = new GridViewProduct();

                    gridViewProduct.ProductNumber = p.ProductNumber;
                    gridViewProduct.ProductName = productName;
                    gridViewProduct.Description = productDescription;
                    gridViewProduct.BasePrice = Utilities.ToCurrencyString(basePrice, currentMaster.selectedLanguage);
                    gridViewProduct.AdjustedPrice = Utilities.ToCurrencyString(adjustedPrice, currentMaster.selectedLanguage);
                    gridViewProduct.IsMasterProduct = p.IsMasterProduct;
                    gridViewProduct.CompositionInformation = p.CompositionInformation;
                    gridViewProduct.ItemId = p.ItemId;

                    gridViewProducts.Add(gridViewProduct);
                }

                // Bind the product details to the grid view.
                ProductsGrid.DataSource = gridViewProducts;
                ProductsGrid.DataBind();
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        protected void Search_OnClick(object sender, EventArgs e)
        {
            errorMessage.Text = string.Empty;
            BindProducts(true);
        }
    }
}