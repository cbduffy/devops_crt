﻿<%--NS Developed by muthait for AFM_TFS_40686 dated 06/26/2014--%>

<%@ Page Language="C#" MasterPageFile="AshleyWebStore.Master"  AutoEventWireup="true" CodeBehind="SignIn.aspx.cs" Inherits="Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages.SignInPage" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <title>Sign In - Dynamics Retail eCommerce Web Storefront</title>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="WebContentWrapper" runat="server">
        <div style="width: 400px; margin-left:auto; margin-top:15%; margin-right:auto">
            <div class="msax-CustomerInfo">
                <label>Customer ID:  </label>
                <input runat="server" type="text" id="CustomerId" placeholder="Please enter AX Customer ID" style="width:300px; float:right;" />
            </div>
            <div class="msax-OkCancel" style="margin-top:30px; text-align:center">
                <a class="msax-CancelLink" href="/Pages/ProductGallery.aspx">Cancel</a>
                <a class="msax-SignInLink" href="#" runat="server" onserverclick="SignInClick">Sign In</a>
            </div>
        </div>
</asp:Content>

