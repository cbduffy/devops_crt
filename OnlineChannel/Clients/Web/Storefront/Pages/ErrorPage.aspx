﻿<%@ Page Title="" Language="C#" MasterPageFile="AshleyWebStore.Master" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages.ErrorPage" %>

<asp:Content ID="head" ContentPlaceHolderID="head" runat="server">
    <title>Error</title>
</asp:Content>

<asp:Content ID="body" ContentPlaceHolderID="WebContentWrapper" runat="server">
<div>
   <h3>Oops! Something went wrong! We are sorry about that! Please try again. <a href="/Pages/Cart.aspx" >Click to Proceed</a></h3> 
</div>
</asp:Content>