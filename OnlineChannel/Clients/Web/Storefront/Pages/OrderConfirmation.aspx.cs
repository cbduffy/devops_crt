﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

using AFM.Commerce.Framework.Extensions.Utils;

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages
{
    using AFM.Commerce.Entities;
    using System;

    public partial class OrderConfirmationPage : System.Web.UI.Page
    {
        /// <summary>
        /// The load event for the page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["confirmationId"]))
            {
              string confirmationId = Request.QueryString["confirmationId"];
              AFMDataUtilities.SetOrderIdInCookie(confirmationId);
            }
            AFMDataUtilities.SetUserPageMode(new AFMUserPageMode { PageMode = PageModeOptions.ThankYou });
            var orderId = AFMDataUtilities.GetOrderrIdFromCookie();
            if (!string.IsNullOrEmpty(orderId) && orderId != "null")
            OrderId.InnerText = AFMDataUtilities.GetOrderrIdFromCookie().Insert(4, " ");
            else
                Response.Redirect("/Pages/ErrorPage.aspx");
        }
    }
}