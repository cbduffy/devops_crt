﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AFM.Commerce.Entities;
using AFM.Commerce.Framework.Extensions.Utils;

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages
{
    public partial class CheckOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(AFMDataUtilities.GetCustomerIdFromCookie()))
                ShoppingCartView.LoginMode = false;
            else
                ShoppingCartView.LoginMode = true;
            //CreateAddressControl.PaymentGatewayUrl = ConfigurationManager.AppSettings["PaymentGatewayUrl"].ToString();

            if (!string.IsNullOrEmpty(Request.QueryString["tranid"]))
            {  
                var transactionId = Request.QueryString["tranid"];
                if (!string.IsNullOrEmpty(Request.QueryString["step"]) && (PageModeOptions)Enum.Parse(typeof(PageModeOptions), Request.QueryString["step"].ToString()) == PageModeOptions.ShippingViewCompleted)
                {
                    ComparetoProceed(null, new AFMUserPageMode { PageMode = PageModeOptions.ShippingViewCompleted });
                }
                else if (string.IsNullOrEmpty(Request.QueryString["step"]))
                {
                    ComparetoProceed(transactionId, new AFMUserPageMode { PageMode = PageModeOptions.ConfirmView });
                }
                else
                    ComparetoProceed(transactionId, new AFMUserPageMode { PageMode = (PageModeOptions)Enum.Parse(typeof(PageModeOptions), Request.QueryString["step"].ToString()) });
            }

        }
        private void ComparetoProceed(string transactionId, AFMUserPageMode userPageMode)
        {
            try
            {
                var HashCheck = SecurityManager.CompareHash(transactionId);
                if (HashCheck)
                {
                    if (transactionId != null)
                        ShoppingCartView.TransactionId = transactionId;
                    AFMDataUtilities.SetUserPageMode(userPageMode);
                }
                else
                    Response.Redirect("/Pages/ErrorPage.aspx");
            }
            catch (Exception ex)
            {               
                Response.Redirect("/Pages/ErrorPage.aspx");
            }
           
        }



    }
}