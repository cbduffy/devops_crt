﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

using AFM.Commerce.Framework.Extensions.Utils;

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages
{
    using System;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using AFM.Commerce.Framework.Extensions.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    public partial class SignInPage : System.Web.UI.Page
    { 
        /// <summary>
        /// The event that gets triggered when sign in is clicked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void SignInClick(object sender, EventArgs e)
        {
            string customerId = CustomerId.Value;
            CustomerController customerController = new CustomerController();
            Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Customer customer = customerController.GetCustomer(customerId);
            var controller = new AFMShoppingCartController();
            if (customer == null)
            {
                CustomerId.Value = "Invalid Customer Id";
            }
            else
            {

                var guestCart = AFMDataUtilities.GetShoppingCartId();
                AFMDataUtilities.SetCustomerIdInCookie(customer.AccountNumber);
                ISearchEngine searchEngine = new SearchEngine();
                IProductValidator productValidator = new ProductValidator();
                var cart = controller.GetActiveShoppingCart(customer.AccountNumber, ShoppingCartDataLevel.All, productValidator, searchEngine);
                if (cart != null)
                {
                    if (!string.IsNullOrEmpty(guestCart))
                        cart = controller.MoveItemsBetweenCarts(guestCart, cart.CartId, customer.AccountNumber, ShoppingCartDataLevel.All, productValidator, searchEngine);
                    SessionType sessionType = AFMDataUtilities.GetSessionType();
                    AFMDataUtilities.SetShoppingCartIdInCookie(sessionType, cart.CartId);
                }
                else
                {
                    CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                    OrderManager manager = OrderManager.Create(runtime);
                    if (!string.IsNullOrEmpty(guestCart))
                    {
                        cart = controller.MoveItemsBetweenCarts(guestCart, null, customer.AccountNumber, ShoppingCartDataLevel.All, productValidator, searchEngine);
                        if (cart != null)
                        {
                            SessionType sessionType = AFMDataUtilities.GetSessionType();
                            AFMDataUtilities.SetShoppingCartIdInCookie(sessionType, cart.CartId);
                        }
                    }

                }
               if (customer.PrimaryAddress!=null)
                AFMDataUtilities.SetAFMZipCodeInCookie(customer.PrimaryAddress.ZipCode);
                Response.Redirect("/Pages/ProductGallery.aspx", true);
            }
        }
    }
}