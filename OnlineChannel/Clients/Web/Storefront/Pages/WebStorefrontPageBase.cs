﻿/* 
 * CS Developed by muthait for AFM_TFS_40686 dated 06/30/2014 - Mapped to MAster Page and remvoe implementations to all page level functions & remapped to masterpage level methods
 * Cart Details implementation to Master Page level from Page level
 */

using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.Threading;

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages
{
    using System;

    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Linq;
    using System.IO;
    using System.Reflection;
    using Util = AFM.Commerce.Framework.Extensions.Utils;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Framework.Extensions.Controllers;
    using AFM.Commerce.Entities;
    using System.Web.UI;
    using AFM.Commerce.Framework.Extensions.Utils;
    public partial class WebStorefrontPageBase : System.Web.UI.MasterPage
    {
        /// <summary>
        /// Indicates whether the sign out link is visible on a page.
        /// </summary>
        public bool allowSignOut;

        /// <summary>
        /// Customer identifier.
        /// </summary>
        public string customerId;

        /// <summary>
        /// AFMZipCode identifier.
        /// </summary>
        public string AFMZipCode;

        /// <summary>
        /// Shopping cart identifier.
        /// </summary>
        public string shoppingCartId;

        /// <summary>
        /// Number of items in shopping cart.
        /// </summary>
        public string numberOfItemsInCart;

        /// <summary>
        /// Gift Card Id
        /// </summary>
        public string giftCardItemId;

        /// <summary>
        /// Selected Language
        /// </summary>
        public string selectedLanguage;

        //CS by muthait
        /// <summary>
        /// The load event for the page.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected virtual void Page_Load(object sender, EventArgs e)
        {
            this.SetSignOutVisibility();
            this.GetAFMZipCode();
            this.GetCartInfo();
            ReadOnlyCollection<ChannelLanguage> culture = CrtUtilities.GetChannelLanguages();
            SetCulture(culture[0].LanguageId);
            selectedLanguage = culture[0].LanguageId;
        }

        /// <summary>
        /// Gets the cart information.
        /// </summary>
        public void GetAFMZipCode()
        {
            AFMZipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
            if (String.IsNullOrEmpty(AFMZipCode))
            {
                var XMLLoadfullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("Common\\DemoData", "CheckoutData.xml"));
                var DemoDataZipCode = XElement.Load(XMLLoadfullPath).Element("Address").Element("Zipcode");
                AFMZipCode = DemoDataZipCode.Value;
                //if(AFMZipCode == null)
                //    AFMZipCode = ConfigurationManager.AppSettings["DefaultZipCode"];
                //AFMDataUtilities.SetAFMZipCodeInCookie(AFMZipCode);
            }
        }

        // CE by muthait

        /// <summary>
        /// Sets the Culture based on the language.
        /// </summary>
        /// <param name="language">The language as string.</param>
        protected void SetCulture(string language)
        {
            Page.UICulture = language;
            Page.Culture = language;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(language);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);

            AFMDataUtilities.SetCultureIdInCookie(language);
        }

        /// <summary>
        /// Gets the cart information.
        /// </summary>
        protected void GetCartInfo()
        {
            var channelController = new ChannelController();
            channelController.SetConfiguration(new SiteConfiguration());
            var configData = channelController.GetChannelConfiguration();
            if (configData != null)
                giftCardItemId = configData.GiftCardItemId;

            SessionType sessionType = AFMDataUtilities.GetSessionType();
            shoppingCartId = AFMDataUtilities.GetShoppingCartIdFromCookie(sessionType);
            //List<long> tiers;
            if (!String.IsNullOrWhiteSpace(shoppingCartId))
            {
                ISearchEngine searchEngine = new SearchEngine();
                IProductValidator productValidator = new ProductValidator();
                AFMShoppingCartController afmShoppingCartController = new AFMShoppingCartController();
                ShoppingCart cart = afmShoppingCartController.GetShoppingCart(shoppingCartId, customerId, ShoppingCartDataLevel.Minimal, productValidator, searchEngine, false);

                if (cart != null)
                {
                    ////Hitesh - Start
                    //string zipCode = Util.AFMDataUtilities.GetAFMZipCodeFromCookie();

                    //tiers = new List<long>();
                   
                    //AFMVendorAffiliation afmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(zipCode);
                    //if (afmVendorAffiliation != null && afmVendorAffiliation.AffiliationId > 0)
                    //{
                    //    tiers.Add(afmVendorAffiliation.AffiliationId);
                    //}
                    //else
                    //{
                    //    Type cstype = this.GetType();
                    //    String csname1 = "PopupScript";

                    //    // Get a ClientScriptManager reference from the Page class.
                    //    ClientScriptManager cs = Page.ClientScript;

                    //    // Check to see if the startup script is already registered.
                    //    if (!cs.IsStartupScriptRegistered(cstype, csname1))
                    //    {
                    //        String cstext1 = "alert('The zip code entered cannot be serviced');";
                    //        cs.RegisterStartupScript(cstype, csname1, cstext1, true);
                    //    }

                    //}
                    ////Hitesh - End
                    //// HXM - NC - To improve submit order performance - Remove Cart Affiliation call
                    //if(AFMDataUtilities.GetUserPageMode()!=null && AFMDataUtilities.GetUserPageMode().PageMode == PageModeOptions.CartView)
                    //    afmShoppingCartController.SetCartAffiliationLoyaltyTiers(cart.CartId, tiers);
                    numberOfItemsInCart = cart.CartCount.ToString();
                }
            }
        }

        /// <summary>
        /// Sets the boolean value which indicates the visibility of sign out and sign in.
        /// </summary>
        protected void SetSignOutVisibility()
        {
            // Get the customer id from cookie.
            customerId = AFMDataUtilities.GetCustomerIdFromCookie();
            //NE Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 
            //if (string.IsNullOrEmpty(customerId))
            //{
            //    customerId = ConfigurationManager.AppSettings["DefaultCustomerId"];
            //}
            //NS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 
            allowSignOut = !string.IsNullOrWhiteSpace(customerId);        }

        /// <summary>
        /// The event that gets triggered when sign out is clicked.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event arguments.</param>
        protected void SignOutClick(object sender, EventArgs e)
        {
            AFMDataUtilities.ClearCustomerIdFromCookie();
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "InlineScript", "<script type=text/javascript>msaxValues.msax_LoginMode = false; </script>");
            //CS by muthait
            AFMDataUtilities.ClearAFMZipCodeFromCookie();
            AFMDataUtilities.ClearUserCartInfoFromCookie();
            AFMDataUtilities.ClearFinanceOptionIdFromCookie();
            //CE by muthait
            AFMDataUtilities.ClearCartIdResponseCookies(SessionType.All);
            Response.Redirect("/Pages/ProductGallery.aspx", true);
        }
    }
}