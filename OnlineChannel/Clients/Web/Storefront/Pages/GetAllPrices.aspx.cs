﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.ObjectModel;
using AFM.Commerce.Entities;
using AFM.Commerce.Framework.Extensions.Controllers;

namespace Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages
{
    public partial class GetAllPrices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //const string channelId = "5637144608";
            List<string> productIdList = new List<string> { "1010135", "1080019" };
            AFMPricingController afmPricingController = new AFMPricingController();
            ReadOnlyCollection<AFMAllAffiliationPrices> data = afmPricingController.GetAllPrices(productIdList);
            grvAllAffPrices.DataSource = data;
            grvAllAffPrices.DataBind();
        }
    }
}