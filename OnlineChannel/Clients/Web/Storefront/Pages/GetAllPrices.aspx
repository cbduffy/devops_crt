﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetAllPrices.aspx.cs" Inherits="Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages.GetAllPrices" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>GetAllPrices</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView runat="server" ID="grvAllAffPrices">
        </asp:GridView>
    </div>
    </form>
</body>
</html>
