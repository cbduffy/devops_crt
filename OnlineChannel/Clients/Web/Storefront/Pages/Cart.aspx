﻿<%@ Page Title="" Language="C#" MasterPageFile="AshleyWebStore.Master" AutoEventWireup="true" CodeBehind="Cart.aspx.cs" Inherits="Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages.Cart" %>

<%@ Register Assembly="AFM.Ecommerce.Controls" Namespace="AFM.Ecommerce.Controls" TagPrefix="msax" %>

<asp:content id="head" contentplaceholderid="head" runat="server">
    <title>Shopping Cart - Dynamics Retail Ecommerce Web Storefront</title>
    <link href="/css/CheckOutStyle.css" rel="stylesheet" type="text/css"/>
</asp:content>

<asp:content id="body" contentplaceholderid="WebContentWrapper" runat="server">
        <div class="msax-ErrorPanel msax-DisplayNone"></div>
        <div id="msax-ShoppingCartControl">
                    <div>
                        <div class="msax-FloatLeft">
                            <h2>Shopping Cart</h2> <!-- Shopping cart -->
                        </div>
                    </div>
            <msax:ButtonControls runat="server" ID="CartButton1"
                CheckoutUrl = "/Pages/Checkout.aspx#shipping" 
                ContinueShoppingUrl = "/Pages/ProductGallery.aspx" 
                />
            
            <msax:ZipCode runat="server" id="ZipcodeControl" /> 
            <msax:ShoppingCart ID="ShoppingCart" runat="server"
                        CartDisplayPromotionBanner="False"
                            SupportDiscountCodes = "False"
                            SupportLoyaltyReward = "False" SignInURL="SignIn.aspx"/>
            <table class="msax-ShoppingCartSummaryContent">
                <tr>
                    <td>
                         <msax:RsaId runat="server" ID="RsaIdControl"/>  
                    </td>
                     <td rowspan="3" align="right" style="vertical-align:top;" >
                        <msax:OrderSummary ID="OrderSummary" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td>                            
                         <msax:PromotionCode runat="server" ID="PromotionsControl"/>  
                    </td>                   
                 </tr>
                <tr>
                    <td>
                        <div style="font-weight:bold;">
                            Need Help? Call Customer Service
                        </div>
                        <div style="color:#0066CC">
                            1(800) 555-2736 (9AM - 5PM EST)
                        </div>
                    </td>
                </tr>
            </table>
            
            <msax:ButtonControls runat="server" ID="CartButton2"
                CheckoutUrl = "/Pages/Checkout.aspx#shipping" 
                ContinueShoppingUrl = "/Pages/ProductGallery.aspx" />
            
            <msax:Loader runat="server" ID="LoaderControl"/>
        </div>
</asp:content>
