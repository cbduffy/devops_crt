﻿<%@ page title="" language="C#" masterpagefile="AshleyWebStore.Master" autoeventwireup="true" codebehind="CheckOut.aspx.cs" inherits="Microsoft.Dynamics.Retail.Ecommerce.Web.Storefront.Pages.CheckOut" %>

<%@ register assembly="AFM.Ecommerce.Controls" namespace="AFM.Ecommerce.Controls" tagprefix="msax" %>

<asp:content id="head" contentplaceholderid="head" runat="server">
    <title>CheckOut</title>
</asp:content>

<asp:content id="body" contentplaceholderid="WebContentWrapper" runat="server">
          
        
        <div id="msax-CheckOutControl">
            <div>
                <msax:CartMode ID="CartMode" runat="server" GoBackUrl="Cart.aspx"  /> 
                 <div class="msax-ErrorPanel msax-DisplayNone"></div> 
            </div>
            <section>
                <div id="mainwrapper">
                    <msax:OrderDetail ID="OrderDetailEdit" runat="server" IsEditable="True" />
                    <msax:ShoppingCart ID="ShoppingCartView" runat="server"
                        CartDisplayPromotionBanner="False"
                    supportdiscountcodes="True"
                    SupportLoyaltyReward = "False" ErrorPageUrl="/Pages/ErrorPage.aspx"/>
                     <!-- NS HXM CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey -->
                        <div id="TermsAncConditionBlock" style="display:none" class="msax-Control" style="float:right">
                            <div class="msax-FieldPanel">
                                <input id="IsTermsAndConditionsCheckBox" type="checkbox" style="display: table-cell" class="msax-FieldPanel" />
                                <label for="IsTermsAndConditionsCheckBox" class="msax-useshippingaddresslabel msax-PaddingLeft10" style="display: table-cell">I hereby accept Terms and Conditions</label>
                            </div>
                        </div>
                        <!-- NE HXM CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey -->
                     <msax:ButtonControls runat="server" ID="SubmitOrderButtons" orderConfirmationUrl="/Pages/OrderConfirmation.aspx" ContinueShoppingUrl="/Pages/ProductGallery.aspx"/>
                    <msax:Address runat="server" ID="CreateAddressControl"/>
                </div>
                <div id="rightwrapper">
                    <msax:OrderSummary ID="OrderSummary" runat="server" ViewTax="True" ViewDiscount="False"/>
                </div>
                <msax:Loader runat="server" ID="LoaderControl"/>
            </section>
        </div>
</asp:content>

