/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Service response class.
    /// </summary>
    [DataContract]
    public class ServiceResponse
    {
        /// <summary>
        /// Creates a new instance of the <see cref="ServiceResponse"/> class.
        /// </summary>
        protected ServiceResponse()
        {
            this.Errors = new List<ResponseError>();
        }

        /// <summary>
        /// Gets the has error value.
        /// </summary>
        public bool HasErrors { get { return this.Errors.Any(); } }

        /// <summary>
        /// Gets the has error redirect url.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "value"), DataMember]
        public string RedirectUrl
        {
            get
            {
                if (this.HasErrors)
                {
                    ResponseError firstErrorWithRedirect = this.Errors.FirstOrDefault(item => !string.IsNullOrWhiteSpace(item.RedirectUrl));

                    if (firstErrorWithRedirect != null)
                    {
                        return firstErrorWithRedirect.RedirectUrl;
                    }
                }

                return string.Empty;
            }

            set { 
                // serialization placeholder.
            }
        }

        /// <summary>
        /// Gets the collection of the <see cref="ResponseError"/>.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification = "Need to have ability to add range.")]
        [DataMember]
        public List<ResponseError> Errors { get; private set; }

        /// <summary>
        /// Gets or sets can retry value.
        /// </summary>
        [DataMember]
        public bool CanRetry { get; set; }

        /// <summary>
        /// Creates and instance of the <see cref="ResponseError"/> and adds to Errors collection.
        /// </summary>
        /// <param name="errorCode">Error code value.</param>
        /// <param name="errorMessage">Error message value.</param>
        /// <param name="args">Error arguments.</param>
        public void AddError(string errorCode, string errorMessage, params object[] args)
        {
            this.Errors.Add(new ResponseError(errorCode, string.Format(CultureInfo.CurrentCulture, errorMessage, args)));
        }
    }

    /// <summary>
    /// Response error class.
    /// </summary>
    [DataContract]
    public sealed class ResponseError
    {
        /// <summary>
        /// Creates the instance of the <see cref="ResponseError"/>
        /// </summary>
        public ResponseError()
        {
        }

        /// <summary>
        /// Creates the instance of the <see cref="ResponseError"/>
        /// </summary>
        /// <param name="errorMessage">Error message value.</param>
        public ResponseError(string errorMessage)
            : this(null, errorMessage)
        {
        }

        /// <summary>
        /// Creates the instance of the <see cref="ResponseError"/>
        /// </summary>
        /// <param name="errorCode">Error code value.</param>
        /// <param name="errorMessage">Error message value.</param>
        public ResponseError(string errorCode, string errorMessage)
        {
            this.ErrorMessage = errorMessage;
            this.ErrorCode = errorCode;
        }

        /// <summary>
        /// Gets or sets the value of the error code.
        /// </summary>
        [DataMember]
        public string ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets the value of the error message.
        /// </summary>
        [DataMember]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the value of the redirect url.
        /// </summary>
        public string RedirectUrl { get; set; }

        /// <summary>
        /// Gets or sets the value to indicate to client whether to show additional technical detail in error.
        /// </summary>
        [DataMember]
        public bool ShowDebugInfo { get; set; }

        /// <summary>
        /// Gets or sets the value of the error message as provided by the back-end.
        /// </summary>
        [DataMember]
        public string ExtendedErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the value of the source of the error.
        /// </summary>
        [DataMember]
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the value of the stack trace of the error.
        /// </summary>
        [DataMember]
        public string StackTrace { get; set; }
    }
}