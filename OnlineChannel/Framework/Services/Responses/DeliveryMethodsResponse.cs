/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Delivery options response class.
    /// </summary>
    [DataContract]
    public class DeliveryOptionsResponse : ServiceResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeliveryOptionsResponse"/> object.
        /// </summary>
        public DeliveryOptionsResponse()
            : this(null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliveryOptionsResponse"/> object.
        /// </summary>
        /// <param name="deliveryOptions">The read only collection of delivery options for the entire order.</param>
        public DeliveryOptionsResponse(Collection<DeliveryOption> deliveryOptions)
            : this(deliveryOptions, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliveryOptionsResponse"/> object.
        /// </summary>
        /// <param name="lineDeliveryOptions">The read only collection of delivery options per line.</param>
        public DeliveryOptionsResponse(Collection<LineDeliveryOption> lineDeliveryOptions)
            : this(null, lineDeliveryOptions)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeliveryOptionsResponse"/> object.
        /// </summary>
        /// <param name="deliveryOptions">The read-only collection of delivery options applicable to entire order.</param>
        /// <param name="lineDeliveryOptions">The read-only collection of delivery options per line.</param>
        public DeliveryOptionsResponse(Collection<DeliveryOption> deliveryOptions, Collection<LineDeliveryOption> lineDeliveryOptions)
        {
            this.DeliveryOptions = deliveryOptions;
            this.LineDeliveryOptions = lineDeliveryOptions;
        }

        /// <summary>
        /// Method checks if passed collection is null. If not creates new one to ensure backward compatability.
        /// </summary>
        /// <param name="deliveryMethods">Delivery methods collection.</param>
        public void SetDeliveryOptions(Collection<DeliveryOption> deliveryMethods)
        {
            this.DeliveryOptions = deliveryMethods ?? new Collection<DeliveryOption>();
            
        }

        /// <summary>
        /// Method checks if passed collection is null. If not creates new one to ensure backward compatability.
        /// </summary>
        /// <param name="itemDeliveryMethods">Item delivery methods collection.</param>
        public void SetLineDeliveryOptions(Collection<LineDeliveryOption> itemDeliveryMethods)
        {
            this.LineDeliveryOptions = itemDeliveryMethods ?? new Collection<LineDeliveryOption>();
        }

        /// <summary>
        /// Gets the header level delivery options.
        /// </summary>
        [DataMember]
        public Collection<DeliveryOption> DeliveryOptions { get; private set; }

        /// <summary>
        /// Gets the delivery options per line.
        /// </summary>
        [DataMember]
        public Collection<LineDeliveryOption> LineDeliveryOptions { get; private set; }

        //TO DO spriya merge for upgrade
        /*NS Developed by  spriya for AFM_TFS_46529 dated 6/19/2014 */
        /// <summary>
        /// Gets the AddressVerification validation result
        /// </summary>
        [DataMember]
        public bool AddressVerficationResult { get; set; }
    }
}