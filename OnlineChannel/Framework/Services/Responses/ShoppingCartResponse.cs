/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Shopping cart response class.
    /// </summary>
    [DataContract]
    public class ShoppingCartResponse : ServiceResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShoppingCartResponse"/> object.
        /// </summary>
        public ShoppingCartResponse()
            : base()
        {
        }

        /// <summary>
        /// Gets or sets the shopping cart.
        /// </summary>
        [DataMember]
        public ShoppingCart ShoppingCart { get; set; }
    }
}