/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using AFM.Commerce.Entities;
    using System.Runtime.Serialization;

    /// <summary>
    /// Sales Order response class for creation of a new order.
    /// </summary>
    [DataContract]
    public class CreateSalesOrderResponse : ServiceResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateSalesOrderResponse"/> object.
        /// </summary>
        public CreateSalesOrderResponse()
            : base()
        {
        }

        /// <summary>
        /// Gets or sets the sales order number.
        /// </summary>
        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        public AFMUserCartData AFMUserCartData { get; set; }
    }
}