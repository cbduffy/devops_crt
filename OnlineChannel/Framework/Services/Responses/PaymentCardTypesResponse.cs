/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Payment card types response.
    /// </summary>
    [DataContract]
    public sealed class PaymentCardTypesResponse : ServiceResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentCardTypesResponse"/> class.
        /// </summary>
        public PaymentCardTypesResponse()
            : this(null)
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentCardTypesResponse"/> class.
        /// </summary>
        public PaymentCardTypesResponse(Collection<PaymentCardType> cardTypes)
        {
            this.CardTypes = cardTypes;
        }

        /// <summary>
        /// Method checks if passed collection is null. If not creates new one to ensure backward compatability.
        /// </summary>
        /// <param name="cardTypes">Card types collection.</param>
        public void SetCardTypes(Collection<PaymentCardType> cardTypes)
        {
            this.CardTypes = cardTypes ?? new Collection<PaymentCardType>();
        }

        /// <summary>
        /// Gets the demo data.
        /// </summary>
        [DataMember]
        public Collection<PaymentCardType> CardTypes { get; private set; }
    }
}