﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Information for a specific kit configuration.
    /// </summary>
    [Serializable]
    [DataContract]
    public sealed class KitComponentVariantResponse : ServiceResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KitComponentVariantResponse"/> object.
        /// </summary>
        public KitComponentVariantResponse()
            : base()
        {
        }

        /// <summary>
        /// Gets or set the information for a specific kit configuration.
        /// </summary>
        [DataMember]
        public IEnumerable<StorefrontListItem> KitComponentVariants { get; set; }
    }
}