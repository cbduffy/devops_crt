﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Delivery preferences response class.
    /// </summary>
    [DataContract]
    public class DeliveryPreferenceResponse : ServiceResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeliveryPreferenceResponse"/> object.
        /// </summary>
        public DeliveryPreferenceResponse()
            : base()
        {
        }

        /// <summary>
        /// Gets or sets the cart delivery preferences.
        /// </summary>
        /// <value>
        /// The cart delivery preferences.
        /// </value>
        [DataMember]
        public CartDeliveryPreferences CartDeliveryPreferences { get; set; }
    }
}
