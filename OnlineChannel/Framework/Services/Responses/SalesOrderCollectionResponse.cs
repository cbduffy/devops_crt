/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Collection of sales order response class.
    /// </summary>
    [DataContract]
    public class SalesOrderCollectionResponse : ServiceResponse
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="SalesOrderCollectionResponse"/> object.
        /// </summary>
        public SalesOrderCollectionResponse()
            : base()
        {
        }

        /// <summary>
        /// A collection of sales orders.
        /// </summary>
        [DataMember]
        public IEnumerable<SalesOrder> SalesOrders { get; set; }
    }
}