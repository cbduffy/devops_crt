﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Tender types response class.
    /// </summary>
    [DataContract]
    public class TenderTypesResponse : ServiceResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TenderTypesResponse"/> object.
        /// </summary>
        public TenderTypesResponse()
            : base()
        {
        }

        /// <summary>
        /// Sets the tender types supported by the channel.
        /// </summary>
        /// <param name="tenderTypes">Tender types.</param>
        public void SetTenderTypes(IEnumerable<TenderType> tenderTypes)
        {
            if (tenderTypes != null)
            {
                foreach (TenderType tenderType in tenderTypes)
                {
                    switch(tenderType)
                    {
                        case TenderType.PayCard:
                            this.HasCreditCardPayment = true;
                            break;
                        case TenderType.PayGiftCard:
                            this.HasGiftCardPayment = true;
                            break;
                        case TenderType.PayLoyaltyCard:
                            this.HasLoyaltyCardPayment = true;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets boolean value which indicates whether the channel supports credit card payment.
        /// </summary>
        [DataMember]
        public bool HasCreditCardPayment { get; set; }

        /// <summary>
        /// Gets or sets boolean value which indicates whether the channel supports gift card payment.
        /// </summary>
        [DataMember]
        public bool HasGiftCardPayment { get; set; }

        /// <summary>
        /// Gets or sets boolean value which indicates whether the channel supports loyalty card payment.
        /// </summary>
        [DataMember]
        public bool HasLoyaltyCardPayment { get; set; }
    }
}