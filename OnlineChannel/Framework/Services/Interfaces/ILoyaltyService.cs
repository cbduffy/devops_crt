﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.ServiceModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    [ServiceContract]
    public interface ILoyaltyService
    {
        /// <summary>
        /// Issues a new loyalty card id for the customer.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        [OperationContract]
        ShoppingCartResponse GenerateLoyaltyCardId(bool isCheckoutSession, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Gets a read only collection of all loyalty card numbers
        /// </summary>
        /// 
        /// <returns>A loyaltyCardsResponse object</returns>
        [OperationContract]
        LoyaltyCardsResponse GetLoyaltyCards();

        /// <summary>
        /// Gets the status of a loyalty card given its id
        /// </summary>
        /// 
        /// <param name="loyaltyCardNumber">The loyalty card Id</param>
        /// <returns>A loyaltyCardResponse object</returns>
        [OperationContract]
        LoyaltyCardResponse GetLoyaltyCardStatus(string loyaltyCardNumber);

        /// <summary>
        /// Gets a read only collection of all loyalty card with their status
        /// </summary>
        /// 
        /// <returns>A loyaltyCardsResponse object</returns>
        [OperationContract]
        LoyaltyCardsResponse GetAllLoyaltyCardsStatus();

        /// <summary>
        /// Gets a stream with all the transaction data specific to a a loyalty card number for a given points category
        /// </summary>
        /// 
        /// <param name="loyaltyCardNumber">The loyalty card Id</param>
        /// <param name="rewardPointId">The reward points Id</param>
        /// <param name="topRows">Number of entries that should appear per page</param>
        /// <returns>A LoyaltyCardTransactionsResponse object</returns>
        [OperationContract]
        LoyaltyCardTransactionsResponse GetLoyaltyCardTransactions(string loyaltyCardNumber, string rewardPointId, int topRows);

        /// <summary>
        /// Updates the loyalty card id on the shopping cart.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="loyaltyCardId">The loyalty card id (or empty string) to set on the cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        [OperationContract]
        ShoppingCartResponse UpdateLoyaltyCardId(bool isCheckoutSession, string loyaltyCardId, ShoppingCartDataLevel dataLevel);
    }
}
