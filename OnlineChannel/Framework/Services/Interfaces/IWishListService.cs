﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    
    /// <summary>
    /// The wish list service interface.
    /// </summary>
    [ServiceContract]
    public interface IWishListService
    {
        /// <summary>
        /// Get the wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        [OperationContract]
        WishListResponse GetWishList(string wishListId);

        /// <summary>
        /// Get the wish lists corresponding to customer.
        /// </summary>
        /// <returns>
        /// The service response containing the wish lists.
        /// </returns>
        [OperationContract]
        WishListCollectionResponse GetWishListsForCurrentCustomer();

        /// <summary>
        /// Deletes the wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        [OperationContract]
        NullResponse DeleteWishList(string wishListId);

        /// <summary>
        /// Creates the wish list.
        /// </summary>
        /// <param name="wishListName">The wish list name.</param>       
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        [OperationContract]
        WishListResponse CreateWishList(string wishListName);

        /// <summary>
        /// Adds items to wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="listings">The items to add.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        [OperationContract]
        WishListResponse AddItemsToWishList(string wishListId, IEnumerable<Listing> listings);

        /// <summary>
        /// Updates items on wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="listings">The items to update.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        [OperationContract]
        WishListResponse UpdateItemsOnWishList(string wishListId, IEnumerable<Listing> listings);

        /// <summary>
        /// Updates the wish list properties.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="wishListName">The wish list name.</param>
        /// <param name="isFavorite">The favorite attribute of wish list.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        [OperationContract]
        WishListResponse UpdateWishListProperties(string wishListId, string wishListName, bool? isFavorite);

        /// <summary>
        /// Remove item from wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="listingId">The item to remove from the wish list.</param>
        [OperationContract]
        NullResponse RemoveItemFromWishList(string wishListId, string listingId);
    }
}