/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.ServiceModel;
    

    /// <summary>
    /// The pricing service interface.
    /// </summary>
    [ServiceContract]
    public interface IPricingService
    {
        /// <summary>
        /// Gets items' prices for the current channel.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <returns>Response containing active listing prices</returns>
        [OperationContract]
        ListingPriceResponse GetActiveListingPrice(IEnumerable<long> listingIds);

        /// <summary>
        /// Gets the best price for an item between all active catalogs for the current channel, affiliations are taken into account.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tiers to be taken into account while calculating prices.</param>
        /// <returns>Response containing active listing prices</returns>
        [OperationContract]
        ListingPriceResponse GetActiveListingPriceWithAffiliationsApplied(IEnumerable<long> listingIds, IEnumerable<long> affiliationLoyaltyTierIds);

        /// <summary>
        /// Gets items' price for the give channel and catalog.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <returns>Response containing active listing prices</returns>
        [OperationContract]
        ListingPriceResponse GetActiveListingPricePerCatalog(IEnumerable<long> listingIds, long channelId, long catalogId);

        /// <summary>
        /// Gets listing price of an item for the give channel and catalog.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tiers to be taken into account while calculating prices.</param>
        /// <returns>Response containing active listing prices</returns>
        [OperationContract]
        ListingPriceResponse GetActiveListingPricePerCatalogWithAffiliationsApplied(IEnumerable<long> listingIds, long channelId, long catalogId, IEnumerable<long> affiliationLoyaltyTierIds);

        /// <summary>
        /// Gets product prices and calculated discounted price for the current channel.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <returns>Response containing independent product prices and calculated discounted price.</returns>
        [OperationContract]
        ListingPriceResponse GetIndependentProductPriceDiscount(IEnumerable<long> listingIds);

        /// <summary>
        /// Gets product prices and calculated discounted price for the given channel and catalog.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <returns>Response containing independent product prices and calculated discounted price.</returns>
        [OperationContract]
        ListingPriceResponse GetIndependentProductPriceDiscountPerCatalog(IEnumerable<long> listingIds, long channelId, long catalogId);

        /// <summary>
        /// Gets product prices and calculated discounted price between all active catalogs and the given affiliations for the current channel.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tiers to be taken into account while calculating prices.</param>
        /// <returns>Response containing independent product prices and calculated discounted price.</returns>
        [OperationContract]
        ListingPriceResponse GetIndependentProductPriceDiscountWithAffiliationsApplied(IEnumerable<long> listingIds, IEnumerable<long> affiliationLoyaltyTierIds);

        /// <summary>
        /// Gets product prices and calculated discounted price for the given channel, catalog and affiliations.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tiers to be taken into account while calculating prices.</param>
        /// <returns>Response containing independent product prices and calculated discounted price.</returns>
        [OperationContract]
        ListingPriceResponse GetIndependentProductPriceDiscountPerCatalogWithAffiliationsApplied(IEnumerable<long> listingIds, long channelId, long catalogId, IEnumerable<long> affiliationLoyaltyTierIds);
    }
}