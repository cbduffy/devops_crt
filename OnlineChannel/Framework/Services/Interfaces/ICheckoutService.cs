/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;    

    /// <summary>
    /// The checkout service interface.
    /// </summary>
    [ServiceContract]
    public interface ICheckoutService
    {
        /// <summary>
        /// Gets details of all the delivery options available for the current legal entity.
        /// </summary>
        /// <returns>
        /// The delivery options response object.
        /// </returns>
        [OperationContract]
        DeliveryOptionsResponse GetDeliveryOptionsInfo();

        /// <summary>
        /// Gets delivery preferences applicable to the current checkout cart.
        /// </summary>
        /// <returns>
        /// The delivery preference response object.
        /// </returns>
        [OperationContract]
        DeliveryPreferenceResponse GetDeliveryPreferences();

        /// <summary>
        /// Gets the applicable delivery options when the user wants to 'ship' the entire order as a single entity.
        /// </summary>
        /// <param name="shipToAddress">The 'ship to' address.</param>
        /// <returns>
        /// The service response containing the available delivery options.
        /// </returns>
        [OperationContract]
        DeliveryOptionsResponse GetOrderDeliveryOptionsForShipping(Address shipToAddress);

        /// <summary>
        /// Gets the delivery options applicable per line when the user wants the items in the cart 'shipped' to them individually.
        /// </summary>
        /// <param name="selectedLineShippingInfo">The shipping information for the selected lines.</param>
        /// <returns>
        /// The service response containing the available delivery options for the specified lines.
        /// </returns>
        [OperationContract]
        DeliveryOptionsResponse GetLineDeliveryOptionsForShipping(IEnumerable<SelectedLineShippingInfo> selectedLineShippingInfo);

        /// <summary>
        /// Commits the selected delivery option to the cart when entire order is being 'delivered' as a single entity.
        /// </summary>
        /// <param name="selectedDeliveryOption">The selected delivery option.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// The updated shopping cart.
        /// </returns>
        [OperationContract]
        ShoppingCartResponse SetOrderDeliveryOption(SelectedDeliveryOption selectedDeliveryOption, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Commits the selected delivery options per line when the sales line in the order are being 'delivered' individually.
        /// </summary>
        /// <param name="selectedLineDeliveryOptions">The line delivery options.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// The updated shopping cart.
        /// </returns>
        [OperationContract]
        ShoppingCartResponse SetLineDeliveryOptions(IEnumerable<SelectedLineDeliveryOption> selectedLineDeliveryOptions, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Gets the payment card types.
        /// </summary>
        /// <returns>The valid card types accepted for payment.</returns>
        [OperationContract]
        PaymentCardTypesResponse GetPaymentCardTypes();

        /// <summary>
        /// Processes a sales order.
        /// </summary>
        /// <param name="tenderDataLine">Tender line data for the order.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <returns>
        /// The sales order response.
        /// </returns>
        [OperationContract]
        CreateSalesOrderResponse CreateOrder(IEnumerable<TenderDataLine> tenderDataLine, string emailAddress);

        /// <summary>
        /// Gets the gift card balance.
        /// </summary>
        /// <param name="giftCardId">Gift card number.</param>
        /// <returns>
        /// The gift card balance response.
        /// </returns>
        [OperationContract]
        GiftCardResponse GetGiftCardInformation(string giftCardId);
     
        /// <summary>
        /// Gets boolean value indicating whether session is authenticated or not.
        /// </summary>
        /// <returns>
        /// True if session is authenticated.
        /// </returns>
        [OperationContract]
        BooleanResponse IsAuthenticatedSession();

        /// <summary>
        /// Gets customer email address.
        /// </summary>
        /// <returns>Email response.</returns>
        [OperationContract]
        StringResponse GetUserEmail();
    }
}