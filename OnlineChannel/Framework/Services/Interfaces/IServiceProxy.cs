﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Incapsulates set of utilities where implementation depends on client (host).
    /// </summary>
    public abstract class ServiceUtilityBase
    {
        public abstract string GetCulture();
        
        public abstract string GetCurrentCustomerId(bool isMandatory);

        public abstract string GetUserName();

        public abstract string GetUserIdentifier();

        public abstract string GetUserEmail();

        public abstract void AddCustomClaims(string customerId);

        public virtual string GetCheckoutCartId()
        {
            SessionType sessionType = GetSessionType();
            string checkoutCartId = GetCheckoutCartIdFromPersistenceStorage(sessionType);
            return checkoutCartId;
        }

        public virtual string GetShoppingCartId()
        {
            SessionType sessionType = GetSessionType();
            string shoppingCartId = GetShoppingCartIdFromPersistenceStorage(sessionType);
            return shoppingCartId;
        }

        public abstract void ClearCheckoutCartIdInPersistenceStorage();

        public virtual string GetCartIdFromPersistenceStorage(SessionType sessionType, CartType cartType)
        {
            if (cartType == CartType.Checkout)
            {
                return GetCheckoutCartIdFromPersistenceStorage(sessionType);
            }
            else if (cartType == CartType.Shopping)
            {
                return GetShoppingCartIdFromPersistenceStorage(sessionType);
            }
            else
            {
                string message = string.Format("CartType '{0}' not recognized.", cartType);
                throw new InvalidOperationException(message);
            }
        }

        public abstract void SetCartIdInPersistenceStorage(SessionType sessionType, CartType cartType, string cartId);

        public abstract string GetShoppingCartIdFromPersistenceStorage(SessionType sessionType);

        public abstract string GetCheckoutCartIdFromPersistenceStorage(SessionType sessionType);

        public abstract void ClearShoppingCartIdInPersistenceStorage();

        public virtual CartType GetCurrentCartType(bool isCheckoutSession)
        {
            if (isCheckoutSession)
            {
                return CartType.Checkout;
            }
            else
            {
                return CartType.Shopping;
            }
        }

        public abstract SessionType GetSessionType();

        public abstract bool IsAssociated();
    }
}