/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.ServiceModel;
    

    /// <summary>
    /// The order service interface.
    /// </summary>
    [ServiceContract]
    public interface IOrderService
    {
        /// <summary>
        /// Get sales orders for the logged in customer.
        /// </summary>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>Sales orders for the logged in customer.</returns>
        [OperationContract]
        SalesOrderCollectionResponse GetSalesOrdersByCustomer(bool includeSalesLines);

        /// <summary>
        /// Get sales order that is successfully committed to the ERP.
        /// </summary>
        /// <param name="salesId">The sales Id.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>The sales order.</returns>
        [OperationContract]
        SalesOrderResponse GetCommittedSalesOrder(string salesId, bool includeSalesLines);

        /// <summary>
        /// Get sales order that is successfully committed to the ERP.
        /// </summary>
        /// <param name="confirmationId">The confirmation Id.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>The sales order.</returns>
        [OperationContract]
        SalesOrderResponse GetCommittedSalesOrderByConfirmationId(string confirmationId, bool includeSalesLines);

        /// <summary>
        /// Get sales order that is pending commitment to the ERP.
        /// </summary>
        /// <param name="transactionId">The transaction Id.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>The sales order.</returns>
        [OperationContract]
        SalesOrderResponse GetPendingSalesOrder(string transactionId, bool includeSalesLines);
    }
}