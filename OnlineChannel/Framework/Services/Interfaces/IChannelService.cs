﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.ServiceModel;

    /// <summary>
    /// The channel service interface.
    /// </summary>
    [ServiceContract]
    public interface IChannelService
    {
        /// <summary>
        /// Gets the channel configuration.
        /// </summary>
        /// <returns>A channel info response that contains the channel configuration.</returns>
        [OperationContract]
        ChannelInfoResponse GetChannelConfiguration();

        /// <summary>
        /// Gets channel tender types.
        /// </summary>
        /// <returns>Tender types response.</returns>
        [OperationContract]
        TenderTypesResponse GetChannelTenderTypes();

        /// <summary>
        /// Gets the channel info for the countries.
        /// </summary>
        /// <returns>Country info response.</returns>
        [OperationContract]
        CountryInfoResponse GetChannelCountryRegionInfo();

        /// <summary>
        /// Gets the states/provinces data for the given country.
        /// </summary>
        /// <param name="countryCode">Country code.</param>
        /// <returns>The states/provinces for the given country.</returns>
        [OperationContract]
        StateProvinceInfoResponse GetStateProvinceInfo(string countryCode);
    }
}