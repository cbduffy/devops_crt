/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    

    /// <summary>
    /// The shopping cart service interface.
    /// </summary>
    [ServiceContract]
    public interface IShoppingCartService
    {
        /// <summary>
        /// Gets the shopping cart associated with the given identifier.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The service response containing the shopping cart.</returns>
        [OperationContract]
        ShoppingCartResponse GetShoppingCart(bool isCheckoutSession, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Adds items to the shopping cart associated with the given identifier.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="listings">The items to add to the cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The service response containing the updated shopping cart.</returns>
        [OperationContract]
        ShoppingCartResponse AddItems(bool isCheckoutSession, IEnumerable<Listing> listings, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Removes items from the shopping cart associated with the given identifier.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="lineIds">The item line identifiers to be removed from the shopping cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The service response containing the updated shopping cart.</returns>
        [OperationContract]
        ShoppingCartResponse RemoveItems(bool isCheckoutSession, IEnumerable<string> lineIds, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Updates items on the shopping cart associated with the given identifier.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="items">The items to be updated.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The service response containing the updated shopping cart.</returns>
        [OperationContract]
        ShoppingCartResponse UpdateItems(bool isCheckoutSession, IEnumerable<TransactionItem> items, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Gets all the promotions for the shopping cart items.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the updated shopping cart.
        /// </returns>
        [OperationContract]
        ShoppingCartResponse GetPromotions(bool isCheckoutSession, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Adds or Removes the discount codes from the cart.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="promotionCode">The promotion code to apply to the cart.</param>
        /// <param name="isAdd">Indicates whether the operation is addition or removal of discount codes.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        [OperationContract]
        ShoppingCartResponse AddOrRemovePromotionCode(bool isCheckoutSession, string promotionCode, bool isAdd, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Gets information about the specific kit configuration that has the provided kit line values.
        /// </summary>
        /// <param name="kitProductMasterIdentifier">The product identifier of the kit product master.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="kitLines">Details of each component line that comprises the kit.</param>
        /// <returns>Specific kit configuration information.</returns>
        [OperationContract]
        KitConfigurationResponse GetKitConfigurationInformation(long kitProductMasterIdentifier, long catalogId, IEnumerable<KitLine> kitLines);

        /// <summary>
        /// Gets the applicable variants of a kit component.
        /// </summary>
        /// <param name="kitComponentProductId">Product identifier of the kit component.</param>
        /// <param name="kitComponentParentId">Product identifier of the kit component master.</param>
        /// <param name="parentKitId">Product identifier of the kit master product.</param>
        /// <param name="kitComponentLineId">The kit component line identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <returns>Returns a collection of kit component variants.</returns>
        [OperationContract]
        KitComponentVariantResponse GetKitComponentVariants(long kitComponentProductId, long kitComponentParentId, long parentKitId, long kitComponentLineId, long catalogId);

        /// <summary>
        /// Starts the checkout process by creating a secure cart and returning it.
        /// </summary>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>A new cart with a random cart id that should be used during the secure checkout process.</returns>
        [OperationContract]
        ShoppingCartResponse CommenceCheckout(ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Sets General AffiliationLoyaltyTiers on Cart.
        /// </summary>
        /// <param name="tiers"></param>
        [OperationContract]
        NullResponse SetCartAffiliationLoyaltyTiers(IEnumerable<long> tiers);

        /// <summary>
        /// Gets General AffiliationLoyaltyTiers from Cart.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        AffiliationLoyaltyTiersResponse GetCartAffiliationLoyaltyTiers();
    }
}