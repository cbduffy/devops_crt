/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    

    /// <summary>
    /// The Customer service interface.
    /// </summary>
    [ServiceContract]
    public interface ICustomerService
    {
        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <returns>A customer response.</returns>
        [OperationContract]
        CustomerResponse GetCustomer();

        /// <summary>
        /// Creates the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>A customer response.</returns>
        [OperationContract]
        CustomerResponse CreateCustomer(Customer customer);

        /// <summary>
        /// Creates the customer.
        /// </summary>
        /// <param name="emailAddress">email address of the customer.</param>
        /// <param name="activationToken">Activation token.</param>
        /// <returns>A customer response.</returns>
        [OperationContract]
        CustomerResponse CreateCustomerByEmail(string emailAddress, string activationToken);

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>Call success/failure response.</returns>
        [OperationContract]
        NullResponse UpdateCustomer(Customer customer);

        /// <summary>
        /// Gets the addresses.
        /// </summary>
        /// <returns>An address collection response.</returns>
        [OperationContract]
        AddressCollectionResponse GetAddresses();

        /// <summary>
        /// Updates the addresses.
        /// </summary>
        /// <param name="addresses">The addresses.</param>
        /// <returns>Call success/failure response.</returns>
        [OperationContract]
        NullResponse UpdateAddresses(IEnumerable<Address> addresses);

        /// <summary>
        /// Search for a customer given the corresponding email address.
        /// </summary>
        /// <param name="emailAddress">email address of the customer.</param>
        /// <returns>True if the customer exists, false otherwise.</returns>
        [OperationContract]
        bool SearchCustomers(string emailAddress);

        /// <summary>
        /// Associates an existing customer to the current credentials if the criteria matches.
        /// </summary>
        /// <param name="email">The customer's e-mail address.</param>
        /// <param name="givenName">Name of the given.</param>
        /// <param name="surname">Name of the sur.</param>
        /// <param name="userId">The user identifier.</param>
        [OperationContract]
        StringResponse AssociateCustomer(string email, string givenName, string surname, string userId);
    }
}