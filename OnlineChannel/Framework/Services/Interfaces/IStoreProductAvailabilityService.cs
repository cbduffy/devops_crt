/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.ServiceModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    

    /// <summary>
    /// The Store Product Avaiability interface.
    /// </summary>
    [ServiceContract]
    public interface IStoreProductAvailabilityService
    {
        /// <summary>
        /// Gets the nearby stores with availability.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="items">The cart items.</param>
        /// <returns>Stores with product availability.</returns>
        [OperationContract]
        StoreProductAvailabilityResponse GetNearbyStoresWithAvailability(decimal latitude, decimal longitude, IEnumerable<TransactionItem> items);

        /// <summary>
        /// Gets the quantities available of the specified listings.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <returns>Available quantities for the listings inquired.</returns>
        [OperationContract]
        ListingAvailableQuantityResponse GetListingAvailableQuantities(IEnumerable<long> listingIds);

        /// <summary>
        /// Gets the nearby stores.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="distance">distance in miles to search</param>
        /// <returns>Stores</returns>
        [OperationContract]
        StoreLocationResponse GetNearbyStores(decimal latitude, decimal longitude, int distance);
    }
}