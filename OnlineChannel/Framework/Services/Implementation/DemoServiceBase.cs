/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

//namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
//{
//    using System.ServiceModel.Activation;
//    using System.ServiceModel.Web;
//    using Microsoft.Dynamics.Retail.Ecommerce.SP.Services.ObjectModel;
//    using Microsoft.Dynamics.Retail.Ecommerce.SP.Services.ViewModel;
//    using Microsoft.SharePoint;
//    using Microsoft.SharePoint.Client.Services;

//    /// <summary>
//    /// Service for demo purposes only.
//    /// </summary>
//    [BasicHttpBindingServiceMetadataExchangeEndpointAttribute]
//    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
//    public class DemoService : IDemoService
//    {
//        /// <summary>
//        /// Generates the server error.
//        /// </summary>
//        /// <exception cref="System.NotImplementedException">Always thrown.</exception>
//        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
//        public void GenerateServerError()
//        {
//            throw new System.NotImplementedException();
//        }

//        /// <summary>
//        /// Gets a service response.
//        /// </summary>
//        /// <returns>Demo service response.</returns>
//        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
//        public DemoServiceResponse GetResponse()
//        {
//            DemoServiceResponse response = null;

//            // Note: the following code runs impersonated as the app pool user
//            SPSecurity.RunWithElevatedPrivileges(() =>
//            {
//                // response = DemoController.Instance.GetResponse();
//            });

//            return response;
//        }
//    }
//}