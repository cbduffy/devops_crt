/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.InteropServices;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using AFM.Commerce.Framework.Extensions.Controllers;
    using AFM.Commerce.Framework.Extensions.Utils;
    using System;

    /// <summary>
    /// Service for customers.
    /// </summary>
    [ComVisible(false)]
    public abstract class CustomerServiceBase : ServiceBase<AFMCustomerController>, ICustomerService
    {
        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <returns>
        /// A customer response.
        /// </returns>
        public virtual CustomerResponse GetCustomer()
        {
            CustomerResponse response = new CustomerResponse();

            ExecuteForAuthorizedUser(response, (string accountNumber) =>
            {
                response.Customer = Controller.GetCustomer(accountNumber);
            });

            return response;
        }

        /// <summary>
        /// Creates the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>
        /// A customer response.
        /// </returns>
        public virtual CustomerResponse CreateCustomer(Customer customer)
        {
            CustomerResponse response = new CustomerResponse();

            if (!ServiceUtility.IsAssociated())
            {
                // Note: the following code runs impersonated as the app pool user
                ExecuteAfterRequestValidation(response, () =>
                {
                    string userId = ServiceUtility.GetUserIdentifier();
                    response.Customer = Controller.CreateCustomer(customer, userId);
                    ServiceUtility.AddCustomClaims(response.Customer.AccountNumber);
                });
            }

            return response;
        }

        /// <summary>
        /// Creates the customer.
        /// </summary>
        /// <param name="emailAddress">Email address of the customer.</param>
        /// <param name="activationToken">Activation token.</param>
        /// <returns>
        /// A customer response.
        /// </returns>
        public virtual CustomerResponse CreateCustomerByEmail(string emailAddress, string activationToken)
        {
            CustomerResponse response = new CustomerResponse();

            if (!ServiceUtility.IsAssociated())
            {
                // Note: the following code runs impersonated as the app pool user
                Execute(response, () =>
                {
                    string userId = ServiceUtility.GetUserIdentifier();
                    response.Customer = Controller.CreateCustomerByEmail(emailAddress, activationToken, userId);
                    ServiceUtility.AddCustomClaims(response.Customer.AccountNumber);
                });
            }

            return response;
        }

        /// <summary>
        /// Search for a customer given the corresponding email address.
        /// </summary>
        /// <param name="emailAddress">Email address of the customer.</param>
        /// <returns>
        /// True if the customer exists, false otherwise.
        /// </returns>
        public virtual bool SearchCustomers(string emailAddress)
        {
            CustomerResponse response = new CustomerResponse();
            bool customerExists = false;

            if (!ServiceUtility.IsAssociated())
            {
                // Note: the following code runs impersonated as the app pool user
                Execute(response, () =>
                {
                    customerExists = Controller.SearchCustomers(emailAddress);
                });
            }

            return customerExists;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>
        /// Call success/failure response.
        /// </returns>
        public virtual NullResponse UpdateCustomer(Customer customer)
        {
            NullResponse response = new NullResponse();

            // Note: the following code runs impersonated as the app pool user
            ExecuteForAuthorizedUser(response, (string accountNumber) =>
            {
                customer.AccountNumber = accountNumber;
                Controller.UpdateCustomer(customer);
            });

            return response;
        }

        /// <summary>
        /// Gets the addresses.
        /// </summary>
        /// <returns>
        /// An address collection response.
        /// </returns>
        public virtual AddressCollectionResponse GetAddresses()
        {
            AddressCollectionResponse response = new AddressCollectionResponse();

            Collection<Address> addresses = null;
            //CS by spriya get Addresses for signed customer
            ExecuteForAuthorizedUser(response, (string accountNumber) =>
            {
                if (!string.IsNullOrEmpty(accountNumber))
                addresses = Controller.GetAddresses(accountNumber);
            });
            if (addresses == null || !addresses.Any())
            {
                // If there are no addresses, then return an empty instance
                // so client side has a data container to populate.
                string customer = AFMDataUtilities.GetCustomerIdFromCookie();
                if (!string.IsNullOrEmpty(customer))
                    addresses = Controller.GetAddresses(customer);
                //Return null 
                /*  if (addresses == null || !addresses.Any())
                  addresses = new Collection<Address>() { new Address() };*/
            }
            //CE by spriya
            response.Addresses = addresses;

            return response;
        }

        /// <summary>
        /// Updates the addresses.
        /// </summary>
        /// <param name="addresses">The addresses.</param>
        /// <returns>
        /// Call success/failure response.
        /// </returns>
        public virtual NullResponse UpdateAddresses(IEnumerable<Address> addresses)
        {
            NullResponse response = new NullResponse();

            ExecuteForAuthorizedUser(response, (string accountNumber) =>
            {
                Controller.UpdateAddresses(accountNumber, addresses);
            });

            return response;
        }

        /// <summary>
        /// Associates an existing customer to the current credentials if the criteria matches.
        /// </summary>
        /// <param name="email">The customer's e-mail address.</param>
        /// <param name="givenName">Name of the given.</param>
        /// <param name="surname">Name of the sur.</param>
        /// <param name="userId">The user identifier.</param>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Thrown when the customer cannot be found.</exception>
        public virtual StringResponse AssociateCustomer(string email, string givenName, string surname, string userId)
        {
            StringResponse response = new StringResponse();
            ExecuteAfterRequestValidation(response, () =>
                {
                    Controller.AssociateCustomer(email, givenName, surname, userId);
                });

            return response;
        }
    }
}