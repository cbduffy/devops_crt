/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using AFM.Commerce.Framework.Extensions.Controllers;

    /// <summary>
    /// Service for product prices.
    /// </summary>
    /// 
    //CE merged for RTM upgrade
    [ComVisible(false)]
    public abstract class PricingServiceBase : ServiceBase<AFMPricingController>, IPricingService
    {
        /// <summary>
        /// Gets items' prices for the current channel.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <returns>Response containing active listing prices</returns>
        public virtual ListingPriceResponse GetActiveListingPrice(IEnumerable<long> listingIds)
        {
            return GetActiveListingPriceWithAffiliationsApplied(listingIds, null);
        }

        /// <summary>
        /// Gets items' price for the give channel and catalog.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <returns>Response containing active listing prices</returns>
        public virtual ListingPriceResponse GetActiveListingPricePerCatalog(IEnumerable<long> listingIds, long channelId, long catalogId)
        {
            return GetActiveListingPricePerCatalogWithAffiliationsApplied(listingIds, channelId, catalogId, null);
        }

        /// <summary>
        /// Gets items' prices for the current channel taking into account affiliation loyalty tiers.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tiers IDS.</param>
        /// <returns>Response containing active listing prices</returns>
        public virtual ListingPriceResponse GetActiveListingPriceWithAffiliationsApplied(IEnumerable<long> listingIds, IEnumerable<long> affiliationLoyaltyTierIds)
        {
            ListingPriceResponse response = new ListingPriceResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = this.ServiceUtility.GetCurrentCustomerId(false);
                // catalog id = null indicates no catalog discounts will be applied. 
                response.ListingPrices = Controller.GetActiveListingPrice(listingIds, 0, null, DateTime.UtcNow, customerId, affiliationLoyaltyTierIds);
            });

            return response;
        }

        /// <summary>
        /// Gets items' price for the give channel and catalog taking into account affiliation loyalty tiers.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tier IDs.</param>
        /// <returns>Response containing active listing prices</returns>
        public virtual ListingPriceResponse GetActiveListingPricePerCatalogWithAffiliationsApplied(IEnumerable<long> listingIds, long channelId, long catalogId, IEnumerable<long> affiliationLoyaltyTierIds)
        {
            ListingPriceResponse response = new ListingPriceResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                response.ListingPrices = Controller.GetActiveListingPrice(listingIds, channelId, catalogId, DateTime.UtcNow, customerId, affiliationLoyaltyTierIds);
            });

            return response;
        }

        /// <summary>
        /// Gets product prices and calculated discounted price for the current channel.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <returns>Response containing independent product prices and calculated discounted price.</returns>
        public virtual ListingPriceResponse GetIndependentProductPriceDiscount(IEnumerable<long> listingIds)
        {
            return GetIndependentProductPriceDiscountWithAffiliationsApplied(listingIds, null);
        }

        /// <summary>
        /// Gets product prices and calculated discounted price for the given channel and catalog.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <returns>Response containing independent product prices and calculated discounted price.</returns>
        public virtual ListingPriceResponse GetIndependentProductPriceDiscountPerCatalog(IEnumerable<long> listingIds, long channelId, long catalogId)
        {
            return GetIndependentProductPriceDiscountPerCatalogWithAffiliationsApplied(listingIds, channelId, catalogId, null);
        }

        /// <summary>
        /// Gets product prices and calculated discounted price between all active catalogs and the given affiliations for the current channel.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tiers to be taken into account while calculating prices.</param>
        /// <returns>Response containing independent product prices and calculated discounted price.</returns>
        public virtual ListingPriceResponse GetIndependentProductPriceDiscountWithAffiliationsApplied(IEnumerable<long> listingIds, IEnumerable<long> affiliationLoyaltyTierIds)
        {
            ListingPriceResponse response = new ListingPriceResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = this.ServiceUtility.GetCurrentCustomerId(false);
                // catalogId = null indicates no catalog discounts will be applied. 
                response.ListingPrices = Controller.GetIndependentProductPriceDiscount(listingIds, 0, null, customerId, affiliationLoyaltyTierIds);
            });

            return response;
        }

        /// <summary>
        /// Gets product prices and calculated discounted price for the given channel, catalog and affiliations.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tiers to be taken into account while calculating prices.</param>
        /// <returns>Response containing independent product prices and calculated discounted price.</returns>
        public virtual ListingPriceResponse GetIndependentProductPriceDiscountPerCatalogWithAffiliationsApplied(IEnumerable<long> listingIds, long channelId, long catalogId, IEnumerable<long> affiliationLoyaltyTierIds)
        {
            ListingPriceResponse response = new ListingPriceResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                response.ListingPrices = Controller.GetIndependentProductPriceDiscount(listingIds, channelId, catalogId, customerId, affiliationLoyaltyTierIds);
            });

            return response;
        }
    }
}