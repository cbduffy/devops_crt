/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Runtime.InteropServices;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;

    /// <summary>
    /// Service for retrieving sales orders.
    /// </summary>
    [ComVisible(false)]
    public abstract class OrderServiceBase : ServiceBase<OrderController>, IOrderService
    {
        /// <summary>
        /// Get sales orders for the logged in customer.
        /// </summary>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>Sales orders for the logged in customer.</returns>
        public virtual SalesOrderCollectionResponse GetSalesOrdersByCustomer(bool includeSalesLines)
        {
            SalesOrderCollectionResponse response = new SalesOrderCollectionResponse();

            ExecuteForAuthorizedUser(response, (string accountNumber) =>
            {
                response.SalesOrders = Controller.GetSalesOrdersByCustomer(accountNumber, includeSalesLines, this.SearchEngine);
            });

            return response;
        }

        /// <summary>
        /// Gets the committed Sales order matching the salesId.
        /// </summary>
        /// <param name="salesId">Sales Id of the order to return.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>Sales Order response containing the matching sales order.</returns>
        public virtual SalesOrderResponse GetCommittedSalesOrder(string salesId, bool includeSalesLines)
        {
            SalesOrderResponse response = new SalesOrderResponse();

            ExecuteForAuthorizedUser(response, (string accountNumber) =>
            {
                response.SalesOrder = Controller.GetCommittedSalesOrder(salesId, includeSalesLines, this.SearchEngine);
            });

            return response;
        }

        /// <summary>
        /// Gets the committed Sales order matching the confirmationId.
        /// </summary>
        /// <param name="confirmationId">ConfirmationId of the order to return.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>Sales Order response containing the matching sales order.</returns>
        public virtual SalesOrderResponse GetCommittedSalesOrderByConfirmationId(string confirmationId, bool includeSalesLines)
        {
            SalesOrderResponse response = new SalesOrderResponse();

            ExecuteForAuthorizedUser(response, (string accountNumber) =>
            {
                response.SalesOrder = Controller.GetCommittedSalesOrderByConfirmationId(confirmationId, includeSalesLines, this.SearchEngine);
            });

            return response;
        }

        /// <summary>
        /// Get the Pending sales order matching the transactionId.
        /// </summary>
        /// <param name="transactionId">Transaction Id of the pending sales order to return.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>Sales Order response containing the matching sales order.</returns>
        public virtual SalesOrderResponse GetPendingSalesOrder(string transactionId, bool includeSalesLines)
        {
            SalesOrderResponse response = new SalesOrderResponse();

            ExecuteForAuthorizedUser(response, (string accountNumber) =>
            {
                response.SalesOrder = Controller.GetPendingSalesOrder(transactionId, includeSalesLines, this.SearchEngine);
            });

            return response;
        }
    }
}