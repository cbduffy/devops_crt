﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.InteropServices;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Extensions.Controllers;
    using AFM.Commerce.Framework.Extensions.Utils;


    /// <summary>
    /// Service for shopping cart checkout.
    /// </summary>
    [ComVisible(false)]
    public abstract class CheckoutServiceBase : ServiceBase<AFMCheckoutController>, ICheckoutService
    {
        /// <summary>
        /// Gets details of all the delivery options available for the current legal entity.
        /// </summary>
        /// <returns>
        /// The delivery options response object.
        /// </returns>
        /// 
        //TO DO : check after RTM merge
        public virtual DeliveryOptionsResponse GetDeliveryOptionsInfo()
        {
            var deliveryOptionsResponse = new DeliveryOptionsResponse();

            ExecuteAfterRequestValidation(deliveryOptionsResponse, () =>
            {
                var afmDeliveryOptions = Controller.AFMGetDeliveryOptionsInfo();
                deliveryOptionsResponse.SetDeliveryOptions(afmDeliveryOptions.DeliveryMethods);
                deliveryOptionsResponse.AddressVerficationResult = afmDeliveryOptions.AddressVerficationResult;
            });
            return deliveryOptionsResponse;
        }

        /// <summary>
        /// Gets delivery preferences applicable to the current checkout cart.
        /// </summary>
        /// <returns>
        /// The delivery preference response object.
        /// </returns>
        public virtual DeliveryPreferenceResponse GetDeliveryPreferences()
        {
            DeliveryPreferenceResponse deliveryPreferenceResponse = new DeliveryPreferenceResponse();

            ExecuteAfterRequestValidation(deliveryPreferenceResponse, () =>
            {
                string checkoutCartId = ServiceUtility.GetCheckoutCartId();

                deliveryPreferenceResponse.CartDeliveryPreferences = Controller.GetDeliveryPreferences(checkoutCartId);
            });

            return deliveryPreferenceResponse;
        }

        /// <summary>
        /// Gets the applicable delivery options when the user wants to 'ship' the entire order as a single entity.
        /// </summary>
        /// <param name="shipToAddress">The 'ship to' address.</param>
        /// <returns>
        /// The service response containing the available delivery options.
        /// </returns>
        public virtual DeliveryOptionsResponse GetOrderDeliveryOptionsForShipping(Address shipToAddress)
        {
            DeliveryOptionsResponse response = new DeliveryOptionsResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                string checkoutCartId = ServiceUtility.GetCheckoutCartId();

                Collection<DeliveryOption> deliveryMethods = Controller.GetOrderDeliveryOptionsForShipping(checkoutCartId, customerId, shipToAddress);
                response.SetDeliveryOptions(deliveryMethods);
            });

            return response;
        }

        /// <summary>
        /// Gets the delivery options applicable per line when the user wants the items in the cart 'shipped' to them individually.
        /// </summary>
        /// <param name="selectedLineShippingInfo">The shipping information for the selected lines.</param>
        /// <returns>
        /// The service response containing the available delivery options for the specified lines.
        /// </returns>
        public virtual DeliveryOptionsResponse GetLineDeliveryOptionsForShipping(IEnumerable<SelectedLineShippingInfo> selectedLineShippingInfo)
        {
            DeliveryOptionsResponse response = new DeliveryOptionsResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                string checkoutCartId = ServiceUtility.GetCheckoutCartId();

                Collection<LineDeliveryOption> lineDeliveryOptions = Controller.GetLineDeliveryOptionsForShipping(checkoutCartId, customerId, selectedLineShippingInfo);
                response.SetLineDeliveryOptions(lineDeliveryOptions);
            });

            return response;
        }

        /// <summary>
        /// Commits the selected delivery option to the cart when entire order is being 'delivered' as a single entity.
        /// </summary>
        /// <param name="selectedDeliveryOption">The selected delivery option.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// The updated shopping cart.
        /// </returns>
        public virtual ShoppingCartResponse SetOrderDeliveryOption(SelectedDeliveryOption selectedDeliveryOption, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse shoppingCartResponse = new ShoppingCartResponse();

            ExecuteAfterRequestValidation(shoppingCartResponse, () =>
            {
                //CS Merged with RTM upgrade
                AFMShoppingCartController afmShoppingCartController = new AFMShoppingCartController();
                //CE Merged with RTM upgrade
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                string checkoutCartId = ServiceUtility.GetCheckoutCartId();
                //Merged with RTM upgrade
                //Hitesh - Start
                string zipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
                List<long> tiers = new List<long>();
                AFMVendorAffiliation afmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(zipCode);
                if (afmVendorAffiliation != null && afmVendorAffiliation.AffiliationId > 0)
                {
                    tiers.Add(afmVendorAffiliation.AffiliationId);
                }
                afmShoppingCartController.SetCartAffiliationLoyaltyTiers(checkoutCartId, tiers);
                //Hitesh - End
            
                shoppingCartResponse.ShoppingCart = Controller.SetOrderDeliveryOption(checkoutCartId, customerId, selectedDeliveryOption, this.ProductValidator, this.SearchEngine, dataLevel);
            });

            return shoppingCartResponse;
        }

        /// <summary>
        /// Commits the selected delivery options per line when the sales line in the order are being 'delivered' individually.
        /// </summary>
        /// <param name="selectedLineDeliveryOptions">The line delivery options.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// The updated shopping cart.
        /// </returns>
        public virtual ShoppingCartResponse SetLineDeliveryOptions(IEnumerable<SelectedLineDeliveryOption> selectedLineDeliveryOptions, ShoppingCartDataLevel dataLevel)
        {

            ShoppingCartResponse shoppingCartResponse = new ShoppingCartResponse();

            ExecuteAfterRequestValidation(shoppingCartResponse, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                string checkoutCartId = ServiceUtility.GetCheckoutCartId();

                shoppingCartResponse.ShoppingCart = Controller.SetLineDeliveryOptions(checkoutCartId, customerId, selectedLineDeliveryOptions, this.ProductValidator, this.SearchEngine, dataLevel);
            });

            return shoppingCartResponse;
        }

        /// <summary>
        /// Processes a sales order.
        /// </summary>
        /// <param name="tenderDataLine">Tender lines data containing payments for the order.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <returns>
        /// The updated shopping cart item collection.
        /// </returns>
        public virtual CreateSalesOrderResponse CreateOrder(IEnumerable<TenderDataLine> tenderDataLine, string emailAddress)
        {
            CreateSalesOrderResponse response = new CreateSalesOrderResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string checkoutCartId = ServiceUtility.GetCheckoutCartId();
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                string userName = ServiceUtility.GetUserName();

                //TO DO : check after RTM Merge
                //CS check RTM merge
                AFMShoppingCartController afmShoppingCartController = new AFMShoppingCartController();
                afmShoppingCartController.SetConfiguration(this.Configuration);
                ShoppingCart checkoutCart = afmShoppingCartController.GetShoppingCart(checkoutCartId, customerId, ShoppingCartDataLevel.Minimal, this.ProductValidator, this.SearchEngine, true);
                 //CE
                 IEnumerable<string> lineIdsToRemoveOnOrderCreation = checkoutCart.Items.Select(i => i.LineId);

                response.OrderNumber = Controller.CreateOrder(checkoutCartId, customerId, tenderDataLine, emailAddress, userName);

                // Since the order has been successfully created by this point,
                // we should swallow all exceptions after this to prevent the call from failing.
                // We do not want the user to unintentionally create duplicate orders.
                try
                {
                    //Now removing the items purchased by the customer which were in the 'Checkout' cart from the 'Shopping' cart
                    // At the same time, retain any items that were added to the 'Shopping' cart in a different session.
                    string shoppingCartId = ServiceUtility.GetShoppingCartId();
                    Controller.RemoveItems(shoppingCartId, customerId, lineIdsToRemoveOnOrderCreation.ToList(), ShoppingCartDataLevel.Minimal, this.ProductValidator, this.SearchEngine);
                }
                catch (Exception ex)
                {
                    NetTracer.Error(ex, "Failed to remove items from shopping cart after order creation.");
                    response.AddError("NeedToDetermineErrorCode", "Failed to remove items from shopping cart.", ex);
                }

                try
                {
                    // Clear the checkout cart id from the cookie.
                    ServiceUtility.ClearCheckoutCartIdInPersistenceStorage();
                    // We will leave the shopping cart cookie as is, in case the user wants to resume their browsing session after checking out.
                }
                catch (Exception ex)
                {
                    NetTracer.Error(ex, "Failed to delete cookies after the end of order creation.");
                    response.AddError("NeedToDetermineErrorCode", "Failed to delete cookies after the end of order creation.", ex);
                }

            });

            return response;
        }

        /// <summary>
        /// Gets the payment card types.
        /// </summary>
        /// <returns>
        /// The valid card types accepted for payment.
        /// </returns>
        public virtual PaymentCardTypesResponse GetPaymentCardTypes()
        {
            PaymentCardTypesResponse response = new PaymentCardTypesResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.SetCardTypes(Controller.GetPaymentCardTypes());
            });

            return response;
        }

        /// <summary>
        /// Gets the gift card balance.
        /// </summary>
        /// <returns>
        /// The gift card balance response.
        /// </returns>
        public virtual GiftCardResponse GetGiftCardInformation(string giftCardId)
        {
            GiftCardResponse response = new GiftCardResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.GiftCardInformation = Controller.GetGiftCardInformation(giftCardId);
            });

            return response;
        }

        /// <summary>
        /// Gets boolean value indicating whether session is authenticated or not.
        /// </summary>
        /// <returns>True if session is authenticated.</returns>
        public virtual BooleanResponse IsAuthenticatedSession()
        {
            BooleanResponse response = new BooleanResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.IsTrue = this.ServiceUtility.GetSessionType().Equals(SessionType.SignedIn);
            });

            return response;
        }

        /// <summary>
        /// Gets customer email address.
        /// </summary>
        /// <returns>Email response.</returns>
        public virtual StringResponse GetUserEmail()
        {
            StringResponse response = new StringResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.Value = this.ServiceUtility.GetUserEmail();
            });

            return response;
        }
    }
}
