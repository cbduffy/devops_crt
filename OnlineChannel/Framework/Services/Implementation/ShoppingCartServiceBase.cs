/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.InteropServices;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using AFM.Commerce.Framework.Extensions;
    using AFM.Commerce.Framework.Extensions.Controllers;
    using AFM.Commerce.Framework.Extensions.Utils;

    /// <summary>
    /// Service for shopping cart.
    /// </summary>
    /// CE Merged with RTM upgrade
    [ComVisible(false)]
    public abstract class ShoppingCartServiceBase : ServiceBase<AFMShoppingCartController>, IShoppingCartService
    {
        /// <summary>
        /// Gets the shopping cart associated .
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether the request originated from the checkout page.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the shopping cart.
        /// </returns>
        public virtual ShoppingCartResponse GetShoppingCart(bool isCheckoutSession, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();

            CartType cartType = ServiceUtility.GetCurrentCartType(isCheckoutSession);
            SessionType sessionType = ServiceUtility.GetSessionType();

            ExecuteAfterRequestValidation(response, () =>
            {
                if (cartType == CartType.Shopping)
                {
                    response.ShoppingCart = GetShoppingCart(sessionType, dataLevel);
                }
                else if (cartType == CartType.Checkout)
                {
                    response.ShoppingCart = GetCheckoutCart(sessionType, dataLevel);
                }
                else
                {
                    string message = string.Format("Invalid cart type encountered: {0}.", cartType);
                    throw new InvalidOperationException(message);
                }

                string responseCartId = string.Empty;
                if (response.ShoppingCart != null)
                {
                    responseCartId = response.ShoppingCart.CartId;
                }

                ServiceUtility.SetCartIdInPersistenceStorage(sessionType, cartType, responseCartId);
            });

            SetShoppingCartResponseError(response);
            return response;
        }

        /// <summary>
        /// Adds items to the shopping cart associated with the given identifier.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="listings">The items to add to the cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the updated shopping cart.
        /// </returns>
        public virtual ShoppingCartResponse AddItems(bool isCheckoutSession, IEnumerable<Listing> listings, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();

            CartType cartType = ServiceUtility.GetCurrentCartType(isCheckoutSession);
            SessionType sessionType = ServiceUtility.GetSessionType();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = string.Empty;

                // If the user is authenticated and not windows authenticated, always get the active cart from the DB before adding items.
                if (sessionType == SessionType.SignedIn)
                {
                    customerId = ServiceUtility.GetCurrentCustomerId(true);
                    //TO DO spriya merged for upgrade
                    response.ShoppingCart = Controller.AddItems(null, customerId, listings, dataLevel, this.ProductValidator, this.SearchEngine, false);
                }
                else if (sessionType == SessionType.Anonymous)
                {
                    string cartId = ServiceUtility.GetCartIdFromPersistenceStorage(sessionType, cartType);
                    //TO DO spriya merged for upgrade
                    response.ShoppingCart = Controller.AddItems(cartId, customerId, listings, dataLevel, this.ProductValidator, this.SearchEngine, false);
                }
                else
                {
                    string message = string.Format("Invalid session type encountered: {0}.", sessionType);
                    throw new InvalidOperationException(message);
                }

                if (response.ShoppingCart != null)
                {
                    ServiceUtility.SetCartIdInPersistenceStorage(sessionType, cartType, response.ShoppingCart.CartId);
                }
            });

            SetShoppingCartResponseError(response);
            return response;
        }

        /// <summary>
        /// Removes items from the shopping cart associated with the given identifier.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="lineIds">The item line identifiers to be removed from the shopping cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the updated shopping cart.
        /// </returns>
        public virtual ShoppingCartResponse RemoveItems(bool isCheckoutSession, IEnumerable<string> lineIds, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();

            CartType cartType = ServiceUtility.GetCurrentCartType(isCheckoutSession);

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);

                if (cartType == CartType.Checkout)
                {
                    // Remove items from the shopping cart.
                    string shoppingCartId = ServiceUtility.GetShoppingCartId();
                    Controller.RemoveItems(shoppingCartId, customerId, lineIds, dataLevel, this.ProductValidator, this.SearchEngine, true);

                    // Remove items from the checkout cart.
                    string checkoutCartId = ServiceUtility.GetCheckoutCartId();
                    response.ShoppingCart = Controller.RemoveItems(checkoutCartId, customerId, lineIds, dataLevel, this.ProductValidator, this.SearchEngine, true);
                }
                else if (cartType == CartType.Shopping)
                {
                    // Remove items from the shopping cart.
                    string shoppingCartId = ServiceUtility.GetShoppingCartId();
                    response.ShoppingCart = Controller.RemoveItems(shoppingCartId, customerId, lineIds, dataLevel, this.ProductValidator, this.SearchEngine, false);
                }
                else
                {
                    string message = string.Format("Invalid cart type encountered: {0}.", cartType);
                    throw new InvalidOperationException(message);
                }
            });

            SetShoppingCartResponseError(response);
            return response;
        }

        /// <summary>
        /// Updates items on the shopping cart associated with the given identifier.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="items">The items to be updated.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the updated shopping cart.
        /// </returns>
        public virtual ShoppingCartResponse UpdateItems(bool isCheckoutSession, IEnumerable<TransactionItem> items, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();
            CartType cartType = ServiceUtility.GetCurrentCartType(isCheckoutSession);

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);


                if (cartType == CartType.Checkout)
                {
                    
                    // Update items on the shopping cart.
                    string shoppingCartId = ServiceUtility.GetShoppingCartId();
                    Controller.UpdateItems(shoppingCartId, customerId, items, dataLevel, this.ProductValidator, this.SearchEngine, false);

                    // Update items on the checkout cart.
                    string checkoutCartId = ServiceUtility.GetCheckoutCartId();
                    response.ShoppingCart = Controller.UpdateItems(checkoutCartId, customerId, items, dataLevel, this.ProductValidator, this.SearchEngine, true);

                }
                else if (cartType == CartType.Shopping)
                {
                    // Update items on the shopping cart.
                    string shoppingCartId = ServiceUtility.GetShoppingCartId();
                    response.ShoppingCart = Controller.UpdateItems(shoppingCartId, customerId, items, dataLevel, this.ProductValidator, this.SearchEngine, false);
                }
                else
                {
                    string message = string.Format("Invalid cart type encountered: {0}.", cartType);
                    throw new InvalidOperationException(message);
                }
            });

            SetShoppingCartResponseError(response);
            return response;
        }

        /// <summary>
        /// Gets all the promotions for the shopping cart items.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The service response containing the updated shopping cart.
        /// </returns>
        public virtual ShoppingCartResponse GetPromotions(bool isCheckoutSession, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();

            CartType cartType = ServiceUtility.GetCurrentCartType(isCheckoutSession);
            SessionType sessionType = ServiceUtility.GetSessionType();

            ExecuteAfterRequestValidation(response, () =>
            {
                string cartId = ServiceUtility.GetCartIdFromPersistenceStorage(sessionType, cartType);

                if (string.IsNullOrWhiteSpace(cartId))
                {
                    response.ShoppingCart = null;
                }
                else
                {
                    //TO DO spriya merged for RTM upgrade
                    response.ShoppingCart = Controller.GetAllPromotionsForShoppingCart(cartId, dataLevel, this.ProductValidator, this.SearchEngine, isCheckoutSession);
                }
            });

            SetShoppingCartResponseError(response);
            return response;
        }

        /// <summary>
        /// Adds or Removes the discount codes from the cart.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="promotionCode">The promotion code to apply to the cart.</param>
        /// <param name="isAdd">Indicates whether the operation is addition or removal of discount codes.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        public virtual ShoppingCartResponse AddOrRemovePromotionCode(bool isCheckoutSession, string promotionCode, bool isAdd, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();

            CartType cartType = ServiceUtility.GetCurrentCartType(isCheckoutSession);
            SessionType sessionType = ServiceUtility.GetSessionType();

            ExecuteAfterRequestValidation(response, () =>
            {
                string cartId = ServiceUtility.GetCartIdFromPersistenceStorage(sessionType, cartType);
                string customerId = ServiceUtility.GetCurrentCustomerId(false);

                response.ShoppingCart = Controller.AddOrRemovePromotionCode(cartId, customerId, promotionCode, isAdd, dataLevel, this.ProductValidator, this.SearchEngine);

                SetShoppingCartResponseError(response);
            });

            return response;
        }

        /// <summary>
        /// Gets information about the specific kit configuration that has the provided kit line values.
        /// </summary>
        /// <param name="kitProductMasterIdentifier">The product identifier of the kit product master.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="kitLines">Details of each component line that comprises the kit.</param>
        /// <returns>Specific kit configuration information.</returns>
        public virtual KitConfigurationResponse GetKitConfigurationInformation(long kitProductMasterIdentifier, long catalogId, IEnumerable<KitLine> kitLines)
        {
            KitConfigurationResponse response = new KitConfigurationResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                response.KitConfigurationInformation = Controller.GetKitConfigurationInformation(kitProductMasterIdentifier, catalogId, kitLines, customerId, this.SearchEngine);
            });

            return response;
        }

        /// <summary>
        /// Gets the applicable variants of a kit component.
        /// </summary>
        /// <param name="kitComponentProductId">Product identifier of the kit component.</param>
        /// <param name="kitComponentParentId">Product identifier of the kit component master.</param>
        /// <param name="parentKitId">Product identifier of the kit master product.</param>
        /// <param name="kitComponentLineId">The kit component line identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <returns>Returns a collection of kit component variants.</returns>
        public virtual KitComponentVariantResponse GetKitComponentVariants(long kitComponentProductId, long kitComponentParentId, long parentKitId, long kitComponentLineId, long catalogId)
        {
            KitComponentVariantResponse response = new KitComponentVariantResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.KitComponentVariants = this.SearchEngine.GetKitComponentVariants(kitComponentProductId, kitComponentParentId, parentKitId, kitComponentLineId, catalogId);
            });

            return response;
        }

        /// <summary>
        /// Starts the checkout process by creating a secure cart and returning it.
        /// </summary>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>A new cart with a random cart id that should be used during the secure checkout process.</returns>
        public virtual ShoppingCartResponse CommenceCheckout(ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string shoppingCartId = ServiceUtility.GetShoppingCartId();
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                string previousCheckoutCartId = ServiceUtility.GetCheckoutCartId();
                SessionType sessionType = ServiceUtility.GetSessionType();

                // Shopping cart would be null if the user lands on the checkout page immediately after signing in.
                // In this case we need to claim the anonymous shopping cart and assign it to the signed in user,
                // because there is no explicit get shopping cart call, which implicitly does the claiming, in the checkout page.
                if (String.IsNullOrWhiteSpace(shoppingCartId) && sessionType == SessionType.SignedIn)
                {
                    ShoppingCart claimedShoppingCart = this.GetShoppingCart(sessionType, ShoppingCartDataLevel.Minimal);
                    shoppingCartId = claimedShoppingCart.CartId;
                    ServiceUtility.SetCartIdInPersistenceStorage(sessionType, CartType.Shopping, shoppingCartId);
                }

                response.ShoppingCart = Controller.CommenceCheckout(shoppingCartId, customerId, previousCheckoutCartId, this.ProductValidator, this.SearchEngine, dataLevel);

                if (response.ShoppingCart == null)
                {
                    string message = string.Format("Unable to create a checkout cart from shopping cart id: {0}", shoppingCartId);
                    throw new InvalidOperationException(message);
                }

                // Update the checkout cart id cookie.
                ServiceUtility.SetCartIdInPersistenceStorage(sessionType, CartType.Checkout, response.ShoppingCart.CartId);
            });

            return response;
        }

        /// <summary>
        /// Gets the shopping cart.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <param name="dataLevel">The data level.</param>
        /// <returns>The latest shopping cart based on the current session type.</returns>
        /// <exception cref="System.InvalidOperationException">Only signed-in and anonymous session types are supported.</exception>
        private ShoppingCart GetShoppingCart(SessionType sessionType, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCart responseShoppingCart = null;

            string shoppingCartId = ServiceUtility.GetShoppingCartIdFromPersistenceStorage(sessionType);
            string customerId = string.Empty;

            if (sessionType == SessionType.Anonymous)
            {
                if (string.IsNullOrWhiteSpace(shoppingCartId))
                {
                    responseShoppingCart = null;
                }
                else
                {
                    //CS Merged with RTM
                    responseShoppingCart = Controller.GetShoppingCart(shoppingCartId, customerId, dataLevel, this.ProductValidator, this.SearchEngine, false);
                    //CE Merged with RTM

                }
            }
            else if (sessionType == SessionType.SignedIn)
            {
                customerId = ServiceUtility.GetCurrentCustomerId(true);

                // Get the latest cart associated with the user.
                ShoppingCart activeAuthenticatedShoppingCart = Controller.GetActiveShoppingCart(customerId, dataLevel, this.ProductValidator, this.SearchEngine);
                string anonymousShoppingCartId = ServiceUtility.GetShoppingCartIdFromPersistenceStorage(SessionType.Anonymous);
                bool isAnonymousShoppingCartIdSet = !string.IsNullOrWhiteSpace(anonymousShoppingCartId);

                if ((activeAuthenticatedShoppingCart == null) && (isAnonymousShoppingCartIdSet))
                {
                    // Claim the shopping cart id present in the cookie.
                    activeAuthenticatedShoppingCart = Controller.ClaimAnonymousCart(anonymousShoppingCartId, customerId, dataLevel, this.ProductValidator, this.SearchEngine, false);
                }
                else if ((activeAuthenticatedShoppingCart != null) && (isAnonymousShoppingCartIdSet))
                {
                    // Move items from the anonymous shopping cart to the authenticated shopping cart.
                    activeAuthenticatedShoppingCart = Controller.MoveItemsBetweenCarts(anonymousShoppingCartId, activeAuthenticatedShoppingCart.CartId, customerId, dataLevel, this.ProductValidator, this.SearchEngine);
                }
                else
                {
                    if (!string.IsNullOrEmpty(shoppingCartId))
                        activeAuthenticatedShoppingCart = Controller.GetShoppingCart(shoppingCartId, customerId, dataLevel, this.ProductValidator, this.SearchEngine, false);
                }

                // Clear anonymous shopping cart identifier.
                ServiceUtility.ClearShoppingCartIdInPersistenceStorage();

                responseShoppingCart = activeAuthenticatedShoppingCart;
            }
            else
            {
                string message = string.Format("Invalid session type encountered: {0}.", sessionType);
                throw new InvalidOperationException(message);
            }

            return responseShoppingCart;
        }

        /// <summary>
        /// Gets the checkout cart.
        /// </summary>
        /// <param name="sessionType">Type of the current session.</param>
        /// <param name="dataLevel">The data level.</param>
        /// <returns>The latest checkout cart based on the current session type.</returns>
        /// <exception cref="System.InvalidOperationException">Only signed-in and anonymous session types are supported.</exception>
        private ShoppingCart GetCheckoutCart(SessionType sessionType, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCart responseCheckoutCart = null;
            string customerId = string.Empty;

            string checkoutCartId = ServiceUtility.GetCheckoutCartIdFromPersistenceStorage(sessionType);

            if (sessionType == SessionType.Anonymous)
            {
                // Anonymous customer will have no customer identifier associated with it.
                //CS Merged with RTM          
                var customercookie = AFMDataUtilities.GetCustomerIdTempFromcookie();
                if (!string.IsNullOrEmpty(customercookie))
                    customerId = customercookie;
                //CE Merged with RTM
            }
            else if (sessionType == SessionType.SignedIn)
            {
                customerId = ServiceUtility.GetCurrentCustomerId(true);
            }
            else
            {
                string message = string.Format("Invalid session type encountered: {0}.", sessionType);
                throw new InvalidOperationException(message);
            }

            //CS Merged with RTM   
            responseCheckoutCart = Controller.GetShoppingCart(checkoutCartId, customerId, dataLevel, this.ProductValidator, this.SearchEngine, true);
            //CE Merged with RTM

            return responseCheckoutCart;
        }

        public NullResponse SetCartAffiliationLoyaltyTiers(IEnumerable<long> tiers)
        {
            string cartId = ServiceUtility.GetShoppingCartId();
            Controller.SetCartAffiliationLoyaltyTiers(cartId, tiers);
            return new NullResponse();
        }

        public AffiliationLoyaltyTiersResponse GetCartAffiliationLoyaltyTiers()
        {
            string cartId = ServiceUtility.GetShoppingCartId();
            IEnumerable<long> tiers = Controller.GetCartAffiliationLoyaltyTiers(cartId);

            return new AffiliationLoyaltyTiersResponse { AffiliationLoyaltyTiers = tiers };
        }

        public void SetShoppingCartResponseError(ShoppingCartResponse shoppingCartResp)
        {
            if (shoppingCartResp.ShoppingCart != null)
            {
                if (!string.IsNullOrEmpty(shoppingCartResp.ShoppingCart.AvalaraTaxError))
                {
                    shoppingCartResp.AddError(Resource.AvalaraTaxNotFound, shoppingCartResp.ShoppingCart.AvalaraTaxError, shoppingCartResp.ShoppingCart.TaxAmountWithCurrency);
                }

                if (!string.IsNullOrEmpty(shoppingCartResp.ShoppingCart.ChargeTypeError))
                {
                    shoppingCartResp.AddError("Error: ", shoppingCartResp.ShoppingCart.ChargeTypeError, string.Empty);
                }

                if (!string.IsNullOrEmpty(shoppingCartResp.ShoppingCart.ATPError))
                {
                    shoppingCartResp.AddError("Error: ", shoppingCartResp.ShoppingCart.ATPError, string.Empty);
                }
            }
        }
    }
}