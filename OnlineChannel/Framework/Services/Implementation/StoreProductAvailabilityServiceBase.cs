/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Service for store availability.
    /// </summary>
    [ComVisible(false)]
    public abstract class StoreProductAvailabilityServiceBase : ServiceBase<StoreProductAvailabilityController>, IStoreProductAvailabilityService
    {
        /// <summary>
        /// Gets the nearby stores with availability.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="items">The cart items.</param>
        /// <returns>
        /// Stores with product availability.
        /// </returns>
        public virtual StoreProductAvailabilityResponse GetNearbyStoresWithAvailability(decimal latitude, decimal longitude, IEnumerable<TransactionItem> items)
        {
            StoreProductAvailabilityResponse response = new StoreProductAvailabilityResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.Stores = Controller.GetNearbyStoresWithAvailability(latitude, longitude, items);
            });

            return response;
        }

        /// <summary>
        /// Gets the quantities available of the specified listings.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <returns>Available quantities for the listings inquired.</returns>
        public virtual ListingAvailableQuantityResponse GetListingAvailableQuantities(IEnumerable<long> listingIds)
        {
            ListingAvailableQuantityResponse response = new ListingAvailableQuantityResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                response.ListingAvailableQuantities = Controller.GetListingAvailableQuantity(listingIds, customerId);
            });

            return response;
        }

        /// <summary>
        /// Gets the nearby stores.
        /// </summary>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="distance">miles to search in</param>
        /// <returns>
        /// Stores.
        /// </returns>
        public virtual StoreLocationResponse GetNearbyStores(decimal latitude, decimal longitude, int distance)
        {
            StoreLocationResponse response = new StoreLocationResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.Stores = StoreLocationController.GetNearbyStores(latitude, longitude, distance);
            });

            return response;
        }
    }
}