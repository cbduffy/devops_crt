/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using AFM.Commerce.Framework.Extensions.Controllers;
    using ViewModel = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Service for wish lists.
    /// </summary>
    [ComVisible(false)]
    public abstract class WishListServiceBase : ServiceBase<WishListController>, IWishListService
    {
        /// <summary>
        /// Gets the wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        public virtual WishListResponse GetWishList(string wishListId)
        {
            WishListResponse response = new WishListResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                response.WishList = Controller.GetWishList(wishListId, customerId, this.SearchEngine);
            });

            return response;
        }

        /// <summary>
        /// Gets the wish lists corresponding to customer.
        /// </summary>
        /// <returns>
        /// The service response containing the wish lists.
        /// </returns>
        public virtual WishListCollectionResponse GetWishListsForCurrentCustomer()
        {
            WishListCollectionResponse response = new WishListCollectionResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                response.WishLists = Controller.GetWishListsForCustomer(customerId, this.SearchEngine);
            });

            return response;
        }

        /// <summary>
        /// Deletes the wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        public virtual NullResponse DeleteWishList(string wishListId)
        {
            NullResponse response = new NullResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                Controller.DeleteWishList(wishListId, customerId);
            });

            return response;
        }

        /// <summary>
        /// Creates the wish list.
        /// </summary>
        /// <param name="wishListName">The wish list name.</param>       
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        public virtual WishListResponse CreateWishList(string wishListName)
        {
            WishListResponse response = new WishListResponse();
            WishList wishList = new WishList();
            wishList.Name = wishListName;

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                response.WishList = Controller.CreateWishList(wishList, customerId);
            });

            return response;
        }

        /// <summary>
        /// Adds items to wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="listings">The items to add.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        public virtual WishListResponse AddItemsToWishList(string wishListId, IEnumerable<Listing> listings)
        {
            WishListResponse response = new WishListResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                response.WishList = Controller.AddItemsToWishList(wishListId, customerId, listings);
            });

            return response;
        }

        /// <summary>
        /// Updates items on wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="listings">The items to update.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        public virtual WishListResponse UpdateItemsOnWishList(string wishListId, IEnumerable<Listing> listings)
        {
            WishListResponse response = new WishListResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                response.WishList = Controller.UpdateItemsOnWishList(wishListId, customerId, listings);
            });

            return response;
        }

        /// <summary>
        /// Updates the wish list properties.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="wishListName">The wish list name.</param>
        /// <param name="isFavorite">The favorite attribute of wish list.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        public virtual WishListResponse UpdateWishListProperties(string wishListId, string wishListName, bool? isFavorite)
        {
            WishListResponse response = new WishListResponse();

            string customerId = ServiceUtility.GetCurrentCustomerId(true);

            WishList wishList = new WishList();
            wishList.Id = wishListId;
            wishList.Name = wishListName;
            wishList.IsFavorite = isFavorite;
            wishList.CustomerId = customerId;

            ExecuteAfterRequestValidation(response, () =>
            {
                response.WishList = Controller.UpdateWishListProperties(wishList, customerId);
            });

            return response;
        }

        /// <summary>
        /// Remove item from wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="listingId">The item to remove from the wish list.</param>
        public virtual NullResponse RemoveItemFromWishList(string wishListId, string listingId)
        {
            NullResponse response = new NullResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                Controller.RemoveItemFromWishList(wishListId, customerId, listingId);
            });

            return response;
        }
    }
}