﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Runtime.InteropServices;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Service for channel.
    /// </summary>
    [ComVisible(false)]
    public abstract class ChannelServiceBase : ServiceBase<ChannelController>, IChannelService
    {
        /// <summary>
        /// Gets the channel configuration.
        /// </summary>
        /// <returns>A channel info response that contains the channel configuration.</returns>
        public virtual ChannelInfoResponse GetChannelConfiguration()
        {
            ChannelInfoResponse response = new ChannelInfoResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                Controller.SetConfiguration(this.Configuration);
                ChannelConfiguration channelConfiguration = Controller.GetChannelConfiguration();
                 response.ChannelConfiguration = channelConfiguration;
            });

            return response;
        }

        /// <summary>
        /// Gets channel tender types.
        /// </summary>
        /// <returns>Tender types response.</returns>
        public virtual TenderTypesResponse GetChannelTenderTypes()
        {
            TenderTypesResponse response = new TenderTypesResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.SetTenderTypes(Controller.GetTenderTypes());
            });

            return response;
        }

        /// <summary>
        /// Gets the channel country region info.
        /// </summary>
        /// <returns>Country info response.</returns>
        public virtual CountryInfoResponse GetChannelCountryRegionInfo()
        {
            CountryInfoResponse response = new CountryInfoResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.Countries = Controller.GetCountryRegionInfo();
            });

            return response;
        }

        /// <summary>
        /// Gets the states/provinces data for the given country.
        /// </summary>
        /// <param name="countryCode">Country code.</param>
        /// <returns>The states/provinces for the given country.</returns>
        public virtual StateProvinceInfoResponse GetStateProvinceInfo(string countryCode)
        {
            StateProvinceInfoResponse response = new StateProvinceInfoResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.StateProvinces = Controller.GetStateProvinces(countryCode);
            });

            return response;
        }
    }
}