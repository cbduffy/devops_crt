﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;

    /// <summary>
    /// Base class for eCommerce Services.
    /// </summary>
    /// <remarks>Declares set of properties which should be implemented in derived service class which is client/host specific.</remarks>
    public abstract class ServiceBase<T> where T : ControllerBase, new()
    {
        private T controllerValue;
        private static object syncRoot = new object();
        protected T Controller
        {
            get
            {
                if (controllerValue == null)
                {
                    lock (syncRoot)
                    {
                        if (controllerValue == null)
                        {
                            controllerValue = CreateController();
                            controllerValue.SetConfiguration(this.Configuration);
                        }
                    }
                }

                return controllerValue;
            }
        }

        protected virtual T CreateController()
        {
            return new T();
        }

        protected abstract ISearchEngine SearchEngine
        {
            get;
        }

        protected abstract IProductValidator ProductValidator
        {
            get;
        }

        protected abstract ServiceUtilityBase ServiceUtility
        {
            get;
        }

        protected abstract IConfiguration Configuration
        {
            get;
        }

        protected abstract void Execute(ServiceResponse response, Action action);

        protected abstract void ExecuteAfterRequestValidation(ServiceResponse response, Action action);

        protected abstract void ExecuteForAuthorizedUser(ServiceResponse response, Action<string> action);
    }
}