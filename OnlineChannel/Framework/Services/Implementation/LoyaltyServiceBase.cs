﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services
{
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.InteropServices;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    [ComVisible(false)]
    public abstract class LoyaltyServiceBase : ServiceBase<LoyaltyController>, ILoyaltyService
    {
        /// <summary>
        /// Issues a new loyalty card id for the customer.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        public virtual ShoppingCartResponse GenerateLoyaltyCardId(bool isCheckoutSession, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();

            CartType cartType = ServiceUtility.GetCurrentCartType(isCheckoutSession);
            SessionType sessionType = ServiceUtility.GetSessionType();

            ExecuteAfterRequestValidation(response, () =>
            {
                string cartId = ServiceUtility.GetCartIdFromPersistenceStorage(sessionType, cartType);
                string customerId = ServiceUtility.GetCurrentCustomerId(false);

                LoyaltyController loyaltyController = new LoyaltyController();
                loyaltyController.SetConfiguration(this.Configuration);
                response.ShoppingCart = loyaltyController.GenerateLoyaltyCardId(cartId, customerId, dataLevel, this.ProductValidator, this.SearchEngine);
            });

            return response;
        }

        /// <summary>
        /// Gets a read only collection of all loyalty card numbers
        /// </summary>
        /// 
        /// <returns>A loyaltyCardsResponse object</returns>
        public virtual LoyaltyCardsResponse GetLoyaltyCards()
        {
            LoyaltyCardsResponse response = new LoyaltyCardsResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);

                LoyaltyController loyaltyController = new LoyaltyController();
                loyaltyController.SetConfiguration(this.Configuration);
                response.LoyaltyCards = loyaltyController.GetLoyaltyCards(customerId);
            });

            return response;
        }

        /// <summary>
        /// Gets the status of a loyalty card given its id
        /// </summary>
        /// 
        /// <param name="loyaltyCardNumber">The loyalty card Id</param>
        /// <returns>A loyaltyCardResponse object</returns>
        public virtual LoyaltyCardResponse GetLoyaltyCardStatus(string loyaltyCardNumber)
        {
            LoyaltyCardResponse response = new LoyaltyCardResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                LoyaltyController loyaltyController = new LoyaltyController();
                loyaltyController.SetConfiguration(this.Configuration);
                response.LoyaltyCard = loyaltyController.GetLoyaltyCardStatus(loyaltyCardNumber);
            });

            return response;
        }

        /// <summary>
        /// Gets a read only collection of all loyalty card with their status
        /// </summary>
        /// 
        /// <returns>A loyaltyCardsResponse object</returns>
        public virtual LoyaltyCardsResponse GetAllLoyaltyCardsStatus()
        {
            LoyaltyCardsResponse response = new LoyaltyCardsResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                LoyaltyController loyaltyController = new LoyaltyController();
                loyaltyController.SetConfiguration(this.Configuration);
                response.LoyaltyCards = loyaltyController.GetAllLoyaltyCardsStatus(customerId);
            });

            return response;
        }


        /// <summary>
        /// Gets a stream with all the transaction data specific to a a loyalty card number for a given points category
        /// </summary>
        /// 
        /// <param name="loyaltyCardNumber">The loyalty card Id</param>
        /// <param name="rewardPointId">The reward points Id</param>
        /// <param name="topRows">Number of entries that should appear per page</param>
        /// <returns>A LoyaltyCardTransactionsResponse object</returns>
        public virtual LoyaltyCardTransactionsResponse GetLoyaltyCardTransactions(string loyaltyCardNumber, string rewardPointId, int topRows)
        {
            LoyaltyCardTransactionsResponse response = new LoyaltyCardTransactionsResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                LoyaltyController loyaltyController = new LoyaltyController();
                loyaltyController.SetConfiguration(this.Configuration);
                response.LoyaltyCardTransactions = loyaltyController.GetLoyaltyCardTransactions(loyaltyCardNumber, rewardPointId, topRows);
            });

            return response;
        }

        /// <summary>
        /// Updates the loyalty card id on the shopping cart.
        /// </summary>
        /// <param name="isCheckoutSession">Indicates whether this is in context of a checkout session.</param>
        /// <param name="loyaltyCardId">The loyalty card id (or empty string) to set on the cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        public virtual ShoppingCartResponse UpdateLoyaltyCardId(bool isCheckoutSession, string loyaltyCardId, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();

            CartType cartType = ServiceUtility.GetCurrentCartType(isCheckoutSession);
            SessionType sessionType = ServiceUtility.GetSessionType();

            ExecuteAfterRequestValidation(response, () =>
            {
                string cartId = ServiceUtility.GetCartIdFromPersistenceStorage(sessionType, cartType);
                string customerId = ServiceUtility.GetCurrentCustomerId(false);

                LoyaltyController loyaltyController = new LoyaltyController();
                loyaltyController.SetConfiguration(this.Configuration);
                response.ShoppingCart = loyaltyController.UpdateLoyaltyCardId(cartId, loyaltyCardId, customerId, dataLevel, this.ProductValidator, this.SearchEngine);
            });

            return response;
        }
    }
}
