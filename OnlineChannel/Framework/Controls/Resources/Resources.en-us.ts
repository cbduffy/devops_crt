﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
	ResourceStrings["en-us"] = {
		String_1: "Shopping cart",
		String_2: "Product details",
		String_3: "Each",
		String_4: "Quantity",
		String_5: "Line total",
		String_6: "Remove",
		String_7: "Savings",
		String_8: "Update quantity",
		String_9: "Order summary",
		String_10: "Subtotal:",
		String_11: "Shipping and handling:",
		String_12: "Order total:",
		String_13: "Total savings:",
        String_14: "Next step",
		String_15: "There are no items in your shopping cart. Please add items to the cart.",
		String_16: "Delivery information",
		String_17: "Delivery preference:",
		String_18: "Shipping address",
		String_19: "Shipping name",
		String_20: "Country/region",
		String_21: "United States",
		String_22: "Address",
		String_23: "City",
		String_24: "State/province",
		String_25: "ZIP/postal code",
		String_26: "Shipping method",
		String_27: "Get shipping options",
		String_28: "Previous step",
		String_29: "Name",
		String_30: "Billing information",
		String_31: "Contact information",
		String_32: "Email address",
		String_33: "Confirm email address",
		String_34: "Payment method",
		String_35: "Card number",
		String_36: "Card type",
		String_37: "Expiration month",
		String_38: "Expiration year",
		String_39: "CCID",
		String_40: "What is this?",
		String_41: "Payment amount",
		String_42: "Billing address",
		String_43: "Same as shipping address",
		String_44: "Address2",
		String_45: "Review and confirm",
		String_46: "Order information",
		String_47: "Edit",
		String_48: "Credit card",
		String_49: "Checkout",
		String_50: "You have not added any promotion code to your order",
		String_51: "Tax:",
		String_52: "Submit order",
		String_53: "Thank you for your order",
		String_54: "Your order confirmation number is ",
		String_55: "Street",
		String_56: "State",
		String_57: "Zipcode",
		String_58: "Email",
		String_59: "Payment",
		String_60: "CardNumber",
		String_61: "Please select shipping method",
		String_62: "The confirm email address must match the email address.",
		String_63: "Sorry, something went wrong. The shopping cart information couldn't be retrieved. Please refresh the page and try again.",
		String_64: "Sorry, something went wrong. The product was not removed from the cart successfully. Please refresh the page and try again.",
		String_65: "Sorry, something went wrong. The product quantity couldn't be updated. Please refresh the page and try again.",
		String_66: "Sorry, something went wrong. Delivery methods could not be retrieved. Please refresh the page and try again.",
		String_67: "Sorry, something went wrong. The shipping information was not stored successfully. Please refresh the page and try again.",
		String_68: "Sorry, something went wrong. The payment card type information was not retrieved successfully. Please refresh the page and try again.",
		String_69: "Sorry, something went wrong. The order submission was not successful. Please refresh the page and try again.",
		String_70: "Invalid parameter",
		String_71: "validatorType attribute is not provided for validator binding.",
		String_72: "Use text characters only. Numbers, spaces, and special characters are not allowed.",
		String_73: "Use text characters only. Numbers, spaces, and special characters are not allowed.",
		String_74: "The quantity field cannot be empty",
		String_75: "Select delivery method.",
		String_76: "The email address is invalid.",
		String_77: "Please enter the name.",
		String_78: "Please enter the street number.",
		String_79: "Please enter the address.",
		String_80: "Please enter the city.",
		String_81: "Please enter the zip/postal code.",
		String_82: "Please enter the state.",
		String_83: "Please enter the country.",
		String_84: "Please specify a payment card name.",
		String_85: "Please enter a valid card number.",
		String_86: "Please enter a valid CCID.",
		String_87: "Please specify a valid amount.",
		String_88: "{0} PRODUCT(S)",
		String_89: "Included",
		String_90: "Color: {0}",
		String_91: "Size: {0}",
		String_92: "Style: {0}",
		String_93: "Sorry, something went wrong. The promotion code could not be added successfully. Please refresh the page and try again.",
		String_94: "Sorry, something went wrong. The promotion code could not be removed successfully. Please refresh the page and try again.",
		String_95: "Apply",
		String_96: "Promotion Codes",
		String_97: "Please enter a promotion code",
		String_98: "Sorry, something went wrong. The channel configuration could not be retrieved successfully. Please refresh the page and try again.",
		String_99: "Ship items",
		String_100: "Pick up in store",
		String_101: "Select delivery options by item",
		String_102: "Find a store",
		String_103: "miles",
		String_104: "Available stores",
		String_105: "Store",
		String_106: "Distance",
		String_107: "Products are not available for pick up in the stores around the location you searched. Please update your delivery preferences and try again.",
		String_108: "Sorry, something went wrong. An error occurred while trying to get stores. Please refresh the page and try again.",
		String_109: "Sorry, we were not able to decipher the address you gave us.  Please enter a valid Address",
		String_110: "Sorry, something went wrong. An error has occured while looking up the address you provided. Please refresh the page and try again.",
		String_111: "Products are not available in this store",
		String_112: "Product availability:",
		String_113: "Products are not available in the selected store, Please select a different store",
		String_114: "Please select a store for pick up",
		String_115: "Store address",
		String_116: "Send to me",
		String_117: "Optional note",
		String_118: "Please enter email address for gift card delivery",
		String_119: "Sorry, something went wrong. An error occurred while trying to get the email address. Please enter the email address in the text box below",
		String_120: "Enter the shipping address and then click get shipping options to view the shipping options that are available for your area.",
		String_121: "Delivery method",
		String_122: "Select your delivery preference",
		String_123: "Cancel",
		String_124: "Done",
		String_125: "for product: {0}",
		String_126: "Please select delivery preference for product {0}",
		String_127: "Add credit card",
		String_128: "Gift card",
		String_129: "Add gift card",
		String_130: "Loyalty card",
		String_131: "Add loyalty card",
		String_132: "Payment information",
		String_133: "Payment total:",
		String_134: "Order total:",
		String_135: "Gift card does not exist",
		String_136: "Gift card balance",
		String_137: "Card details",
		String_138: "Sorry, something went wrong. An error occurred while trying to get payment methods supported by the store. Please refresh the page and try again.",
		String_139: "Please select payment method",
		String_140: "The expiration date is not valid. Please select valid expiration month and year and then try again",
		String_141: "Please enter a valid gift card number",
		String_142: "Get gift card balance",
		String_143: "Apply full amount",
		String_144: "Please enter a gift card number",
		String_145: "Sorry, something went wrong. An error occurred while trying to get gift card balance. Please refresh the page and try again.",
		String_146: "Gift card payment amount cannot be zero",
		String_147: "Gift card payment amount is more than order total",
		String_148: "Gift card does not have sufficient balance",
		String_149: "Payment amount is different from the order total",
		String_150: "Sorry, something went wrong. An error occurred while trying to get loyalty card information. Please refresh the page and try again.",
		String_151: "Please enter a valid loyalty card number",
		String_152: "Loyalty card payment amount cannot be zero",
		String_153: "Loyalty card payment amount is more than order total",
		String_154: "The loyalty card is blocked",
		String_155: "The loyalty card is not allowed for payment",
		String_156: "The loyalty payment amount exceeds what is allowed for this loyalty card in this transaction",
		String_157: "The loyalty card number does not exist",
		String_158: "Please select delivery preference",
		String_159: "Please select a delivery preference...",
		String_160: "Sorry, something went wrong. An error occurred while trying to get delivery methods information. Please refresh the page and try again.",
		String_161: "Select address...",
		String_162: "You have not added loyalty card number to your order",
		String_163: "Enter a reward card for the current order. You can include only one reward card per order",
		String_164: "Sorry, something went wrong. An error occurred while trying to update reward card id in cart. Please refresh the page and try again.",
		String_165: "Sorry, something went wrong. An error occurred while retrieving the country region information. Please refresh the page and try again.",
        String_166: "TBD",
        String_167: "Mini Cart",
        String_168: "Ordering FAQ",
        String_169: "Return policy",
        String_170: "Store locator tool",
        String_171: "Continue shopping",
        String_172: "Cart Order Total",
        String_173: "View full cart contents",
        String_174: "Quantity:",
        String_175: "Added to your cart:",
        String_176: "Loading ...",
        String_177: "Sorry, something went wrong. The cart's promotion information couldn't be retrieved. Please refresh the page and try again.",
        String_178: "Delivery method",
        String_179: "Updating shopping cart ...",
        String_180: "Submitting order ...",
        String_181: "Discount code",
        String_182: "Add coupon code",
        String_183: "Enter a discount code",
        String_184: "Please enter a valid discount code",
        String_185: "Sorry, something went wrong. An error occurred while retrieving the state/province information. Please refresh the page and try again.",
        String_186: "Edit reward card",
        String_187: "Reward card",
        String_188: "Add discount code",
        String_189: "Select country/region",
        String_190: "Select state/province",
        String_191: "You have selected multiple delivery methods for this order",
        String_192: "01-January",
        String_193: "02-February",
        String_194: "03-March",
        String_195: "04-April",
        String_196: "05-May",
        String_197: "06-June",
        String_198: "07-July",
        String_199: "08-August",
        String_200: "09-September",
        String_201: "10-October",
        String_202: "11-November",
        String_203: "12-December",
        String_204: "The number string has more than one decimal operator."
    };
}