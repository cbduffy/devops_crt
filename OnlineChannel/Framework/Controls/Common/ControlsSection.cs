﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls
{
    using System.Configuration;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Represents the configuration section for the Ecommerce controls.
    /// </summary>
    [ComVisible(false)]
    public sealed class ControlsSection : ConfigurationSection
    {
        private const string services = "services";
        private const string checkoutControl = "checkout";
        private const string productUrlFormat = "productUrlFormat";

        /// <summary>
        /// Gets the services.
        /// </summary>
        [ConfigurationProperty(services)]
        public Services Services
        {
            get { return (Services)this[services]; }
        }

        /// <summary>
        /// Gets the checkout control.
        /// </summary>
        [ConfigurationProperty(checkoutControl)]
        public CheckoutControl Checkout
        {
            get { return (CheckoutControl)this[checkoutControl]; }
        }

        /// <summary>
        /// Gets the product url format.
        /// </summary>
        [ConfigurationProperty(productUrlFormat)]
        public string ProductUrlFormat
        {
            get { return (string)this[productUrlFormat]; }
        }
    }

    
    /// <summary>
    /// Represents the configuration element for the services.
    /// </summary>
    [ComVisible(false)]
    public sealed class Services : ConfigurationElement
    {
        private const string CheckoutServiceUrlKey = "checkoutServiceUrl";
        private const string ShoppingCartServiceUrlKey = "shoppingCartServiceUrl";
        private const string StoreProductAvailabilityServiceUrlKey = "storeProductAvailabilityServiceUrl";
        private const string ChannelServiceUrlKey = "channelServiceUrl";
        private const string LoyaltyServiceUrlKey = "loyaltyServiceUrl";
        private const string CustomerServiceUrlKey = "customerServiceUrl";

        /// <summary>
        /// Gets the shopping cart service url.
        /// </summary>
        [ConfigurationProperty(ShoppingCartServiceUrlKey)]
        public string ShoppingCartServiceUrl
        {
            get { return (string)this[ShoppingCartServiceUrlKey]; }
        }

        /// <summary>
        /// Gets the checkout service url.
        /// </summary>
        [ConfigurationProperty(CheckoutServiceUrlKey)]
        public string CheckoutServiceUrl
        {
            get { return (string)this[CheckoutServiceUrlKey]; }
        }

        /// <summary>
        /// Gets the store product availability service url.
        /// </summary>
        [ConfigurationProperty(StoreProductAvailabilityServiceUrlKey)]
        public string StoreProductAvailabilityServiceUrl
        {
            get { return (string)this[StoreProductAvailabilityServiceUrlKey]; }
        }

        /// <summary>
        /// Gets the channel service url.
        /// </summary>
        [ConfigurationProperty(ChannelServiceUrlKey)]
        public string ChannelServiceUrl
        {
            get { return (string)this[ChannelServiceUrlKey]; }
        }

        /// <summary>
        /// Gets the loyalty service url.
        /// </summary>
        [ConfigurationProperty(LoyaltyServiceUrlKey)]
        public string LoyaltyServiceUrl
        {
            get { return (string)this[LoyaltyServiceUrlKey]; }
        }

        /// <summary>
        /// Gets the customer service url.
        /// </summary>
        [ConfigurationProperty(CustomerServiceUrlKey)]
        public string CustomerServiceUrl
        {
            get { return (string)this[CustomerServiceUrlKey]; }
        }
    }

    /// <summary>
    /// Represents the configuration element for the checkout control.
    /// </summary>
    [ComVisible(false)]
    public sealed class CheckoutControl : ConfigurationElement
    {
        private const string isDemoMode = "isDemoMode";
        private const string demoDataPath = "demoDataPath";

        /// <summary>
        /// Gets the isDemoMode property.
        /// </summary>
        /// <remarks>
        /// This property indicates whether the checkout control is in auto fill demo mode or not.
        /// </remarks>
        [ConfigurationProperty(isDemoMode)]
        public bool IsDemoMode
        {
            get { return (bool)this[isDemoMode]; }
        }

        /// <summary>
        /// Gets the demoDataPath property.
        /// </summary>
        /// <remarks>
        /// This property contains the relative to the page path to the demoData.xml file 
        /// that is used by autoFillCheckout in checkout.ts.
        /// </remarks>
        [ConfigurationProperty(demoDataPath)]
        public string DemoDataPath
        {
            get { return (string)this[demoDataPath]; }
        }
    }
}
