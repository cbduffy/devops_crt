﻿/// <reference path="../ExternalScripts/JQuery.d.ts" />
/// <reference path="../ExternalScripts/KnockoutJS.d.ts" />
declare var msaxError: {
    show: (level: any, message: any, errorCodes?: any) => void;
};
declare module msaxValues {
    var msax_CheckoutServiceUrl: any;
    var msax_ShoppingCartServiceUrl: any;
    var msax_OrderConfirmationUrl: any;
    var msax_IsDemoMode: any;
    var msax_DemoDataPath: any;
    var msax_StoreProductAvailabilityServiceUrl: any;
    var msax_ChannelServiceUrl: any;
    var msax_LoyaltyServiceUrl: any;
    var msax_CustomerServiceUrl: any;
    var msax_HasInventoryCheck: any;
    var msax_CheckoutUrl: any;
    var msax_IsCheckoutCart: any;
    var msax_ContinueShoppingUrl: any;
    var msax_CartDiscountCodes: any;
    var msax_CartLoyaltyReward: any;
    var msax_ShoppingCartUrl: any;
    var msax_CartDisplayPromotionBanner: any;
    var msax_ReviewDisplayPromotionBanner: any;
}
declare module Microsoft.Maps {
    var Map: any;
    var loadModule: any;
    var Search: any;
    var Events: any;
    var Pushpin: any;
    var Infobox: any;
    var Point: any;
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class AjaxProxy {
        private relativeUrl;
        constructor(relativeUrl: any);
        private ajaxErrorHandler(e, xhr, settings);
        public SubmitRequest: (webMethod: any, data: any, successCallback: any, errorCallback: any) => void;
    }
    class LoadingOverlay {
        private static existingOverlayClasses;
        static CreateLoadingDialog(loadingDialog: any, width: number, height: number): void;
        static ShowLoadingDialog(loadingDialog: any, loadingText: any, text?: string): void;
        static CloseLoadingDialog(loadingDialog: any): void;
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class ShoppingCartDataLevel {
        static Minimal: number;
        static Extended: number;
        static All: number;
    }
    class TransactionItemType {
        static None: number;
        static Kit: number;
        static KitComponent: number;
    }
    enum LoyaltyCardTenderType {
        AsCardTender = 0,
        AsContactTender = 1,
        NoTender = 2,
        Blocked = 3,
    }
    class ShoppingCartType {
        static None: number;
        static Shopping: number;
        static Checkout: number;
    }
    class AddressType {
        static Delivery: number;
        static Payment: number;
    }
    enum DeliveryPreferenceType {
        None = 0,
        ShipToAddress = 1,
        PickupFromStore = 2,
        ElectronicDelivery = 3,
        DeliverItemsIndividually = 4,
    }
    interface CartProductDetails {
        Name?: string;
        ProductUrl?: string;
        ImageUrl?: string;
        ProductNumber?: string;
        DimensionValues?: string;
        SKU?: string;
        ImageMarkup?: string;
        Quantity?: number;
    }
    interface SelectedLineShippingInfo {
        LineId?: string;
        ShipToAddress?: Address;
    }
    interface SelectedDeliveryOption {
        DeliveryModeId?: string;
        DeliveryModeText?: string;
        DeliveryPreferenceId?: string;
        CustomAddress?: Address;
        StoreAddress?: StoreProductAvailability;
        ElectronicDeliveryEmail?: string;
        ElectronicDeliveryEmailContent?: string;
    }
    interface SelectedLineDeliveryOption {
        LineId?: string;
        DeliveryModeId?: string;
        DeliveryModeText?: string;
        DeliveryPreferenceId?: string;
        CustomAddress?: Address;
        StoreAddress?: StoreProductAvailability;
        ElectronicDeliveryEmail?: string;
        ElectronicDeliveryEmailContent?: string;
    }
    interface DeliveryPreference {
        Value?: string;
        Text?: string;
    }
    interface ImageInfo {
        Url?: string;
        AltText?: string;
    }
    interface TransactionItem {
        LineId: string;
        ItemType?: TransactionItemType;
        ProductDetailsExpanded?: CartProductDetails;
        KitComponents?: TransactionItem[];
        SelectedDeliveryOption?: SelectedDeliveryOption;
        ProductId?: number;
        ProductNumber?: string;
        ItemId?: string;
        VariantInventoryDimensionId?: string;
        Quantity?: number;
        PriceWithCurrency?: string;
        TaxAmountWithCurrency?: string;
        DiscountAmount?: number;
        DiscountAmountWithCurrency?: string;
        NetAmountWithCurrency?: string;
        ShippingAddress?: Address;
        DeliveryModeId?: string;
        DeliveryModeText?: string;
        ElectronicDeliveryEmail?: string;
        PromotionLines?: string[];
        ProductDetails?: string;
        NoOfComponents?: string;
        Color?: string;
        Size?: string;
        Style?: string;
        Name?: string;
        Description?: string;
        ProductUrl?: string;
        Image?: ImageInfo;
        ImageMarkup?: string;
        OfferNames?: string;
        DeliveryPreferences?: DeliveryPreference[];
    }
    interface ShoppingCart {
        CartId?: string;
        Name?: string;
        Items?: TransactionItem[];
        LastModifiedDate?: Date;
        CartType?: ShoppingCartType;
        PromotionLines?: string[];
        DiscountCodes?: string[];
        SelectedDeliveryOption?: SelectedDeliveryOption;
        LoyaltyCardId?: string;
        SubtotalWithCurrency?: string;
        Discount?: string;
        ChargeAmountWithCurrency?: string;
        TaxAmountWithCurrency?: string;
        TotalAmountWithCurrency?: string;
        TotalAmount?: number;
        ShippingAddress?: Address;
        DeliveryModeId?: string;
        DeliveryPreferences?: DeliveryPreference[];
    }
    interface ShoppingCartResponse {
        ShoppingCart?: ShoppingCart;
        Errors?: Error[];
    }
    interface ShoppingCartCollectionResponse {
        ShoppingCarts?: ShoppingCart[];
        Errors?: Error[];
    }
    interface ShippingOption {
        Id?: string;
        ShippingType?: string;
        Description?: string;
    }
    interface ShippingOptions {
        ShippingOptions?: ShippingOption[];
    }
    interface ItemShippingOptions {
        LineId?: string;
        ShippingOptions?: ShippingOptions;
    }
    interface ShippingOptionResponse {
        OrderShippingOptions?: ShippingOptions;
        ItemShippingOptions?: ItemShippingOptions[];
        Errors?: Error[];
    }
    interface DeliveryOption {
        Id?: string;
        Description?: string;
    }
    interface LineDeliveryOption {
        LineId?: string;
        DeliveryOptions?: DeliveryOption[];
    }
    interface DeliveryOptionsResponse {
        DeliveryOptions?: DeliveryOption[];
        LineDeliveryOptions?: LineDeliveryOption[];
        Errors?: Error[];
    }
    interface PaymentCardType {
        Id?: string;
        CardType?: string;
    }
    interface PaymentCardTypesResponse {
        CardTypes?: PaymentCardType[];
        Errors?: Error[];
    }
    interface Address {
        Country?: string;
        State?: string;
        County?: string;
        City?: string;
        DistrictName?: string;
        AttentionTo?: string;
        Name?: string;
        Street?: string;
        StreetNumber?: string;
        ZipCode?: string;
        Phone?: string;
        Email?: string;
        EmailContent?: string;
        RecordId?: number;
        Deactivate?: boolean;
        IsPrimary?: boolean;
        AddressType?: AddressType;
        AddressFriendlyName?: string;
    }
    interface AddressCollectionResponse {
        Addresses?: Address[];
        Errors?: Error[];
    }
    interface Payment {
        PaymentAddress?: Address;
        CardNumber?: string;
        CardType?: string;
        CCID?: string;
        ExpirationMonth?: number;
        ExpirationYear?: number;
        NameOnCard?: string;
    }
    interface TokenizedPaymentCard {
        PaymentAddress?: Address;
        CardType?: string;
        ExpirationMonth?: number;
        ExpirationYear?: number;
        NameOnCard?: string;
        CardToken?: string;
        UniqueCardId?: string;
        MaskedCardNumber?: string;
    }
    interface Error {
        ErrorCode?: string;
        ErrorMessage?: string;
    }
    interface CreateSalesOrderResponse {
        OrderNumber?: string;
        Errors?: Error[];
    }
    interface StoreProductAvailabilityItem {
        RecordId?: number;
        ItemId?: string;
        VariantInventoryDimensionId?: string;
        WarehouseInventoryDimensionId?: string;
        InventoryLocationId?: string;
        AvailableQuantity?: number;
        ProductDetails?: string;
    }
    interface StoreProductAvailability {
        ChannelId?: number;
        Latitude?: number;
        Longitude?: number;
        Distance?: string;
        InventoryLocationId?: string;
        StoreId?: string;
        StoreName?: string;
        PostalAddressId?: string;
        Country?: string;
        State?: string;
        County?: string;
        City?: string;
        DistrictName?: string;
        AttentionTo?: string;
        Name?: string;
        Street?: string;
        StreetNumber?: string;
        ZipCode?: string;
        Phone?: string;
        Email?: string;
        EmailContent?: string;
        RecordId?: number;
        Deactivate?: boolean;
        IsPrimary?: boolean;
        AddressType?: AddressType;
        AddressFriendlyName?: string;
        ProductAvailabilities?: StoreProductAvailabilityItem[];
        SelectDisabled?: string;
        AreItemsAvailableInStore?: boolean;
    }
    interface CountryInfo {
        CountryCode?: string;
        CountryName?: string;
    }
    interface ChannelConfiguration {
        GiftCardItemId?: string;
        PickupDeliveryModeCode?: string;
        EmailDeliveryModeCode?: string;
        BingMapsApiKey?: string;
        CurrencyStringTemplate?: string;
    }
    interface StoreLocation {
        ChannelId?: number;
        Latitude?: number;
        Longitude?: number;
        Distance?: string;
        InventoryLocationId?: string;
        StoreId?: string;
        StoreName?: string;
        PostalAddressId?: string;
        Country?: string;
        State?: string;
        County?: string;
        City?: string;
        DistrictName?: string;
        AttentionTo?: string;
        Name?: string;
        Street?: string;
        StreetNumber?: string;
        ZipCode?: string;
        Phone?: string;
        Email?: string;
        EmailContent?: string;
        RecordId?: number;
        Deactivate?: boolean;
        IsPrimary?: boolean;
        AddressType?: AddressType;
        AddressFriendlyName?: string;
    }
    interface TenderDataLine {
        Amount?: string;
        GiftCardId?: string;
        LoyaltyCardId?: string;
        PaymentCard?: Payment;
    }
    interface LineDeliveryPreference {
        LineId?: string;
        DeliveryPreferenceTypes?: DeliveryPreferenceType[];
    }
    interface CartDeliveryPreferences {
        HeaderDeliveryPreferenceTypes?: DeliveryPreferenceType[];
        LineDeliveryPreferences?: LineDeliveryPreference[];
    }
    interface DeliveryPreferenceResponse {
        CartDeliveryPreferences?: CartDeliveryPreferences;
        Errors?: Error[];
    }
    interface BooleanResponse {
        IsTrue?: boolean;
        Errors?: Error[];
    }
    interface StringResponse {
        Value?: string;
        Errors?: Error[];
    }
    interface StoreProductAvailabilityResponse {
        Stores?: StoreProductAvailability[];
        Errors?: Error[];
    }
    interface StoreLocationResponse {
        Stores?: StoreLocation[];
        Errors?: Error[];
    }
    interface ChannelInfoResponse {
        ChannelConfiguration?: ChannelConfiguration;
        Errors?: Error[];
    }
    interface TenderTypesResponse {
        HasCreditCardPayment?: boolean;
        HasGiftCardPayment?: boolean;
        HasLoyaltyCardPayment?: boolean;
        Errors?: Error[];
    }
    interface CountryInfoResponse {
        Countries?: CountryInfo[];
        Errors?: Error[];
    }
    interface GiftCardInformation {
        GiftCardId?: string;
        Balance?: number;
        BalanceWithCurrency?: string;
        CurrencyCode?: string;
        IsInfoAvailable?: boolean;
    }
    interface GiftCardResponse {
        GiftCardInformation?: GiftCardInformation;
        Errors?: Error[];
    }
    interface LoyaltyCard {
        CardNumber?: string;
        CardTenderType?: number;
    }
    interface LoyaltyCardsResponse {
        LoyaltyCards?: LoyaltyCard[];
        Errors?: Error[];
    }
    interface StateProvinceInfo {
        CountryRegionId?: string;
        StateId?: string;
        StateName?: string;
    }
    interface StateProvinceInfoResponse {
        StateProvinces?: StateProvinceInfo[];
        Errors?: Error[];
    }
    class CartProductDetailsClass implements CartProductDetails {
        public Name: string;
        public ProductUrl: string;
        public ProductNumber: string;
        public ImageUrl: string;
        public Description: string;
        public DimensionValues: string;
        public SKU: string;
        public ImageMarkup: string;
        public Quantity: number;
        constructor(o: any);
    }
    class SelectedLineDeliveryOptionClass implements SelectedLineDeliveryOption {
        public LineId: string;
        public DeliveryModeId: string;
        public DeliveryModeText: string;
        public DeliveryPreferenceId: string;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: string;
        public ElectronicDeliveryEmailContent: string;
        constructor(o: any);
    }
    class SelectedDeliveryOptionClass implements SelectedDeliveryOption {
        public DeliveryModeId: string;
        public DeliveryModeText: string;
        public DeliveryPreferenceId: string;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: string;
        public ElectronicDeliveryEmailContent: string;
        constructor(o: any);
    }
    class ImageInfoClass implements ImageInfo {
        public Url: string;
        public AltText: string;
        constructor(o: any);
    }
    class TransactionItemClass implements TransactionItem {
        public LineId: string;
        public ItemType: TransactionItemType;
        public KitComponents: TransactionItemClass[];
        public SelectedDeliveryOption: SelectedDeliveryOption;
        public ProductId: number;
        public ProductNumber: string;
        public ItemId: string;
        public VariantInventoryDimensionId: string;
        public Quantity: number;
        public PriceWithCurrency: string;
        public TaxAmountWithCurrency: string;
        public DiscountAmount: number;
        public DiscountAmountWithCurrency: string;
        public NetAmountWithCurrency: string;
        public ShippingAddress: Address;
        public DeliveryModeId: string;
        public DeliveryModeText: string;
        public ElectronicDeliveryEmail: string;
        public PromotionLines: string[];
        public ProductDetailsExpanded: CartProductDetails;
        public ProductDetails: string;
        public NoOfComponents: string;
        public Color: string;
        public Size: string;
        public Style: string;
        public Name: string;
        public Description: string;
        public ProductUrl: string;
        public Image: ImageInfo;
        public ImageMarkup: string;
        public OfferNames: string;
        public DeliveryPreferences: DeliveryPreference[];
        constructor(o: any);
    }
    class ShoppingCartClass implements ShoppingCart {
        public CartId: string;
        public Name: string;
        public Items: TransactionItem[];
        public LastModifiedDate: Date;
        public CartType: ShoppingCartType;
        public PromotionLines: string[];
        public DiscountCodes: string[];
        public SelectedDeliveryOption: SelectedDeliveryOption;
        public LoyaltyCardId: string;
        public SubtotalWithCurrency: string;
        public DiscountAmountWithCurrency: string;
        public ChargeAmountWithCurrency: string;
        public TaxAmountWithCurrency: string;
        public TotalAmountWithCurrency: string;
        public TotalAmount: number;
        public ShippingAddress: Address;
        public DeliveryModeId: string;
        constructor(o: any);
    }
    class ShoppingCartResponseClass implements ShoppingCartResponse {
        public ShoppingCart: ShoppingCart;
        public Errors: Error[];
        constructor(o: any);
    }
    class ShoppingCartCollectionResponseClass implements ShoppingCartCollectionResponse {
        public ShoppingCarts: ShoppingCart[];
        public Errors: Error[];
        constructor(o: any);
    }
    class ShippingOptionClass implements ShippingOption {
        public Id: string;
        public ShippingType: string;
        public Description: string;
        constructor(o: any);
    }
    class ShippingOptionsClass implements ShippingOptions {
        public ShippingOptions: ShippingOption[];
        constructor(o: any);
    }
    class ItemShippingOptionsClass implements ItemShippingOptions {
        public LineId: string;
        public ShippingOptions: ShippingOptions;
        constructor(o: any);
    }
    class ShippingOptionResponseClass implements ShippingOptionResponse {
        public OrderShippingOptions: ShippingOptions;
        public ItemShippingOptions: ItemShippingOptions[];
        public Errors: Error[];
        constructor(o: any);
    }
    class LineDeliveryPreferenceClass implements LineDeliveryPreference {
        public LineId: string;
        public DeliveryPreferenceTypes: DeliveryPreferenceType[];
        constructor(o: any);
    }
    class CartDeliveryPreferencesClass implements CartDeliveryPreferences {
        public HeaderDeliveryPreferenceTypes: DeliveryPreferenceType[];
        public LineDeliveryPreferences: LineDeliveryPreference[];
        constructor(o: any);
    }
    class DeliveryOptionClass implements DeliveryOption {
        public Id: string;
        public Description: string;
        constructor(o: any);
    }
    class LineDeliveryOptionClass implements LineDeliveryOption {
        public LineId: string;
        public DeliveryOptions: DeliveryOption[];
        constructor(o: any);
    }
    class DeliveryOptionsResponseClass implements DeliveryOptionsResponse {
        public DeliveryOptions: DeliveryOption[];
        public LineDeliveryOptions: LineDeliveryOption[];
        public Errors: Error[];
        constructor(o: any);
    }
    class PaymentCardTypeClass implements PaymentCardType {
        public Id: string;
        public CardType: string;
        constructor(o: any);
    }
    class PaymentCardTypesResponseClass implements PaymentCardTypesResponse {
        public CardTypes: PaymentCardType[];
        public Errors: Error[];
        constructor(o: any);
    }
    class AddressClass implements Address {
        public Country: string;
        public State: string;
        public County: string;
        public City: string;
        public DistrictName: string;
        public AttentionTo: string;
        public Name: string;
        public Street: string;
        public StreetNumber: string;
        public ZipCode: string;
        public Phone: string;
        public Email: string;
        public EmailContent: string;
        public RecordId: number;
        public Deactivate: boolean;
        public IsPrimary: boolean;
        public AddressType: AddressType;
        public AddressFriendlyName: string;
        constructor(o: any);
    }
    class AddressCollectionResponseClass implements AddressCollectionResponse {
        public Addresses: Address[];
        public Errors: Error[];
        constructor(o: any);
    }
    class PaymentClass implements Payment {
        public PaymentAddress: Address;
        public CardNumber: string;
        public CardType: string;
        public CCID: string;
        public ExpirationMonth: number;
        public ExpirationYear: number;
        public NameOnCard: string;
        public LoyaltyCardId: string;
        public DiscountCode: string;
        constructor(o: any);
    }
    class TokenizedPaymentCardClass implements TokenizedPaymentCard {
        public PaymentAddress: Address;
        public CardType: string;
        public ExpirationMonth: number;
        public ExpirationYear: number;
        public NameOnCard: string;
        public CardToken: string;
        public UniqueCardId: string;
        public MaskedCardNumber: string;
        constructor(o: any);
    }
    class ErrorClass implements Error {
        public ErrorCode: string;
        public ErrorMessage: string;
        constructor(o: any);
    }
    class CreateSalesOrderResponseClass implements CreateSalesOrderResponse {
        public OrderNumber: string;
        public Errors: Error[];
        constructor(o: any);
    }
    class StoreProductAvailabilityItemClass implements StoreProductAvailabilityItem {
        public RecordId: number;
        public ItemId: string;
        public VariantInventoryDimensionId: string;
        public WarehouseInventoryDimensionId: string;
        public InventoryLocationId: string;
        public AvailableQuantity: number;
        public ProductDetails: string;
        constructor(o: any);
    }
    class StoreProductAvailabilityClass implements StoreProductAvailability {
        public ChannelId: number;
        public Latitude: number;
        public Longitude: number;
        public Distance: string;
        public InventoryLocationId: string;
        public StoreId: string;
        public StoreName: string;
        public PostalAddressId: string;
        public ProductAvailabilities: StoreProductAvailabilityItem[];
        public SelectDisabled: string;
        public Country: string;
        public State: string;
        public County: string;
        public City: string;
        public DistrictName: string;
        public AttentionTo: string;
        public Name: string;
        public Street: string;
        public StreetNumber: string;
        public ZipCode: string;
        public Phone: string;
        public Email: string;
        public EmailContent: string;
        public RecordId: number;
        public Deactivate: boolean;
        public IsPrimary: boolean;
        public AddressType: AddressType;
        public AddressFriendlyName: string;
        public AreItemsAvailableInStore: boolean;
        constructor(o: any);
    }
    class StoreLocationClass implements StoreLocation {
        public ChannelId: number;
        public Latitude: number;
        public Longitude: number;
        public Distance: string;
        public InventoryLocationId: string;
        public StoreId: string;
        public StoreName: string;
        public PostalAddressId: string;
        public Country: string;
        public State: string;
        public County: string;
        public City: string;
        public DistrictName: string;
        public AttentionTo: string;
        public Name: string;
        public Street: string;
        public StreetNumber: string;
        public ZipCode: string;
        public Phone: string;
        public Email: string;
        public EmailContent: string;
        public RecordId: number;
        public Deactivate: boolean;
        public IsPrimary: boolean;
        public AddressType: AddressType;
        public AddressFriendlyName: string;
        constructor(o: any);
    }
    class CountryInfoClass implements CountryInfo {
        public CountryCode: string;
        public CountryName: string;
        constructor(o: any);
    }
    class TenderDataLineClass implements TenderDataLine {
        public Amount: string;
        public GiftCardId: string;
        public LoyaltyCardId: string;
        public PaymentCard: Payment;
        constructor(o: any);
    }
    class DeliveryPreferenceResponseClass implements DeliveryPreferenceResponse {
        public CartDeliveryPreferences: CartDeliveryPreferences;
        public Errors: Error[];
        constructor(o: any);
    }
    class StoreProductAvailabilityResponseClass implements StoreProductAvailabilityResponse {
        public Stores: StoreProductAvailability[];
        public Errors: Error[];
        constructor(o: any);
    }
    class StoreLocationResponseClass implements StoreLocationResponse {
        public Stores: StoreLocation[];
        public Errors: Error[];
        constructor(o: any);
    }
    class CountryInfoResponseClass implements CountryInfoResponse {
        public Countries: CountryInfo[];
        public Errors: Error[];
        constructor(o: any);
    }
    class BooleanResponseClass implements BooleanResponse {
        public IsTrue: boolean;
        public Errors: Error[];
        constructor(o: any);
    }
    class StringResponseClass implements StringResponse {
        public Value: string;
        public Errors: Error[];
        constructor(o: any);
    }
    class TenderTypesResponseClass implements TenderTypesResponse {
        public HasCreditCardPayment: boolean;
        public HasGiftCardPayment: boolean;
        public HasLoyaltyCardPayment: boolean;
        public Errors: Error[];
        constructor(o: any);
    }
    class GiftCardInformationClass implements GiftCardInformation {
        public GiftCardId: string;
        public Balance: number;
        public CurrencyCode: string;
        public IsInfoAvailable: boolean;
        constructor(o: any);
    }
    class GiftCardResponseClass implements GiftCardResponse {
        public GiftCardInformation: GiftCardInformation;
        public Errors: Error[];
        constructor(o: any);
    }
    class LoyaltyCardClass implements LoyaltyCard {
        public CardNumber: string;
        public CardTenderType: number;
        constructor(o: any);
    }
    class LoyaltyCardsResponseClass implements LoyaltyCardsResponse {
        public LoyaltyCards: LoyaltyCard[];
        public Errors: Error[];
        constructor(o: any);
    }
    class StateProvinceInfoClass implements StateProvinceInfo {
        public CountryRegionId: string;
        public StateId: string;
        public StateName: string;
        constructor(o: any);
    }
    class StateProvinceInfoResponseClass implements StateProvinceInfoResponse {
        public StateProvinces: StateProvinceInfo[];
        public Errors: Error[];
        constructor(o: any);
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    var ResourceStrings: {};
    var Resources: any;
    class ResourcesHandler {
        static selectUICulture(): void;
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class MiniCart {
        private _cartView;
        private _miniCart;
        private cart;
        private isShoppingCartEnabled;
        private errorMessage;
        private errorPanel;
        private isCheckoutCart;
        private isMiniCartVisible;
        constructor(element: any);
        private getResx(key);
        private shoppingCartNextClick(viewModel, event);
        private disableUserActions();
        private enableUserActions();
        private showError(isError);
        private viewCartClick();
        private showMiniCart();
        private hideMiniCart();
        private toggleCartDisplay(show);
        private updateCart(event, data);
        private getShoppingCart();
        private removeFromCartClick(item);
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class Cart {
        private _cartView;
        private _editRewardCardDialog;
        private _loadingDialog;
        private _loadingText;
        private cart;
        private isShoppingCartLoaded;
        private isShoppingCartEnabled;
        private isPromotionCodesEnabled;
        private kitCartItemTypeValue;
        private errorMessage;
        private errorPanel;
        private dialogOverlay;
        private supportDiscountCodes;
        private supportLoyaltyReward;
        private displayPromotionBanner;
        constructor(element: any);
        private getResx(key);
        private shoppingCartNextClick(viewModel, event);
        private quantityMinusClick(item);
        private quantityPlusClick(item);
        private quantityTextBoxChanged(item, valueAccesor);
        private showError(isError);
        private editRewardCardOverlayClick();
        private createEditRewardCardDialog();
        private showEditRewardCardDialog();
        private closeEditRewardCardDialog();
        private continueShoppingClick();
        private updateShoppingCart(event, data);
        private getShoppingCart();
        private getPromotions();
        private removeFromCartClick(item);
        private updateQuantity(items);
        private applyPromotionCode(cart, valueAccesor);
        private removePromotionCode(cart, valueAccesor);
        private updateLoyaltyCardId();
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class Checkout {
        private _checkoutView;
        private _loadingDialog;
        private _loadingText;
        private cart;
        private isShoppingCartEnabled;
        private kitCartItemTypeValue;
        public errorMessage: any;
        public errorPanel: any;
        private nextButton;
        private _checkoutFragments;
        private countries;
        private allDeliveryOptionDescriptions;
        private dialogOverlay;
        private deliveryPreferences;
        private selectedOrderDeliveryPreference;
        private pickUpInStoreDeliveryModeCode;
        private emailDeliveryModeCode;
        private giftCardItemId;
        private states;
        private tempShippingAddress;
        private _deliveryPreferencesView;
        private _availableStoresView;
        private _location;
        private addresses;
        private selectedAddress;
        private itemSelectedAddress;
        private availableDeliveryOptions;
        private orderDeliveryOption;
        private sendEmailToMe;
        private isAuthenticated;
        private email;
        private _emailAddressTextBox;
        private bingMapsToken;
        private map;
        private mapStoreLocator;
        private DisableTouchInputOnMap;
        private searchLocation;
        private stores;
        private displayLocations;
        private deliveryPreferenceToValidate;
        private itemSelectedDeliveryPreference;
        private showItemDeliveryPreferenceDialog;
        private _itemLevelDeliveryPreferenceSelection;
        private itemDeliveryPreferenceToValidate;
        private item;
        private selectedLineDeliveryOption;
        private hasShipToAddress;
        private hasPickUpInStore;
        private hasEmail;
        private hasMultiDeliveryPreference;
        private anyGiftCard;
        private hasInventoryCheck;
        private cartDeliveryPreferences;
        private _deliveryPreferencesFragments;
        private _itemDeliveryPreferencesFragments;
        private _paymentView;
        private _addDiscountCodeDialog;
        private useShippingAddress;
        private isBillingAddressSameAsShippingAddress;
        private paymentCardTypes;
        private tenderLines;
        private channelCurrencyTemplate;
        private maskedCreditCard;
        private expirtationMonths;
        private confirmEmailValue;
        private paymentTotal;
        private payCreditCard;
        private payGiftCard;
        private payLoyaltyCard;
        private regExForNumber;
        private _creditCardPanel;
        private _giftCardPanel;
        private _loyaltyCardPanel;
        private creditCardAmount;
        private paymentCard;
        private giftCardAmount;
        private giftCardNumber;
        private giftCardBalance;
        private isGiftCardInfoAvailable;
        private checkGiftCardAmountValidity;
        private loyaltyCards;
        private loyaltyCardAmount;
        private loyaltyCardNumber;
        private totalAmount;
        private expirtationYear;
        private _editRewardCardDialog;
        private isPromotionCodesEnabled;
        private creditCardAmt;
        private giftCardAmt;
        private loyaltyCardAmt;
        private emailDelivery;
        private orderLevelDelivery;
        private displayPromotionBanner;
        private orderNumber;
        constructor(element: any);
        private loadXMLDoc(filename);
        private autoFillCheckout();
        private showCheckoutFragment(fragmentCssClass);
        private showDeliveryPreferenceFragment(fragmentCssClass);
        private showItemDeliveryPreferenceFragment(fragmentCssClass);
        private validateItemDeliveryInformation();
        private validateDeliveryInformation($shippingOptions);
        private deliveryPreferencesNextClick();
        private paymentInformationPreviousClick();
        private validateConfirmEmailTextBox(srcElement);
        private paymentInformationNextClick();
        private reviewPreviousClick();
        private quantityMinusClick(item);
        private quantityPlusClick(item);
        private quantityTextBoxChanged(item, valueAccesor);
        private selectOrderDeliveryOption(selectedDeliveryOption);
        private resetSelectedOrderShippingOptions();
        private showError(isError);
        private getResx(key);
        private getDeliveryModeText(deliveryModeId);
        private updateSelectedShippingOptions(cart);
        private getItemDeliveryModeDescription(item);
        private getMap();
        private getNearbyStoresWithAvailability();
        private geocodeCallback(geocodeResult, userData);
        private geocodeErrorCallback(geocodeRequest);
        private renderAvailableStores();
        private displayStores(displayLocations);
        private selectStore(location);
        private updateItemAvailibilities();
        private editRewardCardOverlayClick();
        private createEditRewardCardDialog();
        private showEditRewardCardDialog();
        private closeEditRewardCardDialog();
        private discountCodeOverlayClick();
        private createDiscountCodeDialog();
        private showDiscountCodeDialog();
        private closeDiscountCodeDialog();
        private itemDeliveryPreferenceSelectionOverlayClick();
        private createItemDeliveryPreferenceDialog();
        private showItemDeliveryPreferenceSelection(parent);
        private static getDeliveryPreferencesForLine(lineId, cartDeliveryPreferences);
        private paymentCountryUpdate(srcElement);
        private resetOrderAvailableDeliveryMethods(srcElement);
        private resetItemAvailableDeliveryMethods(srcElement);
        private closeItemDeliveryPreferenceSelection();
        private setItemDeliveryPreferenceSelection();
        private findLocationKeyPress(data, event);
        private removeValidation(element);
        private addValidation(element);
        private updateCustomLoyaltyValidation();
        private checkForGiftCardInCart();
        private updateDeliveryPreferences();
        private showPaymentPanel(data, valueAccessor);
        private hidePaymentPanel(data, valueAccessor);
        private updatePaymentTotal();
        private validatePayments();
        private createPaymentCardTenderLine();
        private updateCheckoutCart(event, data);
        private static isRequestedDeliveryPreferenceApplicable(deliveryPreferenceTypes, reqDeliveryPreferenceType);
        private getShoppingCart();
        private commenceCheckout();
        private getPromotions();
        private getAllDeliveryOptionDescriptions();
        private getChannelConfigurationAndTenderTypes();
        private getDeliveryPreferences();
        private removeFromCartClick(item);
        private updateQuantity(items);
        private applyPromotionCode(cart, valueAccesor);
        private removePromotionCode(cart, valueAccesor);
        private getOrderDeliveryOptions();
        private getItemDeliveryOptions();
        private getNearbyStoresWithAvailabilityService();
        private getNearbyStoresService();
        private getCountryRegionInfoService();
        private getStateProvinceInfoService(countryCode);
        private isAuthenticatedSession();
        private getUserEmail();
        private getUserAddresses();
        private setDeliveryOptions();
        private setItemShippingOptions();
        private getPaymentCardTypes();
        private getLoyaltyCards();
        private updateLoyaltyCardId();
        private getTenderTypes();
        private getGiftCardBalance();
        private applyFullGiftCardAmount();
        private submitOrder();
    }
}
declare module Microsoft {
    class Utils {
        private static isLocalizationDataLoaded;
        private static currentUiCulture;
        private static defaultUiCulture;
        private static uiCultureCookieName;
        static isNullOrUndefined(o: any): boolean;
        static isNullOrEmpty(o: any): boolean;
        static isNullOrWhiteSpace(o: string): boolean;
        static hasElements(o: any[]): boolean;
        static getValueOrDefault(o: string, defaultValue: any): string;
        static hasErrors(o: any): boolean;
        static format(object: string, ...params: any[]): string;
        static parseNumberFromLocaleString(localizedNumberString: string): number;
        static getDecimalOperatorForUiCulture(): string;
        static formatNumber(numberValue: number): string;
        static getCurrentUiCulture(): string;
        static getCookieValue(cookieName: string): string;
        static clone(origObject: any): any;
        private static safeClone(origObject, cloneMap);
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    interface IAsyncResult<T> {
        done(callback: (result: T) => void): IAsyncResult<T>;
        fail(callback: (error: any[]) => void): IAsyncResult<T>;
    }
    class AsyncResult<T> implements IAsyncResult<T> {
        private _result;
        private _errors;
        private _succeded;
        private _failed;
        private _callerContext;
        private _successCallbacks;
        private _errorCallbacks;
        constructor();
        public resolve(result: T): void;
        public reject(errors: any[]): void;
        public done(callback: (result: T) => void): IAsyncResult<T>;
        public fail(callback: (errors: any[]) => void): IAsyncResult<T>;
    }
    class FunctionQueueHelper {
        static callFunctions(functionQueue: Function[], callerContext: any, data?: any): void;
        static queueFunction(functionQueue: Function[], callback: Function): void;
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class ChannelService {
        private static proxy;
        static GetProxy(): void;
        static GetChannelConfiguration(): AsyncResult<ChannelInfoResponse>;
        static GetTenderTypes(): AsyncResult<TenderTypesResponse>;
        static GetCountryRegionInfo(): AsyncResult<CountryInfoResponse>;
        static GetStateProvinceInfo(countryCode: string): AsyncResult<StateProvinceInfoResponse>;
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class CustomerService {
        private static proxy;
        static GetProxy(): void;
        static GetAddresses(): AsyncResult<AddressCollectionResponse>;
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class LoyaltyService {
        private static proxy;
        static GetProxy(): void;
        static GetLoyaltyCards(): AsyncResult<LoyaltyCardsResponse>;
        static GetAllLoyaltyCardsStatus(): void;
        static GetLoyaltyCardStatus(loyaltyCardNumber: string): void;
        static UpdateLoyaltyCardId(isCheckoutSession: boolean, loyaltyCardId: string, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class StoreProductAvailabilityService {
        private static proxy;
        static GetProxy(): void;
        static GetNearbyStoresWithAvailability(latitude: number, longitude: number, items: TransactionItem[]): AsyncResult<StoreProductAvailabilityResponse>;
        static GetNearbyStores(latitude: number, longitude: number): AsyncResult<StoreLocationResponse>;
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    interface IFieldValidatorParams {
        maxLength?: number;
        max?: number;
        min?: number;
        pattern?: string;
        required?: boolean;
        title?: string;
    }
    class FieldValidator {
        private _validationAttributes;
        constructor(params: IFieldValidatorParams);
        public setValidationAttributes(element: Element): void;
        public setTitleAttributeIfInvalid(element: Element): void;
    }
    class EntityValidatorBase {
        constructor();
        public setValidationAttributes(element: Element, fieldName: string): void;
    }
    class ShoppingCartItemValidator extends EntityValidatorBase {
        public Quantity: IFieldValidatorParams;
        constructor();
    }
    class SelectedOrderDeliveryOptionValidator extends EntityValidatorBase {
        public DeliveryModeId: IFieldValidatorParams;
        public DeliveryModeText: IFieldValidatorParams;
        public DeliveryPreferenceId: IFieldValidatorParams;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: IFieldValidatorParams;
        public ElectronicDeliveryEmailContent: IFieldValidatorParams;
        constructor();
    }
    class CustomerValidator extends EntityValidatorBase {
        public FirstName: IFieldValidatorParams;
        public MiddleName: IFieldValidatorParams;
        public LastName: IFieldValidatorParams;
        public Name: IFieldValidatorParams;
        constructor();
    }
    class AddressValidator extends EntityValidatorBase {
        public Phone: any;
        public Url: any;
        public Email: any;
        public Name: any;
        public StreetNumber: any;
        public Street: any;
        public City: any;
        public ZipCode: any;
        public State: any;
        public Country: any;
        constructor();
    }
    class PaymentCardTypeValidator extends EntityValidatorBase {
        public NameOnCard: any;
        public CardNumber: any;
        public CCID: any;
        public PaymentAmount: any;
        public ExpirationMonth: any;
        public ExpirationYear: any;
        constructor();
    }
    class GiftCardTypeValidator extends EntityValidatorBase {
        public CardNumber: any;
        public PaymentAmount: any;
        constructor();
    }
    class LoyaltyCardTypeValidator extends EntityValidatorBase {
        public CardNumber: any;
        public PaymentAmount: any;
        constructor();
    }
    class DiscountCardTypeValidator extends EntityValidatorBase {
        public CardNumber: any;
        constructor();
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class ShoppingCartService {
        private static proxy;
        static GetProxy(): void;
        static UpdateShoppingCart(shoppingCartResponse: ShoppingCartResponse, isCheckoutSession: boolean): void;
        static GetDimensionValues(color: string, size: string, style: string): any;
        static OnUpdateShoppingCart(callerContext: any, handler: any): void;
        static OnUpdateCheckoutCart(callerContext: any, handler: any): void;
        static GetShoppingCart(isCheckoutSession: boolean, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static RemoveFromCart(isCheckoutSession: boolean, lineId: string, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static UpdateQuantity(isCheckoutSession: boolean, items: TransactionItem[], shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static GetPromotions(isCheckoutSession: boolean, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static AddOrRemovePromotion(isCheckoutSession: boolean, promoCode: string, isAdd: boolean, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static CommenceCheckout(dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        private static GetNoImageMarkup();
        private static BuildImageMarkup(imageData, width, height);
        private static BuildImageMarkup50x50(imageData);
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    class CheckoutService {
        private static proxy;
        static GetProxy(): void;
        static SubmitOrder(tenderLineData: TenderDataLine[], email: string): AsyncResult<CreateSalesOrderResponse>;
        static GetDeliveryPreferences(): AsyncResult<DeliveryPreferenceResponse>;
        static GetDeliveryOptionsInfo(): AsyncResult<DeliveryOptionsResponse>;
        static GetOrderDeliveryOptionsForShipping(shipToAddress: Address): AsyncResult<DeliveryOptionsResponse>;
        static GetLineDeliveryOptionsForShipping(selectedLineShippingInfo: SelectedLineShippingInfo[]): AsyncResult<DeliveryOptionsResponse>;
        static SetOrderDeliveryOption(selectedDeliveryOption: SelectedDeliveryOption, dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static SetLineDeliveryOptions(lineDeliveryOptions: SelectedLineDeliveryOption[], dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static GetPaymentCardTypes(): AsyncResult<PaymentCardTypesResponse>;
        static IsAuthenticatedSession(): AsyncResult<BooleanResponse>;
        static GetUserEmail(): AsyncResult<StringResponse>;
        static GetGiftCardBalance(giftCardNumber: string): AsyncResult<GiftCardResponse>;
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    interface IValidatorOptions {
        data?: any;
        field: string;
        validatorType: string;
        validatorField?: string;
        validate?: (element: Element) => {};
    }
    interface ISubmitIfValidOptions {
        containerSelector: string;
        submit: (eventObject: JQueryEventObject) => {};
        validate?: (element: Element) => {};
    }
}
declare module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    interface IResx {
        textContent?: string;
        label?: string;
    }
}
