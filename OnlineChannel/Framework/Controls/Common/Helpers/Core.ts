﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../../ExternalScripts/JQuery.d.ts" />
/// <reference path="../../ExternalScripts/KnockoutJS.d.ts" />

// Initialize page
$(document).ready(() => {
    // Calling selectUICulture() in order to read the UICulture value from the cookie
    // and pick the corresponding resource translations for the string in Checkout control.
    Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.ResourcesHandler.selectUICulture();
    $('.msax-Control').each((index, element) => {
        var viewModelName = $(element.firstElementChild).attr("data-model");
        var viewModel = eval(viewModelName);
        ko.applyBindings(new viewModel(element.firstElementChild), element);
    });   
});

// Show errors
var msaxError = {
    show: function (level, message, errorCodes?) {
        console.error(message)
    }
};

/* This module is to simulate the existence of the variables that are being registered from the server side */
module msaxValues {
    export var msax_CheckoutServiceUrl;
    export var msax_ShoppingCartServiceUrl;
    export var msax_OrderConfirmationUrl;
    export var msax_IsDemoMode;
    export var msax_DemoDataPath;
    export var msax_StoreProductAvailabilityServiceUrl;
    export var msax_ChannelServiceUrl;
    export var msax_LoyaltyServiceUrl;
    export var msax_CustomerServiceUrl;
    export var msax_HasInventoryCheck;
    export var msax_CheckoutUrl;
    export var msax_IsCheckoutCart;
    export var msax_ContinueShoppingUrl;
    export var msax_CartDiscountCodes;
    export var msax_CartLoyaltyReward;
    export var msax_ShoppingCartUrl;
    export var msax_CartDisplayPromotionBanner;
    export var msax_ReviewDisplayPromotionBanner;
}

/* This module is to simulate the existence of Bing maps APIs */
module Microsoft.Maps {
    export var Map;
    export var loadModule;
    export var Search;
    export var Events;
    export var Pushpin;
    export var Infobox;
    export var Point;
}

module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls { 

    export class AjaxProxy {
        private relativeUrl;

        constructor(relativeUrl) {
            this.relativeUrl = relativeUrl;
            $(document).ajaxError(this.ajaxErrorHandler);
        }
        

        private ajaxErrorHandler(e, xhr, settings) {
            var errorMessage =
                'Url:\n' + settings.url +
                '\n\n' +
                'Response code:\n' + xhr.status +
                '\n\n' +
                'Status Text:\n' + xhr.statusText +
                '\n\n' +
                'Response Text: \n' + xhr.responseText;

            msaxError.show('error', 'The web service call was unsuccessful.  Details: ' + errorMessage);
        }

        public SubmitRequest = function (webMethod, data, successCallback, errorCallback) {

            // Example: http://www.contoso.com:40002/sites/retailpublishingportal + /_vti_bin/ShoppingCartService.svc/ + GetShoppingCart
            var webServiceUrl = this.relativeUrl + webMethod;

            var requestDigestHeader = (<HTMLInputElement>($(document).find('#__REQUESTDIGEST'))[0]);
            var retailRequestDigestHeader = (<HTMLInputElement>($(document).find('#__RETAILREQUESTDIGEST'))[0]);
            var requestDigestHeaderValue;
            var retailRequestDigestHeaderValue;

            if (Utils.isNullOrUndefined(requestDigestHeader) || Utils.isNullOrUndefined(retailRequestDigestHeader)) {
                requestDigestHeaderValue = null;
                retailRequestDigestHeaderValue = null;
            }
            else {
                requestDigestHeaderValue = requestDigestHeader.value;
                retailRequestDigestHeaderValue = retailRequestDigestHeader.value;
            }

            // Submit the AJAX call using jQuery.
            $.ajax({
                url: webServiceUrl,
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.RedirectUrl) {
                        window.location = data.RedirectUrl;
                    }
                    else {
                        successCallback(data);
                    }
                },
                error: function (error) {
                    errorCallback(error);
                },
                headers: {
                    "X-RequestDigest": requestDigestHeaderValue,
                    "X-RetailRequestDigest": retailRequestDigestHeaderValue
                }
            });
        };
    }

    export class LoadingOverlay {

        private static existingOverlayClasses: string[] = [];

        public static CreateLoadingDialog(loadingDialog: any, width: number, height: number) {
            // Creates the loading overlay dialog box.
            loadingDialog.dialog({
                modal: true,
                autoOpen: false,
                draggable: true,
                resizable: false,
                position: ['top', 60],
                show: { effect: "fadeIn", duration: 500 },
                hide: { effect: "fadeOut", duration: 500 },
                open: function (event, ui) {
                    setTimeout(function () {
                        loadingDialog.dialog('close');
                    }, 5000); // If the service call does not return (does not fail or succeed) for some reason then we close the dialog on a timeout.
                },
                width: width,
                height: height,
                dialogClass: 'msax-Control msax-LoadingOverlay msax-NoTitle'              
            });
        }

        public static ShowLoadingDialog(loadingDialog: any, loadingText: any, text?: string) {
            // Displays the loading dialog.
            if (Utils.isNullOrWhiteSpace(text)) {
                loadingText.text(Resources.String_176); // Loading ...
            }
            else {
                loadingText.text(text);
            }

            loadingDialog.dialog('open');
            $('.ui-widget-overlay').addClass('msax-LoadingOverlay');
        }

        public static CloseLoadingDialog(loadingDialog: any) {
            // Close the dialog.
            loadingDialog.dialog('close');   
            $('.ui-widget-overlay').removeClass('msax-LoadingOverlay');         
        }
    }
}
