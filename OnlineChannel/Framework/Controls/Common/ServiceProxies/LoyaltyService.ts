﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../Helpers/Core.ts" />
/// <reference path="../Helpers/EcommerceTypes.ts" />

"use strict";

// Web services proxy
// Encapsulates ajax calls to the WCF services.
module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    export class LoyaltyService {
        private static proxy: AjaxProxy;

        public static GetProxy() {
            this.proxy = new AjaxProxy(msaxValues.msax_LoyaltyServiceUrl + '/');
        }

        public static GetLoyaltyCards(): AsyncResult<LoyaltyCardsResponse> {
            var asyncResult = new AsyncResult<LoyaltyCardsResponse>();

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetLoyaltyCards",
                null,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetAllLoyaltyCardsStatus() {
            var asyncResult = new AsyncResult<any>();

            var data = {
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetAllLoyaltyCardsStatus",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });
        }

        public static GetLoyaltyCardStatus(loyaltyCardNumber: string) {
            var asyncResult = new AsyncResult<any>();

            var data = {
                "loyaltyCardNumber": loyaltyCardNumber
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetLoyaltyCardStatus",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });
        }

        public static UpdateLoyaltyCardId(isCheckoutSession: boolean, loyaltyCardId: string, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "loyaltyCardId": loyaltyCardId,
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "UpdateLoyaltyCardId",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }        

    }
} 