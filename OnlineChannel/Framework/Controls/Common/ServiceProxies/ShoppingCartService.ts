﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../Helpers/Core.ts" />
/// <reference path="../Helpers/EcommerceTypes.ts" />

"use strict";

// Web services proxy
// Encapsulates ajax calls to the WCF services.
module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    export class ShoppingCartService {
        private static proxy: AjaxProxy;

        public static GetProxy() {
            this.proxy = new AjaxProxy(msaxValues.msax_ShoppingCartServiceUrl + '/');
        }

        public static UpdateShoppingCart(shoppingCartResponse: ShoppingCartResponse, isCheckoutSession: boolean) {
            if (!Utils.isNullOrUndefined(shoppingCartResponse.ShoppingCart) && Utils.hasElements(shoppingCartResponse.ShoppingCart.Items)) {
                for (var index in shoppingCartResponse.ShoppingCart.Items) {
                    var item = shoppingCartResponse.ShoppingCart.Items[index];
                    if (!Utils.isNullOrWhiteSpace(item.ProductDetails)) {
                        item["ProductDetailsExpanded"] = JSON.parse(item.ProductDetails);
                    } else {
                        item["ProductDetailsExpanded"] = { Name: "", ProductUrl: "", DimensionValues: "", ProductNumber: ""};
                    }

                    item.ImageMarkup = ShoppingCartService.BuildImageMarkup50x50(item.Image);

                    item.OfferNames = item.OfferNames.replace(",", "\n");

                    // Adding kit component details to the item if the item is a kit.
                    if (item.ItemType == TransactionItemType.Kit) {
                        item.NoOfComponents = Utils.format(Resources.String_88 /* {0} PRODUCT(S) */, item.KitComponents.length);

                        for (var j = 0; j < item.KitComponents.length; j++) {
                            item.KitComponents[j].ProductDetailsExpanded = [];

                            item.KitComponents[j].ProductDetailsExpanded.Name = item.KitComponents[j].Name;
                            item.KitComponents[j].ProductDetailsExpanded.ProductUrl = item.KitComponents[j].ProductUrl;
                            item.KitComponents[j].ProductDetailsExpanded.ProductNumber = item.KitComponents[j].ProductNumber;
                            item.KitComponents[j].ProductDetailsExpanded.DimensionValues = ShoppingCartService.GetDimensionValues(item.KitComponents[j].Color, item.KitComponents[j].Size, item.KitComponents[j].Style);

                            item.KitComponents[j].ImageMarkup = ShoppingCartService.BuildImageMarkup50x50(item.KitComponents[j].Image);
                        }
                    }
                }
            }

            // Trigger the update checkout/shopping cart event every time a service updates the cart 
            // so that all subscribers can get the latest cart and handle the event accordingly.
            if (isCheckoutSession) {
                $(document).trigger('UpdateCheckoutCart', [shoppingCartResponse]);
            }
            else {
                $(document).trigger('UpdateShoppingCart', [shoppingCartResponse]);
            }
        }

        public static GetDimensionValues(color: string, size: string, style: string) {
            var hasColor = !Utils.isNullOrWhiteSpace(color);
            var hasSize = !Utils.isNullOrWhiteSpace(size);
            var hasStyle = !Utils.isNullOrWhiteSpace(style);

            var dimensionValues = null;
            if (hasColor || hasSize || hasStyle) {
                dimensionValues = ''
                + (!hasColor ? '' : color)
                + (hasColor && (hasSize || hasStyle) ? ', ' : '')
                + (!hasSize ? '' : size) 
                + (hasStyle && (hasColor || hasSize) ? ', ' : '')
                + (!hasStyle ? '' : style)
                + '';
            }

            return dimensionValues;
        }

        // Function that exposes the UpdateShoppingCart event. 
        // The caller can send the context in which the event should be triggered and irrespective of the triggered context the context given here is available in the event handler.
        public static OnUpdateShoppingCart(callerContext: any, handler: any) {
            $(document).on('UpdateShoppingCart', $.proxy(handler, callerContext));
        }

        // Function that exposes the UpdateCheckoutCart event. 
        // The caller can send the context in which the event should be triggered and irrespective of the triggered context the context given here is available in the event handler.
        public static OnUpdateCheckoutCart(callerContext: any, handler: any) {
            $(document).on('UpdateCheckoutCart', $.proxy(handler, callerContext));
        }

        // Calls the GetCart method.
        public static GetShoppingCart(isCheckoutSession: boolean, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "dataLevel": shoppingCartDataLevel
            };

            if(Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }            

            this.proxy.SubmitRequest(
                "GetShoppingCart",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Calls the RemoveFromCart method.
        public static RemoveFromCart(isCheckoutSession: boolean, lineId: string, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "lineIds": [lineId],
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "RemoveItems",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Calls the UpdateQuantity method.
        public static UpdateQuantity(isCheckoutSession: boolean, items: TransactionItem[], shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "items": items, // Items is in JSON format= [{ "LineId": lineId, "Quantity": quantity }]
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "UpdateItems",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Gets the promotion banners for the cart.
        public static GetPromotions(isCheckoutSession: boolean, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetPromotions",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Adds or removes discount code from shopping cart.
        public static AddOrRemovePromotion(isCheckoutSession: boolean, promoCode: string, isAdd: boolean, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "promotionCode": promoCode,
                "isAdd": isAdd,
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "AddOrRemovePromotionCode",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Initiates the checkout workflow.
        public static CommenceCheckout(dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "dataLevel": dataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "CommenceCheckout",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, true);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Returns markup to be displayed when image url is null or when image url does not exist.
        private static GetNoImageMarkup(): string {
            return Utils.format('<span class=\"msax-NoImageContainer\"></span>');
        }

        // Build image markup with given width and height.
        private static BuildImageMarkup(imageData: ImageInfo, width: number, height: number): string {
            if (!Utils.isNullOrUndefined(imageData)) {
                var imageUrl = imageData.Url;
                var altText = imageData.AltText;
                var imageClassName = "msax-Image";

                if (!Utils.isNullOrWhiteSpace(imageUrl)) {
                    var errorScript = Utils.format('onerror=\"this.parentNode.innerHTML=Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.ShoppingCartService.GetNoImageMarkup();\"');
                    return Utils.format('<img src=\"{0}\" class=\"{1}\" alt=\"{2}\" width=\"{3}\" height=\"{4}\" {5} />', imageUrl, imageClassName, altText, width, height, errorScript);
                }
                else {
                    return ShoppingCartService.GetNoImageMarkup();
                }
            }
            else {
                return ShoppingCartService.GetNoImageMarkup();
            }
        }

        // Builds 50 * 50 image markup.
        private static BuildImageMarkup50x50(imageData: ImageInfo): string {
            return ShoppingCartService.BuildImageMarkup(imageData, 50, 50);
        }
    }
} 