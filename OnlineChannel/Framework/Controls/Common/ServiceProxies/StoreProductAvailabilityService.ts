﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../Helpers/Core.ts" />
/// <reference path="../Helpers/EcommerceTypes.ts" />

"use strict";

// Web services proxy
// Encapsulates ajax calls to the WCF services.
module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {

    export class StoreProductAvailabilityService {
        private static proxy: AjaxProxy;

        public static GetProxy() {
            this.proxy = new AjaxProxy(msaxValues.msax_StoreProductAvailabilityServiceUrl + '/');
        }

        // Calls the GetNearbyStoresWithAvailability method.
        public static GetNearbyStoresWithAvailability(latitude: number, longitude: number, items: TransactionItem[]): AsyncResult<StoreProductAvailabilityResponse> {
            var asyncResult = new AsyncResult<StoreProductAvailabilityResponse>();

            var data = {
                "latitude": latitude,
                "longitude": longitude,
                "items": items
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetNearbyStoresWithAvailability",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Calls the GetNearbyStores method.
        public static GetNearbyStores(latitude: number, longitude: number): AsyncResult<StoreLocationResponse> {
            var asyncResult = new AsyncResult<StoreLocationResponse>();

            var data = {
                "latitude": latitude,
                "longitude": longitude
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetNearbyStores",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }
    }
}  