﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../Helpers/Core.ts" />
/// <reference path="../Helpers/AsyncResult.ts" />

"use strict";

// Web services proxy
// Encapsulates ajax calls to the WCF services.
module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    export class CheckoutService {
        private static proxy;

        public static GetProxy() {
            this.proxy = new AjaxProxy(msaxValues.msax_CheckoutServiceUrl + '/');
        }

        public static SubmitOrder(tenderLineData: TenderDataLine[], email: string): AsyncResult<CreateSalesOrderResponse> {
            var asyncResult = new AsyncResult<CreateSalesOrderResponse>();

            var data = {
                "tenderDataLine": tenderLineData,
                "emailAddress": email
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "CreateOrder",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetDeliveryPreferences(): AsyncResult<DeliveryPreferenceResponse> {
            var asyncResult = new AsyncResult<DeliveryPreferenceResponse>();

            var data = {};

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetDeliveryPreferences",
                data,
                (response) => {
                    asyncResult.resolve(response);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetDeliveryOptionsInfo(): AsyncResult<DeliveryOptionsResponse> {
            var asyncResult = new AsyncResult<DeliveryOptionsResponse>();

            var data = {};

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetDeliveryOptionsInfo",
                data,
                (response) => {
                    asyncResult.resolve(response);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetOrderDeliveryOptionsForShipping(shipToAddress: Address): AsyncResult<DeliveryOptionsResponse> {
            var asyncResult = new AsyncResult<DeliveryOptionsResponse>();

            var data = {
                "shipToAddress": shipToAddress
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetOrderDeliveryOptionsForShipping",
                data,
                (response) => {
                    asyncResult.resolve(response);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetLineDeliveryOptionsForShipping(selectedLineShippingInfo: SelectedLineShippingInfo[]): AsyncResult<DeliveryOptionsResponse> {
            var asyncResult = new AsyncResult<DeliveryOptionsResponse>();

            var data = {
                "selectedLineShippingInfo": selectedLineShippingInfo
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetLineDeliveryOptionsForShipping",
                data,
                (response) => {
                    asyncResult.resolve(response);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static SetOrderDeliveryOption(selectedDeliveryOption: SelectedDeliveryOption, dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "selectedDeliveryOption": selectedDeliveryOption,
                "dataLevel": dataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "SetOrderDeliveryOption",
                data,
                (response) => {
                    ShoppingCartService.UpdateShoppingCart(response, true);
                    asyncResult.resolve(response);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static SetLineDeliveryOptions(lineDeliveryOptions: SelectedLineDeliveryOption[], dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "selectedLineDeliveryOptions": lineDeliveryOptions,
                "dataLevel": dataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "SetLineDeliveryOptions",
                data,
                (response) => {
                    ShoppingCartService.UpdateShoppingCart(response, true);
                    asyncResult.resolve(response);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetPaymentCardTypes(): AsyncResult<PaymentCardTypesResponse> {
            var asyncResult = new AsyncResult<PaymentCardTypesResponse>();

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetPaymentCardTypes",
                null,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static IsAuthenticatedSession(): AsyncResult<BooleanResponse> {
            var asyncResult = new AsyncResult<BooleanResponse>();

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "IsAuthenticatedSession",
                null,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetUserEmail(): AsyncResult<StringResponse> {
            var asyncResult = new AsyncResult<StringResponse>();

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetUserEmail",
                null,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetGiftCardBalance(giftCardNumber: string): AsyncResult<GiftCardResponse> {
            var asyncResult = new AsyncResult<StringResponse>();

            if(Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            var data = {
                "giftCardId": giftCardNumber
            };

            this.proxy.SubmitRequest(
                "GetGiftCardInformation",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }
    }
}
