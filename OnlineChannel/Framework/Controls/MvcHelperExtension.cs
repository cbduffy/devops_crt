﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls
{
    using System;
    using System.Web.Mvc;

    /// <summary>
    /// Mvc helper extensions for ecommerce controls.
    /// </summary>
    public static class MvcHelperExtension
    {
        /// <summary>
        /// Helper extension for Checkout control.
        /// </summary>
        /// <param name="htmlHelper">Indicates the class that the method extends.</param>
        /// <param name="isMobileView">Boolean value indicating whether the control renders in mobile view.</param>
        /// <returns>Checkout control markup.</returns>
        public static MvcHtmlString CheckoutControl(this HtmlHelper htmlHelper, bool isMobileView)
        {
            string htmlContent;

            using (Checkout checkout = new Checkout())
            {
                checkout.IsMobileView = isMobileView;
                htmlContent = checkout.GetControlMarkup();
            }

            return MvcHtmlString.Create(htmlContent);
        }

        /// <summary>
        /// Helper extension for Checkout control header.
        /// </summary>
        /// <param name="htmlHelper">Indicates the class that the method extends.</param>
        /// <param name="orderConfirmationUrl">The redirection url for order confirmation page.</param>
        /// <param name="hasInventoryCheck">Boolean value indicating whether inventory check is added during pick up in store scenario.</param>
        /// <param name="isMobileView">Boolean value indicating whether the control renders in mobile view.</param>
        /// <returns>Checkout control header markup.</returns>
        public static MvcHtmlString CheckoutControlHeader(this HtmlHelper htmlHelper, string orderConfirmationUrl, bool hasInventoryCheck, bool isMobileView)
        {            
            string htmlContent;

            using(Checkout checkout = new Checkout())
            {
                checkout.IsMobileView = isMobileView;
                checkout.OrderConfirmationUrl = orderConfirmationUrl;
                checkout.HasInventoryCheck = hasInventoryCheck;

                htmlContent = checkout.GetHeaderMarkup();
            }

            return MvcHtmlString.Create(htmlContent);
        }

        /// <summary>
        /// Helper extension for shopping cart control.
        /// </summary>
        /// <param name="htmlHelper">Indicates the class that the method extends.</param>
        /// <param name="isMobileView">Boolean value indicating whether the control renders in mobile view.</param>
        /// <returns>Shopping cart control markup.</returns>
        public static MvcHtmlString ShoppingCartControl(this HtmlHelper htmlHelper, bool isMobileView)
        {
            string htmlContent;

            using (ShoppingCart cart = new ShoppingCart())
            {
                cart.IsMobileView = isMobileView;
                htmlContent = cart.GetControlMarkup();
            }

            return MvcHtmlString.Create(htmlContent);
        }

        /// <summary>
        /// Helper extension for shopping cart control header.
        /// </summary>
        /// <param name="htmlHelper">Indicates the class that the method extends.</param>
        /// <param name="checkoutUrl">The redirection url for checkout page.</param>
        /// <param name="isMobileView">Boolean value indicating whether the control renders in mobile view.</param>
        /// <returns>Shopping cart control header markup.</returns>
        public static MvcHtmlString ShoppingCartControlHeader(this HtmlHelper htmlHelper, string checkoutUrl, bool isMobileView)
        {
            string htmlContent;

            using (ShoppingCart cart = new ShoppingCart())
            {
                cart.CheckoutUrl = checkoutUrl;
                cart.IsMobileView = isMobileView;

                htmlContent = cart.GetHeaderMarkup();
            }

            return MvcHtmlString.Create(htmlContent);
        }

        /// <summary>
        /// Helper extension for mini cart control.
        /// </summary>
        /// <param name="htmlHelper">Indicates the class that the method extends.</param>
        /// <param name="isMobileView">Boolean value indicating whether the control renders in mobile view.</param>
        /// <returns>Mini cart control markup.</returns>
        public static MvcHtmlString MiniCartControl(this HtmlHelper htmlHelper, bool isMobileView)
        {
            string htmlContent;

            using (MiniCart cart = new MiniCart())
            {
                cart.IsMobileView = isMobileView;
                htmlContent = cart.GetControlMarkup();
            }

            return MvcHtmlString.Create(htmlContent);
        }

        /// <summary>
        /// Helper extension for Mini cart control header.
        /// </summary>
        /// <param name="htmlHelper">Indicates the class that the method extends.</param>
        /// <param name="checkoutUrl">The redirection url for checkout page.</param>
        /// <param name="isMobileView">Boolean value indicating whether the control renders in mobile view.</param>
        /// <param name="isCheckoutCart">Boolean value indicating whether the cart is checkout cart.</param>
        /// <returns>Mini cart control header markup.</returns>
        public static MvcHtmlString MiniCartControlHeader(this HtmlHelper htmlHelper, string checkoutUrl, bool isMobileView, bool isCheckoutCart)
        {
            string htmlContent;

            using (MiniCart cart = new MiniCart())
            {
                cart.CheckoutUrl = checkoutUrl;
                cart.IsMobileView = isMobileView;
                cart.IsCheckoutCart = isCheckoutCart;

                htmlContent = cart.GetHeaderMarkup();
            }

            return MvcHtmlString.Create(htmlContent);
        }
    }
}