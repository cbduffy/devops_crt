﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;

[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Checkout.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Checkout.mobile.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Images.electronic_delivery_info.png", "image/png")]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Images.i_shipping_truck.png", "image/png")]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Images.progress_end.png", "image/png")]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Images.progress_end_on.png", "image/png")]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Images.progress_step_bg.png", "image/png")]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Images.progress_step_bg_on.png", "image/png")]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Images.progress_step_left.png", "image/png")]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Images.progress_step_left_on.png", "image/png")]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Images.progress_step_right.png", "image/png")]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.Checkout.Images.progress_step_right_on.png", "image/png")]
namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls
{
    /// <summary>
    /// Checkout control.
    /// </summary>
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    [ToolboxData("<{0}:Checkout runat=server></{0}:Checkout>")]
    [ComVisible(false)]
    public class Checkout : RetailWebControl
    {
        private bool hasInventoryCheck = true;
        private bool reviewDisplayPromotionBanner = true;

        /// <summary>
        /// Gets or sets the value indicating the order confirmation redirect url.
        /// </summary>
        public string OrderConfirmationUrl
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the value indicating the control includes inventory check in pick up in store flow.
        /// </summary>
        public bool HasInventoryCheck
        {
            get { return this.hasInventoryCheck; }
            set { this.hasInventoryCheck = value; }
        }

        /// <summary>
        /// Gets or sets the value indicating whether cart in order review section displays promotion banners.
        /// </summary>
        public bool ReviewDisplayPromotionBanner 
        {
            get { return this.reviewDisplayPromotionBanner; }
            set { this.reviewDisplayPromotionBanner = value; }
        }

        /// <summary>
        /// Gets the markup to include control scripts, css, startup scripts in the page.
        /// </summary>
        /// <returns>The header markup.</returns>
        internal string GetHeaderMarkup()
        {
            string output = string.Empty;

            this.GetCssUrls();
            this.GetScriptUrls();

            // Preventing markup getting registered multiple times in a page when using multiple controls.
            string existingHeaderMarkup = string.Empty;
            if (this.Page != null && this.Page.Header != null)
            {
                foreach (Control control in this.Page.Header.Controls)
                {
                    if (control.GetType() == typeof(LiteralControl))
                    {
                        LiteralControl literalControl = (LiteralControl)control;
                        existingHeaderMarkup += literalControl.Text;
                    }
                }
            }

            foreach (string cssUrl in this.cssUrls)
            {
                string cssLink = String.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />", cssUrl);

                // If page header is visible here, check if the markup is present in the header already.
                if (this.Page == null || this.Page.Header == null || !existingHeaderMarkup.Contains(cssLink))
                {
                    output += cssLink;
                }
            }

            foreach (string scriptUrl in this.scriptUrls)
            {
                string scriptLink = String.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", scriptUrl);

                // If page header is visible here, check if the markup is present in the header already.
                if (this.Page == null || this.Page.Header == null || !existingHeaderMarkup.Contains(scriptLink))
                {
                    output += scriptLink;
                }
            }

            string bingMapsLink = String.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", "https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&s=1");

            // If page header is visible here, check if the markup is present in the header already.
            if (this.Page == null || this.Page.Header == null || !existingHeaderMarkup.Contains(bingMapsLink))
            {
                output += bingMapsLink;
            }

            output += this.GetStartupMarkup(existingHeaderMarkup);

            return output;
        }

        /// <summary>
        /// Gets the control html markup with the control class wrapper added.
        /// </summary>
        /// <returns>The control markup.</returns>
        internal string GetControlMarkup()
        {
            string htmlContent = this.GetHtml();

            // Adding wrapper with the control cssClass. 
            return "<div class=\"" + cssClass + "\">" + htmlContent + "</div>";
        }

        /// <summary>
        /// Gets the script urls.
        /// </summary>
        /// <returns>The script urls.</returns>
        protected override Collection<string> GetScriptUrls()
        {
            base.GetScriptUrls();
            return this.scriptUrls;
        }

        /// <summary>
        /// Gets the css urls.
        /// </summary>
        /// <returns>The css urls.</returns>
        protected override Collection<string> GetCssUrls()
        {
            base.GetCssUrls();
            this.AddCssUrl("Checkout.Checkout.css");
            if (IsMobileView)// If we are rendering mobile view then we also need to register Checkout.mobile.css
            {
                this.AddCssUrl("Checkout.Checkout.mobile.css");// Register mobile css file.
            }

            return this.cssUrls;
        }

        /// <summary>
        /// Gets the startup markup.
        /// </summary>
        /// <param name="existingHeaderMarkup">Existing header markup to determine if startup script registration is required.</param>
        /// <returns>The startup markup.</returns>
        protected override string GetStartupMarkup(string existingHeaderMarkup)
        {
            string startupScript = base.GetStartupMarkup(existingHeaderMarkup);

            bool configExists = controlsSection != null;

            if (configExists)
            {
                bool isDemoMode = controlsSection.Checkout.IsDemoMode;
                string demoDataPath = controlsSection.Checkout.DemoDataPath;

                string checkoutStartupScript = string.Format(@"<script type='text/javascript'> 
                    msaxValues['msax_OrderConfirmationUrl'] = '{0}'; 
                    msaxValues['msax_IsDemoMode'] = '{1}'; 
                    msaxValues['msax_HasInventoryCheck'] = '{2}'; 
                    msaxValues['msax_DemoDataPath'] = '{3}'; 
                    msaxValues['msax_ReviewDisplayPromotionBanner'] = '{4}'; 
                </script>", OrderConfirmationUrl, isDemoMode, HasInventoryCheck, demoDataPath, ReviewDisplayPromotionBanner);

                // If page header is visible here, check if the markup is present in the header already.
                if (this.Page == null || this.Page.Header == null || (existingHeaderMarkup != null && !existingHeaderMarkup.Contains(checkoutStartupScript)))
                {
                    startupScript += checkoutStartupScript;
                }

                return startupScript;
            }
            else
            {
                throw new Exception("The required configuration for the checkout control does not exist");
            }
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            string headerMarkup = this.GetHeaderMarkup();
            base.RegisterHeaderMarkup(headerMarkup);
        }

        /// <summary>
        /// Renders the contents of the control to the specified writer. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client. </param>
        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (writer != null)
            {
                // If we are in mobile mode, so that we render Checkout.mobile.html
                string htmlContent = this.GetHtml();
                writer.Write(htmlContent);
            }
        }

        /// <summary>
        /// Gets the control html markup.
        /// </summary>
        /// <returns>The control markup.</returns>
        private string GetHtml()
        {
            return (IsMobileView) ? this.GetHtmlFragment("Checkout.Checkout.mobile.html") : this.GetHtmlFragment("Checkout.Checkout.html");
        }
    }
}
