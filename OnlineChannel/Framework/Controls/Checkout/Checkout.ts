﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../ExternalScripts//JQuery.d.ts" />
/// <reference path="../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../Common/Helpers/Core.ts" />
/// <reference path="../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../Resources/Resources.ts" />

module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    "use strict";

    export class Checkout {

        private _checkoutView;
        private _loadingDialog;
        private _loadingText;
        private cart;
        private isShoppingCartEnabled;
        private kitCartItemTypeValue;
        public errorMessage;
        public errorPanel;
        private nextButton;

        private _checkoutFragments = {
            DeliveryPreferences: "msax-DeliveryPreferences",
            PaymentInformation: "msax-PaymentInformation",
            Review: "msax-Review",
            Confirmation: "msax-Confirmation"
        };

        private countries;
        private allDeliveryOptionDescriptions: DeliveryOption[];
        private dialogOverlay;

        // Delivery preferences section.
        private deliveryPreferences;
        private selectedOrderDeliveryPreference;
        private pickUpInStoreDeliveryModeCode;
        private emailDeliveryModeCode;
        private giftCardItemId;
        private states;
        private tempShippingAddress;

        private _deliveryPreferencesView;
        private _availableStoresView;
        private _location;
        private addresses;
        private selectedAddress;
        private itemSelectedAddress;
        private availableDeliveryOptions; // Can apply to order level or line level delivery based on the context
        private orderDeliveryOption;
        private sendEmailToMe;
        private isAuthenticated: boolean;
        private email;
        private _emailAddressTextBox;
        private bingMapsToken;
        private map;
        private mapStoreLocator;
        private DisableTouchInputOnMap = false;
        private searchLocation;
        private stores;
        private displayLocations;
        private deliveryPreferenceToValidate;

        private itemSelectedDeliveryPreference;
        private showItemDeliveryPreferenceDialog;
        private _itemLevelDeliveryPreferenceSelection;
        private itemDeliveryPreferenceToValidate;
        private item;
        private selectedLineDeliveryOption;
        private hasShipToAddress: boolean;
        private hasPickUpInStore: boolean;
        private hasEmail: boolean;
        private hasMultiDeliveryPreference: boolean;
        private anyGiftCard: boolean;
        private hasInventoryCheck;
        private cartDeliveryPreferences: CartDeliveryPreferences;

        private _deliveryPreferencesFragments = {
            ShipItemsOrderLevel: "msax-ShipItemsOrderLevel",
            PickUpInStoreOrderLevel: "msax-PickUpInStoreOrderLevel",
            EmailOrderLevel: "msax-EmailOrderLevel",
            ItemLevelPreference: "msax-ItemLevelPreference"
        };

        private _itemDeliveryPreferencesFragments = {
            ShipItemsItemLevel: "msax-ShipItemsItemLevel",
            PickUpInStoreItemLevel: "msax-PickUpInStoreItemLevel",
            EmailItemLevel: "msax-EmailItemLevel"
        };

        // Payment methods section.
        private _paymentView;
        private _addDiscountCodeDialog;
        private useShippingAddress;
        private isBillingAddressSameAsShippingAddress;
        private paymentCardTypes;
        private tenderLines: TenderDataLine[] = [];
        private channelCurrencyTemplate;
        private maskedCreditCard;
        private expirtationMonths;
        private confirmEmailValue;
        private paymentTotal;
        private payCreditCard;
        private payGiftCard;
        private payLoyaltyCard;
        private regExForNumber = /[^0-9\.]+/g;
        private _creditCardPanel;
        private _giftCardPanel;
        private _loyaltyCardPanel;
        private creditCardAmount;
        private paymentCard;
        private giftCardAmount;
        private giftCardNumber;
        private giftCardBalance;
        private isGiftCardInfoAvailable;
        private checkGiftCardAmountValidity: boolean = false;

        private loyaltyCards;
        private loyaltyCardAmount;
        private loyaltyCardNumber;
        private totalAmount;

        private expirtationYear = [
            "2014",
            "2015",
            "2016",
            "2017",
            "2018"
        ]; 

        // Order review section.
        private _editRewardCardDialog;
        private isPromotionCodesEnabled;
        private creditCardAmt;
        private giftCardAmt;
        private loyaltyCardAmt;
        private emailDelivery;
        private orderLevelDelivery;
        private displayPromotionBanner;

        // Order confirmation section.
        private orderNumber;

        constructor(element) {
            this._checkoutView = $(element);
            this.errorMessage = ko.observable<string>('');
            this.errorPanel = this._checkoutView.find(" > .msax-ErrorPanel");
            this.nextButton = this._checkoutView.find('.msax-Next');
            this.kitCartItemTypeValue = ko.observable<TransactionItemType>(TransactionItemType.Kit);
            this._loadingDialog = this._checkoutView.find('.msax-Loading');
            this._loadingText = this._loadingDialog.find('.msax-LoadingText');
            LoadingOverlay.CreateLoadingDialog(this._loadingDialog, 200, 200);

            this.countries = ko.observableArray(null);
            this.states = ko.observableArray(null);
            var cart = new ShoppingCartClass(null);
            cart.SelectedDeliveryOption = new SelectedDeliveryOptionClass(null);
            cart.SelectedDeliveryOption.CustomAddress = new AddressClass(null);
            cart.Items = [];
            cart.DiscountCodes = [];
            cart.ShippingAddress = new AddressClass(null);
            this.cart = ko.observable<ShoppingCart>(cart);

            this.commenceCheckout(); 
            this.isAuthenticatedSession();  
            this.getAllDeliveryOptionDescriptions();
            this.getCountryRegionInfoService();
            
            // Subscribing to the UpdateCheckoutCart event.
            ShoppingCartService.OnUpdateCheckoutCart(this, this.updateCheckoutCart);

            // Delivery preferences section.
            this.deliveryPreferences = ko.observableArray<string>(null);
            this.selectedOrderDeliveryPreference = ko.observable<string>(null);
            this._deliveryPreferencesView = this._checkoutView.find(" > ." + this._checkoutFragments.DeliveryPreferences);
            this.deliveryPreferenceToValidate = ko.observable<string>(null);
            this.tempShippingAddress = ko.observable<Address>(null);

            this.addresses = ko.observableArray<string>(null);
            this.selectedAddress = ko.observable<string>(null);
            this.itemSelectedAddress = ko.observable<string>(null);
            this.availableDeliveryOptions = ko.observableArray<DeliveryOption>(null);
            var selectedOrderDeliveryOption = new SelectedDeliveryOptionClass(null);
            selectedOrderDeliveryOption.CustomAddress = new AddressClass(null);
            this.orderDeliveryOption = ko.observable<SelectedDeliveryOption>(selectedOrderDeliveryOption);
            this.isBillingAddressSameAsShippingAddress = ko.observable<boolean>(false);
            this.sendEmailToMe = ko.observable<boolean>(false);
            this.displayLocations = ko.observableArray<StoreProductAvailability>(null);
            this.hasInventoryCheck = ko.observable<boolean>(Utils.isNullOrUndefined(msaxValues.msax_HasInventoryCheck) ? true : msaxValues.msax_HasInventoryCheck.toLowerCase() == "true");

            this.item = ko.observable<TransactionItem>(null);
            this.selectedLineDeliveryOption = ko.observable<SelectedLineDeliveryOption>(null);
            this.itemSelectedDeliveryPreference = ko.observable<string>(null);
            this.itemDeliveryPreferenceToValidate = ko.observable<string>(null);
            this.showItemDeliveryPreferenceDialog = ko.observable<boolean>(false);
            this._itemLevelDeliveryPreferenceSelection = this._deliveryPreferencesView.find('.msax-ItemLevelPreference .msax-ItemLevelPreferenceSelection');            

            // Payment methods section.
            this._paymentView = this._checkoutView.find(" > ." + this._checkoutFragments.PaymentInformation);
            this._addDiscountCodeDialog = this._paymentView.find('.msax-PayPromotionCode .msax-AddDiscountCodeDialog');
            this.useShippingAddress = ko.observable<boolean>(false);
            this.paymentCardTypes = ko.observableArray<PaymentCardType>(null);
            this.confirmEmailValue = ko.observable<string>('');
            this.paymentCard = ko.observable<Payment>(null);
            var payment = new PaymentClass(null);
            payment.PaymentAddress = new AddressClass(null);
            payment.ExpirationYear = 2015;
            payment.ExpirationMonth = 1;
            this.paymentCard(payment);

            this.creditCardAmt = ko.observable<string>('');
            this.giftCardNumber = ko.observable<string>('');
            this.giftCardAmt = ko.observable<string>('');
            this.isGiftCardInfoAvailable = ko.observable<boolean>(false);
            this.giftCardBalance = ko.observable<string>('');
            this._paymentView.find('.msax-GiftCardBalance').hide();
            this.paymentTotal = ko.observable<string>(Utils.formatNumber(0.00));
            this._creditCardPanel = this._paymentView.find('.msax-PayCreditCard .msax-CreditCardDetails');
            this._giftCardPanel = this._paymentView.find('.msax-PayGiftCard .msax-GiftCardDetails');
            this._loyaltyCardPanel = this._paymentView.find('.msax-PayLoyaltyCard .msax-LoyaltyCardDetails');
            this.loyaltyCards = ko.observableArray<string>(null);
            this.loyaltyCardNumber = ko.observable<string>('');
            this.loyaltyCardAmt = ko.observable<string>('');

            this.payCreditCard = ko.observable<boolean>(false);
            this.payGiftCard = ko.observable<boolean>(false);
            this.payLoyaltyCard = ko.observable<boolean>(false);

            this.expirtationMonths = ko.observableArray([
                { key: 1, value: Resources.String_192 },
                { key: 2, value: Resources.String_193 },
                { key: 3, value: Resources.String_194 },
                { key: 4, value: Resources.String_195 },
                { key: 5, value: Resources.String_196 },
                { key: 6, value: Resources.String_197 },
                { key: 7, value: Resources.String_198 },
                { key: 8, value: Resources.String_199 },
                { key: 9, value: Resources.String_200 },
                { key: 10, value: Resources.String_201 },
                { key: 11, value: Resources.String_202 },
                { key: 12, value: Resources.String_203 }
            ]);

            //Order review section.
            this.emailDelivery = ko.observable<boolean>(false);
            this.orderLevelDelivery = ko.observable<boolean>(false);
            this._editRewardCardDialog = this._checkoutView.find('.msax-EditRewardCard');
            this.displayPromotionBanner = ko.observable<boolean>(Utils.isNullOrUndefined(msaxValues.msax_ReviewDisplayPromotionBanner) ? true : msaxValues.msax_ReviewDisplayPromotionBanner.toLowerCase() == "true");

            // Order confirmation section.
            this.orderNumber = ko.observable<string>(null);    
            
            // function that handles the keypress event on the control.
            this._checkoutView.keypress(function (event) {
                if (event.keyCode == 13 /* enter */ || event.keyCode == 8 /* backspace */ || event.keyCode == 27 /* esc */) {
                    event.preventDefault();
                    return false;
                }

                return true;
            });         

            // Computed observables.
            this.isShoppingCartEnabled = ko.computed(() => {
                 return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().Items);
            });

            this.isPromotionCodesEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().DiscountCodes);
            }); 

            this.selectedOrderDeliveryPreference.subscribe((newValue) => {
                this.resetSelectedOrderShippingOptions();
                this.errorPanel.hide();
                this.emailDelivery(false);
                this.orderLevelDelivery(true);
                this.states(null); // reset states value
                
                if (newValue == DeliveryPreferenceType.ShipToAddress) {

                    if (Utils.isNullOrUndefined(this.cart().SelectedDeliveryOption.CustomAddress)) {
                        // CustomAddress is retained as AddressClass object.
                        // If these values are not initialized here then if user goes back to Ship items option after selecting other delivery options
                        // then undefined binding will result in DOM elements not being displayed in ship items address form.
                        var cart = this.cart();
                        cart.SelectedDeliveryOption.CustomAddress = new AddressClass(null);
                        this.cart(cart);                        
                    }                    

                    if ((msaxValues.msax_IsDemoMode.toLowerCase() == "true")
                        && (Utils.isNullOrWhiteSpace(this.cart().SelectedDeliveryOption.CustomAddress.City)
                        || Utils.isNullOrWhiteSpace(this.cart().SelectedDeliveryOption.CustomAddress.Street)
                        || Utils.isNullOrWhiteSpace(this.cart().SelectedDeliveryOption.CustomAddress.State)
                        || Utils.isNullOrWhiteSpace(this.cart().SelectedDeliveryOption.CustomAddress.ZipCode)
                        || Utils.isNullOrWhiteSpace(this.cart().SelectedDeliveryOption.CustomAddress.Country))) {
                        this.autoFillCheckout();
                    }

                    var tempAddress = Utils.clone(this.cart().SelectedDeliveryOption.CustomAddress);
                    this.tempShippingAddress(tempAddress);
                    this.deliveryPreferenceToValidate(' .' + this._deliveryPreferencesFragments.ShipItemsOrderLevel);
                    this.showDeliveryPreferenceFragment(this._deliveryPreferencesFragments.ShipItemsOrderLevel);
                }
                else if (newValue == DeliveryPreferenceType.PickupFromStore) {
                    this.deliveryPreferenceToValidate(' .' + this._deliveryPreferencesFragments.PickUpInStoreOrderLevel);
                    this.showDeliveryPreferenceFragment(this._deliveryPreferencesFragments.PickUpInStoreOrderLevel);
                    this._availableStoresView = this._deliveryPreferencesView.find(".msax-PickUpInStoreOrderLevel .msax-AvailableStores");
                    this._location = this._deliveryPreferencesView.find(".msax-PickUpInStoreOrderLevel input.msax-Location");
                    this._availableStoresView.hide();
                    this.map = this._deliveryPreferencesView.find(".msax-PickUpInStoreOrderLevel .msax-Map");
                    this.getMap();
                }
                else if (newValue == DeliveryPreferenceType.ElectronicDelivery) {
                    this.deliveryPreferenceToValidate(' .' + this._deliveryPreferencesFragments.EmailOrderLevel);
                    this.showDeliveryPreferenceFragment(this._deliveryPreferencesFragments.EmailOrderLevel);

                    var _sendEmailToMeCheckBox = this._checkoutView.find('.msax-EmailOrderLevel .msax-SendEmailToMe');
                    if (!this.isAuthenticated) {
                        _sendEmailToMeCheckBox.hide();
                    }
                    else {
                        _sendEmailToMeCheckBox.show();
                    }

                    var selectedDeliveryOption = new SelectedDeliveryOptionClass(null);
                    selectedDeliveryOption.DeliveryModeId = this.emailDeliveryModeCode;
                    selectedDeliveryOption.DeliveryModeText = this.getDeliveryModeText(this.emailDeliveryModeCode);
                    selectedDeliveryOption.DeliveryPreferenceId = DeliveryPreferenceType.ElectronicDelivery.toString();
                    selectedDeliveryOption.CustomAddress = new AddressClass(null);

                    this.orderDeliveryOption(selectedDeliveryOption);
                    this._emailAddressTextBox = this._deliveryPreferencesView.find('.msax-EmailOrderLevel .msax-EmailTextBox');
                    this.emailDelivery(true);
                }
                else if (newValue == DeliveryPreferenceType.DeliverItemsIndividually) {
                    this.deliveryPreferenceToValidate(' .' + this._deliveryPreferencesFragments.ItemLevelPreference);
                    this.showDeliveryPreferenceFragment(this._deliveryPreferencesFragments.ItemLevelPreference);
                }
                else {
                    this.deliveryPreferenceToValidate('');
                    this.showDeliveryPreferenceFragment('');
                }
            }, this);

            this.itemSelectedDeliveryPreference.subscribe((newValue) => {
                this.resetSelectedOrderShippingOptions();
                this.errorPanel.hide();
                this.selectedLineDeliveryOption().LineId = this.item().LineId;
                this.emailDelivery(false);
                this.orderLevelDelivery(false);
                this.states(null); // reset states value

                // When the dialog box is opened we triger the subscription manually, in which case newValue will be undefined.
                // If the shipping option id was set previously newValue is updated to reflect that.
                // If the shipping option id is not set it means the dialog is opened for the first time for the item, so set it to ship items by default.
                if (Utils.isNullOrUndefined(newValue)) {
                    if (this.selectedLineDeliveryOption().DeliveryPreferenceId == DeliveryPreferenceType.PickupFromStore) {
                        newValue = DeliveryPreferenceType.PickupFromStore;
                    }
                    else if (this.selectedLineDeliveryOption().DeliveryPreferenceId == DeliveryPreferenceType.ElectronicDelivery) {
                        newValue = DeliveryPreferenceType.ElectronicDelivery;
                    }
                    else if (this.selectedLineDeliveryOption().DeliveryPreferenceId == DeliveryPreferenceType.ShipToAddress) {
                        newValue = DeliveryPreferenceType.ShipToAddress;
                    }
                    else {
                        this.itemSelectedDeliveryPreference(DeliveryPreferenceType.None);
                    }
                }

                if (newValue == DeliveryPreferenceType.ShipToAddress) {
                    this.selectedLineDeliveryOption().DeliveryPreferenceId = DeliveryPreferenceType.ShipToAddress;
                    var tempAddress = Utils.clone(this.selectedLineDeliveryOption().CustomAddress);
                    this.tempShippingAddress(tempAddress);

                    var addressDropDownInitialized = false;
                    if (this.isAuthenticated && !Utils.isNullOrUndefined(this.selectedLineDeliveryOption().CustomAddress)) {
                        for (var index in this.addresses()) {
                            if (this.selectedLineDeliveryOption().CustomAddress.Name == this.addresses()[index].Value.Name &&
                                this.selectedLineDeliveryOption().CustomAddress.Street == this.addresses()[index].Value.Street &&
                                this.selectedLineDeliveryOption().CustomAddress.City == this.addresses()[index].Value.City &&
                                this.selectedLineDeliveryOption().CustomAddress.State == this.addresses()[index].Value.State &&
                                this.selectedLineDeliveryOption().CustomAddress.Country == this.addresses()[index].Value.Country &&
                                this.selectedLineDeliveryOption().CustomAddress.ZipCode == this.addresses()[index].Value.ZipCode) {
                                this.itemSelectedAddress(this.addresses()[index].Value);
                                addressDropDownInitialized = true;
                            }
                        }
                    }

                    if (!addressDropDownInitialized) {
                        this.itemSelectedAddress(null);
                    }

                    this.itemDeliveryPreferenceToValidate(' .' + this._itemDeliveryPreferencesFragments.ShipItemsItemLevel);
                    this.showItemDeliveryPreferenceFragment(this._itemDeliveryPreferencesFragments.ShipItemsItemLevel);
                }
                else if (newValue == DeliveryPreferenceType.PickupFromStore) {
                    this.selectedLineDeliveryOption().DeliveryPreferenceId = DeliveryPreferenceType.PickupFromStore;
                    this.itemDeliveryPreferenceToValidate(' .' + this._itemDeliveryPreferencesFragments.PickUpInStoreItemLevel);
                    this.showItemDeliveryPreferenceFragment(this._itemDeliveryPreferencesFragments.PickUpInStoreItemLevel);
                    this._availableStoresView = this._itemLevelDeliveryPreferenceSelection.find(" .msax-PickUpInStoreItemLevel .msax-AvailableStores");
                    this._location = this._itemLevelDeliveryPreferenceSelection.find(" .msax-PickUpInStoreItemLevel input.msax-Location");
                    this._availableStoresView.hide();
                    this.map = this._itemLevelDeliveryPreferenceSelection.find(" .msax-Map");
                    this.getMap();
                }
                else if (newValue == DeliveryPreferenceType.ElectronicDelivery) {
                    this.selectedLineDeliveryOption().DeliveryPreferenceId = DeliveryPreferenceType.ElectronicDelivery;
                    this.selectedLineDeliveryOption().CustomAddress = new AddressClass(null);
                    this.itemDeliveryPreferenceToValidate(' .' + this._itemDeliveryPreferencesFragments.EmailItemLevel);
                    this.showItemDeliveryPreferenceFragment(this._itemDeliveryPreferencesFragments.EmailItemLevel);

                    var _sendEmailToMeCheckBox = this._itemLevelDeliveryPreferenceSelection.find('.msax-SendEmailToMe');
                    if (!this.isAuthenticated) {
                        _sendEmailToMeCheckBox.hide();
                    }
                    else {
                        _sendEmailToMeCheckBox.show();
                    }

                    this.selectedLineDeliveryOption().DeliveryModeId = this.emailDeliveryModeCode;
                    this.selectedLineDeliveryOption().DeliveryModeText = this.getDeliveryModeText(this.emailDeliveryModeCode);    
                    this._emailAddressTextBox = this._itemLevelDeliveryPreferenceSelection.find('.msax-EmailItemLevel .msax-EmailTextBox');
                }
                else {
                    this.itemDeliveryPreferenceToValidate('');
                    this.showItemDeliveryPreferenceFragment('');
                }
            }, this);  

            this.selectedAddress.subscribe((newValue) => {
                if (!Utils.isNullOrUndefined(newValue)) {
                    this.tempShippingAddress(newValue);
                    var element: any = {};
                    element.id = "OrderAddressStreet";
                    element.value = newValue.Street;
                    this.resetOrderAvailableDeliveryMethods(element);
                    element.id = "OrderAddressCity";
                    element.value = newValue.City;
                    this.resetOrderAvailableDeliveryMethods(element);
                    element.id = "OrderAddressZipCode";
                    element.value = newValue.ZipCode;
                    this.resetOrderAvailableDeliveryMethods(element);
                    element.id = "OrderAddressState";
                    element.value = newValue.State;
                    this.resetOrderAvailableDeliveryMethods(element);
                    element.id = "OrderAddressCountry";
                    element.value = newValue.Country;
                    this.resetOrderAvailableDeliveryMethods(element);
                    element.id = "OrderAddressName";
                    element.value = newValue.Name;
                    this.resetOrderAvailableDeliveryMethods(element);
                }
            }, this);
            
            this.itemSelectedAddress.subscribe((newValue) => {
                if (!Utils.isNullOrUndefined(newValue)) {
                    this.tempShippingAddress(newValue);
                    var element: any = {};
                    element.id = "ItemAddressStreet";
                    element.value = newValue.Street;
                    this.resetItemAvailableDeliveryMethods(element);
                    element.id = "ItemAddressCity";
                    element.value = newValue.City;
                    this.resetItemAvailableDeliveryMethods(element);
                    element.id = "ItemAddressZipCode";
                    element.value = newValue.ZipCode;
                    this.resetItemAvailableDeliveryMethods(element);
                    element.id = "ItemAddressState";
                    element.value = newValue.State;
                    this.resetItemAvailableDeliveryMethods(element);
                    element.id = "ItemAddressCountry";
                    element.value = newValue.Country;
                    this.resetItemAvailableDeliveryMethods(element);
                    element.id = "ItemAddressName";
                    element.value = newValue.Name;
                    this.resetItemAvailableDeliveryMethods(element);
                }
            }, this);

            this.isBillingAddressSameAsShippingAddress.subscribe((newValue) => {
                var payment = this.paymentCard();
                var email = payment.PaymentAddress.Email;
                if (newValue && !Utils.isNullOrUndefined(this.cart().ShippingAddress)) {
                    payment.PaymentAddress = this.cart().ShippingAddress;
                    this.getStateProvinceInfoService(payment.PaymentAddress.Country);
                }
                else {
                    payment.PaymentAddress = new AddressClass(null);
                    this.states(null);
                }

                payment.PaymentAddress.Email = email;
                this.paymentCard(payment);

                return newValue;
            }, this);

            this.sendEmailToMe.subscribe((newValue) => {
                if (!Utils.isNullOrUndefined(newValue) && newValue) {
                    if (Utils.isNullOrWhiteSpace(this.email)) {
                        this.errorMessage(Resources.String_119); // Sorry, something went wrong. An error occurred while trying to get the email address. Please enter the email address in the text box below.
                        this.showError(true);
                    }

                    this._emailAddressTextBox.val(this.email);
                }

                return (!Utils.isNullOrUndefined(newValue) && newValue);
            }, this);

            this.maskedCreditCard = ko.computed(() => {
                var ccNumber = '';

                if (!Utils.isNullOrUndefined(this.paymentCard())) {
                    var cardNumber = this.paymentCard().CardNumber;

                    if (!Utils.isNullOrUndefined(cardNumber)) {
                        var ccLength = cardNumber.length;
                        if (ccLength > 4) {
                            for (var i = 0; i < ccLength - 4; i++) {
                                ccNumber += '*';
                                if ((i + 1) % 4 == 0) {
                                    ccNumber += '-';
                                }
                            }
                            // Display only the last 4 digits.
                            ccNumber += cardNumber.substring(ccLength - 4, ccLength);
                        }
                    }
                }

                return ccNumber;
            });
        } 

        private loadXMLDoc(filename) {
            var xhttp;

            if (XMLHttpRequest) {
                xhttp = new XMLHttpRequest();
            }
            else
            {
                xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xhttp.open("GET", filename, false);
            xhttp.send();
            return xhttp.responseXML;
        }

        private autoFillCheckout() { 
            if (Utils.isNullOrEmpty(msaxValues.msax_DemoDataPath)) {
                return;
            }

            var xmlDoc = this.loadXMLDoc(msaxValues.msax_DemoDataPath);

            var address = xmlDoc.getElementsByTagName("Address");
            var country = address[0].getElementsByTagName("Country");
            var name = address[0].getElementsByTagName("Name");
            var street = address[0].getElementsByTagName("Street");
            var city = address[0].getElementsByTagName("City");
            var state = address[0].getElementsByTagName("State");
            var zipcode = address[0].getElementsByTagName("Zipcode");
            var email = xmlDoc.getElementsByTagName("Email");
            var payment = xmlDoc.getElementsByTagName("Payment");
            var cardNumber = payment[0].getElementsByTagName("CardNumber");
            var ccid = payment[0].getElementsByTagName("CCID");

            var tempAddress = new AddressClass(null);
            tempAddress.Name = name[0].textContent;
            tempAddress.Country = country[0].textContent;
            tempAddress.Street = street[0].textContent;
            tempAddress.City = city[0].textContent;
            tempAddress.State = state[0].textContent;
            tempAddress.ZipCode = zipcode[0].textContent;
            tempAddress.Email = email[0].textContent;
            this.cart().SelectedDeliveryOption.CustomAddress = tempAddress;
            this.cart(this.cart());

            var payment = this.paymentCard();
            this.confirmEmailValue(email[0].textContent);
            payment.NameOnCard = name[0].textContent;
            payment.CardNumber = cardNumber[0].textContent;
            payment.CCID = ccid[0].textContent;
            payment.PaymentAddress = tempAddress;
            this.paymentCard(payment);
        }

        private showCheckoutFragment(fragmentCssClass) {
            var allFragments = this._checkoutView.find("> div:not(' .msax-ProgressBar')");           
            allFragments.hide();

            var fragmentToShow = this._checkoutView.find(" > ." + fragmentCssClass);
            fragmentToShow.show();            

            var _progressBar = this._checkoutView.find(" > .msax-ProgressBar");
            var _delivery = _progressBar.find(" > .msax-Delivery");
            var _payment = _progressBar.find(" > .msax-Payment");
            var _review = _progressBar.find(" > .msax-Review");
            var _progressBarEnd = _progressBar.find(" > .msax-ProgressBarEnd");

            switch (fragmentCssClass) {
                case this._checkoutFragments.DeliveryPreferences:
                    _delivery.addClass("msax-Active");
                    if (_payment.hasClass("msax-Active")) {
                        _payment.removeClass("msax-Active");
                    }
                    if (_review.hasClass("msax-Active")) {
                        _review.removeClass("msax-Active");
                    }
                    if (_progressBarEnd.hasClass("msax-Active")) {
                        _progressBarEnd.removeClass("msax-Active");
                    }
                    break;

                case this._checkoutFragments.PaymentInformation:
                    _delivery.addClass("msax-Active");
                    _payment.addClass("msax-Active");
                    if (_review.hasClass("msax-Active")) {
                        _review.removeClass("msax-Active");
                    }
                    if (_progressBarEnd.hasClass("msax-Active")) {
                        _progressBarEnd.removeClass("msax-Active");
                    }
                    break;

                case this._checkoutFragments.Review:
                    _delivery.addClass("msax-Active");
                    _payment.addClass("msax-Active");
                    _review.addClass("msax-Active");

                    if (_progressBarEnd.hasClass("msax-Active")) {
                        _progressBarEnd.removeClass("msax-Active");
                    }
                    break;

                case this._checkoutFragments.Confirmation:
                    _delivery.addClass("msax-Active");
                    _payment.addClass("msax-Active");
                    _review.addClass("msax-Active");
                    _progressBarEnd.addClass("msax-Active");
                    break;                
            }
        }

        private showDeliveryPreferenceFragment(fragmentCssClass) {
            var allFragments = this._deliveryPreferencesView.find(" .msax-DeliveryPreferenceOption");
            allFragments.hide();

            if (!Utils.isNullOrWhiteSpace(fragmentCssClass)) {
                var fragmentToShow = this._deliveryPreferencesView.find(" ." + fragmentCssClass);
                fragmentToShow.show();
            }
        }

        private showItemDeliveryPreferenceFragment(fragmentCssClass) {
            var allFragments = this._itemLevelDeliveryPreferenceSelection.find(" .msax-DeliveryPreferenceOption");
            allFragments.hide();

            if (!Utils.isNullOrWhiteSpace(fragmentCssClass)) {
                var fragmentToShow = this._itemLevelDeliveryPreferenceSelection.find(" ." + fragmentCssClass);
                fragmentToShow.show();
            }
        }

        private validateItemDeliveryInformation() {
            if (Utils.isNullOrWhiteSpace(this.selectedLineDeliveryOption().DeliveryModeId)) {
                if (this.itemSelectedDeliveryPreference() == DeliveryPreferenceType.PickupFromStore) {
                    this.errorMessage(Utils.format(Resources.String_114)); // Please select a store for pick up.
                }
                else if (this.itemSelectedDeliveryPreference() == DeliveryPreferenceType.ShipToAddress) {
                    this.errorMessage(Utils.format(Resources.String_61)); // Please select shipping method.
                }
                else if (this.itemSelectedDeliveryPreference() == DeliveryPreferenceType.None) {
                    this.errorMessage(Resources.String_158); // Please select delivery preference.
                }

                this.showError(false);
                return false;
            }

            this.errorPanel.hide();
            return true;
        }

        private validateDeliveryInformation($shippingOptions: JQuery) {
            if (this.selectedOrderDeliveryPreference() == DeliveryPreferenceType.PickupFromStore && Utils.isNullOrWhiteSpace(this.orderDeliveryOption().DeliveryModeId)) {
                this.errorMessage(Resources.String_114); // Please select a store for pick up.

                this.showError(false);
                return false;
            }
            else if (this.selectedOrderDeliveryPreference() == DeliveryPreferenceType.ShipToAddress && Utils.isNullOrWhiteSpace(this.orderDeliveryOption().DeliveryModeId)) {
                this.errorMessage(Resources.String_61); // Please select shipping method.

                this.showError(false);
                return false;
            }
            else if (this.selectedOrderDeliveryPreference() == DeliveryPreferenceType.DeliverItemsIndividually) {
                var items = this.cart().Items;
                for (var i = 0; i < items.length; i++) {
                    if (Utils.isNullOrWhiteSpace(items[i].SelectedDeliveryOption.DeliveryModeId)) {
                        if (items[i].SelectedDeliveryOption.DeliveryPreferenceId == DeliveryPreferenceType.PickupFromStore) { // Pick up in store
                            this.errorMessage(Utils.format(Resources.String_114 + Resources.String_125, items[i].ProductDetailsExpanded.Name)); // Please select a store for pick up for product {0}.
                        }
                        else if (items[i].SelectedDeliveryOption.DeliveryPreferenceId == DeliveryPreferenceType.ShipToAddress) { // Ship items
                            this.errorMessage(Utils.format(Resources.String_61 + Resources.String_125, items[i].ProductDetailsExpanded.Name)); // Please select shipping method for product {0}.
                        }
                        else {
                            this.errorMessage(Utils.format(Resources.String_126, items[i].ProductDetailsExpanded.Name)); // Please select delivery preference for product {0}.
                        }

                        this.showError(false);
                        return false;
                    }
                }
            }
            else if (this.selectedOrderDeliveryPreference() == DeliveryPreferenceType.None) {
                this.errorMessage(Resources.String_158); // Please select delivery preference.
                this.showError(false);
                return false;
            }

            this.errorPanel.hide(); 
            return true;
        }

        private deliveryPreferencesNextClick() {
            this.states(null); // reset states value

            switch(this.selectedOrderDeliveryPreference()) {
                case DeliveryPreferenceType.DeliverItemsIndividually:
                    this.setItemShippingOptions();
                    this.useShippingAddress(false);
                    break;
                case DeliveryPreferenceType.ShipToAddress:
                    this.setDeliveryOptions();
                    this.useShippingAddress(true);
                    break;
                case DeliveryPreferenceType.ElectronicDelivery:
                    this.orderDeliveryOption().ElectronicDeliveryEmail = this.cart().SelectedDeliveryOption.ElectronicDeliveryEmail;
                    this.orderDeliveryOption().ElectronicDeliveryEmailContent = this.cart().SelectedDeliveryOption.ElectronicDeliveryEmailContent;
                    this.setDeliveryOptions();
                    this.useShippingAddress(false);
                    break;
                default:
                    this.setDeliveryOptions();
                    this.useShippingAddress(false);
                    break;
            }

            this.getPaymentCardTypes();

            // If session is authenticated get the loyalty cards for the user else hide the radio button before the custom loyalty number text box.private removeValidation(element) {
            if (this.isAuthenticated) {
                this.getLoyaltyCards();
                this.paymentCard().PaymentAddress.Email = this.email;
                this.paymentCard(this.paymentCard());
                this.confirmEmailValue(this.email);
            }
            else {
                var _customLoyaltyRadio = this._paymentView.find("#CustomLoyaltyRadio"); 
                _customLoyaltyRadio.hide();
            }
            
            this.removeValidation(this._creditCardPanel);
        }

        private paymentInformationPreviousClick() {
            this.showCheckoutFragment(this._checkoutFragments.DeliveryPreferences);
        }

        private validateConfirmEmailTextBox(srcElement: Element) {
            var $element = $(srcElement);
            var value: string = $element.val();

            if (value !== this.paymentCard().PaymentAddress.Email) {
                this.errorMessage(Resources.String_62); // The confirm email address must match the email address.
                this.showError(false);
                return false;
            }

            this.errorPanel.hide(); 
            return true;
        }

        private paymentInformationNextClick() {
            this.tenderLines = [];
            this.validatePayments();
        }		    
		
        private reviewPreviousClick() {
            if (this.selectedOrderDeliveryPreference() == DeliveryPreferenceType.ShipToAddress) {
                this.useShippingAddress(true);
            }
            else {
                this.useShippingAddress(false);
            }

		    this.showCheckoutFragment(this._checkoutFragments.PaymentInformation);
		}

        private quantityMinusClick(item: TransactionItem) {
            if (item.Quantity == 1) {
                this.removeFromCartClick(item);
            } else {
                item.Quantity = item.Quantity - 1;
                this.updateQuantity([item]);
            }
        }

        private quantityPlusClick(item: TransactionItem) {
            item.Quantity = item.Quantity + 1;
            this.updateQuantity([item]);
        }

        private quantityTextBoxChanged(item: TransactionItem, valueAccesor) {
            var srcElement = valueAccesor.target;
            if (!Utils.isNullOrUndefined(srcElement) && srcElement.value != item.Quantity) {
                item.Quantity = srcElement.value;
                if (item.Quantity < 0) {
                    item.Quantity = 1;
                }

                if (item.Quantity == 0) {
                    this.removeFromCartClick(item);
                } else {
                    this.updateQuantity([item]);
                }
            }
        }

		private selectOrderDeliveryOption(selectedDeliveryOption: SelectedDeliveryOption) {
            this.orderDeliveryOption().DeliveryModeText = selectedDeliveryOption.DeliveryModeText;
			return true;
        }

        private resetSelectedOrderShippingOptions() {
            this.availableDeliveryOptions(null);
            this.orderDeliveryOption(new SelectedDeliveryOptionClass(null));
        }

        private showError(isError: boolean) {
            if (isError) {
                this.errorPanel.addClass("msax-Error");
            }
            else if (this.errorPanel.hasClass("msax-Error")) {
                this.errorPanel.removeClass("msax-Error");
            }

            this.errorPanel.show();
            $(window).scrollTop(0);
        }

        private getResx(key: string) {
            // Gets the resource value.
            return Resources[key];
        }

        private getDeliveryModeText(deliveryModeId: string)
        {
           var deliveryModeText: string ="";
            for (var i = 0; i < this.allDeliveryOptionDescriptions.length; i++) {
                if (this.allDeliveryOptionDescriptions[i].Id == deliveryModeId) {
                    deliveryModeText = this.allDeliveryOptionDescriptions[i].Description;
                    break;
                }
            }

            return deliveryModeText;
        }

        private updateSelectedShippingOptions(cart: ShoppingCart) {
            // Retaining cart level shipping options.
            cart.SelectedDeliveryOption = this.orderDeliveryOption();

            if (this.selectedOrderDeliveryPreference() != DeliveryPreferenceType.DeliverItemsIndividually) {
                // Copying down the selected cart delivery preference at item level
                for (var i = 0; i < cart.Items.length; i++) {
                    cart.Items[i].SelectedDeliveryOption = this.orderDeliveryOption();

                    // If the delivery mode is available at the cart header level and not at cart line level 
                    // then copy down the delivery mode information to line level. 
                    if (!Utils.isNullOrWhiteSpace(cart.DeliveryModeId) && Utils.isNullOrWhiteSpace(cart.Items[i].DeliveryModeId)) {
                        cart.Items[i].DeliveryModeId = cart.DeliveryModeId;
                    }

                    this.getItemDeliveryModeDescription(cart.Items[i]);
                }
            }
            else {
                // Retaining item level shipping options.
                for (var i = 0; i < cart.Items.length; i++) {
                    cart.Items[i].SelectedDeliveryOption = this.cart().Items[i].SelectedDeliveryOption;
                    
                    // If the delivery mode is available at the cart header level and not at cart line level 
                    // then copy down the delivery mode information to line level.
                    if (!Utils.isNullOrWhiteSpace(cart.DeliveryModeId) && Utils.isNullOrWhiteSpace(cart.Items[i].DeliveryModeId)) {
                        cart.Items[i].DeliveryModeId = cart.DeliveryModeId;
                    }

                    this.getItemDeliveryModeDescription(cart.Items[i]);
                }
            }

            this.cart(cart); 
        }

        private getItemDeliveryModeDescription(item: TransactionItem) {
            // This function sets the delivery mode description on shopping cart items given the delivery mode id.
            if (Utils.isNullOrWhiteSpace(item.DeliveryModeId)) {
                item.DeliveryModeText = null;
            }
            else {
                for (var i = 0; i < this.allDeliveryOptionDescriptions.length; i++) {
                    if (this.allDeliveryOptionDescriptions[i].Id == item.DeliveryModeId) {
                        item.DeliveryModeText = this.allDeliveryOptionDescriptions[i].Description;
                    }
                }
            }
        }

        private getMap() {
            // Initialize the map if it exists from a previous search
            if (this.mapStoreLocator) {
                this.mapStoreLocator.dispose();
            }

            // Call to Bing to return the initial map object we will be working with
            // DisableTouchInputOnMap is a global variable which should be specified for each channel
            this.mapStoreLocator = new Microsoft.Maps.Map(this.map[0], { credentials: this.bingMapsToken, zoom: 1, disableTouchInput: this.DisableTouchInputOnMap });


            // This call sets up the search manager that we will use to geocode the starting location specified by the user
            Microsoft.Maps.loadModule('Microsoft.Maps.Search');
        }

        private getNearbyStoresWithAvailability() {            
            if (!Utils.isNullOrUndefined(this._location) && !Utils.isNullOrWhiteSpace(this._location.val())) {

                this.resetSelectedOrderShippingOptions();
                this.getMap();

                var searchManager = new Microsoft.Maps.Search.SearchManager(this.mapStoreLocator);
                // Query the location details provided by the user and attempt to convert to a geocode (normalized lat/lon)
                var geocodeRequest = { where: this._location.val(), count: 1, callback: this.geocodeCallback.bind(this), errorCallback: this.geocodeErrorCallback.bind(this) };
                searchManager.geocode(geocodeRequest);
            }
        }
        
        private geocodeCallback(geocodeResult, userData) {
            // This function is called when a geocode query has successfully executed.
            // Report an error if the geocoding did not return any results.
            // This will be caused by a poorly formed location input by the user.
            if (!geocodeResult.results[0]) {
                this.errorMessage(Resources.String_109); // Sorry, we were not able to decipher the address you gave us.  Please enter a valid Address.
                this.showError(false);
                return;
            }

            this.searchLocation = geocodeResult.results[0].location;

            // Center the map based on the location result returned and a starting (city level) zoom
            // This will trigger the map view change event that will render the store plots
            this.mapStoreLocator.setView({ zoom: 11, center: this.searchLocation });

            //Add a handler for the map change event.  This event is used to render the store location plots each time the user zooms or scrolls to a new viewport
            Microsoft.Maps.Events.addHandler(this.mapStoreLocator, 'viewchanged', this.renderAvailableStores.bind(this));

            // Call the CRT to obtain a list of stores with a radius of the location provided.
            // Note that we request stores for the maximum radius we want to support (200).  The map control
            // is used to determine the "within" scope based on the users zoom settings at runtime.
            if (this.hasInventoryCheck()) {
                this.getNearbyStoresWithAvailabilityService();
            }
            else {
                this.getNearbyStoresService();
            }
        }

        private geocodeErrorCallback(geocodeRequest) {
            // This function handles an error from the geocoding service
            // These errors are thrown due to connectivity or system faults, not poorly formed location inputs.       
            this.errorMessage(Resources.String_110); // Sorry, something went wrong. An error has occured while looking up the address you provided. Please refresh the page and try again.
            this.showError(true);
        }

        private renderAvailableStores() {
            // Clear the current plots on the map and reset the map observables.
            this.mapStoreLocator.entities.clear();
            this._availableStoresView.hide();
            this.displayLocations(null);

            // Initialise the label index
            var storeCount = 0;
            var pin;
            var pinInfoBox;

            // Get the current bounding rectangle of the map view.  This will become our geofence for testing locations against
            var mapBounds = this.mapStoreLocator.getBounds();
            var displayLocations = [];

            // Display search location
            if (!Utils.isNullOrUndefined(this.searchLocation) && mapBounds.contains(this.searchLocation)) {
                // Plot the location to the map
                pin = new Microsoft.Maps.Pushpin(this.searchLocation, { draggable: false, text: "X" });
                this.mapStoreLocator.entities.push(pin);
            }

            // If we have stores, plot them on the map
            if (!Utils.isNullOrEmpty(this.stores)) {
                for (var i = 0; i < this.stores.length; i++) {
                    var currentStoreLocation = this.stores[i];
                    currentStoreLocation.location = { latitude: currentStoreLocation.Latitude, longitude: currentStoreLocation.Longitude };

                    // Test each location to see if it is within the bounding rectangle
                    if (mapBounds.contains(currentStoreLocation.location)) {
                        this._availableStoresView.show();

                        //  Increment the counter used to manage the sequential entity index
                        storeCount++;
                        currentStoreLocation.LocationCount = storeCount;
                        displayLocations.push(currentStoreLocation);

                        // This is the html that appears when a push pin is clicked on the map
                        var storeAddressText = '<div style="width:80%;height:100%;"><p style="background-color:gray;color:black;margin-bottom:5px;"><span style="padding-right:45px;">Store</span><span style="font-weight:bold;">Distance</span><p><p style="margin-bottom:0px;margin-top:0px;"><span style="color:black;padding-right:35px;">' + currentStoreLocation.StoreName + '</span><span style="color:black;">' + currentStoreLocation.Distance + ' miles</span></p><p style="margin-bottom:0px;margin-top:0px;">' + currentStoreLocation.Street + ' </p><p style="margin-bottom:0px;margin-top:0px;">' + currentStoreLocation.City + ', ' + currentStoreLocation.State + ' ' + currentStoreLocation.ZipCode + '</p></div>';

                        // Plot the location to the map	
                        pin = new Microsoft.Maps.Pushpin(currentStoreLocation.location, { draggable: false, text: "" + storeCount + "" });

                        // Populating the Bing map push pin popup with store location data
                        pinInfoBox = new Microsoft.Maps.Infobox(currentStoreLocation.location, { width: 225, offset: new Microsoft.Maps.Point(0, 10), showPointer: true, visible: false, description: storeAddressText });

                        // Registering the event that fires when a pushpin on a Bing map is clicked
                        Microsoft.Maps.Events.addHandler(pin, 'click', (function (pinInfoBox) {
                            return function () {
                                    pinInfoBox.setOptions({ visible: true });
                                }
                        })(pinInfoBox));

                        this.mapStoreLocator.entities.push(pin);
                        this.mapStoreLocator.entities.push(pinInfoBox);
                    }
                }
            }

            this.displayStores(displayLocations);
        }

        private displayStores(displayLocations: StoreProductAvailability[]) {
            // This function is for displaying the store details on the page.
            this.displayLocations(displayLocations);
            var isAvailableInAnyStore = false;

            if (this.hasInventoryCheck()) { // If there is inventory check, the first store where products are available is selected by default.
                for (var i = 0; i < displayLocations.length; i++) {
                    if (displayLocations[i].AreItemsAvailableInStore) {
                        isAvailableInAnyStore = true;
                        this.selectStore(displayLocations[i]);
                        break;
                    }
                }
            }
            else { // If there is no inventory check select the first store by default.
                this.selectStore(displayLocations[0]);
            }

            if (!isAvailableInAnyStore && this.hasInventoryCheck()) {
                this.resetSelectedOrderShippingOptions();
                this.errorMessage(Resources.String_107); // Products are not available for pick up in the stores around the location you searched. Please update your delivery preferences and try again.
                this.showError(false);
            }
        }

        private selectStore(location: StoreProductAvailability) {
            // This function is for selecting a store when user clicks on a store.
            if (!this.hasInventoryCheck() || (this.hasInventoryCheck() && location.AreItemsAvailableInStore)) {
                this.errorPanel.hide();

                if (this.nextButton.hasClass("msax-Grey")) {
                    this.nextButton.removeClass("msax-Grey");
                }

                if (this.selectedOrderDeliveryPreference() == DeliveryPreferenceType.DeliverItemsIndividually) {
                    this.selectedLineDeliveryOption().DeliveryModeId = this.pickUpInStoreDeliveryModeCode;
                    this.selectedLineDeliveryOption().DeliveryModeText = this.getDeliveryModeText(this.pickUpInStoreDeliveryModeCode); // Pick up in store
                    this.selectedLineDeliveryOption().StoreAddress = location;
                    this.selectedLineDeliveryOption().StoreAddress.ProductAvailabilities = null; // This object was parsed into json when reading, this is not required anymore
                }
                else if (this.selectedOrderDeliveryPreference() == DeliveryPreferenceType.PickupFromStore) { 
                    var selectedDeliveryOption = new SelectedDeliveryOptionClass(null);
                    selectedDeliveryOption.DeliveryModeId = this.pickUpInStoreDeliveryModeCode;
                    selectedDeliveryOption.DeliveryModeText = this.getDeliveryModeText(this.pickUpInStoreDeliveryModeCode); // Pick up in store
                    selectedDeliveryOption.StoreAddress = location;
                    selectedDeliveryOption.StoreAddress.ProductAvailabilities = null; // This object was parsed into json when reading, this is not required anymore
                    selectedDeliveryOption.DeliveryPreferenceId = DeliveryPreferenceType.PickupFromStore.toString();

                    this.orderDeliveryOption(selectedDeliveryOption);
                }             
            }
            else {
                this.resetSelectedOrderShippingOptions();
                this.errorMessage(Resources.String_113); // Products are not available in the selected store, Please select a different store.
                this.nextButton.addClass("msax-Grey");
                this.showError(false);
            }

            // This is to add background color to the selected store block.
            var _stores = this._availableStoresView.find(".msax-AvailableStore");
            var selectedChannelId = location.ChannelId;

            _stores.each(function (index, element) {
                if ($(element).hasClass("msax-Selected")) {
                    $(element).removeClass("msax-Selected");
                }

                if (selectedChannelId == parseInt($(element).attr("channelId"))) {
                    $(element).addClass("msax-Selected");
                }
            });   
        }

        private updateItemAvailibilities() {
            // This function is for setting boolean value indication whether products are available in a store.
            // the products details are parsed to json format.
            if (!Utils.isNullOrEmpty(this.stores)) {
                for (var i = 0; i < this.stores.length; i++) {
                    var currentStore = this.stores[i];
                    currentStore.AreItemsAvailableInStore = true;
                    if (currentStore.ProductAvailabilities.length != 0) {
                        for (var j = 0; j < currentStore.ProductAvailabilities.length; j++) {
                            currentStore.ProductAvailabilities[j].ProductDetails = $.parseJSON(currentStore.ProductAvailabilities[j].ProductDetails);
                            if (currentStore.ProductAvailabilities[j].AvailableQuantity == 0) {
                                currentStore.AreItemsAvailableInStore = false;
                                break;
                            }
                        }
                    }
                    else if (currentStore.ProductAvailabilities.length == 0) {
                        currentStore.AreItemsAvailableInStore = false;
                    }
                }
            }
        } 

        private editRewardCardOverlayClick() {
            this.dialogOverlay = $('.ui-widget-overlay');
            this.dialogOverlay.on('click', $.proxy(this.closeEditRewardCardDialog, this));
        }

        private createEditRewardCardDialog() {
            // Creates the edit reward card dialog box.
            this._editRewardCardDialog.dialog({
                modal: true,
                title: Resources.String_186, // Edit reward card
                autoOpen: false,
                draggable: true,
                resizable: false,
                position: ['top', 100],
                show: { effect: "fadeIn", duration: 500 },
                hide: { effect: "fadeOut", duration: 500 },
                width: 500,
                height: 300,
                dialogClass: 'msax-Control'
            });
        }

        private showEditRewardCardDialog() {
            // Specify the close event handler for the dialog.
            $('.ui-dialog-titlebar-close').on('click', $.proxy(this.closeEditRewardCardDialog, this));

            // Displays the edit reward card dialog.
            this._editRewardCardDialog.dialog('open');
            this.editRewardCardOverlayClick();          
        }

        private closeEditRewardCardDialog() {
            // Close the dialog.
            this._editRewardCardDialog.dialog('close');
        }

        private discountCodeOverlayClick() {
            this.dialogOverlay = $('.ui-widget-overlay');
            this.dialogOverlay.on('click', $.proxy(this.closeDiscountCodeDialog, this));
        }

        private createDiscountCodeDialog() {
            // Creates the discount code dialog box.
            this._addDiscountCodeDialog.dialog({
                modal: true,
                title: Resources.String_188, // Add discount code
                autoOpen: false,
                draggable: true,
                resizable: false,
                position: ['top', 100],
                show: { effect: "fadeIn", duration: 500 },
                hide: { effect: "fadeOut", duration: 500 },
                width: 500,
                height: 300,
                dialogClass: 'msax-Control'
            });
        }

        private showDiscountCodeDialog() {
            // Specify the close event handler for the dialog.
            $('.ui-dialog-titlebar-close').on('click', $.proxy(this.closeDiscountCodeDialog, this));

            // Displays the discount code dialog.
            this._addDiscountCodeDialog.dialog('open');
            this.discountCodeOverlayClick();
        }

        private closeDiscountCodeDialog() {
            // Close the dialog.
            this._addDiscountCodeDialog.dialog('close');
        }

        private itemDeliveryPreferenceSelectionOverlayClick() {
            this.dialogOverlay = $('.ui-widget-overlay');
            this.dialogOverlay.on('click', $.proxy(this.closeItemDeliveryPreferenceSelection, this));
        }

        private createItemDeliveryPreferenceDialog() {
            this._itemLevelDeliveryPreferenceSelection.dialog({
                modal: true,
                autoOpen: false,
                draggable: true,
                resizable: false,
                position: ['top', 100],
                show: { effect: "fadeIn", duration: 500 },
                hide: { effect: "fadeOut", duration: 500 },
                width: 980,
                height: 700,
                dialogClass: 'msax-Control msax-NoTitle'
            });
        }

        private showItemDeliveryPreferenceSelection(parent: TransactionItem) {
            parent.DeliveryPreferences = [];

            parent.DeliveryPreferences.push({ Value: DeliveryPreferenceType.None.toString(), Text: Resources.String_159 }); // Please select a delivery preference...

            var lineDeliveryPreferences: DeliveryPreferenceType[] = Checkout.getDeliveryPreferencesForLine(parent.LineId, this.cartDeliveryPreferences);
            var hasShipToAddress = Checkout.isRequestedDeliveryPreferenceApplicable(lineDeliveryPreferences, DeliveryPreferenceType.ShipToAddress);
            var hasPickUpInStore = Checkout.isRequestedDeliveryPreferenceApplicable(lineDeliveryPreferences, DeliveryPreferenceType.PickupFromStore);
            var hasEmail = Checkout.isRequestedDeliveryPreferenceApplicable(lineDeliveryPreferences, DeliveryPreferenceType.ElectronicDelivery);

            // If the channel has ship items option add it to available delivery preferences. 
            if (hasShipToAddress) {
                parent.DeliveryPreferences.push({ Value: DeliveryPreferenceType.ShipToAddress.toString(), Text: Resources.String_99 }); // Ship items
            }

            // If the channel has pick up in store option and cart does not have any gift card add it to available delivery preferences. 
            if (hasPickUpInStore) {
                parent.DeliveryPreferences.push({ Value: DeliveryPreferenceType.PickupFromStore.toString(), Text: Resources.String_100 }); // Pick up in store
            }

            // If the channel has email option and cart has only gift cards add it to available delivery preferences. 
            if (hasEmail) {
                parent.DeliveryPreferences.push({ Value: DeliveryPreferenceType.ElectronicDelivery.toString(), Text: Resources.String_58 }); // Email
            }

            var temp = new SelectedDeliveryOptionClass(null);
            temp.CustomAddress = new AddressClass(null);          
            //temp.isDeliveryAvailable = parent.SelectedDeliveryOption.isDeliveryAvailable;

            // If item level shipping option is set already retain it, else discard all previous order level settings.
            if (!(Utils.isNullOrWhiteSpace(parent.SelectedDeliveryOption.DeliveryModeId))) {
                // If the item has a custom address then retain it.
                if (!Utils.isNullOrUndefined(parent.SelectedDeliveryOption.CustomAddress)) {
                    temp.CustomAddress.Name = parent.SelectedDeliveryOption.CustomAddress.Name;
                    temp.CustomAddress.Street = parent.SelectedDeliveryOption.CustomAddress.Street;
                    temp.CustomAddress.City = parent.SelectedDeliveryOption.CustomAddress.City;
                    temp.CustomAddress.State = parent.SelectedDeliveryOption.CustomAddress.State;
                    temp.CustomAddress.ZipCode = parent.SelectedDeliveryOption.CustomAddress.ZipCode;
                    temp.CustomAddress.Country = parent.SelectedDeliveryOption.CustomAddress.Country;
                }

                temp.DeliveryPreferenceId = parent.SelectedDeliveryOption.DeliveryPreferenceId;
                temp.ElectronicDeliveryEmail = parent.SelectedDeliveryOption.ElectronicDeliveryEmail;
                temp.ElectronicDeliveryEmailContent = parent.SelectedDeliveryOption.ElectronicDeliveryEmailContent;
            }
                
            this.selectedLineDeliveryOption(temp);
            this.item(parent);

            this.errorPanel.hide();
            this.errorPanel = this._itemLevelDeliveryPreferenceSelection.find(" .msax-ErrorPanel");
            this.itemSelectedDeliveryPreference.notifySubscribers(parent.SelectedDeliveryOption.DeliveryPreferenceId);
            this._itemLevelDeliveryPreferenceSelection.dialog('open');

            this.itemDeliveryPreferenceSelectionOverlayClick();          
        }

        private static getDeliveryPreferencesForLine(lineId: string, cartDeliveryPreferences: CartDeliveryPreferences) {
            //Get line preferences for current line
            var lineDeliveryPreferences: LineDeliveryPreference[] = cartDeliveryPreferences.LineDeliveryPreferences;
            for (var i = 0; i < lineDeliveryPreferences.length; i++) {
                if (lineDeliveryPreferences[i].LineId == lineId) {
                    return lineDeliveryPreferences[i].DeliveryPreferenceTypes;
                }
            }

            //If you reach here, then throw error.
            var msg: string = "No delivery preferences were found for line id" + lineId;
            throw new Error(msg);
        }

        private paymentCountryUpdate(srcElement) {
            // Get states when country is updated in payment page.
            if (!Utils.isNullOrUndefined(srcElement)) {
                this.paymentCard().PaymentAddress.Country = srcElement.value;
                this.getStateProvinceInfoService(srcElement.value);
            }

            return true;
        }

        private resetOrderAvailableDeliveryMethods(srcElement) {
            if (!Utils.isNullOrUndefined(srcElement)) {
                var id = srcElement.id;
                switch (id) {
                    case "OrderAddressStreet":
                        if (this.cart().SelectedDeliveryOption.CustomAddress.Street != srcElement.value) {
                            this.cart().SelectedDeliveryOption.CustomAddress.Street = srcElement.value;
                            this.resetSelectedOrderShippingOptions();
                        }

                        break;
                    case "OrderAddressCity":
                        if (this.cart().SelectedDeliveryOption.CustomAddress.City != srcElement.value) {
                            this.cart().SelectedDeliveryOption.CustomAddress.City = srcElement.value;
                            this.resetSelectedOrderShippingOptions();
                        }

                        break;
                    case "OrderAddressZipCode":
                        if (this.cart().SelectedDeliveryOption.CustomAddress.ZipCode != srcElement.value) {
                            this.cart().SelectedDeliveryOption.CustomAddress.ZipCode = srcElement.value;
                            this.resetSelectedOrderShippingOptions();
                        }

                        break;
                    case "OrderAddressState":
                        if (this.cart().SelectedDeliveryOption.CustomAddress.State != srcElement.value) {
                            this.cart().SelectedDeliveryOption.CustomAddress.State = srcElement.value;
                            this.resetSelectedOrderShippingOptions();
                        }

                        break;
                    case "OrderAddressCountry":
                        if (this.cart().SelectedDeliveryOption.CustomAddress.Country != srcElement.value) {
                            this.cart().SelectedDeliveryOption.CustomAddress.Country = srcElement.value;
                            this.getStateProvinceInfoService(srcElement.value); // Get states when country is updated.
                            this.resetSelectedOrderShippingOptions();
                        }

                        break;
                    case "OrderAddressName":
                        this.cart().SelectedDeliveryOption.CustomAddress.Name = srcElement.value;
                        break;
                }
            }

            return true;
        }

        private resetItemAvailableDeliveryMethods(srcElement) {            
            if (!Utils.isNullOrUndefined(srcElement)) {
                var id = srcElement.id;
                switch (id) {
                    case "ItemAddressStreet":
                        if (this.selectedLineDeliveryOption().CustomAddress.Street != srcElement.value) {
                            this.selectedLineDeliveryOption().CustomAddress.Street = srcElement.value;
                            this.resetSelectedOrderShippingOptions();
                            this.selectedLineDeliveryOption().DeliveryModeId = null;
                        }

                        break;
                    case "ItemAddressCity":
                        if (this.selectedLineDeliveryOption().CustomAddress.City != srcElement.value) {
                            this.selectedLineDeliveryOption().CustomAddress.City = srcElement.value;
                            this.resetSelectedOrderShippingOptions();
                            this.selectedLineDeliveryOption().DeliveryModeId = null;
                        }

                        break;
                    case "ItemAddressZipCode":
                        if (this.selectedLineDeliveryOption().CustomAddress.ZipCode != srcElement.value) {
                            this.selectedLineDeliveryOption().CustomAddress.ZipCode = srcElement.value;
                            this.resetSelectedOrderShippingOptions();
                            this.selectedLineDeliveryOption().DeliveryModeId = null;
                        }

                        break;
                    case "ItemAddressState":
                        if (this.selectedLineDeliveryOption().CustomAddress.State != srcElement.value) {
                            this.selectedLineDeliveryOption().CustomAddress.State = srcElement.value;
                            this.resetSelectedOrderShippingOptions();
                            this.selectedLineDeliveryOption().DeliveryModeId = null;
                        }

                        break;
                    case "ItemAddressCountry":
                        if (this.selectedLineDeliveryOption().CustomAddress.Country != srcElement.value) {
                            this.selectedLineDeliveryOption().CustomAddress.Country = srcElement.value;
                            this.getStateProvinceInfoService(srcElement.value); // Get states when country is updated.
                            this.resetSelectedOrderShippingOptions();
                            this.selectedLineDeliveryOption().DeliveryModeId = null;
                        }

                        break;
                    case "ItemAddressName":
                        this.selectedLineDeliveryOption().CustomAddress.Name = srcElement.value;
                        break;
                }
            }

            return true;
        }

        private closeItemDeliveryPreferenceSelection() {
            // Restore error panel to the panel in the parent page.
            this.errorPanel = this._checkoutView.find(" > .msax-ErrorPanel");            
            
            // Close the dialog.
            this._itemLevelDeliveryPreferenceSelection.dialog('close');
            this.cart(this.cart());
        }

        private setItemDeliveryPreferenceSelection() {
           
            // Set the delivery method text on the item shipping options if the shipping option is ship to address.
            if (this.selectedLineDeliveryOption().DeliveryPreferenceId == DeliveryPreferenceType.ShipToAddress) {
                for (var i = 0; i < this.availableDeliveryOptions().length; i++) {
                    if (this.selectedLineDeliveryOption().DeliveryModeId == this.availableDeliveryOptions()[i].Id) {
                        this.selectedLineDeliveryOption().DeliveryModeText = this.availableDeliveryOptions()[i].Description;
                    }
                }

                this.selectedLineDeliveryOption().ElectronicDeliveryEmail = null;
                this.selectedLineDeliveryOption().ElectronicDeliveryEmailContent = null;
            }
            else if (this.selectedLineDeliveryOption().DeliveryPreferenceId == DeliveryPreferenceType.ElectronicDelivery
                || this.selectedLineDeliveryOption().DeliveryPreferenceId == DeliveryPreferenceType.PickupFromStore) {
                this.selectedLineDeliveryOption().CustomAddress = new AddressClass(null);
            }

            var latestLineDeliveryOption: SelectedLineDeliveryOption = this.selectedLineDeliveryOption();
            var currentDeliveryOption = new SelectedDeliveryOptionClass(null);
            currentDeliveryOption.CustomAddress = latestLineDeliveryOption.CustomAddress;
            currentDeliveryOption.DeliveryModeId = latestLineDeliveryOption.DeliveryModeId;;
            currentDeliveryOption.DeliveryModeText = latestLineDeliveryOption.DeliveryModeText;;
            currentDeliveryOption.DeliveryPreferenceId = latestLineDeliveryOption.DeliveryPreferenceId;;
            currentDeliveryOption.ElectronicDeliveryEmail = latestLineDeliveryOption.ElectronicDeliveryEmail;
            currentDeliveryOption.ElectronicDeliveryEmailContent = latestLineDeliveryOption.ElectronicDeliveryEmailContent;
            currentDeliveryOption.StoreAddress = latestLineDeliveryOption.StoreAddress;
            this.item().SelectedDeliveryOption = currentDeliveryOption;

            // Close the dialog.
            this.closeItemDeliveryPreferenceSelection();
        }

        private findLocationKeyPress(data, event) {
            if (event.keyCode == 8 /* backspace */ || event.keyCode == 27 /* esc */) {
                event.preventDefault();
                return false;
            }
            else if (event.keyCode == 13 /* enter */) {
                this.getNearbyStoresWithAvailability();
                return false;
            }

            return true;
        }

        // html5 validation validates the hidden elements also. 
        // In order to avoid this remove the validation 'required' attribute from the elements when we hide them
        private removeValidation(element) {
            $(element).find(":input").each(function (idx, element) {
                $(element).removeAttr('required');
            }); 
        }

        private addValidation(element) {
            $(element).find(":input").each(function (idx, element) {
                $(element).attr('required', true);
            });
        }

        private updateCustomLoyaltyValidation() {
            if (this._paymentView.find('#CustomLoyaltyRadio').is(':checked')) {
                this.addValidation(this._paymentView.find('#LoyaltyCustomCard'));
            }

            return true;
        }

        private checkForGiftCardInCart() {
            this.anyGiftCard = false; // Is true if there is atleast one gift card in the cart. 
            var Items = this.cart().Items;
            for (var i = 0; i < Items.length; i++) {
                if (Items[i].ItemId == this.giftCardItemId) {
                    this.anyGiftCard = true;
                }
            }
        }

        private updateDeliveryPreferences() {
            // Updates the avialable delivery preferences at order level.
            var deliveryPreferences = [];
           
            deliveryPreferences.push({ Value: DeliveryPreferenceType.None, Text: Resources.String_159 }); // Please select a delivery preference...

            // If the channel has ship items option add it to available delivery preferences. 
            if (this.hasShipToAddress) {
                deliveryPreferences.push({ Value: DeliveryPreferenceType.ShipToAddress, Text: Resources.String_99 }); // Ship items
            }

            // If the channel has pick up in store option and cart does not have any gift card add it to available delivery preferences. 
            if (this.hasPickUpInStore) {
                deliveryPreferences.push({ Value: DeliveryPreferenceType.PickupFromStore, Text: Resources.String_100 }); // Pick up in store
            }

            // If the channel has email option and cart has only gift cards add it to available delivery preferences. 
            if (this.hasEmail) {
                deliveryPreferences.push({ Value: DeliveryPreferenceType.ElectronicDelivery, Text: Resources.String_58 }); // Email
            }

            // If the channel has multi item delivery option and cart has more than one item add it to available delivery preferences. 
            if (this.hasMultiDeliveryPreference) {
                deliveryPreferences.push({ Value: DeliveryPreferenceType.DeliverItemsIndividually, Text: Resources.String_101 }); // Select delivery options by item
            }

            this.deliveryPreferences(deliveryPreferences);
        }

        private showPaymentPanel(data, valueAccessor) {
            var srcElement = valueAccessor.target;

            if (!Utils.isNullOrUndefined(srcElement)) {
                if ($(srcElement).hasClass('msax-PayCreditCardLink')) {
                    this._creditCardPanel.show();
                    this.addValidation(this._creditCardPanel);
                    this.payCreditCard(true);
                }
                else if ($(srcElement).hasClass('msax-PayGiftCardLink')) {
                    this._giftCardPanel.show();
                    this.addValidation(this._giftCardPanel);
                    this.payGiftCard(true);
                }
                else if ($(srcElement).hasClass('msax-PayLoyaltyCardLink')) {
                    this._loyaltyCardPanel.show();
                    this.addValidation(this._loyaltyCardPanel);
                    if (this.isAuthenticated) {
                        this.removeValidation(this._paymentView.find('#LoyaltyCustomCard'));
                    }

                    this.payLoyaltyCard(true);
                }

                $(srcElement).hide();
                this.updatePaymentTotal();
            }
        }

        private hidePaymentPanel(data, valueAccessor) {
            var srcElement = valueAccessor.target;

            if (!Utils.isNullOrUndefined(srcElement)) {
                if ($(srcElement.parentElement).hasClass('msax-CreditCard')) {
                    this._creditCardPanel.hide();
                    this._paymentView.find('.msax-PayCreditCard .msax-PayCreditCardLink').show();
                    this.removeValidation(this._creditCardPanel);
                    this.payCreditCard(false);
                }
                else if ($(srcElement.parentElement).hasClass('msax-GiftCard')) {
                    this._giftCardPanel.hide();
                    this._paymentView.find('.msax-PayGiftCard .msax-PayGiftCardLink').show();
                    this.removeValidation(this._giftCardPanel);
                    this.payGiftCard(false);
                }
                else if ($(srcElement.parentElement).hasClass('msax-LoyaltyCard')) {
                    this._loyaltyCardPanel.hide();
                    this._paymentView.find('.msax-PayLoyaltyCard .msax-PayLoyaltyCardLink').show();
                    this.removeValidation(this._loyaltyCardPanel);
                    this.payLoyaltyCard(false);
                }

                this.updatePaymentTotal();
            }
        }

        private updatePaymentTotal() {
            var _creditCardAmountTextBox = this._paymentView.find('#CreditCardAmount');
            this.creditCardAmount = 0;
            this.giftCardAmount = 0;
            this.loyaltyCardAmount = 0;

            if (this.payGiftCard()) {
                var _giftCardTextBox = this._paymentView.find('#GiftCardAmount');
                this.giftCardAmount = Utils.parseNumberFromLocaleString(_giftCardTextBox.val());
                var giftCardAmountWithCurrency = Utils.format(this.channelCurrencyTemplate, Utils.formatNumber(this.giftCardAmount));
                _giftCardTextBox.val(giftCardAmountWithCurrency);
            }

            if (this.payLoyaltyCard()) {
                var _loyaltyCardTextBox = this._paymentView.find('#LoyaltyCardAmount');
                this.loyaltyCardAmount = Utils.parseNumberFromLocaleString(_loyaltyCardTextBox.val());
                var loyaltyCardAmountWithCurrency = Utils.format(this.channelCurrencyTemplate, Utils.formatNumber(this.loyaltyCardAmount));
                _loyaltyCardTextBox.val(loyaltyCardAmountWithCurrency);
            }

            if (this.payCreditCard()) {
                this.creditCardAmount = this.cart().TotalAmount - this.giftCardAmount - this.loyaltyCardAmount;
                if (this.creditCardAmount < 0) {
                    this.creditCardAmount = 0;
                }

                var creditCardAmountWithCurrency = Utils.format(this.channelCurrencyTemplate, Utils.formatNumber(this.creditCardAmount));
                _creditCardAmountTextBox.val(creditCardAmountWithCurrency);
            }

            this.totalAmount = Number(this.creditCardAmount + this.giftCardAmount + this.loyaltyCardAmount);
            if (isNaN(this.totalAmount)) {
                this.totalAmount = 0;
            }

            this.paymentTotal(Utils.format(this.channelCurrencyTemplate, Utils.formatNumber(this.totalAmount)));

            return true;
        }   

        private validatePayments() {
            if (!this.payCreditCard() && !this.payGiftCard() && !this.payLoyaltyCard()) {
                this.errorMessage(Resources.String_139); // Please select payment method
                this.showError(false);
                return;
            }

            if (this.payCreditCard()) {
                var selectedYear = this.paymentCard().ExpirationYear;
                var selectedMonth = this.paymentCard().ExpirationMonth;
                var currentTime = new Date();
                var currentMonth = currentTime.getMonth() + 1; // Get month returns values between 0 - 12.
                var currentYear = currentTime.getFullYear();
                if (selectedYear < currentYear || selectedYear == currentYear && selectedMonth < currentMonth) {
                    this.errorMessage(Resources.String_140); // The expiration date is not valid. Please select valid expiration month and year and then try again
                    this.showError(false);
                    return;
                }
            }

            if (this.payLoyaltyCard()) {
                if(this.loyaltyCardAmount == 0) {
                    this.errorMessage(Resources.String_152); // Loyalty card payment amount cannot be zero
                    this.showError(false);
                    return;
                }

                if (this.loyaltyCardAmount > this.cart().TotalAmount) {
                    this.errorMessage(Resources.String_153); // Loyalty card payment amount is more than order total
                    this.showError(false);
                    return;
                }
            }

            if (this.payGiftCard()) {
                if (Utils.isNullOrWhiteSpace(this.giftCardNumber())) {
                    this.errorMessage(Resources.String_144); // Please enter a gift card number
                    this.showError(false);
                    return;
                }

                if (this.giftCardAmount == 0) {
                    this.errorMessage(Resources.String_146); // Gift card payment amount cannot be zero
                    this.showError(false);
                    return;
                }

                if (this.giftCardAmount > this.cart().TotalAmount) {
                    this.errorMessage(Resources.String_147); // Gift card payment amount is more than order total
                    this.showError(false);
                    return;
                }

                this.checkGiftCardAmountValidity = true;
                // Get gift card balance
                this.getGiftCardBalance();                
            }
            else {
                this.createPaymentCardTenderLine();
            }
        }     

        private createPaymentCardTenderLine() { 
            this.paymentCard(this.paymentCard()); // This is required to trigger computed values and update UI.

            if (this.totalAmount != this.cart().TotalAmount) {
                this.errorMessage(Resources.String_149); // Payment amount is different from the order total
                this.showError(false);
                return;
            }

            if (this.payCreditCard()) {
                var tenderLine = new TenderDataLineClass(null);
                tenderLine.PaymentCard = new PaymentClass(this.paymentCard());
                tenderLine.Amount = this.creditCardAmount;
                this.creditCardAmt(Utils.format(this.channelCurrencyTemplate, Utils.formatNumber(this.creditCardAmount)));
                this.tenderLines.push(tenderLine);
            }

            if (this.payLoyaltyCard()) {
                var tenderLine = new TenderDataLineClass(null);

                if (!this.isAuthenticated || this._paymentView.find('#CustomLoyaltyRadio').is(':checked')) {
                    this.loyaltyCardNumber(this._paymentView.find('#CustomLoyaltyCardNumber').val());
                }

                tenderLine.LoyaltyCardId = this.loyaltyCardNumber();
                tenderLine.Amount = this.loyaltyCardAmount;
                this.loyaltyCardAmt(Utils.format(this.channelCurrencyTemplate, Utils.formatNumber(this.loyaltyCardAmount)));
                this.tenderLines.push(tenderLine);
            }

            if (this.payGiftCard()) {
                var tenderLine = new TenderDataLineClass(null);
                tenderLine.Amount = this.giftCardAmount;
                tenderLine.GiftCardId = this.giftCardNumber();
                this.giftCardAmt(Utils.format(this.channelCurrencyTemplate, Utils.formatNumber(this.giftCardAmount)));
                this.tenderLines.push(tenderLine);
            }

            this.showCheckoutFragment(this._checkoutFragments.Review);
        }

        private updateCheckoutCart(event, data) {
            // Handles the UpdateCheckoutCart event.
            if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                this.errorMessage(this.errorMessage() + data.Errors[0].ErrorMessage);
                this.showError(true);
            }
            else {
                this.updateSelectedShippingOptions(data.ShoppingCart);
            }
        }

        private static isRequestedDeliveryPreferenceApplicable(deliveryPreferenceTypes: DeliveryPreferenceType[], reqDeliveryPreferenceType: DeliveryPreferenceType): boolean {
            for (var i = 0; i < deliveryPreferenceTypes.length; i++) {
                if (deliveryPreferenceTypes[i] == reqDeliveryPreferenceType) {
                    return true;
                }
            }

            return false;
        }

        /* Service calls */

        private getShoppingCart() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);
            ShoppingCartService.GetShoppingCart(true, ShoppingCartDataLevel.All)
                .done((data) => {
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);       
                    
                    if (this.displayPromotionBanner()) {
                        this.getPromotions();
                    }       
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_63);
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private commenceCheckout() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);    

            ShoppingCartService.CommenceCheckout(ShoppingCartDataLevel.All)
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(this.errorMessage() + data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        this.showCheckoutFragment(this._checkoutFragments.DeliveryPreferences);
                        this.getChannelConfigurationAndTenderTypes();
                        this.getDeliveryPreferences();

                        if (this.displayPromotionBanner()) {
                            this.getPromotions();
                        }

                        this.createEditRewardCardDialog();
                        this.createDiscountCodeDialog();
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(this.errorMessage() + Resources.String_63);
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private getPromotions() {
            ShoppingCartService.GetPromotions(true, ShoppingCartDataLevel.All)
                .done((data) => {
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_177); // Sorry, something went wrong. The cart promotions information couldn't be retrieved. Please refresh the page and try again.
                    this.showError(true);
                });
        }

        private getAllDeliveryOptionDescriptions() {
            CheckoutService.GetDeliveryOptionsInfo()
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(this.errorMessage() + data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        this.allDeliveryOptionDescriptions = data.DeliveryOptions;
                    }
                })
                .fail((errors) => {
                    this.errorMessage(this.errorMessage() + Resources.String_160); // Sorry, something went wrong. An error occurred while trying to get delivery methods information. Please refresh the page and try again.
                    this.showError(true);
                });;
        }

        private getChannelConfigurationAndTenderTypes() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);

            ChannelService.GetChannelConfiguration()
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(this.errorMessage() + data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        this.channelCurrencyTemplate = data.ChannelConfiguration.CurrencyStringTemplate;
                        this.bingMapsToken = data.ChannelConfiguration.BingMapsApiKey;
                        this.pickUpInStoreDeliveryModeCode = data.ChannelConfiguration.PickupDeliveryModeCode;
                        this.emailDeliveryModeCode = data.ChannelConfiguration.EmailDeliveryModeCode;
                        this.giftCardItemId = data.ChannelConfiguration.GiftCardItemId;
                        var paymentTotalValue = Utils.parseNumberFromLocaleString(this.paymentTotal());
                        this.paymentTotal(Utils.format(this.channelCurrencyTemplate, Utils.formatNumber(paymentTotalValue)));

                        this.getTenderTypes();
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(this.errorMessage() + Resources.String_98); // Sorry, something went wrong. The channel configuration could not be retrieved successfully. Please refresh the page and try again..
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private getDeliveryPreferences() {
            CheckoutService.GetDeliveryPreferences()
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(this.errorMessage() + data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        this.cartDeliveryPreferences = data.CartDeliveryPreferences;
                        var headerLevelDeliveryPreferenceTypes = this.cartDeliveryPreferences.HeaderDeliveryPreferenceTypes;
                        this.hasShipToAddress = Checkout.isRequestedDeliveryPreferenceApplicable(headerLevelDeliveryPreferenceTypes, DeliveryPreferenceType.ShipToAddress);
                        this.hasPickUpInStore = Checkout.isRequestedDeliveryPreferenceApplicable(headerLevelDeliveryPreferenceTypes, DeliveryPreferenceType.PickupFromStore);
                        this.hasEmail = Checkout.isRequestedDeliveryPreferenceApplicable(headerLevelDeliveryPreferenceTypes, DeliveryPreferenceType.ElectronicDelivery);
                        this.hasMultiDeliveryPreference = Checkout.isRequestedDeliveryPreferenceApplicable(headerLevelDeliveryPreferenceTypes, DeliveryPreferenceType.DeliverItemsIndividually);

                        this.updateDeliveryPreferences();
                        this.createItemDeliveryPreferenceDialog();
                        
                    }
                })
                .fail((errors) => {
                    this.errorMessage(this.errorMessage() + Resources.String_98); // Sorry, something went wrong. The channel configuration could not be retrieved successfully. Please refresh the page and try again..
                    this.showError(true);
                });
        }

        private removeFromCartClick(item) {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_179); // Updating shopping cart ...

            ShoppingCartService.RemoveFromCart(true, item.LineId, ShoppingCartDataLevel.All)
                .done((data) => {
                    this.getDeliveryPreferences()
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    if (this.displayPromotionBanner()) {
                        this.getPromotions();
                    }       
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_64);
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private updateQuantity(items: TransactionItem[]) {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_179); // Updating shopping cart ...

            ShoppingCartService.UpdateQuantity(true, items, ShoppingCartDataLevel.All)
                .done((data) => {
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);

                    if (this.displayPromotionBanner()) {
                        this.getPromotions();
                    }       
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_65);
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private applyPromotionCode(cart: ShoppingCart, valueAccesor) {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_179); // Updating shopping cart ...
            var discountCode = this._addDiscountCodeDialog.find('#DiscountCodeTextBox').val();
            if (!Utils.isNullOrWhiteSpace(discountCode)) {
                ShoppingCartService.AddOrRemovePromotion(true, discountCode, true, ShoppingCartDataLevel.All)
                    .done((data) => {
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                        this.closeDiscountCodeDialog();

                        if (this.displayPromotionBanner()) {
                            this.getPromotions();
                        }       
                    })
                    .fail((errors) => {
                        this.closeDiscountCodeDialog();
                        this.errorMessage(Resources.String_93); /* Sorry, something went wrong. The promotion code could not be added successfully. Please refresh the page and try again. */
                        this.showError(true);
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    });
            }
        }

        private removePromotionCode(cart: ShoppingCart, valueAccesor) {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_179); // Updating shopping cart ...
            var srcElement = valueAccesor.target;

            if (!Utils.isNullOrUndefined(srcElement)
                && !Utils.isNullOrUndefined(srcElement.parentElement)
                && !Utils.isNullOrUndefined(srcElement.parentElement.lastElementChild)
                && !Utils.isNullOrWhiteSpace(srcElement.parentElement.lastElementChild.textContent)) {
                var promoCode = srcElement.parentElement.lastElementChild.textContent;

                ShoppingCartService.AddOrRemovePromotion(true, promoCode, false, ShoppingCartDataLevel.All)
                    .done((data) => {
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);

                        if (this.displayPromotionBanner()) {
                            this.getPromotions();
                        }       
                    })
                    .fail((errors) => {
                        this.errorMessage(Resources.String_94); /* Sorry, something went wrong. The promotion code could not be removed successfully. Please refresh the page and try again. */
                        this.showError(true);
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    });
            }
            else {
                LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
            }
        }

        private getOrderDeliveryOptions() {
            this.resetSelectedOrderShippingOptions();
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);
            var shipToAddress = new AddressClass(null);
            shipToAddress = this.cart().SelectedDeliveryOption.CustomAddress;

            CheckoutService.GetOrderDeliveryOptionsForShipping(shipToAddress)
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        this.availableDeliveryOptions(data.DeliveryOptions);

                        var selectedOrderDeliveryOption = new SelectedDeliveryOptionClass(null);
                        selectedOrderDeliveryOption.DeliveryPreferenceId = DeliveryPreferenceType.ShipToAddress.toString();
                        selectedOrderDeliveryOption.CustomAddress = shipToAddress;
                         
                        //Make a default selection if there is only one delivery option available.
                        if (this.availableDeliveryOptions().length == 1) {
                            selectedOrderDeliveryOption.DeliveryModeText = this.availableDeliveryOptions()[0].Description;
                            selectedOrderDeliveryOption.DeliveryModeId = this.availableDeliveryOptions()[0].Id;
                        }

                        this.orderDeliveryOption(selectedOrderDeliveryOption);
                        this.errorPanel.hide();
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_66);
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });;
        }

        private getItemDeliveryOptions() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);
            var selectedLineShippingInfo: SelectedLineShippingInfo[] = [];
            var currentLineDeliveryOption: SelectedLineDeliveryOption = this.selectedLineDeliveryOption();
            selectedLineShippingInfo[0] = {};
            selectedLineShippingInfo[0].LineId = currentLineDeliveryOption.LineId;
            selectedLineShippingInfo[0].ShipToAddress = currentLineDeliveryOption.CustomAddress;           

            CheckoutService.GetLineDeliveryOptionsForShipping(selectedLineShippingInfo)
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        if (!Utils.isNullOrEmpty(data.LineDeliveryOptions)) {
                            for (var i = 0; i < data.LineDeliveryOptions.length; i++) {
                                if (data.LineDeliveryOptions[i].LineId == this.selectedLineDeliveryOption().LineId) {
                                    this.availableDeliveryOptions(data.LineDeliveryOptions[i].DeliveryOptions);
                                }
                            }
                        }
                        else {
                            this.errorMessage(Resources.String_66); // Sorry, something went wrong. Delivery methods could not be retrieved. Please refresh the page and try again.
                            this.showError(true);
                            LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                            return;
                        }


                        //Make a default selection if there is only one delivery option available.
                        if (this.availableDeliveryOptions().length == 1) {
                            this.selectedLineDeliveryOption().DeliveryModeText = this.availableDeliveryOptions()[0].Description;
                            this.selectedLineDeliveryOption().DeliveryModeId = this.availableDeliveryOptions()[0].Id;
                            this.selectedLineDeliveryOption(this.selectedLineDeliveryOption()); // This shows the above update in UI.
                        }

                        this.errorPanel.hide();
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_66); // Sorry, something went wrong. Delivery methods could not be retrieved. Please refresh the page and try again.
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private getNearbyStoresWithAvailabilityService() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);
            var items;

            if (this.selectedOrderDeliveryPreference() == DeliveryPreferenceType.DeliverItemsIndividually) {
                items = [this.item()];
            }
            else if (this.selectedOrderDeliveryPreference() == DeliveryPreferenceType.PickupFromStore) {
                items = this.cart().Items;
            }

            StoreProductAvailabilityService.GetNearbyStoresWithAvailability(this.searchLocation.latitude, this.searchLocation.longitude, items)
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        if (Utils.isNullOrEmpty(data.Stores) || data.Stores.length == 0) {
                            this.resetSelectedOrderShippingOptions();
                            this.displayLocations(null);
                            this._availableStoresView.hide();
                            this.errorMessage(Resources.String_107); // There are no stores around the location you searched. Please update your delivery preferences and try again.
                            this.showError(true);
                        }
                        else {
                            this.stores = data.Stores;
                            this.updateItemAvailibilities();
                            this.renderAvailableStores();
                            this.errorPanel.hide();
                        }
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_108); // Sorry, something went wrong. An error occurred while trying to get stores. Please refresh the page and try again.
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private getNearbyStoresService() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);

            StoreProductAvailabilityService.GetNearbyStores(this.searchLocation.latitude, this.searchLocation.longitude)
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        if (Utils.isNullOrEmpty(data.Stores) || data.Stores.length == 0) {
                            this.resetSelectedOrderShippingOptions();
                            this.displayLocations(null);
                            this._availableStoresView.hide();
                            this.errorMessage(Resources.String_107); // Products are not available for pick up in the stores around the location you searched. Please update your delivery preferences and try again.
                            this.showError(true);
                        }
                        else {
                            this.stores = data.Stores;
                            this.renderAvailableStores();
                            this.errorPanel.hide();
                        }
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_108); // Sorry, something went wrong. An error occurred while trying to get stores. Please refresh the page and try again.
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private getCountryRegionInfoService() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);

            ChannelService.GetCountryRegionInfo()
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(this.errorMessage() + data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        if (Utils.isNullOrUndefined(data.Countries) || data.Countries.length == 0) {
                            this.errorMessage(this.errorMessage() + Resources.String_165); // Sorry, something went wrong. An error occurred while retrieving the country region information. Please refresh the page and try again.
                            this.showError(true);
                        }
                        else {
                            this.countries(data.Countries);
                        }
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(this.errorMessage() + Resources.String_165); // Sorry, something went wrong. An error occurred while retrieving the country region information. Please refresh the page and try again.
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private getStateProvinceInfoService(countryCode: string) {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);
            if (!Utils.isNullOrWhiteSpace(countryCode)) {
                ChannelService.GetStateProvinceInfo(countryCode)
                    .done((data) => {
                        if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                            this.errorMessage(data.Errors[0].ErrorMessage);
                            this.showError(true);
                        }
                        else {
                            this.states(data.StateProvinces); // Initialize the states observable array with the collection returned by the service. 

                            if (this._checkoutView.find(" ." + this._checkoutFragments.DeliveryPreferences).is(":visible")) { // If in delivery preferences section
                                var tempAddress = this.tempShippingAddress();
                                if (this.selectedOrderDeliveryPreference() == DeliveryPreferenceType.ShipToAddress) {
                                    // If chosen delivery preference is ship to address and a state value exists already use that, 
                                    // else initialize with empty value.
                                    if (!Utils.isNullOrUndefined(this.cart().SelectedDeliveryOption.CustomAddress) && !Utils.isNullOrUndefined(this.cart().SelectedDeliveryOption.CustomAddress.State)) {
                                        tempAddress.State = this.cart().SelectedDeliveryOption.CustomAddress.State;
                                    }
                                    else {
                                        tempAddress.State = '';
                                    }
                                }
                                else if (this.itemSelectedDeliveryPreference() == DeliveryPreferenceType.ShipToAddress && !Utils.isNullOrUndefined(this.selectedLineDeliveryOption().CustomAddress) && Utils.isNullOrUndefined(this.selectedLineDeliveryOption().CustomAddress.State)) {
                                    // If chosen delivery preference is item level ship to address and a state value does not exist already initialize with empty value.
                                    tempAddress.State = '';
                                }

                                this.tempShippingAddress(tempAddress);
                            }
                            else if (!Utils.isNullOrUndefined(this.paymentCard().PaymentAddress) && Utils.isNullOrUndefined(this.paymentCard().PaymentAddress.State)) {
                                // If in the payment section and a state value does not exist already for payment address initialize with empty value.
                                this.paymentCard().PaymentAddress.State = '';
                            }
                        }

                        this.errorPanel.hide();
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    })
                    .fail((errors) => {
                        this.errorMessage(Resources.String_185); // Sorry, something went wrong. An error occurred while retrieving the state/province information. Please refresh the page and try again.
                        this.showError(true);
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    });
            }
        }

        private isAuthenticatedSession() {
            CheckoutService.IsAuthenticatedSession()
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(this.errorMessage() + data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        this.isAuthenticated = data.IsTrue;

                        if (this.isAuthenticated) {
                            this.getUserEmail();
                            this.getUserAddresses();
                        }
                    }
                })
                .fail((errors) => {
                    this.isAuthenticated = false;
                });
        }

        private getUserEmail() {
            CheckoutService.GetUserEmail()
                .done((data) => {                    
                    this.email = data.Value;
                })
                .fail((errors) => {
                });
        }

        private getUserAddresses() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);

            CustomerService.GetAddresses()
                .done((data) => {
                    var addresses = [];

                    for (var i = 0; i < data.Addresses.length; i++) {
                        var address = data.Addresses[i];

                        if (Utils.isNullOrWhiteSpace(address.Name) &&
                            Utils.isNullOrWhiteSpace(address.Street) &&
                            Utils.isNullOrWhiteSpace(address.City) &&
                            Utils.isNullOrWhiteSpace(address.State) &&
                            Utils.isNullOrWhiteSpace(address.ZipCode)) {

                            // If all of the above properties are empty, do not show address in the select address drop down.
                            continue;
                        }

                        var delimiter = Utils.isNullOrWhiteSpace(address.State) && Utils.isNullOrWhiteSpace(address.ZipCode) ? "" : ", ";
                        var addressString = Utils.format("({0}) {1} {2}{3}{4} {5}", address.Name, address.Street, address.City, delimiter, address.State, address.ZipCode);                        
                        addresses.push({ Value: address, Text: addressString });
                    }

                    if (addresses.length > 0) {
                        this.addresses(addresses);
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private setDeliveryOptions() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);

            var orderDeliveryOption: SelectedDeliveryOption = this.orderDeliveryOption();
            orderDeliveryOption.DeliveryPreferenceId = orderDeliveryOption.DeliveryPreferenceId;

            CheckoutService.SetOrderDeliveryOption(orderDeliveryOption, ShoppingCartDataLevel.All)
                .done((data) => {
                    if (Utils.isNullOrUndefined(data.Errors) || data.Errors.length <= 0) {
                        if (!Utils.isNullOrWhiteSpace(this.errorMessage() + this.errorMessage)) {
                            this.errorPanel.show();
                        }

                        this.showCheckoutFragment(this._checkoutFragments.PaymentInformation);
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);

                    if (this.displayPromotionBanner()) {
                        this.getPromotions();
                    }       
                })
                .fail((errors) => {
                    this.errorMessage(this.errorMessage() + Resources.String_67);
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private setItemShippingOptions() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);
            var selectedLineDeliveryOptions: SelectedLineDeliveryOption[] = []; 
            var cart = this.cart();

            for (var i = 0; i < cart.Items.length; i++) {
                selectedLineDeliveryOptions[i] = {};
                selectedLineDeliveryOptions[i].LineId = cart.Items[i].LineId;
                selectedLineDeliveryOptions[i].CustomAddress = cart.Items[i].SelectedDeliveryOption.CustomAddress;
                selectedLineDeliveryOptions[i].DeliveryModeId = cart.Items[i].SelectedDeliveryOption.DeliveryModeId;
                selectedLineDeliveryOptions[i].DeliveryPreferenceId = cart.Items[i].SelectedDeliveryOption.DeliveryPreferenceId;
                selectedLineDeliveryOptions[i].ElectronicDeliveryEmail = cart.Items[i].SelectedDeliveryOption.ElectronicDeliveryEmail;
                selectedLineDeliveryOptions[i].ElectronicDeliveryEmailContent = cart.Items[i].SelectedDeliveryOption.ElectronicDeliveryEmailContent;
                selectedLineDeliveryOptions[i].StoreAddress = cart.Items[i].SelectedDeliveryOption.StoreAddress;
            }

            CheckoutService.SetLineDeliveryOptions(selectedLineDeliveryOptions, ShoppingCartDataLevel.All)
                .done((data) => {
                    if (Utils.isNullOrUndefined(data.Errors) || data.Errors.length <= 0) {
                        if (!Utils.isNullOrWhiteSpace(this.errorMessage() + this.errorMessage)) {
                            this.errorPanel.show();
                        }

                        this.showCheckoutFragment(this._checkoutFragments.PaymentInformation);
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);

                    if (this.displayPromotionBanner()) {
                        this.getPromotions();
                    }       
                })
                .fail((errors) => {
                    this.errorMessage(this.errorMessage() + Resources.String_67); // Sorry, something went wrong. The shipping information was not stored successfully. Please refresh the page and try again.
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private getPaymentCardTypes() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);

            CheckoutService.GetPaymentCardTypes()
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(this.errorMessage() + data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        this.paymentCardTypes(data.CardTypes);
                        if (Utils.isNullOrWhiteSpace(this.errorMessage)) {
                            this.errorPanel.hide();
                        }
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(this.errorMessage() + Resources.String_68);
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private getLoyaltyCards() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);

            LoyaltyService.GetLoyaltyCards()
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        var loyaltyCards = [];
                        var _customLoyaltyRadio = this._paymentView.find("#CustomLoyaltyRadio");
                        var containsValidLoyaltyCard = false;

                        for (var i = 0; i < data.LoyaltyCards.length; i++) {
                            if (data.LoyaltyCards[i].CardTenderType == LoyaltyCardTenderType.AsCardTender || 
                                data.LoyaltyCards[i].CardTenderType == LoyaltyCardTenderType.AsContactTender) {
                                loyaltyCards.push(data.LoyaltyCards[i].CardNumber);
                                containsValidLoyaltyCard = true;
                            }
                        }

                        if (!containsValidLoyaltyCard) {
                            _customLoyaltyRadio.hide();
                        }
                        else {
                            _customLoyaltyRadio.show();

                            // Selecting the first loyalty card by default
                            this.loyaltyCardNumber(loyaltyCards[0].CardNumber);
                        }

                        this.loyaltyCards(loyaltyCards);
                        this.errorPanel.hide();
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_150); // Sorry, something went wrong. An error occurred while trying to get loyalty card information. Please refresh the page and try again
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private updateLoyaltyCardId() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_179); // Updating shopping cart ...
            var loyaltyCardId = this._editRewardCardDialog.find('#RewardCardTextBox').val();

            if (!Utils.isNullOrWhiteSpace(loyaltyCardId)) {
                LoyaltyService.UpdateLoyaltyCardId(true, loyaltyCardId, ShoppingCartDataLevel.All)
                    .done((data) => {
                        this.closeEditRewardCardDialog();
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);

                        if (this.displayPromotionBanner()) {
                            this.getPromotions();
                        }       
                    })
                    .fail((errors) => {
                        this.errorMessage(Resources.String_164); // Sorry, something went wrong. An error occurred while trying to update reward card id in cart. Please refresh the page and try again. 
                        this.showError(true);
                        this.closeEditRewardCardDialog();
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    });
            }
        }

        private getTenderTypes() {
            ChannelService.GetTenderTypes()
                .done((data) => {
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        this.errorMessage(data.Errors[0].ErrorMessage);
                        this.showError(true);
                    }
                    else {
                        this.checkForGiftCardInCart();

                        // If the channel supports credit card tender type display the credit card payment panel.
                        if (data.HasCreditCardPayment) {
                            this._paymentView.find('.msax-PayCreditCard').show();
                            this._paymentView.find('.msax-PayCreditCard .msax-PayCreditCardLink').show();
                            this._creditCardPanel.hide();
                        }
                        else {
                            this._paymentView.find('.msax-PayCreditCard').hide();
                        }

                        // If the cart has any gift card item then payment by gift card is not allowed.
                        // If the channel supports gift card tender type display the gift card payment panel.
                        if (!this.anyGiftCard && data.HasGiftCardPayment) {
                            this._paymentView.find('.msax-PayGiftCard').show();
                            this._giftCardPanel.hide();
                            this._paymentView.find('.msax-PayGiftCard .msax-PayGiftCardLink').show();
                        }
                        else {
                            this._paymentView.find('.msax-PayGiftCard').hide();
                        }

                        // If the channel supports loyalty card tender type display the loyalty card payment panel.
                        if (data.HasLoyaltyCardPayment) {
                            this._paymentView.find('.msax-PayLoyaltyCard').show();
                            this._loyaltyCardPanel.hide();
                            this._paymentView.find('.msax-PayLoyaltyCard .msax-PayLoyaltyCardLink').show();
                        }
                        else {
                            this._paymentView.find('.msax-PayLoyaltyCard').hide();
                        }

                        this.removeValidation(this._creditCardPanel);
                        this.removeValidation(this._giftCardPanel);
                        this.removeValidation(this._loyaltyCardPanel);
                    }
                })
                .fail((errors) => {
                    this.errorMessage(this.errorMessage() + Resources.String_138); // Sorry, something went wrong. An error occurred while trying to get payment methods supported by the store. Please refresh the page and try again.
                    this.showError(true);
                });
        }

        private getGiftCardBalance() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);
            this._paymentView.find('.msax-GiftCardBalance').hide();
            if (!Utils.isNullOrWhiteSpace(this.giftCardNumber())) {
                CheckoutService.GetGiftCardBalance(this.giftCardNumber())
                    .done((data) => {
                        if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                            this.errorMessage(data.Errors[0].ErrorMessage);
                            this.showError(true);
                        }
                        else {
                            if (!data.GiftCardInformation.IsInfoAvailable) {
                                this.isGiftCardInfoAvailable(false);
                            }
                            else {

                                // If user enters amount directly into the gift card amount text box,
                                // then these validations will have to be performed on tender line creation
                                if (this.checkGiftCardAmountValidity) {

                                    if (Number(data.GiftCardInformation.Balance) < Number(this.giftCardAmount)) {
                                        this.errorMessage(Resources.String_148); // Gift card does not have sufficient balance
                                        this.showError(false);
                                    }
                                }

                                this.isGiftCardInfoAvailable(true);
                                this.giftCardBalance(data.GiftCardInformation.BalanceWithCurrency);
                            }

                            this._paymentView.find('.msax-GiftCardBalance').show();
                            this.errorPanel.hide();

                            if (data.GiftCardInformation.IsInfoAvailable && this.checkGiftCardAmountValidity) {
                                this.createPaymentCardTenderLine();
                            }

                            this.checkGiftCardAmountValidity = false;
                        }

                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    })
                    .fail((errors) => {
                        this.errorMessage(Resources.String_145); // Sorry, something went wrong. An error occurred while trying to get gift card balance. Please refresh the page and try again
                        this.showError(true);
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                        this.checkGiftCardAmountValidity = false;
                    });
            }
            else {
                this.errorMessage(Resources.String_144); // Please enter a gift card number
                this.showError(false);
                LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                this.checkGiftCardAmountValidity = false;
            }
        }

        private applyFullGiftCardAmount() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);
            if (!Utils.isNullOrWhiteSpace(this.giftCardNumber())) {
                CheckoutService.GetGiftCardBalance(this.giftCardNumber())
                    .done((data) => {
                        if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                            this.errorMessage(data.Errors[0].ErrorMessage);
                            this.showError(true);
                        }
                        else {
                            var totalAmount = this.cart().TotalAmount;
                            var giftCardBalance = data.GiftCardInformation.Balance;
                            var giftCardBalanceWithCurrency = data.GiftCardInformation.BalanceWithCurrency;
                            var _giftCardTextBox = this._paymentView.find('#GiftCardAmount');
                            if (!data.GiftCardInformation.IsInfoAvailable) {
                                this.isGiftCardInfoAvailable(false);
                            }
                            else {
                                this.isGiftCardInfoAvailable(true);
                                this.giftCardBalance(giftCardBalance);

                                if (Number(giftCardBalance) <= Number(totalAmount)) {
                                    // Using up gift card balance.
                                    _giftCardTextBox.val(giftCardBalanceWithCurrency);
                                    this.updatePaymentTotal();
                                }
                                else {
                                    // Paying full order total using gift card.
                                    _giftCardTextBox.val(totalAmount);
                                    this._creditCardPanel.hide();
                                    this._paymentView.find('.msax-PayCreditCard .msax-PayCreditCardLink').show();
                                    this.payCreditCard(false);

                                    this._loyaltyCardPanel.hide();
                                    this._paymentView.find('.msax-PayLoyaltyCard .msax-PayLoyaltyCardLink').show();
                                    this.payLoyaltyCard(false);
                                }
                            }

                            this._paymentView.find('.msax-GiftCardBalance').show();
                            this.errorPanel.hide();
                        }

                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    })
                    .fail((errors) => {
                        this.errorMessage(Resources.String_145); // // Sorry, something went wrong. An error occurred while trying to get gift card balance. Please refresh the page and try again
                        this.showError(true);
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    });
            }
            else {
                this.errorMessage(Resources.String_144); // Please enter a gift card number
                this.showError(false);
                LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
            }
        }

        private submitOrder() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_180); // Submitting order ...

            CheckoutService.SubmitOrder(this.tenderLines, this.paymentCard().PaymentAddress.Email)
                .done((data) => {

                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        switch (data.Errors[0].ErrorCode) {
                            case "Microsoft_Dynamics_Commerce_Runtime_BlockedLoyaltyCard":
                                this.errorMessage(Resources.String_154); // The loyalty card is blocked
                                break;
                            case "Microsoft_Dynamics_Commerce_Runtime_NoTenderLoyaltyCard":
                                this.errorMessage(Resources.String_155); // The loyalty card is not allowed for payment
                                break;
                            case "Microsoft_Dynamics_Commerce_Runtime_NotEnoughRewardPoints":
                                this.errorMessage(Resources.String_156); // The loyalty payment amount exceeds what is allowed for this loyalty card in this transaction
                                break;
                            case "Microsoft_Dynamics_Commerce_Runtime_InvalidLoyaltyCardNumber":
                                this.errorMessage(Resources.String_157); // The loyalty card number does not exist
                                break;
                            default:
                                this.errorMessage(data.Errors[0].ErrorMessage);
                                break;
                        }

                        this.showError(false);
                    }
                    else {
                        this.orderNumber(data.OrderNumber);
                        this.errorPanel.hide();
                        if (Utils.isNullOrWhiteSpace(msaxValues.msax_OrderConfirmationUrl)) {
                            this.showCheckoutFragment(this._checkoutFragments.Confirmation);
                        }
                        else {
                            window.location.href = msaxValues.msax_OrderConfirmationUrl += '?confirmationId=' + data.OrderNumber;
                        }
                    }

                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);                 
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_69);
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });         
        }    
    }
}