﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../ExternalScripts//JQuery.d.ts" />
/// <reference path="../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../Common/Helpers/Core.ts" />
/// <reference path="../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../Resources/Resources.ts" />

module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    "use strict";

    export class MiniCart {

        private _cartView;
        private _miniCart;
        private cart;
        private isShoppingCartEnabled;
        private errorMessage;
        private errorPanel;
        private isCheckoutCart;
        private isMiniCartVisible: boolean;

        constructor(element) {
            this._cartView = $(element);
            this.errorMessage = ko.observable<string>('');
            this.errorPanel = this._cartView.find(" .msax-ErrorPanel");
            this._miniCart = this._cartView.find(" > .msax-MiniCart");
            this.isCheckoutCart = ko.observable<boolean>(Utils.isNullOrUndefined(msaxValues.msax_IsCheckoutCart) ? false : msaxValues.msax_IsCheckoutCart.toLowerCase() == "true");

            if (!this.isCheckoutCart()) {
                this.getShoppingCart();
            }

            var cart = new ShoppingCartClass(null);
            cart.Items = [];
            cart.DiscountCodes = [];
            this.cart = ko.observable<ShoppingCart>(cart);

            if (this.isCheckoutCart()) {
                // Subscribing to the UpdateCheckoutCart event.
                ShoppingCartService.OnUpdateCheckoutCart(this, this.updateCart);
            }
            else {
                // Subscribing to the UpdateShoppingCart event.
                ShoppingCartService.OnUpdateShoppingCart(this, this.updateCart);
            }

            // Handles the keypress event on the control.
            this._cartView.keypress(function (event) {
                if (event.keyCode == 13 /* enter */ || event.keyCode == 8 /* backspace */ || event.keyCode == 27 /* esc */) {
                    event.preventDefault();
                    return false;
                }

                return true;
            });

            // Computed observables.
            this.isShoppingCartEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().Items);
            });
        }

        private getResx(key: string) {
            // Gets the resource value.
            return Resources[key];
        }

        private shoppingCartNextClick(viewModel: Checkout, event) {
            // If redirection url is specified redirect to the url on button click.
            if (!Utils.isNullOrWhiteSpace(msaxValues.msax_CheckoutUrl)) {
                window.location.href = msaxValues.msax_CheckoutUrl;
            }
        }

        private disableUserActions() {
            this._cartView.find('*').disabled = true;
        }

        private enableUserActions() {
            this._cartView.find('*').disabled = false;
        }

        private showError(isError: boolean) {
            // Displays the error message on the error panel.
            if (isError) {
                this.errorPanel.addClass("msax-Error");
            }
            else if (this.errorPanel.hasClass("msax-Error")) {
                this.errorPanel.removeClass("msax-Error");
            }

            this.errorPanel.show();
            $(window).scrollTop(0);
        }

        private viewCartClick() {
            // If redirection url is specified redirect to the url on click.
            if (!Utils.isNullOrWhiteSpace(msaxValues.msax_ShoppingCartUrl)) {
                window.location.href = msaxValues.msax_ShoppingCartUrl;
            }
        }

        private showMiniCart() {
            // display mini cart
            this.toggleCartDisplay(true);
        }

        private hideMiniCart() {
            // hide mini cart
            this.toggleCartDisplay(false);
        }

        private toggleCartDisplay(show: boolean) {
            // Setting a default location from the top of the screen for the mini cart.            
            var locationOffScreen = -300;

            // Increasing the value of the default location based on the number of items in cart.
            if (this.isShoppingCartEnabled()) {
                locationOffScreen = locationOffScreen * this.cart().Items.length;
            }

            // toggle the display of mini cart
            if (show) {                
                this.isMiniCartVisible = false;

                setTimeout($.proxy(function () {
                    if (!this.isMiniCartVisible) {
                        this._miniCart.animate({ top: 45 }, 300, 'linear');
                    }
                }, this), 500);
            }
            else {               
                this.isMiniCartVisible = true;

                setTimeout($.proxy(function () {
                    if (this.isMiniCartVisible) {
                        this._miniCart.animate({ top: locationOffScreen }, 300);
                    }
                }, this), 500);
            }
        }

        private updateCart(event, data) {
            // Event handler for UpdateShoppingCart/UpdateCheckoutCart events.
            if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                this.errorMessage(data.Errors[0].ErrorMessage);
                this.showError(true);
            }
            else {
                this.cart(data.ShoppingCart);
            }

            this.hideMiniCart();
        }

        // Service calls

        private getShoppingCart() {
            this.disableUserActions();

            ShoppingCartService.GetShoppingCart(false, ShoppingCartDataLevel.All)
                .done((data) => {
                    this.enableUserActions();
                    this.errorPanel.hide();
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_63);
                    this.showError(true);
                    this.enableUserActions();
                });
        }

        private removeFromCartClick(item) {
            this.disableUserActions();

            ShoppingCartService.RemoveFromCart(this.isCheckoutCart(), item.LineId, ShoppingCartDataLevel.All)
                .done((data) => {
                    this.enableUserActions();
                    this.errorPanel.hide();
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_64);
                    this.showError(true);
                    this.enableUserActions();
                });
        }
    }
}