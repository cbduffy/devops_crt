﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;

[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.ShoppingCart.ShoppingCart.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.ShoppingCart.ShoppingCart.mobile.css", "text/css", PerformSubstitution = true)]
[assembly: WebResource("Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls.ShoppingCart.Images.i_ShoppingCart_24.png", "image/png")]
namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls
{
    /// <summary>
    /// Mini cart control.
    /// </summary>
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    [ToolboxData("<{0}:MiniCart runat=server></{0}:MiniCart>")]
    [ComVisible(false)]
    public class MiniCart : RetailWebControl
    {
        /// <summary>
        /// Gets or sets the value indicating the checkout redirect url.
        /// </summary>
        public string CheckoutUrl { get; set; }

        /// <summary>
        /// Gets or sets the value indicating the shopping cart redirect url.
        /// </summary>
        public string ShoppingCartUrl { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the cart is in checkout session or not.
        /// </summary>
        public bool IsCheckoutCart { get; set; }

        /// <summary>
        /// Gets the markup to include control scripts, css, startup scripts in the page.
        /// </summary>
        /// <returns>The header markup.</returns>
        internal string GetHeaderMarkup()
        {
            string output = string.Empty;

            this.GetCssUrls();
            this.GetScriptUrls();

            // Preventing markup getting registered multiple times in a page when using multiple controls.
            string existingHeaderMarkup = string.Empty;
            if (this.Page != null && this.Page.Header != null)
            {
                foreach (Control control in this.Page.Header.Controls)
                {
                    if (control.GetType() == typeof(LiteralControl))
                    {
                        LiteralControl literalControl = (LiteralControl)control;
                        existingHeaderMarkup += literalControl.Text;                        
                    }
                }
            }

            foreach (string cssUrl in this.cssUrls)
            {                
                string cssLink = String.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />", cssUrl);

                // If page header is visible here, check if the markup is present in the header already.
                if (this.Page == null || this.Page.Header == null || !existingHeaderMarkup.Contains(cssLink))
                {
                    output += cssLink;
                }
            }

            foreach (string scriptUrl in this.scriptUrls)
            {
                string scriptLink = String.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", scriptUrl);

                // If page header is visible here, check if the markup is present in the header already.
                if (this.Page == null || this.Page.Header == null || !existingHeaderMarkup.Contains(scriptLink))
                {
                    output += scriptLink;
                }
            }

            output += this.GetStartupMarkup(existingHeaderMarkup);

            return output;
        }

        /// <summary>
        /// Gets the control html markup with the control class wrapper added.
        /// </summary>
        /// <returns>The control markup.</returns>
        internal string GetControlMarkup()
        {
            string htmlContent = this.GetHtml();

            // Adding wrapper with the control cssClass. 
            return "<div class=\"" + cssClass + "\">" + htmlContent + "</div>";
        }

        /// <summary>
        /// Gets the script urls.
        /// </summary>
        /// <returns>The script urls.</returns>
        protected override Collection<string> GetScriptUrls()
        {
            base.GetScriptUrls();
            return this.scriptUrls;
        }

        /// <summary>
        /// Gets the css urls.
        /// </summary>
        /// <returns>The css urls.</returns>
        protected override Collection<string> GetCssUrls()
        {
            base.GetCssUrls();
            this.AddCssUrl("ShoppingCart.ShoppingCart.css");

            // If rendering mobile view then register ShoppingCart.mobile.css in addition
            if (IsMobileView)
            {
                this.AddCssUrl("ShoppingCart.ShoppingCart.mobile.css");
            }

            return this.cssUrls;
        }

        /// <summary>
        /// Gets the startup markup.
        /// </summary>
        /// <param name="existingHeaderMarkup">>Existing header markup to determine if startup script registration is required.</param>
        /// <returns>The startup markup.</returns>
        protected override string GetStartupMarkup(string existingHeaderMarkup)
        {
            string startupScript = base.GetStartupMarkup(existingHeaderMarkup);

            string cartStartupScript = string.Format(@"<script type='text/javascript'> msaxValues['msax_CheckoutUrl'] = '{0}'; msaxValues['msax_IsCheckoutCart'] = '{1}'; msaxValues['msax_ShoppingCartUrl'] = '{2}'; </script>", CheckoutUrl, IsCheckoutCart, ShoppingCartUrl);

            // If page header is visible here, check if the markup is present in the header already.
            if (this.Page == null || this.Page.Header == null || (existingHeaderMarkup != null && !existingHeaderMarkup.Contains(cartStartupScript)))
            {
                startupScript += cartStartupScript;
            }

            return startupScript;
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            string headerMarkup = this.GetHeaderMarkup();
            base.RegisterHeaderMarkup(headerMarkup);
        }

        /// <summary>
        /// Renders the contents of the control to the specified writer. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (writer != null)
            {
                string htmlContent = this.GetHtml();
                writer.Write(htmlContent);
            }
        }

        /// <summary>
        /// Gets the control html markup.
        /// </summary>
        /// <returns>The control markup.</returns>
        private string GetHtml()
        {
            // If in mobile mode, render MiniCart.mobile.html
            return (IsMobileView) ? this.GetHtmlFragment("MiniCart.MiniCart.mobile.html") : this.GetHtmlFragment("MiniCart.MiniCart.html");
        }
    }
}