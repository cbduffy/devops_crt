﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../ExternalScripts//JQuery.d.ts" />
/// <reference path="../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../Common/Helpers/Core.ts" />
/// <reference path="../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../Resources/Resources.ts" />

module Microsoft.Dynamics.Retail.Ecommerce.Sdk.Controls {
    "use strict";

    export class Cart {
        private _cartView;
        private _editRewardCardDialog;
        private _loadingDialog;
        private _loadingText;
        private cart;
        private isShoppingCartLoaded;
        private isShoppingCartEnabled;
        private isPromotionCodesEnabled;
        private kitCartItemTypeValue;
        private errorMessage;
        private errorPanel;
        private dialogOverlay;
        private supportDiscountCodes;
        private supportLoyaltyReward;
        private displayPromotionBanner;

        constructor(element) {
            this._cartView = $(element);
            this.errorMessage = ko.observable<string>('');
            this.errorPanel = this._cartView.find(" > .msax-ErrorPanel");
            this.kitCartItemTypeValue = ko.observable<TransactionItemType>(TransactionItemType.Kit);
            this._editRewardCardDialog = this._cartView.find('.msax-EditRewardCard');
            this._loadingDialog = this._cartView.find('.msax-Loading');
            this._loadingText = this._loadingDialog.find('.msax-LoadingText');
            LoadingOverlay.CreateLoadingDialog(this._loadingDialog, 200, 200);

            this.isShoppingCartLoaded = ko.observable<boolean>(false);
            this.supportDiscountCodes = ko.observable<boolean>(Utils.isNullOrUndefined(msaxValues.msax_CartDiscountCodes) ? true : msaxValues.msax_CartDiscountCodes.toLowerCase() == "true");
            this.supportLoyaltyReward = ko.observable<boolean>(Utils.isNullOrUndefined(msaxValues.msax_CartLoyaltyReward) ? true : msaxValues.msax_CartLoyaltyReward.toLowerCase() == "true");
            this.displayPromotionBanner = ko.observable<boolean>(Utils.isNullOrUndefined(msaxValues.msax_CartDisplayPromotionBanner) ? true : msaxValues.msax_CartDisplayPromotionBanner.toLowerCase() == "true");

            // If displaying promotion banner call GetPromotionBanner service which gets the cart with promotion banners.
            // Otherwise call GetShoppingCart service which gets the cart without promotion banners.
            if (this.displayPromotionBanner()) {
                this.getPromotions();
            }
            else {
                this.getShoppingCart();
            }

            var cart = new ShoppingCartClass(null);
            cart.Items = [];
            cart.DiscountCodes = [];
            this.cart = ko.observable<ShoppingCart>(cart);

            // Subscribing to the UpdateShoppingCart event.
            ShoppingCartService.OnUpdateShoppingCart(this, this.updateShoppingCart);

            // Handles the keypress event on the control.
            this._cartView.keypress(function (event) {
                if (event.keyCode == 13 /* enter */ || event.keyCode == 8 /* backspace */ || event.keyCode == 27 /* esc */) {
                    event.preventDefault();
                    return false;
                }

                return true;
            });

            // Computed observables.
            this.isShoppingCartEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().Items);
            });

            this.isPromotionCodesEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().DiscountCodes);
            });
        }

        private getResx(key: string) {
            // Gets the resource value.
            return Resources[key];
        }

        private shoppingCartNextClick(viewModel: Checkout, event) {
            // If redirection url is specified redirect to the url on button click.
            if (!Utils.isNullOrWhiteSpace(msaxValues.msax_CheckoutUrl)) {
                window.location.href = msaxValues.msax_CheckoutUrl;
            }
        }

        private quantityMinusClick(item: TransactionItem) {
            // Handles quantity minus click.
            if (item.Quantity == 1) {
                this.removeFromCartClick(item);
            } else {
                item.Quantity = item.Quantity - 1;
                this.updateQuantity([item]);
            }
        }

        private quantityPlusClick(item: TransactionItem) {
            // Handles quantity plus click.
            item.Quantity = item.Quantity + 1;
            this.updateQuantity([item]);
        }

        private quantityTextBoxChanged(item: TransactionItem, valueAccesor) {
            // Handles quantity text box change event.
            var srcElement = valueAccesor.target;
            if (!Utils.isNullOrUndefined(srcElement) && srcElement.value != item.Quantity) {
                item.Quantity = srcElement.value;
                if (item.Quantity < 0) {
                    item.Quantity = 1;
                }

                if (item.Quantity == 0) {
                    this.removeFromCartClick(item);
                } else {
                    this.updateQuantity([item]);
                }
            }
        }

        private showError(isError: boolean) {
            // Shows the error message on the error panel.
            if (isError) {
                this.errorPanel.addClass("msax-Error");
            }
            else if (this.errorPanel.hasClass("msax-Error")) {
                this.errorPanel.removeClass("msax-Error");
            }

            this.errorPanel.show();
            $(window).scrollTop(0);
        }

        private editRewardCardOverlayClick() {
            this.dialogOverlay = $('.ui-widget-overlay');
            this.dialogOverlay.on('click', $.proxy(this.closeEditRewardCardDialog, this));
        }

        private createEditRewardCardDialog() {
            // Creates the edit reward card dialog box.
            this._editRewardCardDialog.dialog({
                modal: true,
                title: Resources.String_186, // Edit reward card
                autoOpen: false,
                draggable: true,
                resizable: false,
                position: ['top', 100],
                show: { effect: "fadeIn", duration: 500 },
                hide: { effect: "fadeOut", duration: 500 },
                width: 500,
                height: 300,
                dialogClass: 'msax-Control'
            });
        }

        private showEditRewardCardDialog() {
            // Specify the close event handler for the dialog.
            $('.ui-dialog-titlebar-close').on('click', $.proxy(this.closeEditRewardCardDialog, this));

            // Displays the edit reward card dialog.
            this._editRewardCardDialog.dialog('open');
            this.editRewardCardOverlayClick();
        }

        private closeEditRewardCardDialog() {
            // Close the dialog.
            this._editRewardCardDialog.dialog('close');
        }

        private continueShoppingClick() {
            // Handles continue shopping click
            if (!Utils.isNullOrWhiteSpace(msaxValues.msax_ContinueShoppingUrl)) {
                window.location.href = msaxValues.msax_ContinueShoppingUrl;
            }
        }

        private updateShoppingCart(event, data) {
            // Hanldes the UpdateShoppingCart event.
            if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                this.errorMessage(data.Errors[0].ErrorMessage);
                this.showError(true);
            }
            else {
                this.cart(data.ShoppingCart);
                this.errorPanel.hide();
            }

            this.isShoppingCartLoaded(true);
        }

        // Service calls

        private getShoppingCart() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);

            ShoppingCartService.GetShoppingCart(false, ShoppingCartDataLevel.All)
                .done((data) => {
                    this.createEditRewardCardDialog();
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_63); // Sorry, something went wrong. The shopping cart information couldn't be retrieved. Please refresh the page and try again.
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private getPromotions() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText);

            ShoppingCartService.GetPromotions(false, ShoppingCartDataLevel.All)
                .done((data) => {
                    this.createEditRewardCardDialog();
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_177); // Sorry, something went wrong. The cart promotions information couldn't be retrieved. Please refresh the page and try again.
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private removeFromCartClick(item) {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_179); // Updating shopping cart ...

            ShoppingCartService.RemoveFromCart(false, item.LineId, ShoppingCartDataLevel.All)
                .done((data) => {
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);

                    if (this.displayPromotionBanner()) {
                        this.getPromotions();
                    }
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_64);
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private updateQuantity(items: TransactionItem[]) {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_179); // Updating shopping cart ...

            ShoppingCartService.UpdateQuantity(false, items, ShoppingCartDataLevel.All)
                .done((data) => {
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);

                    if (this.displayPromotionBanner()) {
                        this.getPromotions();
                    }
                })
                .fail((errors) => {
                    this.errorMessage(Resources.String_65);
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                });
        }

        private applyPromotionCode(cart: ShoppingCart, valueAccesor) {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_179); // Updating shopping cart ...
            var srcElement = valueAccesor.target;

            if (!Utils.isNullOrUndefined(srcElement)
                && !Utils.isNullOrUndefined(srcElement.parentElement)
                && !Utils.isNullOrUndefined(srcElement.parentElement.firstElementChild)) {

                if (!Utils.isNullOrWhiteSpace(srcElement.parentElement.firstElementChild.value)) {
                    var promoCode = srcElement.parentElement.firstElementChild.value;

                    ShoppingCartService.AddOrRemovePromotion(false, promoCode, true, ShoppingCartDataLevel.All)
                        .done((data) => {
                            LoadingOverlay.CloseLoadingDialog(this._loadingDialog);

                            if (this.displayPromotionBanner()) {
                                this.getPromotions();
                            }
                        })
                        .fail((errors) => {
                            this.errorMessage(Resources.String_93); /* Sorry, something went wrong. The promotion code could not be added successfully. Please refresh the page and try again. */
                            this.showError(true);
                            LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                        });
                }
                else {
                    this.errorMessage(Resources.String_97); /* Please enter a promotion code */
                    this.showError(true);
                    LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                }
            }
            else {
                LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
            }
        }

        private removePromotionCode(cart: ShoppingCart, valueAccesor) {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_179); // Updating shopping cart ...
            var srcElement = valueAccesor.target;

            if (!Utils.isNullOrUndefined(srcElement)
                && !Utils.isNullOrUndefined(srcElement.parentElement)
                && !Utils.isNullOrUndefined(srcElement.parentElement.lastElementChild)
                && !Utils.isNullOrWhiteSpace(srcElement.parentElement.lastElementChild.textContent)) {
                var promoCode = srcElement.parentElement.lastElementChild.textContent;

                ShoppingCartService.AddOrRemovePromotion(false, promoCode, false, ShoppingCartDataLevel.All)
                    .done((data) => {
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);

                        if (this.displayPromotionBanner()) {
                            this.getPromotions();
                        }
                    })
                    .fail((errors) => {
                        this.errorMessage(Resources.String_94); /* Sorry, something went wrong. The promotion code could not be removed successfully. Please refresh the page and try again. */
                        this.showError(true);
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    });
            }
            else {
                LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
            }
        }

        private updateLoyaltyCardId() {
            LoadingOverlay.ShowLoadingDialog(this._loadingDialog, this._loadingText, Resources.String_179); // Updating shopping cart ...
            var loyaltyCardId = this._editRewardCardDialog.find('#RewardCardTextBox').val();

            if (!Utils.isNullOrWhiteSpace(loyaltyCardId)) {
                LoyaltyService.UpdateLoyaltyCardId(false, loyaltyCardId, ShoppingCartDataLevel.All)
                    .done((data) => {
                        this.closeEditRewardCardDialog();
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);

                        if (this.displayPromotionBanner()) {
                            this.getPromotions();
                        }
                    })
                    .fail((errors) => {
                        this.errorMessage(Resources.String_164); // Sorry, something went wrong. An error occurred while trying to update reward card id in cart. Please refresh the page and try again. 
                        this.showError(true);
                        this.closeEditRewardCardDialog();
                        LoadingOverlay.CloseLoadingDialog(this._loadingDialog);
                    });
            }
        }
    }
}