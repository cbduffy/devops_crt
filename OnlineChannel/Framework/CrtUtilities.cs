﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Microsoft.Dynamics.Commerce.Runtime.Configuration;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Crt", Justification = "Crt is well known term.")]
    public static class CrtUtilities
    {
        /// <summary>
        /// Represents an invalid value for the default channel identifier which is used as a seed value here.
        /// It is being assumed that a valid channel identifier would never be this value.
        /// </summary>
        private const long InvalidDefaultChannelId = 0;

        /// <summary>
        /// Identifies a configuration key to access CRT DB Connection string.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Crt", Justification = "Crt is well known CommerceRunTime")]
        private const string KeyCrtConnectionString = "CommerceRuntimeConnectionString";

        /// <summary>
        /// Caches the default channel identifier between web service calls.
        /// </summary>
        /// <remarks>This variable is not thread safe.</remarks>
        private static long DefaultChannelIdentifer = InvalidDefaultChannelId;

        /// <summary>
        /// Caches the commerce runtime configuration between web service calls.
        /// </summary>
        /// <remarks>This variable is not thread safe.</remarks>
        private static CommerceRuntimeConfiguration CrtConfiguration = null;

        /// <summary>
        /// Caches the three letter currency code (eg. USD, EUR, GBP, etc.) for the specific channel.
        /// </summary>
        private static string ChannelCurrencyCode = string.Empty;

        /// <summary>
        /// Caches the currency symbol (eg. $, £, €, etc.) for the specific channel.
        /// </summary>
        private static string ChannelCurrencySymbol = string.Empty;

        /// <summary>
        /// Caches the CommerceRuntime 
        /// /// </summary>
        private static CommerceRuntime runtime = null;

        /// <summary>
        /// Caches the CommerceRuntime 
        /// /// </summary>
        private static List<Product> deliveryServiceProductList = new List<Product>();

        /// <summary>
        /// Get the languages for the current channel.
        /// </summary>
        /// <returns>A read-only collection of languages</returns>
        public static ReadOnlyCollection<ChannelLanguage> GetChannelLanguages()
        {
            ChannelDataManager channelDataManager = new ChannelDataManager(new RequestContext(CrtUtilities.GetCommerceRuntime()));
            return channelDataManager.GetCurrentChannelLanguages();
        }

        /// <summary>
        /// Gets the currency code for the channel.
        /// </summary>
        /// <returns>The currency code as string.</returns>
        public static string GetChannelCurrencyCode()
        {
            if (string.IsNullOrWhiteSpace(ChannelCurrencyCode))
            {
                ChannelConfiguration channelConfiguration = null;

                ChannelManager channelManager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());
                channelConfiguration = channelManager.GetChannelConfiguration();

                ChannelCurrencyCode = channelConfiguration.Currency;
            }

            return ChannelCurrencyCode;
        }

        /// <summary>
        /// Gets the currency symbol for the channel.
        /// </summary>
        /// <returns>The currency symbol as string.</returns>
        public static string GetChannelCurrencySymbol()
        {
            if (string.IsNullOrWhiteSpace(ChannelCurrencySymbol))
            {
                string channelCurrencyCode = GetChannelCurrencyCode();

                ChannelCurrencySymbol = GetChannelCurrencySymbol(ChannelCurrencyCode);
            }

            return ChannelCurrencySymbol;
        }

        /// <summary>
        /// Gets the currency configuration.
        /// </summary>
        /// <param name="currencyCode">The currency code.</param>
        /// <returns>A Tuple that consists of the currency symbol as string and a boolean 
        /// that defines whether the symbol should be displayed as prefix or suffix</returns>
        private static string GetChannelCurrencySymbol(string currencyCode)
        {
            Dictionary<string, string> currencySymbolsByCode;

            currencySymbolsByCode = new Dictionary<string, string>();

            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            List<CultureInfo> culturesList = new List<CultureInfo>();
            culturesList.AddRange(cultures);

            var regions = culturesList.Select(x => new RegionInfo(x.LCID));

            foreach (var region in regions)
            {
                if (!currencySymbolsByCode.ContainsKey(region.ISOCurrencySymbol))
                {
                    currencySymbolsByCode.Add(region.ISOCurrencySymbol, region.CurrencySymbol);
                }
            }

            string currencySymbol = string.Empty;

            if (!currencySymbolsByCode.TryGetValue(currencyCode, out currencySymbol))
            {
                var message = string.Format("No currency symbol could be found for currency code {0}.", currencyCode);
                throw new InvalidOperationException(message);
            }

            return currencySymbol;
        }

        /// <summary>
        /// Gets the CommerceRuntime instance initialized by using the currently executing application's config.
        /// </summary>
        /// <returns>Commerce runtime instance.</returns>
        /// <remarks>
        /// Caches the commerce runtime configuration and default channel identifier.
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "The method might be called by other implementations.")]
        public static CommerceRuntime GetCommerceRuntime()
        {
            if (runtime == null)
            {
                if (CrtConfiguration == null)
                {
                    CrtConfiguration = GetCrtConfiguration();
                }

                runtime = GetCommerceRuntime(CrtConfiguration);
            }

            return runtime;
        }

        /// <summary>
        /// Gets the CommerceRuntime instance initialized by using the provided application configuration.
        /// </summary>
        /// <param name="appConfiguration">The application configuration.</param>
        /// <returns>Commerce runtime instance.</returns>
        /// <remarks>Caches the default channel identifier.</remarks>
        public static CommerceRuntime GetCommerceRuntime(Configuration appConfiguration)
        {
            string crtConnectionString = GetCrtConnectionString();
            CommerceRuntimeSection section = CommerceRuntimeConfigurationManager.GetConfigurationSection(appConfiguration, CommerceRuntimeConfigurationManager.SectionName);
            CommerceRuntimeConfiguration commerceRuntimeConfiguration = new CommerceRuntimeConfiguration(section, crtConnectionString);
            CommerceRuntime runtime = GetCommerceRuntime(commerceRuntimeConfiguration);
            return runtime;
        }

        /// <summary>
        /// Gets instance of CultureInfo associated with the current channel.
        /// </summary>
        /// <returns>The channel's default culture.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "The method is used by another client.")]
        public static CultureInfo GetChannelDefaultCulture()
        {
            ChannelManager channelManager = ChannelManager.Create(GetCommerceRuntime());
            OnlineChannel channel = channelManager.GetOnlineChannel(channelManager.GetCurrentChannelId());
            ChannelLanguage language = channel.ChannelLanguages.Single(l => l.IsDefault);
            CultureInfo culture = new CultureInfo(language.LanguageId);
            return culture;
        }

        /// <summary>
        /// Handles non-serializable exception.
        /// </summary>
        /// <remarks>
        /// CRT SDK is a PCL-based library. It's a known limitation of PCL that PCL doesn't support .Net serialization.
        /// As a workaround we wrap every CRT exception in System.Exception.
        /// Please use object.ToString method to obtain CRT Exception details, such as Type, Message, Details and Call stack.
        /// </remarks>
        /// <param name="exception">The original exception.</param>
        /// <returns>The standart exception.</returns>
        public static void HandleNonSerializableException(Exception exception)
        {
            if (exception == null)
            {
                return;
            }

            if (Attribute.GetCustomAttributes(exception.GetType()).OfType<SerializableAttribute>().Any())
            {
                throw exception;
            }

            // The rest of exceptions are not decorated with [Serializable] attribute 
            // - cannot pass this through AppDomain boundaries - receiving side still insists on this attribute, 
            // ignoring other serializators such as [DataContract].
            // Workaround is to wrap into new Exception.

            string message = exception.Message;

            // TimerJob's proc_AddTimerJobHistory stored procedure takes the message parameter that's 1000 characters or less.
            const int MaxMessageSize = 1000;

            var crtException = exception as CommerceRuntimeException;
            if (crtException != null)
            {
                message = string.Format("{0}, ErrorResourceId: {1}, Type: {2}", message, crtException.ErrorResourceId, crtException.GetType().FullName);
            }

            throw new Exception(message.Length <= MaxMessageSize ? message : message.Substring(0, MaxMessageSize));
        }

        /// <summary>
        /// Gets the commerce runtime based on the passed in commerce runtime configuration.
        /// </summary>
        /// <param name="commerceRuntimeConfiguration">The commerce runtime configuration.</param>
        /// <returns>An instance of commerce runtime.</returns>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.ConfigurationException">The default channel identifier cannot be zero</exception>
        private static CommerceRuntime GetCommerceRuntime(CommerceRuntimeConfiguration commerceRuntimeConfiguration)
        {
            if (DefaultChannelIdentifer == InvalidDefaultChannelId)
            {
                using (CommerceRuntime commerceRuntime = CommerceRuntime.Create(commerceRuntimeConfiguration, CommercePrincipal.AnonymousPrincipal))
                {
                    ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
                    DefaultChannelIdentifer = channelManager.GetDefaultChannelId();
                }

                if (DefaultChannelIdentifer == InvalidDefaultChannelId)
                {
                    string message = string.Format(CultureInfo.InvariantCulture, "The default channel identifier {0} was returned from CRT. Please ensure that a default operating unit number has been specified as part of the <commerceRuntime> configuration section.", DefaultChannelIdentifer);
                    throw new Microsoft.Dynamics.Commerce.Runtime.ConfigurationException(ConfigurationErrors.InvalidChannelConfiguration, message);
                }
            }

            CommercePrincipal principal = new CommercePrincipal(new CommerceIdentity(DefaultChannelIdentifer, new string[] { CommerceRoles.Storefront }));
            CommerceRuntime runtime = CommerceRuntime.Create(commerceRuntimeConfiguration, principal);

            return runtime;
        }

        /// <summary>
        /// Gets the commerce runtime configuration by using the currently executing application's config..
        /// </summary>
        /// <returns>The commerce runtime configuration.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "The method might be called by other implementations.")]
        private static CommerceRuntimeConfiguration GetCrtConfiguration()
        {
            string crtConnectionString = GetCrtConnectionString();
            CommerceRuntimeSection section = CommerceRuntimeConfigurationManager.GetConfigurationSection(CommerceRuntimeConfigurationManager.SectionName);
            CommerceRuntimeConfiguration commerceRuntimeConfiguration = new CommerceRuntimeConfiguration(section, crtConnectionString);

            return commerceRuntimeConfiguration;
        }

        /// <summary>
        /// Get the commerce runtime connection string from the application config file.
        /// </summary>
        /// <returns>The commerce runtime connection string.</returns>
        private static string GetCrtConnectionString()
        {
            string crtConnectionString = ConfigurationManager.ConnectionStrings[KeyCrtConnectionString].ConnectionString;
            if (string.IsNullOrEmpty(crtConnectionString))
            {
                NetTracer.Error("The commerce runtime connection string '{0}' was not found in the application config.", KeyCrtConnectionString);
            }

            return crtConnectionString;
        }

        /// <summary>
        /// Get the and cash Delivery Service Product Detail
        /// </summary>
        /// <returns>The commerce runtime connection string.</returns>
        public static List<Product> GetDevliveryServiceProduct(List<long> serviceItems)
        {
            var resultList = new List<Product>();
            var newItemList = new List<long>();
            foreach (var item in serviceItems)
            {
                var tempITem = deliveryServiceProductList.FirstOrDefault(temp => temp.RecordId == item);
                if (tempITem == null)
                {
                    if (item > 0)
                        newItemList.Add(item);
                }
                else
                {
                    resultList.Add(tempITem);
                }
            }
            if (!newItemList.IsNullOrEmpty())
            {
                ProductManager pm = ProductManager.Create(runtime);
                ProductSearchCriteria criteria = new ProductSearchCriteria(DefaultChannelIdentifer);
                criteria.Ids = newItemList;
                criteria.DataLevel = CommerceEntityDataLevel.Minimal;
                List<Product> serviceItemDetails = pm.SearchProducts(criteria, new QueryResultSettings()).Results.ToList();
                if (!serviceItemDetails.IsNullOrEmpty())
                {
                    resultList.AddRange(serviceItemDetails.AsEnumerable());
                    deliveryServiceProductList.AddRange(serviceItemDetails.AsEnumerable());
                }
            }
            return resultList;
        }
    }
}
