﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.MSInternal", "CA908:AvoidTypesThatRequireJitCompilationInPrecompiledAssemblies", Scope = "member", Target = "Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.CatalogController.#GetActiveProductCatalogs(System.Int64)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.MSInternal", "CA908:AvoidTypesThatRequireJitCompilationInPrecompiledAssemblies", Scope = "member", Target = "Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.WishListController.#GetWishListsForCustomer(System.String,Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.ISearchEngine)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.MSInternal", "CA908:AvoidTypesThatRequireJitCompilationInPrecompiledAssemblies", Scope = "member", Target = "Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.ShoppingCartController.#PopulateKitItemDetails(Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.ShoppingCart,System.Collections.ObjectModel.ReadOnlyCollection`1<Microsoft.Dynamics.Commerce.Runtime.DataModel.Product>,Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.ISearchEngine)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.MSInternal", "CA908:AvoidTypesThatRequireJitCompilationInPrecompiledAssemblies", Scope = "member", Target = "Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.ShoppingCartController.#GetKitConfigurationInformation(System.Int64,System.Int64,System.Collections.Generic.IEnumerable`1<Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.KitLine>,System.String,Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.ISearchEngine)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.MSInternal", "CA908:AvoidTypesThatRequireJitCompilationInPrecompiledAssemblies", Scope = "member", Target = "Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.ISearchEngine.#GetKitVariantProductInfo(System.Int64,System.Int64,System.Collections.Generic.IEnumerable`1<Microsoft.Dynamics.Commerce.Runtime.DataModel.KitLineProductProperty>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.MSInternal", "CA908:AvoidTypesThatRequireJitCompilationInPrecompiledAssemblies", Scope = "member", Target = "Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.OrderMapper.#ConvertToViewModel(System.Collections.Generic.IEnumerable`1<Microsoft.Dynamics.Commerce.Runtime.DataModel.SalesLine>,Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.ISearchEngine)")]