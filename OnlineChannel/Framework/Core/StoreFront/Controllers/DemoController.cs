﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    public static class DemoController
    {
        public static ReadOnlyCollection<Category> GetChannelNavigationalHierarchy()
        {
            CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
            ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
            long channelId = channelManager.GetCurrentChannelId();
            ReadOnlyCollection<Category> categories = channelManager.GetChannelCategoryHierarchy(new QueryResultSettings()).Results;
            return categories;
        }

        public static ReadOnlyCollection<Product> GetProducts(string language)
        {
            CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
            ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
            long channelId = channelManager.GetCurrentChannelId();
            var criteria = new ProductSearchCriteria(channelId);
            criteria.DataLevel = CommerceEntityDataLevel.Complete;
            ReadOnlyCollection<Category> categories = GetChannelNavigationalHierarchy();
            criteria.CategoryIds = categories.Select(i => i.RecordId).ToList();
            ProductManager productManager = ProductManager.Create(commerceRuntime);
            ReadOnlyCollection<Product> products = productManager.SearchProducts(criteria, new QueryResultSettings()).Results;
            return products;
        }

        public static ReadOnlyCollection<Product> GetProductsByIds(IList<long> productIds)
        {
            CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
            ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
            long channelId = channelManager.GetCurrentChannelId();
            ProductManager manager = ProductManager.Create(commerceRuntime);
            ProductSearchCriteria criteria = new ProductSearchCriteria(channelId);
            criteria.Ids = productIds;
            criteria.DataLevel = CommerceEntityDataLevel.Complete;

            return manager.SearchProducts(criteria, new QueryResultSettings()).Results;
        }
    }
}
