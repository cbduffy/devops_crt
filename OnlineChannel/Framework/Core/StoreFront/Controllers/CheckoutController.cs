/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Controller for Checkout operations.
    /// </summary>
    public class CheckoutController : ControllerBase
    {
        private const string DefaultOrderNumberMask = "#####@@@@@";
        private const string OrderNumberMaskPropertyName = "OrderNumberMask";
        private ShoppingCartController shoppingCartControllerValue;
        private CustomerController customerControllerValue;
        private static object syncRoot = new object();

        /// <summary>
        /// Represents the error message that the gift card has a different currency than the store currency.
        /// </summary>
        private const string GiftCardCurrencyDifferentFromChannelCurrencyError = "GiftCardCurrencyDifferentFromChannelCurrency";

        /// <summary>
        /// Represents the error message that the gift does not have the required balance since the checkout page was last acceessed.
        /// </summary>
        private const string GiftCardBalanceChangedSincePostedError = "GiftCardBalanceChangedSincePosted";

        /// <summary>
        /// Gets the applicable delivery options when the user wants to 'ship' the entire order as a single entity.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="shipToaddress">The ship toaddress.</param>
        /// <returns>
        /// The available delivery options.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null or empty.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "This specific type is required here.")]
        public virtual Collection<DeliveryOption> GetOrderDeliveryOptionsForShipping(string shoppingCartId, string customerId, Address shipToaddress)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            Collection<DeliveryOption> deliveryMethods = new Collection<DeliveryOption>();

            CrtDataModel.Address address = AddressMapper.ConvertToDataModel(shipToaddress);

            manager.UpdateCartShippingAddress(shoppingCartId, customerId, address, deliveryMode: string.Empty, modes: CrtDataModel.CalculationModes.None);

            IEnumerable<CrtDataModel.DeliveryOption> deliveryOptions = manager.GetOrderDeliveryOptions(shoppingCartId, customerId);

            return new ShoppingCartMapper(this.Configuration).ConvertToViewModel(deliveryOptions);
        }

        /// <summary>
        /// Gets information for all the delivery modes that are supported for the legal entity.
        /// </summary>
        /// <returns>The information for all the available delivery options.</returns>        
        public virtual Collection<DeliveryOption> GetDeliveryOptionsInfo()
        {
            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            IEnumerable<CrtDataModel.DeliveryOption> deliveryOptions = manager.GetAllDeliveryOptions();
            return new ShoppingCartMapper(this.Configuration).ConvertToViewModel(deliveryOptions);
        }

        /// <summary>
        /// Gets the delivery options applicable per line when the user wants the items in the cart 'shipped' to them individually.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="selectedLineShippingInfo">The shipping information for the selected lines.</param>
        /// <returns>
        /// The delivery options available per item.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null or empty.</exception>
        public virtual Collection<LineDeliveryOption> GetLineDeliveryOptionsForShipping(
            string shoppingCartId,
            string customerId,
            IEnumerable<SelectedLineShippingInfo> selectedLineShippingInfo)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (selectedLineShippingInfo == null)
            {
                throw new ArgumentNullException("selectedLineShippingInfo");
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

            Collection<CrtDataModel.SalesLineDeliveryOption> salesLineDeliveryOptions = new Collection<CrtDataModel.SalesLineDeliveryOption>();

            // This loop is necessary since the different shipping lines might have a different shipping address associated with them. However, the OrderManager API
            // allows entry of just one address for all cart lines being queried.
            foreach (var selectedShippingLine in selectedLineShippingInfo)
            {
                ICollection<string> cartLineIds = new Collection<string> { selectedShippingLine.LineId };
                CrtDataModel.Address address = AddressMapper.ConvertToDataModel(selectedShippingLine.ShipToAddress);

                var currentSalesLineDeliveryOptions = manager.GetLineDeliveryOptions(shoppingCartId, customerId, cartLineIds, address);
                salesLineDeliveryOptions.AddRange(currentSalesLineDeliveryOptions);
            }

            Collection<LineDeliveryOption> lineDeliveryOptions = new ShoppingCartMapper(this.Configuration).ConvertToViewModel(salesLineDeliveryOptions);

            return lineDeliveryOptions;
        }

        /// <summary>
        /// Gets delivery preferences applicable to the current checkout cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <returns>The applicable delivery preferences.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when shoppingCartId is null.</exception>
        public virtual CartDeliveryPreferences GetDeliveryPreferences(string shoppingCartId)
        {

            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            CrtDataModel.CartDeliveryPreferences cartDeliveryPreferences = manager.GetDeliveryPreferences(shoppingCartId);

            return CheckoutMapper.ConvertToViewModel(cartDeliveryPreferences);
        }


        /// <summary>
        /// Commits the selected delivery option to the cart when entire order is being 'delivered' as a single entity.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="selectedDeliveryOption">The selected delivery option.</param>
        /// <param name="productValidator">The product validator.</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// The updated shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId or shippingOptions is null or empty.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "This specific type is required here.")]
        public virtual ShoppingCart SetOrderDeliveryOption(string shoppingCartId, string customerId, SelectedDeliveryOption selectedDeliveryOption, IProductValidator productValidator, ISearchEngine searchEngine, ShoppingCartDataLevel dataLevel)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (selectedDeliveryOption == null)
            {
                throw new ArgumentNullException("selectedDeliveryOption");
            }

            this.ValidateSelectedDeliveryOption(selectedDeliveryOption);

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.Address address = GetDeliveryAddress(selectedDeliveryOption);
            CrtDataModel.Cart cart;
            string warehouseId = string.Empty;

            CrtDataModel.DeliveryPreferenceType selectedDeliveryPreferenceType = GetDeliveryPreferenceType(selectedDeliveryOption);

            switch (selectedDeliveryPreferenceType)
            {
                case CrtDataModel.DeliveryPreferenceType.PickupFromStore:
                    cart = manager.GetCart(shoppingCartId, customerId, CrtDataModel.CalculationModes.All);
                    cart.ShippingAddress = address;
                    cart.DeliveryMode = selectedDeliveryOption.DeliveryModeId;                    
                    cart = manager.CreateOrUpdateCart(cart);

                    // when picking up from store, cart line store number must be provided
                    foreach (CrtDataModel.CartLine cartLine in cart.CartLines)
                    {
                        cartLine.StoreNumber = selectedDeliveryOption.StoreAddress.StoreId;
                    }
                    cart = manager.UpdateCartLines(cart.Id, cart.CartLines);
                    break;

                case CrtDataModel.DeliveryPreferenceType.ElectronicDelivery:
                    cart = manager.UpdateCartShippingAddress(
                        shoppingCartId,
                        customerId,
                        address,
                        selectedDeliveryOption.DeliveryModeId,
                        CrtDataModel.CalculationModes.All,
                        warehouseId,
                        selectedDeliveryOption.ElectronicDeliveryEmail,
                        selectedDeliveryOption.ElectronicDeliveryEmailContent);
                    break;

                default:
                    cart = manager.UpdateCartShippingAddress(
                        shoppingCartId, 
                        customerId, 
                        address,
                        selectedDeliveryOption.DeliveryModeId, 
                        CrtDataModel.CalculationModes.All, 
                        warehouseId);
                    break;
            }

            ShoppingCart shoppingCart;
            if (dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = this.ShoppingCartController.AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            return shoppingCart;
        }

        /// <summary>
        /// Validates the selected delivery option.
        /// </summary>
        /// <param name="selectedDeliveryOption">The selected delivery option.</param>
        protected void ValidateSelectedDeliveryOption(SelectedDeliveryOption selectedDeliveryOption)
        {
            if (selectedDeliveryOption==null)
            {
                throw new ArgumentNullException("selectedDeliveryOption");
            }

            var errorMessage = string.Empty;

            if (string.IsNullOrWhiteSpace(selectedDeliveryOption.DeliveryModeId))
            {
                errorMessage += "The delivery mode cannot be empty for a valid delivery option.";
            }

            CrtDataModel.DeliveryPreferenceType selectedDeliveryPreferenceType = GetDeliveryPreferenceType(selectedDeliveryOption);

            if (selectedDeliveryPreferenceType == CrtDataModel.DeliveryPreferenceType.PickupFromStore)
            {
                if (selectedDeliveryOption.StoreAddress == null)
                {
                    errorMessage += "The store address field cannot be null for a PickUpFromStore delivery option.";
                }
                else if (string.IsNullOrWhiteSpace(selectedDeliveryOption.StoreAddress.StoreId))
                {
                    errorMessage += "The store id field inside store address cannot be empty for a PickUpFromStore delivery option.";
                }
            }
            else if (selectedDeliveryPreferenceType == CrtDataModel.DeliveryPreferenceType.ElectronicDelivery)
            {
                if (string.IsNullOrWhiteSpace(selectedDeliveryOption.ElectronicDeliveryEmail))
                {
                    errorMessage += "The email address field cannot be empty for electronic delivery option.";
                }
            }
            else
            {
                if (selectedDeliveryOption.CustomAddress == null)
                {
                    errorMessage += "The custom address field cannot be null for a ShipToAddress delivery option.";
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                throw new DataValidationException(DataValidationErrors.AggregateValidationError, errorMessage);
            }
        }

        /// <summary>
        /// Commits the selected delivery options per line when the sales line in the order are being 'delivered' individually.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="lineDeliveryOptions">The line delivery options.</param>
        /// <param name="productValidator">The product validator.</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// The updated shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId or shippingOptions is null or empty.</exception>
        public virtual ShoppingCart SetLineDeliveryOptions(string shoppingCartId, string customerId, IEnumerable<SelectedLineDeliveryOption> lineDeliveryOptions, IProductValidator productValidator, ISearchEngine searchEngine, ShoppingCartDataLevel dataLevel)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (lineDeliveryOptions == null)
            {
                throw new ArgumentNullException("shippingOptions");
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            CrtDataModel.Cart currentCart = manager.GetCart(shoppingCartId, customerId, CrtDataModel.CalculationModes.None);
            UpdateCartLineShippingAddresses(lineDeliveryOptions, currentCart.CartLines);
            CrtDataModel.Cart cart = manager.UpdateCartLines(shoppingCartId, customerId, currentCart.CartLines, CrtDataModel.CalculationModes.All);

            ShoppingCart shoppingCart;
            if (dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = this.ShoppingCartController.AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            return shoppingCart;
        }

        /// <summary>
        /// Get the types of payment cards.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="tenderDataLines">The tenderline date for payment.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="userName">Name of the user.</param>
        /// <returns>The channel reference identifier for the order.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId or emailAddress is null or empty.  Thrown when payments is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} for customer {1} was not found.</exception>
        public virtual string CreateOrder(string shoppingCartId, string customerId, IEnumerable<TenderDataLine> tenderDataLines, string emailAddress, string userName)
        {
            return this.CreateOrder(shoppingCartId, customerId, tenderDataLines, emailAddress, userName, true);
        }

        /// <summary>
        /// Get the types of payment cards.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="tenderDataLines">The tenderline date for payment.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="userName">Name of the user.</param>
        /// <param name="addAddressesToCustomer">Boolean value which indicates whether the shipping addresses in the order will be added to the customer address collection.</param>
        /// <returns>The channel reference identifier for the order.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId or emailAddress is null or empty.  Thrown when payments is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} for customer {1} was not found.</exception>
        public virtual string CreateOrder(string shoppingCartId, string customerId, IEnumerable<TenderDataLine> tenderDataLines, string emailAddress, string userName, bool addAddressesToCustomer)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }

            if (tenderDataLines == null)
            {
                throw new ArgumentNullException("tenderDataLines");
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.Cart cart = manager.GetCart(shoppingCartId, customerId, CrtDataModel.CalculationModes.None);

            if (addAddressesToCustomer)
            {
                try
                {
                    // Trying to add the shipping addresses that were used during order creation to the customer address collection.
                    // Failure of this action should not affect order creation hence we swallow the exceptions.
                    this.CustomerController.UpdateCustomerAddresses(cart, customerId);
                }
                catch (Exception ex)
                {
                    NetTracer.Warning(ex, "Failed to add shipping addresses to customer address collection.");
                }
            }

            if (cart == null)
            {
                throw new DataValidationException(DataValidationErrors.CartNotFound, "Shopping cart {0} for customer {1} was not found.", shoppingCartId, customerId);
            }

            if (string.IsNullOrWhiteSpace(cart.OrderNumber))
            {
                cart.OrderNumber = TokenHelper.GenerateOrderNumber(GetOrderNumberMask(), userName);
                manager.CreateOrUpdateCart(cart, CrtDataModel.CalculationModes.None);
            }

            IEnumerable<CrtDataModel.CartTenderLine> cartTenderLines = GetCartTenderLines(tenderDataLines, cart.TotalAmount, cart.Id);
            CrtDataModel.SalesOrder salesOrder = manager.CreateOrderFromCart(shoppingCartId, customerId, cartTenderLines, emailAddress);

            return salesOrder.ChannelReferenceId;
        }

        /// <summary>
        /// Get the types of payment cards.
        /// </summary>
        /// <returns>A response containing payment card types.</returns>
        public virtual Collection<PaymentCardType> GetPaymentCardTypes()
        {
            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            return new ShoppingCartMapper(this.Configuration).ConvertToViewModel(manager.GetSupportedCardTypes());
        }

        /// <summary>
        /// Gets the shopping cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null or empty.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} was not found.</exception>
        public virtual ShoppingCart GetShoppingCart(string shoppingCartId, string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            return this.ShoppingCartController.GetShoppingCart(shoppingCartId, customerId, dataLevel, productValidator, searchEngine);
        }

        /// <summary>
        /// Removes the items from the shopping cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="lineIds">The line ids.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// A shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} was not found.</exception>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null.</exception>
        public virtual ShoppingCart RemoveItems(string shoppingCartId, string customerId, IEnumerable<string> lineIds, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            return this.ShoppingCartController.RemoveItems(shoppingCartId, customerId, lineIds, dataLevel, productValidator, searchEngine);
        }

        /// <summary>
        /// Updates cart lines shipping addresses.
        /// </summary>
        /// <param name="selectedLineDeliveryOptions">The selected shipping options.</param>   
        /// <param name="lines">Cartlines to be updated.</param>
        protected virtual void UpdateCartLineShippingAddresses(IEnumerable<SelectedLineDeliveryOption> selectedLineDeliveryOptions, IEnumerable<CrtDataModel.CartLine> lines)
        {
            if (selectedLineDeliveryOptions == null)
            {
                throw new ArgumentNullException("shippingOptions");
            }
            
            foreach (SelectedLineDeliveryOption lineDeliveryOption in selectedLineDeliveryOptions)
            {
                ValidateSelectedDeliveryOption(lineDeliveryOption);

                CrtDataModel.Address shippingAddress = GetDeliveryAddress(lineDeliveryOption);

                CrtDataModel.CartLine cartLine = lines.Where(l => l.LineId == lineDeliveryOption.LineId).Single();
                cartLine.LineData.ShippingAddress = shippingAddress;
                cartLine.LineData.DeliveryMode = lineDeliveryOption.DeliveryModeId;

                CrtDataModel.DeliveryPreferenceType selectedDeliveryPreferenceType = GetDeliveryPreferenceType(lineDeliveryOption);
                if (selectedDeliveryPreferenceType == CrtDataModel.DeliveryPreferenceType.PickupFromStore)
                {
                    cartLine.LineData.StoreNumber = lineDeliveryOption.StoreAddress.StoreId;
                }
                if (selectedDeliveryPreferenceType == CrtDataModel.DeliveryPreferenceType.ElectronicDelivery)
                {
                    cartLine.LineData.ElectronicDeliveryEmail = lineDeliveryOption.ElectronicDeliveryEmail;
                    cartLine.LineData.ElectronicDeliveryEmailContent = lineDeliveryOption.ElectronicDeliveryEmailContent;
                }
            }
        }

        // CS by muthait dated 26Nov2014 - RTM Upgrade
        /// <summary>
        /// Gets the delivery address.
        /// </summary>
        /// <param name="deliveryOptions">The selected shipping options.</param>
        /// <returns>The address based on the selected shipping option.</returns>
        public virtual CrtDataModel.Address GetDeliveryAddress(SelectedDeliveryOption selectedDeliveryOption)
        {
            if (selectedDeliveryOption == null)
            {
                throw new ArgumentNullException("deliveryOptions");
            }
            
            CrtDataModel.Address address = null;
            CrtDataModel.DeliveryPreferenceType selectedType = GetDeliveryPreferenceType(selectedDeliveryOption);

            switch (selectedType)
            {
                case CrtDataModel.DeliveryPreferenceType.PickupFromStore:
                    address = AddressMapper.ConvertToDataModel(selectedDeliveryOption.StoreAddress);
                    break;
                case CrtDataModel.DeliveryPreferenceType.ShipToAddress:
                case CrtDataModel.DeliveryPreferenceType.ElectronicDelivery:
                    address = AddressMapper.ConvertToDataModel(selectedDeliveryOption.CustomAddress);
                    break;
            }

            return address;
        }

        /// <summary>
        /// Gets the shipping option type based on the selected shipping option.
        /// </summary>
        /// <param name="selectedDeliveryOptions">The selected shipping options.</param>
        /// <returns>The shipping option type.s</returns>
        protected virtual CrtDataModel.DeliveryPreferenceType GetDeliveryPreferenceType(SelectedDeliveryOption selectedDeliveryOptions)
        {
            if (selectedDeliveryOptions == null)
            {
                throw new ArgumentNullException("selectedDeliveryOptions");
            }
            
            // First try to get a saved address
            CrtDataModel.DeliveryPreferenceType selectedType;
            if (!Enum.TryParse(selectedDeliveryOptions.DeliveryPreferenceId, out selectedType))
            {
                var message = string.Format("Selected delivery option has an invalid value for delivery preference id: {0}.", selectedDeliveryOptions.DeliveryPreferenceId);
                throw new InvalidCastException(message);
            }

            return selectedType;
        }

        /// <summary>
        /// Gets the order number mask.
        /// </summary>
        /// <returns>The order number mask.</returns>
        protected virtual string GetOrderNumberMask()
        {
            string orderNumberMask = ConfigurationManager.AppSettings[CheckoutController.OrderNumberMaskPropertyName];

            if (string.IsNullOrWhiteSpace(orderNumberMask))
            {
                orderNumberMask = CheckoutController.DefaultOrderNumberMask;
            }

            return orderNumberMask;
        }

        /// <summary>
        /// Get the gift card balance.
        /// </summary>
        /// <returns>A response containing gift card balance.</returns>
        public virtual GiftCardInformation GetGiftCardInformation(string giftCardNumber)
        {
            GiftCardInformation giftCardInformation = new GiftCardInformation();
            try
            {
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                CrtDataModel.GiftCard giftCard = manager.GetGiftCard(giftCardNumber);
                giftCardInformation = CheckoutMapper.ConvertToViewModel(giftCard);
                giftCardInformation.IsInfoAvailable = true;
            }
            catch (Microsoft.Dynamics.Commerce.Runtime.HeadquarterTransactionServiceException)
            {
                giftCardInformation.IsInfoAvailable = false;
            }

            return giftCardInformation;
        }

        /// <summary>
        /// Gets the data model cart tender lines after validating the passed in view model tender lines.
        /// </summary>
        /// <param name="tenderDataLines">The tender data lines.</param>
        /// <param name="cartTotalAmount">The cart total amount.</param>
        /// <param name="checkoutCartId">The checkout cart identifier (used only to provide debug context).</param>
        /// <returns></returns>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Tender lines have more than one credit card
        /// or
        /// Tender lines have amount 0 or negative
        /// or
        /// Tender lines totals do not match cart total</exception>
        protected virtual ICollection<CrtDataModel.CartTenderLine> GetCartTenderLines(IEnumerable<TenderDataLine> tenderDataLines, Decimal cartTotalAmount, string checkoutCartId)
        {
            ChannelManager channelManager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());
            string channelCurrencyCode = channelManager.GetChannelConfiguration().Currency;

            List<CrtDataModel.CartTenderLine> cartTenderLines = new List<CrtDataModel.CartTenderLine>();
            cartTenderLines = new ShoppingCartMapper(this.Configuration).ConvertToDataModel(tenderDataLines);

            UpdateValidateCreditCardTenderLines(cartTotalAmount, checkoutCartId, cartTenderLines);
            ValidateGiftCardTenderLines(checkoutCartId, channelCurrencyCode, cartTenderLines);


            // Validate cart tenderlines do not have any non negative amount
            var cartTenderLinesNegativeAmount = cartTenderLines.Where(tl => tl.Amount <= 0);
            if (cartTenderLinesNegativeAmount.Any())
            {
                throw new DataValidationException(DataValidationErrors.IncorrectPaymentAmountSign, "Tender lines have amount 0 or negative. CheckoutCartId:{0}", checkoutCartId);
            }

            // Validate tender line amounts add up to total amount before submitting order
            decimal totalAmountForTenderLines = cartTenderLines.Sum(a => a.Amount);
            if (totalAmountForTenderLines != cartTotalAmount)
            {
                throw new DataValidationException(DataValidationErrors.AmountDueMustBePaidBeforeCheckout, "Tender lines total do not match cart total. TenderLineTotal:{0}, CartTotal:{1}, CheckoutCartId:{0}", totalAmountForTenderLines, cartTotalAmount, checkoutCartId);
            }
            return cartTenderLines;
        }

        /// <summary>
        /// Updates and validates credit card tender lines.
        /// </summary>
        /// <param name="cartTotalAmount">The cart total amount.</param>
        /// <param name="checkoutCartId">The checkout cart identifier.</param>
        /// <param name="cartTenderLines">The cart tender lines.</param>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Tender lines have more than one credit card. CheckoutCartId:{0}</exception>
        protected virtual void UpdateValidateCreditCardTenderLines(Decimal cartTotalAmount, string checkoutCartId, IEnumerable<CrtDataModel.CartTenderLine> cartTenderLines)
        {
            var rawCreditCardLines = cartTenderLines.Where(tl => tl.PaymentCard != null);
            var tokenizedCreditCardLines = cartTenderLines.Where(tl => tl.TokenizedPaymentCard != null);

            //There can be only one credit card tenderline, raw or tokenized.
            int rawCreditCardLinesCount = rawCreditCardLines.Count();
            int tokenizedCreditCardLinesCount = tokenizedCreditCardLines.Count();

            // If more than one credit card is defined then block submit payment
            if (((rawCreditCardLinesCount + tokenizedCreditCardLinesCount) > 1)
                || (rawCreditCardLinesCount > 1)
                || (tokenizedCreditCardLinesCount > 1))
            {
                throw new DataValidationException(DataValidationErrors.MultipleCreditCardPaymentNotSupported, "Tender lines have more than one credit card. CheckoutCartId:{0}", checkoutCartId);
            }

            decimal nonCreditCardAmount = cartTenderLines.Where(tl => (tl.PaymentCard == null && tl.TokenizedPaymentCard == null)).Sum(a => a.Amount);
            decimal finalCreditCardAmount = cartTotalAmount - nonCreditCardAmount;

            if (rawCreditCardLinesCount == 1)
            {
                rawCreditCardLines.Single().Amount = finalCreditCardAmount;
            }
            else if (tokenizedCreditCardLinesCount == 1)
            {
                tokenizedCreditCardLines.Single().Amount = finalCreditCardAmount;
            }
            else
            {
                if (finalCreditCardAmount != 0)
                {
                    var message = string.Format("There is a non-zero amount [{0}] that must be paid by credit card. However, no credit card payment lines were found.", finalCreditCardAmount);
                    throw new InvalidOperationException(message);
                }
            }
        }

        /// <summary>
        /// Validates the gift card tender lines.
        /// </summary>
        /// <param name="checkoutCartId">The checkout cart identifier.</param>
        /// <param name="channelCurrencyCode">The channel currency code.</param>
        /// <param name="cartTenderLines">The cart tender lines.</param>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">
        /// </exception>
        protected virtual void ValidateGiftCardTenderLines(string checkoutCartId, string channelCurrencyCode, IEnumerable<CrtDataModel.CartTenderLine> cartTenderLines)
        {
            // Gift card validations.
            // We allow only one gift card payment per online order.
            CrtDataModel.CartTenderLine giftCardTenderLine = cartTenderLines.Where(tl => (!string.IsNullOrWhiteSpace(tl.GiftCardId))).SingleOrDefault();
            if (giftCardTenderLine != null)
            {
                GiftCardInformation currentGiftCard = GetGiftCardInformation(giftCardTenderLine.GiftCardId);

                // Validate that gift card current matches online storefront currency.
                if (currentGiftCard.CurrencyCode != channelCurrencyCode)
                {
                    string message = string.Format("The gift card currency '{0}' does not match the channel currency '{1}'. CheckoutCartId:{2}", currentGiftCard.CurrencyCode, channelCurrencyCode, checkoutCartId);
                    throw new DataValidationException(GiftCardCurrencyDifferentFromChannelCurrencyError, message);
                }

                // Validate that the latest balance on the Gift card can cover the requested order amount.
                if (currentGiftCard.Balance < giftCardTenderLine.Amount)
                {
                    string message = string.Format("The gift card balance available has changed since the user selected the payment option. UserSelected:'{0}' ActualAmount:{1}. CheckoutCartId:{2}", giftCardTenderLine.Amount, currentGiftCard.Balance, checkoutCartId);
                    throw new DataValidationException(GiftCardBalanceChangedSincePostedError, message);
                }
            }
        }

        /// <summary>
        /// Gets an instance of ShoppingCartController.
        /// </summary>
        protected virtual ShoppingCartController ShoppingCartController
        {
            get
            {
                if (this.shoppingCartControllerValue == null)
                {
                    lock (syncRoot)
                    {
                        if (this.shoppingCartControllerValue == null)
                        {
                            this.shoppingCartControllerValue = new ShoppingCartController();
                            this.shoppingCartControllerValue.SetConfiguration(this.Configuration);
                        }
                    }
                }

                return this.shoppingCartControllerValue;
            }
        }

        /// <summary>
        /// Gets an instance of CustomerController.
        /// </summary>
        protected virtual CustomerController CustomerController
        {
            get
            {
                if (this.customerControllerValue == null)
                {
                    lock (syncRoot)
                    {
                        if (this.customerControllerValue == null)
                        {
                            this.customerControllerValue = new CustomerController();
                            this.customerControllerValue.SetConfiguration(this.Configuration);
                        }
                    }
                }

                return this.customerControllerValue;
            }
        }
    }
}