﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// The controller for the Wish List.
    /// </summary>
    public class WishListController : ControllerBase
    {
        private const string PropertyKeyImage = "Image";

        /// <summary>
        /// Gets the wish list.
        /// </summary>
        /// <param name="wishListId">The wish list identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// The wish list.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// wishListId
        /// or
        /// customerId
        /// </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        public virtual WishList GetWishList(string wishListId, string customerId, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentNullException("wishListId");
            }

            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentNullException("customerId");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();

            CommerceListManager commerceListManager = CommerceListManager.Create(runtime);
            CrtDataModel.CommerceList wishListDM = commerceListManager.GetCommerceList(wishListId, customerId);

            WishList wishList = WishListMapper.ConvertToViewModel(wishListDM);
            IEnumerable<long> productIds = wishList.WishListLines.Select(wll => wll.ProductId);
            IEnumerable<CrtDataModel.Product> products = new ShoppingCartMapper(Configuration).GetProducts(productIds);

            PopulateWishListDetailsFromProducts(wishList, products, searchEngine);

            return wishList;
        }

        /// <summary>
        /// Gets the wish lists corresponding to customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// The wish lists.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        public virtual Collection<WishList> GetWishListsForCustomer(string customerId, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentNullException("customerId");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            CommerceListManager manager = CommerceListManager.Create(runtime);

            Collection<WishList> wishLists = new Collection<WishList>();
            ReadOnlyCollection<CrtDataModel.CommerceList> wishListsDM = manager.GetCommerceListsByCustomer(customerId, false, false);

            Collection<long> productIds = new Collection<long>();
            foreach (CrtDataModel.CommerceList wishListDM in wishListsDM)
            {
                WishList wishList = WishListMapper.ConvertToViewModel(wishListDM);
                wishLists.Add(wishList);

                var wishListLines = wishList.WishListLines;
                if (wishListLines != null)
                {
                    productIds.AddRange(wishListLines.Select(item => item.ProductId));
                }
            }

            IEnumerable<CrtDataModel.Product> products = new ShoppingCartMapper(this.Configuration).GetProducts(productIds);
            PopulateWishListDetailsFromProducts(wishLists, products, searchEngine);

            return wishLists;
        }

        protected virtual void PopulateWishListDetailsFromProducts(IEnumerable<WishList> wishLists, IEnumerable<CrtDataModel.Product> products, ISearchEngine searchEngine)
        {
            if (wishLists == null)
            {
                throw new ArgumentNullException("wishLists");
            }

            if (products == null)
            {
                throw new ArgumentNullException("products");
            }

            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }
            
            foreach (WishList wishList in wishLists)
            {
                PopulateWishListDetailsFromProducts(wishList, products, searchEngine);
            }
        }

        protected virtual void PopulateWishListDetailsFromProducts(WishList wishList, IEnumerable<CrtDataModel.Product> products, ISearchEngine searchEngine)
        {
            if (wishList == null)
            {
                throw new ArgumentNullException("wishList");
            }

            if (products == null)
            {
                throw new ArgumentNullException("products");
            }

            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }

            string currentCulture = this.Configuration.GetCurrentCulture();

            foreach (WishListLine wishListLine in wishList.WishListLines)
            {
                CrtDataModel.Product wishListLineProduct;

                // Check if the wish list line product is a standalone non variant product
                wishListLineProduct = products.Where(p => p.RecordId == wishListLine.ProductId).FirstOrDefault();

                if (wishListLineProduct == null)
                {
                    // This means that the wish list product could be a product variant.
                    CrtDataModel.ProductVariant wishListLineProductVariant = null;

                    foreach (Product product in products)
                    {
                        if (product.IsMasterProduct)
                        {
                            wishListLineProductVariant = product.CompositionInformation.VariantInformation.Variants.Where(v => v.DistinctProductVariantId == wishListLine.ProductId).SingleOrDefault();
                            if (wishListLineProductVariant != null)
                            {
                                wishListLineProduct = product;
                                break;
                            }
                        }
                    }

                    if (wishListLineProduct == null || wishListLineProductVariant == null)
                    {
                        // This is a valid scenario. This would happen if the wish list line product belongs to a different channel or is no longer availalbe on this channel.
                        // Assigning default values.
                        wishListLine.Name = string.Format(Resources.ProductNameUnavailableTemplate, wishListLine.ProductId);
                        wishListLine.Description = Resources.ProductUnavailableDescription;

                        NetTracer.Warning("Provided product {0} could not be resolved into product details.", wishListLine.ProductId);
                    }
                    else
                    {
                        wishListLine.Name = wishListLineProduct.ProductName;
                        wishListLine.Description = wishListLineProduct.Description;
                        wishListLine.ProductNumber = wishListLineProduct.ProductNumber;
                        wishListLine.PriceWithCurrency = wishListLineProductVariant.BasePrice.ToCurrencyString(currentCulture);
                        wishListLine.TotalWithCurrency = (wishListLineProductVariant.AdjustedPrice * wishListLine.Quantity).ToCurrencyString(currentCulture);
                        wishListLine.Color = wishListLineProductVariant.Color;
                        wishListLine.Style = wishListLineProductVariant.Style;
                        wishListLine.Size = wishListLineProductVariant.Size;
                    }
                }
                else
                {
                    wishListLine.Name = wishListLineProduct.ProductName;
                    wishListLine.PriceWithCurrency = wishListLineProduct.BasePrice.ToCurrencyString(currentCulture);
                    wishListLine.TotalWithCurrency = (wishListLineProduct.AdjustedPrice * wishListLine.Quantity).ToCurrencyString(currentCulture);
                }

                PopulateWishListLineDetailsFromSearchResults(wishListLine, searchEngine);
            }
        }

        /// <summary>
        /// Populates the wish list line details from search results.
        /// </summary>
        /// <param name="wishListLine">The wish list line.</param>
        protected virtual void PopulateWishListLineDetailsFromSearchResults(WishListLine wishListLine, ISearchEngine searchEngine)
        {
            if (wishListLine == null)
            {
                throw new ArgumentNullException("wishListLine");
            }

            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }
            
            // If the product details could not be found, then it is a valid scenario. 
            // Reason: The product could no longer be available on the current channel; or the product could have been added from a different channel by the same customer.
            bool isResultManadatory = false;
            IDictionary<string, object> properties = searchEngine.GetListingProperties(isResultManadatory, wishListLine.ProductId,
                searchEngine.PathSearchPropertyName,
                searchEngine.ImageSearchPropertyName,
                searchEngine.DescriptionSearchPropertyName,
                searchEngine.TitleSearchPropertyName,
                searchEngine.ColorSearchPropertyName,
                searchEngine.SizeSearchPropertyName,
                searchEngine.StyleSearchPropertyName,
                searchEngine.ConfigurationSearchPropertyName);

            if (properties != null)
            {
                // Update product url.
                object pathValue;
                if (properties.TryGetValue(searchEngine.PathSearchPropertyName, out pathValue))
                {
                    wishListLine.ProductUrl = (string)pathValue;
                }

                // Update image url.
                object imageSearchValue;
                if (properties.TryGetValue(searchEngine.ImageSearchPropertyName, out imageSearchValue))
                {
                    wishListLine.Image = (ImageInfo)imageSearchValue;
                }

                // Update product description.
                object descriptionValue;
                if (properties.TryGetValue(searchEngine.DescriptionSearchPropertyName, out descriptionValue))
                {
                    wishListLine.Description = (string)descriptionValue;
                }

                // Update product title.
                object titleSearchValue;
                if (properties.TryGetValue(searchEngine.TitleSearchPropertyName, out titleSearchValue))
                {
                    wishListLine.Name = (string)titleSearchValue;
                }

                // Update product color.
                object colorSearchValue;
                if (properties.TryGetValue(searchEngine.ColorSearchPropertyName, out colorSearchValue))
                {
                    wishListLine.Color = (string)colorSearchValue;
                }

                // Update product size.
                object sizeSearchValue;
                if (properties.TryGetValue(searchEngine.SizeSearchPropertyName, out sizeSearchValue))
                {
                    wishListLine.Size = (string)sizeSearchValue;
                }

                // Update product style.
                object styleSearchValue;
                if (properties.TryGetValue(searchEngine.StyleSearchPropertyName, out styleSearchValue))
                {
                    wishListLine.Style = (string)styleSearchValue;
                }

                // Update product configuration.
                object configurationSearchValue;
                if (properties.TryGetValue(searchEngine.ConfigurationSearchPropertyName, out configurationSearchValue))
                {
                    wishListLine.Configuration = (string)configurationSearchValue;
                }
            }
            else
            {
                // This is a valid scenario. This would happen if the wish list line product belongs to a different channel or is no longer available on this channel.
                // Assigning default values.
                wishListLine.ProductUrl = Utilities.InactiveLinkUrl;
                wishListLine.Image = new ImageInfo();

                NetTracer.Information("No search results were found for product {0} while getting wish list line details. Using fall back values.", wishListLine.ProductId);
            }
        }

        /// <summary>
        /// Deletes the wish list.
        /// </summary>
        /// <param name="wishListId">The wish list identifier.</param>
        public virtual void DeleteWishList(string wishListId, string customerId)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentNullException("wishListId");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            CommerceListManager manager = CommerceListManager.Create(runtime);

            ReadOnlyCollection<CommerceList> wishLists = manager.GetCommerceListsByCustomer(customerId, false, false);
            if (wishLists.Any(wishList => wishList.Id.Equals(wishListId)))
            {
                manager.DeleteCommerceList(wishListId);
            }
        }

        /// <summary>
        /// Creates the wish list.
        /// </summary>
        /// <param name="wishList">The wish list to create.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// The wish list.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// wishList
        /// or
        /// customerId
        /// </exception>
        public virtual WishList CreateWishList(WishList wishList, string customerId)
        {
            if (wishList == null)
            {
                throw new ArgumentNullException("wishList");
            }

            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentNullException("customerId");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            CommerceListManager manager = CommerceListManager.Create(runtime);
            wishList.CustomerId = customerId;

            if (Convert.ToBoolean(wishList.IsFavorite, CultureInfo.InvariantCulture))
            {
                CrtDataModel.CommerceList prevFavWishList = manager.GetCommerceListsByCustomer(wishList.CustomerId, true, false).SingleOrDefault();
                if (prevFavWishList != null)
                {
                    prevFavWishList.IsFavorite = false;
                    prevFavWishList = manager.UpdateCommerceListProperties(prevFavWishList);
                }
            }
            CrtDataModel.CommerceList wishListDM = manager.CreateCommerceList(WishListMapper.ConvertToDataModel(wishList));

            wishList = WishListMapper.ConvertToViewModel(wishListDM);
            return wishList;
        }

        /// <summary>
        /// Adds items to wish list.
        /// </summary>
        /// <param name="wishListId">The wish list identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="listings">The items to add to the wish list.</param>
        /// <returns>
        /// The wish list.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// wishListId
        /// or
        /// customerId
        /// or
        /// listings
        /// </exception>
        public virtual WishList AddItemsToWishList(string wishListId, string customerId, IEnumerable<Models.Listing> listings)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentNullException("wishListId");
            }

            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentNullException("customerId");
            }

            if (listings == null)
            {
                throw new ArgumentNullException("listings");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            CommerceListManager manager = CommerceListManager.Create(runtime);

            CrtDataModel.CommerceList wishListDM = manager.GetCommerceList(wishListId, customerId);

            Dictionary<long, CrtDataModel.CommerceListLine> productIdWishListLineMap = new Dictionary<long, CrtDataModel.CommerceListLine>();
            foreach (CrtDataModel.CommerceListLine wishListLine in wishListDM.CommerceListLines)
            {
                productIdWishListLineMap.Add(wishListLine.ProductId, wishListLine);
            }

            foreach (Models.Listing listing in listings)
            {
                long listingId = listing.ListingId;
                if (productIdWishListLineMap.ContainsKey(listingId))
                {
                    // Update wish list line quantity if item exists already.
                    CrtDataModel.CommerceListLine wishListLine = productIdWishListLineMap[listingId];
                    wishListLine.Quantity = listing.Quantity + wishListLine.Quantity;
                    wishListLine = manager.UpdateCommerceListLine(wishListLine);
                }
                else
                {
                    // Add wish list line
                    CrtDataModel.CommerceListLine wishListLine = new CrtDataModel.CommerceListLine { CommerceListId = wishListId, CustomerId = customerId, ProductId = listing.ListingId, Quantity = listing.Quantity };
                    wishListDM.CommerceListLines.Add(manager.AddToCommerceList(wishListId, wishListLine));
                }
            }

            WishList wishList = WishListMapper.ConvertToViewModel(wishListDM);
            return wishList;
        }

        /// <summary>
        /// Update items on wish list.
        /// </summary>
        /// <param name="wishListId">The wish list identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="listings">The item to update on the wish list.</param>
        /// <returns>
        /// The wish list.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// wishListId
        /// or
        /// customerId
        /// or
        /// listings
        /// </exception>
        /// <exception cref="System.InvalidOperationException"></exception>
        public virtual WishList UpdateItemsOnWishList(string wishListId, string customerId, IEnumerable<Models.Listing> listings)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentNullException("wishListId");
            }

            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentNullException("customerId");
            }

            if (listings == null)
            {
                throw new ArgumentNullException("listings");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            CommerceListManager manager = CommerceListManager.Create(runtime);
            CrtDataModel.CommerceList wishListDM = manager.GetCommerceList(wishListId, customerId);

            foreach (Models.Listing listing in listings)
            {
                try
                {
                    // Update wish list line.
                    CrtDataModel.CommerceListLine wishListLine = wishListDM.CommerceListLines.Where(wll => wll.ProductId == listing.ListingId).Single();
                    wishListLine.Quantity = listing.Quantity;
                    wishListLine = manager.UpdateCommerceListLine(wishListLine);
                }
                catch (InvalidOperationException ex)
                {
                    throw new InvalidOperationException(string.Format("The product {0} does not exist in the wish list {1}", listing.ListingId, wishListId), ex);
                }
            }

            WishList wishList = WishListMapper.ConvertToViewModel(wishListDM);
            return wishList;
        }

        /// <summary>
        /// Update wish list properties.
        /// </summary>
        /// <param name="wishList">The wish list to update.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// The updated wish list.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// wishList
        /// or
        /// customerId
        /// </exception>
        public virtual WishList UpdateWishListProperties(WishList wishList, string customerId)
        {
            if (wishList == null)
            {
                throw new ArgumentNullException("wishList");
            }

            if (string.IsNullOrWhiteSpace(wishList.CustomerId))
            {
                throw new ArgumentNullException("wishList.CustomerId");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            CommerceListManager manager = CommerceListManager.Create(runtime);
            CommerceList wishListDM = new CommerceList();
            CommerceList currentFavoriteWishList = new CommerceList();

            // If we are making the wish list as favorite then reset the favorite flag on the current favorite wish list.
            if (wishList.IsFavorite ?? false)
            {
                ReadOnlyCollection<CommerceList> wishListsDM = manager.GetCommerceListsByCustomer(customerId, false, false);
                foreach (CommerceList currentWishListDM in wishListsDM)
                {
                    if (currentWishListDM.Id.Equals(wishList.Id, StringComparison.OrdinalIgnoreCase))
                    {
                        wishListDM = currentWishListDM;
                    }

                    if (currentWishListDM.IsFavorite)
                    {
                        currentFavoriteWishList = currentWishListDM;
                    }
                }
            }
            else
            {
                wishListDM = manager.GetCommerceList(wishList.Id, wishList.CustomerId);
            }

            bool update = false;

            if (wishList.Name != null && wishList.Name != wishListDM.Name)
            {
                wishListDM.Name = wishList.Name;
                update = true;
            }
            
            if (wishList.IsFavorite != null && wishList.IsFavorite != wishListDM.IsFavorite)
            {
                wishListDM.IsFavorite = Convert.ToBoolean(wishList.IsFavorite, CultureInfo.InvariantCulture);
                update = true;

                if ((wishList.IsFavorite ?? false) && !string.IsNullOrWhiteSpace(currentFavoriteWishList.Id))
                {
                    currentFavoriteWishList.IsFavorite = false;
                    currentFavoriteWishList = manager.UpdateCommerceListProperties(currentFavoriteWishList);
                }
            }

            if (update)
            {
                wishListDM = manager.UpdateCommerceListProperties(WishListMapper.ConvertToDataModel(wishList));
                wishList = WishListMapper.ConvertToViewModel(wishListDM);
            }

            return wishList;
        }

        /// <summary>
        /// Remove item from wish list.
        /// </summary>
        /// <param name="wishListId">The wish list identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="listingId">The item to remove from the wish list.</param>
        /// <exception cref="System.ArgumentNullException">
        /// wishListId
        /// or
        /// customerId
        /// or
        /// listings
        /// </exception>
        public virtual void RemoveItemFromWishList(string wishListId, string customerId, string listingId)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentNullException("wishListId");
            }

            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentNullException("customerId");
            }

            if (listingId == null)
            {
                throw new ArgumentNullException("listings");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            CommerceListManager manager = CommerceListManager.Create(runtime);
            CrtDataModel.CommerceList wishListDM = manager.GetCommerceList(wishListId, customerId);
            CrtDataModel.CommerceListLine wishListLine = wishListDM.CommerceListLines.Where(wll => wll.ProductId == Convert.ToInt64(listingId, CultureInfo.InvariantCulture)).SingleOrDefault();
            if (wishListLine != null)
            {
                manager.RemoveFromCommerceList(wishListLine.LineId);
            }
        }
    }
}