/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Customer Controller class.
    /// </summary>
    public class CustomerController : ControllerBase
    {
        private ChannelController channelControllerValue;
        private static object syncRoot = new object();
        // CS by muthait dated 26Nov2014 - changed private to public
        /// <summary>
        /// Represents the error message that the no customer currently exists with the provided email address.
        /// </summary>
        public const string NoCustomerExistsWithGivenEmailError = "NoCustomerExistsWithGivenEmailError";

        /// <summary>
        /// Email template for reset password email.
        /// </summary>
        public static readonly string ResetPasswordEmailTemplate = "ResetPass";


        IAuthorizationManager authorizationManagerValue;
        protected IAuthorizationManager AuthorizationManager
        {
            get
            {
                if (this.authorizationManagerValue == null)
                {
                    throw new InvalidOperationException("This call requires the Authorization Manager to be initialized by calling SetAuthorizationManager method.");
                }
                return this.authorizationManagerValue;
            }
            private set
            {
                this.authorizationManagerValue = value;
            }
        }

        public void SetAuthorizationManager(IAuthorizationManager manager)
        {
            this.AuthorizationManager = manager;
        }

        /// <summary>
        /// Get a customer by account number.
        /// </summary>
        /// <param name="accountNumber">The customer account number.</param>
        /// <returns>A Customer view model.</returns>
        public virtual Customer GetCustomer(string accountNumber)
        {
            var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.Customer dmCustomer = manager.GetCustomer(accountNumber);

            if (dmCustomer == null)
            {
                return null;
            }

            return CustomerMapper.ConvertToViewModel(dmCustomer);
        }

        /// <summary>
        /// Create a new customer.
        /// </summary>
        /// <param name="customer">An existing customer.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// A new customer, based on existing customer.
        /// </returns>
        public virtual Customer CreateCustomer(Customer customer, string userId)
        {
            var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.Customer newDMCustomer = CustomerMapper.UpdateDataModel(new CrtDataModel.Customer(), customer);

            CrtDataModel.Customer dmCustomer = manager.CreateCustomer(newDMCustomer);

            // Associate the newly created customer w/the current identity.
            CreateUserCustomerMap(dmCustomer, userId);

            Customer vmCustomer = CustomerMapper.ConvertToViewModel(dmCustomer);

            return vmCustomer;
        }

        /// <summary>
        /// Create a new customer.
        /// </summary>
        /// <param name="emailAddress">Email addres for an existing customer who is in 3rd party company.</param>
        /// <param name="activationToken">Activation token.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// A new customer in current company, based on existing customer.
        /// </returns>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">There are no customer that exists with the provided email address.</exception>
        public virtual Customer CreateCustomerByEmail(string emailAddress, string activationToken, string userId)
        {
            var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

            // Validate the request
            manager.ValidateAccountActivationRequest(emailAddress, activationToken);

            // Search customer by email address
            CrtDataModel.CustomerSearchCriteria criteria = new CrtDataModel.CustomerSearchCriteria() { Keyword = emailAddress };
            CrtDataModel.GlobalCustomer newDMGlobalCustomer = manager.SearchCustomers(criteria, new CrtDataModel.QueryResultSettings()).Results.FirstOrDefault();

            if (newDMGlobalCustomer == null)
            {
                string message = string.Format(CultureInfo.CurrentCulture, "Customer not found for the provided email address: {0}.", emailAddress);
                throw new DataValidationException(NoCustomerExistsWithGivenEmailError, message);
            }

            CrtDataModel.Customer dmCustomer = null;
            if (string.IsNullOrEmpty(newDMGlobalCustomer.AccountNumber))
            {
                CrtDataModel.Customer newCustomer = new CrtDataModel.Customer();
                newCustomer.NewCustomerPartyNumber = newDMGlobalCustomer.PartyNumber;
                dmCustomer = manager.CreateCustomer(newCustomer);
            }
            else
            {
                dmCustomer = manager.GetCustomer(newDMGlobalCustomer.AccountNumber);
            }

            // Associate the newly created customer w/the current identity.
            CreateUserCustomerMap(dmCustomer, userId);

            // Update channel DB to mark the request as 'finished'
            manager.FinalizeAccountActivation(emailAddress, activationToken);

            return CustomerMapper.ConvertToViewModel(dmCustomer);
        }

        /// <summary>
        /// Search for a customer given the corresponding email address.
        /// </summary>
        /// <param name="emailAddress">email address of the customer.</param>
        /// <returns>True if the customer exists, false otherwise.</returns>
        public virtual bool SearchCustomers(string emailAddress)
        {
            // Validate that there is a customer associated with the email address
            CustomerManager manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.CustomerSearchCriteria criteria = new CrtDataModel.CustomerSearchCriteria() { Keyword = emailAddress };
            PagedResult<CrtDataModel.GlobalCustomer> customers = manager.SearchCustomers(criteria, new CrtDataModel.QueryResultSettings());

            return customers.Results.Any();
        }

        /// <summary>
        /// Search for a customer given the corresponding email address.
        /// </summary>
        /// <param name="emailAddress">email address of the customer.</param>
        /// <returns>The customer Id if the customer was found, null otherwise.</returns>
        public virtual string SearchCustomersAndReturnCustomerId(string emailAddress)
        {
            // Validate that there is a customer associated with the email address
            CustomerManager manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.CustomerSearchCriteria criteria = new CrtDataModel.CustomerSearchCriteria() { Keyword = emailAddress };
            PagedResult<CrtDataModel.GlobalCustomer> customers = manager.SearchCustomers(criteria, new CrtDataModel.QueryResultSettings());

            if (customers.Results.Any())
            {
                return customers.Results[0].AccountNumber;
            }

            return null;
        }

        /// <summary>
        /// Update an existing customer.
        /// </summary>
        /// <param name="customer">An existing customer.</param>
        public virtual void UpdateCustomer(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }

            var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.Customer dmCustomer = manager.GetCustomer(customer.AccountNumber);
            dmCustomer = CustomerMapper.UpdateDataModel(dmCustomer, customer);

            dmCustomer = manager.UpdateCustomer(dmCustomer);
        }

        /// <summary>
        /// Update a customer's addresses.
        /// </summary>
        /// <param name="accountNumber">The customer account number.</param>
        /// <param name="updatedAddresses">The address.</param>
        public virtual void UpdateAddresses(string accountNumber, IEnumerable<Address> updatedAddresses)
        {
            var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
            CrtDataModel.Customer dmCustomer = manager.GetCustomer(accountNumber);

            if (dmCustomer == null)
            {
                // Customer not found
                return;
            }

            dmCustomer = CustomerMapper.ConvertToDataModel(dmCustomer, updatedAddresses);

            manager.UpdateCustomer(dmCustomer);

            return;
        }

        /// <summary>
        /// Adds the shipping address information to the customer address collection.
        /// </summary>
        /// <param name="cart">The shopping cart.</param>
        /// <param name="customerId">The customer id.</param>
        public virtual void UpdateCustomerAddresses(CrtDataModel.Cart cart, string customerId)
        {
            if (cart == null)
            {
                throw new ArgumentNullException("cart");
            }

            if (!String.IsNullOrWhiteSpace(customerId))
            {
                ChannelConfiguration channelConfiguration = this.ChannelController.GetChannelConfiguration();
                List<Address> addresses = new List<Address>();

                // We do not know if the delivery preference was entered at order level or item level at this point.
                // So we are adding all the order and item level addresses to the customer's saved addresses list.
                // If there is a shipping address at order level and delivery mode is not pick up in store or email delivery then update customer address list with the shipping address.
                if (cart.ShippingAddress != null && cart.DeliveryMode != channelConfiguration.PickupDeliveryModeCode && cart.DeliveryMode != channelConfiguration.EmailDeliveryModeCode)
                {
                    addresses.Add(AddressMapper.ConvertToViewModel(cart.ShippingAddress));
                }

                // Check the same for every cart line.
                foreach (CrtDataModel.CartLine cartLine in cart.CartLines)
                {
                    Address vmCartLineShippingAddress = AddressMapper.ConvertToViewModel(cartLine.ShippingAddress);
                    if (cartLine.ShippingAddress != null &&
                        cartLine.DeliveryMode != channelConfiguration.PickupDeliveryModeCode &&
                        cartLine.DeliveryMode != channelConfiguration.EmailDeliveryModeCode)
                    {
                        addresses.Add(vmCartLineShippingAddress);
                    }
                }

                if (addresses.Any())
                {
                    addresses = addresses.GroupBy(a => new
                    {
                        a.Country, a.ZipCode, a.State, a.City, a.Street, a.Name, a.County, a.AttentionTo, a.DistrictName,
                        a.AddressType, a.AddressFriendlyName, a.Deactivate, a.Email, a.EmailContent, a.IsPrimary, a.Phone,
                        a.RecordId, a.StreetNumber, a.ExtensionData
                    })
                        .Select(a => a.First())
                        .ToList();

                    this.UpdateAddresses(customerId, addresses);
                }
            }
        }

        /// <summary>
        /// Get a customer's addresses.
        /// </summary>
        /// <param name="accountNumber">The customer account number.</param>
        /// <returns>
        /// The customer's addresses.
        /// </returns>
        public virtual Collection<Address> GetAddresses(string accountNumber)
        {
            var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
            CrtDataModel.Customer dmCustomer = manager.GetCustomer(accountNumber);

            if (dmCustomer == null)
            {
                // Customer not found
                return null;
            }

            Collection<Address> allAddresses = CustomerMapper.ConvertToViewModel(dmCustomer.Addresses);

            return allAddresses;
        }

        /// <summary>
        /// Associate an existing customer to the current credentials if the criteria matches.
        /// </summary>
        /// <param name="email">The customer's e-mail address.</param>
        /// <param name="givenName">Name of the given.</param>
        /// <param name="surname">Name of the sur.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// The customer's account number as string.
        /// </returns>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Thrown when the customer cannot be found.</exception>
        public virtual string AssociateCustomer(string email, string givenName, string surname, string userId)
        {
            // get customer by email and loyalty card number
            var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.Customer dmCustomer;

            string firstName = !string.IsNullOrWhiteSpace(givenName) ? givenName : email;
            dmCustomer = new CrtDataModel.Customer()
            {
                Email = email,
                FirstName = firstName,
                LastName = surname,
                Language = CultureInfo.CurrentUICulture.ToString(),
            };

            dmCustomer = manager.CreateCustomer(dmCustomer);

            CreateUserCustomerMap(dmCustomer, userId);

            return dmCustomer.AccountNumber;
        }

        /// <summary>
        /// Gets the customer account number using the specified e-mail address.
        /// </summary>
        /// <param name="emailAddress">The e-mail address.</param>
        /// <returns>
        /// The customer identifier.
        /// </returns>
        public virtual string GetRegisteredCustomerIdByEmailAddress(string emailAddress)
        {
            string userId = string.Concat("f|%|", emailAddress);

            UserCustomerMap userCustomerMap;
            if (this.AuthorizationManager.TryReadCustomerMap(userId, out userCustomerMap))
            {
                if (userCustomerMap.Status == UserCustomerMapStatus.Active)
                {
                    return userCustomerMap.CustomerId;
                }
            }

            return null;
        }

        /// <summary>
        /// Sends an email with an activation Url to the customer.
        /// </summary>
        /// <param name="customerId">The customer Id.</param>
        /// <param name="templateId">The email template Id.</param>
        /// <param name="activationUrl">The activation Url.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "templateId", Justification = "The parameter will be used in future.")]
        public virtual void SendEmailToCustomer(string customerId, string templateId, string activationUrl)
        {
            var properties = new Collection<CrtDataModel.NameValuePair>
            {
                new CrtDataModel.NameValuePair
                {
                    Name = "ActivationUrl",
                    Value = activationUrl,
                }
            };

            var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
            manager.SendEmailToCustomer(customerId, ResetPasswordEmailTemplate, properties);
        }

        /// <summary>
        /// Sends email to the customer with the activation Url.
        /// </summary>
        /// <param name="emailAddress">Customer's email address.</param>
        /// <param name="emailTemplateId">Template Id of the email.</param>
        /// <param name="customerName">Customer's name.</param>
        /// <param name="activationUrl">The activation Url.</param>
        /// <param name="activationToken">The activation token.</param>
        /// <param name="storefrontName">The storefront name.</param>
        public virtual void SendAccountActivationEmailToCustomer(string emailAddress, string emailTemplateId, string customerName, string activationUrl, string activationToken, string storefrontName)
        {
            var properties = new Collection<Microsoft.Dynamics.Commerce.Runtime.DataModel.NameValuePair>
            {
                new Microsoft.Dynamics.Commerce.Runtime.DataModel.NameValuePair
                {
                    Name = "customername",
                    Value = customerName
                },
                new Microsoft.Dynamics.Commerce.Runtime.DataModel.NameValuePair
                {
                    Name = "ActivationLink",
                    Value = activationUrl
                },
                new Microsoft.Dynamics.Commerce.Runtime.DataModel.NameValuePair
                {
                    Name = "CompanyName",
                    Value = storefrontName
                },
                new Microsoft.Dynamics.Commerce.Runtime.DataModel.NameValuePair
                {
                    Name = "ActivationToken",
                    Value = activationToken
                }                    
            };

            CustomerManager manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
            manager.SendAccountActivationEmailToCustomer(emailAddress, emailTemplateId, properties);
        }

        /// <summary>
        /// Checks that the activation token is valid given an email address.
        /// </summary>
        /// <param name="emailAddress">The user's email address.</param>
        /// <param name="activationToken">The activation token.</param>
        public virtual void ValidateAccountActivationRequest(string emailAddress, string activationToken)
        {
            CustomerManager manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
            manager.ValidateAccountActivationRequest(emailAddress, activationToken);
        }

        /// <summary>
        /// Creates the user customer map.
        /// </summary>
        /// <param name="dmCustomer">The data model customer.</param>
        /// <param name="userId">The user identifier.</param>
        protected virtual void CreateUserCustomerMap(CrtDataModel.Customer dmCustomer, string userId)
        {
            if (dmCustomer == null)
            {
                throw new ArgumentNullException("dmCustomer");
            }

            UserCustomerMap userCustomerMap = new UserCustomerMap()
                {
                    CustomerId = dmCustomer.AccountNumber,
                    CustomerVerifiedDateTime = DateTime.UtcNow,
                    Status = UserCustomerMapStatus.Active,
                    UserId = userId,
                };

            this.AuthorizationManager.CreateUserCustomerMap(userCustomerMap);
        }

        /// <summary>
        /// Gets an instance of ChannelController.
        /// </summary>
        protected virtual ChannelController ChannelController
        {
            get
            {
                if (this.channelControllerValue == null)
                {
                    lock (syncRoot)
                    {
                        if (this.channelControllerValue == null)
                        {
                            this.channelControllerValue = new ChannelController();
                            this.channelControllerValue.SetConfiguration(this.Configuration);
                        }
                    }
                }

                return this.channelControllerValue;
            }
        }
    }
}