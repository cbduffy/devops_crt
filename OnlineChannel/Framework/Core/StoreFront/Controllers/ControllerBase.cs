﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;

    /// <summary>
    /// Base class for the controllers.
    /// </summary>
    public class ControllerBase
    {
        private IConfiguration configurationValue;

        /// <summary>
        /// Sets the configuration for the controller.
        /// </summary>
        /// <param name="configuration">The configuration object.</param>
        public void SetConfiguration(IConfiguration configuration)
        {
            this.configurationValue = configuration;
        }

        /// <summary>
        /// Gets the configuration for the controller.
        /// </summary>
        protected IConfiguration Configuration
        {
            get
            {
                if (this.configurationValue == null)
                {
                    throw new InvalidOperationException("This call requires the configuration to be intiazlied by calling SetConfiguration method.");
                }

                return this.configurationValue;
            }
        }
    }
}
