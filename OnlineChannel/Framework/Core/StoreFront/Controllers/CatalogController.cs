﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// The controller for the Catalog.
    /// </summary>
    public class CatalogController : ControllerBase
    {
        /// <summary>
        /// Returns a read only collection of all catalog Ids that belong to a specific store.
        /// </summary>
        /// <param name="storeId">The store Id.</param>
        /// <returns>A read only collection of catalog Ids.</returns>
        public virtual ReadOnlyCollection<long> GetActiveProductCatalogs(long storeId)
        {
            PagedResult<ProductCatalog> productCatalogsResult = null;

            ProductManager productManager = ProductManager.Create(CrtUtilities.GetCommerceRuntime());

            // TODO: the result should be filtered to remove catalogs which were not published to this (used at publishing time) channel,
            // otherwise it might cause performance issues in SP search because we will add too many parameters which in fact
            // could belong to the catalogs which were not published to *this* channel at all, as a result search experience will be slower than it could be.
            productCatalogsResult = productManager.GetProductCatalogs(storeId, true, new QueryResultSettings());

            ReadOnlyCollection<ProductCatalog> productCatalogs = productCatalogsResult.Results;
            List<long> activeProductCatalogIds = new List<long>();

            foreach (ProductCatalog catalog in productCatalogs)
            {
                activeProductCatalogIds.Add(catalog.RecordId);
            }

            return activeProductCatalogIds.AsReadOnly();
        }
    }
}
