﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using ViewModel = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// The controller for the Channel.
    /// </summary>
    public class ChannelController : ControllerBase
    {
        /// <summary>
        /// Gets the channel configuration.
        /// </summary>
        /// <returns>The channel configuration.</returns>
        public virtual ViewModel.ChannelConfiguration GetChannelConfiguration()
        {
            ChannelManager channelManager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());
            ChannelConfiguration channelConfiguration = channelManager.GetChannelConfiguration();
            var channelMapper = new ChannelMapper(this.Configuration);
            return channelMapper.ConvertToViewModel(channelConfiguration);
        }

        /// <summary>
        /// Gets the channel default culture.
        /// </summary>
        /// <returns>The channel default culture.</returns>
        public virtual CultureInfo GetChannelDefaultCulture()
        {
            return CrtUtilities.GetChannelDefaultCulture();
        }

        /// <summary>
        /// Gets channel tender types.
        /// </summary>
        /// <returns>Channel tender types.</returns>
        public virtual IEnumerable<Models.TenderType> GetTenderTypes()
        {
            ChannelManager manager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());
            ReadOnlyCollection<CrtDataModel.TenderType> tenderTypes = manager.GetChannelTenderTypes(new QueryResultSettings()).Results;

            IEnumerable<Models.TenderType> operationIds = tenderTypes.Select(t => (Models.TenderType)t.OperationId);

            return operationIds;
        }

        /// <summary>
        /// Gets the region info for all countries.
        /// </summary>
        public virtual IEnumerable<Models.CountryInfo> GetCountryRegionInfo()
        {
            ChannelManager manager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());

            //Selecting Country ShortName to sort
            SortingInfo sort = new SortingInfo("ShortName");
            QueryResultSettings qrSettings = new QueryResultSettings(sort);

            ReadOnlyCollection<CountryRegionInfo> countryRegionInfoCollection =
                manager.GetCountries(CrtUtilities.GetChannelDefaultCulture().Name, qrSettings).Results;

            IEnumerable<Models.CountryInfo> countryInfoCollection = ChannelMapper.ConvertToViewModel(countryRegionInfoCollection);

            //Ordering on Country name as CRT returns ordered by ISOCode
            countryInfoCollection = countryInfoCollection.OrderBy(countryInfo => countryInfo.CountryName);

            return countryInfoCollection;
        }

        /// <summary>
        /// Gets the states/provinces data for the given country.
        /// </summary>
        /// <param name="countryCode">Country code.</param>
        /// <returns>The states/provinces for the given country.</returns>
        public virtual IEnumerable<Models.StateProvinceInfo> GetStateProvinces(string countryCode)
        {
            ChannelManager channelManager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());
            ReadOnlyCollection<StateProvinceInfo> dmStateProvinceInfo = channelManager.GetStateProvinces(countryCode, new QueryResultSettings()).Results;
            IEnumerable<Models.StateProvinceInfo> stateProvinceInfo = ChannelMapper.ConvertToViewModel(dmStateProvinceInfo);

            //Ordering the states by name.
            stateProvinceInfo = stateProvinceInfo.OrderBy(spi => spi.StateName);
            return stateProvinceInfo;
        }
    }
}
