/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Store product availability controller
    /// </summary>
    public class PricingController : ControllerBase
    {
        /// <summary>
        /// Get latest prices for provided listings.
        /// </summary>
        /// <param name="productIds">Product identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="activeDate">Date that the price is needed for.</param>
        /// <param name="customerId">The customer id.</param>
        /// <returns>Price of listing.</returns>
        public virtual ReadOnlyCollection<ListingPrice> GetActiveListingPrice(IEnumerable<long> productIds, long channelId, long? catalogId, DateTime activeDate, string customerId)
        {
            return GetActiveListingPrice(productIds, channelId, catalogId, activeDate, customerId, null);
        }
        
        /// <summary>
        /// Get latest prices for provided listings.
        /// </summary>
        /// <param name="productIds">Product identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="activeDate">Date that the price is needed for.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tiers to be taken into account while calculating prices.</param>
        /// <returns>Price of listing.</returns>
        public virtual ReadOnlyCollection<ListingPrice> GetActiveListingPrice(IEnumerable<long> productIds, long channelId, long? catalogId, DateTime activeDate, string customerId, IEnumerable<long> affiliationLoyaltyTierIds)
        {
            var runtime = CrtUtilities.GetCommerceRuntime();
            var manager = ProductManager.Create(runtime);

            if (channelId == 0)
            {
                var channelManager = ChannelManager.Create(runtime);
                channelId = channelManager.GetCurrentChannelId();
            }

            var context = new ProjectionDomain(channelId, catalogId);

            IList<AffiliationLoyaltyTier> tiers = null;
            if (affiliationLoyaltyTierIds != null)
            {
                tiers = new List<AffiliationLoyaltyTier>();
                foreach (long affiliationLoyaltyTierId in affiliationLoyaltyTierIds)
                {
                    tiers.Add(new AffiliationLoyaltyTier { AffiliationId = affiliationLoyaltyTierId, AffiliationType = RetailAffiliationType.General });
                }
            }
            var productPrices = manager.GetActiveProductPrice(context, productIds, activeDate, customerId, tiers);

            return new PricingMapper(this.Configuration).ConvertToViewModel(productPrices).AsReadOnly();
        }

        /// <summary>
        ///  Gets product prices and calculated discounted price for the given channel, catalog and affiliations.
        /// </summary>
        /// <param name="productIds">Product identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tiers to be taken into account while calculating prices.</param>
        /// <returns>Independent product prices and calculated discounted price for the given product identifiers.</returns>
        public virtual ReadOnlyCollection<ListingPrice> GetIndependentProductPriceDiscount(IEnumerable<long> productIds, long channelId, long? catalogId, string customerId, IEnumerable<long> affiliationLoyaltyTierIds)
        {
            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            ProductManager manager = ProductManager.Create(runtime);

            if (channelId == 0)
            {
                ChannelManager channelManager = ChannelManager.Create(runtime);
                channelId = channelManager.GetCurrentChannelId();
            }

            ProjectionDomain context = new ProjectionDomain(channelId, catalogId);

            IList<AffiliationLoyaltyTier> tiers = null;
            if (affiliationLoyaltyTierIds != null)
            {
                tiers = new List<AffiliationLoyaltyTier>();
                foreach (long affiliationLoyaltyTierId in affiliationLoyaltyTierIds)
                {
                    tiers.Add(new AffiliationLoyaltyTier { AffiliationId = affiliationLoyaltyTierId, AffiliationType = RetailAffiliationType.General });
                }
            }

            ReadOnlyCollection<ProductPrice> productPrices = manager.GetIndependentProductPriceDiscount(context, productIds, customerId, tiers);

            return new PricingMapper(this.Configuration).ConvertToViewModel(productPrices).AsReadOnly();
        }
    }
}