﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;

    /// <summary>
    /// Store locator controller
    /// </summary>
    public static class StoreLocationController
    {
        /// <summary>
        /// Get nearby stores.
        /// </summary>
        /// <param name="latitude">The latitude of the location to search for stores.</param>
        /// <param name="longitude">The longiture of the location to search for stores.</param>
        /// <param name="distance">distance to search in miles.</param>
        /// <returns>Response instance.</returns>
        /// <exception cref="ArgumentException">Thrown when one of the input parameters is invalid.</exception>
        public static Collection<StoreLocation> GetNearbyStores(decimal latitude, decimal longitude, int distance)
        {
            Collection<StoreLocation> stores = new Collection<StoreLocation>();

            QueryResultSettings criteria = new QueryResultSettings();

            SearchArea searchArea = new SearchArea();
            searchArea.Latitude = latitude;
            searchArea.Longitude = longitude;
            searchArea.DistanceUnit = DistanceUnit.Miles;
            searchArea.Radius = (distance == 0 ) ? 200 : distance; /* If the client does not specify the radius for search it is defaulted to 200 miles */

            var manager = Microsoft.Dynamics.Commerce.Runtime.Client.StoreLocatorManager.Create(CrtUtilities.GetCommerceRuntime());
            IEnumerable<Microsoft.Dynamics.Commerce.Runtime.DataModel.OrgUnitLocation> storeLocations = manager.GetStoreLocations(criteria, searchArea);
            foreach (Microsoft.Dynamics.Commerce.Runtime.DataModel.OrgUnitLocation storeLocation in storeLocations)
            {
                stores.Add(StoreLocationMapper.ConvertToViewModel(storeLocation));
            }

            return stores;
        }

        /// <summary>
        /// Gets the default store identifier.
        /// </summary>
        /// <returns>The default store identifier.</returns>
        public static long GetDefaultStoreId()
        {
            long storeId = 0;

            StoreLocatorManager manager = StoreLocatorManager.Create(CrtUtilities.GetCommerceRuntime());
            ReadOnlyCollection<Commerce.Runtime.DataModel.OrgUnitLocation> storeLocations = manager.GetStoreLocations(new QueryResultSettings());

            if (!storeLocations.IsNullOrEmpty())
            {
                storeId = storeLocations.First().ChannelId;
            }
            else
            {
                throw new DataValidationException(Resources.Microsoft_Dynamics_Commerce_Runtime_ObjectNotFound, "The default store identifier could not be found.");
            }

            return storeId;
        }
    }
}