/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Store product availability controller
    /// </summary>
    public class StoreProductAvailabilityController : ControllerBase
    {
        /// <summary>
        /// Get stores nearby with product availability.
        /// </summary>
        /// <param name="latitude">The latitude of the location to search for stores.</param>
        /// <param name="longitude">The longiture of the location to search for stores.</param>
        /// <param name="TransactionItems">Shopping cart items.</param>
        /// <returns>Response instance.</returns>
        /// <exception cref="ArgumentException">Thrown when one of the input parameters is invalid.</exception>
        public virtual Collection<StoreProductAvailability> GetNearbyStoresWithAvailability(decimal latitude, decimal longitude, IEnumerable<TransactionItem> shoppingCartItems)
        {
            if (shoppingCartItems == null)
            {
                throw new ArgumentNullException("shoppingCartItems");
            }

            Collection<StoreProductAvailability> productAvailability = new Collection<StoreProductAvailability>();

            QueryResultSettings criteria = new QueryResultSettings();
            List<ItemUnit> items = new List<ItemUnit>();
            foreach (TransactionItem shoppingCartItem in shoppingCartItems)
            {
                items.Add(new ItemUnit() { ItemId = shoppingCartItem.ItemId, VariantInventoryDimensionId = shoppingCartItem.VariantInventoryDimensionId });
            }

            SearchArea searchArea = new SearchArea();
            searchArea.Latitude = latitude;
            searchArea.Longitude = longitude;
            searchArea.DistanceUnit = DistanceUnit.Miles;
            searchArea.Radius = 200;

            var manager = InventoryManager.Create(CrtUtilities.GetCommerceRuntime());
            IEnumerable<OrgUnitAvailability> stores = manager.GetStoreAvailabilities(criteria, items, searchArea);
            foreach (var store in stores)
            {
                productAvailability.Add(StoreProductAvailabilityMapper.ConvertToViewModel(store, shoppingCartItems));
            }

            return productAvailability;
        }

        /// <summary>
        /// Get available quantities of specified listings.
        /// </summary>
        /// <param name="listingIds">Listing identifiers.</param>
        /// <param name="customerId">The customer id.</param>
        /// <returns>
        /// A collection of available quantities for the listings inquired.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">listingIds</exception>
        public virtual IEnumerable<ListingAvailableQuantity> GetListingAvailableQuantity(IEnumerable<long> listingIds, string customerId)
        {
            if (listingIds == null)
            {
                throw new ArgumentNullException("listingIds");
            }

            Collection<StoreProductAvailability> productAvailability = new Collection<StoreProductAvailability>();

            QueryResultSettings queryResultSettings = new QueryResultSettings();
            queryResultSettings.Paging.Top = listingIds.Count();

            var runtime = CrtUtilities.GetCommerceRuntime();
            var manager = InventoryManager.Create(runtime);

            IEnumerable<Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductAvailableQuantity> result;
            IEnumerable<Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductAvailableQuantity> returnedQuantities = manager.GetProductAvailabilities(queryResultSettings, listingIds, 0, customerId);
            var returnedListingIds = returnedQuantities.Select(key => key.ProductId);
            var missingListingIds = listingIds.Except(returnedListingIds);
            IEnumerable<Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductAvailableQuantity> missingQuantities = missingListingIds.Select(listingId => new Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductAvailableQuantity
            {
                ProductId = listingId,
                AvailableQuantity = 0,
                UnitOfMeasure = string.Empty,
            });

            result = returnedQuantities.Union(missingQuantities);

            return StoreProductAvailabilityMapper.ConvertToViewModel(result);
        }
    }
}