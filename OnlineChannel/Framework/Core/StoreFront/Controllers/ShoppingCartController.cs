﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// The controller for the Shopping Cart.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Refactoring will be costly at this stage. Should be taken up in next cycle.")]
    public class ShoppingCartController : ControllerBase
    {
        private const string PropertyKeyImage = "Image";

        /// <summary>
        /// Represents the term after which a authenticated users' cart expires 
        /// </summary>
        private const string ShoppingCartExpiryTermPropertyName = "StoreFront_ShoppingCartExpiryTerm";

        private PricingController pricingControllerValue;
        private StoreProductAvailabilityController storeProductAvailabilityControllerValue;
        private static object pricingSyncRoot = new object();
        private static object storeProductAvailabilitySyncRoot = new object();

        /// <summary>
        /// Gets the shopping cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null or empty.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} was not found.</exception>
        public virtual ShoppingCart GetShoppingCart(string shoppingCartId, string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            OrderManager manager = OrderManager.Create(runtime);
            Cart cart = manager.GetCart(shoppingCartId, customerId, CalculationModes.All);
            ShoppingCart shoppingCart;

            if (cart == null)
            {
                throw new DataValidationException(DataValidationErrors.CartNotFound, "Shopping cart {0} was not found.", shoppingCartId);
            }

            if (dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            return shoppingCart;
        }

        /// <summary>
        /// Adds the items to the shopping cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="listings">The listings.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>A shopping cart.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when listings is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} was not found.</exception>
        public virtual ShoppingCart AddItems(string shoppingCartId, string customerId, IEnumerable<Models.Listing> listings, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (listings == null)
            {
                throw new ArgumentNullException("listings");
            }

            ShoppingCart shoppingCart = null;

            if (!string.IsNullOrWhiteSpace(customerId))
            {
                //Always get the active shopping cart before adding items for an authenticated user.
                shoppingCart = GetActiveShoppingCart(customerId, dataLevel, productValidator, searchEngine);
                // if active shopping cart does not exist create an new cart Id.
                shoppingCartId = shoppingCart == null ? string.Empty : shoppingCart.CartId;
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                shoppingCartId = TokenHelper.GenerateTransactionId();
                // Create empty cart if it does not exist yet.
                manager.CreateOrUpdateCart(new Cart() { Id = shoppingCartId, CustomerId = customerId }, CalculationModes.None);
            }

            CalculationModes modes = CalculationModes.Prices | CalculationModes.Discounts | CalculationModes.Totals;
            ICollection<CartLine> cartLines = new ShoppingCartMapper(this.Configuration).GetCartLinesFromListing(listings, productValidator);
            Cart cart = manager.AddCartLines(shoppingCartId, customerId, cartLines, modes);

            if (cart == null)
            {
                throw new DataValidationException(DataValidationErrors.CartNotFound, "Shopping cart {0} was not found.", shoppingCartId);
            }

            if (dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            return shoppingCart;
        }

        /// <summary>
        /// Removes the items from the shopping cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="lineIds">The line ids.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// A shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} was not found.</exception>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null.</exception>
        public virtual ShoppingCart RemoveItems(string shoppingCartId, string customerId, IEnumerable<string> lineIds, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (lineIds == null)
            {
                throw new ArgumentNullException("lineIds");
            }

            CalculationModes modes = CalculationModes.Prices | CalculationModes.Discounts | CalculationModes.Totals;
            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            Cart cart = manager.DeleteCartLines(shoppingCartId, customerId, lineIds, modes);

            if (cart == null)
            {
                throw new DataValidationException(DataValidationErrors.CartNotFound, "Shopping cart {0} was not found.", shoppingCartId);
            }

            ShoppingCart shoppingCart;
            if (dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            return shoppingCart;
        }

        /// <summary>
        /// Sets AffiliationLoyaltyTiers on cart level.
        /// </summary>
        /// <param name="cartId">The cart ID.</param>
        /// <param name="affiliationLoyaltyTierIds">Collection of AffiliationLoyaltyTier IDs.</param>
        /// <remarks>The tiers are created with type General.</remarks>
        public virtual void SetCartAffiliationLoyaltyTiers(string cartId, IEnumerable<long> affiliationLoyaltyTierIds)
        {
            if (affiliationLoyaltyTierIds == null)
            {
                throw new ArgumentNullException("affiliationLoyaltyTierIds");
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            Cart cart = manager.GetCart(cartId, CalculationModes.None);
            cart.AffiliationLines.Clear();

            IList<AffiliationLoyaltyTier> tiers = new List<AffiliationLoyaltyTier>();
            foreach (long affiliationLoyaltyTierId in affiliationLoyaltyTierIds)
            {
                cart.AffiliationLines.Add(new AffiliationLoyaltyTier { AffiliationId = affiliationLoyaltyTierId, AffiliationType = RetailAffiliationType.General });
            }

            manager.CreateOrUpdateCart(cart);
        }

        /// <summary>
        /// Gets General AffiliationLoyaltyTier IDs applied to the card.
        /// </summary>
        /// <param name="cartId">The Cart ID.</param>
        /// <returns>Collection of AffiliationLoyaltyTier IDs.</returns>
        public virtual IEnumerable<long> GetCartAffiliationLoyaltyTiers(string cartId)
        {
            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            Cart cart = manager.GetCart(cartId, CalculationModes.None);
            IList<long> tierIds = new List<long>();
            foreach (AffiliationLoyaltyTier tier in cart.AffiliationLines)
            {
                if (tier.AffiliationType == RetailAffiliationType.General)
                {
                    tierIds.Add(tier.AffiliationId);
                }
            }

            return tierIds;
        }

        /// <summary>
        /// Updates the items.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="items">The items.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when items is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">
        /// Shopping cart {0} was not found.
        /// </exception>
        public virtual ShoppingCart UpdateItems(string shoppingCartId, string customerId, IEnumerable<TransactionItem> items, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            Cart currentCart = manager.GetCart(shoppingCartId, customerId, CalculationModes.None);
            foreach (TransactionItem i in items)
            {
                if (i.Quantity <= 0)
                {
                    string message = string.Format("The quantity of an item must be positive - Line:{0} Item:{1} Quantity:{2}", i.LineId, i.ProductId, i.Quantity);
                    throw new DataValidationException(DataValidationErrors.InvalidQuantity, message);
                }

                CartLine cartLine = currentCart.CartLines.Where(l => l.LineId == i.LineId).Single();
                cartLine.Quantity = i.Quantity;
            }

            Cart cart = manager.UpdateCartLines(shoppingCartId, customerId, currentCart.CartLines, CalculationModes.All);

            if (cart == null)
            {
                throw new DataValidationException(DataValidationErrors.CartNotFound, "Shopping cart {0} was not found.", shoppingCartId);
            }

            ShoppingCart shoppingCart;
            if (dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            return shoppingCart;
        }

        /// <summary>
        /// Moves the items between carts.
        /// </summary>
        /// <param name="sourceShoppingCartId">The source shopping cart identifier.</param>
        /// <param name="destinationShoppingCartId">The destination shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">The data level.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// sourceShoppingCartId
        /// or
        /// destinationShoppingCartId
        /// </exception>
        public virtual ShoppingCart MoveItemsBetweenCarts(string sourceShoppingCartId, string destinationShoppingCartId, string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(sourceShoppingCartId))
            {
                throw new ArgumentNullException("sourceShoppingCartId");
            }

            if (string.IsNullOrWhiteSpace(destinationShoppingCartId))
            {
                throw new ArgumentNullException("destinationShoppingCartId");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            OrderManager manager = OrderManager.Create(runtime);

            Cart cart = manager.GetCart(sourceShoppingCartId, customerId, CalculationModes.None);

            ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
            ShoppingCart sourceShoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);

            if (sourceShoppingCart != null && sourceShoppingCart.Items.Any())
            {
                var listingsToMove = new ShoppingCartMapper(this.Configuration).GetListingsFromCart(sourceShoppingCart);
                ShoppingCart destinationShoppingCart = AddItems(destinationShoppingCartId, customerId, listingsToMove, dataLevel, productValidator, searchEngine);

                // Line ids to delete from source.
                IEnumerable<string> lineIdsToDelete = sourceShoppingCart.Items.Select(i => i.LineId);
                sourceShoppingCart = RemoveItems(sourceShoppingCartId, customerId, lineIdsToDelete, dataLevel, productValidator, searchEngine);

                return destinationShoppingCart;
            }
            else
            {
                ShoppingCart destinationShoppingCart = GetShoppingCart(destinationShoppingCartId, customerId, dataLevel, productValidator, searchEngine);
                return destinationShoppingCart;
            }
        }

        /// <summary>
        /// Get all the promotions for the items in the cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        public virtual ShoppingCart GetAllPromotionsForShoppingCart(string shoppingCartId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            OrderManager manager = OrderManager.Create(runtime);

            Cart cart = manager.GetPromotionsForCart(shoppingCartId);

            if (cart == null)
            {
                throw new DataValidationException(DataValidationErrors.CartNotFound, "Shopping cart {0} was not found.", shoppingCartId);
            }

            ShoppingCart shoppingCart;
            if (dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            return shoppingCart;
        }

        /// <summary>
        /// Starts the checkout process by creating a secure cart and returning it.
        /// </summary>
        /// <param name="shoppingCartId">The current cart id to be used for the checkout process.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="previousCheckoutCartId">The identifier for any previous checkout cart that was abandoned.</param>
        /// <param name="productValidator">The product validator.</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// A new cart with a random cart id that should be used during the secure checkout process.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when shoppingCartId is null.</exception>
        public virtual ShoppingCart CommenceCheckout(string shoppingCartId, string customerId, string previousCheckoutCartId, IProductValidator productValidator, ISearchEngine searchEngine, ShoppingCartDataLevel dataLevel)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            // Get the saved cart
            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            Cart shoppingCart = manager.GetCart(shoppingCartId, customerId, CalculationModes.None);

            string checkoutCartId = TokenHelper.GenerateTransactionId();
            Cart checkoutCart = new Cart { Id = checkoutCartId, CustomerId = customerId, CartType = CrtDataModel.CartType.Checkout, LoyaltyCardId = shoppingCart.LoyaltyCardId };
            checkoutCart = manager.CreateOrUpdateCart(checkoutCart, CalculationModes.None);

            // Add the cart lines from the shopping cart to checkout cart such that the original cart line ids are retained.
            checkoutCart = manager.AddCartLines(checkoutCartId, checkoutCart.CustomerId, shoppingCart.CartLines);

            if (shoppingCart.DiscountCodes != null && shoppingCart.DiscountCodes.Any())
            {
                checkoutCart = manager.AddDiscountCodesToCart(checkoutCartId, new Collection<string>(shoppingCart.DiscountCodes));
            }

            ShoppingCart checkoutCartVM;
            if (dataLevel == ShoppingCartDataLevel.All)
            {
                checkoutCartVM = AddKitDetailsToShoppingCart(checkoutCart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                checkoutCartVM = mapper.ConvertToViewModel(checkoutCart, dataLevel, productValidator, searchEngine);
            }

            // Delete any previous checkout cart.
            if (!string.IsNullOrWhiteSpace(previousCheckoutCartId))
            {
                try
                {
                    manager.DeleteCarts(customerId, new string[] { previousCheckoutCartId });
                }
                catch (CommerceRuntimeException e)
                {
                    NetTracer.Warning(e, "ShoppingCartController::CommenceCheckout - Failed to delete a previous checkout cart with id {0}.", previousCheckoutCartId);
                }
            }

            return checkoutCartVM;
        }

        /// <summary>
        /// Gets information about the specific kit configuration that has the provided kit line values.
        /// </summary>
        /// <param name="kitProductMasterIdentifier">The product identifier of the kit product master.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="kitLines">Details of each component line that comprises the kit.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>Specific kit configuration information.</returns>
        /// <exception cref="System.ArgumentNullException">kitLines</exception>
        /// <exception cref="System.InvalidOperationException">When no price is returned for the kit configuration variant.</exception>
        /// <exception cref="System.ArgumentNullException">kitLines</exception>
        /// <exception cref="System.ArgumentNullException">kitLines</exception>
        public virtual KitConfigurationInformation GetKitConfigurationInformation(long kitProductMasterIdentifier, long catalogId, IEnumerable<KitLine> kitLines, string customerId, ISearchEngine searchEngine)
        {
            if (kitLines == null)
            {
                throw new ArgumentNullException("kitLines");
            }

            if (kitProductMasterIdentifier == 0)
            {
                throw new ArgumentNullException("kitProductMasterIdentifier");
            }

            if (catalogId == 0)
            {
                throw new ArgumentNullException("catalogId");
            }

            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }

            Collection<KitLineProductProperty> kitLineProductProperties = new ShoppingCartMapper(this.Configuration).ConvertToDataModel(kitLines);
            KitConfigurationInformation kitConfigurationInfo = new KitConfigurationInformation();
            Tuple<string, string, long, string> kitVariantInfo = searchEngine.GetKitVariantProductInfo(kitProductMasterIdentifier, catalogId, kitLineProductProperties);
            kitConfigurationInfo.Title = kitVariantInfo.Item1;
            kitConfigurationInfo.CustomerItemNumber = kitVariantInfo.Item2;
            kitConfigurationInfo.ProductIdentifier = kitVariantInfo.Item3;
            kitConfigurationInfo.ConfigurationIdentifier = kitVariantInfo.Item4;
            if (kitConfigurationInfo.ProductIdentifier != 0)
            {
                List<long> listingIds = new List<long>();
                listingIds.Add(kitConfigurationInfo.ProductIdentifier);

                ListingPrice kitVariantListingPrice = this.PricingController.GetIndependentProductPriceDiscount(listingIds, 0, 0, customerId, null).FirstOrDefault(p => p.ListingId == kitConfigurationInfo.ProductIdentifier);
                if (kitVariantListingPrice == null)
                {
                    // We want to fail hard with a clear message if price is missing.
                    throw new InvalidOperationException(string.Format("No price was returned for product id {0}.", kitConfigurationInfo.ProductIdentifier));
                }
                kitConfigurationInfo.BasePriceWithCurrency = kitVariantListingPrice.BasePriceWithCurrency;
                kitConfigurationInfo.BasePrice = kitVariantListingPrice.BasePrice;
                kitConfigurationInfo.AdjustedPriceWithCurrency = kitVariantListingPrice.AdjustedPriceWithCurrency;
                kitConfigurationInfo.AdjustedPrice = kitVariantListingPrice.AdjustedPrice;
                kitConfigurationInfo.DiscountedPrice = kitVariantListingPrice.DiscountedPrice;
                kitConfigurationInfo.DiscountedPriceWithCurrency = kitVariantListingPrice.DiscountedPriceWithCurrency;

                kitConfigurationInfo.AvailabilityCount = this.StoreProductAvailabilityController.GetListingAvailableQuantity(listingIds, customerId).First(a => a.ListingId == kitConfigurationInfo.ProductIdentifier).AvailableQuantity;
            }

            return kitConfigurationInfo;
        }

        /// <summary>
        /// Adds or Removes the discount codes from the cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="promotionCode">The promotion code.</param>
        /// <param name="isAdd">Indicates whether the operation is addition or removal of discount codes.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <param name="productValidator">Product validator.</param>
        /// <param name="searchEngine">Search engine.</param>
        /// <returns>
        /// A shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when shoppingCartId or promotionCode is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} was not found.</exception>
        public virtual ShoppingCart AddOrRemovePromotionCode(string shoppingCartId, string customerId, string promotionCode, bool isAdd, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (string.IsNullOrWhiteSpace(promotionCode))
            {
                throw new ArgumentNullException("promotionCode");
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            Cart cart;

            if (isAdd)
            {
                cart = manager.AddDiscountCodesToCart(shoppingCartId, customerId, new Collection<string>() { promotionCode });
            }
            else
            {
                cart = manager.RemoveDiscountCodesFromCart(shoppingCartId, customerId, new Collection<string>() { promotionCode });
            }

            if (cart == null)
            {
                throw new DataValidationException(DataValidationErrors.CartNotFound, "Shopping cart {0} was not found.", shoppingCartId);
            }

            ShoppingCart shoppingCart;

            if (dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            return shoppingCart;
        }

        /// <summary>
        /// Gets the latest modified shopping cart associated with the current user.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The active shoppping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">customerId</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.SecurityException">Thrown when the user is not authorized to get shopping carts.</exception>
        public virtual ShoppingCart GetActiveShoppingCart(string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentNullException("customerId");
            }

            ShoppingCart shoppingCart;

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            Cart cart = manager.GetActiveCart(customerId, CalculationModes.All);

            if (cart != null && dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            if ((shoppingCart == null) || (HasCartExpired(shoppingCart.LastModifiedDate)))
            {
                return null;
            }

            return shoppingCart;
        }

        /// <summary>
        /// Claims the anonymous cart.
        /// </summary>
        /// <param name="shoppingCartIdToBeClaimed">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>A shopping cart.</returns>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} of customer {1} was not found.</exception>
        public virtual ShoppingCart ClaimAnonymousCart(string shoppingCartIdToBeClaimed, string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

            // First validate the cart to be claimed
            Cart cartToClaim = manager.GetCart(shoppingCartIdToBeClaimed, customerId, CalculationModes.None);

            // Make sure only anonymous cart can be claimed. CustomerId check is needed for defence in depth. 
            if (cartToClaim == null || !string.IsNullOrWhiteSpace(cartToClaim.CustomerId))
            {
                return null;
            }

            Cart cart = new Cart
            {
                Id = shoppingCartIdToBeClaimed,
                CustomerId = customerId,
                CartType = CrtDataModel.CartType.Shopping,
            };

            cart = manager.CreateOrUpdateCart(cart, CalculationModes.None);

            if (cart == null)
            {
                throw new DataValidationException(DataValidationErrors.CartNotFound, "Shopping cart {0} of customer {1} was not found.", shoppingCartIdToBeClaimed, customerId);
            }

            ShoppingCart shoppingCart;

            if (dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            return shoppingCart;
        }

        // CS by muthait dated 26Nov2014 changed protected to public
        /// <summary>
        /// Checks if the shopping cart of a customer has expired.
        /// <param name="cartLastModifiedDate">The last modified date of the shopping cart that is checked for expiry.</param>/>
        /// <returns>Boolean indicating whether the cart is expired or not.</returns>
        /// </summary>
        public virtual bool HasCartExpired(DateTime cartLastModifiedDate)
        {
            DateTime newDate = DateTime.UtcNow;
            try
            {
                int expiryTerm = Int32.Parse(ConfigurationManager.AppSettings[ShoppingCartExpiryTermPropertyName], CultureInfo.InvariantCulture);
                TimeSpan ts = newDate - cartLastModifiedDate;
                // Difference in days.
                int differenceInDays = ts.Days;
                return differenceInDays > expiryTerm;
            }
            catch (FormatException Ex)
            {
                throw new FormatException("Exception obtaining expiry term", Ex);
            }
        }

        /// <summary>
        /// Gets an instance of PricingController.
        /// </summary>
        protected virtual PricingController PricingController
        {
            get
            {
                if (this.pricingControllerValue == null)
                {
                    lock (pricingSyncRoot)
                    {
                        if (this.pricingControllerValue == null)
                        {
                            this.pricingControllerValue = new PricingController();
                            this.pricingControllerValue.SetConfiguration(this.Configuration);
                        }
                    }
                }

                return this.pricingControllerValue;
            }
        }

        /// <summary>
        /// Gets an instance of StoreProductAvailabilityController.
        /// </summary>
        protected virtual StoreProductAvailabilityController StoreProductAvailabilityController
        {
            get
            {
                if (this.storeProductAvailabilityControllerValue == null)
                {
                    lock (storeProductAvailabilitySyncRoot)
                    {
                        if (this.storeProductAvailabilityControllerValue == null)
                        {
                            this.storeProductAvailabilityControllerValue = new StoreProductAvailabilityController();
                            this.storeProductAvailabilityControllerValue.SetConfiguration(this.Configuration);
                        }
                    }
                }

                return this.storeProductAvailabilityControllerValue;
            }
        }
        //CS by muthait - 26NOV2014
        /// <summary>
        /// Populates the cart item with kit details.
        /// </summary>
        /// <param name="cart">The shopping cart.</param>
        public virtual ShoppingCart AddKitDetailsToShoppingCart(CrtDataModel.Cart cart, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            OrderManager manager = OrderManager.Create(runtime);

            // Get the product details for all the products in shopping cart.
            ReadOnlyCollection<Product> products = manager.GetProductsInCart(cart.Id);

            ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
            ShoppingCart shoppingCart = mapper.ConvertToViewModel(cart, ShoppingCartDataLevel.All, productValidator, searchEngine);
            KitMapper kitsMapper = new KitMapper(this.Configuration);
            kitsMapper.PopulateKitItemDetails(shoppingCart.Items, products, searchEngine);

            return shoppingCart;
        }
    }
}