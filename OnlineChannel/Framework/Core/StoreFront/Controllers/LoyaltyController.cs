﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// The controller for the customer loyalty card information.
    /// </summary>
    public class LoyaltyController : ControllerBase
    {
        private ShoppingCartController shoppingCartControllerValue;
        private static object syncRoot = new object();

        /// <summary>
        /// Generates loyalty card id and associates the loyalty card with the current cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart id.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// A shopping cart.
        /// </returns>
        public virtual ShoppingCart GenerateLoyaltyCardId(string shoppingCartId, string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrEmpty(customerId))
            {
                NetTracer.Warning("GenerateLoyaltyCardId is called when customer Id is null");
            }

            ShoppingCart shoppingCart = null;

            LoyaltyManager loyaltyManager = LoyaltyManager.Create(CrtUtilities.GetCommerceRuntime());
            CrtDataModel.LoyaltyCard loyaltyCard = loyaltyManager.IssueLoyaltyCard(null, customerId);

            if (String.IsNullOrEmpty(shoppingCartId))
            {
                shoppingCartId = TokenHelper.GenerateTransactionId();
                OrderManager orderManager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                CrtDataModel.Cart cart = new CrtDataModel.Cart { Id = shoppingCartId, CustomerId = customerId };
                orderManager.CreateOrUpdateCart(cart);
            }

            shoppingCart = UpdateLoyaltyCardId(shoppingCartId, loyaltyCard.CardNumber, customerId, dataLevel, productValidator, searchEngine);
            return shoppingCart;
        }

        /// <summary>
        /// Gets a read only collection with all loyalty card objects
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>
        /// read only collection of all loyalty card numbers
        /// </returns>
        protected virtual ReadOnlyCollection<CrtDataModel.LoyaltyCard> GetLoyaltyCardsInternal(string customerId)
        {
            Collection<CrtDataModel.LoyaltyCard> loyaltyCards = new Collection<CrtDataModel.LoyaltyCard>();
            if (!string.IsNullOrEmpty(customerId))
            {
                LoyaltyManager loyaltyManager = LoyaltyManager.Create(CrtUtilities.GetCommerceRuntime());
                loyaltyCards.AddRange(loyaltyManager.GetCustomerLoyaltyCards(customerId));
            }
            else
            {
                NetTracer.Warning("GetLoyaltyCards is called when customer Id is null");
            }

            return loyaltyCards.AsReadOnly();
        }

        /// <summary>
        /// Gets a read only collection with all loyalty card objects
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>
        /// read only collection of all loyalty card numbers
        /// </returns>
        public virtual ReadOnlyCollection<LoyaltyCard> GetLoyaltyCards(string customerId)
        {
            return LoyaltyMapper.ConvertToViewModel(GetLoyaltyCardsInternal(customerId));
        }

        /// <summary>
        /// Gets a loyalty card with its status
        /// </summary>
        /// <param name="loyaltyCardNumber">The loyalty card Id</param>
        /// <returns>A loyalty card object</returns>
        protected virtual CrtDataModel.LoyaltyCard GetLoyaltyCardStatusInternal(string loyaltyCardNumber)
        {
            LoyaltyManager loyaltyManager = LoyaltyManager.Create(CrtUtilities.GetCommerceRuntime());

            return loyaltyManager.GetLoyaltyCardStatus(loyaltyCardNumber);
        }

        public virtual LoyaltyCard GetLoyaltyCardStatus(string loyaltyCardNumber)
        {
            return LoyaltyMapper.ConvertToViewModel(GetLoyaltyCardStatusInternal(loyaltyCardNumber));
        }

        /// <summary>
        /// Gets a read only collection of all loyalty card with their status
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>
        /// Read only collection of all loyalty card with their status
        /// </returns>
        protected virtual ReadOnlyCollection<CrtDataModel.LoyaltyCard> GetAllLoyaltyCardsStatusInternal(string customerId)
        {
            ReadOnlyCollection<CrtDataModel.LoyaltyCard> allLoyaltyCards = GetLoyaltyCardsInternal(customerId);
            List<CrtDataModel.LoyaltyCard> allLoyaltyCardsStatus = new List<CrtDataModel.LoyaltyCard>();

            foreach (CrtDataModel.LoyaltyCard loyaltyCard in allLoyaltyCards)
            {
                allLoyaltyCardsStatus.Add(GetLoyaltyCardStatusInternal(loyaltyCard.CardNumber));
            }

            return allLoyaltyCardsStatus.AsReadOnly();
        }

        /// <summary>
        /// Gets a read only collection of all loyalty card with their status
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <returns>
        /// Read only collection of all loyalty card with their status
        /// </returns>
        public virtual ReadOnlyCollection<LoyaltyCard> GetAllLoyaltyCardsStatus(string customerId)
        {
            return LoyaltyMapper.ConvertToViewModel(GetAllLoyaltyCardsStatusInternal(customerId));
        }

        /// <summary>
        /// Gets all the transaction data specific to a a loyalty card number for a given points category
        /// </summary>
        /// <param name="loyaltyCardNumber">The loyalty card Id</param>
        /// <param name="rewardPointId">The reward points Id</param>
        /// <param name="topRows">Number of entries that should appear per page</param>
        /// <returns>A PagedResult object that includes all transactions</returns>
        protected virtual PagedResult<CrtDataModel.LoyaltyCardTransaction> GetLoyaltyCardTransactionsInternal(string loyaltyCardNumber, string rewardPointId, int topRows)
        {
            LoyaltyManager loyaltyManager = LoyaltyManager.Create(CrtUtilities.GetCommerceRuntime());
            CrtDataModel.QueryResultSettings settings = new CrtDataModel.QueryResultSettings();
            settings.Paging.Top = topRows;

            return loyaltyManager.GetLoyaltyCardTransactions(loyaltyCardNumber, rewardPointId, settings);
        }

        /// <summary>
        /// Gets all the transaction data specific to a a loyalty card number for a given points category
        /// </summary>
        /// <param name="loyaltyCardNumber">The loyalty card Id</param>
        /// <param name="rewardPointId">The reward points Id</param>
        /// <param name="topRows">Number of entries that should appear per page</param>
        /// <returns>A PagedResult object that includes all transactions</returns>
        public virtual ReadOnlyCollection<LoyaltyCardTransaction> GetLoyaltyCardTransactions(string loyaltyCardNumber, string rewardPointId, int topRows)
        {
            return LoyaltyMapper.ConvertToViewModel(GetLoyaltyCardTransactionsInternal(loyaltyCardNumber, rewardPointId, topRows));
        }

        /// <summary>
        /// Updates the loyalty card id.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart id.</param>
        /// <param name="loyaltyCardId">The loyalty card id.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// A shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when shoppingCartId is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">
        /// The loyalty card provided is blocked.
        /// or
        /// Invalid loyalty card number.
        /// or
        /// Shopping cart {0} of customer {1} was not found.
        /// </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1309:UseOrdinalStringComparison", MessageId = "System.String.Equals(System.String,System.StringComparison)")]
        public virtual ShoppingCart UpdateLoyaltyCardId(string shoppingCartId, string loyaltyCardId, string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            ShoppingCart shoppingCart = null;

            // Anonymous customers can only use anonymous loyalty cards
            // Check if an anonymous customer is using a loyalty card that belongs to authenticated customer
            if (string.IsNullOrEmpty(customerId) && !string.IsNullOrEmpty(loyaltyCardId))
            {
                CrtDataModel.LoyaltyCard loyaltyCard = GetLoyaltyCardStatusInternal(loyaltyCardId);
                if (!string.IsNullOrEmpty(loyaltyCard.CustomerAccount))
                {
                    throw new DataValidationException(LoyaltyWorkflowErrors.ConflictLoyaltyCardCustomerAndTransactionCustomer);
                }
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.Cart cart = null;

            try
            {
                cart = manager.UpdateCartLoyaltyCardId(shoppingCartId, customerId, loyaltyCardId, CrtDataModel.CalculationModes.All);
            }
            catch (CartValidationException ex)
            {
                if (ex.LineValidationResults[0][0].ErrorResourceId.Equals(DataValidationErrors.BlockedLoyaltyCard, StringComparison.InvariantCulture))
                {
                    throw new DataValidationException(DataValidationErrors.BlockedLoyaltyCard, "The loyalty card provided is blocked.", ex);
                }
                else if (ex.LineValidationResults[0][0].ErrorResourceId.Equals(DataValidationErrors.InvalidLoyaltyCardNumber, StringComparison.InvariantCulture))
                {
                    throw new DataValidationException(DataValidationErrors.InvalidLoyaltyCardNumber, "Invalid loyalty card number.", ex);
                }
                else if (ex.LineValidationResults[0][0].ErrorResourceId.Equals(LoyaltyWorkflowErrors.ConflictLoyaltyCardCustomerAndTransactionCustomer, StringComparison.InvariantCulture))
                {
                    throw new DataValidationException(LoyaltyWorkflowErrors.ConflictLoyaltyCardCustomerAndTransactionCustomer, string.Format("No loyalty card was found. CustomerId: {0}, LoyaltyCardId: {1} ShoppingCartId:{2}", customerId, loyaltyCardId, shoppingCartId), ex);
                }
                else throw;
            }

            if (cart != null && dataLevel == ShoppingCartDataLevel.All)
            {
                shoppingCart = this.ShoppingCartController.AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
            }
            else if (cart == null)
            {
                throw new DataValidationException(DataValidationErrors.CartNotFound, "Shopping cart {0} of customer {1} was not found.", shoppingCartId, customerId);
            }
            else
            {
                ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
            }

            return shoppingCart;
        }

        /// <summary>
        /// Gets an instance of ShoppingCartController.
        /// </summary>
        protected virtual ShoppingCartController ShoppingCartController
        {
            get
            {
                if (this.shoppingCartControllerValue == null)
                {
                    lock (syncRoot)
                    {
                        if (this.shoppingCartControllerValue == null)
                        {
                            this.shoppingCartControllerValue = new ShoppingCartController();
                            this.shoppingCartControllerValue.SetConfiguration(this.Configuration);
                        }
                    }
                }

                return this.shoppingCartControllerValue;
            }
        }
    }
}
