/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Controller for sales orders.
    /// </summary>
    public class OrderController : ControllerBase
    {
        /// <summary>
        /// Gets orders for the given customer account number.
        /// </summary>
        /// <param name="accountNumber">Account Number of the customer.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>Sales Orders for the customer.</returns>
        public virtual IEnumerable<SalesOrder> GetSalesOrdersByCustomer(string accountNumber, bool includeSalesLines, ISearchEngine searchEngine)
        {
            var manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            var settings = new CrtDataModel.QueryResultSettings();

            IEnumerable<CrtDataModel.SalesOrder> dmOrders = manager.GetOrdersByCustomer(settings, accountNumber);

            if (dmOrders == null || !dmOrders.Any())
            {
                return null;
            }

            return new OrderMapper(this.Configuration).ConvertToViewModel(dmOrders, includeSalesLines, searchEngine);
        }

        /// <summary>
        /// Gets the committed Sales order matching the salesId.
        /// </summary>
        /// <param name="salesId">Sales Id of the order to return.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>Sales order matching the sales id, if found; otherwise null.</returns>
        public virtual SalesOrder GetCommittedSalesOrder(string salesId, bool includeSalesLines, ISearchEngine searchEngine)
        {
            var manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.SalesOrder dmOrder = manager.GetOrder(salesId);

            if (dmOrder == null)
            {
                return null;
            }

            var orderMapper = new OrderMapper(this.Configuration);
            SalesOrder salesOrder = orderMapper.ConvertToViewModel(new List<CrtDataModel.SalesOrder> { dmOrder }, includeSalesLines, searchEngine).First();

            return salesOrder;
        }

        /// <summary>
        /// Gets the committed Sales order matching the confirmationId.
        /// </summary>
        /// <param name="confirmationId">Confirmation Id of the order to return.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>Sales order matching the sales id, if found; otherwise null.</returns>
        public virtual SalesOrder GetCommittedSalesOrderByConfirmationId(string confirmationId, bool includeSalesLines, ISearchEngine searchEngine)
        {
            var manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.SalesOrder dmOrder = manager.GetOrderByChannelReferenceId(confirmationId);

            if (dmOrder == null)
            {
                return null;
            }

            var orderMapper = new OrderMapper(this.Configuration);
            SalesOrder salesOrder = orderMapper.ConvertToViewModel(new List<CrtDataModel.SalesOrder> { dmOrder }, includeSalesLines, searchEngine).First();

            return salesOrder;
        }

        /// <summary>
        /// Get the Pending sales order matching the cart identifier.
        /// </summary>
        /// <param name="cartId">Cart identifier of the pending sales order to return.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <returns>Pending sales order matching the cart identifier, if found; otherwise null.</returns>
        public virtual SalesOrder GetPendingSalesOrder(string cartId, bool includeSalesLines, ISearchEngine searchEngine)
        {
            var manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

            CrtDataModel.SalesOrder dmOrder = manager.GetOrderByTransactionId(cartId);

            if (dmOrder == null)
            {
                return null;
            }

            var orderMapper = new OrderMapper(this.Configuration);
            SalesOrder salesOrder = orderMapper.ConvertToViewModel(new List<CrtDataModel.SalesOrder> { dmOrder }, includeSalesLines, searchEngine).First();

            return salesOrder;
        }
    }
}