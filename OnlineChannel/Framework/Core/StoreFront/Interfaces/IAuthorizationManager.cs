﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront
{
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    public interface IAuthorizationManager
    {
        /// <summary>
        /// Tries to get a customer map from a user id.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="userCustomerMap">The user customer mapping found.</param>
        /// <returns>Bool that indicates whether a mapping was found or not.</returns>
        bool TryReadCustomerMap(string userId, out UserCustomerMap userCustomerMap);

        /// <summary>
        /// Creates a pending user customer mapping.  This is a mapping that cannot be used yet, as it is in pending state.
        /// </summary>
        /// <param name="userId">The user Id.</param>
        /// <param name="customerId">The customer Id.</param>
        /// <param name="xmlData">The additional data.</param>
        /// <returns>A user customer map.</returns>
        UserCustomerMap CreatePendingUserCustomerMap(string userId, string customerId, string xmlData);

        /// <summary>
        /// Updates the user customer map.
        /// </summary>
        /// <param name="userCustomerMap">The user customer map.</param>
        void UpdateUserCustomerMap(UserCustomerMap userCustomerMap);

        /// <summary>
        /// Creates a user customer map.
        /// </summary>
        /// <param name="userCustomerMap">The map.</param>
        void CreateUserCustomerMap(UserCustomerMap userCustomerMap);

        /// <summary>
        /// Deletes a user customer map.
        /// </summary>
        /// <param name="userId">The user Id.</param>
        void DeleteUserCustomerMap(string userId);
    }
}
