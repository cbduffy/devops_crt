/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Abstracts search functionality.
    /// </summary>
    public interface ISearchEngine
    {
        /// <summary>
        /// Gets the requested properties for the given listing ids.
        /// </summary>
        /// <param name="isResultMandatory">if set to <c>true</c> then the method will fail if no satisfactory results are found.
        /// if set to <c>false</c> then the method will return a null if no satisfactory results are found.</param>
        /// <param name="listingRecordIds">AX Listing's record IDs.</param>
        /// <param name="propertyNames">The properties of the given listing ids that needs to be returned.</param>
        /// <returns>
        /// listingRecordId and properties of given listing as key value pairs. For instance: (12345, Path, /Sites/RetailPublishingPortal/12345/12345).
        /// </returns>
        /// <exception cref="System.ArgumentNullException">propertyNames</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This specific type is required here.")]
        IDictionary<long, IDictionary<string, object>> GetListingsProperties(bool isResultMandatory, IEnumerable<long> listingRecordIds, params string[] propertyNames);

        /// <summary>
        /// Gets the requested properties for the given listing id.
        /// </summary>
        /// <param name="isResultMandatory">if set to <c>true</c> then the method will fail if no satisfactory results are found.
        /// <returns>        /// if set to <c>false</c> then the method will return a null if no satisfactory results are found.</param>
        /// <param name="listingRecordId">AX Listing's record ID.</param>
        /// <param name="propertyNames">The properties of the given listing id that needs to be returned.</param>

        /// Properties of given listing as key value pairs. For instance: (Path, /Sites/RetailPublishingPortal/12345/67890).
        /// </returns>
        /// <exception cref="System.ArgumentNullException">propertyNames</exception>
        IDictionary<string, object> GetListingProperties(bool isResultMandatory, long listingRecordId, params string[] propertyNames);

        /// <summary>
        /// Gets the information of the kit variant that has the given products as its components.
        /// Only variants of the given kit master product are looked up for a match.
        /// </summary>
        /// <param name="kitMasterProductId">Product identifier of the kit master product.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="kitLines">The different kit lines that define a unique configuration of the kit.</param>
        /// <returns>Returns title, customer item number and product identifier of the kit variant.</returns>
        /// <exception cref="System.ArgumentNullException">kitLines</exception>
        Tuple<string, string, long, string> GetKitVariantProductInfo(long kitMasterProductId, long catalogId, IEnumerable<KitLineProductProperty> kitLines);

        /// <summary>
        /// Gets the applicable variants of a kit component.
        /// </summary>
        /// <param name="kitComponentProductId">Product identifier of the kit component.</param>
        /// <param name="kitComponentParentId">Product identifier of the kit component master.</param>
        /// <param name="parentKitId">Product identifier of the kit master product.</param>
        /// <param name="kitComponentLineId">The kit component line identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <returns>Returns a collection of kit component variants.</returns>
        IEnumerable<StorefrontListItem> GetKitComponentVariants(long kitComponentProductId, long kitComponentParentId, long parentKitId, long kitComponentLineId, long catalogId);

        /// <summary>
        /// Gets the product image data and converts the relative image url to absolute image url.
        /// </summary>
        /// <param name="imageSearchValue">The product image data.</param>
        /// <returns>The product image data with absolute image url.</returns>
        ImageInfo UpdateProductImageData(RichMediaLocations imageSearchValue);
        
        /// <summary>
        /// Represents the name of the search property that contains the item's number.
        /// </summary>
        string DescriptionSearchPropertyName
        {
            get;
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's path.
        /// </summary>
        string PathSearchPropertyName
        {
            get;
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's image.
        /// </summary>
        string ImageSearchPropertyName
        {
            get;
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's title.
        /// </summary>
        string TitleSearchPropertyName
        {
            get;
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's color.
        /// </summary>
        string ColorSearchPropertyName
        {
            get;
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's size.
        /// </summary>
        string SizeSearchPropertyName
        {
            get;
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's style.
        /// </summary>
        string StyleSearchPropertyName
        {
            get;
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's configuration.
        /// </summary>
        string ConfigurationSearchPropertyName
        {
            get;
        }
    }
}