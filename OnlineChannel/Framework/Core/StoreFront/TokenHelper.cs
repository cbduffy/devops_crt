/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core
{
    using System;
    using System.Security.Cryptography;
    using System.Text;
    //CS by muthait dated 26Nov2014 - changed internal to public
    /// <summary>
    /// Helper class for generating unique transaction ids.
    /// </summary>
    public static class TokenHelper
    {
        /// <summary>
        /// Generates a unique transaction id.
        /// </summary>
        /// <returns>A base64-encoded transaction id.</returns>
        public static string GenerateTransactionId()
        {
            byte[] transactionId = new byte[32];
            using (var rng = RNGCryptoServiceProvider.Create())
            {
                rng.GetBytes(transactionId);
            }

            return Convert.ToBase64String(transactionId);
        }

        /// <summary>
        /// Generates an alphanumeric user-friendly order number, according to the specified mask on the channel and the transaction identifier.
        /// </summary>
        /// <param name="orderConfirmationMask">The order confirmation mask.</param>
        /// <param name="emailAddress">The e-mail address.</param>
        /// <returns>The user-friendly order number.</returns>
        public static string GenerateOrderNumber(string orderConfirmationMask, string emailAddress)
        {
            if (string.IsNullOrWhiteSpace(orderConfirmationMask))
            {
                return string.Empty;
            }

            return FillMask(orderConfirmationMask, emailAddress ?? string.Empty);
        }

        private static string FillMask(string mask, string emailAddress)
        {
            StringBuilder orderNumber = new StringBuilder();
            int nextEmailAddressIndex = 0;

            Random random = new Random();

            for (int i = 0; i < mask.Length; i++)
            {
                char characterToAppend;

                switch (mask[i])
                {
                    case '#':
                        characterToAppend = GetReplacement(random);
                        break;
                    case '@':
                        characterToAppend = GetNextLetterOrDigit(emailAddress, ref nextEmailAddressIndex, random);
                        break;
                    default:
                        characterToAppend = mask[i];
                        break;
                }

                orderNumber.Append(char.ToUpperInvariant(characterToAppend));
            }

            return orderNumber.ToString();
        }

        private static char GetNextLetterOrDigit(string characterPool, ref int nextCharacterPoolIndex, Random random)
        {
            while (nextCharacterPoolIndex < characterPool.Length)
            {
                if (char.IsLetterOrDigit(characterPool[nextCharacterPoolIndex]))
                {
                    return characterPool[nextCharacterPoolIndex++];
                }

                nextCharacterPoolIndex++;
            }

            return GetReplacement(random);
        }

        private static char GetReplacement(Random random)
        {
            char replacement;
            int nextValue = random.Next(0, 35);

            if (nextValue < 26)
            {
                // letter
                replacement = (char)('a' + nextValue);
            }
            else
            {
                // digit
                replacement = (char)('0' + nextValue % 26);
            }

            return replacement;
        }
    }
}