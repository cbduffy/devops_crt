﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core
{
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Runtime.Serialization.Json;
    using System.Text;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    public static class Utilities
    {
        /// <summary>
        /// Represents the alternate text of the default product image.
        /// </summary>
        private const string DefaultProductImageAltText = "Product not available.";

        /// <summary>
        /// Represents the url of the default product image.
        /// </summary>
        private const string DefaultProductImageUrl = @"Products/Unavailable_Product_lrg.png";

        /// <summary>
        /// Caches the default image data for any product which is not found on the current channel.
        /// </summary>
        /// <remarks>This variable is not thread safe.</remarks>
        private static string DefaultProductImageData = string.Empty;

        /// <summary>
        /// Caches the service account identifier for the payment connector that will be used for authorizing payments.
        /// This field is used when the credit card token is being generated by an external entity.
        /// </summary>
        /// <remarks>This variable is not thread safe.</remarks>
        private static string PaymentConnectorServiceAccountId = string.Empty;

        /// <summary>
        /// Represents name of configuration app settings key which stores Currency Template
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Crt", Justification = "Crt is well known CommerceRunTime")]
        private const string KeyCrtConnectionString = "CommerceRuntimeConnectionString";

        /// <summary>
        /// Gets the supported languages for the channel.
        /// </summary>
        /// <returns>Read-only collection of the supported languages for the channel.</returns>
        public static ReadOnlyCollection<ChannelLanguage> GetChannelLanguages()
        {
            return CrtUtilities.GetChannelLanguages();
        }

        /// <summary>
        /// Represents name of configuration app settings key which stores the payment connector service account identifier.
        /// </summary>
        private const string PaymentConnectorServiceAccountIdPropertyName = "PaymentConnector_ServiceAccountId";

        /// <summary>
        /// Gets the currency symbol for the channel.
        /// </summary>
        /// <returns>The currency symbol as string.</returns>
        public static string GetChannelCurrencySymbol()
        {
            return CrtUtilities.GetChannelCurrencySymbol();
        }

        /// <summary>
        /// Formats decimal value to represent currency.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <returns>Amount as a currency string.</returns>
        public static string ToCurrencyString(this decimal amount)
        {
            return ToCurrencyString(amount, CultureInfo.InvariantCulture.Name);
        }

        /// <summary>
        /// Gets the payment connector service account identifier.
        /// </summary>
        /// <returns>the payment connector service account identifier.</returns>
        public static string GetPaymentConnectorServiceAccountId()
        {
            if (string.IsNullOrWhiteSpace(PaymentConnectorServiceAccountId))
            {
                PaymentConnectorServiceAccountId = ConfigurationManager.AppSettings[PaymentConnectorServiceAccountIdPropertyName];
            }

            return PaymentConnectorServiceAccountId;
        }

        /// <summary>
        /// Formats decimal value to represent currency based on the culture.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="cultureName">The name of the culture.</param>
        /// <returns>Amount as currency string.</returns>
        public static string ToCurrencyString(this decimal amount, string cultureName)
        {
            string currencyStringTemplate = Utilities.GetChannelCurrencyStringTemplate(cultureName);

            CultureInfo cultureInfo = (!string.IsNullOrWhiteSpace(cultureName)) ? new CultureInfo(cultureName) : CultureInfo.InvariantCulture;
            string amountWithoutSymbol = amount.ToString("N2", cultureInfo);

            string returnedAmount = string.Format(currencyStringTemplate, amountWithoutSymbol);

            return returnedAmount;
        }

        /// <summary>
        /// Converts Enrichment to JSON format.
        /// </summary>
        /// <param name="enrichments">Original enrichment passed from AX.</param>
        /// <returns>JSON which was created from the enrichment.</returns>
        public static string ConvertRichMediaToJson(RichMediaLocations enrichments)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(RichMediaLocations));
                ser.WriteObject(ms, enrichments);

                return Encoding.Default.GetString(ms.ToArray());
            }
        }

        /// <summary>
        /// Gets the data for the default product image.
        /// </summary>
        /// <returns>Default product image data.</returns>
        public static string GetDefaultProductImageData()
        {
            if (string.IsNullOrEmpty(Utilities.DefaultProductImageData))
            {
                RichMediaLocationsRichMediaLocation medialocation = new RichMediaLocationsRichMediaLocation();
                medialocation.AltText = DefaultProductImageAltText;
                medialocation.Url = DefaultProductImageUrl;

                RichMediaLocations richMediaLocations = new RichMediaLocations();
                richMediaLocations.Items = new RichMediaLocationsRichMediaLocation[] { medialocation };

                Utilities.DefaultProductImageData = Utilities.ConvertRichMediaToJson(richMediaLocations);
            }

            return Utilities.DefaultProductImageData;
        }

        /// <summary>
        /// Gets the channel currency string template.
        /// </summary>
        /// <param name="cultureName">Name of the culture.</param>
        /// <returns>The template for channel currency string.</returns>
        public static string GetChannelCurrencyStringTemplate(string cultureName)
        {
            CultureInfo cultureInfo = (!string.IsNullOrWhiteSpace(cultureName)) ? new CultureInfo(cultureName) : CultureInfo.InvariantCulture;
            string currencySymbol = CrtUtilities.GetChannelCurrencySymbol();

            NumberFormatInfo nfi = cultureInfo.NumberFormat;
            bool symbolToTheRight = (nfi.CurrencyPositivePattern % 2 == 0) ? false : true;

            string currencyTemplate = symbolToTheRight ? "{0}" + currencySymbol : currencySymbol + "{0}";

            return currencyTemplate;
        }

        /// <summary>
        /// Represent the url of an inactive link.
        /// </summary>
        public static readonly string InactiveLinkUrl = "#";
    }
}
