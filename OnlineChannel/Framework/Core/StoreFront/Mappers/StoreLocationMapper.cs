﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Helper class for mapping StoreProductAvailability related objects.
    /// </summary>
    public static class StoreLocationMapper
    {
        /// <summary>
        /// Map <see cref="CrtDataModel.OrgUnitLocation"/> collection to <see cref="StoreLocation"/> collection.
        /// </summary>
        /// <param name="data">Enumerable list of <see cref="CrtDataModel.OrgUnitLocation"/>.</param>
        /// <returns>Enumerable list of <see cref="StoreLocation"/>.</returns>
        internal static Collection<StoreLocation> ConvertToViewModel(IEnumerable<CrtDataModel.OrgUnitLocation> data)
        {
            Collection<StoreLocation> view = null;

            if (data == null)
            {
                view = new Collection<StoreLocation>();

                foreach (CrtDataModel.OrgUnitLocation dataItem in data)
                {
                    view.Add(ConvertToViewModel(dataItem));
                }
            }

            return view;
        }

        /// <summary>
        /// Map <see cref="CrtDataModel.OrgUnitLocation"/> to <see cref="StoreLocation"/>.
        /// </summary>
        /// <param name="dataItem">Data item.</param>
        /// <returns>Instance of the <see cref="StoreLocation"/></returns>
        internal static StoreLocation ConvertToViewModel(CrtDataModel.OrgUnitLocation dataItem)
        {
            StoreLocation viewItem = null;

            if (dataItem != null)
            {
                viewItem = new StoreLocation();
                viewItem.ChannelId = dataItem.ChannelId;
                viewItem.City = dataItem.City;
                viewItem.Country = dataItem.Country;
                viewItem.County = dataItem.County;
                viewItem.Distance = string.Format(CultureInfo.InvariantCulture, "{0:N2}", dataItem.Distance);
                viewItem.InventoryLocationId = dataItem.InventoryLocationId;
                viewItem.Latitude = dataItem.Latitude;
                viewItem.Longitude = dataItem.Longitude;
                viewItem.State = dataItem.State;
                viewItem.StoreName = dataItem.OrgUnitName;
                viewItem.StoreId = dataItem.OrgUnitNumber;
                viewItem.Street = dataItem.Street;
                viewItem.ZipCode = dataItem.Zip;
                viewItem.PostalAddressId = dataItem.PostalAddressId;
            }

            return viewItem;
        }
    }
}