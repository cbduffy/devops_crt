﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Map DataModel address to ViewModel address.
    /// </summary>
    public static class LoyaltyMapper
    {
        internal static LoyaltyCard ConvertToViewModel(CrtDataModel.LoyaltyCard loyaltyCard)
        {
            if (loyaltyCard == null)
            {
                return null;
            }

            LoyaltyCard vmLoyaltyCard = new LoyaltyCard()
            {
                CardNumber = loyaltyCard.CardNumber,
                CardTenderType = (LoyaltyCardTenderType)loyaltyCard.CardTenderType,
                CustomerAccount = loyaltyCard.CustomerAccount,
                LoyaltyGroups = ConvertToViewModel(loyaltyCard.LoyaltyGroups),
                PartyRecordId = loyaltyCard.PartyRecordId,
                RecordId = loyaltyCard.RecordId,
                RewardPoints = ConvertToViewModel(loyaltyCard.RewardPoints),
            };

            return vmLoyaltyCard;
        }

        internal static ReadOnlyCollection<LoyaltyCard> ConvertToViewModel(ReadOnlyCollection<CrtDataModel.LoyaltyCard> loyaltyCardCollection)
        {
            List<LoyaltyCard> vmLoyaltyCards = new List<LoyaltyCard>();

            foreach (CrtDataModel.LoyaltyCard loyaltyCard in loyaltyCardCollection)
            {
                vmLoyaltyCards.Add(ConvertToViewModel(loyaltyCard));
            }

            return vmLoyaltyCards.AsReadOnly();
        }

        internal static LoyaltyGroup ConvertToViewModel(CrtDataModel.LoyaltyGroup loyaltyGroup)
        {
            if (loyaltyGroup == null)
            {
                return null;
            }

            LoyaltyGroup vmLoyaltyGroup = new LoyaltyGroup()
            {
                Description = loyaltyGroup.Description,
                LoyaltyCardTiers = ConvertToViewModel(loyaltyGroup.LoyaltyCardTiers),
                LoyaltyTiers = ConvertToViewModel(loyaltyGroup.LoyaltyTiers),
                Name = loyaltyGroup.Name,
                RecordId = loyaltyGroup.RecordId
            };

            return vmLoyaltyGroup;
        }

        internal static Collection<LoyaltyGroup> ConvertToViewModel(IList<CrtDataModel.LoyaltyGroup> loyaltyGroupCollection)
        {
            if (loyaltyGroupCollection == null)
            {
                throw new ArgumentNullException("loyaltyGroupCollection");
            }

            Collection<LoyaltyGroup> vmLoyaltyGroupCollection = new Collection<LoyaltyGroup>();

            foreach (CrtDataModel.LoyaltyGroup loyaltyGroup in loyaltyGroupCollection)
            {
                vmLoyaltyGroupCollection.Add(ConvertToViewModel(loyaltyGroup));
            }

            return vmLoyaltyGroupCollection;
        }

        internal static LoyaltyCardTier ConvertToViewModel(CrtDataModel.LoyaltyCardTier loyaltyCardTier)
        {
            if (loyaltyCardTier == null)
            {
                throw new ArgumentNullException("loyaltyCardTier");
            }

            string validFromDateFriendly = String.Format(CultureInfo.InvariantCulture, "{0:d}", loyaltyCardTier.ValidFrom);
            string validToDateFriendly = String.Format(CultureInfo.InvariantCulture, "{0:d}", loyaltyCardTier.ValidTo);

            LoyaltyCardTier vmLoyaltyCardTier = new LoyaltyCardTier()
            {
                LoyaltyTierRecordId = loyaltyCardTier.LoyaltyTierRecordId,
                RecordId = loyaltyCardTier.RecordId,
                TierId = loyaltyCardTier.TierId,
                ValidFromFriendly = validFromDateFriendly,
                ValidToFriendly = validToDateFriendly
            };

            return vmLoyaltyCardTier;
        }

        internal static Collection<LoyaltyCardTier> ConvertToViewModel(IList<CrtDataModel.LoyaltyCardTier> loyaltyCardTierCollection)
        {
            if (loyaltyCardTierCollection == null)
            {
                throw new ArgumentNullException("loyaltyCardTierCollection");
            }

            Collection<LoyaltyCardTier> vmLoyaltyCardTierCollection = new Collection<LoyaltyCardTier>();

            foreach (CrtDataModel.LoyaltyCardTier loyaltyCardTier in loyaltyCardTierCollection)
            {
                vmLoyaltyCardTierCollection.Add(ConvertToViewModel(loyaltyCardTier));
            }

            return vmLoyaltyCardTierCollection;
        }

        internal static LoyaltyTier ConvertToViewModel(CrtDataModel.LoyaltyTier loyaltyTier)
        {
            if (loyaltyTier == null)
            {
                throw new ArgumentNullException("loyaltyTier");
            }

            LoyaltyTier vmLoyaltyTier = new LoyaltyTier()
            {
                Description = loyaltyTier.Description,
                RecordId = loyaltyTier.RecordId,
                TierId = loyaltyTier.TierId,
                TierLevel = loyaltyTier.TierLevel
            };

            return vmLoyaltyTier;
        }

        internal static Collection<LoyaltyTier> ConvertToViewModel(IList<CrtDataModel.LoyaltyTier> loyaltyTierCollection)
        {
            if (loyaltyTierCollection == null)
            {
                throw new ArgumentNullException("loyaltyTierCollection");
            }

            Collection<LoyaltyTier> vmLoyaltyTierCollection = new Collection<LoyaltyTier>();

            foreach (CrtDataModel.LoyaltyTier loyaltyTier in loyaltyTierCollection)
            {
                vmLoyaltyTierCollection.Add(ConvertToViewModel(loyaltyTier));
            }

            return vmLoyaltyTierCollection;
        }

        internal static LoyaltyRewardPoint ConvertToViewModel(CrtDataModel.LoyaltyRewardPoint loyaltyRewardPoint)
        {
            if (loyaltyRewardPoint == null)
            {
                throw new ArgumentNullException("loyaltyRewardPoint");
            }

            LoyaltyRewardPoint vmloyaltyRewardPoint = new LoyaltyRewardPoint()
            {
                ActivePoints = loyaltyRewardPoint.ActivePoints,
                Description = loyaltyRewardPoint.Description,
                ExpirationTimeValue = loyaltyRewardPoint.ExpirationTimeValue,
                ExpiredPoints = loyaltyRewardPoint.ExpiredPoints,
                IsRedeemable = loyaltyRewardPoint.IsRedeemable,
                IssuedPoints = loyaltyRewardPoint.IssuedPoints,
                RecordId = loyaltyRewardPoint.RecordId,
                RedeemRanking = loyaltyRewardPoint.RedeemRanking,
                RewardPointCurrency = loyaltyRewardPoint.RewardPointCurrency,
                RewardPointId = loyaltyRewardPoint.RewardPointId,
                RewardPointType = (LoyaltyRewardPointType)loyaltyRewardPoint.RewardPointType,
                UsedPoints = loyaltyRewardPoint.UsedPoints
            };

            return vmloyaltyRewardPoint;
        }

        internal static IList<LoyaltyRewardPoint> ConvertToViewModel(IList<CrtDataModel.LoyaltyRewardPoint> loyaltyRewardPointCollection)
        {
            if (loyaltyRewardPointCollection == null)
            {
                throw new ArgumentNullException("loyaltyRewardPointCollection");
            }

            Collection<LoyaltyRewardPoint> vmLoyaltyRewardPointCollection = new Collection<LoyaltyRewardPoint>();

            foreach (CrtDataModel.LoyaltyRewardPoint loyaltyRewardPoint in loyaltyRewardPointCollection)
            {
                if (loyaltyRewardPoint != null)
                {
                    vmLoyaltyRewardPointCollection.Add(ConvertToViewModel(loyaltyRewardPoint));
                }
            }

            return vmLoyaltyRewardPointCollection;
        }

        internal static LoyaltyCardTransaction ConvertToViewModel(CrtDataModel.LoyaltyCardTransaction loyaltyCardTransaction)
        {
            if (loyaltyCardTransaction == null)
            {
                throw new ArgumentNullException("loyaltyCardTransaction");
            }

            ChannelManager channelManager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());
            string timeZoneId = channelManager.GetChannelConfiguration().TimeZoneInfoId;
            DateTime entryDate, expirationDateFriendly;

            // If a time zone id is obtained for the channel then convert the date from utc to channel time zone else keep it in utc.
            if (!string.IsNullOrWhiteSpace(timeZoneId))
            {
                TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                entryDate = TimeZoneInfo.ConvertTimeFromUtc(loyaltyCardTransaction.EntryDateTime, timeZoneInfo);
                expirationDateFriendly = TimeZoneInfo.ConvertTimeFromUtc(loyaltyCardTransaction.ExpirationDate, timeZoneInfo);
            }
            else
            {
                entryDate = loyaltyCardTransaction.EntryDateTime;
                expirationDateFriendly = loyaltyCardTransaction.ExpirationDate;
            }

            LoyaltyCardTransaction vmLoyaltyCardTransaction = new LoyaltyCardTransaction()
            {
                ChannelName = loyaltyCardTransaction.ChannelName,
                EntryDate = entryDate.ToString(),
                EntryTime = loyaltyCardTransaction.EntryDateTime.ToShortTimeString(),
                EntryTypeFriendly = loyaltyCardTransaction.EntryType.ToString(),
                ExpirationDateFriendly = expirationDateFriendly.ToString(),
                RewardPointAmountQuantity = loyaltyCardTransaction.RewardPointAmountQuantity,
                TransactionId = loyaltyCardTransaction.TransactionId
            };

            return vmLoyaltyCardTransaction;
        }

        internal static ReadOnlyCollection<LoyaltyCardTransaction> ConvertToViewModel(ReadOnlyCollection<CrtDataModel.LoyaltyCardTransaction> loyaltyCardTransactions)
        {
            List<LoyaltyCardTransaction> vmLoyaltyCardTransactions = new List<LoyaltyCardTransaction>();

            foreach (CrtDataModel.LoyaltyCardTransaction loyaltyCardTransaction in loyaltyCardTransactions)
            {
                vmLoyaltyCardTransactions.Add(ConvertToViewModel(loyaltyCardTransaction));
            }

            return vmLoyaltyCardTransactions.AsReadOnly();
        }

        internal static ReadOnlyCollection<LoyaltyCardTransaction> ConvertToViewModel(PagedResult<CrtDataModel.LoyaltyCardTransaction> pagedResult)
        {
            ReadOnlyCollection<CrtDataModel.LoyaltyCardTransaction> transactions = pagedResult.Results;

            return ConvertToViewModel(transactions);
        }
    }
}