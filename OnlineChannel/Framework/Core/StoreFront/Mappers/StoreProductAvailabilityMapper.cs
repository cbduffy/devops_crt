/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Helper class for mapping StoreProductAvailability related objects.
    /// </summary>
    public static class StoreProductAvailabilityMapper
    {
        /// <summary>
        /// Map <see cref="CrtDataModel.OrgUnitAvailability"/> collection to <see cref="StoreProductAvailability"/> collection.
        /// </summary>
        /// <param name="data">Enumerable list of <see cref="CrtDataModel.OrgUnitAvailability"/>.</param>
        /// <param name="shoppingCartItems">Enumerable list of <see cref="TransactionItem"/>.</param>
        /// <returns>Enumerable list of <see cref="StoreProductAvailability"/>.</returns>
        internal static Collection<StoreProductAvailability> ConvertToViewModel(IEnumerable<CrtDataModel.OrgUnitAvailability> data, IEnumerable<TransactionItem> shoppingCartItems)
        {
            Collection<StoreProductAvailability> view = null;

            if (data == null)
            {
                view = new Collection<StoreProductAvailability>();

                foreach (CrtDataModel.OrgUnitAvailability dataItem in data)
                {
                    view.Add(ConvertToViewModel(dataItem, shoppingCartItems));
                }
            }

            return view;
        }

        /// <summary>
        /// Map <see cref="CrtDataModel.OrgUnitAvailability" /> to <see cref="StoreProductAvailability" />.
        /// </summary>
        /// <param name="dataItem">Data item.</param>
        /// <param name="shoppingCartItems">Enumerable list of <see cref="TransactionItem" />.</param>
        /// <returns>
        /// Instance of the <see cref="StoreProductAvailability" />
        /// </returns>
        internal static StoreProductAvailability ConvertToViewModel(CrtDataModel.OrgUnitAvailability dataItem, IEnumerable<TransactionItem> shoppingCartItems)
        {
            StoreProductAvailability viewItem = null;

            if (dataItem != null)
            {
                viewItem = new StoreProductAvailability();
                viewItem.ChannelId = dataItem.OrgUnitLocation.ChannelId;
                viewItem.City = dataItem.OrgUnitLocation.City;
                viewItem.Country = dataItem.OrgUnitLocation.Country;
                viewItem.County = dataItem.OrgUnitLocation.County;
                viewItem.Distance = string.Format(CultureInfo.InvariantCulture, "{0:N2}", dataItem.OrgUnitLocation.Distance);
                viewItem.InventoryLocationId = dataItem.OrgUnitLocation.InventoryLocationId;
                viewItem.Latitude = dataItem.OrgUnitLocation.Latitude;
                viewItem.Longitude = dataItem.OrgUnitLocation.Longitude;
                viewItem.State = dataItem.OrgUnitLocation.State;
                viewItem.StoreName = dataItem.OrgUnitLocation.OrgUnitName;
                viewItem.StoreId = dataItem.OrgUnitLocation.OrgUnitNumber;
                viewItem.Street = dataItem.OrgUnitLocation.Street;
                viewItem.ZipCode = dataItem.OrgUnitLocation.Zip;
                viewItem.PostalAddressId = dataItem.OrgUnitLocation.PostalAddressId;

                // Right now view supports one product at a time.
                foreach (CrtDataModel.ItemAvailability item in dataItem.ItemAvailabilities)
                {
                    viewItem.ProductAvailabilities.Add(ConvertToViewModel(item, shoppingCartItems));
                }
            }

            return viewItem;
        }


        /// <summary>
        /// Map <see cref="CrtDataModel.ItemAvailability" /> collection to <see cref="StoreProductAvailabilityItem" /> collection.
        /// </summary>
        /// <param name="data">Enumerable list of <see cref="CrtDataModel.ItemAvailability" />.</param>
        /// <param name="shoppingCartItems">Enumerable list of <see cref="TransactionItem" />.</param>
        /// <returns>
        /// Instance of the <see cref="StoreProductAvailabilityItem" />
        /// </returns>
        internal static Collection<StoreProductAvailabilityItem> ConvertToViewModel(IEnumerable<CrtDataModel.ItemAvailability> data, IEnumerable<TransactionItem> shoppingCartItems)
        {
            Collection<StoreProductAvailabilityItem> view = null;

            if (data == null)
            {
                view = new Collection<StoreProductAvailabilityItem>();

                foreach (CrtDataModel.ItemAvailability dataItem in data)
                {
                    view.Add(ConvertToViewModel(dataItem, shoppingCartItems));
                }
            }

            return view;
        }

        /// <summary>
        /// Map <see cref="CrtDataModel.ItemAvailability" /> to <see cref="StoreProductAvailabilityItem" />.
        /// </summary>
        /// <param name="dataItem">Data item.</param>
        /// <param name="shoppingCartItems">Enumerable list of <see cref="TransactionItem" />.</param>
        /// <returns>
        /// Instance of the <see cref="StoreProductAvailabilityItem" />
        /// </returns>
        internal static StoreProductAvailabilityItem ConvertToViewModel(CrtDataModel.ItemAvailability dataItem, IEnumerable<TransactionItem> shoppingCartItems)
        {
            StoreProductAvailabilityItem viewItem = null;

            if (dataItem != null)
            {
                viewItem = new StoreProductAvailabilityItem();

                // Map shoppingCartItem property to view property.
                if (shoppingCartItems != null && shoppingCartItems.Any())
                {
                    TransactionItem shoppingCartItem = shoppingCartItems.FirstOrDefault(item => item.ItemId == dataItem.ItemId && item.VariantInventoryDimensionId == dataItem.VariantInventoryDimensionId);
                    viewItem.ProductDetails = shoppingCartItem.ProductDetails;
                }

                viewItem.RecordId = dataItem.RecordId;
                viewItem.ItemId = dataItem.ItemId;
                viewItem.VariantInventoryDimensionId = dataItem.VariantInventoryDimensionId;
                viewItem.InventoryLocationId = dataItem.InventoryLocationId;
                viewItem.AvailableQuantity = dataItem.AvailableQuantity;
            }

            return viewItem;
        }

        /// <summary>
        /// Maps CRT data model ItemAvailability to view model ListingAvailableQuantity.
        /// </summary>
        /// <param name="itemAvailability">CRT data model ItemAvailability.</param>
        /// <returns>View model ListingAvailableQuantity.</returns>
        internal static ListingAvailableQuantity ConvertToViewModel(CrtDataModel.ProductAvailableQuantity itemAvailability)
        {
            ListingAvailableQuantity result = null;
            if (itemAvailability != null)
            {
                result = new ListingAvailableQuantity();
                result.ListingId = itemAvailability.ProductId;
                result.UnitOfMeasure = itemAvailability.UnitOfMeasure;
                result.AvailableQuantity = itemAvailability.AvailableQuantity;
            }

            return result;
        }

        /// <summary>
        /// Maps a collection of CRT data model ItemAvailability to view model ListingAvailableQuantity.
        /// </summary>
        /// <param name="itemAvailablities">Collection of CRT data model ItemAvailability.</param>
        /// <returns>Collection of view model ListingAvailableQuantity.</returns>
        internal static IEnumerable<ListingAvailableQuantity> ConvertToViewModel(IEnumerable<CrtDataModel.ProductAvailableQuantity> itemAvailablities)
        {
            List<ListingAvailableQuantity> resultCollection = new List<ListingAvailableQuantity>();

            if (itemAvailablities != null)
            {
                foreach (var listingAvailableQuantity in itemAvailablities)
                {
                    var resultItem = StoreProductAvailabilityMapper.ConvertToViewModel(listingAvailableQuantity);
                    if (resultItem != null)
                    {
                        resultCollection.Add(resultItem);
                    }
                }
            }

            return resultCollection;
        }
    }
}