/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
    
    /// <summary>
    /// Map checkout objects between View Model and Data Model. 
    /// </summary>
    public static class CheckoutMapper
    {
        /// <summary>
        /// Convert View Model giftcard information.
        /// </summary>
        /// <param name="giftCard">The Datamodel gift card to convert.</param>
        /// <returns>A view model gift card.</returns>
        internal static GiftCardInformation ConvertToViewModel(CrtDataModel.GiftCard giftCard)
        {
            if (giftCard == null)
            {
                throw new ArgumentNullException("giftCard");
            }

            return new GiftCardInformation()
            {
                GiftCardId = giftCard.Id,
                Balance = giftCard.Balance,
                BalanceWithCurrency = Utilities.ToCurrencyString(giftCard.Balance),
                CurrencyCode = giftCard.BalanceCurrencyCode
            };
        }

        internal static LineDeliveryPreference ConvertToViewModel(CrtDataModel.LineDeliveryPreference lineDeliveryPreference)
        {
            LineDeliveryPreference salesLineDeliveryPreference = new LineDeliveryPreference() 
            { 
                LineId = lineDeliveryPreference.LineId ,
            };


            salesLineDeliveryPreference.DeliveryPreferenceTypes = CheckoutMapper.ConvertToViewModel(lineDeliveryPreference.DeliveryPreferenceTypes);

            return salesLineDeliveryPreference;
        }

        internal static IEnumerable<LineDeliveryPreference> ConvertToViewModel(IEnumerable<CrtDataModel.LineDeliveryPreference> lineDeliveryPreferences)
        {
            List<LineDeliveryPreference> salesLineDeliveryPreferences = new List<LineDeliveryPreference>();

            foreach ( var dataModelLineDeliveryPreference in lineDeliveryPreferences)
            {
                salesLineDeliveryPreferences.Add(CheckoutMapper.ConvertToViewModel(dataModelLineDeliveryPreference));
            }

            return salesLineDeliveryPreferences;
        }

        internal static CartDeliveryPreferences ConvertToViewModel(CrtDataModel.CartDeliveryPreferences dataModelCartDeliveryPreference)
        {
            CartDeliveryPreferences cartDeliveryPreferences = new CartDeliveryPreferences();
            
            cartDeliveryPreferences.HeaderDeliveryPreferenceTypes = CheckoutMapper.ConvertToViewModel(dataModelCartDeliveryPreference.HeaderDeliveryPreferenceTypes);
            cartDeliveryPreferences.LineDeliveryPreferences = CheckoutMapper.ConvertToViewModel(dataModelCartDeliveryPreference.LineDeliveryPreferences);

            return cartDeliveryPreferences;

        }

        internal static IEnumerable<DeliveryPreferenceType> ConvertToViewModel(IEnumerable<CrtDataModel.DeliveryPreferenceType> deliveryPreferenceTypes)
        {
            List<DeliveryPreferenceType> deliveryPreferences = new List<DeliveryPreferenceType>();

            foreach (var dataModeldeliveryPreferenceType in deliveryPreferenceTypes)
            {
                deliveryPreferences.Add((DeliveryPreferenceType)dataModeldeliveryPreferenceType);
            }

            return deliveryPreferences;
        }
    }
}