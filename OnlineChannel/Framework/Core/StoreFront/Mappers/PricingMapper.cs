/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    // CS by muthait dated 26Nov2014 - changed internal to public
    /// <summary>
    /// Performs mappings between the Pricing view model and Commerce Runtime Sdk entities.
    /// </summary>
    public class PricingMapper
    {
        private IConfiguration configurationValue;

        public PricingMapper(IConfiguration configuration)
        {
            this.configurationValue = configuration;
        }

        /// <summary>
        /// Maps data model ProductPrice entity to ViewModel ListingPrice entity.
        /// </summary>
        /// <param name="productPrice">The product price.</param>
        /// <returns>
        /// View model representation of listing price.
        /// </returns>
        public ListingPrice ConvertToViewModel(ProductPrice productPrice)
        {
            ListingPrice listingPriceViewModel = null;

            if (productPrice != null)
            {
                listingPriceViewModel = new ListingPrice();
                listingPriceViewModel.ListingId = productPrice.ListingId;
                listingPriceViewModel.ItemId = productPrice.ItemId;
                listingPriceViewModel.InventoryDimensionId = productPrice.ItemId;
                listingPriceViewModel.BasePriceWithCurrency = productPrice.BasePrice.ToCurrencyString(configurationValue.GetCurrentCulture());
                listingPriceViewModel.BasePrice = productPrice.BasePrice;
                listingPriceViewModel.TradeAgreementPriceWithCurrency = productPrice.TradeAgreementPrice.ToCurrencyString(configurationValue.GetCurrentCulture());
                listingPriceViewModel.TradeAgreementPrice = productPrice.TradeAgreementPrice;
                listingPriceViewModel.AdjustedPriceWithCurrency = productPrice.AdjustedPrice.ToCurrencyString(configurationValue.GetCurrentCulture());
                listingPriceViewModel.AdjustedPrice = productPrice.AdjustedPrice;
                listingPriceViewModel.CurrencyCode = productPrice.CurrencyCode;
                listingPriceViewModel.UnitOfMeasure = productPrice.UnitOfMeasure;
                listingPriceViewModel.ActiveDate = productPrice.ValidFrom.DateTime;
                listingPriceViewModel.DiscountedPrice = (productPrice.AdjustedPrice - productPrice.DiscountAmount);
                listingPriceViewModel.DiscountedPriceWithCurrency = (productPrice.AdjustedPrice - productPrice.DiscountAmount).ToCurrencyString(configurationValue.GetCurrentCulture());
            }

            return listingPriceViewModel;
        }

        /// <summary>
        /// Converts Data Model ProductPrice collection to View Model ListingPrice collection.
        /// </summary>
        /// <param name="productPrices">Data model Listing Prices</param>
        /// <returns>View Model Listing Prices</returns>
        public Collection<ListingPrice> ConvertToViewModel(IEnumerable<ProductPrice> productPrices)
        {
            if (productPrices == null)
            {
                return null;
            }

            return new Collection<ListingPrice>(productPrices.Select(i => ConvertToViewModel(i)).ToList());
        }
    }
}