/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    // CS by muthait - 26Nov2014 - RTM Upgrade , internal swapped to public in all methods
    /// <summary>
    /// Performs mappings between the Shopping Cart view model and Commerce Runtime Sdk entities.
    /// </summary>
    public class ShoppingCartMapper
    {
        private const string ItemDescriptionProperty = "ITEMDESCRIPTION";
        private const string ItemNameProperty = "ITEMNAME";

        private IConfiguration configurationValue;

        public ShoppingCartMapper(IConfiguration configuration)
        {
            this.configurationValue = configuration;
        }

        
        /// <summary>
        /// Converts from CRT Cart to view model ShoppingCart.
        /// </summary>
        /// <param name="shoppingCart">The CRT shopping cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <param name="productValidator">The product validator.</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <returns>A view model Shopping Cart.</returns>
        public ShoppingCart ConvertToViewModel(CrtDataModel.Cart shoppingCart, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (shoppingCart == null)
            {
                return null;
            }

            CrtDataModel.Customer dmCustomer = null;
            if (!string.IsNullOrWhiteSpace(shoppingCart.CustomerId))
            {
                var customerManager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
                dmCustomer = customerManager.GetCustomer(shoppingCart.CustomerId);
            }

            ShoppingCart cartViewModel = new ShoppingCart();
            cartViewModel.CartId = shoppingCart.Id.ToString();
            cartViewModel.TotalAmount = shoppingCart.TotalAmount;
            cartViewModel.TotalAmountWithCurrency = shoppingCart.TotalAmount.ToCurrencyString(configurationValue.GetCurrentCulture());
            cartViewModel.LastModifiedDate = shoppingCart.ModifiedDateTime.HasValue ? shoppingCart.ModifiedDateTime.Value.DateTime : DateTime.UtcNow;

            if (dataLevel > ShoppingCartDataLevel.Minimal)
            {
                cartViewModel.DiscountAmountWithCurrency = shoppingCart.DiscountAmount.ToCurrencyString(configurationValue.GetCurrentCulture());
                cartViewModel.DiscountAmount = shoppingCart.DiscountAmount;
                cartViewModel.ChargeAmountWithCurrency = shoppingCart.ChargeAmount.ToCurrencyString(configurationValue.GetCurrentCulture());
                cartViewModel.SubtotalWithCurrency = shoppingCart.SubtotalAmount.ToCurrencyString(configurationValue.GetCurrentCulture());
                cartViewModel.DeliveryModeId = shoppingCart.DeliveryMode;
                cartViewModel.ShippingAddress = AddressMapper.ConvertToViewModel(shoppingCart.ShippingAddress);
                cartViewModel.CartType = (CartType)shoppingCart.CartType;
                cartViewModel.Name = shoppingCart.Name;
                cartViewModel.LoyaltyCardId = shoppingCart.LoyaltyCardId;
                cartViewModel.TaxAmountWithCurrency = shoppingCart.TaxAmount.ToCurrencyString(configurationValue.GetCurrentCulture());
                if (shoppingCart.DiscountCodes != null)
                {
                    foreach (var discountCode in shoppingCart.DiscountCodes)
                    {
                        cartViewModel.DiscountCodes.Add(discountCode);
                    }
                }
            }

            if (dataLevel == ShoppingCartDataLevel.All)
            {
                foreach (var promotionLine in shoppingCart.PromotionLines)
                {
                    cartViewModel.PromotionLines.Add(promotionLine);
                }
            }

            var items = ConvertToViewModel(shoppingCart.CartLines, dataLevel, productValidator, searchEngine);
            if (items != null)
            {
                foreach (var item in items)
                {
                    cartViewModel.Items.Add(item);
                }
            }

            return cartViewModel;
        }


        /// <summary>
        /// Converts CRT DeliveryOption to DeliveryOption view model.
        /// </summary>
        /// <param name="deliveryOptions">The CRT delivery options.</param>
        /// <returns>The delivery methods view model.</returns>
        public Collection<DeliveryOption> ConvertToViewModel(IEnumerable<CrtDataModel.DeliveryOption> deliveryOptions)
        {
            Collection<DeliveryOption> Options = new Collection<DeliveryOption>();
            foreach (CrtDataModel.DeliveryOption deliveryOption in deliveryOptions)
            {
                Options.Add(new ShoppingCartMapper(this.configurationValue).ConvertToViewModel(deliveryOption));
            }
            return Options;
        }

        /// <summary>
        /// Converts CRT DeliveryOption to DeliveryOption view model.
        /// </summary>
        /// <param name="deliveryOptions">The CRT delivery option.</param>
        /// <returns>The delivery option view model.</returns>
        public DeliveryOption ConvertToViewModel(CrtDataModel.DeliveryOption deliveryOption)
        {
            return new DeliveryOption()
            {
                Id = deliveryOption.Code,
                Description = deliveryOption.Description
            };
        }

        /// <summary>
        /// Converts CRT cart lines to view model shopping cart items.
        /// </summary>
        /// <param name="cartLines">The CRT cart lines.</param>
        /// <param name="productValidator">The product validator.</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>A view model collection of shopping cart items.</returns>
        public Collection<TransactionItem> ConvertToViewModel(IEnumerable<CrtDataModel.CartLine> cartLines, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (cartLines == null)
            {
                return null;
            }

            Collection<TransactionItem> cartItems = new Collection<TransactionItem>();            

            bool isResultManadatory = false;
            IDictionary<long, IDictionary<string, object>> propertiesByProductId = searchEngine.GetListingsProperties(
                isResultManadatory, 
                cartLines.Select(cl => cl.ProductId),
                searchEngine.ImageSearchPropertyName,
                searchEngine.PathSearchPropertyName
                );

            foreach (CrtDataModel.CartLine cartLine in cartLines)
            {
                IDictionary<string, object> productProperties;
                propertiesByProductId.TryGetValue(cartLine.ProductId, out productProperties);
                cartItems.Add(ConvertToViewModel(cartLine, dataLevel, productProperties, productValidator, searchEngine));
            }

            return cartItems;
        }

        /// <summary>
        /// Converts CRT cart line to view model shopping cart item.
        /// </summary>
        /// <param name="cartLine">The CRT cart line.</param>
        /// <param name="properties">The key value pairs of product properties.</param>
        /// <param name="productValidator">The product validator.</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>A view model shopping cart item.</returns>
        public TransactionItem ConvertToViewModel(CrtDataModel.CartLine cartLine, ShoppingCartDataLevel dataLevel, IDictionary<string, object> properties, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (cartLine == null)
            {
                return null;
            }

            TransactionItem cartItem = new TransactionItem();

            cartItem.LineId = cartLine.LineId;
            cartItem.ItemId = cartLine.LineData.ItemId;
            cartItem.VariantInventoryDimensionId = cartLine.LineData.InventoryDimensionId;
            cartItem.ProductId = cartLine.LineData.ProductId;
            cartItem.ProductDetails = (string)cartLine.LineData["ProductDetails"];
            cartItem.Quantity = (int)cartLine.LineData.Quantity;

            //NS by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
            cartItem.NetAmountWithCurrency = cartLine.NetAmountWithoutTax.ToCurrencyString(configurationValue.GetCurrentCulture());
                // (cartLine.LineData.TotalAmount - cartLine.LineData.TaxAmountExclusive).ToCurrencyString(configurationValue.GetCurrentCulture());
            cartItem.AFMNetAmount = cartLine.NetAmountWithoutTax;
            //NE by RxL

            cartItem.Comment = cartLine.Comment;

            // Seed default values
            cartItem.Image = new ImageInfo();
            cartItem.ProductUrl = Utilities.InactiveLinkUrl;

            object imageSearchValue;
            object pathValue;

            if (properties != null)
            {
                if (properties.TryGetValue(searchEngine.ImageSearchPropertyName, out imageSearchValue))
                {
                    cartItem.Image = (ImageInfo)imageSearchValue;
                }

                if (properties.TryGetValue(searchEngine.PathSearchPropertyName, out pathValue))
                {
                    cartItem.ProductUrl = (string)pathValue;
                }
            }

            if (dataLevel > ShoppingCartDataLevel.Minimal)
            {
                cartItem.TaxAmountWithCurrency = cartLine.LineData.TaxAmount.ToCurrencyString(configurationValue.GetCurrentCulture());
                //N by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
                cartItem.AFMTaxAmount = cartLine.LineData.TaxAmount;
                cartItem.DiscountAmountWithCurrency = cartLine.LineData.DiscountAmount.ToCurrencyString(configurationValue.GetCurrentCulture());
                cartItem.DiscountAmount = cartLine.LineData.DiscountAmount;
                cartItem.OfferNames = String.Join(",", cartLine.LineData.DiscountLines.Select(dl => dl.OfferName));
                cartItem.PriceWithCurrency = cartLine.LineData.Price.ToCurrencyString(configurationValue.GetCurrentCulture());
                cartItem.DeliveryModeId = cartLine.LineData.DeliveryMode;
                cartItem.ElectronicDeliveryEmail = cartLine.LineData.ElectronicDeliveryEmail;
                cartItem.ShippingAddress = AddressMapper.ConvertToViewModel(cartLine.LineData.ShippingAddress);
            }

            if (dataLevel == ShoppingCartDataLevel.All)
            {
                foreach (var promotionLine in cartLine.PromotionLines)
                {
                    cartItem.PromotionLines.Add(promotionLine);
                }
            }

            // Throws an exception if the Product Details blob is malformed.
            productValidator.ValidateProductDetails(cartItem.ProductDetails);

            return cartItem;
        }

        /// <summary>
        /// Gets the products from cart item.
        /// </summary>
        /// <param name="shoppingCart">The shopping cart.</param>
        /// <returns>Listings.</returns>
        public ICollection<Listing> GetListingsFromCart(ShoppingCart shoppingCart)
        {
            if (shoppingCart == null)
            {
                throw new ArgumentNullException("shopping cart");
            }

            ICollection<Listing> listings = new Collection<Listing>();
            Collection<TransactionItem> Items = shoppingCart.Items;

            foreach (TransactionItem item in Items)
            {
                Listing listing = new Listing();
                listing.ListingId = item.ProductId;
                listing.ProductDetails = item.ProductDetails;
                listing.Quantity = item.Quantity;
                listing.Comment = item.Comment;

                listings.Add(listing);
            }

            return listings;
        }

        /// <summary>
        /// Gets the cart lines from listings.
        /// </summary>
        /// <param name="listings">The listings.</param>
        /// <returns>Cart lines.</returns>
        public Collection<CrtDataModel.CartLine> GetCartLinesFromListing(IEnumerable<Listing> listings, IProductValidator productValidator)
        {
            if (listings == null)
            {
                return null;
            }

            IDictionary<long, Listing> indexedListings = new Dictionary<long, Listing>();

            foreach (var listing in listings)
            {
                if (listing.Quantity <= 0)
                {
                    string message = string.Format("The quantity of a listing cannot be negative - Listing {0} Quantity {1}", listing.ListingId, listing.Quantity);
                    throw new DataValidationException(DataValidationErrors.InvalidQuantity, message);
                }

                long listingId = listing.ListingId;
                if (!indexedListings.ContainsKey(listingId))
                {
                    indexedListings.Add(listingId, listing);
                }
            }

            // From a perf view, it might be better to cache the gift card item id, instead of fetching the channel configuration everytime.
            ChannelManager channelManager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());
            string giftCardItemId = channelManager.GetChannelConfiguration().GiftCardItemId;

            IEnumerable<CrtDataModel.Product> dmProducts = GetProducts(indexedListings.Keys);
            Collection<CrtDataModel.CartLine> cartLines = new Collection<CrtDataModel.CartLine>();

            foreach (CrtDataModel.Product dmProduct in dmProducts)
            {

                // First process all potential standalone products.
                if (!dmProduct.IsMasterProduct)
                {
                    // Finds the first listing matching the given record id.
                    if (!indexedListings.ContainsKey(dmProduct.RecordId))
                    {
                        continue;
                    }
                    Listing listing = indexedListings[dmProduct.RecordId];

                    // Throws an exception if the Product Details blob is malformed.
                    productValidator.ValidateProductDetails(listing.ProductDetails);

                    CrtDataModel.CartLine cartLine = new CrtDataModel.CartLine();

                    cartLine.LineData = GetCartItemFromProductListing(dmProduct, listing);

                    // This is required if the gift card can be a standalone product.
                    if (string.Equals(dmProduct.ItemId, giftCardItemId, StringComparison.InvariantCultureIgnoreCase))
                    {
                        SetCartLineGiftCardProperties(cartLine, listing.GiftCardAmount);
                    }
                    cartLines.Add(cartLine);

                    continue;
                }

                // otherwise we have to identify which of this product's variants were part of the original set of listings
                List<long> lookupIds = new List<long>(dmProduct.CompositionInformation.VariantInformation.IndexedVariants.Keys);
                IEnumerable<long> intersection = indexedListings.Keys.Intersect(lookupIds);

                // Loop to process all listings that map to variants of a product.
                foreach (var variantId in intersection)
                {
                    // Finds the first listing matching the given record id.
                    if (!indexedListings.ContainsKey(variantId))
                    {
                        continue;
                    }
                    Listing listing = indexedListings[variantId];

                    // Throws an exception if the Product Details blob is malformed.
                    productValidator.ValidateProductDetails(listing.ProductDetails);

                    CrtDataModel.CartLine cartLine = new CrtDataModel.CartLine();

                    cartLine.LineData = GetCartItemFromProductListing(
                            dmProduct.CompositionInformation.VariantInformation.IndexedVariants[variantId],
                            listing);

                    // This is required if we support variants of a gift card.
                    if (string.Equals(dmProduct.ItemId, giftCardItemId, StringComparison.InvariantCultureIgnoreCase))
                    {
                        SetCartLineGiftCardProperties(cartLine, listing.GiftCardAmount);
                    }
                    cartLines.Add(cartLine);
                }
            }

            return cartLines;
        }

        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <param name="productIds">The product ids.</param>
        /// <returns>Products.</returns>
        public IEnumerable<CrtDataModel.Product> GetProducts(IEnumerable<long> productIds)
        {
            var commerceRuntime = CrtUtilities.GetCommerceRuntime();
            ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
            long channelId = channelManager.GetCurrentChannelId();
            ProductManager manager = ProductManager.Create(commerceRuntime);
            CrtDataModel.ProductSearchCriteria criteria = new CrtDataModel.ProductSearchCriteria(channelId);
            criteria.Ids = productIds.ToList();
            criteria.DataLevel = CrtDataModel.CommerceEntityDataLevel.Identity;

            return manager.SearchProducts(criteria, new CrtDataModel.QueryResultSettings()).Results;
        }

        /// <summary>
        /// Sets Gift card properties on the cartline.
        /// </summary>
        /// <param name="cartLine">CartLine to be updated.</param>
        /// <param name="giftCardAmount">The gift card amount.</param>
        public void SetCartLineGiftCardProperties(CrtDataModel.CartLine cartLine, decimal giftCardAmount)
        {
            cartLine.Price = giftCardAmount;
            cartLine.IsGiftCardLine = true;
        }

        /// <summary>
        /// Gets the cart item line from listing.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <param name="quantity">The quantity.</param>
        /// <param name="productDetails">The product details.</param>
        /// <returns>A CartLineData instance.</returns>
        public CrtDataModel.CartLineData GetCartItemFromProductListing(CrtDataModel.Product product, Listing listing)
        {
            if (product == null)
            {
                return null;
            }

            CrtDataModel.CartLineData cartLineData = new CrtDataModel.CartLineData();
            cartLineData.ItemId = product.ItemId;
            cartLineData.InventoryDimensionId = string.Empty;
            cartLineData.ProductId = product.RecordId;
            cartLineData.Quantity = listing.Quantity;
            cartLineData.Comment = listing.Comment;

            cartLineData["ProductDetails"] = listing.ProductDetails;

            return cartLineData;
        }

        /// <summary>
        /// Gets the cart item line from listing.
        /// </summary>
        /// <param name="productVariant">The listing.</param>
        /// <param name="quantity">The quantity.</param>
        /// <param name="productDetails">The product details JSON.</param>
        /// <returns>A CartLineData instance.</returns>
        public CrtDataModel.CartLineData GetCartItemFromProductListing(CrtDataModel.ProductVariant productVariant, Listing listing)
        {
            if (productVariant == null)
            {
                return null;
            }

            CrtDataModel.CartLineData cartLineData = new CrtDataModel.CartLineData();
            cartLineData.ItemId = productVariant.ItemId;
            cartLineData.InventoryDimensionId = productVariant.InventoryDimensionId;
            cartLineData.ProductId = productVariant.DistinctProductVariantId;
            cartLineData.Quantity = listing.Quantity;
            cartLineData.Comment = listing.Comment;

            cartLineData["ProductDetails"] = listing.ProductDetails;

            return cartLineData;
        }

        /// <summary>
        /// Converts CRT SalesLineDeliveryOptions to LineDeliveryOption view model.
        /// </summary>
        /// <param name="salesLineDeliveryOptions">The CRT delivery options.</param>
        /// <param name="deliveryOptionsMapping">The delivery options mapping.</param>
        /// <returns>
        /// The item delivery methods view model.
        /// </returns>
        public Collection<LineDeliveryOption> ConvertToViewModel(IEnumerable<CrtDataModel.SalesLineDeliveryOption> salesLineDeliveryOptions)
        {
            Collection<LineDeliveryOption> lineDeliveryOptions = new Collection<LineDeliveryOption>();
            foreach (CrtDataModel.SalesLineDeliveryOption salesLineDeliveryOption in salesLineDeliveryOptions)
            {
                LineDeliveryOption lineDeliveryOption = new LineDeliveryOption(
                     salesLineDeliveryOption.SalesLineId,
                     new ShoppingCartMapper(this.configurationValue).ConvertToViewModel(salesLineDeliveryOption.DeliveryOptions));

                lineDeliveryOptions.Add(lineDeliveryOption);
            }
            return lineDeliveryOptions;
        }

        /// <summary>
        /// Converts card types to PaymentCardType view model.
        /// </summary>
        /// <param name="cardTypes">The card types.</param>
        /// <returns>A PaymentCardType view model.</returns>
        public Collection<PaymentCardType> ConvertToViewModel(IEnumerable<string> cardTypes)
        {
            Collection<PaymentCardType> paymentCards = new Collection<PaymentCardType>();
            foreach (string cardType in cardTypes)
            {
                paymentCards.Add(new PaymentCardType() { Id = cardType, CardType = cardType });
            }
            return paymentCards;
        }

        /// <summary>
        /// Converts Payment view model to PaymentCard data model.
        /// </summary>
        /// <param name="payment">The payment.</param>
        /// <returns>
        /// A PaymentCard data model.
        /// </returns>
        public static CrtDataModel.PaymentCard ConvertToDataModel(Payment payment)
        {
            CrtDataModel.PaymentCard paymentCard = new CrtDataModel.PaymentCard()
            {
                Address1 = string.Format(CultureInfo.CurrentCulture, "{0} {1}", payment.PaymentAddress.StreetNumber, payment.PaymentAddress.Street),
                City = payment.PaymentAddress.City,
                Zip = payment.PaymentAddress.ZipCode,
                Country = payment.PaymentAddress.Country,
                CardNumber = payment.CardNumber,
                CardTypes = payment.CardType,
                CCID = payment.CCID,
                ExpirationMonth = payment.ExpirationMonth,
                ExpirationYear = payment.ExpirationYear,
                NameOnCard = payment.NameOnCard,     
                State = payment.PaymentAddress.State,
            };

            return paymentCard;
        }

        /// <summary>
        /// Converts TokenizedPaymentCard view model to TokenizedPaymentCard data model.
        /// </summary>
        /// <param name="tokenizedPaymentCard">The tokenized payment card.</param>
        /// <param name="paymentConnectorServiceAccountId">The payment connector service account identifier.</param>
        /// <returns>
        /// A PaymentCard data model.
        /// </returns>
        public static CrtDataModel.TokenizedPaymentCard ConvertToDataModel(TokenizedPaymentCard tokenizedPaymentCard, string paymentConnectorServiceAccountId)
        {
            CrtDataModel.TokenizedPaymentCard tokenizedPaymentCardDM = new CrtDataModel.TokenizedPaymentCard()
            {
                Address1 = string.Format(CultureInfo.CurrentCulture, "{0} {1}", tokenizedPaymentCard.PaymentAddress.StreetNumber, tokenizedPaymentCard.PaymentAddress.Street),
                City = tokenizedPaymentCard.PaymentAddress.City,
                Zip = tokenizedPaymentCard.PaymentAddress.ZipCode,
                Country = tokenizedPaymentCard.PaymentAddress.Country,
                CardTypes = tokenizedPaymentCard.CardType,
                ExpirationMonth = tokenizedPaymentCard.ExpirationMonth,
                ExpirationYear = tokenizedPaymentCard.ExpirationYear,
                NameOnCard = tokenizedPaymentCard.NameOnCard,
                State = tokenizedPaymentCard.PaymentAddress.State,
                
                CardTokenInfo = new CrtDataModel.CardTokenInfo()
                {
                    MaskedCardNumber = tokenizedPaymentCard.MaskedCardNumber,
                    UniqueCardId = tokenizedPaymentCard.UniqueCardId,
                    CardToken = tokenizedPaymentCard.CardToken,
                    ServiceAccountId = paymentConnectorServiceAccountId
                }
            };

            return tokenizedPaymentCardDM;
        }

        /// <summary>
        /// Converts TenderDataLine view model to CartTenderLine data model.
        /// </summary>
        /// <param name="tenderDataLines">The view model tenderlinedata.</param>
        /// <returns>A CartTenderline data model.</returns>
        public List<CrtDataModel.CartTenderLine> ConvertToDataModel(IEnumerable<TenderDataLine> tenderDataLines)
        {
            ChannelManager channelManager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());
            string channelCurrency = channelManager.GetChannelConfiguration().Currency;

            IEnumerable<CrtDataModel.TenderType> tenderTypes = channelManager.GetChannelTenderTypes(new CrtDataModel.QueryResultSettings()).Results;
            IEnumerable<CrtDataModel.TenderType> creditCardTenderTypes = tenderTypes.Where(t => t.OperationId == (int)CrtDataModel.RetailOperation.PayCard);
            IEnumerable<CrtDataModel.TenderType> giftCardTenderTypes = tenderTypes.Where(t => t.OperationId == (int)CrtDataModel.RetailOperation.PayGiftCertificate);
            IEnumerable<CrtDataModel.TenderType> loyaltyCardTenderTypes = tenderTypes.Where(t => t.OperationId == (int)CrtDataModel.RetailOperation.PayLoyalty);

            bool anyCreditCardTenderLine = tenderDataLines.Any(t => (t.PaymentCard != null || t.TokenizedPaymentCard != null));
            bool anyGiftCardTenderLine = tenderDataLines.Any(t => !string.IsNullOrEmpty(t.GiftCardId));
            bool anyLoyaltyCardTenderLine = tenderDataLines.Any(t => !string.IsNullOrEmpty(t.LoyaltyCardId));
            string creditCardTenderTypeId = null;
            string giftCardTenderTypeId = null;
            string loyaltyCardTenderTypeId = null;

            if (anyCreditCardTenderLine)
            {
                if (!creditCardTenderTypes.Any())
                {
                    throw new ConfigurationException(ConfigurationErrors.UnableToFindTenderTypeConfig, "Operation id for credit card tender type not found");
                }

                if (creditCardTenderTypes.Skip(1).Any())
                {
                    throw new ConfigurationException(ConfigurationErrors.UnableToFindTenderTypeConfig, "Multiple operation id for tender type credit card exists");
                }

                creditCardTenderTypeId = creditCardTenderTypes.Single().TenderTypeId;
            }

            if (anyGiftCardTenderLine)
            {
                if (!giftCardTenderTypes.Any())
                {
                    throw new ConfigurationException(ConfigurationErrors.UnableToFindTenderTypeConfig, "Operation id for gift card tender type not found");
                }

                if (giftCardTenderTypes.Skip(1).Any())
                {
                    throw new ConfigurationException(ConfigurationErrors.UnableToFindTenderTypeConfig, "Multiple operation id for tender type gift card exists");
                }

                giftCardTenderTypeId = giftCardTenderTypes.Single().TenderTypeId;
            }

            if (anyLoyaltyCardTenderLine)
            {
                if (!loyaltyCardTenderTypes.Any())
                {
                    throw new ConfigurationException(ConfigurationErrors.UnableToFindTenderTypeConfig, "Operation id for loyalty card tender type not found");
                }

                if (loyaltyCardTenderTypes.Skip(1).Any())
                {
                    throw new ConfigurationException(ConfigurationErrors.UnableToFindTenderTypeConfig, "Multiple operation id for tender type loyalty card exists");
                }

                loyaltyCardTenderTypeId = loyaltyCardTenderTypes.Single().TenderTypeId;
            }

            List<CrtDataModel.CartTenderLine> cartTenderlines = new List<CrtDataModel.CartTenderLine>();
            foreach (TenderDataLine tenderDataLine in tenderDataLines)
            {
                CrtDataModel.CartTenderLine cartTenderline = new CrtDataModel.CartTenderLine();

                cartTenderline.Currency = channelCurrency;
                if (tenderDataLine.PaymentCard != null)
                {
                    cartTenderline.TenderTypeId = creditCardTenderTypeId;
                    cartTenderline.PaymentCard = ConvertToDataModel(tenderDataLine.PaymentCard);
                    cartTenderline.CardTypeId = cartTenderline.PaymentCard.CardTypes;
                }
                else if (tenderDataLine.TokenizedPaymentCard != null)
                {
                    cartTenderline.TenderTypeId = creditCardTenderTypeId;

                    string paymentServiceAccountId = Utilities.GetPaymentConnectorServiceAccountId();
                    if (string.IsNullOrWhiteSpace(paymentServiceAccountId))
                    {
                        var message = "A non-empty payment service account identifier that was used to create the credit card token must be specified for payment processing to proceed.";
                        throw new ConfigurationException(DataValidationErrors.ObjectNotFound, message);
                    }

                    cartTenderline.TokenizedPaymentCard = ConvertToDataModel(tenderDataLine.TokenizedPaymentCard, paymentServiceAccountId);
                    cartTenderline.CardTypeId = cartTenderline.TokenizedPaymentCard.CardTypes;
                }
                else if (!string.IsNullOrEmpty(tenderDataLine.GiftCardId))
                {
                    cartTenderline.Amount = tenderDataLine.Amount;
                    cartTenderline.TenderTypeId = giftCardTenderTypeId;
                    cartTenderline.GiftCardId = tenderDataLine.GiftCardId;
                }
                else if (!string.IsNullOrEmpty(tenderDataLine.LoyaltyCardId))
                {
                    cartTenderline.Amount = tenderDataLine.Amount;
                    cartTenderline.TenderTypeId = loyaltyCardTenderTypeId;
                    cartTenderline.LoyaltyCardId = tenderDataLine.LoyaltyCardId;
                }

                cartTenderlines.Add(cartTenderline);
            }
            return cartTenderlines;
        }

        /// <summary>
        /// Converts KitLine view model to KitLineProductProperty data model.
        /// </summary>
        /// <param name="kitLine">A KitLine type from view model.</param>
        /// <returns>A KitLineProductProperty type from the data model.</returns>
        public CrtDataModel.KitLineProductProperty ConvertToDataModel(KitLine kitLine)
        {
            if (kitLine == null)
            {
                return null;
            }

            CrtDataModel.KitLineProductProperty kitLineProductProperty = new CrtDataModel.KitLineProductProperty();
            kitLineProductProperty.KitLineIdentifier = kitLine.LineId;
            kitLineProductProperty.ProductId = kitLine.ProductId;

            return kitLineProductProperty;
        }

        /// <summary>
        /// Converts KitLine view model to KitLineProductProperty data model.
        /// </summary>
        /// <param name="kitLines">Collection of KitLine types from view model.</param>
        /// <returns>A collection of KitLineProductProperty types from the data model.</returns>
        public Collection<CrtDataModel.KitLineProductProperty> ConvertToDataModel(IEnumerable<KitLine> kitLines)
        {
            Collection<CrtDataModel.KitLineProductProperty> kitLineProductProperties = new Collection<CrtDataModel.KitLineProductProperty>();

            if (kitLines != null)
            {
                foreach (var kitLine in kitLines)
                {
                    CrtDataModel.KitLineProductProperty kitLineProductProperty = ConvertToDataModel(kitLine);
                    kitLineProductProperties.Add(kitLineProductProperty);
                }
            }

            return kitLineProductProperties;
        }
    }
}