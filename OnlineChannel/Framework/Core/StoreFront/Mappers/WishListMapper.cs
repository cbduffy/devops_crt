﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Performs mappings between the wish list view model and Commerce Runtime Sdk entities.
    /// </summary>
    public static class WishListMapper
    {
        /// <summary>
        /// Converts from CRT Wish list to view model Wish list.
        /// </summary>
        /// <param name="wishList">The CRT wish list.</param>
        /// <returns>The view model wish list.</returns>
        internal static WishList ConvertToViewModel(CrtDataModel.CommerceList wishList)
        {
            WishList wishListViewModel = new WishList();
            wishListViewModel.Id = wishList.Id;
            wishListViewModel.Name = wishList.Name;
            wishListViewModel.CustomerId = wishList.CustomerId;
            wishListViewModel.IsFavorite = wishList.IsFavorite;
            wishListViewModel.WishListLines = ConvertToViewModel(wishList.CommerceListLines);

            return wishListViewModel;
        }

        /// <summary>
        /// Converts CRT wish list lines to view model wish list lines.
        /// </summary>
        /// <param name="wishListLines">The CRT wish list lines.</param>
        /// <returns>A view model collection of wish list lines.</returns>
        internal static Collection<WishListLine> ConvertToViewModel(IEnumerable<CrtDataModel.CommerceListLine> wishListLines)
        {
            Collection<WishListLine> viewModelWishListLines = new Collection<WishListLine>();
            WishListLine viewModelWishListLine;
            foreach (CrtDataModel.CommerceListLine wishListLine in wishListLines)
            {
                viewModelWishListLine = new WishListLine();
                viewModelWishListLine.WishListId = wishListLine.CommerceListId;
                viewModelWishListLine.LineId = wishListLine.LineId;
                viewModelWishListLine.CustomerId = wishListLine.CustomerId;
                viewModelWishListLine.ProductId = wishListLine.ProductId;
                viewModelWishListLine.Quantity = wishListLine.Quantity;
                viewModelWishListLines.Add(viewModelWishListLine);
            }

            return viewModelWishListLines;
        }

        /// <summary>
        /// Converts from view model Wish list to CRT Wish list.
        /// </summary>
        /// <param name="wishList">The view model wish list.</param>
        /// <returns>The CRT wish list.</returns>
        internal static CrtDataModel.CommerceList ConvertToDataModel(WishList wishList)
        {
            CrtDataModel.CommerceList wishListDataModel = new CrtDataModel.CommerceList();
            wishListDataModel.Id = wishList.Id;
            wishListDataModel.Name = wishList.Name;
            wishListDataModel.CustomerId = wishList.CustomerId;
            wishListDataModel.IsFavorite = Convert.ToBoolean(wishList.IsFavorite, CultureInfo.InvariantCulture);
            wishListDataModel.CommerceListLines = ConvertToDataModel(wishList.WishListLines);

            return wishListDataModel;
        }

        /// <summary>
        /// Converts view model wish list lines to CRT wish list lines.
        /// </summary>
        /// <param name="wishListLines">The view model wish list lines.</param>
        /// <returns>The CRT wish list lines.</returns>
        internal static Collection<CrtDataModel.CommerceListLine> ConvertToDataModel(IEnumerable<WishListLine> wishListLines)
        {
            Collection<CrtDataModel.CommerceListLine> dataModelWishListLines = new Collection<CrtDataModel.CommerceListLine>();
            CrtDataModel.CommerceListLine dataModelWishListLine;
            foreach (WishListLine wishListLine in wishListLines)
            {
                dataModelWishListLine = new CrtDataModel.CommerceListLine();
                dataModelWishListLine.CommerceListId = wishListLine.WishListId;
                dataModelWishListLine.LineId = wishListLine.LineId;
                dataModelWishListLine.CustomerId = wishListLine.CustomerId;
                dataModelWishListLine.ProductId = wishListLine.ProductId;
                dataModelWishListLine.Quantity = wishListLine.Quantity;
            }

            return dataModelWishListLines;
        }
    }
}