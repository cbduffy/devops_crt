﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Map DataModel address to ViewModel address.
    /// </summary>
    public static partial class AddressMapper
    {
        /// <summary>
        /// Convert the Data Model Address to a View Model Address.
        /// </summary>
        /// <param name="address">The data model address</param>
        /// <returns>Address</returns>
        //internal static Address ConvertToViewModel(CrtDataModel.Address address)
        //{
        //    if (address == null)
        //    {
        //        return null;
        //    }

        //    Address vmAddress = new Address()
        //    {
        //        StreetNumber = address.StreetNumber,
        //        Street = address.Street,
        //        City = address.City,
        //        State = address.State,
        //        County = address.County,
        //        DistrictName = address.DistrictName,
        //        ZipCode = address.ZipCode,
        //        Country = address.ThreeLetterISORegionName,
        //        Email = address.Email,
        //        EmailContent = address.EmailContent,
        //        Phone = address.Phone,
        //        AttentionTo = address.AttentionTo,
        //        AddressType = (AddressType)address.AddressTypeValue,
        //        RecordId = address.RecordId,
        //        AddressFriendlyName = address.Name,
        //        IsPrimary = address.IsPrimary,
        //        Name = address.Name,
        //    };

        //    return vmAddress;
        //}

        /// <summary>
        /// Converts Address view model to CRT Address.
        /// </summary>
        /// <param name="address">The view model address.</param>
        /// <returns>The CRT address.</returns>
        internal static CrtDataModel.Address ConvertToDataModel(Address address)
        {
            if (address == null)
            {
                return null;
            }

            return new CrtDataModel.Address()
            {
                StreetNumber = address.StreetNumber,
                Street = address.Street,
                City = address.City,
                State = address.State,
                County = address.County,
                DistrictName = address.DistrictName,
                ZipCode = address.ZipCode,
                ThreeLetterISORegionName = address.Country,
                Email = address.Email,
                EmailContent = address.EmailContent,
                Phone = address.Phone,
                AttentionTo = address.AttentionTo,
                AddressTypeValue = (int)address.AddressType,
                Name = address.Name,
                IsPrimary = address.IsPrimary
            };
        }
    }
}