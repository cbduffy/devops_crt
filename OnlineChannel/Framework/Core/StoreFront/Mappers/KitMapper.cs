﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Encapsulates logic specific to populating kit details.
    /// </summary>
    public class KitMapper
    {
        private const string PropertyKeyImage = "Image";

        private IConfiguration configurationValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="KitMapper"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public KitMapper(IConfiguration configuration)
        {
            this.configurationValue = configuration;
        }

        /// <summary>
        /// Locates and populates the details for all kit items in the collection of transaction items passed in.
        /// </summary>
        /// <param name="transactionItems">The transaction items that should be updated.</param>
        /// <param name="productDetails">The product details for all the product identifiers that appear in the shopping cart..</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Provided product {0} could not be resolved into product details.</exception>
        public virtual void PopulateKitItemDetails(IEnumerable<TransactionItem> transactionItems, IEnumerable<CrtDataModel.Product> productDetails, ISearchEngine searchEngine)
        {
            if (transactionItems == null)
            {
                throw new ArgumentNullException("transactionItems");
            }

            if (productDetails == null)
            {
                throw new ArgumentNullException("productDetails");
            }

            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }

            IEnumerable<CrtDataModel.Product> kitProducts = productDetails.Where(p => p.IsKit == true);

            foreach (var kitProductMaster in kitProducts)
            {
                // Find the shopping cart item that corresponds to the current kit product.
                foreach (TransactionItem transactionItem in transactionItems)
                {
                    Dictionary<long, long> variantToMasterLookup = new Dictionary<long, long>();

                    long cartItemId = transactionItem.ProductId;
                    CrtDataModel.ProductVariant kitVariant = null;

                    //Check if the current shopping cart item is one of the variants of the current kit product master.
                    kitVariant = kitProductMaster.CompositionInformation.VariantInformation.Variants.Where(v => v.DistinctProductVariantId == cartItemId).SingleOrDefault();
                    if (kitVariant != null)
                    {
                        transactionItem.ItemType = TransactionItemType.Kit;
                        transactionItem.KitComponents = new Collection<TransactionItem>();

                        var kitComponentList = kitProductMaster.CompositionInformation.KitDefinition.IndexedKitVariantToComponentMap[cartItemId].KitComponentKeyList;
                        //// Fetch all the components ids along with their positions
                        foreach (CrtDataModel.KitComponentKey kitComponent in kitComponentList)
                        {
                            TransactionItem kitComponentCartItem = new TransactionItem();
                            kitComponentCartItem.ProductId = kitComponent.DistinctProductId;
                            kitComponentCartItem.LineId = kitComponent.KitLineIdentifier.ToString();

                            // If we have made it this far, then it means that the product is available on the current channel, hence listing properties for the given product id must exist.
                            bool isResultManadatory = true;
                            IDictionary<string, object> kitComponentCartItemProperties = searchEngine.GetListingProperties(isResultManadatory, kitComponent.DistinctProductId, searchEngine.PathSearchPropertyName);
                            if (kitComponentCartItemProperties != null)
                            {
                                object pathValue;
                                if (kitComponentCartItemProperties.TryGetValue(searchEngine.PathSearchPropertyName, out pathValue))
                                {
                                    kitComponentCartItem.ProductUrl = (string)pathValue;
                                }
                            }
                            else
                            {
                                kitComponentCartItem.ProductUrl = Utilities.InactiveLinkUrl;
                            }

                            CrtDataModel.Product kitComponentProductMaster = null;
                            CrtDataModel.ProductVariant kitComponentVariant = null;
                            // Check if the product is a standalone non variant product
                            kitComponentProductMaster = productDetails.Where(p => p.RecordId == kitComponent.DistinctProductId).FirstOrDefault();

                            if (kitComponentProductMaster == null)
                            {
                                //This means that current kit component is not a standalone (non-variant) product. Therefore, it must be a variant of a product master.
                                foreach (CrtDataModel.Product product in productDetails)
                                {
                                    // Certain products, such as gift cards, do not have composition information.
                                    if (product.CompositionInformation != null)
                                    {
                                        kitComponentVariant = product.CompositionInformation.VariantInformation.Variants.Where(v => v.DistinctProductVariantId == kitComponent.DistinctProductId).SingleOrDefault();
                                        if (kitComponentVariant != null)
                                        {
                                            // we found the product master of the kit component.
                                            kitComponentProductMaster = product;
                                            break;
                                        }
                                    }
                                }
                            }

                            // If we have still not located the product entity for the kit component, then we have a problem.
                            if (kitComponentProductMaster == null)
                            {
                                throw new DataValidationException(DataValidationErrors.ListingIdNotFound, "Provided product {0} could not be resolved into product details.", kitComponent.DistinctProductId);
                            }

                            // This check is necessary, because we might have the same product appearing multiple times as a component in the current kit variant.
                            if (!variantToMasterLookup.ContainsKey(kitComponent.DistinctProductId))
                            {
                                variantToMasterLookup.Add(kitComponent.DistinctProductId, kitComponentProductMaster.RecordId);
                            }

                            kitComponentCartItem.ProductName = kitComponentProductMaster.ProductName;
                            kitComponentCartItem.ProductNumber = kitComponentProductMaster.ProductNumber;
                            kitComponentCartItem.Description = kitComponentProductMaster.Description;
                            kitComponentCartItem.Image = this.GetKitComponentImageData(kitComponent.DistinctProductId, kitComponentProductMaster, searchEngine);

                            if (kitComponentVariant == null)
                            {
                                //This means that the component is a standalone product.
                                kitComponentCartItem.Size = string.Empty;
                                kitComponentCartItem.Style = string.Empty;
                                kitComponentCartItem.Color = string.Empty;
                            }
                            else
                            {
                                //T his means that the component is a variant of the product master.
                                kitComponentCartItem.Size = kitComponentVariant.Size;
                                kitComponentCartItem.Style = kitComponentVariant.Style;
                                kitComponentCartItem.Color = kitComponentVariant.Color;
                            }

                            transactionItem.KitComponents.Add(kitComponentCartItem);
                        }

                        // Populate the few remaining details of a kit component that are stored at the kit definition level.
                        PopulateComponentLevelPropertiesFromKitDefinition(transactionItem, kitProductMaster, variantToMasterLookup);
                    }
                }
            }
        }

        /// <summary>
        /// Get the image url of a kit component.
        /// </summary>
        /// <param name="kitComponentProductId">The kit component identifier.</param>
        /// <param name="kitComponentProductMaster">The product master of the kit component.</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <returns>
        /// Image Url for the kit component.
        /// </returns>
        protected virtual ImageInfo GetKitComponentImageData(long kitComponentProductId, CrtDataModel.Product kitComponentProductMaster, ISearchEngine searchEngine)
        {
            if (kitComponentProductMaster == null)
            {
                throw new ArgumentNullException("kitComponentProductMaster");
            }

            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }

            ImageInfo image = new ImageInfo();
            CrtDataModel.ProductPropertyDictionary indexedProperties = kitComponentProductMaster.GetIndexedProperties(kitComponentProductId, CrtUtilities.GetChannelDefaultCulture().Name);
            CrtDataModel.ProductProperty imageProperty = null;
            if (indexedProperties.TryGetValue(PropertyKeyImage, out imageProperty))
            {
                if (imageProperty != null)
                {
                    CrtDataModel.RichMediaLocations imageSearchValue = imageProperty.Value as CrtDataModel.RichMediaLocations;
                    image = searchEngine.UpdateProductImageData(imageSearchValue);
                }
            }

            return image;
        }

        /// <summary>
        /// Populates the cart item with kit component level properties from the kit definition.
        /// </summary>
        /// <param name="transactionItem">The shopping cart item which is a kit component.</param>
        /// <param name="kitProductMaster">Product representing the kit product master.</param>
        /// <param name="variantToMasterLookup">Quick lookup to map a variant identifier to its product master identifer.</param>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Unable to find component properties for component {0} at kit line {2}.</exception>
        protected virtual void PopulateComponentLevelPropertiesFromKitDefinition(TransactionItem transactionItem, CrtDataModel.Product kitProductMaster, Dictionary<long, long> variantToMasterLookup)
        {
            if (transactionItem == null)
            {
                throw new ArgumentNullException("cartItem");
            }

            if (kitProductMaster == null)
            {
                throw new ArgumentNullException("kitProductMaster");
            }

            if (variantToMasterLookup == null)
            {
                throw new ArgumentNullException("variantToMasterLookup");
            }

            //Need to fetch quantity, price defined at the kit component level.
            IEnumerable<CrtDataModel.KitLineDefinition> kitLineDefinitions = kitProductMaster.CompositionInformation.KitDefinition.KitLineDefinitions;
            foreach (TransactionItem kitComponentItem in transactionItem.KitComponents)
            {
                long currentKitLineId = Convert.ToInt64(kitComponentItem.LineId);
                long currentComponentId = kitComponentItem.ProductId;
                CrtDataModel.KitLineDefinition currentKitLineDefinition = kitLineDefinitions.Where(k => k.KitLineIdentifier == currentKitLineId).Single();

                CrtDataModel.KitLineProductPropertyDictionary indexedComponentPropertiesForCurrentLine = currentKitLineDefinition.IndexedComponentProperties;

                CrtDataModel.KitLineProductProperty kitLineProductProperty = null;

                // First do a lookup on the kit component's variant identifer.
                if (indexedComponentPropertiesForCurrentLine.TryGetValue(currentComponentId, out kitLineProductProperty))
                {
                    kitComponentItem.Quantity = Convert.ToInt32(kitLineProductProperty.Quantity);
                    kitComponentItem.PriceWithCurrency = GetKitComponentPriceCurrencyString(kitLineProductProperty.Charge);
                }
                else
                {
                    // Do a lookup based on the component's master identifer.
                    long currentComponentMasterId = variantToMasterLookup[currentComponentId];
                    if (indexedComponentPropertiesForCurrentLine.TryGetValue(currentComponentMasterId, out kitLineProductProperty))
                    {
                        kitComponentItem.Quantity = Convert.ToInt32(kitLineProductProperty.Quantity);
                        kitComponentItem.PriceWithCurrency = GetKitComponentPriceCurrencyString(kitLineProductProperty.Charge);
                    }
                    else
                    {
                        throw new DataValidationException(DataValidationErrors.ListingIdNotFound, "Unable to find component properties for component {0} at kit line {2}.", kitComponentItem.ProductId, kitComponentItem.LineId);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the kit component price currency string.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <returns>
        /// A formatted string for the kit component price.
        /// </returns>
        protected string GetKitComponentPriceCurrencyString(decimal amount)
        {
            string formattedKitComponentPrice = Resources.Included;

            if (amount != 0)
            {
                formattedKitComponentPrice = amount.ToCurrencyString(this.configurationValue.GetCurrentCulture());
            }

            return formattedKitComponentPrice;
        }
    }
}