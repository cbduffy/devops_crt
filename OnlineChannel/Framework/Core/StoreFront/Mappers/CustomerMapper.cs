/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;


    /// <summary>
    /// Customer Mapper class.
    /// </summary>
    public static partial class CustomerMapper
    {
        /// <summary>
        /// Convert the Data Model Customer to a View Model Customer.
        /// </summary>
        /// <param name="customer">The data model customer.</param>
        /// <returns>A view model customer.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the customer is null.</exception>
        //internal static Customer ConvertToViewModel(CrtDataModel.Customer customer)
        //{
        //    if (customer == null)
        //    {
        //        throw new ArgumentNullException("customer");
        //    }

        //    Customer vmCustomer = new Customer()
        //    {
        //        AccountNumber = customer.AccountNumber,
        //        Addresses = ConvertToViewModel(customer.Addresses),
        //        Email = customer.Email,
        //        FirstName = customer.FirstName,
        //        LastName = customer.LastName,
        //        MiddleName = customer.MiddleName,
        //        Phone = customer.Phone,
        //        PhoneExt = customer.PhoneExt,
        //        RecordId = customer.RecordId,
        //        Url = customer.Url,
        //        PrimaryAddress = AddressMapper.ConvertToViewModel(customer.GetPrimaryAddress())
        //    };

        //    return vmCustomer;
        //}

        /// <summary>
        /// Convert a Data Model list of Addresses to a View Model list of Addresses.
        /// </summary>
        /// 
        /// <param name="addresses">The data model addresses.</param>
        /// <returns>Collection of view model address objects.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the addresses are null.</exception>
        //internal static Collection<Address> ConvertToViewModel(IEnumerable<CrtDataModel.Address> addresses)
        //{
        //    if (addresses == null)
        //    {
        //        throw new ArgumentNullException("addresses");
        //    }

        //    Collection<Address> vmAddresses = new Collection<Address>();

        //    foreach (CrtDataModel.Address address in addresses)
        //    {
        //        if (address != null)
        //        {
        //            vmAddresses.Add(AddressMapper.ConvertToViewModel(address));
        //        }
        //    }

        //    return vmAddresses;
        //}

        /// <summary>
        /// Convert a ViewModel customer to a Data Model customer.
        /// </summary>
        /// <param name="dataModelCustomer">The data model customer.</param>
        /// <param name="customer">The view model customer.</param>
        /// <returns>The data model customer.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the customer is null.</exception>
        //internal static CrtDataModel.Customer UpdateDataModel(CrtDataModel.Customer dataModelCustomer, Customer customer)
        //{
        //    if (customer == null)
        //    {
        //        throw new ArgumentNullException("customer");
        //    }

        //    if (dataModelCustomer == null)
        //    {
        //        throw new ArgumentNullException("dataModelCustomer");
        //    }

        //    if (customer.Email != null)
        //    {
        //        dataModelCustomer.Email = customer.Email;
        //    }

        //    if (customer.FirstName != null)
        //    {
        //        dataModelCustomer.FirstName = customer.FirstName;
        //    }

        //    if (customer.LastName != null)
        //    {
        //        dataModelCustomer.LastName = customer.LastName;
        //    }

        //    if (customer.MiddleName != null)
        //    {
        //        dataModelCustomer.MiddleName = customer.MiddleName;
        //    }

        //    if (customer.Phone != null)
        //    {
        //        dataModelCustomer.Phone = customer.Phone;
        //    }

        //    if (customer.PhoneExt != null)
        //    {
        //        dataModelCustomer.PhoneExt = customer.PhoneExt;
        //    }

        //    if (customer.Url != null)
        //    {
        //        dataModelCustomer.Url = customer.Url;
        //    }

        //    dataModelCustomer.Language = "en-us"; // only support en-Us in the UI at this time

        //    if (customer.Addresses != null)
        //    {
        //        Address primaryAddressInCollection = customer.Addresses.FirstOrDefault(a => a.RecordId == customer.PrimaryAddress.RecordId);

        //        if (primaryAddressInCollection != null)
        //        {
        //            primaryAddressInCollection.AddressFriendlyName = customer.PrimaryAddress.AddressFriendlyName;
        //            primaryAddressInCollection.AttentionTo = customer.PrimaryAddress.AttentionTo;
        //            primaryAddressInCollection.City = customer.PrimaryAddress.City;
        //            primaryAddressInCollection.Country = customer.PrimaryAddress.Country;
        //            primaryAddressInCollection.Deactivate = customer.PrimaryAddress.Deactivate;
        //            primaryAddressInCollection.DistrictName = customer.PrimaryAddress.DistrictName;
        //            primaryAddressInCollection.Email = customer.PrimaryAddress.Email;
        //            primaryAddressInCollection.EmailContent = customer.PrimaryAddress.EmailContent;
        //            primaryAddressInCollection.Name = customer.PrimaryAddress.Name;
        //            primaryAddressInCollection.IsPrimary = customer.PrimaryAddress.IsPrimary;
        //            primaryAddressInCollection.Phone = customer.PrimaryAddress.Phone;
        //            primaryAddressInCollection.State = customer.PrimaryAddress.State;
        //            primaryAddressInCollection.Street = customer.PrimaryAddress.Street;
        //            primaryAddressInCollection.StreetNumber = customer.PrimaryAddress.StreetNumber;
        //            primaryAddressInCollection.AddressType = customer.PrimaryAddress.AddressType;
        //            primaryAddressInCollection.ZipCode = customer.PrimaryAddress.ZipCode;
        //        }
        //        else
        //        {
        //            // if the primary address is added to the addresses collection before this line of code this needs to be removed.
        //            foreach (Address address in customer.Addresses)
        //            {
        //                address.IsPrimary = false;
        //            }

        //            customer.PrimaryAddress.IsPrimary = true;

        //            List<Address> customerAddresses = customer.Addresses.ToList();
        //            customerAddresses.Add(customer.PrimaryAddress);
        //            customer.Addresses = (IEnumerable<Address>)customerAddresses;
        //        }
        //    }

        //    dataModelCustomer = ConvertToDataModel(dataModelCustomer, customer.Addresses);

        //    return dataModelCustomer;
        //}

        /// <summary>
        /// Convert a View Model list of addresses to a Data Model list of addresses.
        /// </summary>
        /// <param name="dmCustomer">The data model model to be updated.</param>
        /// <param name="addresses">The view model addresses.</param>
        /// <returns>The updated data model customer.</returns>
        //internal static CrtDataModel.Customer ConvertToDataModel(CrtDataModel.Customer dmCustomer, IEnumerable<Address> addresses)
        //{
        //    List<CrtDataModel.Address> dmAddresses = new List<CrtDataModel.Address>();

        //    if (addresses != null)
        //    {
        //        foreach (Address address in addresses)
        //        {
        //            if (address != null)
        //            {
        //                CrtDataModel.Address tempAdddress = new CrtDataModel.Address();

        //                // If the address exists already find the matching record.                       
        //                foreach (CrtDataModel.Address dmAddress in dmCustomer.Addresses)
        //                {
        //                    if (dmAddress.RecordId == address.RecordId || CustomerMapper.AddressValueComparer(dmAddress, address))
        //                    {
        //                        tempAdddress = dmAddress;
        //                        break;
        //                    }
        //                }

        //                dmAddresses.Add(UpdateDataModel(tempAdddress, address));
        //            }
        //        }
        //    }

        //    dmCustomer.Addresses = dmAddresses;

        //    return dmCustomer;
        //}

        /// <summary>
        /// Convert a View Model address to a Data Model address.
        /// </summary>
        /// <param name="dmAddress">The data model address.</param>
        /// <param name="address">The view model address.</param>
        /// <returns>The data model address.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the address is null.</exception>
        //internal static CrtDataModel.Address UpdateDataModel(CrtDataModel.Address dmAddress, Address address)
        //{
        //    if (address == null)
        //    {
        //        throw new ArgumentNullException("address");
        //    }

        //    if (dmAddress == null)
        //    {
        //        throw new ArgumentNullException("dmAddress");
        //    }

        //    dmAddress.AttentionTo = address.AttentionTo;
        //    dmAddress.City = address.City;
        //    dmAddress.ThreeLetterISORegionName = address.Country;
        //    dmAddress.County = address.County;
        //    dmAddress.DistrictName = address.DistrictName;
        //    dmAddress.Phone = address.Phone;
        //    dmAddress.State = address.State;
        //    dmAddress.Street = address.Street;
        //    dmAddress.StreetNumber = address.StreetNumber;
        //    dmAddress.ZipCode = address.ZipCode;
        //    dmAddress.Deactivate = address.Deactivate;
        //    dmAddress.AddressType = (CrtDataModel.AddressType)Enum.Parse(address.AddressType.GetType(), address.AddressType.ToString());
        //    dmAddress.IsPrimary = address.IsPrimary;
        //    dmAddress.Name = address.Name;
        //    dmAddress.Email = address.Email;

        //    return dmAddress;
        //}

        /// <summary>
        /// Checks if two address values are equal.
        /// </summary>
        /// <param name="dmAddress">The data model address.</param>
        /// <param name="address">The view model address.</param>
        /// <returns>True if equal, false otherwise.</returns>
        internal static bool AddressValueComparer(CrtDataModel.Address dmAddress, Address address)
        {
            if (String.Equals(dmAddress.ThreeLetterISORegionName ?? string.Empty, address.Country ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.State ?? string.Empty, address.State ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.City ?? string.Empty, address.City ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.Street ?? string.Empty, address.Street ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.Name ?? string.Empty, address.Name ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.ZipCode ?? string.Empty, address.ZipCode ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.County ?? string.Empty, address.County ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.AttentionTo ?? string.Empty, address.AttentionTo ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.DistrictName ?? string.Empty, address.DistrictName ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.Phone ?? string.Empty, address.Phone ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.StreetNumber ?? string.Empty, address.StreetNumber ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                String.Equals(dmAddress.Email ?? string.Empty, address.Email ?? string.Empty, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }
    }
}