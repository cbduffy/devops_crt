﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Maps data model channel configuration to view model channel configuration.
    /// </summary>
    public class ChannelMapper
    {
        private IConfiguration configurationValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChannelMapper"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public ChannelMapper(IConfiguration configuration)
        {
            this.configurationValue = configuration;
        }

        /// <summary>
        /// Convert data model channel configuration to view model channel configuration.
        /// </summary>
        /// <param name="dmChannelConfiguration">The data model channel configuration.</param>
        /// <returns>The view model channel configuration.</returns>
        internal ChannelConfiguration ConvertToViewModel(CrtDataModel.ChannelConfiguration dmChannelConfiguration)
        {
            if (dmChannelConfiguration == null)
            {
                return null;
            }

            string currentCulture = this.configurationValue.GetCurrentCulture();
            ChannelConfiguration vmChannelConfiguration = new ChannelConfiguration()
            {
                RecordId = dmChannelConfiguration.RecordId,
                InventLocation = dmChannelConfiguration.InventLocation,
                Currency = dmChannelConfiguration.Currency,
                CompanyCurrency = dmChannelConfiguration.CompanyCurrency,
                PriceIncludesSalesTax = dmChannelConfiguration.PriceIncludesSalesTax,
                CountryRegionId = dmChannelConfiguration.CountryRegionId,
                DefaultLanguageId = dmChannelConfiguration.DefaultLanguageId,
                PickupDeliveryModeCode = dmChannelConfiguration.PickupDeliveryModeCode,
                BingMapsApiKey = dmChannelConfiguration.BingMapsApiKey,
                TimeZoneInfoId = dmChannelConfiguration.TimeZoneInfoId,
                EmailDeliveryModeCode = dmChannelConfiguration.EmailDeliveryModeCode,
                GiftCardItemId = dmChannelConfiguration.GiftCardItemId,
                CurrencyStringTemplate = Utilities.GetChannelCurrencyStringTemplate(currentCulture)
            };

            return vmChannelConfiguration;
        }

        /// <summary>
        /// Converts data model country region info to view model country info.
        /// </summary>
        /// <param name="countryRegionInfo">The data model country region info.</param>
        /// <returns>The view model country info.</returns>
        internal static CountryInfo ConvertToViewModel(CrtDataModel.CountryRegionInfo countryRegionInfo)
        {
            CountryInfo countryInfo = new CountryInfo()
            {
                CountryCode = countryRegionInfo.CountryRegionId,
                CountryName = countryRegionInfo.ShortName
            };

            return countryInfo;
        }

        /// <summary>
        /// Converts a data model country region info collection to view model country info collection.
        /// </summary>
        /// <param name="countryRegionInfoCollection">The data model country region info collection.</param>
        /// <returns>The view model country info collection.</returns>
        internal static Collection<CountryInfo> ConvertToViewModel(ReadOnlyCollection<CrtDataModel.CountryRegionInfo> countryRegionInfoCollection)
        {
            Collection<CountryInfo> countryInfoCollection = new Collection<CountryInfo>();

            foreach (CrtDataModel.CountryRegionInfo countryRegionInfo in countryRegionInfoCollection)
            {
                CountryInfo countryInfo = ConvertToViewModel(countryRegionInfo);
                countryInfoCollection.Add(countryInfo);
            }

            return countryInfoCollection;
        }

        /// <summary>
        /// Converts data model state/province info collection to view model state/province info collection.
        /// </summary>
        /// <param name="dmStateProvinceInfoCollection">The data model state/province info collection.</param>
        /// <returns>The view model state/province info collection.</returns>
        internal static Collection<StateProvinceInfo> ConvertToViewModel(IEnumerable<CrtDataModel.StateProvinceInfo> dmStateProvinceInfoCollection)
        {
            Collection<StateProvinceInfo> stateProvinceInfoCollection = new Collection<StateProvinceInfo>(new List<StateProvinceInfo>(dmStateProvinceInfoCollection.Count()));

            foreach (CrtDataModel.StateProvinceInfo dmStateProvinceInfo in dmStateProvinceInfoCollection)
            {
                StateProvinceInfo stateProvinceInfo = ConvertToViewModel(dmStateProvinceInfo);
                stateProvinceInfoCollection.Add(stateProvinceInfo);
            }

            return stateProvinceInfoCollection;
        }

        /// <summary>
        /// Converts data model state/province info to view model state/province info.
        /// </summary>
        /// <param name="dmStateProvinceInfo">The data model state/province info.</param>
        /// <returns>The view model state/province info.</returns>
        internal static StateProvinceInfo ConvertToViewModel(CrtDataModel.StateProvinceInfo dmStateProvinceInfo)
        {
            StateProvinceInfo stateProvinceInfo = new StateProvinceInfo
            {
                CountryRegionId = dmStateProvinceInfo.CountryRegionId,
                StateId = dmStateProvinceInfo.StateId,
                StateName = dmStateProvinceInfo.StateName
            };

            return stateProvinceInfo;
        }
    }
}