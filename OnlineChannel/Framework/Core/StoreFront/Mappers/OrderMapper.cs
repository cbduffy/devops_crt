﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
    
    /// <summary>
    /// Map DataModel orders to ViewModel orders.
    /// </summary>
    public partial class OrderMapper
    {
        private const string ListingPropProductName = "ProductName";
        private const string ListingPropDescription = "Description";
        private const string ListingPropImage = "Image";
        private const string ListingPropColor = "Color";
        private const string ListingPropSize = "Size";
        private const string ListingPropStyle = "Style";
        private const string ListingPropConfiguration = "Configuration";

        private IConfiguration configurationValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="OrderMapper"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public OrderMapper(IConfiguration configuration)
        {
            this.configurationValue = configuration;
        }

        /// <summary>
        /// Convert Data Model SalesOrder collection to View Model SalesOrder collection.
        /// </summary>
        /// <param name="orders">The data model orders.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <returns>
        /// The view model orders.
        /// </returns>
        internal Collection<SalesOrder> ConvertToViewModel(IEnumerable<CrtDataModel.SalesOrder> orders, bool includeSalesLines, ISearchEngine searchEngine)
        {
            if (orders == null)
            {
                return null;
            }

            CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();

            // Retrieve delivery mode descriptions.
            OrderManager orderManager = OrderManager.Create(commerceRuntime);
            IEnumerable<CrtDataModel.DeliveryOption> deliveryOptions = orderManager.GetAllDeliveryOptions();
            IDictionary<string, CrtDataModel.DeliveryOption> deliveryOptionsDictionary = deliveryOptions.ToDictionary(dlvo => dlvo.Code);

            // Retrieve current channel info.
            ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
            string timeZoneId = channelManager.GetChannelConfiguration().TimeZoneInfoId;
            long channelId = channelManager.GetCurrentChannelId();

            // Retrieve the products from CRT.
            IEnumerable<long> allUniqueProductIds = this.GetAllUniqueProductIds(orders);

            var criteria = new CrtDataModel.ProductSearchCriteria(channelId);
            criteria.Ids = allUniqueProductIds.AsReadOnly();
            criteria.DataLevel = CrtDataModel.CommerceEntityDataLevel.Complete;

            ProductManager productManager = ProductManager.Create(commerceRuntime);
            CrtDataModel.ProductSearchResult productSearchResult = productManager.SearchProducts(criteria, new CrtDataModel.QueryResultSettings());
            Dictionary<long, long> productMasterIdLookUp = productSearchResult.ProductIdLookupMap;
            IEnumerable<CrtDataModel.Product> productsWithoutKitComponents = productSearchResult.Results;

            Collection<SalesOrder> salesOrders = new Collection<SalesOrder>(orders.Select(i => ConvertToViewModel(i, includeSalesLines, searchEngine, timeZoneId, deliveryOptionsDictionary, productsWithoutKitComponents)).ToList());

            // Gather kit component details only if sales line are to be included.
            if (includeSalesLines)
            {
                IEnumerable<long> kitComponentProductIds = this.GetProductIdsOfKitComponents(allUniqueProductIds, productsWithoutKitComponents, productSearchResult.ProductIdLookupMap);
                if (kitComponentProductIds.Any())
                {
                    criteria.Ids = kitComponentProductIds.AsReadOnly();
                    ReadOnlyCollection<CrtDataModel.Product> kitComponentProducts = productManager.SearchProducts(criteria, new CrtDataModel.QueryResultSettings()).Results;

                    Collection<CrtDataModel.Product> productsIncludingKitComponents = new Collection<CrtDataModel.Product>();
                    productsIncludingKitComponents.AddRange(kitComponentProducts);
                    productsIncludingKitComponents.AddRange(productsWithoutKitComponents);

                    IEnumerable<TransactionItem> transactionItems = GetAllTransactionItems(salesOrders);

                    KitMapper kitsMapper = new KitMapper(this.configurationValue);
                    kitsMapper.PopulateKitItemDetails(transactionItems, productsIncludingKitComponents, searchEngine);
                }
            }

            return salesOrders;
        }

        /// <summary>
        /// Gets the product ids of kit components.
        /// </summary>
        /// <param name="productIdsFromSalesLines">The product identifiers from the sales lines.</param>
        /// <param name="products">The products.</param>
        /// <param name="productMasterIdLookUp">The product master identifier look up.</param>
        /// <returns>
        /// The product identifiers of the kit components that belong to any of the products across the sales lines.
        /// </returns>
        private ICollection<long> GetProductIdsOfKitComponents(IEnumerable<long> productIdsFromSalesLines, IEnumerable<CrtDataModel.Product> products, IDictionary<long, long> productMasterIdLookUp)
        {
            HashSet<long> kitComponentProductIds = new HashSet<long>();

            Dictionary<long, CrtDataModel.Product> productLookUpById = products.ToDictionary(p => p.RecordId);

            foreach (long productId in productIdsFromSalesLines)
            {
                if (productMasterIdLookUp.ContainsKey(productId))
                {
                    long parentProductId = productMasterIdLookUp[productId];

                    CrtDataModel.Product product = productLookUpById[parentProductId];

                    if (product.IsKit)
                    {
                        var kitVariantToProductMap = product.CompositionInformation.KitDefinition.IndexedKitVariantToComponentMap[productId];
                        ICollection<long> componentIds = kitVariantToProductMap.KitComponentKeyList.Select(kcl => kcl.DistinctProductId).ToList();

                        kitComponentProductIds.AddRange(componentIds);
                    }
                }
            }

            return kitComponentProductIds;
        }

        /// <summary>
        /// Gets all transaction items.
        /// </summary>
        /// <param name="salesOrders">The sales orders.</param>
        /// <returns>All the transaction items from the sales orders.</returns>
        private ICollection<TransactionItem> GetAllTransactionItems(IEnumerable<SalesOrder> salesOrders)
        {
            List<TransactionItem> transactionItems = new List<TransactionItem>();

            foreach (SalesOrder salesOrder in salesOrders)
            {
                transactionItems.AddRange(salesOrder.Items);
            }

            return transactionItems;
        }

        /// <summary>
        /// Gets a collection of all product identifiers that occur across all the sales orders.
        /// </summary>
        /// <param name="salesOrders">The sales orders.</param>
        /// <returns>
        /// Collection of unique product identifiers.
        /// </returns>
        private ICollection<long> GetAllUniqueProductIds(IEnumerable<CrtDataModel.SalesOrder> salesOrders)
        {
            HashSet<long> uniqueProductIds = new HashSet<long>();

            foreach (CrtDataModel.SalesOrder salesOrder in salesOrders)
            {
                if (salesOrder != null)
                {
                    foreach (CrtDataModel.SalesLine salesLine in salesOrder.SalesLines)
                    {
                        if (salesLine != null)
                        {
                            uniqueProductIds.Add(salesLine.ListingId);
                        }
                    }
                }
            }

            return uniqueProductIds;
        }

        /// <summary>
        /// Convert the Data Model SalesLines to View Model SalesOrderItems.
        /// </summary>
        /// <param name="salesLines">SalesLines</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <param name="deliveryOptionsDictionary">The delivery options dictionary.</param>
        /// <param name="products">The products.</param>
        /// <returns>
        /// View Model SalesOrderItems
        /// </returns>
        private Collection<TransactionItem> ConvertToViewModel(IEnumerable<CrtDataModel.SalesLine> salesLines, ISearchEngine searchEngine, IDictionary<string, CrtDataModel.DeliveryOption> deliveryOptionsDictionary, IEnumerable<CrtDataModel.Product> products)
        {
            if (salesLines == null)
            {
                return null;
            }

            IEnumerable<long> salesLineProductIds = salesLines.Select(s => s.ListingId).Distinct();

            // Build a map for products by  product id, as well as a lookup from listing id to product id.
            Dictionary<long, CrtDataModel.Product> productsByProductId = new Dictionary<long, CrtDataModel.Product>();
            Dictionary<long, long> productIdsByListingId = new Dictionary<long, long>();

            foreach (var product in products)
            {
                if (!productsByProductId.ContainsKey(product.RecordId))
                {
                    productsByProductId.Add(product.RecordId, product);
                }

                // If the product is a master, save the lookups for any variants present in the original sales line list
                // otherwise, the lookup is itself
                if (!product.IsMasterProduct)
                {
                    productIdsByListingId[product.RecordId] = product.RecordId;
                }
                else
                {
                    // Find which of the variants of this product were requested
                    foreach (var variantId in product.CompositionInformation.VariantInformation.IndexedVariants.Keys)
                    {
                        if (salesLineProductIds.Contains(variantId))
                        {
                            productIdsByListingId[variantId] = product.RecordId;
                        }
                    }
                }
            }

            Collection<TransactionItem> saleItems = new Collection<TransactionItem>();
            foreach (CrtDataModel.SalesLine salesLine in salesLines)
            {
                // There will be scenarios in which the product in the salesline is no longer available on the current channel.
                // For example, the salesline belongs to an order which was placed on a different channel which had different listings.
                CrtDataModel.Product parentProduct = null;
                long productId = 0;
                if (productIdsByListingId.TryGetValue(salesLine.ListingId, out productId))
                {
                    parentProduct = productsByProductId[productId];
                }

                TransactionItem salesOrderItem = GetSalesOrderItemFromSalesLine(salesLine, parentProduct, searchEngine, deliveryOptionsDictionary);

                saleItems.Add(salesOrderItem);
            }

            return saleItems;
        }

        /// <summary>
        /// Convert the Data Model SalesOrder to a View Model SalesOrder.
        /// </summary>
        /// <param name="order">The data model sales order.</param>
        /// <param name="includeSalesLines">bool to decide if Sales lines be returned. Can have performance impact</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <param name="timeZoneId">The time zone identifier.</param>
        /// <param name="deliveryOptionsDictionary">The delivery options dictionary.</param>
        /// <param name="products">The products.</param>
        /// <returns>
        /// The view model sales order.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the order is null.</exception>
        private SalesOrder ConvertToViewModel(CrtDataModel.SalesOrder order, bool includeSalesLines, ISearchEngine searchEngine, string timeZoneId, IDictionary<string, CrtDataModel.DeliveryOption> deliveryOptionsDictionary, IEnumerable<CrtDataModel.Product> products)
        {
            if (order == null)
            {
                throw new ArgumentNullException("order");
            }

            DateTime orderPlacedDate;
            // If a time zone id is obtained for the channel then convert the order date from utc to channel time zone else keep it in utc.
            if (!string.IsNullOrWhiteSpace(timeZoneId))
            {
                TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                orderPlacedDate = TimeZoneInfo.ConvertTimeFromUtc(order.OrderPlacedDate.DateTime, timeZoneInfo);
            }
            else
            {
                orderPlacedDate = order.OrderPlacedDate.DateTime;
            }

            string currentCulture = configurationValue.GetCurrentCulture();
            SalesOrder vmOrder = new SalesOrder()
            {
                Id = order.Id,
                ChannelId = order.ChannelId,
                OrderPlacedDate = orderPlacedDate.ToString(),
                PaymentStatus = (int)order.Status,
                RequestedDeliveryDate = order.RequestedDeliveryDate.GetValueOrDefault().DateTime.ToShortDateString(),
                SalesId = order.SalesId,
                Status = order.Status.ToString(),
                TaxAmountWithCurrency = order.TaxAmount.ToCurrencyString(currentCulture),
                TotalAmountWithCurrency = order.TotalAmount.ToCurrencyString(currentCulture),
                ConfirmationId = order.ChannelReferenceId,
                ShippingAddress = AddressMapper.ConvertToViewModel(order.ShippingAddress),
                SubtotalWithCurrency = order.SubtotalAmount.ToCurrencyString(currentCulture),
                LoyaltyCardId = order.LoyaltyCardId,
                ChargeAmountWithCurrency = order.ChargeAmount.ToCurrencyString(currentCulture),
                DiscountAmountWithCurrency = (order.LineDiscount + order.TotalDiscount + order.PeriodicDiscountAmount).ToCurrencyString(currentCulture),

                //NS by RxL 68588	ECS - Guest Order Status retrieval is incorrect.
                ReceiptEmail = order.ReceiptEmail,
                AFMBillingZipCode = order["AFMBILLINGZIPCODE"] == null ? string.Empty : order["AFMBILLINGZIPCODE"].ToString(),
                //NE
                //NS by Spriya - Synchrony Online
                AfmFinancingOptionId = order["FINANCINGOPTIONID"] == null ? string.Empty : order["FINANCINGOPTIONID"].ToString(),
                AFMCardName = order["CARDNAME"] != null ? order["CARDNAME"].ToString() : string.Empty,
                AFMPromoDesc = order["PROMODESC"] == null ? string.Empty : order["PROMODESC"].ToString(),
                //NE by Spriya - Synchrony Online

                
            };           

            vmOrder.DeliveryModeId = order.DeliveryMode;
            vmOrder.DeliveryModeDescription = OrderMapper.GetDeliveryModeDescription(order.DeliveryMode, deliveryOptionsDictionary);

            if (order.DiscountCodes != null)
            {
                vmOrder.DiscountCodes.AddRange(order.DiscountCodes);
            }
            
            AFMPopulateCustomSalesOrderDetails(order, vmOrder);//N Developed by v-hapat for thank you page

            //This is expensive call as we search products again to populate Sales lines. Use it when calling single order details
            if (includeSalesLines)
            {
                var items = ConvertToViewModel(order.SalesLines, searchEngine, deliveryOptionsDictionary, products);
                if (items != null)
                {
                    vmOrder.Items.AddRange(items);
                }
            }

            

            if (order.Status == CrtDataModel.SalesStatus.Unknown)
            {
                vmOrder.Status = "Pending";
            }

            return vmOrder;
        }

        /// <summary>
        /// Convert the Data Model SalesLine to a View Model SalesOrderItem.
        /// </summary>
        /// <param name="salesLine">A data model sales line.</param>
        /// <param name="product">A data model product.</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <param name="deliveryOptionsDictionary">The delivery options dictionary.</param>
        /// <returns>
        /// View Model SalesOrderItem
        /// </returns>
        private TransactionItem GetSalesOrderItemFromSalesLine(CrtDataModel.SalesLine salesLine, CrtDataModel.Product product, ISearchEngine searchEngine, IDictionary<string, CrtDataModel.DeliveryOption> deliveryOptionsDictionary)
        {
            if (salesLine == null)
            {
                return null;
            }

            TransactionItem salesItem = new TransactionItem();
            salesItem.DiscountAmountWithCurrency = salesLine.DiscountAmount.ToCurrencyString(configurationValue.GetCurrentCulture());
            salesItem.ProductId = salesLine.ListingId;
            salesItem.PriceWithCurrency = salesLine.Price.ToCurrencyString(configurationValue.GetCurrentCulture());
            salesItem.Quantity = (int)salesLine.Quantity;
            salesItem.NetAmountWithCurrency = salesLine.NetAmount.ToCurrencyString(configurationValue.GetCurrentCulture());           
            salesItem.DeliveryModeId = salesLine.DeliveryMode;
            salesItem.ElectronicDeliveryEmail = salesLine.ElectronicDeliveryEmailAddress;
            salesItem.DeliveryModeDescription = OrderMapper.GetDeliveryModeDescription(salesLine.DeliveryMode, deliveryOptionsDictionary);
            salesItem.ItemId = salesLine.ItemId;

            //NS developed by v-hapat
            salesItem.TaxAmountWithCurrency = salesLine.TaxAmount.ToCurrencyString(configurationValue.GetCurrentCulture());
            
            //NS by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
            salesItem.AFMNetAmount = salesLine.NetAmount;
            salesItem.AFMTaxAmount = salesLine.TaxAmount;
            //NE by RxL

            salesItem.DiscountAmount = salesLine.DiscountAmount;
            salesItem.OfferNames = String.Join(",", salesLine.DiscountLines.Select(dl => dl.OfferName));

            if (salesItem.Quantity > 0)
                salesItem.PriceAfterDiscount = Decimal.Round((salesLine.Price - (salesLine.DiscountAmount / salesLine.Quantity)), 2).ToCurrencyString();
            else
                salesItem.PriceAfterDiscount = salesItem.PriceWithCurrency;
            //NE developed by v-hapat

            // If the product details could not be found, then it is a valid scenario. 
            // Reason: The product could no longer be available on the current channel; or the product could have been added from a different channel by the same customer.
            bool isResultManadatory = false;
            IDictionary<string, object> salesItemProperties = searchEngine.GetListingProperties(isResultManadatory, salesItem.ProductId, searchEngine.PathSearchPropertyName, searchEngine.DescriptionSearchPropertyName);
            if (salesItemProperties != null)
            {
                object pathValue;
                if (salesItemProperties.TryGetValue(searchEngine.PathSearchPropertyName, out pathValue))
                {
                    salesItem.ProductUrl = (string)pathValue;
                }

                object descriptionValue;
                if (salesItemProperties.TryGetValue(searchEngine.DescriptionSearchPropertyName, out descriptionValue))
                {
                    salesItem.Description = (string)descriptionValue;
                }
            }
            else
            {
                // There can be times when the call to GetListingProperties fails to be information for the specified product id because the product is no longer available on the channel.
                // This is a valid scenario. In this case, we want to leave the product url for the sales line item to be empty.
                salesItem.ProductUrl = Utilities.InactiveLinkUrl;
            }

            // Ideally we want to fetch properties from the product.
            if (product != null)
            {
                PopulateSalesItemDetailsFromProduct(salesItem, product);
            }
            else
            {
                // Use the properties on the salesline as a fallback.
                salesItem.ImageData = Utilities.GetDefaultProductImageData();
                salesItem.ProductName = string.Format(Resources.ProductNameUnavailableTemplate, salesLine.ProductId);
                salesItem.ProductUrl = Utilities.InactiveLinkUrl;
                salesItem.Description = string.Format("[{0}] {1}", Resources.ProductUnavailableDescription, salesLine.Description);
                if (salesLine.Variant != null)
                {
                    salesItem.Color = salesLine.Variant.Color;
                    salesItem.Size = salesLine.Variant.Size;
                    salesItem.Configuration = salesLine.Variant.Configuration;
                }
            }

            salesItem.ShippingAddress = AddressMapper.ConvertToViewModel(salesLine.ShippingAddress);

            //NS - RxL - FDD0258-ECS-Review Customer Profile History Wish List V3 Phased-signed
            AFMPopulateCustomSalesItemDetails(salesLine, salesItem);

            return salesItem;
        }

        /// <summary>
        /// Populates the sales item details from product.
        /// </summary>
        /// <param name="salesItem">The sales item.</param>
        /// <param name="product">The product.</param>
        private void PopulateSalesItemDetailsFromProduct(TransactionItem salesItem, CrtDataModel.Product product)
        {
            salesItem.ProductNumber = product.ProductNumber;

            CultureInfo currentChannelCulture = CrtUtilities.GetChannelDefaultCulture();

            CrtDataModel.ProductPropertyDictionary indexedProperties;

            if (product.IsMasterProduct
                && product.CompositionInformation.VariantInformation.IndexedVariants.ContainsKey(salesItem.ProductId))
            {
                indexedProperties = product.GetIndexedProperties(salesItem.ProductId, currentChannelCulture.Name);
            }
            else
            {
                indexedProperties = product.GetIndexedProperties(currentChannelCulture.Name);
            }

            if (indexedProperties.ContainsKey(ListingPropImage))
            {
                salesItem.ImageData = Utilities.ConvertRichMediaToJson((CrtDataModel.RichMediaLocations)indexedProperties[ListingPropImage].Value);
            }

            CrtDataModel.ProductProperty productNameProperty;
            if (indexedProperties.TryGetValue(ListingPropProductName, out productNameProperty))
            {
                salesItem.ProductName = (string)productNameProperty.Value;
            }

            CrtDataModel.ProductProperty productDescriptionProperty;
            if (indexedProperties.TryGetValue(ListingPropDescription, out productDescriptionProperty))
            {
                salesItem.Description = (string)productDescriptionProperty.Value;
            }

            CrtDataModel.ProductProperty productColorProperty;
            if (indexedProperties.TryGetValue(ListingPropColor, out productColorProperty))
            {
                salesItem.Color = (string)productColorProperty.Value;
            }

            CrtDataModel.ProductProperty productSizeProperty;
            if (indexedProperties.TryGetValue(ListingPropSize, out productSizeProperty))
            {
                salesItem.Size = (string)productSizeProperty.Value;
            }

            CrtDataModel.ProductProperty productStyleProperty;
            if (indexedProperties.TryGetValue(ListingPropStyle, out productStyleProperty))
            {
                salesItem.Style = (string)productStyleProperty.Value;
            }

            CrtDataModel.ProductProperty productConfigurationProperty;
            if (indexedProperties.TryGetValue(ListingPropConfiguration, out productConfigurationProperty))
            {
                salesItem.Configuration = (string)productConfigurationProperty.Value;
            }
        }

        /// <summary>
        /// Gets the delivery mode description.
        /// </summary>
        /// <param name="deliveryModeId">The delivery mode identifier.</param>
        /// <param name="deliveryOptionsDictionary">The delivery options dictionary.</param>
        /// <returns>
        /// The description for the provided delivery mode identifier.
        /// </returns>
        private static string GetDeliveryModeDescription(string deliveryModeId, IDictionary<string, CrtDataModel.DeliveryOption> deliveryOptionsDictionary)
        {
            string deliveryModeDescription = deliveryModeId;

            if (!string.IsNullOrWhiteSpace(deliveryModeId))
            {
                CrtDataModel.DeliveryOption deliveryOptionOnOrder = null;
                if (deliveryOptionsDictionary.TryGetValue(deliveryModeId, out deliveryOptionOnOrder))
                {
                    deliveryModeDescription = deliveryOptionOnOrder.Description;
                }
            }

            return deliveryModeDescription;
        }
    }
}