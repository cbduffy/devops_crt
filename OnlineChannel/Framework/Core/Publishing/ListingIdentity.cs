﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Publishing
{
    using System;

    /// <summary>
    /// Represents unique ID of the listing.
    /// </summary>
    public sealed class ListingIdentity : IEquatable<ListingIdentity>
    {
        public long ProductId
        {
            get;
            set;
        }

        public string LanguageId
        {
            get;
            set;
        }

        public long CatalogId
        {
            get;
            set;
        }

        public string Tag
        {
            get;
            set;
        }

        public bool Equals(ListingIdentity other)
        {
            if (other == null)
            {
                return false;
            }

            return
                this.ProductId == other.ProductId &&
                this.LanguageId == other.LanguageId &&
                this.CatalogId == other.CatalogId &&
                this.Tag == other.Tag;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            ListingIdentity otherIdentity = obj as ListingIdentity;
            if (otherIdentity == null)
            {
                return false;
            }

            return otherIdentity.Equals(this);
        }

        public override int GetHashCode()
        {
            int tagIdHash = this.Tag == null ? 0 : this.Tag.GetHashCode();
            int languageHash = this.LanguageId == null ? 0 : this.LanguageId.GetHashCode();
            return this.ProductId.GetHashCode() ^ languageHash ^ this.CatalogId.GetHashCode() ^ tagIdHash;
        }
    }
}