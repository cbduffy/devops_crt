﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Publishing
{
    using System;

    public sealed class PublishingConfiguration
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "crt", Justification = "CRT is well known term, it states for CommerceRuntime.")]
        public PublishingConfiguration(int categoriesPageSize, int productAttributesPageSize, bool forceNoCatalogPublishing, int crtListingPageSize, bool forceTimingInfoLogging, bool checkEveryListingForRemoval)
        {
            this.CategoriesPageSize = categoriesPageSize;
            this.ProductAttributesPageSize = productAttributesPageSize;
            this.ForceNoCatalogPublishing = forceNoCatalogPublishing;
            this.CRTListingPageSize = crtListingPageSize;
            this.ForceTimingInfoLogging = forceTimingInfoLogging;
            this.CheckEveryListingForRemoval = checkEveryListingForRemoval;
        }

        public int CategoriesPageSize
        {
            get;
            private set;
        }

        public int ProductAttributesPageSize
        {
            get;
            private set;
        }

        public bool ForceNoCatalogPublishing
        {
            get;
            private set;
        }

        public int CRTListingPageSize
        {
            get;
            private set;
        }

        public bool ForceTimingInfoLogging
        {
            get;
            private set;
        }

        public bool CheckEveryListingForRemoval
        {
            get;
            private set;
        }
    }
}