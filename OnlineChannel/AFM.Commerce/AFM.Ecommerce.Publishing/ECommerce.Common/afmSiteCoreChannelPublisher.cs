﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Publishing;

namespace ECommerce.Publishing
{
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// This class implements IChannelPublisher
    /// </summary>
    public class AFMSiteCoreChannelPublisher : IChannelPublisher
    {
        /// <summary>
        /// Channels the information available.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="isPublishingRequested">if set to <c>true</c> [is publishing requested].</param>
        public virtual void OnChannelInformationAvailable(PublishingParameters parameters, bool isPublishingRequested)
        {
            //LoggerUtilities.ProcessLogMessage("Channel info is available.");
        }

        /// <summary>
        /// Site core requested args parameter
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="isPublishingRequested"></param>
        /// <param name="args"></param>
        public virtual void OnChannelInformationAvailable(PublishingParameters parameters, bool isPublishingRequested, object args)
        {
            OnChannelInformationAvailable(parameters, isPublishingRequested);
        }


        /// <summary>
        /// Validates the product attributes.
        /// </summary>
        /// <param name="attributes">The attributes.</param>


        public virtual void OnValidateProductAttributes(IEnumerable<AttributeProduct> attributes)
        {
           // LoggerUtilities.ProcessLogMessage("Product attributes are being validated");
        }


        /// <summary>
        /// Site core overloaded method with object args parameter
        /// </summary>
        /// <param name="attributes"></param>
        /// <param name="args"></param>
        public virtual void OnValidateProductAttributes(IEnumerable<AttributeProduct> attributes,object args)
        {
            OnValidateProductAttributes(attributes);
        }


    }
}
