﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Framework.Core.ClientManager;
using AFM.Commerce.Framework.Core.Models;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Publishing;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace ECommerce.Publishing
{
    using Microsoft.Dynamics.Retail.Channels;
    using System.Collections.ObjectModel;

    /// <summary>
    /// This class implements ICatalogPublishser and provides a custom method to get listings and custom product entities
    /// </summary>
    public class AFMSiteCoreCatalogPublisher : ICatalogPublisher
    {
        private List<Listing> _productDataListings;

        public AFMSiteCoreCatalogPublisher()
        {
            _productDataListings = new List<Listing>();
        }
        public List<Listing> AFMGetChangedProducts()
        {
            return _productDataListings;

        }
        /// <summary>
        /// This function gets the custom product related data
        /// </summary>
        /// <returns></returns>
        public virtual AFMProductDetails AFMGetProductCustomEntities(List<long> productRecordIdsList)
        {
            try
            {
                AFMProductDetailsClientManager productDetailsClientManager =
AFMProductDetailsClientManager.Create(CrtUtilities.GetCommerceRuntime());
                return productDetailsClientManager.GetListingExtension(productRecordIdsList);
            }
            catch
            {
                //AFM.Commerce.Logging.LoggerUtilities.ProcessLogMessage(ex.StackTrace);
                throw;
            }
        }

        
        
        /// <summary>
        /// Processes the page.
        /// </summary>
        /// <param name="listings">The listings.</param>
        /// <param name="pageNumberInCatalog">The page number in catalog.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="args">Requested by site core</param>
        public virtual void OnChangedProductsFound(
            Microsoft.Dynamics.Commerce.Runtime.DataModel.ChangedProductsSearchResult changedProducts,
            int pageNumberInCatalog, long catalogId)
        {
            if (null != changedProducts && null != changedProducts.Results)
            {
                foreach (Product product in changedProducts.Results)
                {
                    try
                    {
                        ReadOnlyCollection<Listing> listings = Listing.GetListings(product);
                        foreach (Listing listing in listings)
                        {
                            _productDataListings.Add(listing);
                        }
                    }catch
                    {
                       //AFM.Commerce.Logging.LoggerUtilities.ProcessLogMessage(ex.StackTrace);
                        throw;
                     }
                }
            }

        }


        /// <summary>
        /// site core required object args overloaded method
        /// </summary>
        /// <param name="changedProducts"></param>
        /// <param name="pageNumberInCatalog"></param>
        /// <param name="catalogId"></param>
        /// <param name="args"></param>
        public virtual void OnChangedProductsFound(
            ChangedProductsSearchResult changedProducts,
            int pageNumberInCatalog, long catalogId, object args)
        {

           OnChangedProductsFound(changedProducts, pageNumberInCatalog, catalogId);


        }

        /// <summary>
        /// Currents the catalog read completed.
        /// </summary>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="publisher">The publisher.</param>
        /// <param name="args">Requested by site core</param>
        /// 


        public virtual void OnCatalogReadCompleted(long catalogId, Publisher publisher)
        {
            string message = string.Format("Catalog read completed. CatalogID={0}", catalogId);
        }

        /// <summary>
        /// site core required object args  overloaded method
        /// </summary>
        /// <param name="catalogId"></param>
        /// <param name="publisher"></param>
        /// <param name="args"></param>
        public virtual void OnCatalogReadCompleted(long catalogId, Publisher publisher, object args)
        {
            OnCatalogReadCompleted(catalogId, publisher);
        }


        public virtual bool ExpandProducts
        {
            get { return true; }
        }


        public virtual void OnDeleteProductsByCatalogIdRequested(Dictionary<string, List<long>> catalogs)
        {
            //throw new NotImplementedException();
        }

        public virtual void OnDeleteProductsByLanguageIdRequested(Dictionary<string, List<string>> languageIds)
        {
            //throw new NotImplementedException();
        }

        public virtual void OnDeleteIndividualProductsRequested(IList<ListingIdentity> ids)
        {
            //throw new NotImplementedException();
        }
    }
}
