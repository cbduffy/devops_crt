﻿
using AFM.Commerce.Runtime.Services.Common;

namespace AFM.Commerce.Runtime.Services.DataTypes
{
    using System.Collections.Generic;
    using System.Linq;

    public sealed class RecordIdTableType : JoinableTableType
    {
        private const string RecordIdColumnName = "RECID";
        private const string TableTypeName = "RecordIdTableType";

        public RecordIdTableType(IEnumerable<long> recordIds)
            : this(recordIds, "RECID")
        {
        }

        public RecordIdTableType(IEnumerable<long> recordIds, string joinColumn)
            : base("RecordIdTableType")
        {
            ThrowIf.Null<IEnumerable<long>>(recordIds, "recordIds");
            ThrowIf.NullOrWhiteSpace(joinColumn, "joinColumn");
            base.JoinColumn = joinColumn;
            this.CreateTableSchema();
            foreach (long num in recordIds.Distinct<long>())
            {
                base.DataTable.Rows.Add(new object[] { num });
            }
        }

        protected override void CreateTableSchema()
        {
            base.DataTable.Columns.Add(this.TableTypeJoinColumn, typeof(long));
        }

        public override string TableTypeJoinColumn
        {
            get
            {
                return "RECID";
            }
        }
    }
}
