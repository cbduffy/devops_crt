﻿namespace AFM.Commerce.Runtime.Services.DataTypes
{
    using System;
    using System.Runtime.CompilerServices;

    public abstract class JoinableTableType : TableType
    {
        public JoinableTableType(string tableTypeName)
            : base(tableTypeName)
        {
        }

        public string JoinColumn { get; set; }

        public bool JoinColumnsSpecified
        {
            get
            {
                return (!string.IsNullOrWhiteSpace(this.TableTypeJoinColumn) && !string.IsNullOrWhiteSpace(this.JoinColumn));
            }
        }

        public abstract string TableTypeJoinColumn { get; }
    }
}
