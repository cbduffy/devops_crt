﻿using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace AFM.Commerce.Runtime.Services.DataTypes
{
    public sealed class ElectronicAddressTableType : TableType
    {
        private const string ElectronicAddressTableTypeName = "ElectronicAddressTableTypeName";

        public ElectronicAddressTableType(IEnumerable<ContactInfo> contactInfos)
            : base("ElectronicAddressTableTypeName")
        {
            this.CreateTableSchema();
            foreach (ContactInfo contactInfo in contactInfos)
                this.DataTable.Rows.Add(this.GetDataRow(contactInfo));
        }

        protected override void CreateTableSchema()
        {
            this.DataTable.Columns.Add("METHODYTPE", typeof(int));
            this.DataTable.Columns.Add("RECORDID", typeof(long));
            this.DataTable.Columns.Add("DIRPARTYLOCATIONIRECORDID", typeof(long));
            this.DataTable.Columns.Add("DIRPARTYRECORDID", typeof(long));
            this.DataTable.Columns.Add("LOCATIONDESCRIPTION", typeof(string)).MaxLength = 60;
            this.DataTable.Columns.Add("LOCATOR", typeof(string)).MaxLength = (int)byte.MaxValue;
            this.DataTable.Columns.Add("LOCATOREXTENSION", typeof(string)).MaxLength = 10;
            this.DataTable.Columns.Add("LOGISTICSLOCATIONID", typeof(string)).MaxLength = 30;
            this.DataTable.Columns.Add("LOGISTICSLOCATIONRECORDID", typeof(long));
            this.DataTable.Columns.Add("PARENTLOCATION", typeof(long));
            this.DataTable.Columns.Add("ISPRIMARY", typeof(int));
            this.DataTable.Columns.Add("ISMOBILEPHONE", typeof(int));
            this.DataTable.Columns.Add("ISPRIVATE", typeof(int));
            this.DataTable.Columns.Add("DIRPARTYLOCATIONROLERECORDID", typeof(long));
            this.DataTable.Columns.Add("LOGISTICSLOCATIONROLERECORDID", typeof(long));
        }

        private DataRow GetDataRow(ContactInfo contactInfo)
        {
            DataRow dataRow = this.DataTable.NewRow();
            dataRow["METHODYTPE"] = (object)contactInfo.AddressType;
            dataRow["RECORDID"] = (object)contactInfo.RecordId;
            dataRow["DIRPARTYLOCATIONIRECORDID"] = (object)contactInfo.PartyLocationRecordId;
            dataRow["DIRPARTYRECORDID"] = (object)contactInfo.PartyRecordId;
            dataRow["LOCATIONDESCRIPTION"] = contactInfo.Description.Length > 60 ? (object)contactInfo.Description.Substring(0, 60) : (object)contactInfo.Description;
            dataRow["LOCATOR"] = contactInfo.Value.Length > (int)byte.MaxValue ? (object)contactInfo.Value.Substring(0, (int)byte.MaxValue) : (object)contactInfo.Value;
            dataRow["LOCATOREXTENSION"] = contactInfo.ValueExtension == null || contactInfo.ValueExtension.Length <= 10 ? (object)contactInfo.ValueExtension : (object)contactInfo.ValueExtension.Substring(0, 10);
            dataRow["LOGISTICSLOCATIONID"] = (object)contactInfo.LogisticsLocationId;
            dataRow["LOGISTICSLOCATIONRECORDID"] = (object)contactInfo.LogisticsLocationRecordId;
            dataRow["PARENTLOCATION"] = (object)contactInfo.ParentLocation;
            dataRow["ISPRIMARY"] = (object)Convert.ToBoolean(contactInfo.IsPrimary ? 1 : 0);
            dataRow["ISMOBILEPHONE"] = (object)Convert.ToBoolean(contactInfo.IsMobilePhone ? 1 : 0);
            dataRow["ISPRIVATE"] = (object)Convert.ToBoolean(contactInfo.IsPrivate ? 1 : 0);
            dataRow["DIRPARTYLOCATIONROLERECORDID"] = (object)0;
            dataRow["LOGISTICSLOCATIONROLERECORDID"] = (object)0;
            return dataRow;
        }

        private static class ElectronicAddressTableTypeFields
        {
            internal const string MethodType = "METHODYTPE";
            internal const string RecordId = "RECORDID";
            internal const string DirPartyLocationRecordId = "DIRPARTYLOCATIONIRECORDID";
            internal const string DirPartyRecordId = "DIRPARTYRECORDID";
            internal const string LocationDescription = "LOCATIONDESCRIPTION";
            internal const string Locator = "LOCATOR";
            internal const string LocatorExtension = "LOCATOREXTENSION";
            internal const string LogisticsLocationId = "LOGISTICSLOCATIONID";
            internal const string LogisticsLocationRecordId = "LOGISTICSLOCATIONRECORDID";
            internal const string ParentLocation = "PARENTLOCATION";
            internal const string IsPrimary = "ISPRIMARY";
            internal const string IsMobilePhone = "ISMOBILEPHONE";
            internal const string IsPrivate = "ISPRIVATE";
            internal const string DirPartyLocationRoleRecordId = "DIRPARTYLOCATIONROLERECORDID";
            internal const string LogisticsLocationRoleRecordId = "LOGISTICSLOCATIONROLERECORDID";
        }
    }
}
