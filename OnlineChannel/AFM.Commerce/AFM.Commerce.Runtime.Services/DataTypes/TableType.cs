﻿using System;
using System.Data;

namespace AFM.Commerce.Runtime.Services.DataTypes
{
    public abstract class TableType : IDisposable
    {
        private readonly DataTable dataTable;
        // Flag: Has Dispose already been called? 
        bool disposed = false;

        public DataTable DataTable
        {
            get
            {
                return this.dataTable;
            }
        }

        protected TableType(string tableTypeName)
        {
            this.dataTable = new DataTable(tableTypeName);
        }

        // Public implementation of Dispose pattern callable by consumers. 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                this.dataTable.Dispose();
            }
            disposed = true;
        }

        ~TableType()
        {
            Dispose(false);
        }


        protected abstract void CreateTableSchema();
    }
}
