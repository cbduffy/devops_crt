﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Runtime.Services.Common
{
    public enum ExtendedCardType
    {
        Unknown = 0,
        Debit = 1,
        Visa = 2,
        MasterCard = 3,
        Discover = 4,
        Amex = 5,
        Gift = 6,
        Maestro = 7,
        VisaElec = 8,
        PRO100 = 9,
        Synchrony = 10
    }
}
