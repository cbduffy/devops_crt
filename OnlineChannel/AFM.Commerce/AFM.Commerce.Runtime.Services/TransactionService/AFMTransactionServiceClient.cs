﻿
using AFM.Commerce.Runtime.Services.Common;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
using Microsoft.Dynamics.Commerce.Runtime.TransactionService;
using System;
using System.Collections.ObjectModel;

namespace AFM.Commerce.Runtime.Services.TransactionService
{
    public class AFMTransactionServiceClient
    {
      //TO DO spriya merged for upgrade from 5.1
        public static void UpdateCustomerInformation(ref Customer customer, ServiceRequest request, Address address = null)
        {
            ThrowIf.Null<Customer>(customer, "customer");
            TransactionServiceClient transactionService = new TransactionServiceClient(request.RequestContext);
            ReadOnlyCollection<object> data = transactionService.InvokeExtensionMethod(CustomerConstants.UpdateCustomerInformationTransactionServiceMethod, GetUpsertCustomerInformationTransactionServiceParameters(customer, address));
            ParseUpsertCustomerInformationResponse(customer, address, data);
        }       

        private static object[] GetUpsertCustomerInformationTransactionServiceParameters(Customer customer, Address address)
        {
            object[] ret;
            if (address == null)
            {
               /* if (customer.GetPrimaryAddress() != null && string.IsNullOrEmpty(customer.FirstName))
                {
                    customer.GetPrimaryAddress()[CustomerConstants.AddressFirstName] = customer.FirstName;
                    customer.GetPrimaryAddress()[CustomerConstants.AddressLastName] = customer.LastName;
                }*/
                    address = new Address();
            }
            ret = new object[29]
              {
               (object) customer.AccountNumber,
                (object) customer[CustomerConstants.SourceInfo],
                (object) Convert.ToInt64(customer[CustomerConstants.IsMarketingOptin]),
                (object) Convert.ToInt64(customer[CustomerConstants.IsGuestCheckout]),
                (object) Convert.ToInt64(customer[CustomerConstants.IsOlderThan13]),
                (object) address.LogisticsLocationRecordId,
                (object) address.PhoneRecordId,
                (object) Convert.ToInt64( address[CustomerConstants.Phone2RecordId]),
                (object) Convert.ToInt64(address[CustomerConstants.Phone3RecordId]), 
                (object) address.EmailRecordId,
                (object) Convert.ToInt64(address[CustomerConstants.Email2RecordId]), 
                (object) address.Email,
                (object) address[CustomerConstants.Email2] ,                
                (object) CustomerConstants.Email1,
                (object) CustomerConstants.Email2,
                (object) address.Phone,
                (object) address.PhoneExt,                
                (object) CustomerConstants.Phone1, 
                (object) address[CustomerConstants.Phone2], 
                (object) address[CustomerConstants.Phone2Extension],                 
                (object) CustomerConstants.Phone2, 
                (object) address[CustomerConstants.Phone3], 
                (object) address[CustomerConstants.Phone3Extension],                 
                (object) CustomerConstants.Phone3,
                (object) address[CustomerConstants.AddressFirstName],
                (object) address[CustomerConstants.AddressLastName],
                (object) customer[CustomerConstants.IsMarketingOptinUniqueId],
                (object) customer[CustomerConstants.IsMarketingOptinVersionId],
                (object) ((DateTimeOffset)customer[CustomerConstants.IsMarketingOptinDate]).DateTime
              };

            return ret;
        }


        private static void ParseUpsertCustomerInformationResponse(Customer customer,Address address, ReadOnlyCollection<object> data)
        {
            if (address == null)
                address = new Address();

           /* Address primaryAddress = customer.GetPrimaryAddress();
            if (primaryAddress == null)
                return;*/
            customer["AFMRETAILCUSTRECID"] = long.Parse(data[0].ToString());
            address[CustomerConstants.LogisticElectronicLocationRecordId] = long.Parse(data[1].ToString());
            address[CustomerConstants.Phone1RecordId] = long.Parse(data[2].ToString());
            address[CustomerConstants.Phone2RecordId] = long.Parse(data[3].ToString());
            address[CustomerConstants.Phone3RecordId] = long.Parse(data[4].ToString());
            address[CustomerConstants.Email1RecordId] = long.Parse(data[5].ToString());
            address[CustomerConstants.Email2RecordId] = long.Parse(data[6].ToString());
        }
    }
}
