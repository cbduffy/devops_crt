﻿/// <reference path="../ExternalScripts/JQuery.d.ts" />
/// <reference path="../ExternalScripts/KnockoutJS.d.ts" />
/// <reference path="../ExternalScripts/jquery.tooltipster.d.ts" />
/// <reference path="../ExternalScripts/spin.d.ts" />
declare module AFM.Ecommerce.Controls {
    interface OrderConfirmation {
        cart: Cart;
        BillingAddress: Address;
        ShippingAddress: Address;
        PaymentAddress: Address;
    }
    class OrderConfirmationClass implements OrderConfirmation {
        public cart: any;
        public isShoppingCartEnabled: any;
        public isViewTaxEnabled: any;
        public isDiscountViewEnabled: any;
        public BillingAddress: any;
        public ShippingAddress: any;
        public PaymentAddress: any;
    }
    class ShoppingCartDataLevel {
        static Minimal: number;
        static Extended: number;
        static All: number;
    }
    class TransactionItemType {
        static None: number;
        static Kit: number;
        static KitComponent: number;
        static LineOrderNumber: any;
        static Lineitems: any;
    }
    class PageMode {
        static Shipping: string;
        static Billing: string;
        static Payment: string;
        static Confirm: string;
        static cartview: string;
        static thankyou: string;
    }
    interface IUserPageMode {
        PageMode?: PageModeOptions;
    }
    class UserPageMode implements IUserPageMode {
        public PageMode: PageModeOptions;
        constructor(o: any);
    }
    class PageModeOptions {
        static CartView: number;
        static ShippingView: number;
        static ShippingViewCompleted: number;
        static BillingView: number;
        static BillingViewcompleted: number;
        static PaymentView: number;
        static PaymentViewCompleted: number;
        static ConfirmView: number;
        static ConfirmViewCompleted: number;
        static ThankYou: number;
    }
    class PageStatus {
        static Complete: number;
        static Incomplete: number;
    }
    enum LoyaltyCardTenderType {
        AsCardTender = 0,
        AsContactTender = 1,
        NoTender = 2,
        Blocked = 3,
    }
    class ShoppingCartType {
        static None: number;
        static Shopping: number;
        static Checkout: number;
    }
    class AddressType {
        static Delivery: number;
        static Payment: number;
        static Invoice: number;
    }
    class AFMItemType {
        static Default: number;
        static DeliveryServiceItem: number;
        static ServiceItem: number;
    }
    enum DeliveryPreferenceType {
        None = 0,
        ShipToAddress = 1,
        PickupFromStore = 2,
        ElectronicDelivery = 3,
        DeliverItemsIndividually = 4,
    }
    interface AFMSalesTransHeader {
        OrderNumber?: string;
        IsOkToText?: boolean;
        OkToTextSurveyUniqueId?: string;
        OkToTextSurveyVersionId?: string;
        TermsAndContitionsUniqueId?: string;
        TermsAndContitionsVersionId?: string;
        IsAcceptTermsAndConditions?: boolean;
        IsOptinDealsUniqueId?: string;
        IsOptinDealsVersionId?: string;
    }
    interface CartProductDetails {
        Name?: string;
        ProductUrl?: string;
        ImageUrl?: string;
        Description?: string;
        ProductNumber?: string;
        DimensionValues?: any;
        SKU?: string;
        ImageMarkup?: string;
        Quantity?: number;
        ImageAlt?: string;
    }
    interface AFMZipCodeResponse {
        AFMZipCode?: string;
    }
    interface SelectedShippingOptions {
        DeliveryMethodId?: string;
        DeliveryMethodText?: string;
        ShippingOptionId?: string;
        CustomAddress?: Address;
        StoreAddress?: StoreProductAvailability;
        ElectronicDeliveryEmail?: string;
        ElectronicDeliveryEmailContent?: string;
    }
    interface SelectedItemShippingOptions {
        LineId?: string;
        Quantity?: number;
        DeliveryMethodId?: string;
        DeliveryMethodText?: string;
        ShippingOptionId?: string;
        CustomAddress?: Address;
        StoreAddress?: StoreProductAvailability;
        ElectronicDeliveryEmail?: string;
        ElectronicDeliveryEmailContent?: string;
    }
    interface SelectedLineShippingInfo {
        LineId?: string;
        ShipToAddress?: Address;
    }
    interface SelectedDeliveryOption {
        DeliveryModeId?: string;
        DeliveryModeText?: string;
        DeliveryPreferenceId?: string;
        CustomAddress?: Address;
        StoreAddress?: StoreProductAvailability;
        ElectronicDeliveryEmail?: string;
        ElectronicDeliveryEmailContent?: string;
    }
    interface SelectedLineDeliveryOption {
        LineId?: string;
        DeliveryModeId?: string;
        DeliveryModeText?: string;
        DeliveryPreferenceId?: string;
        CustomAddress?: Address;
        StoreAddress?: StoreProductAvailability;
        ElectronicDeliveryEmail?: string;
        ElectronicDeliveryEmailContent?: string;
    }
    interface DeliveryPreference {
        Value?: string;
        Text?: string;
    }
    interface ImageInfo {
        Url?: string;
        AltText?: string;
    }
    interface TransactionItem {
        LineId: string;
        ItemType?: TransactionItemType;
        ProductDetailsExpanded?: CartProductDetails;
        KitComponents?: TransactionItem[];
        SelectedDeliveryOption?: SelectedDeliveryOption;
        ProductId?: number;
        ProductNumber?: string;
        ItemId?: string;
        VariantInventoryDimensionId?: string;
        Quantity?: number;
        PriceWithCurrency?: string;
        TaxAmountWithCurrency?: string;
        DiscountAmount?: number;
        DiscountAmountWithCurrency?: string;
        NetAmountWithCurrency?: string;
        ShippingAddress?: Address;
        DeliveryModeId?: string;
        DeliveryModeText?: string;
        ElectronicDeliveryEmail?: string;
        PromotionLines?: string[];
        ProductDetails?: string;
        NoOfComponents?: string;
        Color?: string;
        Size?: string;
        Style?: string;
        Name?: string;
        Description?: string;
        ProductUrl?: string;
        Image?: ImageInfo;
        ImageMarkup?: string;
        OfferNames?: string;
        DeliveryPreferences?: DeliveryPreference[];
        AFMVendorId?: string;
        AFMVendorName?: string;
        IsEcommOwned: boolean;
        AFMShippingMode: string;
        AFMCartLineATP: AFMATPResponseItem;
        PriceAfterDiscount: string;
        LineOrderNumber?: string;
        AFMKitItemId: string;
        AFMKitProductId: number;
        AFMKitItemProductDetails: string;
        AFMKitSequenceNumber: number;
        AFMUnit: string;
        AFMKitItemQuantity: number;
        AFMItemType: number;
        Listing?: Listing[];
        AFMMultipleQty?: number;
        AFMLowestQty?: number;
        AFMQtyOptions?: number[];
        AFMQtyPerBox?: number;
    }
    interface LineItems {
        cartItemId?: string;
        LineOrderNumber?: string;
    }
    interface AFMATPItemGroupingResponse {
        AccountNumber: string;
        Items: AFMATPResponseItem[];
        ShipTo: string;
    }
    interface AFMATPResponseItem {
        BeginDateRange?: Date;
        BestDate?: Date;
        EndDateRange?: Date;
        IsAvailable?: boolean;
        ItemId?: string;
        Message?: string;
        Quantity?: number;
        ShippingSpan?: string;
        BeginDateRangeMOD?: Date;
        BestDateMOD?: Date;
        EndDateRangeMOD?: Date;
        MessageMOD?: string;
    }
    interface Listing {
        ListingId?: number;
        ProductDetails?: string;
        KitComponentDetails: KitComponentInfo;
        ItemId?: string;
        AFMKitItemId?: string;
        AFMKitSequenceNumber?: number;
        AFMKitProductId?: number;
        AFMKitItemProductDetails?: string;
        LineId?: string;
        Quantity?: number;
    }
    class ListingClass implements Listing {
        public ListingId: number;
        public ProductDetails: string;
        public KitComponentDetails: KitComponentInfo;
        public ItemId: string;
        public AFMKitItemId: string;
        public AFMKitSequenceNumber: number;
        public AFMKitProductId: number;
        public AFMKitItemProductDetails: string;
        public LineId: string;
        public Quantity: number;
        constructor(o: any);
    }
    interface KitComponentInfo {
        KitLineIdentifier?: string;
        KitLineProductId?: number;
        Unit?: string;
        IsDefaultComponent?: boolean;
        KitProductMasterId?: number;
        Title?: string;
    }
    class KitComponentInfoClass implements KitComponentInfo {
        public KitLineIdentifier: string;
        public KitLineProductId: number;
        public Unit: string;
        public KitProductMasterId: number;
        public Title: string;
        constructor(o: any);
    }
    interface ShoppingCart {
        CartId?: string;
        Name?: string;
        Items?: TransactionItem[];
        LastModifiedDate?: Date;
        CartType?: ShoppingCartType;
        PromotionLines?: string[];
        DiscountCodes?: string[];
        SelectedDeliveryOption?: SelectedDeliveryOption;
        LoyaltyCardId?: string;
        SubtotalWithCurrency?: string;
        EstShipping?: string;
        EstHomeDelivery?: string;
        AvalaraTaxError?: string;
        ATPError?: string;
        CartCount: number;
        CustomerId: string;
        SalesOrderNumber: string;
        Discount?: string;
        ChargeAmountWithCurrency?: string;
        TaxAmountWithCurrency?: string;
        TotalAmountWithCurrency?: string;
        TotalAmount?: number;
        ShippingAddress?: Address;
        DeliveryModeId?: string;
        DeliveryPreferences?: DeliveryPreference[];
        TenderLines: TenderDataLine[];
        EstShippingDiscount?: string;
        EstHomeDeliveryDiscount?: string;
        RsaId?: string;
    }
    interface ShoppingCartResponse {
        ShoppingCart?: ShoppingCart;
        Errors?: Error[];
    }
    interface ShoppingCartCollectionResponse {
        ShoppingCarts?: ShoppingCart[];
        Errors?: Error[];
    }
    interface ShippingOption {
        Id?: string;
        ShippingType?: string;
        Description?: string;
    }
    interface ShippingOptions {
        ShippingOptions?: ShippingOption[];
    }
    interface ItemShippingOptions {
        LineId?: string;
        ShippingOptions?: ShippingOptions;
    }
    interface ShippingOptionResponse {
        OrderShippingOptions?: ShippingOptions;
        ItemShippingOptions?: ItemShippingOptions[];
        Errors?: Error[];
    }
    interface DeliveryOption {
        Id?: string;
        Description?: string;
    }
    interface LineDeliveryOption {
        LineId?: string;
        DeliveryOptions?: DeliveryOption[];
    }
    interface DeliveryOptionsResponse {
        DeliveryOptions?: DeliveryOption[];
        LineDeliveryOptions?: LineDeliveryOption[];
        Errors?: Error[];
        AddressVerficationResult?: boolean;
    }
    interface PaymentCardType {
        Id?: string;
        CardType?: string;
    }
    interface PaymentCardTypesResponse {
        CardTypes?: PaymentCardType[];
        Errors?: Error[];
    }
    interface Address {
        Country?: string;
        State?: string;
        County?: string;
        City?: string;
        DistrictName?: string;
        AttentionTo?: string;
        FirstName?: string;
        LastName?: string;
        Street?: string;
        StreetNumber?: string;
        ZipCode?: string;
        Phone?: string;
        Email?: string;
        EmailContent?: string;
        RecordId?: number;
        Deactivate?: boolean;
        IsPrimary?: boolean;
        AddressType?: AddressType;
        AddressFriendlyName?: string;
        Street2?: string;
        Email2: string;
        Phone2: string;
        Phone3: string;
        Phone1Extension: string;
        Phone2Extension: string;
        Phone3Extension: string;
    }
    interface AddressCollectionResponse {
        Addresses?: Address[];
        Errors?: Error[];
    }
    interface Payment {
        PaymentAddress?: Address;
        CardNumber?: string;
        CardType?: string;
        CCID?: string;
        ExpirationMonth?: number;
        ExpirationYear?: number;
        NameOnCard?: string;
        LoyaltyCardId?: string;
        DiscountCode?: string;
        SynchronyPromoDesc?: string;
        FinancingOptionId?: string;
        MinimumPurchase?: number;
    }
    interface UserCartInfo {
        ShippingAddress?: Address;
        PaymentAddress?: Address;
        LineItems?: LineItems[];
        OrderNumber?: string;
        IsOkToText?: boolean;
        IsOptinDeals: boolean;
    }
    interface AddressState {
        CountryRegion?: string;
        StateCode?: string;
        StateName?: string;
    }
    interface AddressData {
        IsAddressValid?: boolean;
        ErrorMessage?: string;
        suggestedAddress?: Address;
        Street2?: string;
        IsServiceVerified: boolean;
    }
    class AFMAddressData implements AddressData {
        public IsAddressValid: boolean;
        public ErrorMessage: string;
        public suggestedAddress: Address;
        public Street2: string;
        public IsServiceVerified: boolean;
        constructor(o: any);
    }
    interface AFMOrderSummaryData {
        SubtotalWithCurrency: string;
        EstShipping: string;
        EstHomeDelivery: string;
        TaxAmountWithCurrency: string;
        TotalAmountWithCurrency: string;
    }
    class AFMOrderSummary implements AFMOrderSummaryData {
        public SubtotalWithCurrency: string;
        public EstShipping: string;
        public EstHomeDelivery: string;
        public TaxAmountWithCurrency: string;
        public TotalAmountWithCurrency: string;
        constructor(o: any);
    }
    class UserCartData implements UserCartInfo {
        public ShippingAddress: Address;
        public PaymentAddress: Address;
        public LineItems: LineItems[];
        public OrderNumber: string;
        public IsOkToText: boolean;
        public IsOptinDeals: boolean;
        constructor(o: any);
    }
    interface CardTokenData {
        Last4Digits: string;
        ExpirationMonth: string;
        ExpirationYear: string;
        Name: string;
        CardToken: string;
        CardType: string;
        UniqueCardId?: string;
        Error?: string;
        SynchronyPromoDesc?: string;
        FinancingOptionId?: string;
        SynchronyMinPurchase?: number;
        RemoveCartDiscount?: boolean;
    }
    interface PaymentCardResponse {
        PaymentCards?: Payment[];
    }
    interface TokenizedPaymentCard {
        PaymentAddress?: Address;
        CardType?: string;
        ExpirationMonth?: number;
        ExpirationYear?: number;
        NameOnCard?: string;
        CardToken?: string;
        UniqueCardId?: string;
        MaskedCardNumber?: string;
        SynchronyPromoDesc?: string;
        FinancingOptionId?: string;
        MinimumPurchase?: number;
        RemoveCartDiscount?: boolean;
    }
    interface Error {
        ErrorCode?: string;
        ErrorMessage?: string;
    }
    interface CreateSalesOrderResponse {
        OrderNumber?: string;
        Errors?: Error[];
    }
    interface StoreProductAvailabilityItem {
        RecordId?: number;
        ItemId?: string;
        VariantInventoryDimensionId?: string;
        WarehouseInventoryDimensionId?: string;
        InventoryLocationId?: string;
        AvailableQuantity?: number;
        ProductDetails?: string;
    }
    interface StoreProductAvailability {
        ChannelId?: number;
        Latitude?: number;
        Longitude?: number;
        Distance?: string;
        InventoryLocationId?: string;
        StoreId?: string;
        StoreName?: string;
        PostalAddressId?: string;
        Country?: string;
        State?: string;
        County?: string;
        City?: string;
        DistrictName?: string;
        AttentionTo?: string;
        Name?: string;
        Street?: string;
        StreetNumber?: string;
        ZipCode?: string;
        Phone?: string;
        Email?: string;
        EmailContent?: string;
        RecordId?: number;
        Deactivate?: boolean;
        IsPrimary?: boolean;
        AddressType?: AddressType;
        AddressFriendlyName?: string;
        ProductAvailabilities?: StoreProductAvailabilityItem[];
        SelectDisabled?: string;
        AreItemsAvailableInStore?: boolean;
    }
    interface CountryInfo {
        CountryCode?: string;
        CountryName?: string;
    }
    interface ChannelConfiguration {
        GiftCardItemId?: string;
        PickupDeliveryModeCode?: string;
        EmailDeliveryModeCode?: string;
        BingMapsApiKey?: string;
        CurrencyStringTemplate?: string;
    }
    interface StoreLocation {
        ChannelId?: number;
        Latitude?: number;
        Longitude?: number;
        Distance?: string;
        InventoryLocationId?: string;
        StoreId?: string;
        StoreName?: string;
        PostalAddressId?: string;
        Country?: string;
        State?: string;
        County?: string;
        City?: string;
        DistrictName?: string;
        AttentionTo?: string;
        Name?: string;
        Street?: string;
        StreetNumber?: string;
        ZipCode?: string;
        Phone?: string;
        Email?: string;
        EmailContent?: string;
        RecordId?: number;
        Deactivate?: boolean;
        IsPrimary?: boolean;
        AddressType?: AddressType;
        AddressFriendlyName?: string;
    }
    interface TenderDataLine {
        Amount?: string;
        GiftCardId?: string;
        LoyaltyCardId?: string;
        PaymentCard?: Payment;
        TokenizedPaymentCard?: TokenizedPaymentCard;
    }
    interface LineDeliveryPreference {
        LineId?: string;
        DeliveryPreferenceTypes?: DeliveryPreferenceType[];
    }
    interface CartDeliveryPreferences {
        HeaderDeliveryPreferenceTypes?: DeliveryPreferenceType[];
        LineDeliveryPreferences?: LineDeliveryPreference[];
    }
    interface DeliveryPreferenceResponse {
        CartDeliveryPreferences?: CartDeliveryPreferences;
        Errors?: Error[];
    }
    interface BooleanResponse {
        IsTrue?: boolean;
        Errors?: Error[];
    }
    interface StringResponse {
        Value?: string;
        Errors?: Error[];
    }
    interface StoreProductAvailabilityResponse {
        Stores?: StoreProductAvailability[];
        Errors?: Error[];
    }
    interface StoreLocationResponse {
        Stores?: StoreLocation[];
        Errors?: Error[];
    }
    interface ChannelInfoResponse {
        ChannelConfiguration?: ChannelConfiguration;
        Errors?: Error[];
    }
    interface TenderTypesResponse {
        HasCreditCardPayment?: boolean;
        HasGiftCardPayment?: boolean;
        HasLoyaltyCardPayment?: boolean;
        Errors?: Error[];
    }
    interface CountryInfoResponse {
        Countries?: CountryInfo[];
        Errors?: Error[];
    }
    interface GiftCardInformation {
        GiftCardId?: string;
        Balance?: number;
        BalanceWithCurrency?: string;
        CurrencyCode?: string;
        IsInfoAvailable?: boolean;
    }
    interface GiftCardResponse {
        GiftCardInformation?: GiftCardInformation;
        Errors?: Error[];
    }
    interface LoyaltyCard {
        CardNumber?: string;
        CardTenderType?: number;
    }
    interface LoyaltyCardsResponse {
        LoyaltyCards?: LoyaltyCard[];
        Errors?: Error[];
    }
    interface StateProvinceInfo {
        CountryRegionId?: string;
        StateId?: string;
        StateName?: string;
    }
    interface StateProvinceInfoResponse {
        StateProvinces?: StateProvinceInfo[];
        Errors?: Error[];
    }
    class CartProductDetailsClass implements CartProductDetails {
        public Name: string;
        public ProductUrl: string;
        public ProductNumber: string;
        public ImageUrl: string;
        public Description: string;
        public DimensionValues: any;
        public SKU: string;
        public ImageMarkup: string;
        public Quantity: number;
        constructor(o: any);
    }
    class SelectedShippingOptionsClass implements SelectedShippingOptions {
        public DeliveryMethodId: string;
        public DeliveryMethodText: string;
        public ShippingOptionId: string;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: string;
        public ElectronicDeliveryEmailContent: string;
        constructor(o: any);
    }
    class SelectedItemShippingOptionsClass implements SelectedItemShippingOptions {
        public LineId: string;
        public Quantity: number;
        public DeliveryMethodId: string;
        public DeliveryMethodText: string;
        public ShippingOptionId: string;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: string;
        public ElectronicDeliveryEmailContent: string;
        constructor(o: any);
    }
    class SelectedLineDeliveryOptionClass implements SelectedLineDeliveryOption {
        public LineId: string;
        public DeliveryModeId: string;
        public DeliveryModeText: string;
        public DeliveryPreferenceId: string;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: string;
        public ElectronicDeliveryEmailContent: string;
        constructor(o: any);
    }
    class SelectedDeliveryOptionClass implements SelectedDeliveryOption {
        public DeliveryModeId: string;
        public DeliveryModeText: string;
        public DeliveryPreferenceId: string;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: string;
        public ElectronicDeliveryEmailContent: string;
        constructor(o: any);
    }
    class ImageInfoClass implements ImageInfo {
        public Url: string;
        public AltText: string;
        constructor(o: any);
    }
    class TransactionItemClass implements TransactionItem {
        public LineId: string;
        public ItemType: TransactionItemType;
        public KitComponents: TransactionItemClass[];
        public SelectedDeliveryOption: SelectedDeliveryOption;
        public ProductId: number;
        public ProductNumber: string;
        public ItemId: string;
        public VariantInventoryDimensionId: string;
        public Quantity: number;
        public PriceWithCurrency: string;
        public TaxAmountWithCurrency: string;
        public DiscountAmount: number;
        public DiscountAmountWithCurrency: string;
        public NetAmountWithCurrency: string;
        public ShippingAddress: Address;
        public DeliveryModeId: string;
        public DeliveryModeText: string;
        public ElectronicDeliveryEmail: string;
        public PromotionLines: string[];
        public ProductDetailsExpanded: CartProductDetails;
        public ProductDetails: string;
        public NoOfComponents: string;
        public Color: string;
        public Size: string;
        public Style: string;
        public Name: string;
        public Description: string;
        public ProductUrl: string;
        public Image: ImageInfo;
        public ImageMarkup: string;
        public OfferNames: string;
        public DeliveryPreferences: DeliveryPreference[];
        public AFMVendorId: string;
        public AFMVendorName: string;
        public AFMShippingMode: string;
        public IsEcommOwned: boolean;
        public AFMCartLineATP: AFMATPResponseItem;
        public PriceAfterDiscount: string;
        public LineOrderNumber: string;
        public AFMKitItemId: string;
        public AFMKitProductId: number;
        public AFMKitItemProductDetails: string;
        public AFMKitSequenceNumber: number;
        public AFMUnit: string;
        public AFMKitItemQuantity: number;
        public AFMItemType: number;
        public Listing: Listing[];
        public AFMMultipleQty: number;
        public AFMLowestQty: number;
        public AFMQtyOptions: number[];
        public AFMQtyPerBox: number;
        constructor(o: any);
    }
    class ShoppingCartClass implements ShoppingCart {
        public CartId: string;
        public Name: string;
        public Items: TransactionItem[];
        public LastModifiedDate: Date;
        public CartType: ShoppingCartType;
        public PromotionLines: string[];
        public DiscountCodes: string[];
        public SelectedDeliveryOption: SelectedDeliveryOption;
        public LoyaltyCardId: string;
        public SubtotalWithCurrency: string;
        public DiscountAmountWithCurrency: string;
        public ChargeAmountWithCurrency: string;
        public TaxAmountWithCurrency: string;
        public TotalAmountWithCurrency: string;
        public TotalAmount: number;
        public EstShipping: string;
        public EstHomeDelivery: string;
        public AvalaraTaxError: string;
        public ATPError: string;
        public CartCount: number;
        public CustomerId: string;
        public SalesOrderNumber: string;
        public GrandTotal: string;
        public ShippingAddress: Address;
        public DeliveryModeId: string;
        public TenderLines: TenderDataLine[];
        public EstShippingDiscount: string;
        public EstHomeDeliveryDiscount: string;
        public RsaId: string;
        constructor(o: any);
    }
    class ShoppingCartResponseClass implements ShoppingCartResponse {
        public ShoppingCart: ShoppingCart;
        public Errors: Error[];
        constructor(o: any);
    }
    class ShoppingCartCollectionResponseClass implements ShoppingCartCollectionResponse {
        public ShoppingCarts: ShoppingCart[];
        public Errors: Error[];
        constructor(o: any);
    }
    class ShippingOptionClass implements ShippingOption {
        public Id: string;
        public ShippingType: string;
        public Description: string;
        constructor(o: any);
    }
    class ShippingOptionsClass implements ShippingOptions {
        public ShippingOptions: ShippingOption[];
        constructor(o: any);
    }
    class ItemShippingOptionsClass implements ItemShippingOptions {
        public LineId: string;
        public ShippingOptions: ShippingOptions;
        constructor(o: any);
    }
    class ShippingOptionResponseClass implements ShippingOptionResponse {
        public OrderShippingOptions: ShippingOptions;
        public ItemShippingOptions: ItemShippingOptions[];
        public Errors: Error[];
        constructor(o: any);
    }
    class LineDeliveryPreferenceClass implements LineDeliveryPreference {
        public LineId: string;
        public DeliveryPreferenceTypes: DeliveryPreferenceType[];
        constructor(o: any);
    }
    class CartDeliveryPreferencesClass implements CartDeliveryPreferences {
        public HeaderDeliveryPreferenceTypes: DeliveryPreferenceType[];
        public LineDeliveryPreferences: LineDeliveryPreference[];
        constructor(o: any);
    }
    class DeliveryOptionClass implements DeliveryOption {
        public Id: string;
        public Description: string;
        constructor(o: any);
    }
    class LineDeliveryOptionClass implements LineDeliveryOption {
        public LineId: string;
        public DeliveryOptions: DeliveryOption[];
        constructor(o: any);
    }
    class DeliveryOptionsResponseClass implements DeliveryOptionsResponse {
        public DeliveryOptions: DeliveryOption[];
        public LineDeliveryOptions: LineDeliveryOption[];
        public Errors: Error[];
        public AddressVerficationResult: boolean;
        constructor(o: any);
    }
    class PaymentCardTypeClass implements PaymentCardType {
        public Id: string;
        public CardType: string;
        public Errors: Error[];
        constructor(o: any);
    }
    class PaymentCardTypesResponseClass implements PaymentCardTypesResponse {
        public CardTypes: PaymentCardType[];
        public Errors: Error[];
        constructor(o: any);
    }
    class AddressClass implements Address {
        public Country: string;
        public State: string;
        public County: string;
        public City: string;
        public DistrictName: string;
        public AttentionTo: string;
        public Name: string;
        public FirstName: string;
        public LastName: string;
        public Street: string;
        public Street2: string;
        public StreetNumber: string;
        public ZipCode: string;
        public Phone: string;
        public Email: string;
        public EmailContent: string;
        public RecordId: number;
        public Deactivate: boolean;
        public IsPrimary: boolean;
        public AddressType: AddressType;
        public AddressFriendlyName: string;
        public Email2: string;
        public Phone2: string;
        public Phone3: string;
        public Phone1Extension: string;
        public Phone2Extension: string;
        public Phone3Extension: string;
        constructor(o: any);
    }
    class AddressCollectionResponseClass implements AddressCollectionResponse {
        public Addresses: Address[];
        public Errors: Error[];
        constructor(o: any);
    }
    class PaymentClass implements Payment {
        public PaymentAddress: Address;
        public CardNumber: string;
        public CardType: string;
        public CCID: string;
        public ExpirationMonth: number;
        public ExpirationYear: number;
        public NameOnCard: string;
        public LoyaltyCardId: string;
        public DiscountCode: string;
        public Last4Digits: string;
        public SynchronyPromoDesc: string;
        public FinancingOptionId: string;
        public MinimumPurchase: number;
        public RemoveCartDiscount: boolean;
        constructor(o: any);
    }
    class PaymentCardResponseClass implements PaymentCardResponse {
        public PaymentCards: Payment[];
        constructor(o: any);
    }
    class TokenizedPaymentCardClass implements TokenizedPaymentCard {
        public PaymentAddress: Address;
        public CardType: string;
        public ExpirationMonth: number;
        public ExpirationYear: number;
        public NameOnCard: string;
        public CardToken: string;
        public UniqueCardId: string;
        public MaskedCardNumber: string;
        public SynchronyPromoDesc: string;
        public FinancingOptionId: string;
        public MinimumPurchase: number;
        public RemoveCartDiscount: boolean;
        constructor(o: any);
    }
    class ErrorClass implements Error {
        public ErrorCode: string;
        public ErrorMessage: string;
        constructor(o: any);
    }
    class CreateSalesOrderResponseClass implements CreateSalesOrderResponse {
        public OrderNumber: string;
        public Errors: Error[];
        constructor(o: any);
    }
    class StoreProductAvailabilityItemClass implements StoreProductAvailabilityItem {
        public RecordId: number;
        public ItemId: string;
        public VariantInventoryDimensionId: string;
        public WarehouseInventoryDimensionId: string;
        public InventoryLocationId: string;
        public AvailableQuantity: number;
        public ProductDetails: string;
        constructor(o: any);
    }
    class StoreProductAvailabilityClass implements StoreProductAvailability {
        public ChannelId: number;
        public Latitude: number;
        public Longitude: number;
        public Distance: string;
        public InventoryLocationId: string;
        public StoreId: string;
        public StoreName: string;
        public PostalAddressId: string;
        public ProductAvailabilities: StoreProductAvailabilityItem[];
        public SelectDisabled: string;
        public Country: string;
        public State: string;
        public County: string;
        public City: string;
        public DistrictName: string;
        public AttentionTo: string;
        public Name: string;
        public Street: string;
        public StreetNumber: string;
        public ZipCode: string;
        public Phone: string;
        public Email: string;
        public EmailContent: string;
        public RecordId: number;
        public Deactivate: boolean;
        public IsPrimary: boolean;
        public AddressType: AddressType;
        public AddressFriendlyName: string;
        public AreItemsAvailableInStore: boolean;
        constructor(o: any);
    }
    class StoreLocationClass implements StoreLocation {
        public ChannelId: number;
        public Latitude: number;
        public Longitude: number;
        public Distance: string;
        public InventoryLocationId: string;
        public StoreId: string;
        public StoreName: string;
        public PostalAddressId: string;
        public Country: string;
        public State: string;
        public County: string;
        public City: string;
        public DistrictName: string;
        public AttentionTo: string;
        public Name: string;
        public Street: string;
        public StreetNumber: string;
        public ZipCode: string;
        public Phone: string;
        public Email: string;
        public EmailContent: string;
        public RecordId: number;
        public Deactivate: boolean;
        public IsPrimary: boolean;
        public AddressType: AddressType;
        public AddressFriendlyName: string;
        constructor(o: any);
    }
    class CountryInfoClass implements CountryInfo {
        public CountryCode: string;
        public CountryName: string;
        constructor(o: any);
    }
    class TenderDataLineClass implements TenderDataLine {
        public Amount: string;
        public GiftCardId: string;
        public LoyaltyCardId: string;
        public PaymentCard: Payment;
        public TokenizedPaymentCard: TokenizedPaymentCard;
        constructor(o: any);
    }
    class DeliveryPreferenceResponseClass implements DeliveryPreferenceResponse {
        public CartDeliveryPreferences: CartDeliveryPreferences;
        public Errors: Error[];
        constructor(o: any);
    }
    class StoreProductAvailabilityResponseClass implements StoreProductAvailabilityResponse {
        public Stores: StoreProductAvailability[];
        public Errors: Error[];
        constructor(o: any);
    }
    class StoreLocationResponseClass implements StoreLocationResponse {
        public Stores: StoreLocation[];
        public Errors: Error[];
        constructor(o: any);
    }
    class CountryInfoResponseClass implements CountryInfoResponse {
        public Countries: CountryInfo[];
        public Errors: Error[];
        constructor(o: any);
    }
    class BooleanResponseClass implements BooleanResponse {
        public IsTrue: boolean;
        public Errors: Error[];
        constructor(o: any);
    }
    class StringResponseClass implements StringResponse {
        public Value: string;
        public Errors: Error[];
        constructor(o: any);
    }
    class TenderTypesResponseClass implements TenderTypesResponse {
        public HasCreditCardPayment: boolean;
        public HasGiftCardPayment: boolean;
        public HasLoyaltyCardPayment: boolean;
        public Errors: Error[];
        constructor(o: any);
    }
    class GiftCardInformationClass implements GiftCardInformation {
        public GiftCardId: string;
        public Balance: number;
        public CurrencyCode: string;
        public IsInfoAvailable: boolean;
        constructor(o: any);
    }
    class GiftCardResponseClass implements GiftCardResponse {
        public GiftCardInformation: GiftCardInformation;
        public Errors: Error[];
        constructor(o: any);
    }
    class LoyaltyCardClass implements LoyaltyCard {
        public CardNumber: string;
        public CardTenderType: number;
        constructor(o: any);
    }
    class LoyaltyCardsResponseClass implements LoyaltyCardsResponse {
        public LoyaltyCards: LoyaltyCard[];
        public Errors: Error[];
        constructor(o: any);
    }
    class StateProvinceInfoClass implements StateProvinceInfo {
        public CountryRegionId: string;
        public StateId: string;
        public StateName: string;
        constructor(o: any);
    }
    class StateProvinceInfoResponseClass implements StateProvinceInfoResponse {
        public StateProvinces: StateProvinceInfo[];
        public Errors: Error[];
        constructor(o: any);
    }
    interface AFMOrderConfirmationResponse {
        ShoppingCart: ShoppingCart;
        ShippingAddress: Address;
        BillingAddress: Address;
    }
    class AFMOrderConfirmationResponseClass implements AFMOrderConfirmationResponse {
        public ShoppingCart: ShoppingCart;
        public ShippingAddress: Address;
        public BillingAddress: Address;
        constructor(o: any);
    }
}
declare module AFM.Ecommerce.Controls {
    var ResourceStrings: {};
    var ExternalResourceStrings: any;
    var Resources: any;
    class ResourcesHandler {
        private static uiCultureKey;
        static selectUICulture(): void;
    }
}
declare module AFM.Ecommerce.Controls {
    var CartData: any;
    var errorMessage: string;
    var errorPanel: JQuery;
    var CardToken: Observable<string>;
    var TenderLinesData: TenderDataLine[];
    var AFMOrderSummaryData: AFMOrderSummary;
    var PaymentCardData: any;
    var IsConfirmedit: boolean;
    var AFMZipCodeData: Observable<string>;
    var ErrorCount: number;
    var SynchronyMinPurchaseAmount: Observable<number>;
    class CartBase {
        static _cartView: JQuery;
        static _loadingDialog: any;
        static _loadingText: any;
        static CreateCartData(): void;
        static GetUserPageMode(): void;
        static LoadBaseData(): void;
        static LoadInitialData(): void;
        static DataModelLoad(): void;
        private static HideAllControls();
        static UpdateCartData(cartData: any): void;
        static ClearPageMode(): void;
        static UpdateFocusPageModeValue(pageMode: PageModeOptions): void;
        static OnClearPageMode(callerContext: any, handler: any): void;
        static OnPageModeSubscribers(callerContext: any, handler: any): void;
        static OnUpdateFocusPageMode(pageMode: PageModeOptions, callerContext: any, handler: any): void;
        static showError(isError: boolean): void;
        static showInfo(isInfo: boolean): void;
        static FindPageMode(pageMode: any): any;
        static CreateDDO(orderNumber: any): void;
    }
}
declare module AFM.Ecommerce.Controls {
    class CookieManager {
        private static afmZipCode;
        private static shoppingcartId;
        static getCookie(cookieName: string): string;
        static setSessionCookie(cname: any, cvalue: any): void;
        static setCookie(cname: any, cvalue: any, exdays: any): void;
    }
}
declare function findBrowser(): string;
declare function get_hostname(url: any): any;
declare var msaxError: {
    show: (level: any, message: any, errorCodes?: any) => void;
};
declare module msaxValues {
    var msax_CurrencyTemplate: any;
    var msax_CheckoutServiceUrl: any;
    var msax_ShoppingCartServiceUrl: any;
    var msax_OrderConfirmationUrl: any;
    var msax_IsDemoMode: any;
    var msax_DemoDataPath: any;
    var msax_StoreProductAvailabilityServiceUrl: any;
    var msax_ChannelServiceUrl: any;
    var msax_LoyaltyServiceUrl: any;
    var msax_CustomerServiceUrl: any;
    var msax_HasInventoryCheck: any;
    var msax_CheckoutUrl: any;
    var msax_IsCheckoutCart: any;
    var msax_ContinueShoppingUrl: any;
    var msax_CartDiscountCodes: any;
    var msax_CartLoyaltyReward: any;
    var msax_ShoppingCartUrl: any;
    var msax_CartDisplayPromotionBanner: any;
    var msax_ReviewDisplayPromotionBanner: any;
    var msax_AFMExtendedServiceUrl: any;
    var msax_CurrentPageMode: any;
    var msax_TransactionId: any;
    var msax_ViewTax: any;
    var msax_ViewDiscount: any;
    var msax_ShoppingCartReadMode: any;
    var msax_IsEditable: any[];
    var msax_PageMode: AFM.Ecommerce.Controls.PageModeOptions;
    var msax_LocalPageMode: AFM.Ecommerce.Controls.PageMode;
    var msax_LoginMode: any;
    var msax_SigInUrl: any;
    var msax_GoBackUrl: any;
    var msax_ErrorPageUrl: any;
    var msax_SurveyTermsAndConditions: any;
    var msax_IsAcceptTermsAndConditions: AFM.Ecommerce.Controls.AFMSalesTransHeader;
    var msax_MinimumPurchaseSynchronyCart: any;
    var msax_Synchronycart: any;
}
declare module AFM.Maps {
    var Map: any;
    var loadModule: any;
    var Search: any;
    var Events: any;
    var Pushpin: any;
    var Infobox: any;
    var Point: any;
}
declare module AFM.Ecommerce.Controls {
    interface Parameter {
        key: string;
        value: boolean;
    }
    class AjaxProxy {
        private relativeUrl;
        constructor(relativeUrl: any);
        private ajaxErrorHandler(e, xhr, settings);
        public SubmitRequest: (webMethod: any, data: any, successCallback: any, errorCallback: any) => void;
    }
    class LoadingOverlay {
        static opts: {
            lines: number;
            length: number;
            width: number;
            radius: number;
            speed: number;
            color: string;
            hwaccel: boolean;
            shadow: boolean;
            className: string;
            delay: number;
            backdrop: string;
            backdropOpacity: number;
            position: string;
            stopOnAjaxError: boolean;
            stopOnReveal: boolean;
        };
        static spinner: any;
        static CreateLoadingDialog(loadingDialog: any, width: number, height: number): void;
        static ShowLoadingDialog(loadingDialog: any, loadingText: any, text?: string): void;
        static CloseLoadingDialog(loadingDialog: any): void;
        static CreateRemoveItemDialog(loadingDialog: any, callerContext: any, handler: any, callerContextCancel: any, handlerCancel: any, RemoveButtonText: string, CancelButtonText: string): void;
        static openAddressDialog(loadingDialog: any, handler: any, handlerCancel: any, callerContext: any, htmlContent: any, UseAddressButtonText: string, CancelButtonText: string): void;
        static openRemoveItemDialog(loadingDialog: any, dialogText: any, text: string): void;
        static closeRemoveItemDialog(loadingDialog: any): void;
        static openAddressItemDialog(loadingDialog: any): void;
    }
}
declare module AFM {
    class Utils {
        static isNullOrUndefined(o: any): boolean;
        static isNullOrEmpty(o: any): boolean;
        static isNullOrWhiteSpace(o: string): boolean;
        static hasElements(o: any[]): boolean;
        static getValueOrDefault(o: string, defaultValue: any): string;
        static hasErrors(o: any): boolean;
        static format(object: string, ...params: any[]): string;
        static CurrencyString(doubleValue: any, usa: any): string;
        static xsep(num: any, sep: any): string;
        static indexOf(needle: any, property: any): any;
        static Trim(x: any): any;
        static SetPhoneFormat(x: any): any;
        static RemovePhoneFormat(x: any): any;
    }
}
declare module AFM.Ecommerce.Controls {
    interface IAsyncResult<T> {
        done(callback: (result: T) => void): IAsyncResult<T>;
        fail(callback: (error: any[]) => void): IAsyncResult<T>;
    }
    class AsyncResult<T> implements IAsyncResult<T> {
        private _result;
        private _errors;
        private _succeded;
        private _failed;
        private _callerContext;
        private _successCallbacks;
        private _errorCallbacks;
        constructor();
        public resolve(result: T): void;
        public reject(errors: any[]): void;
        public done(callback: (result: T) => void): IAsyncResult<T>;
        public fail(callback: (errors: any[]) => void): IAsyncResult<T>;
    }
    class FunctionQueueHelper {
        static callFunctions(functionQueue: Function[], callerContext: any, data?: any): void;
        static queueFunction(functionQueue: Function[], callback: Function): void;
    }
}
declare module AFM.Ecommerce.Controls {
    class ChannelService {
        private static proxy;
        static GetProxy(): void;
        static GetChannelConfiguration(): AsyncResult<ChannelInfoResponse>;
        static GetTenderTypes(): AsyncResult<TenderTypesResponse>;
        static GetCountryRegionInfo(): AsyncResult<CountryInfoResponse>;
        static GetStateProvinceInfo(countryCode: string): AsyncResult<StateProvinceInfoResponse>;
    }
}
declare module AFM.Ecommerce.Controls {
    class CustomerService {
        private static proxy;
        static GetProxy(): void;
        static GetAddresses(): AsyncResult<AddressCollectionResponse>;
    }
}
declare module AFM.Ecommerce.Controls {
    class LoyaltyService {
        private static proxy;
        static GetProxy(): void;
        static GetLoyaltyCards(): AsyncResult<LoyaltyCardsResponse>;
        static GetAllLoyaltyCardsStatus(): void;
        static GetLoyaltyCardStatus(loyaltyCardNumber: string): void;
        static UpdateLoyaltyCardId(isCheckoutSession: boolean, loyaltyCardId: string, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
    }
}
declare module AFM.Ecommerce.Controls {
    class StoreProductAvailabilityService {
        private static proxy;
        static GetProxy(): void;
        static GetNearbyStoresWithAvailability(latitude: number, longitude: number, items: TransactionItem[]): AsyncResult<StoreProductAvailabilityResponse>;
        static GetNearbyStores(latitude: number, longitude: number): AsyncResult<StoreLocationResponse>;
    }
}
declare module AFM.Ecommerce.Controls {
    class ButtonControls {
        private cart;
        private pageElement;
        private isShoppingCartEnabled;
        private ProceedtoCartButtons;
        private SubmitOrderButtons;
        private SubmitOrderDisable;
        private CheckoutDisable;
        private PreviousButton;
        public paymentCard: any;
        public tenderLines: any;
        public orderNumber: any;
        private headerValues;
        private IsPageModeSubscribersCalled;
        public orderReviewPageMode: any;
        constructor(element: any);
        private PageModeSubscribers(event, data);
        private UpdateShoppingCart(event, data);
        private UpdateCheckoutCart(event, data);
        private UpdateShoppingCartData(data);
        private ClearPageMode(event, data);
        private CartViewPageMode(event, data);
        private ConfirmViewPageMode(event, data);
        private ConfirmViewCompletedPageMode(event, data);
        private shoppingCartNextClick(viewModel, event);
        private continueShoppingClick(viewModel, event);
        private commenceCheckout();
        private reviewPreviousClick();
        private submitOrder();
        private EnableSubmitOrder();
        private DisableSubmitOrder();
    }
}
declare module AFM.Ecommerce.Controls {
    class CartMode {
        private ShippingBtn;
        private BillingBtn;
        private PaymentBtn;
        private ConfirmBtn;
        private pageHeader;
        private isShippingModePaased;
        private isBillinggModePaased;
        private isPaymentModePaased;
        private isConfirmModePaased;
        private IsPageModeSubscribersCalled;
        constructor(element: any);
        private PageModeSubscribers(event, data);
        private CallGoBackButton();
        private updateShoppingCart(event, data);
        private ShippingViewPageMode(event, data);
        private ShippingViewCompletedPageMode(event, data);
        private BillingViewPageMode(event, data);
        private BillingViewCompletedPageMode(event, data);
        private PaymentViewPageMode(event, data);
        private PaymentViewCompletedPageMode(event, data);
        private ConfirmViewPageMode(event, data);
        private ConfirmViewCompletedPageMode(event, data);
        private ShippingBtnActive();
        private BillingBtnActive();
        private ConfirmBtnActive();
        private ShippingButton();
        private BillingButton();
        private PaymentButton();
        private ConfirmButton();
    }
}
declare module AFM.Ecommerce.Controls {
    class CreateAddress {
        private isShoppingCartEnabled;
        public tempFirstName: any;
        public tempLastName: any;
        public tempStreet: any;
        public tempStreet2: any;
        public tempCity: any;
        public tempState: any;
        public tempCountry: any;
        public tempZipCode: any;
        public tempEmail: any;
        public tempEmail2: any;
        public tempPhone: any;
        public tempPhone2: any;
        public tempPhone3: any;
        public tempAddressType: any;
        public tempRecordId: any;
        public suggestedAddressText: any;
        public billingAddressFill: any;
        public tempBillingRecordId: any;
        public tempBillingAddressType: any;
        public paymentCard: any;
        public availableDeliveryMethods: any;
        public orderShippingOptions: any;
        public isBillingAddressSameAsShippingAddress: any;
        public isOptinDeals: any;
        public tempBillingFirstName: any;
        public tempBillingLastName: any;
        public tempBillingStreet: any;
        public tempBillingStreet2: any;
        public tempBillingCity: any;
        public tempBillingState: any;
        public tempBillingCountry: any;
        public tempBillingZipcode: any;
        public tempBillingEmail: any;
        public tempBillingPhone: any;
        public isOktoText: any;
        public ShippingconfirmEmailValue: any;
        public BillingconfirmEmailValue: any;
        public shippingAddressCollectionView: any;
        public shippingAddressView: any;
        public paymentAddressView: any;
        public AFMZipCode: any;
        public userCartInfo: any;
        public ShippingAddress: any;
        public PaymentAddress: any;
        public AvailableAddressCollection: any;
        public selectedAddress: any;
        public CartMode: any;
        public _CountryInfoClass: any;
        public _CountryInfoClassList: any;
        public IsEditAllowed: any;
        public IsCartInfoLoaded: any;
        public isOptinDealsVisiable: any;
        private addressConfirmDialog;
        private isShippingFromDialog;
        private avaSuggestedAddress;
        public _StateInfoClassList: any;
        public IsPageModeSubscribersCalled: any;
        constructor(element: any);
        private PageModeSubscribers(event, data);
        private preventKeyPress(data, event);
        private ReplacerSuggestedAddress();
        private CancelSuggested();
        private updateShoppingCart(event, data);
        private LoadAddressState();
        private ClearPageMode(event, data);
        private HideAddressFieldErrors();
        private ShippingViewPageMode(event, data);
        private ShippingViewCompletedPageMode(event, data);
        private BillingViewPageMode(event, data);
        private BillingViewCompletedPageMode(event, data);
        private ConfirmViewPageMode(event, data);
        private ConfirmViewCompletedPageMode(event, data);
        private UpdateShippingAddressfromAvailableAddresses(data);
        private GetAddressCollection(IsShipping);
        private resetDeliveryMethods();
        private getUserCartInfo();
        private UseAddressButton(data);
        private validatePhoneTextBox(element, valueAccessor);
        private validateBillingPhoneTextBox(element, valueAccessor);
        private validateEmail(element, valueAccessor);
        private validateAddressName(srcElement);
        private validateEmail2(element, valueAccessor);
        private validateAltPhoneTextBox(element, valueAccessor);
        private validateInputOnBlur(element, valueAccessor);
        private showZipCodeMasterError();
        private hideZipCodeMasterError();
        private paymentInformationNextClick();
        private SetUserCartDataInfowithRedirect(userCartInfo);
        private paymentInformationPreviousClick();
        private shippingOptionsNextClick();
        private shippingOptionsPreviousClick();
        private getDeliveryMethods();
        private syncDeliveryOptionsNextClick();
        private selectShippingOption(selectedShippingOption);
        private setShippingOptions();
        private updateSelectedShippingOptions(cart);
        private updateZipCodeInfo();
        private getZipCodeInfo();
        private loadXMLDoc(filename);
        private autoFillCheckout();
        private validateShippingConfirmEmail(srcElement);
        private validateBillingConfirmEmail(srcElement);
        private validateShippingConfirmEmailTextBox(element, valueAccessor);
        private validateBillingConfirmEmailTextBox(selement, valueAccessor);
        private InitateTempAddressDetails();
    }
}
declare module AFM.Ecommerce.Controls {
    class Loader {
        constructor();
    }
}
declare module AFM.Ecommerce.Controls {
    var showNavigatingAwayPopUp: any;
    var navigateAway: any;
    class OrderDetail {
        private cart;
        private isShoppingCartEnabled;
        private isEditableEnabled;
        private findParent;
        private AddressView;
        private ShippingAddress;
        private PaymentAddress;
        private paymentCard;
        public tenderLines: TenderDataLine[];
        private cardToken;
        private IsPageModeSubscribersCalled;
        private IsShoppingCartHasError;
        private IsMinimumPurchase;
        public orderConfirmPageMode: any;
        public thankYouPageMode: any;
        private ShoppingCartView;
        constructor(element: any);
        private PageModeSubscribers(event, data);
        private checkEditMode();
        private updateShoppingCart(event, data);
        private EditShippingClick();
        private EditBillingDetailClick();
        private EditPaymentTyepClick();
        private ClearPageMode(event, data);
        private ConfirmViewPageMode(event, data);
        private ConfirmViewCompletedPageMode(event, data);
        private ThankYouPageMode(event, data);
        private getUserCartInfo();
        private getUserCartInfoForThankyouPage();
        private getCardTokenData();
        public LoadInitialData(): void;
        private LoadTempPaymentData();
    }
}
declare module AFM.Ecommerce.Controls {
    class OrderSummary {
        private cart;
        private isShoppingCartEnabled;
        private showTax;
        private isDiscountViewEnabled;
        private pageElement;
        private estimateShippingLable;
        private estimateHDLabel;
        private estimateTotalLable;
        private OrderSummaryView;
        private IsPageModeSubscribersCalled;
        private IsShoppingCartHasError;
        public ShoppingCartPageMode: any;
        constructor(element: any);
        private PageModeSubscribers(event, data);
        private updateShoppingCart(event, data);
        private LoadTax();
        private ConfirmViewPageMode(event, data);
        private ConfirmViewCompletedPageMode(event, data);
        private BillingViewPageMode(event, data);
        private BillingViewCompletedPageMode(event, data);
        private ShippingViewPageMode(event, data);
        private ShippingViewCompletedPageMode(event, data);
        private ThankYouPageMode(event, data);
        private CartViewPageMode(event, data);
        private ClearPageMode(event, data);
        private CheckforDiscountDisplay();
    }
}
declare module AFM.Ecommerce.Controls {
    class PromotionCode {
        public popupStatus: any;
        private cart;
        public PromotionCode: any;
        private isPromotionCodesEnabled;
        private isShoppingCartEnabled;
        private DiscountCodes;
        private PromotionCodeTextBox;
        private PromotionCodeView;
        private IsPageModeSubscribersCalled;
        constructor(element: any);
        private ClearPageMode(event, data);
        private PageModeSubscribers(event, data);
        private CartViewPageMode(event, data);
        private updateShoppingCart(event, data);
        private preventKeyPress(data, event);
        private updatePromotionCodeTextBoxChanged(viewModel, valueAccesor);
        private getPromotions();
        private applyPromotionCode(cart, valueAccesor);
        private removePromotionCode(code, valueAccesor);
        private updatePromoCodeButton();
        private disablePopup();
        private showErrorPanel(isError, error);
    }
}
declare module AFM.Ecommerce.Controls {
    class RsaId {
        private cart;
        public rsaId: any;
        public RSAID: any;
        private hasRsaId;
        private isShoppingCartEnabled;
        private RsaIdTextBox;
        private RsaIdView;
        private RsaEditMode;
        private IsPageModeSubscribersCalled;
        constructor(element: any);
        private updateShoppingCart(event, data);
        private ClearPageMode(event, data);
        private PageModeSubscribers(event, data);
        private CartViewPageMode(event, data);
        private preventKeyPress(data, event);
        private updateRsaIdTextBoxChanged(viewModel, valueAccesor);
        private applyRsaId(cart, valueAccesor);
        private removeRsaId(cart, valueAccesor);
        private editRsaId(cart, valueAccesor);
        private showErrorPanel(isError, error);
    }
}
declare module AFM.Ecommerce.Controls {
    class Cart {
        private cart;
        private _editRewardCardDialog;
        private isShoppingCartEnabled;
        private isOrderConfirmationenabled;
        private kitCartItemTypeValue;
        private dialogOverlay;
        private supportDiscountCodes;
        private supportLoyaltyReward;
        private displayPromotionBanner;
        private mypageMode;
        private ShoppingCartView;
        private CheckoutDisable;
        private SubmitOrderDisable;
        private removeItemConfirmDialog;
        public LineItems: any;
        public orderNumber: any;
        public IsReadOnly: any;
        public currentCartMode: any;
        private itemtobeRemove;
        private isLoggedIn;
        private IsDisplayWishList;
        private IsSynchronyCart;
        private IsSynchronyPaymentCart;
        private CheckSynchronyCart;
        private CheckSynchronyPaymentCart;
        public AFMZipCode: any;
        public IsPageModeSubscribersCalled: any;
        private shoppingcartresponseTemp;
        private isConfimMode;
        public ShoppingCartPageMode: any;
        constructor(element: any);
        private PageModeSubscribers(event, data);
        private ClearPageMode(event, data);
        private CartViewPageMode(event, data);
        private ThankYouPageMode(event, data);
        private ConfirmViewPageMode(event, data);
        private ConfirmViewCompletedPageMode(event, data);
        private addToWishList(item);
        private getResx(key);
        private quantityMinusClick(item);
        private quantityPlusClick(item);
        private quantityTextBoxChanged(item, valueAccesor);
        private checkItemQuantity();
        private quantityDropDownChanged(item, valueAccesor);
        private updateQuantityonClick(item);
        private editRewardCardOverlayClick();
        private createEditRewardCardDialog();
        private showEditRewardCardDialog();
        private closeEditRewardCardDialog();
        private UpdateShoppingCart(event, data);
        private UpdateCheckoutCart(event, data);
        private UpdateShoppingCartData(data);
        private DisableSubmitOrderProcessdButton();
        private EnableSubmitOrderProcessdButton();
        private getShoppingCart();
        private getPromotions();
        private removeFromCartClick(item);
        private RemoveItemCancel();
        private RemoveItemFromCart();
        private updateQuantity(items);
        private updateLoyaltyCardId();
        private updateZipCodeInfo();
        private getZipCodeInfo();
        private continueShoppingClick();
        private signInClick();
    }
}
declare module AFM.Ecommerce.Controls {
    class ZipCode {
        private cart;
        private isShoppingCartEnabled;
        public AFMZipCode: any;
        public ZipCodeValue: any;
        public popupStatus: any;
        public currentCartMode: any;
        private IsPageModeSubscribersCalled;
        constructor(element: any);
        private PageModeSubscribers(event, data);
        private CartViewPageMode(event, data);
        private ShippingViewPageMode(event, data);
        private ShippingViewCompletedPageMode(event, data);
        private UpdateShoppingCart(event, data);
        private updateZipCodeInfo();
        private getZipCodeInfo();
        private updateZipCodeButton();
        private updateZipCodeonChange();
        private disablePopup();
        private showErrorPane(isError);
    }
}
declare module AFM.Ecommerce.Controls {
}
declare module AFM.Ecommerce.Controls {
}
declare module AFM.Ecommerce.Controls {
    interface IFieldValidatorParams {
        maxLength?: number;
        max?: number;
        min?: number;
        pattern?: string;
        required?: boolean;
        title?: string;
    }
    class FieldValidator {
        private _validationAttributes;
        constructor(params: IFieldValidatorParams);
        public setValidationAttributes(element: Element, addressType: string): void;
        public setTitleAttributeIfInvalid(element: Element, addressType: string): void;
    }
    class EntityValidatorBase {
        constructor();
        public setValidationAttributes(element: Element, fieldName: string, addressType: string): void;
    }
    class ShoppingCartItemValidator extends EntityValidatorBase {
        public Quantity: IFieldValidatorParams;
        constructor();
    }
    class PromotionValidator extends EntityValidatorBase {
        public Promotioncode: IFieldValidatorParams;
        constructor();
    }
    class SelectedOrderShippingOptionsValidator extends EntityValidatorBase {
        public CartId: IFieldValidatorParams;
        public DeliveryMethodId: IFieldValidatorParams;
        public DeliveryMethodText: IFieldValidatorParams;
        public ShippingOptionId: IFieldValidatorParams;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: IFieldValidatorParams;
        public ElectronicDeliveryEmailContent: IFieldValidatorParams;
        constructor();
    }
    class SelectedOrderDeliveryOptionValidator extends EntityValidatorBase {
        public DeliveryModeId: IFieldValidatorParams;
        public DeliveryModeText: IFieldValidatorParams;
        public DeliveryPreferenceId: IFieldValidatorParams;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: IFieldValidatorParams;
        public ElectronicDeliveryEmailContent: IFieldValidatorParams;
        constructor();
    }
    class CustomerValidator extends EntityValidatorBase {
        public FirstName: IFieldValidatorParams;
        public MiddleName: IFieldValidatorParams;
        public LastName: IFieldValidatorParams;
        public Name: IFieldValidatorParams;
        constructor();
    }
    class AddressValidator extends EntityValidatorBase {
        public Phone: any;
        public Url: any;
        public Email: any;
        public Name: any;
        public FirstName: any;
        public LastName: any;
        public Street2: any;
        public Phone2: any;
        public Email2: any;
        public StreetNumber: any;
        public Street: any;
        public City: any;
        public ZipCode: any;
        public State: any;
        public Country: any;
        constructor();
    }
    class ZipCodeValidator extends EntityValidatorBase {
        public ZipCode: any;
        constructor();
    }
    class PaymentCardTypeValidator extends EntityValidatorBase {
        public NameOnCard: any;
        public CardNumber: any;
        public CCID: any;
        public PaymentAmount: any;
        public ExpirationMonth: any;
        public ExpirationYear: any;
        constructor();
    }
    class GiftCardTypeValidator extends EntityValidatorBase {
        public CardNumber: any;
        public PaymentAmount: any;
        constructor();
    }
    class LoyaltyCardTypeValidator extends EntityValidatorBase {
        public CardNumber: any;
        public PaymentAmount: any;
        constructor();
    }
    class DiscountCardTypeValidator extends EntityValidatorBase {
        public CardNumber: any;
        constructor();
    }
}
declare module AFM.Ecommerce.Controls {
    interface IResx {
        textContent?: string;
        label?: string;
        title?: string;
        placeholder?: string;
    }
}
declare module AFM.Ecommerce.Controls {
    interface IValidatorOptions {
        data?: any;
        field: string;
        validatorType: string;
        validatorField?: string;
        validate?: (element: Element) => {};
        addressType: string;
    }
    interface ISubmitIfValidOptions {
        data?: any;
        field: string;
        validatorType: string;
        validatorField?: string;
        addressType: string;
        containerSelector: string;
        click?: (element: Element) => {};
        submit: (eventObject: JQueryEventObject) => {};
        validate?: (element: Element) => {};
    }
}
declare module AFM.Ecommerce.Controls {
    class AFMExtendedService {
        private static proxy;
        static GetProxy(): void;
        static GetZipCode(): AsyncResult<AFMZipCodeResponse>;
        static GetCultureId(): AsyncResult<string>;
        static SetShippingtoLineItems(shippingOptions: SelectedLineDeliveryOption, dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static SetZipCode(zipCode: string, isCheckoutSession: boolean): AsyncResult<ShoppingCartResponse>;
        static SendOrderSummary(orderSummary: AFMOrderSummary): AsyncResult<string>;
        static GetUserCartInfo(): AsyncResult<UserCartInfo>;
        static SetUserCartInfo(userCartData: UserCartInfo): AsyncResult<string>;
        static GetUserPageMode(): AsyncResult<UserPageMode>;
        static SetUserPageMode(userPageMode: UserPageMode): AsyncResult<string>;
        static ClearAllCookies(): AsyncResult<string>;
        static SubmitOrder(tenderLineData: TenderDataLine[], headerValues: AFMSalesTransHeader, email: string, cardToken: string): AsyncResult<CreateSalesOrderResponse>;
        static CommenceCheckout(dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static GetCardTokenData(tranid: string): AsyncResult<CardTokenData>;
        static ValidateAddress(address: Address): AsyncResult<AFMAddressData>;
        static GetAFMAddressState(countryRegionId: string): AsyncResult<ObservableArray<StateProvinceInfo>>;
        static ValidateZipcode(countryRegionId: string, zipCode: string): AsyncResult<boolean>;
        static ValidatePromocode(promoCode: string): AsyncResult<boolean>;
        static GetShippingAddresses(IsShipping: boolean): AsyncResult<AddressCollectionResponse>;
        static GetOrderConfirmationDetails(): AsyncResult<AFMOrderConfirmationResponse>;
        static RedirectToPayment(): AsyncResult<string>;
        static ValidateRsaId(rsaId: string, isCheckoutSession: boolean): AsyncResult<boolean>;
        static RemoveRsaIdFromCart(isCheckoutSession: boolean): AsyncResult<boolean>;
    }
}
declare module AFM.Ecommerce.Controls {
    class CheckoutService {
        private static proxy;
        static GetProxy(): void;
        static SubmitOrder(tenderLineData: TenderDataLine[], email: string): AsyncResult<CreateSalesOrderResponse>;
        static GetPaymentCardTypes(): AsyncResult<PaymentCardTypesResponse>;
        static GetPaymentCard(): void;
        static GetDeliveryPreferences(): AsyncResult<DeliveryPreferenceResponse>;
        static GetDeliveryOptionsInfo(): AsyncResult<DeliveryOptionsResponse>;
        static GetOrderDeliveryOptionsForShipping(shipToAddress: Address): AsyncResult<DeliveryOptionsResponse>;
        static GetLineDeliveryOptionsForShipping(selectedLineShippingInfo: SelectedLineShippingInfo[]): AsyncResult<DeliveryOptionsResponse>;
        static SetOrderDeliveryOption(selectedDeliveryOption: SelectedDeliveryOption, dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static SetLineDeliveryOptions(lineDeliveryOptions: SelectedLineDeliveryOption[], dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static IsAuthenticatedSession(): AsyncResult<BooleanResponse>;
        static GetUserEmail(): AsyncResult<StringResponse>;
        static GetGiftCardBalance(giftCardNumber: string): AsyncResult<GiftCardResponse>;
    }
}
declare module AFM.Ecommerce.Controls {
    class ShoppingCartService {
        private static proxy;
        static GetProxy(): void;
        static UpdateShoppingCart(shoppingCartResponse: ShoppingCartResponse, isCheckoutSession: boolean): void;
        static CreateListingObject(transactionItem: TransactionItem): ListingClass;
        static GetDimensionValues(color: string, size: string, style: string): any;
        static OnUpdateShoppingCart(callerContext: any, handler: any): void;
        static OnUpdateCheckoutCart(callerContext: any, handler: any): void;
        static GetShoppingCart(shoppingCartDataLevel: ShoppingCartDataLevel, isCheckoutSession: boolean): AsyncResult<ShoppingCartResponse>;
        static RemoveFromCart(isCheckoutSession: boolean, lineId: string, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static RemoveKitFromCart(isCheckoutSession: boolean, lineIds: string[], shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static UpdateQuantity(isCheckoutSession: boolean, items: TransactionItem[], shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static GetPromotions(isCheckoutSession: boolean, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static AddOrRemovePromotion(isCheckoutSession: boolean, promoCode: string, isAdd: boolean, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        static CommenceCheckout(dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse>;
        private static ReplaceOrAddQueryString(url, key, value);
        private static GetNoImageMarkup();
        private static BuildImageMarkup(imageData, width, height);
        private static BuildImageMarkup50x50(imageData);
    }
}
