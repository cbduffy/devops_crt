﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../../ExternalScripts/KnockoutJS.d.ts" />
/// <reference path="../Helpers/Utils.ts" />
/// <reference path="../../Resources/Resources.ts" />

module AFM.Ecommerce.Controls {
    /*
     * Validator knockout binding extension.
     *
     *
     * Validator binding usage example:
     * validator: { 
     *          data: customerAddEditViewModel.CustomerProxy,
     *          field: 'FirstName',
     *          validatorType: 'CustomerValidator'
     *          validate: function(srcElement) { 
     *              return srcElement.value > 5; // Do BL validation.
     *          }
     *        }
     *
     * @param {any} [data] - instance of entity object to bind data to. If not provided, binding context is used to bind data to given field.
     * @param {string} field - field of entity object to bind data to.
     * @param {string} validatorType - validator class name to be used to validate the entity.
     * @param {string} [validatorField] - name of validatorType class field to be used for validation. If not provided - field attribute is used instead.
     * @param {(element: Element) => {}} [validate] - custom validation method. Optional.
     *
     */
    export interface IValidatorOptions {
        data?: any;
        field: string;
        validatorType: string;
        validatorField?: string;
        validate?: (element: Element) => {}; // Customer validation method.
        // HXM for bug 62784 
        addressType: string;
    }

    /*
     * Validator helper knockout binding extension.
     *
     *
     * Validator binding usage example:
     * submitIfValid: { 
     *          containerSelector: '.someJQuerySelector',
     *          submit: function(eventObject) { 
     *              // Do something.
     *          }
     *        }
     *
     * @param {string} containerSelector - JQuery selector to identify container element content of which needs to be validated.
     * @param {(eventObject: JQueryEventObject) => {}} submit - submit method to execute once validation complete.
     *
     */
    export interface ISubmitIfValidOptions {
        data?: any;
        field: string;
        validatorType: string;
        validatorField?: string;
        addressType: string;
        containerSelector: string;
        click?: (element: Element) => {};
        submit: (eventObject: JQueryEventObject) => {};
        validate?: (element: Element) => {}; // Customer validation method.
    }
}

ko.bindingHandlers.validator = {
    init: (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) => {
        // This will be called when the binding is first applied to an element
        // Set up any initial state, event handlers, etc. here

        var binding: AFM.Ecommerce.Controls.IValidatorOptions = ko.utils.unwrapObservable(valueAccessor()) || {};

        // Optional, in case user wants to combine input value binding with validation.
        if (!AFM.Utils.isNullOrWhiteSpace(binding.field)) {
            // Apply value data-bind.
            // if "data" attribute is provided we use it's field directly, otherwise use field of the object from binding context.
            var valueObject = binding.data ? binding.data[binding.field] : bindingContext.$data[binding.field];

            // To handle not initialized field we create new observable to set value.
            if (AFM.Utils.isNullOrUndefined(valueObject)) {
                valueObject = ko.observable('');
                valueObject.subscribe((newValue) => {
                    bindingContext.$data[binding.field] = newValue;
                });
            }

            ko.applyBindingsToNode(element, { value: valueObject, valueUpdate: 'input' });
        }

        // Set validation attributes.
        if (AFM.Utils.isNullOrUndefined(binding.validatorType)) {
            throw AFM.Ecommerce.Controls.Resources.String_71;
        }

        var validator = Object.create(AFM.Ecommerce.Controls[binding.validatorType].prototype);
        validator.constructor.apply(validator);

        var field = binding.validatorField ? binding.validatorField : binding.field;
        //address type parameter added by Sakalija for bug no 72474
        validator.setValidationAttributes(element, field,binding.addressType);

        var $element = $(element);
        $element.attr("msax-isValid", true);
               
        // In HTML5 forms not allowed to be placed in another form. 
        // With ASP.NET page form tag already exists on the host page.
        // Since we do not have a form tag wrapping fields that are groupped together
        // we manually have to find the wrapping container and validate.
        $element.change((eventObject) => {
            // Invoke HTML5 validation.
            var isValid = eventObject.currentTarget.checkValidity();

            // Custom validation.
            if (isValid && binding.validate) {
                isValid = binding.validate.call(viewModel, eventObject.currentTarget);
            }

            $element.attr("msax-isValid", isValid);

            // For radio button style for invalid field will be done by label.
            if (eventObject.currentTarget.type === "radio") {
                var $label = $element.parent().find("[for=" + eventObject.currentTarget.id + "]");
                $label.attr("msax-isValid", isValid);
            }

            //Commented by Sakalija to set default title attributes for bug no 72474
            // Sets title attribute to an element if the validation on element fails.
            // The title attribute will indicate what the expected value for element is.
            //if (!AFM.Utils.isNullOrWhiteSpace(validator[field])) {
            //    // HXM for bug 62784
            //    validator[field].setTitleAttributeIfInvalid(element, binding.addressType);
            //}

            // Let other handlers to execute to support checkedValue for radio buttons.
            return isValid;
        });

    }
};

ko.bindingHandlers.submitIfValid = {
    init: (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) => {
        var binding: AFM.Ecommerce.Controls.ISubmitIfValidOptions = ko.utils.unwrapObservable(valueAccessor()) || {};

        var $element = $(element);

        $element.click((eventObject: JQueryEventObject) => {
            eventObject.preventDefault();

            // Try find in the parent stack.
            var $wrapper = $element.closest(binding.containerSelector);

            // If element is not one of the parents, search entire page.
            if ($wrapper.length === 0) {
                $wrapper = $(binding.containerSelector);
            }

            // Trigger change to invoke validators.
            $wrapper.find("input,select").each((index, elem) => {
                $(elem).change();
            });

            // Find all invalid fields
            var $invalidFields = $wrapper.find("[msax-isValid=false]");

            // Select first invalid field.
            $invalidFields.first().focus();
            $invalidFields.first().select();

            // Call submit method if there are no invalid fields.
            if ($invalidFields.length === 0) {
                var isValid = true;
                if (binding.validate) {
                    isValid = binding.validate.call(viewModel, $wrapper);
                }

                if (isValid) {
                    binding.submit.call(viewModel, eventObject);
                }
            }
        });
    }
};

//NS by muthait dated 25Sep2014

ko.bindingHandlers.clickIfValid = {
    init: (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) => {
        var binding: AFM.Ecommerce.Controls.ISubmitIfValidOptions = ko.utils.unwrapObservable(valueAccessor()) || {};

        var $element = $(element);
        $element.click((eventObject: JQueryEventObject) => {
            eventObject.preventDefault();
            
            // Try find in the parent stack.
            var $wrapper = $element.closest(binding.containerSelector);

            // If element is not one of the parents, search entire page.
            if ($wrapper.length === 0) {
                $wrapper = $(binding.containerSelector);
            }


            // Trigger change to invoke validators.
            $wrapper.find("input,select").each((index, elem) => {
                $(elem).change();
            });

            

            // Find all invalid fields
            var $invalidFields = $wrapper.find("[msax-isValid=false]");

            var errorField;
            var i = 0;
             //title assigned for error message by Sakalija for bug no 72474
           if ($invalidFields.length > 0) {
                $.each($invalidFields, function (index, attr) {

                    if (attr.nodeName == "INPUT" || attr.nodeName == "SELECT") {
                        //AFM.Ecommerce.Controls.errorMessage = attr.value;
                        $invalidFields[index].focus();
                        errorField = ".msax-" + attr.id + "Error";
                        $(document).find(errorField).text(attr.title);
                        $(document).find(errorField).show();
                        i = i + 1;
                    }
                });
            }

            // Call submit method if there are no invalid fields.
            if ($invalidFields.length === 0) {
               // AFM.Ecommerce.Controls.errorMessage = "";
               // AFM.Ecommerce.Controls.CartBase.showError(false);
                var isValid = true;
                if (binding.validate) {
                    isValid = binding.validate.call(viewModel, $wrapper);
                }

                if (isValid) {
                    binding.click.call(viewModel, eventObject);
                }
            }
        });
    }
};