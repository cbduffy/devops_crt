﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: WebResource("AFM.Ecommerce.Controls.Common.Scripts.js", "application/x-javascript")]
namespace AFM.Ecommerce.Controls
{
    /// <summary>
    /// RetailWebControl base class
    /// </summary>
    [ComVisible(false)]
    public abstract class RetailWebControl : WebControl
    {
        private const string filePathFormat = "{0}.{1}";

        private bool isMobileView = false;
        private bool isMobileViewSet = false;

        protected const string cssClass = "msax-Control";
        protected ControlsSection controlsSection;
        protected Collection<String> cssUrls = new Collection<String>();
        protected Collection<String> scriptUrls = new Collection<String>();

        /// <summary>
        /// Tag override.
        /// </summary>
        protected override System.Web.UI.HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Div;
            }  
        }

        /// <summary>
        /// property for control to decide if it is mobile view or now. If Mobile view is not set by client we 
        /// default to Browser property IsMobileDevice to decide mobile view
        /// </summary>
        public bool IsMobileView
        {
            get
            {
                if (isMobileViewSet)
                {
                    return isMobileView;
                }
                else
                {
                    // Checking if page header is visible here. 
                    // This way if the client does not use runat='server' on the page header, we will not be throwing any error.
                    return this.Page.Header != null ? this.Page.Request.Browser.IsMobileDevice : false;
                }
            }
            set
            {
                isMobileViewSet = true;
                isMobileView = value;
            }
        }

        /// <summary>
        /// Gets all the script urls required for the control.
        /// </summary>
        /// <returns>Collection of script urls.</returns>
        protected virtual Collection<string> GetScriptUrls()
        {
            this.AddScriptUrl("Common.Scripts.js");
            return this.scriptUrls;
        }

        /// <summary>
        /// Gets all the css urls required for the control.
        /// </summary>
        /// <returns>Collection of css urls.</returns>
        protected virtual Collection<String> GetCssUrls()
        {
            this.AddCssUrl("/css/Controls/CoreControl.css",false);
            return this.cssUrls;
        }

        /// <summary>
        /// Gets the startup markup required for the control.
        /// </summary>
        /// <param name="existingHeaderMarkup">Existing header markup to determine if startup script registration is required.</param>
        /// <returns>Startup markup.</returns>
        protected virtual string GetStartupMarkup(string existingHeaderMarkup)
        {
            string currencyTemplate = WebConfigurationManager.AppSettings["StoreFront_CurrencyTemplate"];

            controlsSection = (ControlsSection)WebConfigurationManager.GetSection("ecommerceControls");
            bool configExists = controlsSection != null
                && !string.IsNullOrWhiteSpace(controlsSection.Services.ShoppingCartServiceUrl)
                && !string.IsNullOrWhiteSpace(controlsSection.Services.CheckoutServiceUrl)
                && !string.IsNullOrWhiteSpace(controlsSection.Services.StoreProductAvailabilityServiceUrl)
                //&& !string.IsNullOrWhiteSpace(controlsSection.Services.ChannelServiceUrl)
                //&& !string.IsNullOrWhiteSpace(controlsSection.Services.LoyaltyServiceUrl)
                && !string.IsNullOrWhiteSpace(controlsSection.Services.CustomerServiceUrl)
                && !string.IsNullOrWhiteSpace(controlsSection.ProductUrlFormat);

            if (configExists)
            {
                string shoppingCartServiceUrl = controlsSection.Services.ShoppingCartServiceUrl;
                string checkoutServiceUrl = controlsSection.Services.CheckoutServiceUrl;
                string storeProductAvailabilityServiceUrl = controlsSection.Services.StoreProductAvailabilityServiceUrl;
                //string channelServiceUrl = controlsSection.Services.ChannelServiceUrl;
                //string loyaltyServiceUrl = controlsSection.Services.LoyaltyServiceUrl;
                string customerServiceUrl = controlsSection.Services.CustomerServiceUrl;
                string productUrlFormat = controlsSection.ProductUrlFormat;
                string afmExtendedServiceUrl = controlsSection.Services.AFMExtendedServiceUrl;
                string startupScript = string.Empty;
                string baseStartupScript = string.Format(@"<script type='text/javascript'> var msaxValues = {{ msax_CurrencyTemplate:'{0}',
                        msax_ProductUrlFormat:'{1}',
                        msax_ShoppingCartServiceUrl:'{2}',
                        msax_CheckoutServiceUrl:'{3}',
                        msax_StoreProductAvailabilityServiceUrl: '{4}',
                        msax_CustomerServiceUrl: '{5}',
                        msax_AFMExtendedServiceUrl: '{6}'
                    }};</script>",
                currencyTemplate, productUrlFormat, shoppingCartServiceUrl, checkoutServiceUrl, storeProductAvailabilityServiceUrl, customerServiceUrl, afmExtendedServiceUrl);

                // If page header is visible here, check if the startup script markup is present in the header already.
                if (this.Page == null || this.Page.Header == null || (existingHeaderMarkup != null && !existingHeaderMarkup.Contains(baseStartupScript)))
                {
                    startupScript += baseStartupScript;
                }

                return startupScript;
            }
            else
            {
                throw new Exception("The required configuration for the ecommerce controls do not exist");
            }
        }

        /// <summary>
        /// Adds the given script file name to the collection.
        /// </summary>
        /// <param name="fileName">The script file.</param>
        protected void AddScriptUrl(String fileName)
        {
            Type type = typeof(RetailWebControl);
            this.scriptUrls.Add(this.GetFileUrl(type, fileName,false));
        }

        /// <summary>
        /// Adds the given css file name to the collection.
        /// </summary>
        /// <param name="fileName">The css file.</param>
        protected void AddCssUrl(String fileName,bool isWebResource)
        {
            Type type = typeof(RetailWebControl);
            this.cssUrls.Add(this.GetFileUrl(type, fileName,isWebResource));
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            this.CssClass = cssClass;
        }
        

        /// <summary>
        /// Adds the given header markup to the page header.
        /// </summary>
        /// <param name="headerMarkup">The header markup.</param>
        protected void RegisterHeaderMarkup(string headerMarkup)
        {
            // Checking if page header is visible here before registering markup. 
            // This way if the client does not use runat='server' on the page header, we will not be throwing any error.
            if (!string.IsNullOrWhiteSpace(headerMarkup) && this.Page.Header != null)
            {
                this.Page.Header.Controls.Add(new LiteralControl(headerMarkup));
            }
        }

        /// <summary>
        /// Get html content of the control.
        /// </summary>
        /// <param name="fileName">The file name.</param>
        /// <returns>Html content of the file.</returns>
        protected string GetHtmlFragment(string fileName)
        {
            string resxHtml = GetResourceText(fileName);
            string result = string.Empty;

            // Exctract content inside of the placeholders.
            RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline;
            Regex regx = new Regex("<!--CONTENT_START-->(?<controlHtmlContent>.*)<!--CONTENT_END-->", options);
            Match match = regx.Match(resxHtml);

            if (match.Success)
            {
                result = match.Groups["controlHtmlContent"].Value;
            }

            return result;
        }

        /// <summary>
        /// Get text from embeded resource.
        /// </summary>
        /// <returns>Text from embeded resoruce file.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "Objects disposed properly.")]
        private string GetResourceText(string fileName)
        {
            string result = string.Empty;
            Type type = this.GetType();

            using (Stream stream = type.Assembly.GetManifestResourceStream(String.Format(filePathFormat, type.Namespace, fileName)))
            {
                using (StreamReader sr = new StreamReader(stream))
                {
                    result = sr.ReadToEnd();
                }
            }

            return result;
        }


        /// <summary>
        /// Gets the url corresponding to the given file.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="fileName">The file name.</param>
        /// <returns>The web resource url.</returns>
        private string GetFileUrl(Type type, string fileName, bool isWebResource)
        {
            if (!isWebResource & fileName.Contains("css"))
            {
               // return String.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />", fileName);
                return fileName;
            }
            else
            {
                // This check and initialization is for MVC support.
                if (this.Page == null)
                {
                    this.Page = new Page();
                }

                return this.Page.ClientScript.GetWebResourceUrl(type,
                    String.Format(filePathFormat, type.Namespace, fileName));
            }
        }
    }
}
