﻿
/*
CS by muthait dated 13 Sep 2014
*/
/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../../Resources/Resources.ts" />

module AFM.Ecommerce.Controls {
    var stringValidationErrorMessage = Resources.String_72;
    var stringValidationRegex = Resources.String_73;


    /**
     * Interface for FieldValidator constructor parameters.
     * To be able to initialize any subset of FieldValidator fields.
     * @param {number} [maxLength] - maxLength HTML5 input attribute.
     * @param {number} [max] - max HTML5 input attribute.
     * @param {number} [min] - min HTML5 input attribute.
     * @param {string} [pattern] - pattern HTML5 input attribute.
     * @param {boolean} [required] - required HTML5 input attribute.
     */
    export interface IFieldValidatorParams {
        maxLength?: number;
        max?: number;
        min?: number;
        pattern?: string;
        required?: boolean;
        title?: string;
    };

    /**
    * Validator class for entity fields. Sets up HTML5 attributes.
    */
    export class FieldValidator {
        private _validationAttributes: IFieldValidatorParams;

        /**
        * Constructs instance of FieldValidator class. Sets up HTML5 validation attributes.
        *
        * @param {IFieldValidatorParams} params - HTML5 validation attributes to set.
        */
        constructor(params: IFieldValidatorParams) {
            this._validationAttributes = params;
        }

        /**
         * Sets validation attributes to an element.
         *
         * @param {Element} element to set validation attributes for.
         */
        public setValidationAttributes(element: Element, addressType: string): void {
            for (var attrName in this._validationAttributes) {
                var value: any;
                //Code by Sakalija to set title attributes to fix Bug no 72474
                if (attrName == "title") {
                    if (!AFM.Utils.isNullOrUndefined(addressType) && !AFM.Utils.isNullOrUndefined(this._validationAttributes[attrName])) {
                        value = this._validationAttributes[attrName].replace("{addresstype}", addressType);
                    }
                    else {
                        value = this._validationAttributes[attrName];
                    }
                }
                else {
                    value = this._validationAttributes[attrName];
                }
                if (value) {
                    element.setAttribute(attrName, value);
                }
            }
            // Remove "required" attribute from element if it is not set to true.
            if (this._validationAttributes.required !== true) {
                element.removeAttribute("required");
            }
        }


        /**
         * Sets title attribute to an element if the validation on element fails.
         * The title attribute will indicate what the expected value for element is.
         *
         * @param {Element} element to check validation and set title attribute for.
         */
        public setTitleAttributeIfInvalid(element: Element, addressType: string): void {
            //NC HXM for Bug 62784
            if (!AFM.Utils.isNullOrUndefined(addressType) && !AFM.Utils.isNullOrUndefined(this._validationAttributes["title"]) )
                var value: any = this._validationAttributes["title"].replace("{addresstype}", addressType);
            else
            var value: any = this._validationAttributes["title"];
            if (value && element.getAttribute("msax-isValid") == "false") {
                //NC HXM for Bug 62784
                if (addressType != "") {
                element.setAttribute("title", value);
            }
            else {
                element.setAttribute("title", value);
            }
            }
            else {
                element.removeAttribute("title");
            }
        }
    }

    /**
     * Base class for entity validators.
     * Fields of derived classes are used for validation.
     */
    export class EntityValidatorBase {

        constructor() {
        }

        /**
         * Set validation attributes of specified field to an element.
         *
         * @param {Element} element to set validation attributes for.
         * @param {string} fieldName - field of validation object to be used for attributes set.
         */
        // HXM for 62784
        public setValidationAttributes(element: Element, fieldName: string, addressType: string): void {
            var fieldValidator: FieldValidator = this[fieldName];
            if (fieldValidator) {
                fieldValidator.setValidationAttributes(element, addressType);
            }
        }
    }

    /**
     * Validator for ShoppingCartItem entity.
     */
    export class ShoppingCartItemValidator extends EntityValidatorBase {
        public Quantity: IFieldValidatorParams;

        constructor() {
            super();

            this.Quantity = new FieldValidator({
                maxLength: 4, required: true, title: Resources.String_74, pattern:"^(0?[0-9]{1,3}|[0-4][0-9][0-9][0-9]|5000)$" });
        }
    }


    /**
     * Validator for Promotion Code entity.
     */
    export class PromotionValidator extends EntityValidatorBase {
        public Promotioncode: IFieldValidatorParams;

        constructor() {
            super();

            this.Promotioncode = new FieldValidator({
                maxLength: 100, required: true, title: Resources.String_97
            });
        }
    }

    /**
     * Validator for SelectedOrderShippingOptions entity.
     */
    export class SelectedOrderShippingOptionsValidator extends EntityValidatorBase {
        public CartId: IFieldValidatorParams;
        public DeliveryMethodId: IFieldValidatorParams;
        public DeliveryMethodText: IFieldValidatorParams;
        public ShippingOptionId: IFieldValidatorParams;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: IFieldValidatorParams;
        public ElectronicDeliveryEmailContent: IFieldValidatorParams;

        constructor() {
            super();

            this.DeliveryMethodId = new FieldValidator({
                required: true, title: Resources.String_75
            });
        }
    }

    /**
     * Validator for SelectedOrderDeliveryOption entity.
     */
    export class SelectedOrderDeliveryOptionValidator extends EntityValidatorBase {
        public DeliveryModeId: IFieldValidatorParams;
        public DeliveryModeText: IFieldValidatorParams;
        public DeliveryPreferenceId: IFieldValidatorParams;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: IFieldValidatorParams;
        public ElectronicDeliveryEmailContent: IFieldValidatorParams;

        constructor() {
            super();

            this.DeliveryModeId = new FieldValidator({
                required: true, title: Resources.String_75
            });
        }
    }

    /**
     * Validator for Address entity.
     */
    export class CustomerValidator extends EntityValidatorBase {
        public FirstName: IFieldValidatorParams;
        public MiddleName: IFieldValidatorParams;
        public LastName: IFieldValidatorParams;
        public Name: IFieldValidatorParams;

        constructor() {
            super();

            this.FirstName = new FieldValidator({ maxLength: 25, required: true, title: stringValidationErrorMessage, pattern: stringValidationRegex });
            this.MiddleName = new FieldValidator({ maxLength: 25, title: stringValidationErrorMessage, pattern: stringValidationRegex });
            this.LastName = new FieldValidator({ maxLength: 25, required: true, title: stringValidationErrorMessage, pattern: stringValidationRegex });
            this.Name = new FieldValidator({ maxLength: 100, required: true });
        }
    }

    /**
     * Validator for Address entity.
     */
    export class AddressValidator extends EntityValidatorBase {
        public Phone;
        public Url;
        public Email;
        public Name;
        //NS Developed by spriya writewebsale
        public FirstName;
        public LastName;
        public Street2;
        public Phone2;
        public Email2;
        //NE Developed by spriya writewebsale
        public StreetNumber;
        public Street;
        public City;
        public ZipCode;
        public State;
        public Country;

        constructor() {
            super();

            this.Phone = new FieldValidator({ maxLength: 10, required: true, pattern: "^[+]?[0-9]{10,10}$" ,title:Resources.String_1169});
            this.Url = new FieldValidator({ maxLength: 255 });
            this.Email = new FieldValidator({
                maxLength: 80, required: true, title: Resources.String_76,
                pattern: "^[\\w-]+(?:\\.[\\w-]+)*@(?:[\\w-]+\\.)+[a-zA-Z]{2,7}$"
            });
            this.Name = new FieldValidator({ maxLength: 51, required: true, title: Resources.String_77 });
            this.StreetNumber = new FieldValidator({ maxLength: 250, title: Resources.String_78 });
            this.Street = new FieldValidator({ maxLength: 255, required: true, title: Resources.String_79 });
            this.City = new FieldValidator({ maxLength: 60, required: true, title: Resources.String_80 });
            this.ZipCode = new FieldValidator({ maxLength: 10, required: true, title: Resources.String_81, pattern: "^([0-9]{5})(?:[-\s]*([0-9]{4}))?$" });
            // muthait  --> for CANADA Zipcode in future : "/^([A-Z][0-9][A-Z])\s*([0-9][A-Z][0-9])$/", for all other countries : "/^(?:[A-Z0-9]+([- ]?[A-Z0-9]+)*)?$/" , - based on country code we must write switch case - CS by muthait dated 12Nov2014
            this.State = new FieldValidator({ required: true, title: Resources.String_82 });
            this.Country = new FieldValidator({ required: true, title: Resources.String_83 });
            // HXM Changed error message strings to address ConTest- 62784
            this.FirstName = new FieldValidator({ maxLength: 25, required: true, title: Resources.String_1255, pattern: "[a-zA-Z ]+" } );
            this.LastName = new FieldValidator({ maxLength: 25, required: true, title: Resources.String_1256, pattern: "[a-zA-Z ]+" } );
            this.Phone2 = new FieldValidator({ maxLength: 10, title: Resources.String_1169, pattern: "^[+]?[0-9]{10,10}$" });
            this.Street2 = new FieldValidator({ maxLength: 255, title: Resources.String_79 });
            this.Email2 = new FieldValidator({
                maxLength: 80, title: Resources.String_76,
                pattern: "^[\\w-]+(?:\\.[\\w-]+)*@(?:[\\w-]+\\.)+[a-zA-Z]{2,7}$"
            });

        }
    }

    // NS by muthait dated 16Nov2014
     /**
     * Validator for ZipCode PopOut
     */
    export class ZipCodeValidator extends EntityValidatorBase {
        public ZipCode;
        constructor() {
            super();
            this.ZipCode = new FieldValidator({ maxLength: 5, required: true, title: Resources.String_81, pattern: "^[0-9]{5}$" }); 
        }
    }
    // NE by muthait dated 16NOV2014

    /**
     * Validator for PaymentCardLine entity.
     */
    export class PaymentCardTypeValidator extends EntityValidatorBase {
        public NameOnCard;
        public CardNumber;
        public CCID;
        public PaymentAmount;
        public ExpirationMonth;
        public ExpirationYear;

        constructor() {
            super();

            this.NameOnCard = new FieldValidator({ maxLength: 100, required: true, title: Resources.String_84 });
            this.CardNumber = new FieldValidator({ maxLength: 30, required: true, title: Resources.String_85 }); //, pattern: "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$" });
            this.CCID = new FieldValidator({ maxLength: 50, required: true, title: Resources.String_86, pattern: "^[0-9]{3,4}$" });
            this.PaymentAmount = new FieldValidator({ maxLength: 100, required: true, title: Resources.String_87, pattern: "\w+([0123456789.]\w+)*" });
        }
    }

    /**
    * Validator for gift card entity.
    */
    export class GiftCardTypeValidator extends EntityValidatorBase {
        public CardNumber;
        public PaymentAmount;

        constructor() {
            super();

            this.CardNumber = new FieldValidator({ maxLength: 30, required: true, title: Resources.String_141 }); // Please enter a valid gift card number
            this.PaymentAmount = new FieldValidator({ maxLength: 100, required: true, title: Resources.String_87 }); //Please specify a valid amount
        }
    }

    /**
     * Validator for loyalty card entity.
     */
    export class LoyaltyCardTypeValidator extends EntityValidatorBase {
        public CardNumber;
        public PaymentAmount;

        constructor() {
            super();

            this.CardNumber = new FieldValidator({ maxLength: 30, required: true, title: Resources.String_151 }); // Please enter a valid loyalty card number
            this.PaymentAmount = new FieldValidator({ maxLength: 100, required: true, title: Resources.String_87 }); //Please specify a valid amount
        }
    }

    /**
     * Validator for discount code entity.
     */
    export class DiscountCardTypeValidator extends EntityValidatorBase {
        public CardNumber;

        constructor() {
            super();

            this.CardNumber = new FieldValidator({ maxLength: 100, required: true, title: Resources.String_184 }); // Please enter a valid discount code
        }
    }
} 