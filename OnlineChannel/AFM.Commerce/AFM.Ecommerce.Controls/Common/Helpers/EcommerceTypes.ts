﻿/*
CS by muthait dated 12 Sep 2014
*/
/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

module AFM.Ecommerce.Controls {
    // HXM - Start Order Confirmation should not use cart blob
    export interface OrderConfirmation {
        cart: Cart;
        BillingAddress: Address;
        ShippingAddress: Address;
        PaymentAddress: Address;
    } 

    export class OrderConfirmationClass implements OrderConfirmation {
        public cart;
        public isShoppingCartEnabled;
        public isViewTaxEnabled;
        public isDiscountViewEnabled;
        public BillingAddress;
        public ShippingAddress;
        public PaymentAddress;
    }
    // HXM - End Order Confirmation should not use cart blob
    // Represents the level of information contained in shopping cart entity in storefront.
    export class ShoppingCartDataLevel {

        /// The default level which indicates only minimal information is obtained.
        /// This data level is mainly used for mini shopping cart where product details and pricing details will suffice.
        public static Minimal = 0;

        /// This data level indicates that cart contains additional information such as shipping options, discount codes, loyalty details.
        /// This is used everywhere except mini shopping cart and the shopping cart display pages.
        public static Extended = 1;

        /// This level indicates that cart contains all information associated with it.
        /// This includes the kit component level details and cart promotions. This is used in the shopping cart display in the 'View shopping cart', 'checkout review', 'order details' pages.
        public static All = 2;
    };

    // Enum for Shopping cart item type.
    export class TransactionItemType {

        // None (indicates that the shopping cart item is neither a kit nor a kit component).
        public static None = 0;

        // Indicates that the shopping cart item is a kit.
        public static Kit = 1;

        // Indicates that the shopping cart item is a kit component.
        public static KitComponent = 2;

        //Indicates the Line Order Number
        public static LineOrderNumber;

        //Indicates the Line Item Order numbers
        public static Lineitems;
    }

    //Enum for Page mode to switch between pages
    export class PageMode {
        //Goes to Shipping page
        public static Shipping = "shipping";

        //Goes to Billing page
        public static Billing = "billing";

        //Goes to Payment page
        public static Payment = "payment";

        //Goes to Confirm page
        public static Confirm = "confirm";

        //Goes to CartView page
        public static cartview = "cartviewpagemode";

        //Goes to CartView page
        public static thankyou = "thankyou";

    }

    export interface IUserPageMode {
        //Get & Set the PageMode of the current control mode
        PageMode?:PageModeOptions;
    }

    export class UserPageMode implements IUserPageMode {
        public PageMode: PageModeOptions;

        constructor(o: any) {
            o = o || {};
            this.PageMode = o.PageMode;
        }
    }

    export class PageModeOptions {
        public static CartView = 0;
        public static ShippingView = 1;
        public static ShippingViewCompleted = 2;
        public static BillingView = 3;
        public static BillingViewcompleted = 4;
        public static PaymentView = 5;
        public static PaymentViewCompleted = 6;
        public static ConfirmView = 7;
        public static ConfirmViewCompleted = 8;
        public static ThankYou = 9;
    }
    // HXM CR 117
    export class PageStatus {
        //Validates whether current pagemode is completed
        public static Complete = 1;
        //Validates whether current pagemode is not completed
        public static Incomplete = 2;
    }

    // Enum for loyalty card tender type.
    export enum LoyaltyCardTenderType {

        // The defult value indicating the card can only redeem reward points that are earned on itself.
        AsCardTender = 0,

        // The card can redeem reward points that are earned on any cards of the same customer.
        AsContactTender = 1,

        // The card can only earn reward points but not redeem.
        NoTender = 2,

        // The card is blocked.
        Blocked = 3
    }

    // Enum for Shopping cart type.
    export class ShoppingCartType {

        // None (Cart Type not defined).
        public static None = 0;

        // Shopping cart.
        public static Shopping = 1;

        // Checkout cart - The temporary copy of the shopping cart created during the checkout process.
        public static Checkout = 2;
    }

    // Enum for Address type.
    export class AddressType {

        // AX/Delivery 
        public static Delivery = 2;

        // AX/Payment 
        public static Payment = 5;

        // AX/Payment 
        public static Invoice = 1;
    }

    //Enum for AFM Item Type
    export class AFMItemType {
        //Default Item Type
        public static Default = 0;

        //Delivery Service Item Type
        public static DeliveryServiceItem = 1;

        //ServiceItem type
        public static ServiceItem = 2;
    }

    // Enum for delivery preferences type.
    export enum DeliveryPreferenceType {
        /// Represents the value when the delivery preference type has not been set.
        None = 0,
        /// Selects the option to ship the items to an address.
        ShipToAddress = 1,
        /// Selects the option to pick up the items from a particular store.
        PickupFromStore = 2,
        /// Selects the option to ship the item thru email.
        ElectronicDelivery = 3,
        /// Selects the option to delivery items individually. Applicable only to order header level delivery preferences.
        DeliverItemsIndividually = 4
    }


     // External Order header values  class.
    export interface AFMSalesTransHeader  {
        OrderNumber?: string;
        IsOkToText?: boolean;        
        OkToTextSurveyUniqueId?:string;
        OkToTextSurveyVersionId?:string;
        TermsAndContitionsUniqueId?:string;
        TermsAndContitionsVersionId?:string;
        IsAcceptTermsAndConditions?: boolean;
        IsOptinDealsUniqueId?: string;
        IsOptinDealsVersionId?: string;
    }

    // Shopping cart product details interface 
    export interface CartProductDetails {
        Name?: string;
        ProductUrl?: string;
        ImageUrl?: string;
        Description?: string;
        ProductNumber?: string;
        DimensionValues?: any;
        SKU?: string;
        ImageMarkup?: string;
        Quantity?: number;
        ImageAlt?: string;
    }

	//NS by muthait dated 07/22/2014
	export interface AFMZipCodeResponse {
		AFMZipCode?: string;
	}
	//NE by muthait dated 07/22/2014

    // Selected shipping options interface.
    export interface SelectedShippingOptions {
        DeliveryMethodId?: string;
        DeliveryMethodText?: string;
        ShippingOptionId?: string;
        CustomAddress?: Address;
        StoreAddress?: StoreProductAvailability;
        ElectronicDeliveryEmail?: string;
        ElectronicDeliveryEmailContent?: string;
    }

    // Selected item shipping options interface.
    export interface SelectedItemShippingOptions {
        LineId?: string;
        Quantity?: number;
        DeliveryMethodId?: string;
        DeliveryMethodText?: string;
        ShippingOptionId?: string;
        CustomAddress?: Address;
        StoreAddress?: StoreProductAvailability;
        ElectronicDeliveryEmail?: string;
        ElectronicDeliveryEmailContent?: string;
    }
    
    // Selected line shipping information interface.
    export interface SelectedLineShippingInfo {
        LineId?: string;
        ShipToAddress?: Address;
    }

    // Selected delivery option interface.
    export interface SelectedDeliveryOption {
        DeliveryModeId?: string;
        DeliveryModeText?: string; // Used for rendering.
        DeliveryPreferenceId?: string;
        CustomAddress?: Address;
        StoreAddress?: StoreProductAvailability;
        ElectronicDeliveryEmail?: string;
        ElectronicDeliveryEmailContent?: string;
    }

    // Selected line delivery option interface.
    export interface SelectedLineDeliveryOption {
        LineId?: string;
        DeliveryModeId?: string;
        DeliveryModeText?: string;
        DeliveryPreferenceId?: string;
        CustomAddress?: Address;
        StoreAddress?: StoreProductAvailability;
        ElectronicDeliveryEmail?: string;
        ElectronicDeliveryEmailContent?: string;
    }

    // Delivery preference interface.
    export interface DeliveryPreference {
        Value?: string;
        Text?: string;
    }

    // Image info interface 
    export interface ImageInfo {
        Url?: string;
        AltText?: string;
    }
    // Shopping cart item interface.
    export interface TransactionItem {
        LineId: string;
        ItemType?: TransactionItemType;
        ProductDetailsExpanded?: CartProductDetails;
        KitComponents?: TransactionItem[];
        SelectedDeliveryOption?: SelectedDeliveryOption;
        ProductId?: number;
        ProductNumber?: string;
        ItemId?: string;
        VariantInventoryDimensionId?: string;
        Quantity?: number;
        PriceWithCurrency?: string;
        TaxAmountWithCurrency?: string;
        DiscountAmount?: number;
        DiscountAmountWithCurrency?: string;
        NetAmountWithCurrency?: string;
        ShippingAddress?: Address;
        DeliveryModeId?: string;
        DeliveryModeText?: string;
        ElectronicDeliveryEmail?: string;
        PromotionLines?: string[];
        ProductDetails?: string;
        NoOfComponents?: string;
        Color?: string;
        Size?: string;
        Style?: string;
        Name?: string;
        Description?: string;
        ProductUrl?: string;
        Image?: ImageInfo;
        ImageMarkup?: string; // Required only for rendering in the UI.
        OfferNames?: string;
        DeliveryPreferences?: DeliveryPreference[];
        //NS Developed by muthait for AFM_TFS_40686 dated 06/24/2014
        AFMVendorId?: string;
        AFMVendorName?: string;
        IsEcommOwned: boolean;
        AFMShippingMode: string;
        AFMCartLineATP: AFMATPResponseItem;
        PriceAfterDiscount: string;
        LineOrderNumber?: string;
        AFMKitItemId: string;
        AFMKitProductId: number;
        AFMKitItemProductDetails: string;
        AFMKitSequenceNumber: number;
        AFMUnit: string;
        AFMKitItemQuantity: number;
        AFMItemType: number;
        Listing?: Listing[];
        //NE Developed by muthait for AFM_TFS_40686 dated 06/24/2014

        //NS CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
        AFMMultipleQty?: number;
        AFMLowestQty?: number;
        AFMQtyOptions?: number[];
        AFMQtyPerBox?: number;
        //NE CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
    }


    export interface LineItems {
        cartItemId?: string;
        LineOrderNumber?: string;
    }

    //NS Developed by muthait for AFM_TFS_40686 dated 06/26/2014
    export interface AFMATPItemGroupingResponse {
        AccountNumber: string;
        Items: AFMATPResponseItem[];
        ShipTo: string;
    }
    export interface AFMATPResponseItem {
        BeginDateRange?: Date;
        BestDate?: Date;
        EndDateRange?: Date;
        IsAvailable?: boolean;
        ItemId?: string;
        Message?: string;
        Quantity?: number;
        ShippingSpan?: string;

        BeginDateRangeMOD?: Date;
        BestDateMOD?: Date;
        EndDateRangeMOD?: Date;
        MessageMOD?: string;
    }
    //NE Developed by muthait for AFM_TFS_40686 dated 06/26/2014

    // NS Developed by muthait for Listing Object to be sent for Add to wishlist functionality - 09JAN2015
    export interface Listing {
        ListingId?: number;
        ProductDetails?: string;
        KitComponentDetails: KitComponentInfo;
        ItemId?: string;
        AFMKitItemId?: string;
        AFMKitSequenceNumber?: number;
        AFMKitProductId?: number;
        AFMKitItemProductDetails?: string;
        LineId?: string;
        // NS Developed by ashish for Listing Object to have Quantity for item moved in the wishlist 20June2015
        Quantity?: number;
       // NE Developed by ashish for Listing Object to have Quantity for item moved in the wishlist 20June2015
    }

    export class ListingClass implements Listing {
        public ListingId: number;
        public ProductDetails: string;
        public KitComponentDetails: KitComponentInfo;
        public ItemId: string;
        public AFMKitItemId: string;
        public AFMKitSequenceNumber: number;
        public AFMKitProductId: number;
        public AFMKitItemProductDetails: string;
        public LineId: string;
         // NS Developed by ashish for Listing Object to have Quantity for item moved in the wishlist 20June2015
        public Quantity: number;
         // NE Developed by ashish for Listing Object to have Quantity for item moved in the wishlist 20June2015
        constructor(o: any) {
            o = o || {};
            this.ListingId = o.ListingId
            this.ProductDetails = o.ProductDetails;
            this.KitComponentDetails = o.KitComponentDetails;
            this.ItemId = o.ItemId;
            this.AFMKitItemId = o.AFMKitItemId;
            this.AFMKitSequenceNumber = o.AFMKitSequenceNumber;
            this.AFMKitProductId = o.AFMKitSequenceNumber;
            this.AFMKitItemProductDetails = o.AFMKitSequenceNumber;
            this.LineId = o.LineId;
             // NS Developed by ashish for Listing Object to have Quantity for item moved in the wishlist 20June2015
            this.Quantity = o.Quantity;
            //  NE Developed by ashish for Listing Object to have Quantity for item moved in the wishlist 20June2015
        }
    }

    export interface KitComponentInfo {
        KitLineIdentifier?: string;
        KitLineProductId?: number;
        Unit?: string;
        IsDefaultComponent?: boolean;
        KitProductMasterId?: number;
        Title?: string;
    }

    export class KitComponentInfoClass implements KitComponentInfo {
        public KitLineIdentifier: string;
        public KitLineProductId: number;
        public Unit: string;
        public KitProductMasterId: number;
        public Title: string;

        constructor(o: any) {
            o = o || {};
            this.KitLineIdentifier = o.KitLineIdentifier;
            this.KitLineProductId = o.KitLineProductId;
            this.KitProductMasterId = o.KitProductMasterId;
            this.Title = o.Title;
            this.Unit = o.Unit;

        }
    }

    // ND Developed by muthait - 09JAN2014
    // Shopping cart interface.
    export interface ShoppingCart {
        CartId?: string;
        Name?: string;
        Items?: TransactionItem[];
        LastModifiedDate?: Date;
        CartType?: ShoppingCartType;
        PromotionLines?: string[];
        DiscountCodes?: string[];
        SelectedDeliveryOption?: SelectedDeliveryOption;
        LoyaltyCardId?: string;
        SubtotalWithCurrency?: string;
        //NS Developed by muthait for AFM_TFS_40686 dated 06/23/2014
        EstShipping?: string;
        EstHomeDelivery?: string;
        AvalaraTaxError?: string;
        ATPError?: string;
        CartCount: number;
        CustomerId: string;
        SalesOrderNumber: string;
        //NE Developed by muthait for AFM_TFS_40686 dated 06/23/2014
        Discount?: string;
        ChargeAmountWithCurrency?: string;
        TaxAmountWithCurrency?: string;
        TotalAmountWithCurrency?: string;
        TotalAmount?: number;
        ShippingAddress?: Address;
        DeliveryModeId?: string;
        DeliveryPreferences?: DeliveryPreference[];
        TenderLines: TenderDataLine[];

        EstShippingDiscount?: string;
        EstHomeDeliveryDiscount?: string;

        //N - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
        RsaId?: string;
    }

    // Shopping cart response interface.
    export interface ShoppingCartResponse {
        ShoppingCart?: ShoppingCart;
        Errors?: Error[];
    }

    // Shopping cart collection response interface.
    export interface ShoppingCartCollectionResponse {
        ShoppingCarts?: ShoppingCart[];
        Errors?: Error[];
    }

    // shipping option interface.
    export interface ShippingOption {
        Id?: string;
        ShippingType?: string;
        Description?: string;
    }

    // Shipping options interface.
    export interface ShippingOptions {
        ShippingOptions?: ShippingOption[];
    }

    // Item shipping options interface.
    export interface ItemShippingOptions {
        LineId?: string;
        ShippingOptions?: ShippingOptions;
    }

    // Shipping options response interface.
    export interface ShippingOptionResponse {
        OrderShippingOptions?: ShippingOptions;
        ItemShippingOptions?: ItemShippingOptions[];
        Errors?: Error[];
    }

    // Delivery option interface.
    export interface DeliveryOption {
        Id?: string;
        Description?: string;
    }

    // Line delivery option interface.
    export interface LineDeliveryOption {
        LineId?: string;
        DeliveryOptions?: DeliveryOption[];
    }

    // Delivery options response interface.
    export interface DeliveryOptionsResponse {
        DeliveryOptions?: DeliveryOption[];
        LineDeliveryOptions?: LineDeliveryOption[];
        Errors?: Error[];
        AddressVerficationResult?: boolean;
    }

    // Payment card type interface.
    export interface PaymentCardType {
        Id?: string;
        CardType?: string;
    }

    // Payment card types response interface.
    export interface PaymentCardTypesResponse {
        CardTypes?: PaymentCardType[];
        Errors?: Error[];
    }

    // Address interface.
    export interface Address {
        Country?: string;
        State?: string;
        County?: string;
        City?: string;
        DistrictName?: string;
        AttentionTo?: string;
       // Name?: string;
        //NS Developed by spriya
        FirstName?: string;
        LastName?: string;
        //NE Developed by spriya
        Street?: string;
        StreetNumber?: string;
        ZipCode?: string;
        Phone?: string;
        Email?: string;
        EmailContent?: string;
        RecordId?: number;
        Deactivate?: boolean;
        IsPrimary?: boolean;
        AddressType?: AddressType;
        AddressFriendlyName?: string;
        Street2?: string;
        Email2: string;
        Phone2: string;
        Phone3: string;
        Phone1Extension: string;
        Phone2Extension: string;
        Phone3Extension: string;
    }


    // Address collection response interface.
    export interface AddressCollectionResponse {
        Addresses?: Address[];
        Errors?: Error[];
    }

    // Payment interface.
    export interface Payment {
        PaymentAddress?: Address;
        CardNumber?: string;
        CardType?: string;
        CCID?: string;
        ExpirationMonth?: number;
        ExpirationYear?: number;
        NameOnCard?: string;
        LoyaltyCardId?: string;
        DiscountCode?: string;
        SynchronyPromoDesc?: string;
        FinancingOptionId?: string;
        MinimumPurchase?: number;
    }

    //NS by muthait dated 26/08/2014
    //UserCartInfo to persist in cookie for maintaing address infomartion while redirection
    export interface UserCartInfo {
        ShippingAddress?: Address;
        PaymentAddress?: Address;
        LineItems?: LineItems[];
        OrderNumber?: string;
        IsOkToText?: boolean;
        IsOptinDeals: boolean;
    }

    //NS by v-hapat dated on 11/07/2014
    export interface AddressState {
        CountryRegion?: string;
        StateCode?: string;
        StateName?: string;
    }

    //NE by v-hapat dated on 11/07/2014

    //NS Developed by spriya Dated 01/12/2014
    export interface AddressData {
        IsAddressValid?: boolean;
        ErrorMessage?: string;
        suggestedAddress?: Address;
        //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
        Street2?: string; 
        //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
        //CR 306 - Address validation off - CS Developed by spriya Dated 09/14/2015
        IsServiceVerified: boolean;
        //CR 306 - Address validation off - CE Developed by spriya Dated 09/14/2015
    }

     export class AFMAddressData implements AddressData {
        public IsAddressValid: boolean;
        public ErrorMessage: string;
         public suggestedAddress: Address;
         //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
         public Street2: string;
         //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
         //CR 306 - Address validation off - CS Developed by spriya Dated 09/14/2015
        public IsServiceVerified: boolean;
        //CR 306 - Address validation off - CE Developed by spriya Dated 09/14/2015

        constructor(o: any) {
            o = o || {};
            this.ErrorMessage = o.ErrorMessage;
            this.IsAddressValid = o.IsAddressValid;
            this.suggestedAddress = o.suggestedAddress;
            this.IsServiceVerified = o.IsServiceVerified;
        }
    }
    //NE Developed by spriya

    export interface AFMOrderSummaryData{
         SubtotalWithCurrency: string;
         EstShipping: string;
         EstHomeDelivery: string;
         TaxAmountWithCurrency: string;
         TotalAmountWithCurrency: string;
    }

    export class AFMOrderSummary implements AFMOrderSummaryData{
         public SubtotalWithCurrency: string;
         public EstShipping: string;
         public EstHomeDelivery: string;
         public TaxAmountWithCurrency: string;
        public TotalAmountWithCurrency: string;

        constructor(o: any) {
            o = o || {};
            this.SubtotalWithCurrency = o.SubtotalWithCurrency;
            this.EstShipping = o.EstShipping;
            this.EstHomeDelivery = o.EstHomeDelivery;
            this.TaxAmountWithCurrency = o.TaxAmountWithCurrency;
            this.TotalAmountWithCurrency = o.TotalAmountWithCurrency;
        }
    }

    export class UserCartData implements UserCartInfo {
        public ShippingAddress: Address;
        public PaymentAddress: Address;
        public LineItems: LineItems[];
        public OrderNumber: string;
        public IsOkToText: boolean;
        public IsOptinDeals: boolean;

        constructor(o: any) {
            o = o || {};
            this.ShippingAddress = o.ShippingAddress;
            this.PaymentAddress = o.PaymentAddress;
            this.LineItems = o.LineItems;
            this.OrderNumber = o.OrderNumber;
            this.IsOkToText = o.IsOkToText;
            this.IsOptinDeals = o.IsOptinDeals ;
        }
    }

    // Card Token from Ashley Connector after credit card processing
    export interface CardTokenData {
        Last4Digits: string;
        ExpirationMonth: string;
        ExpirationYear: string;
        Name: string;
        CardToken: string;
        CardType: string;
        UniqueCardId?: string;
        Error?: string;
        SynchronyPromoDesc?: string;
        FinancingOptionId?: string;
        SynchronyMinPurchase?: number;
        RemoveCartDiscount?: boolean;

    }
    //NE by muthait dated 26/08/2014

    // Payment card response interface.
    export interface PaymentCardResponse {
        PaymentCards?: Payment[];
    }

    // Payment interface.
    export interface TokenizedPaymentCard {
        PaymentAddress?: Address;
        CardType?: string;
        ExpirationMonth?: number;
        ExpirationYear?: number;
        NameOnCard?: string;
        CardToken?: string;
        UniqueCardId?: string;
        MaskedCardNumber?: string;
        SynchronyPromoDesc?: string;
        FinancingOptionId?: string;
        MinimumPurchase?: number;
        RemoveCartDiscount?: boolean;
    }

    // Error interface.
    export interface Error {
        ErrorCode?: string;
        ErrorMessage?: string;
    }

    // Create sales order response interface.
    export interface CreateSalesOrderResponse {
        OrderNumber?: string;
        Errors?: Error[];
    }

    // Store product item availability interface.
    export interface StoreProductAvailabilityItem {
        RecordId?: number;
        ItemId?: string;
        VariantInventoryDimensionId?: string;
        WarehouseInventoryDimensionId?: string;
        InventoryLocationId?: string;
        AvailableQuantity?: number;
        ProductDetails?: string;
    }

    // Store product availability interface.
    export interface StoreProductAvailability {
        ChannelId?: number;
        Latitude?: number;
        Longitude?: number;
        Distance?: string;
        InventoryLocationId?: string;
        StoreId?: string;
        StoreName?: string;
        PostalAddressId?: string;
        Country?: string;
        State?: string;
        County?: string;
        City?: string;
        DistrictName?: string;
        AttentionTo?: string;
        Name?: string;
        Street?: string;
        StreetNumber?: string;
        ZipCode?: string;
        Phone?: string;
        Email?: string;
        EmailContent?: string;
        RecordId?: number;
        Deactivate?: boolean;
        IsPrimary?: boolean;
        AddressType?: AddressType;
        AddressFriendlyName?: string;
        ProductAvailabilities?: StoreProductAvailabilityItem[];
        SelectDisabled?: string;
        AreItemsAvailableInStore?: boolean;
    }

    // Country info interface.
    export interface CountryInfo {
        CountryCode?: string;
        CountryName?: string;
    }

    // Channel configuration interface.
    export interface ChannelConfiguration {
        GiftCardItemId?: string;
        PickupDeliveryModeCode?: string;
        EmailDeliveryModeCode?: string;
        BingMapsApiKey?: string;
        CurrencyStringTemplate?: string;
    }

    // Store location interface.
    export interface StoreLocation {
        ChannelId?: number;
        Latitude?: number;
        Longitude?: number;
        Distance?: string;
        InventoryLocationId?: string;
        StoreId?: string;
        StoreName?: string;
        PostalAddressId?: string;
        Country?: string;
        State?: string;
        County?: string;
        City?: string;
        DistrictName?: string;
        AttentionTo?: string;
        Name?: string;
        Street?: string;
        StreetNumber?: string;
        ZipCode?: string;
        Phone?: string;
        Email?: string;
        EmailContent?: string;
        RecordId?: number;
        Deactivate?: boolean;
        IsPrimary?: boolean;
        AddressType?: AddressType;
        AddressFriendlyName?: string;
    }

    // Tender data line interface.
    export interface TenderDataLine {
        Amount?: string;
        GiftCardId?: string;
        LoyaltyCardId?: string;
        PaymentCard?: Payment;
        TokenizedPaymentCard?: TokenizedPaymentCard;
    }

    // Sales line delivery preference interface.
    export interface LineDeliveryPreference {
        LineId?: string;
        DeliveryPreferenceTypes?: DeliveryPreferenceType[];
    }

    // Cart delivery preference interface.
    export interface CartDeliveryPreferences {
        HeaderDeliveryPreferenceTypes?: DeliveryPreferenceType[];
        LineDeliveryPreferences?: LineDeliveryPreference[];
    }

    // Delivery preference response interface.
    export interface DeliveryPreferenceResponse {
        CartDeliveryPreferences?: CartDeliveryPreferences;
        Errors?: Error[];
    }

    // Boolean response interface.
    export interface BooleanResponse {
        IsTrue?: boolean;
        Errors?: Error[];
    }

    // String response interface.
    export interface StringResponse {
        Value?: string;
        Errors?: Error[];
    }

    // Store product availability response interface.
    export interface StoreProductAvailabilityResponse {
        Stores?: StoreProductAvailability[];
        Errors?: Error[];
    }

    // Store location response interface.
    export interface StoreLocationResponse {
        Stores?: StoreLocation[];
        Errors?: Error[];
    }

    // Channel information response interface.
    export interface ChannelInfoResponse {
        ChannelConfiguration?: ChannelConfiguration;
        Errors?: Error[];
    }

    // Tender types response interface.
    export interface TenderTypesResponse {
        HasCreditCardPayment?: boolean;
        HasGiftCardPayment?: boolean;
        HasLoyaltyCardPayment?: boolean;
        Errors?: Error[];
    }

    // Country info response interface.
    export interface CountryInfoResponse {
        Countries?: CountryInfo[];
        Errors?: Error[];
    }

    // Gift card information interface.
    export interface GiftCardInformation {
        GiftCardId?: string;
        Balance?: number;
        BalanceWithCurrency?: string;
        CurrencyCode?: string;
        IsInfoAvailable?: boolean;
    }

    // Gift card response interface.
    export interface GiftCardResponse {
        GiftCardInformation?: GiftCardInformation;
        Errors?: Error[];
    }

    // Loyalty card interface.
    export interface LoyaltyCard {
        CardNumber?: string;
        CardTenderType?: number;
    }

    // Loyalty card response interface.
    export interface LoyaltyCardsResponse {
        LoyaltyCards?: LoyaltyCard[];
        Errors?: Error[];
    }

    // State/province info interface.
    export interface StateProvinceInfo {
        CountryRegionId?: string;
        StateId?: string;
        StateName?: string;
    }

    // State province info response interface.
    export interface StateProvinceInfoResponse {
        StateProvinces?: StateProvinceInfo[];
        Errors?: Error[];
    }


    // Shopping cart product details class.
    export class CartProductDetailsClass implements CartProductDetails {
        public Name: string;
        public ProductUrl: string;
        public ProductNumber: string;
        public ImageUrl: string;
        public Description: string;
        public DimensionValues: any;
        public SKU: string;
        public ImageMarkup: string;
        public Quantity: number;

        constructor(o: any) {
            o = o || {};
            this.Name = o.Name;
            this.ProductUrl = o.ProductUrl;
            this.ProductNumber = o.ProductNumber;
            this.ImageUrl = o.ImageUrl;
            this.Description = o.Description;
            this.DimensionValues = o.DimensionValues;
            this.SKU = o.SKU;
            this.ImageMarkup = o.ImageMarkup;
            this.Quantity = o.Quantity;
        }
    }

    // Selected shipping options class.
    export class SelectedShippingOptionsClass implements SelectedShippingOptions {
        public DeliveryMethodId: string;
        public DeliveryMethodText: string;
        public ShippingOptionId: string;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: string;
        public ElectronicDeliveryEmailContent: string;

        constructor(o: any) {
            o = o || {};
            this.DeliveryMethodId = o.DeliveryMethodId;
            this.DeliveryMethodText = o.DeliveryMethodText;
            this.ShippingOptionId = o.ShippingOptionId;
            this.CustomAddress = o.CustomAddress;
            this.StoreAddress = o.StoreAddress;
            this.ElectronicDeliveryEmail = o.ElectronicDeliveryEmail;
            this.ElectronicDeliveryEmailContent = o.ElectronicDeliveryEmailContent;
        }
    }

    // Selected item shipping options class.
    export class SelectedItemShippingOptionsClass implements SelectedItemShippingOptions {
        public LineId: string;
        public Quantity: number;
        public DeliveryMethodId: string;
        public DeliveryMethodText: string;
        public ShippingOptionId: string;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: string;
        public ElectronicDeliveryEmailContent: string;

        constructor(o: any) {
            o = o || {};
            this.LineId = o.LineId;
            this.Quantity = o.Quantity;
            this.DeliveryMethodId = o.DeliveryMethodId;
            this.DeliveryMethodText = o.DeliveryMethodText;
            this.ShippingOptionId = o.ShippingOptionId;
            this.CustomAddress = o.CustomAddress;
            this.StoreAddress = o.StoreAddress;
            this.ElectronicDeliveryEmail = o.ElectronicDeliveryEmail;
            this.ElectronicDeliveryEmailContent = o.ElectronicDeliveryEmailContent;
        }
    }

    // Selected item shipping options class.
    export class SelectedLineDeliveryOptionClass implements SelectedLineDeliveryOption {
        public LineId: string;
        public DeliveryModeId: string;
        public DeliveryModeText: string;
        public DeliveryPreferenceId: string;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: string;
        public ElectronicDeliveryEmailContent: string;

        constructor(o: any) {
            o = o || {};
            this.LineId = o.LineId;
            this.DeliveryModeId = o.DeliveryModeId;
            this.DeliveryModeText = o.DeliveryModeText;
            this.DeliveryPreferenceId = o.DeliveryPreferenceId;
            this.CustomAddress = o.CustomAddress;
            this.StoreAddress = o.StoreAddress;
            this.ElectronicDeliveryEmail = o.ElectronicDeliveryEmail;
            this.ElectronicDeliveryEmailContent = o.ElectronicDeliveryEmailContent;
        }
    }

    // Selected order delivery option class.
    export class SelectedDeliveryOptionClass implements SelectedDeliveryOption {
        public DeliveryModeId: string;
        public DeliveryModeText: string;
        public DeliveryPreferenceId: string;
        public CustomAddress: Address;
        public StoreAddress: StoreProductAvailability;
        public ElectronicDeliveryEmail: string;
        public ElectronicDeliveryEmailContent: string;

        constructor(o: any) {
            o = o || {};
            this.DeliveryModeId = o.DeliveryModeId;
            this.DeliveryModeText = o.DeliveryModeText;
            this.DeliveryPreferenceId = o.DeliveryPreferenceId;
            this.CustomAddress = o.CustomAddress;
            this.StoreAddress = o.StoreAddress;
            this.ElectronicDeliveryEmail = o.ElectronicDeliveryEmail;
            this.ElectronicDeliveryEmailContent = o.ElectronicDeliveryEmailContent;
        }
    }

    // Image info class
    export class ImageInfoClass implements ImageInfo {
        public Url: string;
        public AltText: string;

        constructor(o: any) {
            o = o || {};
            this.Url = o.Url;
            this.AltText = o.AltText;
        }
    }
    // Shopping cart item class.
    export class TransactionItemClass implements TransactionItem {
        public LineId: string;
        public ItemType: TransactionItemType;
        public KitComponents: TransactionItemClass[];
        public SelectedDeliveryOption: SelectedDeliveryOption;
        public ProductId: number;
        public ProductNumber: string;
        public ItemId: string;
        public VariantInventoryDimensionId: string;
        public Quantity: number;
        public PriceWithCurrency: string;
        public TaxAmountWithCurrency: string;
        public DiscountAmount: number;
        public DiscountAmountWithCurrency: string;
        public NetAmountWithCurrency: string;
        public ShippingAddress: Address;
        public DeliveryModeId: string;
        public DeliveryModeText: string;
        public ElectronicDeliveryEmail: string;
        public PromotionLines: string[];
        public ProductDetailsExpanded: CartProductDetails;
        public ProductDetails: string;
        public NoOfComponents: string;
        public Color: string;
        public Size: string;
        public Style: string;
        public Name: string;
        public Description: string;
        public ProductUrl: string;
        public Image: ImageInfo;
        public ImageMarkup: string;
        public OfferNames: string;
        public DeliveryPreferences: DeliveryPreference[];
        //NS Developed by muthait for AFM_TFS_40686 dated 06/24/2014
        public AFMVendorId: string;
        public AFMVendorName: string;
        public AFMShippingMode: string;
        public IsEcommOwned: boolean;
        public AFMCartLineATP: AFMATPResponseItem;
        public PriceAfterDiscount: string;
        public LineOrderNumber: string;

        public AFMKitItemId: string;
        public AFMKitProductId: number;
        public AFMKitItemProductDetails: string;
        public AFMKitSequenceNumber: number;
        public AFMUnit: string;
        public AFMKitItemQuantity: number;
        public AFMItemType: number; 
        public Listing: Listing[];

        //NE Developed by muthait for AFM_TFS_40686 dated 06/24/2014
        //NS CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
        public AFMMultipleQty: number;
        public AFMLowestQty: number;
        public AFMQtyOptions: number[];
        public AFMQtyPerBox: number;
        //NE CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
        constructor(o: any) {
            o = o || {};
            this.LineId = o.LineId;
            this.ItemType = o.ItemType;
            if (o.KitComponents) {
                this.KitComponents = [];
                for (var i = 0; i < o.KitComponents.length; i++) {
                    this.KitComponents[i] = o.KitComponents[i] ? new TransactionItemClass(o.KitComponents[i]) : null;
                }
            }

            if (o.SelectedDeliveryOption) {
                this.SelectedDeliveryOption = o.SelectedDeliveryOption;
            } else {
                this.SelectedDeliveryOption = new SelectedDeliveryOptionClass(null);
            }

            this.ProductId = o.ProductId;
            this.ProductNumber = o.ProductNumber;
            this.ItemId = o.ItemId;
            this.VariantInventoryDimensionId = o.VariantInventoryDimensionId;
            this.Quantity = o.Quantity;
            this.PriceWithCurrency = o.PriceWithCurrency;
            this.TaxAmountWithCurrency = o.TaxAmountWithCurrency;
            this.DiscountAmount = o.DiscountAmount;
            this.DiscountAmountWithCurrency = o.DiscountAmountWithCurrency;
            this.NetAmountWithCurrency = o.NetAmountWithCurrency;
            this.ShippingAddress = o.ShippingAddress;
            this.DeliveryModeId = o.DeliveryModeId;
            this.DeliveryModeText = o.DeliveryModeText;
            this.ElectronicDeliveryEmail = o.ElectronicDeliveryEmail;
            if (o.PromotionLines) {
                this.PromotionLines = [];
                for (var i = 0; i < o.PromotionLines.length; i++) {
                    this.PromotionLines[i] = o.PromotionLines[i];
                }
            }

            this.ProductDetailsExpanded = o.ProductDetailsExpanded;
            this.ProductDetails = o.ProductDetails;
            this.NoOfComponents = o.NoOfComponents;
            this.Color = o.Color;
            this.Style = o.Style;
            this.Size = o.Size;
            this.Name = o.Name;
            this.Description = o.Description;
            this.ProductUrl = o.ProductUrl;
            this.Image = o.Image;
            this.ImageMarkup = o.ImageMarkup;
            this.OfferNames = o.OfferNames;
            if (o.DeliveryPreferences) {
                this.DeliveryPreferences = [];
                for (var i = 0; i < o.DeliveryPreferences.length; i++) {
                    this.DeliveryPreferences[i] = o.DeliveryPreferences[i];
                }
            }
            //NS Developed by muthait for AFM_TFS_40686 dated 06/24/2014
            this.AFMVendorId = o.AFMVendorId;
            this.AFMVendorName = o.AFMVendorName;
            this.AFMShippingMode = o.AFMShippingMode;
            this.IsEcommOwned = o.IsEcommOwned;
            this.AFMCartLineATP = o.AFMCartLineATP;
            this.PriceAfterDiscount = o.PriceAfterDiscount;
            this.AFMKitItemId = o.AFMKitItemId;
            this.AFMKitSequenceNumber = o.AFMKitSequenceNumber;
            this.AFMKitProductId = o.AFMKitProductId;
            this.AFMKitItemProductDetails = o.AFMKitItemProductDetails;
            this.AFMKitItemQuantity = o.AFMKitItemQuantity;
            this.AFMUnit = o.AFMUnit;
            this.LineOrderNumber = o.LineOrderNumber;
            this.AFMItemType = o.AFMItemType;
            this.Listing = o.Listing;
            //NE Developed by muthait for AFM_TFS_40686 dated 06/24/2014

            //NS CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
            this.AFMMultipleQty = o.AFMMultipleQty;
            this.AFMLowestQty = o.AFMLowestQty;
            this.AFMQtyPerBox = o.AFMQtyPerBox;
            this.AFMQtyOptions = [];
            if (this.AFMMultipleQty > 1 || this.AFMLowestQty > 1) {               
                for (var i = 1; i <= 10; i++) {
                    this.AFMQtyOptions[i] = o.AFMQtyOptions[i];
                }

            }
            //NE CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
        }
    }

    // Shopping cart class.
    export class ShoppingCartClass implements ShoppingCart {
        public CartId: string;
        public Name: string;
        public Items: TransactionItem[];
        public LastModifiedDate: Date;
        public CartType: ShoppingCartType;
        public PromotionLines: string[];
        public DiscountCodes: string[];
        public SelectedDeliveryOption: SelectedDeliveryOption;
        public LoyaltyCardId: string;
        public SubtotalWithCurrency: string;
        public DiscountAmountWithCurrency: string;
        public ChargeAmountWithCurrency: string;
        public TaxAmountWithCurrency: string;
        public TotalAmountWithCurrency: string;
        public TotalAmount: number;
        //NS Developed by muthait for AFM_TFS_40686 dated 06/23/2014
        public EstShipping: string;
        public EstHomeDelivery: string;
        public AvalaraTaxError: string;
        public ATPError: string;
        public CartCount: number;
        public CustomerId: string;
        public SalesOrderNumber: string;
        //NE Developed by muthait for AFM_TFS_40686 dated 06/23/2014

        public GrandTotal: string;
        public ShippingAddress: Address;
        public DeliveryModeId: string;

        public TenderLines: TenderDataLine[];

        public EstShippingDiscount: string;
        public EstHomeDeliveryDiscount: string;

        //N - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
        public RsaId: string;

        constructor(o: any) {
            o = o || {};
            this.CartId = o.CartId;
            this.Name = o.Name;
            if (o.Items) {
                this.Items = [];
                for (var i = 0; i < o.Items.length; i++) {
                    this.Items[i] = o.Items[i] ? new TransactionItemClass(o.Items[i]) : null;
                }
            }
            this.LastModifiedDate = o.LastModifiedDate;
            this.CartType = o.CartType;
            if (o.PromotionLines) {
                this.PromotionLines = [];
                for (var i = 0; i < o.PromotionLines.length; i++) {
                    this.PromotionLines[i] = o.PromotionLines[i];
                }
            }
            if (o.DiscountCodes) {
                this.DiscountCodes = [];
                for (var i = 0; i < o.DiscountCodes.length; i++) {
                    this.DiscountCodes[i] = o.DiscountCodes[i];
                }
            }
            this.SelectedDeliveryOption = o.SelectedDeliveryOption;
            this.LoyaltyCardId = o.LoyaltyCardId;
            if (o.SelectedDeliveryOption) {
                this.SelectedDeliveryOption = o.SelectedDeliveryOption;
            } else {
                this.SelectedDeliveryOption = new SelectedDeliveryOptionClass(null);
            }

            this.LoyaltyCardId = o.LoyaltyCardId;
            this.SubtotalWithCurrency = o.SubtotalWithCurrency;
            this.DiscountAmountWithCurrency = o.DiscountAmountWithCurrency;
            this.ChargeAmountWithCurrency = o.ChargeAmountWithCurrency;
            this.TaxAmountWithCurrency = o.TaxAmountWithCurrency;
            this.TotalAmount = o.TotalAmount;
            this.TotalAmountWithCurrency = o.TotalAmountWithCurrency;
            //NS Developed by muthait for AFM_TFS_40686 dated 06/23/2014
            this.EstShipping = o.EstShipping;
            this.EstHomeDelivery = o.EstHomeDelivery;
            this.AvalaraTaxError = o.AvalaraTaxError;
            this.ATPError = o.ATPError;
            this.CartCount = o.CartCount;
            this.CustomerId = o.CustomerId;
            this.SalesOrderNumber = o.SalesOrderNumber;
            //NE Developed by muthait for AFM_TFS_40686 dated 06/23/2014
            this.GrandTotal = o.GrandTotal;
            this.ShippingAddress = o.ShippingAddress;
            this.DeliveryModeId = o.DeliveryModeId;

            if (o.TenderLines) {
                this.TenderLines = [];
                for (var i = 0; i < o.TenderLines.length; i++) {
                    this.TenderLines[i] = o.TenderLines[i];
                }
            }
            this.EstShippingDiscount = o.EstShippingDiscount;
            this.EstHomeDeliveryDiscount = o.EstHomeDeliveryDiscount;

            //N - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
            this.RsaId = o.RsaId;
        }
    }

    // Shopping cart response class.
    export class ShoppingCartResponseClass implements ShoppingCartResponse {
        public ShoppingCart: ShoppingCart;
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            this.ShoppingCart = o.ShoppingCart;
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Shopping cart collection response class.
    export class ShoppingCartCollectionResponseClass implements ShoppingCartCollectionResponse {
        public ShoppingCarts: ShoppingCart[];
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            if (o.ShoppingCarts) {
                this.ShoppingCarts = [];
                for (var i = 0; i < o.ShoppingCarts.length; i++) {
                    this.ShoppingCarts[i] = o.ShoppingCarts[i] ? new ShoppingCartClass(o.ShoppingCarts[i]) : null;
                }
            }
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Shipping option class.
    export class ShippingOptionClass implements ShippingOption {
        public Id: string;
        public ShippingType: string;
        public Description: string;

        constructor(o: any) {
            o = o || {};
            this.Id = o.Id;
            this.ShippingType = o.ShippingType;
            this.Description = o.Description;
        }
    }

    // Shipping options class.
    export class ShippingOptionsClass implements ShippingOptions {
        public ShippingOptions: ShippingOption[];

        constructor(o: any) {
            o = o || {};
            if (o.ShippingOptions) {
                this.ShippingOptions = [];
                for (var i = 0; i < o.ShippingOptions.length; i++) {
                    this.ShippingOptions[i] = o.ShippingOptions[i] ? new ShippingOptionClass(o.ShippingOptions[i]) : null;
                }
            }
        }
    }

    // Item shipping options class.
    export class ItemShippingOptionsClass implements ItemShippingOptions {
        public LineId: string;
        public ShippingOptions: ShippingOptions;

        constructor(o: any) {
            o = o || {};
            this.LineId = o.LineId;
            this.ShippingOptions = o.ShippingOptions;
        }
    }

    // Shipping options response class.
    export class ShippingOptionResponseClass implements ShippingOptionResponse {
        public OrderShippingOptions: ShippingOptions;
        public ItemShippingOptions: ItemShippingOptions[];
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            this.OrderShippingOptions = o.OrderShippingOptions;
            if (o.ItemShippingOptions) {
                this.ItemShippingOptions = [];
                for (var i = 0; i < o.ItemShippingOptions.length; i++) {
                    this.ItemShippingOptions[i] = o.ItemShippingOptions[i] ? new ItemShippingOptionsClass(o.ItemShippingOptions[i]) : null;
                }
            }
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Sales line delivery preference class.
    export class LineDeliveryPreferenceClass implements LineDeliveryPreference {
        public LineId: string;
        public DeliveryPreferenceTypes: DeliveryPreferenceType[];

        constructor(o: any) {
            o = o || {};
            this.LineId = o.LineId;
            this.DeliveryPreferenceTypes = o.DeliveryPreferenceTypes;
        }
    }

    // Cart delivery preference  class.
    export class CartDeliveryPreferencesClass implements CartDeliveryPreferences {
        public HeaderDeliveryPreferenceTypes: DeliveryPreferenceType[];
        public LineDeliveryPreferences: LineDeliveryPreference[];

        constructor(o: any) {
            o = o || {};
            this.HeaderDeliveryPreferenceTypes = o.HeaderDeliveryPreferenceTypes;
            if (o.LineDeliveryPreferences) {
                this.LineDeliveryPreferences = [];
                for (var i = 0; i < o.LineDeliveryPreferences.length; i++) {
                    this.LineDeliveryPreferences[i] = o.LineDeliveryPreferences[i] ? new LineDeliveryPreferenceClass(o.LineDeliveryPreferences[i]) : null;
                }
            }
        }
    }

    // Delivery option class.
    export class DeliveryOptionClass implements DeliveryOption {
        public Id: string;
        public Description: string;

        constructor(o: any) {
            o = o || {};
            this.Id = o.Id;
            this.Description = o.Description;
        }
    }

    // Line delivery options class
    export class LineDeliveryOptionClass implements LineDeliveryOption {
        public LineId: string;
        public DeliveryOptions: DeliveryOption[];

        constructor(o: any) {
            o = o || {};
            this.LineId = o.LineId;
            if (o.DeliveryOptions) {
                this.DeliveryOptions = [];
                for (var i = 0; i < o.DeliveryOptions.length; i++) {
                    this.DeliveryOptions[i] = o.DeliveryOptions[i] ? new DeliveryOptionClass(o.DeliveryOptions[i]) : null;
                }
            }
        }
    }

    // Delivery options response class.
    export class DeliveryOptionsResponseClass implements DeliveryOptionsResponse {
        public DeliveryOptions: DeliveryOption[];
        public LineDeliveryOptions: LineDeliveryOption[];
        public Errors: Error[];
        public AddressVerficationResult: boolean;
        constructor(o: any) {
            o = o || {};
            if (o.DeliveryOptions) {
                this.DeliveryOptions = [];
                for (var i = 0; i < o.DeliveryOptions.length; i++) {
                    this.DeliveryOptions[i] = o.DeliveryOptions[i] ? new DeliveryOptionClass(o.DeliveryOptions[i]) : null;
                }
            }
            if (o.LineDeliveryOptions) {
                this.LineDeliveryOptions = [];
                for (var i = 0; i < o.LineDeliveryOptions.length; i++) {
                    this.LineDeliveryOptions[i] = o.LineDeliveryOptions[i] ? new LineDeliveryOptionClass(o.LineDeliveryOptions[i]) : null;
                }
            }
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
            this.AddressVerficationResult = o.AddressVerficationResult;
        }
    }

    // Payment card type class.
    export class PaymentCardTypeClass implements PaymentCardType {
        public Id: string;
        public CardType: string;
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            this.Id = o.Id;
            this.CardType = o.CardType;
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Payment card types response class.
    export class PaymentCardTypesResponseClass implements PaymentCardTypesResponse {
        public CardTypes: PaymentCardType[];
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            if (o.CardTypes) {
                this.CardTypes = [];
                for (var i = 0; i < o.CardTypes.length; i++) {
                    this.CardTypes[i] = o.CardTypes[i] ? new PaymentCardTypeClass(o.CardTypes[i]) : null;
                }
            }
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }   

    // Address class.
    export class AddressClass implements Address {
        public Country: string;
        public State: string;
        public County: string;
        public City: string;
        public DistrictName: string;
        public AttentionTo: string;
        public Name: string;
        //NS Developed by spriya writewebsale
        public FirstName: string;
        public LastName: string;
        //NE Developed by spriya writewebsale
        public Street: string;
        public Street2: string;
        public StreetNumber: string;
        public ZipCode: string;
        public Phone: string;
        public Email: string;
        public EmailContent: string;
        public RecordId: number;
        public Deactivate: boolean;
        public IsPrimary: boolean;
        public AddressType: AddressType;
        public AddressFriendlyName: string;
        public Email2: string;
        public Phone2: string;
        public Phone3: string;
        public Phone1Extension: string;
        public Phone2Extension: string;
        public Phone3Extension: string;

        constructor(o: any) {
            o = o || {};
            this.Country = o.Country;
            this.State = o.State;
            this.County = o.County;
            this.City = o.City;
            this.DistrictName = o.DisstrictName;
            this.AttentionTo = o.AttentionTo;
            this.Name = o.Name;
            //NS Developed by spriya writewebsale
            this.FirstName = o.FirstName;
            this.LastName = o.LastName;
            //NE Developed by spriya writewebsale
            this.Street = o.Street;
            this.Street2 = o.Street2;
            this.StreetNumber = o.StreetNumber;
            this.ZipCode = o.ZipCode;
            this.Phone = o.Phone;
            this.Email = o.Email;
            this.RecordId = o.RecordId;
            this.Deactivate = o.Deactivate;
            this.IsPrimary = o.IsPrimary;
            this.AddressType = o.AddressType;
            this.AddressFriendlyName = o.AddressFriendlyName;
            this.Email2 = o.Email2;
            this.Phone2 = o.Phone2;
            this.Phone3 = o.Phone3;
            this.Phone1Extension = o.Phone1Extension;
            this.Phone2Extension = o.Phone2Extension;
            this.Phone3Extension = o.Phone3Extension;
        }
    }

    // Address collection response class.
    export class AddressCollectionResponseClass implements AddressCollectionResponse {
        public Addresses: Address[];
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            if (o.Addresses) {
                this.Addresses = [];
                for (var i = 0; i < o.Addresses.length; i++) {
                    this.Addresses[i] = o.Addresses[i] ? new AddressClass(o.Addresses[i]) : null;
                }
            }
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Payment class.
    export class PaymentClass implements Payment {
        public PaymentAddress: Address;
        public CardNumber: string;
        public CardType: string;
        public CCID: string;
        public ExpirationMonth: number;
        public ExpirationYear: number;
        public NameOnCard: string;
        public LoyaltyCardId: string;
        public DiscountCode: string;
        public Last4Digits: string;
        public SynchronyPromoDesc: string;
        public FinancingOptionId: string;
        public MinimumPurchase: number;
        public RemoveCartDiscount: boolean;

        constructor(o: any) {
            o = o || {};
            this.PaymentAddress = o.PaymentAddress;
            this.CardNumber = o.CardNumber;
            this.CardType = o.CardType;
            this.CCID = o.CCID;
            this.ExpirationMonth = o.ExpirationMonth;
            this.ExpirationYear = o.ExpirationYear;
            this.NameOnCard = o.NameOnCard;
            this.LoyaltyCardId = o.LoyaltyCardId;
            this.DiscountCode = o.DiscountCode;
            this.Last4Digits = o.Last4Digits;
            this.SynchronyPromoDesc = o.SynchronyPromoDesc;
            this.FinancingOptionId = o.FinancingOptionId;
            this.MinimumPurchase = o.MinimumPurchase;
            this.RemoveCartDiscount = o.RemoveCartDiscount;
        }
    }

    // Payment card response class.
    export class PaymentCardResponseClass implements PaymentCardResponse {
        public PaymentCards: Payment[];

        constructor(o: any) {
            o = o || {};
            if (o.PaymentCards) {
                this.PaymentCards = [];
                for (var i = 0; i < o.PaymentCards.length; i++) {
                    this.PaymentCards[i] = o.PaymentCards[i] ? new PaymentClass(o.PaymentCards[i]) : null;
                }
            }
        }
    }

    // Tokenized payment card class.
    export class TokenizedPaymentCardClass implements TokenizedPaymentCard {
        public PaymentAddress: Address;
        public CardType: string;
        public ExpirationMonth: number;
        public ExpirationYear: number;
        public NameOnCard: string;
        public CardToken: string;
        public UniqueCardId: string;
        public MaskedCardNumber: string;
        public SynchronyPromoDesc: string;
        public FinancingOptionId: string;
        public MinimumPurchase: number;
        public RemoveCartDiscount: boolean;

        constructor(o: any) {
            o = o || {};
            this.PaymentAddress = o.PaymentAddress;
            this.CardType = o.CardType;
            this.ExpirationMonth = o.ExpirationMonth;
            this.ExpirationYear = o.ExpirationYear;
            this.NameOnCard = o.NameOnCard;
            this.CardToken = o.CardToken;
            this.UniqueCardId = o.UniqueCardId;
            this.MaskedCardNumber = o.MaskedCardNumber;
            this.SynchronyPromoDesc = o.SynchronyPromoDesc;
            this.FinancingOptionId = o.FinancingOptionId;
            this.MinimumPurchase = o.MinimumPurchase;
            this.RemoveCartDiscount = o.RemoveCartDiscount;
        }
    }

    // Error class.
    export class ErrorClass implements Error {
        public ErrorCode: string;
        public ErrorMessage: string;

        constructor(o: any) {
            o = o || {};
            this.ErrorCode = o.ErrorCode;
            this.ErrorMessage = o.ErrorMessage;
        }
    }

    // Create sales order response class.
    export class CreateSalesOrderResponseClass implements CreateSalesOrderResponse {
        public OrderNumber: string;
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            this.OrderNumber = o.OrderNumber;
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Store product item availability class.
    export class StoreProductAvailabilityItemClass implements StoreProductAvailabilityItem {
        public RecordId: number;
        public ItemId: string;
        public VariantInventoryDimensionId: string;
        public WarehouseInventoryDimensionId: string;
        public InventoryLocationId: string;
        public AvailableQuantity: number;
        public ProductDetails: string;

        constructor(o: any) {
            o = o || {};
            this.RecordId = o.RecordId;
            this.ItemId = o.ItemId;
            this.VariantInventoryDimensionId = o.VariantInventoryDimensionId;
            this.WarehouseInventoryDimensionId = o.WarehouseInventoryDimensionId;
            this.InventoryLocationId = o.InventoryLocationId;
            this.AvailableQuantity = o.AvailableQuantity;
            this.ProductDetails = o.ProductDetails;
        }
    }

    // Store product availability class.
    export class StoreProductAvailabilityClass implements StoreProductAvailability {
        public ChannelId: number;
        public Latitude: number;
        public Longitude: number;
        public Distance: string;
        public InventoryLocationId: string;
        public StoreId: string;
        public StoreName: string;
        public PostalAddressId: string;
        public ProductAvailabilities: StoreProductAvailabilityItem[];
        public SelectDisabled: string;
        public Country: string;
        public State: string;
        public County: string;
        public City: string;
        public DistrictName: string;
        public AttentionTo: string;
        public Name: string;
        public Street: string;
        public StreetNumber: string;
        public ZipCode: string;
        public Phone: string;
        public Email: string;
        public EmailContent: string;
        public RecordId: number;
        public Deactivate: boolean;
        public IsPrimary: boolean;
        public AddressType: AddressType;
        public AddressFriendlyName: string;
        public AreItemsAvailableInStore: boolean;

        constructor(o: any) {
            o = o || {};
            this.ChannelId = o.ChannelId;
            this.Latitude = o.Latitude;
            this.Longitude = o.Longitude;
            this.Distance = o.Distance;
            this.InventoryLocationId = o.InventoryLocationId;
            this.StoreId = o.StoreId;
            this.StoreName = o.StoreName;
            this.PostalAddressId = o.PostalAddressId;
            if (o.ProductAvailabilities) {
                this.ProductAvailabilities = [];
                for (var i = 0; i < o.ProductAvailabilities.length; i++) {
                    this.ProductAvailabilities[i] = o.ProductAvailabilities[i] ? new StoreProductAvailabilityItemClass(o.ProductAvailabilities[i]) : null;
                }
            }
            this.SelectDisabled = o.SelectDisabled;
            this.Country = o.Country;
            this.State = o.State;
            this.County = o.County;
            this.City = o.City;
            this.DistrictName = o.DisstrictName;
            this.AttentionTo = o.AttentionTo;
            this.Name = o.Name;
            this.Street = o.Street;
            this.StreetNumber = o.StreetNumber;
            this.ZipCode = o.ZipCode;
            this.Phone = o.Phone;
            this.Email = o.EMail;
            this.RecordId = o.RecordId;
            this.Deactivate = o.Deactivate;
            this.IsPrimary = o.IsPrimary;
            this.AddressType = o.AddressType;
            this.AddressFriendlyName = o.AddressFriendlyName;
            this.AreItemsAvailableInStore = o.AreItemsAvailableInStore;
        }
    }

    // Store location class.
    export class StoreLocationClass implements StoreLocation {
        public ChannelId: number;
        public Latitude: number;
        public Longitude: number;
        public Distance: string;
        public InventoryLocationId: string;
        public StoreId: string;
        public StoreName: string;
        public PostalAddressId: string;
        public Country: string;
        public State: string;
        public County: string;
        public City: string;
        public DistrictName: string;
        public AttentionTo: string;
        public Name: string;
        public Street: string;
        public StreetNumber: string;
        public ZipCode: string;
        public Phone: string;
        public Email: string;
        public EmailContent: string;
        public RecordId: number;
        public Deactivate: boolean;
        public IsPrimary: boolean;
        public AddressType: AddressType;
        public AddressFriendlyName: string;

        constructor(o: any) {
            o = o || {};
            this.ChannelId = o.ChannelId;
            this.Latitude = o.Latitude;
            this.Longitude = o.Longitude;
            this.Distance = o.Distance;
            this.InventoryLocationId = o.InventoryLocationId;
            this.StoreId = o.StoreId;
            this.StoreName = o.StoreName;
            this.PostalAddressId = o.PostalAddressId;
            this.Country = o.Country;
            this.State = o.State;
            this.County = o.County;
            this.City = o.City;
            this.DistrictName = o.DisstrictName;
            this.AttentionTo = o.AttentionTo;
            this.Name = o.Name;
            this.Street = o.Street;
            this.StreetNumber = o.StreetNumber;
            this.ZipCode = o.ZipCode;
            this.Phone = o.Phone;
            this.Email = o.EMail;
            this.RecordId = o.RecordId;
            this.Deactivate = o.Deactivate;
            this.IsPrimary = o.IsPrimary;
            this.AddressType = o.AddressType;
            this.AddressFriendlyName = o.AddressFriendlyName;
        }
    }

    // Country Info class.
    export class CountryInfoClass implements CountryInfo {
        public CountryCode: string;
        public CountryName: string;

        constructor(o: any) {
            o = o || {};
            this.CountryCode = o.CountryCode;
            this.CountryName = o.CountryName;
        }
    }

    // Tender data line class.
    export class TenderDataLineClass implements TenderDataLine {
        public Amount: string;
        public GiftCardId: string;
        public LoyaltyCardId: string;
        public PaymentCard: Payment;
        public TokenizedPaymentCard: TokenizedPaymentCard;

        constructor(o: any) {
            o = o || {};
            this.Amount = o.Amount;
            this.GiftCardId = o.GiftCardId;
            this.LoyaltyCardId = o.LoyaltyCardId;
            this.PaymentCard = o.PaymentCard;
            this.TokenizedPaymentCard = o.TokenizedPaymentCard;
        }
    }

    // Delivery preference response class.
    export class DeliveryPreferenceResponseClass implements DeliveryPreferenceResponse {
        public CartDeliveryPreferences: CartDeliveryPreferences;
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            this.CartDeliveryPreferences = o.CartDeliveryPreferences ? new CartDeliveryPreferencesClass(o.CartDeliveryPreferences) : null;
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Store product availability response class.
    export class StoreProductAvailabilityResponseClass implements StoreProductAvailabilityResponse {
        public Stores: StoreProductAvailability[];
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            if (o.Stores) {
                this.Stores = [];
                for (var i = 0; i < o.Stores.length; i++) {
                    this.Stores[i] = o.Stores[i] ? new StoreProductAvailabilityClass(o.Stores[i]) : null;
                }
            }
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Store location response class.
    export class StoreLocationResponseClass implements StoreLocationResponse {
        public Stores: StoreLocation[];
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            if (o.Stores) {
                this.Stores = [];
                for (var i = 0; i < o.Stores.length; i++) {
                    this.Stores[i] = o.Stores[i] ? new StoreLocationClass(o.Stores[i]) : null;
                }
            }
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Country info response class.
    export class CountryInfoResponseClass implements CountryInfoResponse {
        public Countries: CountryInfo[];
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            if (o.Countries) {
                this.Countries = [];
                for (var i = 0; i < o.Countries.length; i++) {
                    this.Countries[i] = o.Countries[i] ? new CountryInfoClass(o.Countries[i]) : null;
                }
            }
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Boolean response class.
    export class BooleanResponseClass implements BooleanResponse {
        public IsTrue: boolean;
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            this.IsTrue = o.IsTrue;
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // String response class.
    export class StringResponseClass implements StringResponse {
        public Value: string;
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            this.Value = o.Value;
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Tender types response class.
    export class TenderTypesResponseClass implements TenderTypesResponse {
        public HasCreditCardPayment: boolean;
        public HasGiftCardPayment: boolean;
        public HasLoyaltyCardPayment: boolean;
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            this.HasCreditCardPayment = o.HasCreditCardPayment;
            this.HasGiftCardPayment = o.HasGiftCardPayment;
            this.HasLoyaltyCardPayment = o.HasLoyaltyCardPayment;
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Gift card information class.
    export class GiftCardInformationClass implements GiftCardInformation {
        public GiftCardId: string;
        public Balance: number;
        public CurrencyCode: string;
        public IsInfoAvailable: boolean;

        constructor(o: any) {
            o = o || {};
            this.GiftCardId = o.GiftCardId;
            this.Balance = o.Balance;
            this.CurrencyCode = o.CurrencyCode;
            this.IsInfoAvailable = o.IsInfoAvailable;
        }
    }

    // Gift card response class.
    export class GiftCardResponseClass implements GiftCardResponse {
        public GiftCardInformation: GiftCardInformation;
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            this.GiftCardInformation = o.GiftCardInformation;
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // Loyalty card class.
    export class LoyaltyCardClass implements LoyaltyCard {
        public CardNumber: string;
        public CardTenderType: number;

        constructor(o: any) {
            o = o || {};
            this.CardNumber = o.CardNumber;
            this.CardTenderType = o.CardTenderType;
        }
    }

    // Loyalty card response class.
    export class LoyaltyCardsResponseClass implements LoyaltyCardsResponse {
        public LoyaltyCards: LoyaltyCard[];
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            this.LoyaltyCards = [];
            for (var i = 0; i < o.LoyaltyCards.length; i++) {
                this.LoyaltyCards[i] = o.LoyaltyCards[i] ? new LoyaltyCardClass(o.LoyaltyCards[i]) : null;
            }
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }

    // State/province info class.
    export class StateProvinceInfoClass implements StateProvinceInfo {
        public CountryRegionId: string;
        public StateId: string;
        public StateName: string;

        constructor(o: any) {
            o = o || {};
            this.CountryRegionId = o.CountryRegionId;
            this.StateId = o.StateId;
            this.StateName = o.StateName;
        }
    }

    // State/province info response class.
    export class StateProvinceInfoResponseClass implements StateProvinceInfoResponse {
        public StateProvinces: StateProvinceInfo[];
        public Errors: Error[];

        constructor(o: any) {
            o = o || {};
            if (o.Countries) {
                this.StateProvinces = [];
                for (var i = 0; i < o.StateProvinces.length; i++) {
                    this.StateProvinces[i] = o.StateProvinces[i] ? new StateProvinceInfoClass(o.StateProvinces[i]) : null;
                }
            }
            if (o.Errors) {
                this.Errors = [];
                for (var i = 0; i < o.Errors.length; i++) {
                    this.Errors[i] = o.Errors[i] ? new ErrorClass(o.Errors[i]) : null;
                }
            }
        }
    }    

    export interface AFMOrderConfirmationResponse {        
        ShoppingCart: ShoppingCart;
        ShippingAddress: Address;
        BillingAddress: Address;
    }

    // AFMOrderConfirmationResponse class.
    export class AFMOrderConfirmationResponseClass implements AFMOrderConfirmationResponse {
        ShoppingCart: ShoppingCart;
        ShippingAddress: Address;
        BillingAddress: Address;

        constructor(o: any) {
            o = o || {};
            this.ShoppingCart = o.ShoppingCart;
            this.ShippingAddress = o.ShippingAddress;
            this.BillingAddress = o.BillingAddress;           
        }
    }
}
