﻿/*
NS by muthait dated 12 Sep 2014
*/

/// <reference path="../../ExternalScripts/JQuery.d.ts" />
/// <reference path="../../ExternalScripts/KnockoutJS.d.ts" />
/// <reference path="../../ExternalScripts/spin.d.ts" />
/// <reference path="../../Resources/Resources.ts" />

// Initialize page
$(() => {
    AFM.Ecommerce.Controls.ResourcesHandler.selectUICulture();
});

$(document).on('CreateCartData', function (event, data) {
    AFM.Ecommerce.Controls.CartBase.CreateCartData();
});

function findBrowser() {
    var ua = navigator.userAgent, tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) return 'Opera ' + tem[1];
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
    return M.join(' ');
}

function get_hostname(url) {
    var m = url.match(/^http:\/\/[^/]+/);
    return m ? m[0] : null;
}


//function ViewModel(element) {
//    var data = this;
//    data.Cart = new AFM.Ecommerce.Controls.Cart();
//    data.OrderSummary = new AFM.Ecommerce.Controls.OrderSummary();
//    data.OrderDetail = new AFM.Ecommerce.Controls.OrderDetail(element);
//    data.OrderDetail2 = new AFM.Ecommerce.Controls.OrderDetail(element);
//    data.ButtonControls = new AFM.Ecommerce.Controls.ButtonControls(element);
//}

// Show errors
var msaxError = {
    show: (level, message, errorCodes?) => {
        console.error(message);
    }
};


/* This module is to simulate the existence of the variables that are being registered from the server side */
module msaxValues {
    export var msax_CurrencyTemplate;
    export var msax_CheckoutServiceUrl;
    export var msax_ShoppingCartServiceUrl;
    export var msax_OrderConfirmationUrl;
    export var msax_IsDemoMode;
    export var msax_DemoDataPath;
    export var msax_StoreProductAvailabilityServiceUrl;
    export var msax_ChannelServiceUrl;
    export var msax_LoyaltyServiceUrl;
    export var msax_CustomerServiceUrl;
    export var msax_HasInventoryCheck;
    export var msax_CheckoutUrl;
    export var msax_IsCheckoutCart;
    export var msax_ContinueShoppingUrl;
    export var msax_CartDiscountCodes;
    export var msax_CartLoyaltyReward;
    export var msax_ShoppingCartUrl;
    export var msax_CartDisplayPromotionBanner;
    export var msax_ReviewDisplayPromotionBanner;
    //NS by muthait
    export var msax_AFMExtendedServiceUrl;
    export var msax_CurrentPageMode;
    export var msax_TransactionId;
    export var msax_ViewTax;
    export var msax_ViewDiscount;
    export var msax_ShoppingCartReadMode;
    export var msax_IsEditable = [];
    export var msax_PageMode: AFM.Ecommerce.Controls.PageModeOptions;
    export var msax_LocalPageMode:AFM.Ecommerce.Controls.PageMode;
    //export var msax_PaymentGatewayUrl;
    export var msax_LoginMode;
    export var msax_SigInUrl;
    export var msax_GoBackUrl;
    export var msax_ErrorPageUrl;
    //NE by muthait

    //NS HXM CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
    export var msax_SurveyTermsAndConditions;
    export var msax_IsAcceptTermsAndConditions: AFM.Ecommerce.Controls.AFMSalesTransHeader;
    //NE HXM CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
    export var msax_MinimumPurchaseSynchronyCart;
    export var msax_Synchronycart;

}

/* This module is to simulate the existence of Bing maps APIs */
module AFM.Maps {
    export var Map;
    export var loadModule;
    export var Search;
    export var Events;
    export var Pushpin;
    export var Infobox;
    export var Point;
}


module AFM.Ecommerce.Controls {

    export interface Parameter {
        key: string;
        value:boolean;
    }


    export class AjaxProxy {
        private relativeUrl;

        constructor(relativeUrl) {
            this.relativeUrl = relativeUrl;
            $(document).ajaxError(this.ajaxErrorHandler);
        }


        private ajaxErrorHandler(e, xhr, settings) {
            var errorMessage =
                'Url:\n' + settings.url +
                '\n\n' +
                'Response code:\n' + xhr.status +
                '\n\n' +
                'Status Text:\n' + xhr.statusText +
                '\n\n' +
                'Response Text: \n' + xhr.responseText;

            msaxError.show('error', 'The web service call was unsuccessful.  Details: ' + errorMessage);
        }

        public SubmitRequest = function (webMethod, data, successCallback, errorCallback) {

            // Example: http://www.contoso.com:40002/sites/retailpublishingportal + /_vti_bin/ShoppingCartService.svc/ + GetShoppingCart
            var webServiceUrl = this.relativeUrl + webMethod;

            var requestDigestHeader = (<HTMLInputElement>($(document).find('#__REQUESTDIGEST'))[0]);
            var retailRequestDigestHeader = (<HTMLInputElement>($(document).find('#__RETAILREQUESTDIGEST'))[0]);
            var requestDigestHeaderValue;
            var retailRequestDigestHeaderValue;

            if (Utils.isNullOrUndefined(requestDigestHeader) || Utils.isNullOrUndefined(retailRequestDigestHeader)) {
                requestDigestHeaderValue = null;
                retailRequestDigestHeaderValue = null;
            }
            else {
                requestDigestHeaderValue = requestDigestHeader.value;
                retailRequestDigestHeaderValue = retailRequestDigestHeader.value;
            }

            // Submit the AJAX call using jQuery.
            $.ajax({
                url: webServiceUrl,
                data: JSON.stringify(data),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.RedirectUrl) {
                        window.location = data.RedirectUrl;
                    }
                    else {
                        successCallback(data);
                    }
                },
                error: function (error) {
                    errorCallback(error);
                },
                headers: {
                    "X-RequestDigest": requestDigestHeaderValue,
                    "X-RetailRequestDigest": retailRequestDigestHeaderValue
                }
            });
        };
    }

    export class LoadingOverlay {

    public static opts = {
        /* spin.js default options – See spin.js web site for more options and documentation */
        lines: 12, // The number of lines to draw
        length: 20, // The length of each line
        width: 10, // The line thickness
        radius: 20, // The radius of the inner circle
        speed: 0.8,
        color: '#042a5f', // #rgb or #rrggbb or array of colors
        hwaccel: true, // Whether to use hardware acceleration
        shadow: false,
        className: '.spinner', // The CSS class to assign to the spinner

        /* this plugin's options*/
        delay: 1000, /* How long to delay before showing spinner so it doesn’t show on quick operations.
                           Backdrop always shows immediately and ignores this value. */
        backdrop: 'white',
        backdropOpacity: 0.8,
        position: 'fixed', // This can be any of the css position values but you’ll likely only want to use ‘fixed’ or ‘absolute’
        stopOnAjaxError: true,
        stopOnReveal: true
    };

        public static spinner;

        public static CreateLoadingDialog(loadingDialog: any, width: number, height: number) {
            // Creates the loading overlay dialog box.
            //loadingDialog.dialog({
            //    modal: true,
            //    autoOpen: false,
            //    draggable: true,
            //    resizable: false,
            //    position: ['top', 100],
            //    show: { effect: "fadeIn", duration: 500 },
            //    hide: { effect: "fadeOut", duration: 500 },
            //    open: function (event, ui) {
            //        setTimeout(function () {
            //            loadingDialog.dialog('close');
            //        }, 50000); // If the service call does not return (does not fail or succeed) for some reason then we close the dialog on a timeout.
            //    },
            //    width: width,
            //    height: height
            //});

            //$('.ui-dialog').addClass('msax-Control');
                      

            LoadingOverlay.spinner = new Spinner(LoadingOverlay.opts);
            LoadingOverlay.spinner.spin(document.getElementsByClassName('msax-Loading')[0]);
            


        }

        public static ShowLoadingDialog(loadingDialog: any, loadingText: any, text?: string) {
            // Displays the loading dialog.
            //if (Utils.isNullOrWhiteSpace(text)) {
            //loadingText.text(Resources.String_176); // Loading ...
            //}
            //else {
            //    loadingText.text(text);
            //}

            //loadingDialog.dialog('open');
            $(loadingDialog).show();
        }

        public static CloseLoadingDialog(loadingDialog: any) {
            // Close the dialog.
            //loadingDialog.dialog('close');
            $(loadingDialog).hide();
        }

        public static CreateRemoveItemDialog(loadingDialog: any, callerContext: any, handler: any, callerContextCancel: any, handlerCancel: any, RemoveButtonText:string,CancelButtonText:string) {
            loadingDialog.dialog({
                autoOpen: false,
                height: 200,
                width: 500,
                modal: true,                
                buttons: {
                    "Remove from Cart": $.proxy(handler, callerContext),
                    "Cancel": $.proxy(handlerCancel, callerContextCancel)
                },
                close: function () {
                    loadingDialog.dialog("close");
                }
            });

            $('.ui-dialog').addClass('msax-Control');
        }

        // NS - Changed by Aniket for edit label feature. Added button label parameter on 30/12/2015
        //public static openAddressDialog(loadingDialog: any, handler: any, handlerCancel: any, callerContext: any, htmlContent: any, UseAddressButtonText:string, CancelButtonText:string) {
        //    loadingDialog.removeClass('msax-DisplayNone');
        //    $('.msax-AddressReplaceConfrimText').html(htmlContent);
        //    loadingDialog.dialog({
        //        autoOpen: true,
        //        height: "auto",
        //        width: 600,
        //        modal: true,
        //        buttons: {
        //            UseAddressButtonText: $.proxy(handler, callerContext),
        //            CancelButtonText: $.proxy(handlerCancel, callerContext)
        //        },
        //        close: function () {
        //            loadingDialog.dialog("close");
        //        }
        //    });
        //    $('.ui-dialog').addClass('msax-Control');          
        //}

        public static openAddressDialog(loadingDialog: any, handler: any, handlerCancel: any, callerContext: any, htmlContent: any, UseAddressButtonText: string, CancelButtonText: string) {
            loadingDialog.removeClass('msax-DisplayNone');
            $('.msax-AddressReplaceConfrimText').html(htmlContent);
            loadingDialog.dialog({
                autoOpen: true,
                height: "auto",
                width: 600,
                modal: true,
                buttons: [{
                    text: UseAddressButtonText,
                    click: $.proxy(handler, callerContext)
                },
                {
                    text:CancelButtonText,
                    click: $.proxy(handlerCancel, callerContext)
                }],
                close: function () {
                    loadingDialog.dialog("close");
                }
            });
            $('.ui-dialog').addClass('msax-Control');
        }
        // NE - Changed by Aniket for edit label feature. Added button label parameter on 30/12/2015

        public static openRemoveItemDialog(loadingDialog: any, dialogText: any,text:string)
        {           
            dialogText.text(text);
            loadingDialog.dialog("open");
        }

        public static closeRemoveItemDialog(loadingDialog: any) {
            loadingDialog.dialog("close");
        }

        public static openAddressItemDialog(loadingDialog: any) {
            loadingDialog.dialog("open");
        }

    }
}
