﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

/// <reference path="../../Resources/Resources.ts" />

module AFM {
    export class Utils {
        // Test to see if the object is null or undefined.
        public static isNullOrUndefined(o): boolean {
            return (o === undefined || o === null);
        }

        // Tests to see if an object is null or empty.
        public static isNullOrEmpty(o): boolean {
            return (Utils.isNullOrUndefined(o) || o === '');
        }

        // Tests to see if an object is null or contains just white space.
        public static isNullOrWhiteSpace(o: string): boolean {
            return (Utils.isNullOrEmpty(o) || (typeof o === 'string' && o.replace(/\s/g, '').length < 1));
        }

        // Test to see if the array has elements.
        public static hasElements(o: any[]): boolean {
            return !Utils.isNullOrUndefined(o) && o.length > 0;
        }

        // Return specified default value if object is null or undefined or whitespace.
        public static getValueOrDefault(o: string, defaultValue): string {
            return Utils.isNullOrWhiteSpace(o) ? defaultValue : o;
        }

        // Tests to see if an object has errors.
        public static hasErrors(o: any): boolean {
            return (!Utils.isNullOrUndefined(o) && !this.hasElements(o.Errors));
        }


        /**
        * Basic C# like string format function.
        */
        public static format(object: string, ...params: any[]): string {
            if (Utils.isNullOrWhiteSpace(object)) {
                return object;
            }

            if (params == null) {
                throw AFM.Ecommerce.Controls.Resources.String_70;
            }

            for (var index = 0; index < params.length; index++) {
                if (params[index] == null) {
                    throw AFM.Ecommerce.Controls.Resources.String_70;
                }

                var regexp = new RegExp('\\{' + index + '\\}', 'gi');
                object = object.replace(regexp, params[index]);
            }

            return object;
        }

        public static CurrencyString(doubleValue, usa) {
            var dec = String(doubleValue).split(/[.,]/)
            , sep = usa ? ',' : '.'
            , decsep = usa ? '.' : ',';
            var currency = usa ? '$' : ''; 
            var seperateddata = currency + Utils.xsep(dec[0], sep) + (dec[1] ? decsep + dec[1] : '');
            if (seperateddata.split('.').length <= 1)
                seperateddata = seperateddata + ".00";
            else if (seperateddata.split('.').length == 2)
            {
                var decimalData = seperateddata.substring(seperateddata.lastIndexOf('.') + 1, seperateddata.length);
                if (!Utils.isNullOrEmpty(decimalData) && decimalData.length == 1)
                    seperateddata = seperateddata + "0";
            }
            return seperateddata;
        
        }

        public static xsep(num, sep) {
            var n = String(num).split('')
                , i = -3;
            while (n.length + i > 0) {
                n.splice(i, 0, sep);
                i -= 4;
            }
            return n.join('');
        }

        public static indexOf(needle,property) {
            if (typeof Array.prototype.indexOf === 'function') {
                Utils.indexOf = Array.prototype.indexOf;
            } else {
                Utils.indexOf = function (needle) {
                    var i = -1, index = -1;

                    for (i = 0; i < this.length; i++) {
                        if (this[i][property] === needle) {
                            index = i;
                            break;
                        }
                    }

                    return index;
                };
            }

            return Utils.indexOf.call(this, needle, property);
        }

      
        public static Trim(x) {
            if (x != null)
                return x.replace(/^\s+|\s+$/gm, '');
            else
                return x;
    }

        public static SetPhoneFormat(x) {
            if (x != null)
            {
                if (x.length >= 10) {
                    x = x.replace(/[\s\(\)-]+/g, ''); 
                    x = '(' + x.substr(0, 3) + ') ' + x.substr(3, 3) + '-' + x.substr(6, x.length - 1);
                }
                return x;
            }
            else
                return x;
        }

        public static RemovePhoneFormat(x) {
            if (x != null) {
                if (x.length >= 10) {
                    x = x.replace(/[\s\(\)-]+/g, '');
                }
                return x;
            }
            else
                return x;
        }

        
    }
}