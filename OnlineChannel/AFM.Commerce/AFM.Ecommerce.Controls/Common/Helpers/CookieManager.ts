﻿/*
NS by muthait dated 17 Sep 2014
*/


/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />

module AFM.Ecommerce.Controls {
    "use strict";

    export class CookieManager {

        private static afmZipCode = "AFMZipCode";
        private static shoppingcartId = "sct";

        public static getCookie(cookieName: string): string {
            var nameWithEqSign = cookieName + "=";
            var allCookies = document.cookie.split(';');
            for (var i = 0; i < allCookies.length; i++) {
                var singleCookie = allCookies[i];
                while (singleCookie.charAt(0) == ' ') {
                    singleCookie = singleCookie.substring(1, singleCookie.length);
                }
                if (singleCookie.indexOf(nameWithEqSign) == 0) {
                    return singleCookie.substring(nameWithEqSign.length, singleCookie.length);
                }
            }
            return null;
        }

        public static setSessionCookie(cname, cvalue) {
            document.cookie = cname + "=" + cvalue + "; ";
        } 

        public static setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires;
        } 
    }
}