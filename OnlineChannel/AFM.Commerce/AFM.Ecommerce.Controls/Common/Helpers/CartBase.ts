﻿
/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../ExternalScripts//jquery.tooltipster.d.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../../Resources/Resources.ts" />

module AFM.Ecommerce.Controls {
    "use strict";

    export var CartData;
    export var errorMessage = '';
    export var errorPanel = $(document).find(".msax-ErrorPanel");
    export var CardToken = ko.observable<string>(null);;
    export var TenderLinesData: TenderDataLine[] = [];
    export var AFMOrderSummaryData = new AFMOrderSummary(null);
    export var PaymentCardData;
    export var IsConfirmedit = false;//N developed by V- Hapat page mode navigation
    export var AFMZipCodeData = ko.observable<string>(null);
    export var ErrorCount = 0;
    export var SynchronyMinPurchaseAmount = ko.observable<number>(0);

    export class CartBase {
        public static _cartView = $(document);
        public static _loadingDialog;
        public static _loadingText;

        public static CreateCartData() {

            CartBase._loadingDialog = CartBase._cartView.find('.msax-Loading');
            LoadingOverlay.CreateLoadingDialog(CartBase._loadingDialog, 300, 100);
            CartBase.HideAllControls();
            CartBase.LoadBaseData();
            if (window.location.hash.split('#').length > 1) {
                if (window.location.hash.split('#')[1].toLowerCase() == PageMode.Shipping) {
                    AFMExtendedService.CommenceCheckout(ShoppingCartDataLevel.All).done((data) => {
                        CartData(data.ShoppingCart);
                        CartBase.GetUserPageMode();
                    });
                    msaxValues.msax_LocalPageMode = PageMode.Shipping;
                }
                else if (window.location.hash.split('#')[1].toLowerCase() == PageMode.Billing) {
                    msaxValues.msax_LocalPageMode = PageMode.Billing;
                    CartBase.GetUserPageMode();
                } else if (window.location.hash.split('#')[1].toLowerCase() == PageMode.Payment) {
                    msaxValues.msax_LocalPageMode = PageMode.Payment;
                    CartBase.GetUserPageMode();
                } else if (window.location.hash.split('#')[1].toLowerCase() == PageMode.Confirm) {
                    msaxValues.msax_LocalPageMode = PageMode.Confirm;
                    CartBase.GetUserPageMode();
                } else if (window.location.hash.split('#')[1].toLowerCase() == PageMode.cartview) {
                    msaxValues.msax_LocalPageMode = PageMode.cartview;
                    CartBase.GetUserPageMode();
                }
                else if (window.location.hash.split('#')[1].toLowerCase() == PageMode.thankyou) {
                    msaxValues.msax_LocalPageMode = PageMode.thankyou;
                    CartBase.GetUserPageMode();
                }
                else {
                    location.hash = "shipping";
                    CartBase.CreateCartData();
                }

            } else {
                CartBase.GetUserPageMode();
            }



                      
        }

        public static GetUserPageMode() {
            AFMExtendedService.GetUserPageMode().done((data) => {
                msaxValues.msax_PageMode = data.PageMode;
                if (msaxValues.msax_LocalPageMode != PageMode.Shipping) {
                    AFM.Ecommerce.Controls.CartBase.LoadInitialData();
                }
                else
                    AFM.Ecommerce.Controls.CartBase.DataModelLoad();
            }).fail((error) => {
                    msaxValues.msax_PageMode = PageModeOptions.CartView;
                    AFM.Ecommerce.Controls.CartBase.LoadInitialData();
                    AFM.Ecommerce.Controls.CartBase.DataModelLoad();
                });
        }

        public static LoadBaseData() {
            if (Utils.isNullOrUndefined(CartData)) {
                var cart = new ShoppingCartClass(null);
                cart.SelectedDeliveryOption = new SelectedDeliveryOptionClass(null);
                cart.SelectedDeliveryOption.CustomAddress = new AddressClass(null);
                cart.Items = [];
                cart.DiscountCodes = [];
                cart.ShippingAddress = new AddressClass(null);
                CartData = ko.observable<ShoppingCart>(cart);

            }
        }

        public static LoadInitialData() {
            var CheckoutDisable = $(document).find('.msax-CheckoutButton');
            if (Utils.isNullOrEmpty(msaxValues.msax_PageMode) || msaxValues.msax_PageMode == PageModeOptions.CartView) {
                ShoppingCartService.GetShoppingCart(ShoppingCartDataLevel.All, false).done((data) => {
                    CartData(data.ShoppingCart);
                    AFM.Ecommerce.Controls.CartBase.DataModelLoad();
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        errorMessage = data.Errors[0].ErrorMessage;
                        this.showError(true);
                        CheckoutDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
                        CheckoutDisable.addClass("disableButton");
                        ErrorCount = data.Errors.length;
                    }
                    else {
                        this.showError(false);
                        CheckoutDisable.removeAttr('disabled').css('mouse', 'cursor');
                        CheckoutDisable.removeClass("disableButton");
                    }
                });
            }
            else if (msaxValues.msax_PageMode == PageModeOptions.ThankYou) {
                AFMExtendedService.GetOrderConfirmationDetails().done((data) => {
                    //CartData(data.ShoppingCart);
                    AFM.Ecommerce.Controls.CartBase.DataModelLoad();
                }).fail((error) => {
                        errorMessage = Resources.String_1151;
                        CartBase.showError(true);
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    });
            }
            else {
                   ShoppingCartService.GetShoppingCart(ShoppingCartDataLevel.All, true).done((data) => {
                    CartData(data.ShoppingCart);
                    AFM.Ecommerce.Controls.CartBase.DataModelLoad();
                    if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                        errorMessage = data.Errors[0].ErrorMessage;
                        ErrorCount = data.Errors.length;
                        //CS Bug: 91822 Dated 24/08/2015 Developed by spriya 
                        if (!Utils.isNullOrUndefined(data.ShoppingCart.ATPError) && (msaxValues.msax_PageMode == PageModeOptions.BillingView || msaxValues.msax_PageMode == PageModeOptions.BillingViewcompleted))
                            $(document).find('.msax - ErrorPanel').hide();
                        else
                            this.showError(true);
                        //CE Bug: 91822 Dated 24/08/2015 Developed by spriya 
                        $(document).find('.msax-SubmitOrder').hide();
                    }
                    else {
                        $(document).find('.msax-SubmitOrder').show();
                        this.showError(false);
                    }

                }).fail((error) => {
                        if (!Utils.isNullOrEmpty(msaxValues.msax_ErrorPageUrl))
                            window.location.href = msaxValues.msax_ErrorPageUrl;
                    });
            }
        }
        public static DataModelLoad() {

            $('.msax-Control').each((index, element) => {
                var viewModelName = $(element.firstElementChild).attr("data-model");
                var viewModel = eval(viewModelName);
                if (viewModelName != undefined)
                    ko.applyBindings(new viewModel(element.firstElementChild), element);
            });
            CartBase.ClearPageMode();
            CartBase.UpdateFocusPageModeValue(msaxValues.msax_PageMode);
        }


        private static HideAllControls() {
            $(document).find('.msax-ShoppingCart').hide();
            $(document).find('.msax-OrderSummary').hide();
            $(document).find('.msax-ProceedtoCartButtons').hide();
            $(document).find('.msax-SubmitOrderButtons').hide();
            $(document).find('.msax-ShippingAddressCollection').hide();
            $(document).find('.msax-ShippingAddress').hide();
            $(document).find('.msax-BillingAddress').hide();
            $(document).find('.msax-EditAddress').hide();
            $(document).find('.msax-PromotionCode').hide();            
            //N - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
            $(document).find('.msax-RsaId').hide();
            //CartBase._loadingText = CartBase._loadingDialog.find('.msax-LoadingText');
            //CartBase._loadingDialog.find('.msax-LoadingText');
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179);

        }
        public static UpdateCartData(cartData) {
            CartData(cartData);
        }

        public static ClearPageMode() {
            $(document).trigger('ClearPageMode');
        }

        public static UpdateFocusPageModeValue(pageMode: PageModeOptions) {
            CartBase.HideAllControls();
            var pageModeValue = CartBase.FindPageMode(pageMode);
            msaxValues.msax_PageMode = pageMode;
            location.hash = msaxValues.msax_LocalPageMode.toString();
            if (pageMode != PageModeOptions.ThankYou && pageMode != PageModeOptions.ConfirmView)
                CartBase.CreateDDO("");
            $(document).trigger('PageModeSubscribers');
            $(document).trigger(pageModeValue);
        }

        public static OnClearPageMode(callerContext: any, handler: any) {
            $(document).on('ClearPageMode', $.proxy(handler, callerContext));
        }
        public static OnPageModeSubscribers(callerContext: any, handler: any) {
            $(document).on('PageModeSubscribers', $.proxy(handler, callerContext));
        }


        public static OnUpdateFocusPageMode(pageMode: PageModeOptions, callerContext: any, handler: any) {
            var pageModeValue = CartBase.FindPageMode(pageMode);
            //if (msaxValues.msax_PageMode == pageMode)
            $(document).on(pageModeValue, $.proxy(handler, callerContext));
        }

        public static showError(isError: boolean) {

            var errorPanel = CartBase._cartView.find(".msax-ErrorPanel");
            // Shows the error message on the error panel.
            if (isError) {

                errorPanel.empty();             
                errorPanel.append($('<span/>').text(errorMessage));
                errorPanel.removeClass("msax-Info");
                errorPanel.addClass("msax-Error");
                $(window).scrollTop(0);
                errorPanel.show();
            }
            else if (errorPanel.hasClass("msax-Error")) {
                errorPanel.removeClass("msax-Error");
                errorPanel.removeClass("msax-Info");
                errorPanel.hide();
            }

        }

        public static showInfo (isInfo: boolean) {

            var errorPanel = CartBase._cartView.find(".msax-ErrorPanel");
            // Shows the error message on the error panel.
            if (isInfo) {
                errorPanel.empty();
                errorPanel.append($('<span/>').text(errorMessage));
                errorPanel.removeClass("msax-Error");
                errorPanel.addClass("msax-Info");
                $(window).scrollTop(0);
                errorPanel.show();
            }
            else if (errorPanel.hasClass("msax-Info")) {
                errorPanel.removeClass("msax-Error");
                errorPanel.removeClass("msax-Info");
                errorPanel.hide();
            }

        }

        public static FindPageMode(pageMode) {
            var pageModeValue;
            switch (pageMode) {
                case PageModeOptions.CartView:
                    pageModeValue = "CartViewPageMode";
                    msaxValues.msax_LocalPageMode = "CartViewPageMode";
                    break;
                case PageModeOptions.ShippingView:
                    pageModeValue = "ShippingViewPageMode";
                    msaxValues.msax_LocalPageMode = "shipping";
                    break;
                case PageModeOptions.ShippingViewCompleted:
                    pageModeValue = "ShippingViewCompletedPageMode";
                    msaxValues.msax_LocalPageMode = "shipping";
                    break;
                case PageModeOptions.BillingView:
                    pageModeValue = "BillingViewPageMode";
                    msaxValues.msax_LocalPageMode = "billing";
                    break;
                case PageModeOptions.BillingViewcompleted:
                    pageModeValue = "BillingViewCompletedPageMode";
                    msaxValues.msax_LocalPageMode = "billing";
                    break;
                case PageModeOptions.PaymentView:
                    pageModeValue = "PaymentViewPageMode";
                    break;
                case PageModeOptions.PaymentViewCompleted:
                    pageModeValue = "PaymentViewCompletedPageMode";
                    break;
                case PageModeOptions.ConfirmView:
                    pageModeValue = "ConfirmViewPageMode";
                    msaxValues.msax_LocalPageMode = "confirm";
                    break;
                case PageModeOptions.ConfirmViewCompleted:
                    pageModeValue = "ConfirmViewCompletedPageMode";
                    msaxValues.msax_LocalPageMode = "confirm";
                    break;
                case PageModeOptions.ThankYou:
                    pageModeValue = "ThankYouPageMode";
                    msaxValues.msax_LocalPageMode = "thankyou";
                    break;
                default:
                    pageModeValue = "CartViewPageMode";
                    msaxValues.msax_LocalPageMode = "CartViewPageMode";
                    break;
            }
            return pageModeValue;
        }

        public static CreateDDO(orderNumber) {

            if (!Utils.isNullOrUndefined(CartData()) && !Utils.isNullOrUndefined(CartData().Items)) {
                var data = false;
                if (msaxValues.msax_PageMode == PageModeOptions.ThankYou && !Utils.isNullOrEmpty(orderNumber))
                    data = true
            var DDO = {};
                var DDO_checkoutInfo = [];
                var DDO_checkoutPages;
                var pageData = {
                    pageName: $(document).find("title").text(),
                    pageURL: $(location).attr('href'),
                    pageDepartment: CartBase.FindPageMode(msaxValues.msax_PageMode),
                    pageSiteSection: msaxValues.msax_LocalPageMode,
                    pageError: errorMessage
                };

                var siteData = {
                    sitePlatform: findBrowser(),
                    siteDomain: get_hostname($(location).attr('href'))
                };

                var userData = {
                    userStatus: msaxValues.msax_LoginMode == true ? "Signed In" : "Not Signed In"
                };


                var DDO_product_info = [];
                for (var itemVal in CartData().Items) {
                    var itemData = CartData().Items[itemVal];
                    DDO_checkoutInfo.push({
                        checkoutProductName: itemData["ProductDetailsExpanded"].Name,
                        checkoutProductPrice: itemData.NetAmountWithCurrency.replace(",", ""),
                        checkoutProductID: itemData.ItemId
                    });
                    if (data) {
                        DDO_product_info.push(
                            {
                                product_sku: itemData.ItemId,
                                product_name: itemData["ProductDetailsExpanded"].Name,
                                product_qty: itemData.Quantity,
                                product_price: itemData.NetAmountWithCurrency.replace("$", "")
                            });
                    }
                }

                DDO_checkoutPages = {
                    checkoutState: msaxValues.msax_LoginMode == true ? "SignedInCustomer" : "Guest",
                    checkoutInfo: DDO_checkoutInfo
                };
                if (data) {
                    var DDO_orderDetail = {
                        orderID: orderNumber,
                        tax: CartData().TaxAmountWithCurrency.replace("$", ""),
                        shipping: CartData().EstShipping.replace("$", ""),
                        discount: CartData().DiscountAmountWithCurrency.replace("$", ""),
                        promoCode: CartData().DiscountCodes,
                        totalRevenue: CartData().TotalAmountWithCurrency.replace("$", ""),
                        product_info: DDO_product_info,
                       //N - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
                        rsaId: CartData().RsaId
                    };
                }
                if (data) {
                    DDO = {
                        pageData: pageData,
                        siteData: siteData,
                        userData: userData,
                        checkoutPages: DDO_checkoutPages,
                        orderDetail: DDO_orderDetail
                    };
                    $(document).trigger('UpdateDDO_orderDetail', DDO);
                }
                else {
                    DDO = {
                        pageData: pageData,
                        siteData: siteData,
                        userData: userData,
                        checkoutPages: DDO_checkoutPages
                    };
                    $(document).trigger('UpdateDDO_checkoutPages', DDO);
                }
            }
        }
    }
} 