﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


/// <reference path="../Helpers/Core.ts" />
/// <reference path="../Helpers/AsyncResult.ts" />

"use strict";

// Web services proxy
// Encapsulates ajax calls to the SharePoint WCF services.
module AFM.Ecommerce.Controls {
    //NS by muthait
    export class AFMExtendedService {
        private static proxy;

        public static GetProxy() {
            this.proxy = new AjaxProxy(msaxValues.msax_AFMExtendedServiceUrl + '/');
        }

        public static GetZipCode(): AsyncResult<AFMZipCodeResponse> {
            var asyncResult = new AsyncResult<AFMZipCodeResponse>();

            var data = {
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetZipCode",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetCultureId(): AsyncResult<string> {
            var asyncResult = new AsyncResult<string>();

            var data = {
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetCultureId",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }


        public static SetShippingtoLineItems(shippingOptions: SelectedLineDeliveryOption, dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "shippingOptions": shippingOptions,
                "dataLevel": dataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "SetShippingtoLineItems",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, true);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static SetZipCode(zipCode: string, isCheckoutSession:boolean): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "zipCode": zipCode
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "SetZipCode",
                data,
                (data) => {
                    if (!Utils.isNullOrUndefined(data)) {
                        ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    }
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static SendOrderSummary(orderSummary: AFMOrderSummary): AsyncResult<string> {
            var asyncResult = new AsyncResult<string>();

            var data = {
                "orderSummary": orderSummary
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "SendOrderSummary",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetUserCartInfo(): AsyncResult<UserCartInfo> {
            var asyncResult = new AsyncResult<UserCartInfo>();;

            var data = {
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetUserCartInfo",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static SetUserCartInfo(userCartData: UserCartInfo): AsyncResult<string> {
            var asyncResult = new AsyncResult<string>();

            var data = {
                "userCartData": userCartData
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "SetUserCartInfo",
                data,
                (data) => {
                    asyncResult.resolve(data);
                    //location.reload();
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetUserPageMode(): AsyncResult<UserPageMode> {
            var asyncResult = new AsyncResult<UserPageMode>();;

            var data = {
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetUserPageMode",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static SetUserPageMode(userPageMode: UserPageMode): AsyncResult<string> {
            var asyncResult = new AsyncResult<string>();

            var data = {
                "userPageMode": userPageMode
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "SetUserPageMode",
                data,
                (data) => {
                    asyncResult.resolve(data);
                    //location.reload();
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static ClearAllCookies(): AsyncResult<string> {
            var asyncResult = new AsyncResult<string>();;

            var data = {
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "ClearAllCookies",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }
       
        public static SubmitOrder(tenderLineData: TenderDataLine[], headerValues: AFMSalesTransHeader,  email: string, cardToken: string): AsyncResult<CreateSalesOrderResponse> {
            var asyncResult = new AsyncResult<CreateSalesOrderResponse>();

            var data = {
                "tenderDataLine": tenderLineData,
                "headerValues": headerValues,
                "emailAddress": email,
                "cardToken" : cardToken
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "CreateOrder",
                data,
                (data) => {
                    //ShoppingCartService.UpdateShoppingCart(data, true);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Initiates the checkout workflow.
        public static CommenceCheckout(dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "dataLevel": dataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "CommenceCheckout",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, true);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetCardTokenData(tranid: string): AsyncResult<CardTokenData> {
            var asyncResult = new AsyncResult<CardTokenData>();

            var data = {
                "tranid": tranid
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetCardTokenData",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });
            return asyncResult;
        }

        public static ValidateAddress(address: Address): AsyncResult<AFMAddressData> {
            var asyncResult = new AsyncResult<AFMAddressData>();

            var data = {
                "address": address
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "ValidateAddress",
                data,
                (data) => {
                    asyncResult.resolve(data);
                    //location.reload();
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetAFMAddressState(countryRegionId: string): AsyncResult<ObservableArray<StateProvinceInfo>> {
            var asyncResult = new AsyncResult<ObservableArray<StateProvinceInfo>>();

            var data = {
                "countryRegionId": countryRegionId
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetAFMAddressState",
                data,
                (data) => {
                    asyncResult.resolve(data);                    
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static ValidateZipcode(countryRegionId: string, zipCode: string): AsyncResult<boolean> {
            var asyncResult = new AsyncResult<boolean>();

            var data = {
                "countryRegionId": countryRegionId,
                "zipCode": zipCode  
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "ValidateZipcode",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static ValidatePromocode(promoCode: string): AsyncResult<boolean> {
            var asyncResult = new AsyncResult<boolean>();

            var data = {
                "promoCode": promoCode
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "ValidatePromocode",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        //CS Developed by spriya Dated 12/12/2014 - Get signed in customer addresses based on IsShipping flag
        public static GetShippingAddresses(IsShipping : boolean): AsyncResult<AddressCollectionResponse> {
            var asyncResult = new AsyncResult<AddressCollectionResponse>();

            var data = {
                "IsShipping": IsShipping
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetShippingAddresses",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        //CE Devloped by spriya

        public static GetOrderConfirmationDetails(): AsyncResult<AFMOrderConfirmationResponse> {
            var asyncResult = new AsyncResult<AFMOrderConfirmationResponse>();

            var data = {                
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetOrderConfirmationDetails",
                data,
                (data) => {
                    asyncResult.resolve(data);
                    var shoppingCartResonse = new ShoppingCartResponseClass(data);
                    ShoppingCartService.UpdateShoppingCart(shoppingCartResonse, true);                                       
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static RedirectToPayment():AsyncResult<string> {
            var asyncResult = new AsyncResult<string>();
            var data = {
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "RedirectToPayment",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                   
                });
            return asyncResult;
        }

        //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
        public static ValidateRsaId(rsaId: string, isCheckoutSession: boolean): AsyncResult<boolean> {
            var asyncResult = new AsyncResult<boolean>();

            var data = {
                "rsaId": rsaId,
                "isCheckoutSession": isCheckoutSession
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "ValidateRsaId",
                data,
                (data) => {
                    asyncResult.resolve(data);                   
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static RemoveRsaIdFromCart(isCheckoutSession: boolean): AsyncResult<boolean> {
            var asyncResult = new AsyncResult<boolean>();

            var data = {
                "isCheckoutSession": isCheckoutSession
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "RemoveRsaIdFromCart",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }
        //NE - RxL
    }
}

