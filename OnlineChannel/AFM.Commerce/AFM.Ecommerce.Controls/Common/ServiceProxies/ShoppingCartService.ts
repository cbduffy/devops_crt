﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


/// <reference path="../Helpers/Core.ts" />
/// <reference path="../Helpers/EcommerceTypes.ts" />

"use strict";

// Web services proxy
// Encapsulates ajax calls to the SharePoint WCF services.
module AFM.Ecommerce.Controls {
    export class ShoppingCartService {
        private static proxy: AjaxProxy;

        public static GetProxy() {
            this.proxy = new AjaxProxy(msaxValues.msax_ShoppingCartServiceUrl + '/');
        }

        public static UpdateShoppingCart(shoppingCartResponse: ShoppingCartResponse, isCheckoutSession: boolean) {
            if (!Utils.isNullOrUndefined(shoppingCartResponse.ShoppingCart) && Utils.hasElements(shoppingCartResponse.ShoppingCart.Items)) {
                var KitSequenceNumbers = [];
                var KitSequenceSet = [];
                var ReassembledCartItems: TransactionItem[] = [];

                //NS by muthait dated 26 Sep 2014 - Kit assemble after explode
                for (var itemVal in shoppingCartResponse.ShoppingCart.Items) {
                    var itemData = shoppingCartResponse.ShoppingCart.Items[itemVal];
                    if (!Utils.isNullOrUndefined(itemData.AFMKitSequenceNumber) && itemData.AFMKitProductId > 0) {
                        if (Utils.isNullOrUndefined(KitSequenceSet[itemData.AFMKitSequenceNumber])) {
                            KitSequenceSet[itemData.AFMKitSequenceNumber] = [[], []];
                            KitSequenceNumbers.push(itemData.AFMKitSequenceNumber);
                            var data = new TransactionItemClass(null);
                            data.ProductDetails = itemData.AFMKitItemProductDetails;
                            data.AFMVendorName = itemData.AFMVendorName;
                            data.AFMShippingMode = itemData.AFMShippingMode;
                            data.AFMCartLineATP = itemData.AFMCartLineATP;
                            // CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box for kit
                            data.AFMMultipleQty = 0;
                            data.AFMLowestQty = 0;
                            data.AFMQtyPerBox = 0;
                            data.DeliveryModeId = itemData.DeliveryModeId;
                            data.Quantity = itemData.AFMKitItemQuantity;
                            data.ProductId = itemData.AFMKitProductId;
                            data.ItemId = itemData.AFMKitItemId;
                            data.LineId = itemData.AFMKitSequenceNumber.toString();
                            data.AFMKitSequenceNumber = itemData.AFMKitSequenceNumber;
                            var listingObject = this.CreateListingObject(itemData);
                            data.Listing = [];
                            data.Listing.push(listingObject);
                            KitSequenceSet[itemData.AFMKitSequenceNumber][0] = data;
                            KitSequenceSet[itemData.AFMKitSequenceNumber][1].push(itemData);
                        }
                        else {
                            var listingObject = this.CreateListingObject(itemData);
                            var currentKitObject: TransactionItem = KitSequenceSet[itemData.AFMKitSequenceNumber][0];
                            currentKitObject.Listing.push(listingObject);
                            KitSequenceSet[itemData.AFMKitSequenceNumber][0] = currentKitObject;
                            KitSequenceSet[itemData.AFMKitSequenceNumber][1].push(itemData);
                        }
                    } else {
                        if (itemData.AFMItemType != AFMItemType.DeliveryServiceItem) {
                            var listingObject = this.CreateListingObject(itemData);
                            itemData.Listing = [];
                            itemData.Listing.push(listingObject);
                            //Add for Zero price Item Validation
                            if (+itemData.PriceWithCurrency.split('$')[1].replace(",", "") == 0.0) {
                                //Add Condition Bug 98355:WSR - "Please remove item having zero price." message displays when user changes zip code during checkout on HD item
                                if (msaxValues.msax_LocalPageMode != PageMode.Billing) {
                                    var error = new ErrorClass(null);
                                    error.ErrorMessage = Resources.String_1145;// "Please Reomve Item Price with Zero value";
                                    shoppingCartResponse.Errors.push(error);
                                }
                            }
                            //Add for Zero price Item Validation
                            ReassembledCartItems.push(itemData);
                        }
                    }
                }
                for (var KitData in KitSequenceNumbers) {
                    var dt = KitSequenceNumbers[KitData];
                    var assembledKit = new TransactionItemClass(null);
                    assembledKit = KitSequenceSet[dt][0];
                    assembledKit.KitComponents = KitSequenceSet[dt][1];
                    var KitPriceValue = 0.0;
                    var KitPriceAfterDiscount = 0.0;
                    var OfferNames = "";
                    var KitDiscountValue = 0.0;
                    var KitTotalValue = 0.00;
                    var KitTaxValue = 0.0;
                    //NS develop by v-hapat CR159 on dated 10/10/2014
                    var IsKitCompPriceZero = false;
                    //NE develop by v-hapat CR159 on dated 10/10/2014
                    assembledKit.AFMCartLineATP = null;
                    for (var KtData in assembledKit.KitComponents) {
                        var kititemdata = assembledKit.KitComponents[KtData];

                        var componentQtyInKit = kititemdata.Quantity / kititemdata.AFMKitItemQuantity;
                        var PriceValue = +kititemdata.PriceWithCurrency.split('$')[1].replace(",", "");
                        KitPriceValue += PriceValue * componentQtyInKit;
                        var PriceAfterDiscount = +kititemdata.PriceAfterDiscount.split('$')[1].replace(",", "");
                        KitPriceAfterDiscount += PriceAfterDiscount * componentQtyInKit;
                        KitTaxValue += +kititemdata.TaxAmountWithCurrency.split('$')[1].replace(",", "");
                        KitTotalValue += +kititemdata.NetAmountWithCurrency.split('$')[1].replace(",", "");
                        //NS develop by v-hapat CR159 on dated 10/10/2014                        
                        if (+kititemdata.PriceWithCurrency.split('$')[1].replace(",", "") == 0.0
                            ) {
                            IsKitCompPriceZero = true;
                        }
                        //NE develop by v-hapat CR159 on dated 10/10/2014
                        //var offer = OfferNames.match(kititemdata.OfferNames);  
                        //if (Utils.isNullOrUndefined(offer)) {
                        //    OfferNames += kititemdata.OfferNames + " ";
                        //} else if (offer[0].length != kititemdata.OfferNames.length) {
                        //    OfferNames += kititemdata.OfferNames + " ";
                        //}
                        //NS developed by hardik for Bug # 83173 dated 04/24/2015
                        if (Utils.isNullOrUndefined(kititemdata.DeliveryModeId) && !Utils.isNullOrUndefined(assembledKit.DeliveryModeId))
                            assembledKit.DeliveryModeId = null;
                        else
                            assembledKit.DeliveryModeId = kititemdata.DeliveryModeId;
                        //NE developed by hardik for Bug # 83173 dated 04/24/2015
                        //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
                        if (Utils.isNullOrUndefined(assembledKit.AFMCartLineATP)) {
                            assembledKit.AFMCartLineATP = kititemdata.AFMCartLineATP;
                        }
                        else {
                            if (assembledKit.AFMCartLineATP.BestDate == kititemdata.AFMCartLineATP.BestDate && assembledKit.AFMCartLineATP.EndDateRange == kititemdata.AFMCartLineATP.EndDateRange)
                                assembledKit.DeliveryModeId = kititemdata.DeliveryModeId;
                        }
                        //NE by RxL
                    }
                    //NS 100465-ECS - Promo code description duplicates on the component header in the cart
                    var offersList = assembledKit.KitComponents.map(function (o) { if (!Utils.isNullOrEmpty(o.OfferNames)) return o.OfferNames; });
                    offersList = $.unique(offersList);
                    if (offersList.length > 0)
                        OfferNames = offersList.join(" ");
                    //NE 100465 - ECS - Promo code description duplicates on the component header in the cart
                    assembledKit.OfferNames = OfferNames.trim();
                    if (IsKitCompPriceZero) {
                        assembledKit.PriceWithCurrency = Utils.CurrencyString(0, true);
                        assembledKit.PriceAfterDiscount = Utils.CurrencyString(0, true);
                        assembledKit.PriceAfterDiscount = Utils.CurrencyString(0, true);
                        assembledKit.DiscountAmount = 0;
                        assembledKit.DiscountAmountWithCurrency = Utils.CurrencyString(0, true);
                        assembledKit.TaxAmountWithCurrency = Utils.CurrencyString(0, true);
                        assembledKit.NetAmountWithCurrency = Utils.CurrencyString(0, true);
                        //Add Condition Bug 98355:WSR - "Please remove item having zero price." message displays when user changes zip code during checkout on HD item
                        if (msaxValues.msax_LocalPageMode != PageMode.Billing) {
                            var error = new ErrorClass(null);
                            error.ErrorMessage = Resources.String_1145;// "Please Reomve Item Price with Zero value";
                            shoppingCartResponse.Errors.push(error);
                        }
                    }
                    else {
                        // HXM To fix 68342 ECS - Item prices shows 3 digits after the decimal
                        assembledKit.PriceWithCurrency = Utils.CurrencyString(Math.round(KitPriceValue * 100) / 100, true);
                        assembledKit.PriceAfterDiscount = Utils.CurrencyString(Math.round(KitPriceAfterDiscount * 100) / 100, true);
                        assembledKit.DiscountAmount = KitPriceValue - KitPriceAfterDiscount;
                        // HXM To fix 70131 ECS - You Save Amount is not correct on kits when more than 1 quantity is in the cart.
                        assembledKit.DiscountAmountWithCurrency = Utils.CurrencyString(Math.round((KitPriceValue - KitPriceAfterDiscount) * 100) / 100 * kititemdata.AFMKitItemQuantity, true);
                        assembledKit.TaxAmountWithCurrency = Utils.CurrencyString(KitTaxValue, true);
                        //HXM to potentially fix 70413 ECS -Dollar amount issue applying discounts
                        assembledKit.NetAmountWithCurrency = Utils.CurrencyString(Math.round(KitTotalValue * 100) / 100, true);
                    }

                    assembledKit.ItemType = TransactionItemType.Kit;
                    ReassembledCartItems.push(assembledKit);
                }

                var ShoppingCartResponseExtended = shoppingCartResponse;

                if (ReassembledCartItems.length < shoppingCartResponse.ShoppingCart.Items.length && ReassembledCartItems.length > 0) {

                    ShoppingCartResponseExtended.ShoppingCart.Items = ReassembledCartItems;
                }

                for (var index in ShoppingCartResponseExtended.ShoppingCart.Items) {

                    var item = ShoppingCartResponseExtended.ShoppingCart.Items[index];
                    if (!Utils.isNullOrWhiteSpace(item.ProductDetails)) {
                        item["ProductDetailsExpanded"] = JSON.parse(item.ProductDetails);
                    } else {
                        item["ProductDetailsExpanded"] = { Name: "", ProductUrl: "", DimensionValues: "", ProductNumber: "", ImageUrl: "", ImageAlt: "" };
                    }
                    var convertedDimensionsValues = [];

                    for (var pde in item["ProductDetailsExpanded"].DimensionValues) if (item["ProductDetailsExpanded"].DimensionValues.hasOwnProperty(pde)) {
                        convertedDimensionsValues.push({
                            "Key": pde,
                            "Value": item["ProductDetailsExpanded"].DimensionValues[pde]
                        });
                    }
                    item["ProductDetailsExpanded"].DimensionValues = convertedDimensionsValues;


                    //item.ImageMarkup = ShoppingCartService.BuildImageMarkup50x50(item.Image);
                    var imageData: ImageInfo = new ImageInfoClass(null);
                    imageData.Url = item["ProductDetailsExpanded"].ImageUrl;
                    imageData.AltText = item["ProductDetailsExpanded"].ImageAlt;

                    item.ImageMarkup = ShoppingCartService.BuildImageMarkup50x50(imageData);

                    if (!Utils.isNullOrUndefined(item.OfferNames)) {
                        item.OfferNames = item.OfferNames.replace(",", "\n");
                    }

                    // Adding kit component details to the item if the item is a kit.
                    if (item.ItemType == TransactionItemType.Kit) {
                        item.NoOfComponents = Utils.format(Resources.String_88 /* {0} PRODUCT(S) */, item.KitComponents.length);

                        for (var j = 0; j < item.KitComponents.length; j++) {
                            if (!Utils.isNullOrWhiteSpace(item.KitComponents[j].ProductDetails)) {
                                item.KitComponents[j].ProductDetailsExpanded = JSON.parse(item.KitComponents[j].ProductDetails);
                            } else {
                                item.KitComponents[j].ProductDetailsExpanded = { Name: "", ProductUrl: "", DimensionValues: "", ProductNumber: "", ImageUrl: "" };
                            }

                            var convertedKitComponentDimensionsValues = [];

                            for (var pkde in item.KitComponents[j].ProductDetailsExpanded.DimensionValues) if (item.KitComponents[j].ProductDetailsExpanded.DimensionValues.hasOwnProperty(pkde)) {
                                convertedKitComponentDimensionsValues.push({
                                    "Key": pkde,
                                    "Value": item.KitComponents[j].ProductDetailsExpanded.DimensionValues[pkde]
                                });
                            }
                            item.KitComponents[j].ProductDetailsExpanded.DimensionValues = convertedKitComponentDimensionsValues;


                            var imageDataforKit: ImageInfo = new ImageInfoClass(null);
                            imageDataforKit.Url = item.KitComponents[j].ProductDetailsExpanded.ImageUrl;
                            imageDataforKit.AltText = item.KitComponents[j].ProductDetailsExpanded.ImageAlt;

                            //item.KitComponents[j].ImageMarkup = ShoppingCartService.BuildImageMarkup50x50(item.KitComponents[j].Image);
                            item.KitComponents[j].ImageMarkup = ShoppingCartService.BuildImageMarkup50x50(imageDataforKit);
                        }
                    }
                }
            }
            CartData(shoppingCartResponse.ShoppingCart);
            if (!Utils.isNullOrUndefined(shoppingCartResponse.Errors) && shoppingCartResponse.Errors.length > 0)
                ErrorCount = shoppingCartResponse.Errors.length;
            if (!Utils.isNullOrUndefined(shoppingCartResponse.ShoppingCart))
                $(document).trigger('UpdateCartCount', shoppingCartResponse.ShoppingCart.CartCount);
            else
                $(document).trigger('UpdateCartCount', 0);

            // Trigger the update checkout/shopping cart event every time a service updates the cart 
            // so that all subscribers can get the latest cart and handle the event accordingly.          
            if (isCheckoutSession) {
                $(document).trigger('UpdateCheckoutCart', [shoppingCartResponse]);
            }
            else {
                $(document).trigger('UpdateShoppingCart', [shoppingCartResponse]);
            }
        }

        public static CreateListingObject(transactionItem: TransactionItem) {
            var listingObject = new ListingClass(null);
            listingObject.AFMKitItemId = Utils.isNullOrEmpty(transactionItem.AFMKitItemId) ? "" : transactionItem.AFMKitItemId;
            var kitComponentInfo = new KitComponentInfoClass(null);
            kitComponentInfo.KitLineIdentifier = Utils.isNullOrUndefined(transactionItem.LineId) ? "" : transactionItem.LineId;
            kitComponentInfo.KitLineProductId = Utils.isNullOrUndefined(transactionItem.ProductId) ? 0 : transactionItem.ProductId;
            kitComponentInfo.KitProductMasterId = Utils.isNullOrUndefined(transactionItem.AFMKitProductId) ? 0 : transactionItem.AFMKitProductId;
            kitComponentInfo.Title = Utils.isNullOrEmpty(transactionItem.Name) ? "" : transactionItem.Name;
            kitComponentInfo.Unit = Utils.isNullOrUndefined(transactionItem.AFMUnit) ? "" : transactionItem.AFMUnit;
            listingObject.KitComponentDetails = kitComponentInfo;

            listingObject.AFMKitItemProductDetails = Utils.isNullOrEmpty(transactionItem.AFMKitItemProductDetails) ? "" : transactionItem.AFMKitItemProductDetails;
            listingObject.AFMKitProductId = Utils.isNullOrEmpty(transactionItem.AFMKitProductId) ? 0 : transactionItem.AFMKitProductId;
            listingObject.AFMKitSequenceNumber = Utils.isNullOrUndefined(transactionItem.AFMKitSequenceNumber) ? 0 : transactionItem.AFMKitSequenceNumber;
            listingObject.ItemId = Utils.isNullOrUndefined(transactionItem.ItemId) ? "" : transactionItem.ItemId;
            listingObject.ListingId = Utils.isNullOrUndefined(transactionItem.ProductId) ? 0 : transactionItem.ProductId;
            listingObject.ProductDetails = Utils.isNullOrEmpty(transactionItem.ProductDetails) ? "" : transactionItem.ProductDetails;
            listingObject.LineId = AFM.Utils.isNullOrEmpty(transactionItem.LineId) ? "" : transactionItem.LineId;
            //  NS Developed by ashish for Listing Object to have Quantity for item moved in the wishlist 20June2015
            listingObject.Quantity = transactionItem.Quantity;
            // NE Developed by ashish for Listing Object to have Quantity for item moved in the wishlist 20June2015
            return listingObject;
        }

        public static GetDimensionValues(color: string, size: string, style: string) {
            var hasColor = !Utils.isNullOrWhiteSpace(color);
            var hasSize = !Utils.isNullOrWhiteSpace(size);
            var hasStyle = !Utils.isNullOrWhiteSpace(style);

            var dimensionValues = null;
            if (hasColor || hasSize || hasStyle) {
                dimensionValues = ''
                + (!hasColor ? '' : color)
                + (hasColor && (hasSize || hasStyle) ? ', ' : '')
                + (!hasSize ? '' : size)
                + (hasStyle && (hasColor || hasSize) ? ', ' : '')
                + (!hasStyle ? '' : style)
                + '';
            }

            return dimensionValues;
        }

        // Function that exposes the UpdateShoppingCart event. 
        // The caller can send the context in which the event should be triggered and irrespective of the triggered context the context given here is available in the event handler.
        public static OnUpdateShoppingCart(callerContext: any, handler: any) {
            $(document).on('UpdateShoppingCart', $.proxy(handler, callerContext));
        }

        // Function that exposes the UpdateCheckoutCart event. 
        // The caller can send the context in which the event should be triggered and irrespective of the triggered context the context given here is available in the event handler.
        public static OnUpdateCheckoutCart(callerContext: any, handler: any) {
            $(document).on('UpdateCheckoutCart', $.proxy(handler, callerContext));
        }

        // Calls the GetCart method.
        public static GetShoppingCart(shoppingCartDataLevel: ShoppingCartDataLevel, isCheckoutSession: boolean): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetShoppingCart",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Calls the RemoveFromCart method.
        public static RemoveFromCart(isCheckoutSession: boolean, lineId: string, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "lineIds": [lineId],
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "RemoveItems",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
        // Calls the RemoveFromCart method.
        public static RemoveKitFromCart(isCheckoutSession: boolean, lineIds: string[], shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "lineIds": lineIds,
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "RemoveItems",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }
        //NE - RxL

        // Calls the UpdateQuantity method.
        public static UpdateQuantity(isCheckoutSession: boolean, items: TransactionItem[], shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "items": items, // Items is in JSON format= [{ "LineId": lineId, "Quantity": quantity }]
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "UpdateItems",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Gets the promotion banners for the cart.
        public static GetPromotions(isCheckoutSession: boolean, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetPromotions",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Adds or removes discount code from shopping cart.
        public static AddOrRemovePromotion(isCheckoutSession: boolean, promoCode: string, isAdd: boolean, shoppingCartDataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "isCheckoutSession": isCheckoutSession,
                "promotionCode": promoCode,
                "isAdd": isAdd,
                "dataLevel": shoppingCartDataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "AddOrRemovePromotionCode",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, isCheckoutSession);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Initiates the checkout workflow.
        public static CommenceCheckout(dataLevel: ShoppingCartDataLevel): AsyncResult<ShoppingCartResponse> {
            var asyncResult = new AsyncResult<ShoppingCartResponse>();

            var data = {
                "dataLevel": dataLevel
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "CommenceCheckout",
                data,
                (data) => {
                    ShoppingCartService.UpdateShoppingCart(data, true);
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        // Replace or add given key value pair as query string.
        private static ReplaceOrAddQueryString(url: string, key: string, value: string): string {
            var re = new RegExp("([?|&])" + key + "=.*?(&|#|$)", "i"); // Looking for case insensitive match to ?key=value(&/#/$) or &key=value&(&/#/$)
            if (url.match(re)) {
                return url.replace(re, '$1' + key + "=" + value + '$2'); // If the regex is found then replace the query string while retaining capture group 1 and 2.
            } else {
                var hash = '';
                var separator = url.indexOf('?') !== -1 ? "&" : "?"; // Check if any query string exists if it does then '&' is separator for the new one else it is '?'.
                if (url.indexOf('#') !== -1) {
                    hash = url.replace(/.*#/, '#'); // Extracting fragment out of url if any.
                    url = url.replace(/#.*/, ''); // Removing fragment out of url.
                }

                return url + separator + key + "=" + value + hash;
            }
        }

        // Returns markup to be displayed when image url is null or when image url does not exist.
        private static GetNoImageMarkup(): string {
            return Utils.format('<span class=\"msax-NoImageContainer\"></span>');
        }

        // Build image markup with given width and height.
        private static BuildImageMarkup(imageData: ImageInfo, width: number, height: number): string {
            if (!Utils.isNullOrUndefined(imageData)) {
                var imageUrl = imageData.Url;
                var altText = imageData.AltText;
                var imageClassName = "msax-Image";

                if (!Utils.isNullOrWhiteSpace(imageUrl)) {
                    var errorScript = Utils.format('onerror=\"this.parentNode.innerHTML=AFM.Ecommerce.Controls.ShoppingCartService.GetNoImageMarkup();\"');
                    return Utils.format('<img src=\"{0}\" class=\"{1}\" alt=\"{2}\" {3} />', imageUrl, imageClassName, altText, errorScript);
                }
                else {
                    return ShoppingCartService.GetNoImageMarkup();
                }
            }
            else {
                return ShoppingCartService.GetNoImageMarkup();
            }
        }

        // Builds 50 * 50 image markup.
        private static BuildImageMarkup50x50(imageData: ImageInfo): string {
            if (Utils.isNullOrEmpty(imageData.Url)) {
                imageData.Url = "";
                imageData.AltText = "";
            }
            else if (Utils.isNullOrUndefined(imageData.AltText)) {
                imageData.AltText = "";
            }

            return ShoppingCartService.BuildImageMarkup(imageData, 50, 50);
        }
    }
}
