﻿/*
CS by muthait dated 12 Sep 2014
*/
/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


/// <reference path="../Helpers/Core.ts" />
/// <reference path="../Helpers/AsyncResult.ts" />

"use strict";

// Web services proxy
// Encapsulates ajax calls to the WCF services.
module AFM.Ecommerce.Controls {
    export class ChannelService {
        private static proxy;

        public static GetProxy() {
            this.proxy = new AjaxProxy(msaxValues.msax_ChannelServiceUrl + '/');
        }

        public static GetChannelConfiguration(): AsyncResult<ChannelInfoResponse> {
            var asyncResult = new AsyncResult<ChannelInfoResponse>();

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetChannelConfiguration",
                null,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetTenderTypes(): AsyncResult<TenderTypesResponse> {
            var asyncResult = new AsyncResult<TenderTypesResponse>();

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetChannelTenderTypes",
                null,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetCountryRegionInfo(): AsyncResult<CountryInfoResponse> {
            var asyncResult = new AsyncResult<CountryInfoResponse>();

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetChannelCountryRegionInfo",
                null,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }

        public static GetStateProvinceInfo(countryCode: string): AsyncResult<StateProvinceInfoResponse> {
            var asyncResult = new AsyncResult<StateProvinceInfoResponse>();

            var data = {
                'countryCode': countryCode
            };

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetStateProvinceInfo",
                data,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }
    }
}
 