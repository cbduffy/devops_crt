﻿/*
CS by muthait dated 12 Sep 2014
*/
/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


/// <reference path="../Helpers/Core.ts" />
/// <reference path="../Helpers/AsyncResult.ts" />

"use strict";

// Web services proxy
// Encapsulates ajax calls to the WCF services.
module AFM.Ecommerce.Controls {
    export class CustomerService {
        private static proxy;

        public static GetProxy() {
            this.proxy = new AjaxProxy(msaxValues.msax_CustomerServiceUrl + '/');
        }

        public static GetAddresses(): AsyncResult<AddressCollectionResponse> {
            var asyncResult = new AsyncResult<AddressCollectionResponse>();

            if (Utils.isNullOrUndefined(this.proxy)) {
                this.GetProxy();
            }

            this.proxy.SubmitRequest(
                "GetAddresses",
                null,
                (data) => {
                    asyncResult.resolve(data);
                },
                (errors) => {
                    asyncResult.reject(errors);
                });

            return asyncResult;
        }
    }
}
 