﻿/*
NS by muthait dated 12 Sep 2014
*/
/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../Common/Helpers/Core.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../../Resources/Resources.ts" />


module AFM.Ecommerce.Controls {
    "use strict";
    export class CartMode {
        private ShippingBtn;
        private BillingBtn;
        private PaymentBtn;
        private ConfirmBtn;
        private pageHeader;
        //NS Developed By v-hapat for Bug:79338
        private isShippingModePaased;
        private isBillinggModePaased;
        private isPaymentModePaased;
        private isConfirmModePaased;
        //NE Developed By v-hapat for Bug:79338
        private IsPageModeSubscribersCalled;
        constructor(element) {
            this.ShippingBtn = $(element).find(".msax-ShippingBtn");
            this.BillingBtn = $(element).find(".msax-BillingBtn");
            this.PaymentBtn = $(element).find(".msax-PaymentBtn");
            this.ConfirmBtn = $(element).find(".msax-ConfirmBtn");

            this.pageHeader = ko.observable<string>("");

            //NS Developed By v-hapat for Bug:79338
            this.isShippingModePaased = false;
            this.isBillinggModePaased = false;
            this.isPaymentModePaased = false;
            this.isConfirmModePaased = false;
            $(element).find(".msax_BackButton").css({ cursor: 'pointer' });
            //NE Developed By v-hapat for Bug:79338

            //subscribe calls
            ShoppingCartService.OnUpdateCheckoutCart(this, this.updateShoppingCart);
            CartBase.OnPageModeSubscribers(this, this.PageModeSubscribers);
            this.IsPageModeSubscribersCalled = false;
        }

        private PageModeSubscribers(event, data) {
            if (!this.IsPageModeSubscribersCalled) {
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ShippingView, this, this.ShippingViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ShippingViewCompleted, this, this.ShippingViewCompletedPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.BillingView, this, this.BillingViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.BillingViewcompleted, this, this.BillingViewCompletedPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.PaymentView, this, this.PaymentViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.PaymentViewCompleted, this, this.PaymentViewCompletedPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmView, this, this.ConfirmViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmViewCompleted, this, this.ConfirmViewCompletedPageMode);
                this.IsPageModeSubscribersCalled = true;
            }
        }


        private CallGoBackButton() {
            if (msaxValues.msax_LocalPageMode == PageMode.Shipping) {
                if (!Utils.isNullOrEmpty(msaxValues.msax_GoBackUrl))
                    window.location.href = msaxValues.msax_GoBackUrl;
            }
            else if (msaxValues.msax_LocalPageMode == PageMode.Billing)
                CartBase.UpdateFocusPageModeValue(PageModeOptions.ShippingViewCompleted);
            else if (msaxValues.msax_LocalPageMode == PageMode.Payment)
                CartBase.UpdateFocusPageModeValue(PageModeOptions.BillingViewcompleted);
            else if (msaxValues.msax_LocalPageMode == PageMode.Confirm) {
                // CartBase.UpdateFocusPageModeValue(PageModeOptions.BillingViewcompleted);
                // CS by sudoddik for bug: 76835
                //CartBase.UpdateFocusPageModeValue(PageModeOptions.PaymentViewCompleted);
                //CE
                IsConfirmedit = true;
                $(document).trigger('ShowPasswordAndTACInputs', "");
                $(document).trigger('ShowNavigatingAwayPopUp', null);
                if (navigateAway) {
                    msaxValues.msax_LocalPageMode = PageMode.Billing;
                    var userpagemode = new UserPageMode(null);
                    userpagemode.PageMode = PageModeOptions.BillingView;
                    AFMExtendedService.SetUserPageMode(userpagemode).done((data) => {
                        CartBase.UpdateFocusPageModeValue(PageModeOptions.PaymentView)
                });
                }
            }      
        }

        private updateShoppingCart(event, data) {
        }

        private ShippingViewPageMode(event, data) {
            this.pageHeader(Resources.String_1098);
            this.BillingBtn.removeClass("active");//Update by  V-hapat for CartMode Navigation
            this.PaymentBtn.removeClass("active");//Update by  V-hapat for CartMode Navigation
            this.ConfirmBtn.removeClass("active");//Update by  V-hapat for CartMode Navigation
            this.ShippingBtn.addClass("active");
            //NS Developed By v-hapat for Bug:79338
            var userpagemode = new UserPageMode(null);
            userpagemode.PageMode = PageModeOptions.ShippingView;
            AFMExtendedService.SetUserPageMode(userpagemode);
            msaxValues.msax_LocalPageMode = PageMode.Shipping;
            //NE Developed By v-hapat for Bug:79338
            $(document).find('#TermsAncConditionBlock').hide();
            this.ShippingBtnActive();
        }

        private ShippingViewCompletedPageMode(event, data) {
            this.pageHeader(Resources.String_1098);
            this.BillingBtn.removeClass("active");
            this.PaymentBtn.removeClass("active");
            this.ConfirmBtn.removeClass("active");
            this.ShippingBtn.addClass("active");
            //NS Developed By v-hapat for Bug:79338
            this.isShippingModePaased = true;
            var userpagemode = new UserPageMode(null);
            userpagemode.PageMode = PageModeOptions.ShippingViewCompleted;
            AFMExtendedService.SetUserPageMode(userpagemode);
            msaxValues.msax_LocalPageMode = PageMode.Shipping;
            //NE Developed By v-hapat for Bug:79338
            $(document).find('#TermsAncConditionBlock').hide();
            this.ShippingBtnActive();
        }

        private BillingViewPageMode(event, data) {
            this.pageHeader(Resources.String_1139);
            this.ShippingBtn.removeClass("active");
            this.PaymentBtn.removeClass("active");//Update by  V-hapat for CartMode Navigation
            this.ConfirmBtn.removeClass("active");//Update by  V-hapat for CartMode Navigation
            this.BillingBtn.addClass("active");
            //NS Developed By v-hapat for Bug:79338
            msaxValues.msax_LocalPageMode = PageMode.Billing;
            var userpagemode = new UserPageMode(null);
            userpagemode.PageMode = PageModeOptions.BillingView;
            AFMExtendedService.SetUserPageMode(userpagemode);
            //NE Developed By v-hapat for Bug:79338
            $(document).find('#TermsAncConditionBlock').hide();
            this.BillingBtnActive();
        }

        private BillingViewCompletedPageMode(event, data) {
            this.pageHeader(Resources.String_1139);
            this.ShippingBtn.removeClass("active");
            this.PaymentBtn.removeClass("active");//Update by  V-hapat for CartMode Navigation
            this.ConfirmBtn.removeClass("active");//Update by  V-hapat for CartMode Navigation
            this.BillingBtn.addClass("active");//Update by  V-hapat for CartMode Navigation
            //NS Developed By v-hapat for Bug:79338
            this.isBillinggModePaased = true;
            msaxValues.msax_LocalPageMode = PageMode.Billing;
            var userpagemode = new UserPageMode(null);
            userpagemode.PageMode = PageModeOptions.BillingViewcompleted;
            AFMExtendedService.SetUserPageMode(userpagemode);
            //NE Developed By v-hapat for Bug:79338
            $(document).find('#TermsAncConditionBlock').hide();
            this.BillingBtnActive();
        }

        private PaymentViewPageMode(event, data) {
            //NS Developed By v-hapat for Bug:79338
            msaxValues.msax_LocalPageMode = PageMode.Payment;
            var userpagemode = new UserPageMode(null);
            userpagemode.PageMode = PageModeOptions.PaymentView;
            //AFMExtendedService.SetUserPageMode(userpagemode);
            //NE Developed By v-hapat for Bug:79338
            //window.location.href = msaxValues.msax_PaymentGatewayUrl + "?tranid=" + encodeURIComponent(CartData().CartId);
            AFMExtendedService.RedirectToPayment().done((data) => {
                window.location.href = data;
            });;
        }

        private PaymentViewCompletedPageMode(event, data) {
            //NS Developed By v-hapat for Bug:79338
            this.isBillinggModePaased = true;
            msaxValues.msax_LocalPageMode = PageMode.Payment;
            var userpagemode = new UserPageMode(null);
            userpagemode.PageMode = PageModeOptions.PaymentViewCompleted;
            //AFMExtendedService.SetUserPageMode(userpagemode);
            //NE Developed By v-hapat for Bug:79338
            //window.location.href = msaxValues.msax_PaymentGatewayUrl + "?tranid=" + encodeURIComponent(CartData().CartId);
            AFMExtendedService.RedirectToPayment().done((data) => {
                window.location.href = data;
            });;
        }

        private ConfirmViewPageMode(event, data) {
            this.pageHeader(Resources.String_1140);
            this.ShippingBtn.removeClass("active");
            this.ConfirmBtn.removeClass("active");
            this.BillingBtn.removeClass("active");
            this.ShippingBtn.addClass("completed");
            this.BillingBtn.addClass("completed");
            this.PaymentBtn.addClass("completed");
            this.ConfirmBtn.addClass("active");
            //NS Developed By v-hapat for Bug:79338
            this.isConfirmModePaased = true;
            msaxValues.msax_LocalPageMode = PageMode.Confirm;
            var userpagemode = new UserPageMode(null);
            userpagemode.PageMode = PageModeOptions.ConfirmView;
            AFMExtendedService.SetUserPageMode(userpagemode);
            //NE Developed By v-hapat for Bug:79338
            $(document).find('#TermsAncConditionBlock').show();
            this.ConfirmBtnActive();
        }

        private ConfirmViewCompletedPageMode(event, data) {
            this.pageHeader(Resources.String_1140);
            this.ShippingBtn.removeClass("active");
            this.ConfirmBtn.removeClass("active");
            this.BillingBtn.removeClass("active");
            this.ShippingBtn.addClass("completed");
            this.BillingBtn.addClass("completed");
            this.PaymentBtn.addClass("completed");
            this.ConfirmBtn.addClass("active");
            //NS Developed By v-hapat for Bug:79338            
            msaxValues.msax_LocalPageMode = PageMode.Confirm;
            var userpagemode = new UserPageMode(null);
            userpagemode.PageMode = PageModeOptions.ConfirmViewCompleted;
            AFMExtendedService.SetUserPageMode(userpagemode);
            //NE Developed By v-hapat for Bug:79338
            $(document).find('#TermsAncConditionBlock').show();
            this.ConfirmBtnActive();
        }

        private ShippingBtnActive() {
            this.ShippingBtn.removeAttr('disabled');//Update by  V-hapat for CartMode Navigation
            this.BillingBtn.attr('disabled', 'disabled');//Update by  V-hapat for CartMode Navigation
            this.PaymentBtn.attr('disabled', 'disabled');//Update by  V-hapat for CartMode Navigation
            this.ConfirmBtn.attr('disabled', 'disabled');//Update by  V-hapat for CartMode Navigation
            //NS Developed By v-hapat for Bug:79338            
            if (IsConfirmedit) {
                this.ShippingBtn.css('cursor', 'pointer');
                this.BillingBtn.css('cursor', 'pointer');
                this.PaymentBtn.css('cursor', 'pointer');
                this.ConfirmBtn.css('cursor', 'pointer');
            }
            else {
                this.ShippingBtn.css('cursor', 'default');
                this.BillingBtn.css('cursor', 'default');
                this.PaymentBtn.css('cursor', 'default');
            }
            //NE Developed By v-hapat for Bug:79338                
        }

        private BillingBtnActive() {
            this.BillingBtn.removeAttr('disabled');//Update by  V-hapat for CartMode Navigation
            this.PaymentBtn.attr('disabled', 'disabled');//Update by  V-hapat for CartMode Navigation
            this.ConfirmBtn.attr('disabled', 'disabled');//Update by  V-hapat for CartMode Navigation
            this.ShippingBtn.addClass("completed");//Update by  V-hapat for CartMode Navigation
            //NS Developed By v-hapat for Bug:79338            
            if (IsConfirmedit) {
                this.ShippingBtn.css('cursor', 'pointer');
                this.BillingBtn.css('cursor', 'pointer');
                this.PaymentBtn.css('cursor', 'pointer');
                this.ConfirmBtn.css('cursor', 'pointer');
            }
            else {
                this.ShippingBtn.css('cursor', 'default');
                this.BillingBtn.css('cursor', 'default');
                this.PaymentBtn.css('cursor', 'default');
            }
            //NE Developed By v-hapat for Bug:79338            
        }

        private ConfirmBtnActive() {
            this.ShippingBtn.css('cursor', 'pointer');//Update by  V-hapat for CartMode Navigation
            this.BillingBtn.css('cursor', 'pointer');//Update by  V-hapat for CartMode Navigation
            this.PaymentBtn.css('cursor', 'pointer');//Update by  V-hapat for CartMode Navigation
            this.ConfirmBtn.css({ cursor: 'default' });//Update by  V-hapat for CartMode Navigation
            //NS Developed By v-hapat for Bug:79338            
            this.ShippingBtn.removeAttr('disabled');
            this.BillingBtn.removeAttr('disabled');
            this.ConfirmBtn.removeAttr('disabled');
            //NE Developed By v-hapat for Bug:79338            
        }

        private ShippingButton() {
            //NS Developed By v-hapat for Bug:79338            
            if (this.isConfirmModePaased) {
                IsConfirmedit = true;
                CartBase.UpdateFocusPageModeValue(PageModeOptions.ShippingViewCompleted);
            }
            //NE Developed By v-hapat for Bug:79338            
        }
        private BillingButton() {
            //NS Developed By v-hapat for Bug:79338            
            if (this.isConfirmModePaased) {
                IsConfirmedit = true;
                CartBase.UpdateFocusPageModeValue(PageModeOptions.BillingViewcompleted);
            }
            //NE Developed By v-hapat for Bug:79338            
        }
        private PaymentButton() {
            //NS Developed By v-hapat for Bug:79338            
            if (this.isConfirmModePaased)
                //window.location.href = msaxValues.msax_PaymentGatewayUrl + "?tranid=" + encodeURIComponent(CartData().CartId);
                AFMExtendedService.RedirectToPayment().done((data) => {
                    window.location.href = data;
                });;
            //NE Developed By v-hapat for Bug:79338            
        }
        private ConfirmButton() {
            //NS Developed By v-hapat for Bug:79338            
            if (this.isConfirmModePaased && IsConfirmedit)
                CartBase.UpdateFocusPageModeValue(PageModeOptions.ConfirmViewCompleted);
            //NE Developed By v-hapat for Bug:79338            
        }

    }
}