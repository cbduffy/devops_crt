﻿/// <reference path="../../resources/resources.ts" />
/*
NS by muthait dated 16 Sep 2014
*/
/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../Common/Helpers/Core.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../../Resources/Resources.ts" />

module AFM.Ecommerce.Controls {
    "use strict";
    export class CreateAddress {
        private isShoppingCartEnabled;
        public tempFirstName;
        public tempLastName;
        public tempStreet;
        public tempStreet2;
        public tempCity;
        public tempState;
        public tempCountry
        public tempZipCode;
        public tempEmail;
        public tempEmail2;
        public tempPhone;
        public tempPhone2;
        public tempPhone3;
        //CS Developed by spriya
        public tempAddressType;
        public tempRecordId;
        public suggestedAddressText;
        public billingAddressFill;
        public tempBillingRecordId;
        public tempBillingAddressType;
        //CE Developed by spriya
        public paymentCard;
        public availableDeliveryMethods;
        public orderShippingOptions;
        public isBillingAddressSameAsShippingAddress;
        public isOptinDeals;
        public tempBillingFirstName;
        public tempBillingLastName;
        public tempBillingStreet;
        public tempBillingStreet2;
        public tempBillingCity;
        public tempBillingState;
        public tempBillingCountry;
        public tempBillingZipcode;
        public tempBillingEmail;
        public tempBillingPhone;
        // HXM C - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey 
        public isOktoText;
        public ShippingconfirmEmailValue;
        public BillingconfirmEmailValue;
        public shippingAddressCollectionView;
        public shippingAddressView;
        public paymentAddressView;
        public AFMZipCode;
        public userCartInfo;
        public ShippingAddress;
        public PaymentAddress;
        public AvailableAddressCollection;
        public selectedAddress;
        //NS Developed By v-hapat for Bug:79338
        public CartMode;
        public _CountryInfoClass;
        public _CountryInfoClassList;
        public IsEditAllowed;
        public IsCartInfoLoaded;

        //NE Developed By v-hapat for Bug:79338

        public isOptinDealsVisiable; //N Hardik: Bug 94142:WSR - Remove "Opt-in" Check Box(Billing Page) - CR 320 (INC0132820)

        private addressConfirmDialog;
        private isShippingFromDialog;
        private avaSuggestedAddress;        

        public _StateInfoClassList;
        public IsPageModeSubscribersCalled;

    constructor(element) {
        LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText);

        this.shippingAddressCollectionView = $(document).find('.msax-ShippingAddressCollection');
        this.shippingAddressView = $(document).find('.msax-ShippingAddress');
        this.paymentAddressView = $(document).find('.msax-BillingAddress');
        this.CartMode = $(document).find("msax-CartMode");//N Developed By v-hapat for Bug:79338
        this.AFMZipCode = ko.observable<string>('');
        if (!Utils.isNullOrEmpty(AFMZipCodeData()))
            this.AFMZipCode(AFMZipCodeData());

        this.selectedAddress = ko.observable<string>(null);
        this.isShoppingCartEnabled = ko.computed(() => {
            return !Utils.isNullOrUndefined(CartData()) && Utils.hasElements(CartData().Items);
        });

        this.ShippingconfirmEmailValue = ko.observable<string>('');
        this.BillingconfirmEmailValue = ko.observable<string>('');
        this.availableDeliveryMethods = ko.observableArray<DeliveryOption>(null);

        this.orderShippingOptions = ko.observable<SelectedDeliveryOption>(new SelectedDeliveryOptionClass(null));
        this.isBillingAddressSameAsShippingAddress = ko.observable<boolean>(false);
        //this.isOptinDeals = ko.observable<boolean>(false);

        //NS IsOptinDeals is enabled by default - Sakalija for bug:72453
        this.isOptinDeals = ko.observable<Boolean>(true);
        //NE

        this.isOptinDealsVisiable = ko.observable<Boolean>(false);

        //this.paymentCard = ko.observable<Payment>(null);
        this.IsEditAllowed = true;
        this.userCartInfo = new UserCartData(null);
        this.ShippingAddress = new AddressClass(null);
        this.PaymentAddress = new AddressClass(null);

        this.AvailableAddressCollection = ko.observable<AddressCollectionResponse>(new AddressCollectionResponseClass(null));
        this.shippingAddressCollectionView.show();
        //NS Developed By v-hapat for Bug:79338
        this._CountryInfoClassList = ko.observableArray<CountryInfoClass>([{ CountryCode: 'USA', CountryName: Resources.String_21 }]);
        //NE Developed By v-hapat for Bug:79338

        this._StateInfoClassList = ko.observableArray<StateProvinceInfoClass>(null);
        this.IsCartInfoLoaded = false;
        // HXM C - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey 
        this.isOktoText = ko.observable<boolean>(false);
        this.InitateTempAddressDetails();
        this.LoadAddressState();

        this.addressConfirmDialog = $(element).find(".msax-AddressReplaceConfrimPopup");
        this.isShippingFromDialog = true;
        this.suggestedAddressText = ko.observable<any>(null);

        ShoppingCartService.OnUpdateCheckoutCart(this, this.updateShoppingCart);
        CartBase.OnClearPageMode(this, this.ClearPageMode);
        CartBase.OnPageModeSubscribers(this, this.PageModeSubscribers);
        this.IsPageModeSubscribersCalled = false;

        }

        private PageModeSubscribers(event, data) {
        if (!this.IsPageModeSubscribersCalled) {
            CartBase.OnUpdateFocusPageMode(PageModeOptions.ShippingView, this, this.ShippingViewPageMode);
            CartBase.OnUpdateFocusPageMode(PageModeOptions.ShippingViewCompleted, this, this.ShippingViewCompletedPageMode);
            CartBase.OnUpdateFocusPageMode(PageModeOptions.BillingView, this, this.BillingViewPageMode);
            CartBase.OnUpdateFocusPageMode(PageModeOptions.BillingViewcompleted, this, this.BillingViewCompletedPageMode);
            CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmView, this, this.ConfirmViewPageMode);
            CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmViewCompleted, this, this.ConfirmViewCompletedPageMode);
            this.IsPageModeSubscribersCalled = true;
        }
        }


        // NS by spriya - Bug 65682 : Firefox backspace delete issue

        private preventKeyPress(data, event) {

        if (event.keyCode == 8 && !Utils.isNullOrUndefined(event.target.tagName) && event.target.tagName.toLowerCase() == "select") {
            event.preventDefault();
            return false;
        }
        return true;
        }
          // NE by spriya - Bug 65682

        private ReplacerSuggestedAddress() {
        if (this.isShippingFromDialog) {
            this.tempStreet(this.avaSuggestedAddress.Street);
            this.tempStreet2(this.avaSuggestedAddress.Street2);
            this.tempCity(this.avaSuggestedAddress.City);
            this.tempState(this.avaSuggestedAddress.State);
            this.tempZipCode(this.avaSuggestedAddress.ZipCode);
            this.tempCountry(this.avaSuggestedAddress.Country);
            LoadingOverlay.closeRemoveItemDialog(this.addressConfirmDialog);
            this.getDeliveryMethods();
        }
        else {
            this.tempBillingCity(this.avaSuggestedAddress.City);
            this.tempBillingStreet(this.avaSuggestedAddress.Street);
            this.tempBillingStreet2(this.avaSuggestedAddress.Street2);
            this.tempBillingState(this.avaSuggestedAddress.State);
            this.tempBillingZipcode(this.avaSuggestedAddress.ZipCode);
            this.tempBillingCountry(this.avaSuggestedAddress.Country);
            LoadingOverlay.closeRemoveItemDialog(this.addressConfirmDialog);
            this.SetUserCartDataInfowithRedirect(this.userCartInfo);
        }
        this.avaSuggestedAddress = null;
        }

        private CancelSuggested() {
        LoadingOverlay.closeRemoveItemDialog(this.addressConfirmDialog);
        }

        private updateShoppingCart(event, data) {
        //CartBase.showError(false);
        // Hanldes the UpdateShoppingCart event.
        if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
            errorMessage = data.Errors[0].ErrorMessage;
            CartBase.showError(true);
            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

        }
        else {
            //CartData(data.ShoppingCart);
            CartBase.showError(false);
            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

        }
        }

        private LoadAddressState() {
        //Load State
        this.IsCartInfoLoaded = false;
        LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText);
        AFMExtendedService.GetAFMAddressState('USA').done((data) => {
            this._StateInfoClassList(data);
            var tempStateListValue = new StateProvinceInfoClass(null);
            tempStateListValue.CountryRegionId = 'USA';
            tempStateListValue.StateId = '';
            tempStateListValue.StateName = '--Select State--';
            this._StateInfoClassList.unshift(tempStateListValue);
            // HXM to fix bug 64792
            //if (!this.IsCartInfoLoaded) {
            //    this.tempBillingState('');
            //    this.tempState('');
            //}
            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
        }).fail((errors) => {
                errorMessage = "Error occured while getting addresslist";
                CartBase.showError(true);
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            });
        }

        private ClearPageMode(event, data) {
        CartBase.showError(false);
        this.shippingAddressCollectionView.hide();
        this.shippingAddressView.hide();
        this.paymentAddressView.hide();
        }

        private HideAddressFieldErrors() {
            $(document).find('.msax-OrderPhoneNumber1Error').hide();
            $(document).find('.msax-OrderPhoneNumber2Error').hide();
            $(document).find('.msax-OrderPhoneNumber3Error').hide();
            $(document).find('.msax-ShippingEmailError').hide();
            $(document).find('.msax-ShippingConfirmEmailError').hide();
            $(document).find('.msax-ShippingConfirmEmailError1').hide();
            $(document).find('.msax-ShippingEmail2Error').hide();
            $(document).find('.msax-OrderAddressFirstNameError').hide();
            $(document).find('.msax-OrderAddressLastNameError').hide();
            $(document).find('.msax-OrderAddressStreet1Error').hide();
            $(document).find('.msax-OrderAddressCityError').hide();
            $(document).find('.msax-OrderAddressStateError').hide();
            $(document).find('.msax-OrderAddressLastNameError').hide();
            $(document).find('.msax-OrderAddressZipCodeError').hide();

            $(document).find('.msax-ConfirmBillingEmailTextBoxError').hide();
            $(document).find('.msax-ConfirmBillingEmailTextBoxError1').hide();
            $(document).find('.msax-EmailTextBoxError').hide();
            $(document).find('.msax-PaymentAddressFirstNameError').hide();
            $(document).find('.msax-PaymentAddressLastNameError').hide();
            $(document).find('.msax-PaymentAddressCityError').hide();
            $(document).find('.msax-PaymentAddressStateError').hide();
            $(document).find('.msax-PaymentAddressStreet1Error').hide();
            $(document).find('.msax-PaymentAddressZipCodeError').hide();
            $(document).find('.msax-PaymentPhoneNumber1Error').hide();

        }

        private ShippingViewPageMode(event, data) {
            CartBase.showError(false);
            this.HideAddressFieldErrors();
        if (!Utils.isNullOrEmpty(CartData()) && !Utils.isNullOrEmpty(CartData().CustomerId)) {
            this.GetAddressCollection(true);
            this.shippingAddressCollectionView.show();
            $('.msax-ShippingAddressCollection').css('display', 'inline');
        }
        this.getZipCodeInfo();
        this.shippingAddressView.show();
        this.paymentAddressView.hide();
        //NS Developed By v-hapat for Bug:79338
        this.getUserCartInfo();
        $(document).find('.msax-ShoppingCart').hide();
        $(document).find('.msax-EditAddress').hide();
        $(document).find('.msax-ButtonControls').hide();
            //NE Developed By v-hapat for Bug:79338
        }

        private ShippingViewCompletedPageMode(event, data) {
            CartBase.showError(false);
            this.HideAddressFieldErrors();
        if (!Utils.isNullOrEmpty(CartData()) && !Utils.isNullOrEmpty(CartData().CustomerId))
            $('.msax-ShippingAddressCollection').css('display', 'inline');
        if (Utils.isNullOrUndefined(this.AvailableAddressCollection()) ||
            (Utils.isNullOrUndefined(this.AvailableAddressCollection().Addresses) || this.AvailableAddressCollection().Addresses.length == 0)) {
            this.GetAddressCollection(true);
            this.shippingAddressCollectionView.show();
        }
        this.getZipCodeInfo();
        this.shippingAddressView.show();
        this.paymentAddressView.hide();
        if (!Utils.isNullOrUndefined(this.ShippingAddress.ZipCode)) {
            this.getUserCartInfo();
        } else {
            this.getUserCartInfo();////NS Developed By v-hapat for Bug:79338
            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
        }
        //NS Developed By v-hapat for Bug:79338
        $(document).find('.msax-ShoppingCart').hide();
        $(document).find('.msax-EditAddress').hide();
        $(document).find('.msax-ButtonControls').hide();
            //NS Developed By v-hapat for Bug:79338
        }

        private BillingViewPageMode(event, data) {
            CartBase.showError(false);
            this.HideAddressFieldErrors();
            this.hideZipCodeMasterError();
        if (!Utils.isNullOrEmpty(CartData()) && !Utils.isNullOrEmpty(CartData().CustomerId))
            this.GetAddressCollection(false);
        this.shippingAddressCollectionView.hide();
        this.shippingAddressView.hide();
        this.paymentAddressView.show();
        //NS Developed By v-hapat for Bug:79338
        this.IsEditAllowed = true;
        this.getUserCartInfo();
        $(document).find('.msax-ShoppingCart').hide();
        $(document).find('.msax-EditAddress').hide();
        $(document).find('.msax-ButtonControls').hide();
            //$(document).find('.msax-Tax').show();
            //NE Developed By v-hapat for Bug:79338
        }

        private BillingViewCompletedPageMode(event, data) {
            CartBase.showError(false);
            this.HideAddressFieldErrors();
            this.hideZipCodeMasterError();
        //this.GetAddressCollection(false);
        this.shippingAddressCollectionView.hide();
        this.shippingAddressView.hide();
        this.paymentAddressView.show();
        this.IsEditAllowed = true;
        if (!Utils.isNullOrUndefined(this.ShippingAddress.ZipCode)) {
            this.getUserCartInfo();
        } else {
            this.getUserCartInfo();//NS Developed By v-hapat for Bug:79338
            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
        }
        //NS Developed By v-hapat for Bug:79338
        $(document).find('.msax-ShoppingCart').hide();
        $(document).find('.msax-EditAddress').hide();
        $(document).find('.msax-ButtonControls').hide();
            //NS Developed By v-hapat for Bug:79338
        }

        private ConfirmViewPageMode(event, data) {
        this.shippingAddressCollectionView.hide();
        this.shippingAddressView.hide();
        this.paymentAddressView.hide();
        $(document).find('.msax-ButtonControls').show();
        }

        private ConfirmViewCompletedPageMode(event, data) {
        this.shippingAddressCollectionView.hide();
        this.shippingAddressView.hide();
        this.paymentAddressView.hide();
        $(document).find('.msax-ButtonControls').show();
        }

        private UpdateShippingAddressfromAvailableAddresses(data: Address) {
        this.selectedAddress(data);
        var divPosition = $('.msax-ButtonSection').offset();
        $("html, body").animate({ scrollTop: divPosition.top }, "slow");

        }
        private GetAddressCollection(IsShipping) {
        /*CustomerService.GetAddresses().done((data) => {
            this.AvailableAddressCollection(data);*/
        AFMExtendedService.GetShippingAddresses(IsShipping).done((data) => {
            if (IsShipping)
                this.AvailableAddressCollection(data);
            else if (data != null && data.Addresses != null && data.Addresses.length > 0) {
                var billingAddress = data.Addresses[0];
                this.tempBillingFirstName(billingAddress.FirstName);
                this.tempBillingLastName(billingAddress.LastName);
                this.tempBillingCountry(billingAddress.Country);
                this.tempBillingStreet(billingAddress.Street);
                this.tempBillingStreet2(billingAddress.Street2);
                this.tempBillingCity(billingAddress.City);
                this.tempBillingState(billingAddress.State);
                this.tempBillingZipcode(billingAddress.ZipCode);
                this.tempBillingAddressType(billingAddress.AddressType);
                if (billingAddress.RecordId != null && billingAddress.RecordId != 0)
                    this.tempBillingRecordId(billingAddress.RecordId);
                this.tempBillingPhone(Utils.RemovePhoneFormat(billingAddress.Phone));
                this.tempBillingEmail(billingAddress.Email);
                this.BillingconfirmEmailValue(billingAddress.Email);
            }
        });
        }
        private resetDeliveryMethods() {
        this.availableDeliveryMethods(null);
        this.orderShippingOptions(new SelectedDeliveryOptionClass(null));
        }


        private getUserCartInfo() {
        LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText);
        this.IsCartInfoLoaded = true;
        AFMExtendedService.GetUserCartInfo()
            .done((data) => {
                if (!Utils.isNullOrUndefined(data)) {
                    //NS Developed By v-hapat for Bug:79338
                    if (data.ShippingAddress != null && data.ShippingAddress.FirstName != null) {
                        this.ShippingAddress = data.ShippingAddress;
                        this.tempFirstName(this.ShippingAddress.FirstName);
                        this.tempLastName(this.ShippingAddress.LastName);
                        this.tempStreet(this.ShippingAddress.Street);
                        if (!Utils.isNullOrEmpty(this.ShippingAddress.Street2))
                            this.tempStreet2(this.ShippingAddress.Street2);
                        else
                            this.tempStreet2('');
                        this.tempCity(this.ShippingAddress.City);
                        this.tempState(this.ShippingAddress.State);
                        this.tempCountry(this.ShippingAddress.Country);
                        this.tempZipCode(this.ShippingAddress.ZipCode);

                        this.tempPhone(Utils.RemovePhoneFormat(this.ShippingAddress.Phone));
                        this.tempPhone2(Utils.RemovePhoneFormat(this.ShippingAddress.Phone2));
                        this.tempPhone3(Utils.RemovePhoneFormat(this.ShippingAddress.Phone3));
                        this.tempEmail(this.ShippingAddress.Email);
                        this.tempEmail2(this.ShippingAddress.Email2);
                        this.tempAddressType(AddressType.Delivery);
                        //CS Developed by spriya - Retail Address record ID if already present
                        if (this.ShippingAddress.RecordId != null || this.ShippingAddress.RecordId != 0)
                            this.tempRecordId(this.ShippingAddress.RecordId);
                        //CE Developed by spriya
                        //NS Developed By v-hapat
                        if (!Utils.isNullOrEmpty(this.ShippingAddress.Phone))
                            this.ShippingAddress.Phone = Utils.SetPhoneFormat(this.tempPhone());
                        if (!Utils.isNullOrEmpty(this.ShippingAddress.Phone2))
                            this.ShippingAddress.Phone2 = Utils.SetPhoneFormat(this.tempPhone2());
                        if (!Utils.isNullOrEmpty(this.ShippingAddress.Phone3))
                            this.ShippingAddress.Phone3 = Utils.SetPhoneFormat(this.tempPhone3());
                        //NS Developed By v-hapat
                        this.ShippingconfirmEmailValue(this.ShippingAddress.Email);
                    }
                    else {
                        this.tempState('');
                    }
                    //NE Developed By v-hapat for Bug:79338


                    //if (Utils.isNullOrEmpty(this.PaymentAddress.ZipCode))
                    if (data.PaymentAddress != null && data.PaymentAddress.FirstName != null)
                        this.PaymentAddress = data.PaymentAddress;
                    else if (Utils.isNullOrEmpty(this.PaymentAddress) || Utils.isNullOrEmpty(this.PaymentAddress.FirstName)) {
                        this.PaymentAddress = new AddressClass(null);
                    }


                    this.tempBillingFirstName(this.PaymentAddress.FirstName);
                    this.tempBillingLastName(this.PaymentAddress.LastName);
                    this.tempBillingStreet(this.PaymentAddress.Street);
                    this.tempBillingStreet2(this.PaymentAddress.Street2);
                    this.tempBillingCity(this.PaymentAddress.City);
                    if (this.PaymentAddress.State != null)
                        this.tempBillingState(this.PaymentAddress.State);
                    else
                        this.tempBillingState('');
                    this.tempBillingCountry(this.PaymentAddress.Country);
                    this.tempBillingZipcode(this.PaymentAddress.ZipCode);
                    this.tempBillingEmail(this.PaymentAddress.Email);
                    this.tempBillingPhone(Utils.RemovePhoneFormat(this.PaymentAddress.Phone));
                    this.BillingconfirmEmailValue(this.PaymentAddress.Email);

                    this.tempBillingAddressType(AddressType.Invoice);
                    if (!Utils.isNullOrEmpty(this.PaymentAddress.Phone))
                        this.PaymentAddress.Phone = Utils.SetPhoneFormat(this.tempBillingPhone());

                    if (
                        this.tempFirstName() == this.tempBillingFirstName()
                        && this.tempLastName() == this.tempBillingLastName()
                        && this.tempStreet() == this.tempBillingStreet()
                        && this.tempStreet2() == this.tempBillingStreet2()
                        && this.tempCity() == this.tempBillingCity()
                        && this.tempState() == this.tempBillingState()
                        && this.tempCountry() == this.tempBillingCountry()
                        && this.tempZipCode() == this.tempBillingZipcode()
                        && this.tempPhone() == this.tempBillingPhone()
                        && this.tempEmail() == this.tempBillingEmail()
                        ) {
                        this.IsEditAllowed = false;
                        this.isBillingAddressSameAsShippingAddress(true);
                    }
                    else {
                        this.IsEditAllowed = false;
                        this.isBillingAddressSameAsShippingAddress(false);
                    }
                }
                this.IsEditAllowed = true;
                // NC CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
                if (!Utils.isNullOrEmpty(data.IsOkToText))
                    this.isOktoText(data.IsOkToText)

                    //NS developed by v-hapat for BUG-63467 on dated 11/13/2014
                    if (!Utils.isNullOrEmpty(data.IsOptinDeals))
                    this.isOptinDeals(data.IsOptinDeals)
                    //NE developed by v-hapat for BUG-63467 on dated 11/13/2014
                   LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            })
            .fail((errors) => {
            });
        }

        private UseAddressButton(data) {
            this.HideAddressFieldErrors();
            this.hideZipCodeMasterError();
            
        this.UpdateShippingAddressfromAvailableAddresses(data);
            this.shippingOptionsNextClick();
            var $wrapper = $(".msax-ShippingAddressform");

            // Trigger change to invoke validators.
            $wrapper.find("input,select").each((index, elem) => {
                $(elem).change();
                $(elem).blur();
            });
        }
         //event added by Sakalija for bug no 72474
        private validatePhoneTextBox(element, valueAccessor) {
        var srcElement = valueAccessor.target;
        $("#" + srcElement.id).removeClass("msax-RequiredError");
        var regExp = new RegExp('^\\d{10}$');
        if (!Utils.isNullOrEmpty(srcElement.value)) {
            if (!regExp.test(srcElement.value)) {
                $("#" + srcElement.id).attr("msax-isValid", false);
                $(document).find('.msax-OrderPhoneNumber1Error').show();
                $("#" + srcElement.id).addClass("msax-RequiredError");
                return false;
            }
            $("#" + srcElement.id).attr("msax-isValid", true);
            $(document).find('.msax-OrderPhoneNumber1Error').hide();
        }
        else {
            $("#" + srcElement.id).attr("msax-isValid", false);
            $(document).find('.msax-OrderPhoneNumber1Error').show();
            $("#" + srcElement.id).addClass("msax-RequiredError");
            return false;
        }      
        CartBase.showError(false);
        return true;
        }

        private validateBillingPhoneTextBox(element, valueAccessor) {
            var srcElement = valueAccessor.target;
            $("#" + srcElement.id).removeClass("msax-RequiredError");
            var regExp = new RegExp('^\\d{10}$');
            if (!Utils.isNullOrEmpty(srcElement.value)) {
                if (!regExp.test(srcElement.value)) {
                    $("#" + srcElement.id).attr("msax-isValid", false);
                    $(document).find('.msax-PaymentPhoneNumber1Error').show();
                    $("#" + srcElement.id).addClass("msax-RequiredError");
                    return false;
                }
                $("#" + srcElement.id).attr("msax-isValid", true);
                $(document).find('.msax-PaymentPhoneNumber1Error').hide();
            }
            else {
                $("#" + srcElement.id).attr("msax-isValid", false);
                $(document).find('.msax-PaymentPhoneNumber1Error').show();
                $("#" + srcElement.id).addClass("msax-RequiredError");
                return false;
            }
            CartBase.showError(false);
            return true;
        }

        private validateEmail(element, valueAccessor) {
            var srcElement = valueAccessor.target;
            $("#" + srcElement.id).removeClass("msax-RequiredError");
        //var regExp = new RegExp('^[\\w-]+(?:\\.[\\w-]+)*@(?:[\\w-]+\\.)+[a-zA-Z]{2,7}$');
        var regExp = new RegExp('^[\\w-]+(?:\\.[\\w-]+)*@((?=\\d+\\.\\d+\\.\\d+\\.\\d+$)(?:(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])\.?){4}$)|((?:[\\w-]+\\.)+[a-zA-Z]{2,7}$)');
        if (!Utils.isNullOrEmpty(srcElement.value)) {
            if (!regExp.test(srcElement.value)) {
                $("#" + srcElement.id).attr("msax-isValid", false);
                if (srcElement.id == "ShippingEmail") {
                    $(document).find('.msax-ShippingEmailError').show();
                    $("#" + srcElement.id).addClass("msax-RequiredError");
                }
                else {
                    $(document).find('.msax-EmailTextBoxError').show();
                    $("#" + srcElement.id).addClass("msax-RequiredError");
                }
                return false;
            }
        }
        else {
            $("#" + srcElement.id).attr("msax-isValid", false);
        if (srcElement.id == "ShippingEmail")
            $(document).find('.msax-ShippingEmailError').show();
        else
            $(document).find('.msax-EmailTextBoxError').show();
            $("#" + srcElement.id).addClass("msax-RequiredError");
            return true;
        }
            $("#" + srcElement.id).attr("msax-isValid", true);
            if (srcElement.id == "ShippingEmail")
                $(document).find('.msax-ShippingEmailError').hide();
            else
                $(document).find('.msax-EmailTextBoxError').hide();
        return true;
        }

        private validateAddressName(srcElement: Element) {
        var $element = $(srcElement);
        var value: string = $element.val();
        var regExp = new RegExp('^[a-zA-Z ]{0,25}$');
        if (!Utils.isNullOrEmpty(value)) {
            if (!regExp.test(value)) {
               // errorMessage = Resources.String_1170;
                //CartBase.showError(true);
                return false;
            }
        }
        else {
           // errorMessage = Resources.String_1170;
           // CartBase.showError(true);
            return false;
        }
        //CartBase.showError(false);
        return true;
        }

        private validateEmail2(element, valueAccessor) {

            var srcElement = valueAccessor.target;
            $("#" + srcElement.id).removeClass("msax-RequiredError");
        //var regExp = new RegExp('^[\\w-]+(?:\\.[\\w-]+)*@(?:[\\w-]+\\.)+[a-zA-Z]{2,7}$');
        var regExp = new RegExp('^[\\w-]+(?:\\.[\\w-]+)*@((?=\\d+\\.\\d+\\.\\d+\\.\\d+$)(?:(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])\.?){4}$)|((?:[\\w-]+\\.)+[a-zA-Z]{2,7}$)');
        if (!Utils.isNullOrEmpty(srcElement.value)) {
            if (!regExp.test(srcElement.value)) {
                $("#" + srcElement.id).attr("msax-isValid", false);
                $(document).find('.msax-ShippingEmail2Error').show();
                $("#" + srcElement.id).addClass("msax-RequiredError");
                return false;
            }
        }
            $("#" + srcElement.id).attr("msax-isValid", true);
            $(document).find('.msax-ShippingEmail2Error').hide();
            return true;
        CartBase.showError(false);
        return true;
        }

        private validateAltPhoneTextBox(element, valueAccessor) {
            var srcElement = valueAccessor.target;
            $("#" + srcElement.id).removeClass("msax-RequiredError");
        var regExp = new RegExp('^\\d{10}$');
            if (!Utils.isNullOrEmpty(srcElement.value)) {
                if (!regExp.test(srcElement.value)) {
                    $("#" + srcElement.id).attr("msax-isValid", false);
                    if (srcElement.id == "OrderPhoneNumber2")
                        $(document).find('.msax-OrderPhoneNumber2Error').show();
                    if (srcElement.id == "OrderPhoneNumber3")
                        $(document).find('.msax-OrderPhoneNumber3Error').show();
                    $("#" + srcElement.id).addClass("msax-RequiredError");
                    return false;
                }
            }
            $("#" + srcElement.id).attr("msax-isValid", true);
            if (srcElement.id == "OrderPhoneNumber2")
                $(document).find('.msax-OrderPhoneNumber2Error').hide();
            if (srcElement.id == "OrderPhoneNumber3")
                $(document).find('.msax-OrderPhoneNumber3Error').hide();
        
             
        CartBase.showError(false);
        return true;
        }
         //Input validation on blur event added by Sakalija for bug no 72474
        private validateInputOnBlur(element, valueAccessor) {
            this.hideZipCodeMasterError();    
            var srcElement = valueAccessor.target;
            $("#" + srcElement.id).removeClass("msax-RequiredError");
        if (!Utils.isNullOrUndefined(srcElement)) {
            if (!Utils.isNullOrEmpty(srcElement.value)) {
                $("#" + srcElement.id).attr("msax-isValid", true);              
                $(document).find('.msax-' + srcElement.id + 'Error').hide();
                return true;
            } else {
                $("#" + srcElement.id).attr("msax-isValid", false);
                $(document).find('.msax-' + srcElement.id + 'Error').text(srcElement.title);
                $(document).find('.msax-' + srcElement.id + 'Error').show();
                $("#" + srcElement.id).addClass("msax-RequiredError");
                return true;
            }
        }
        return true;
        }

        //CR 306 - Address validation off - CS Developed by spriya Dated 09/14/2015
        private showZipCodeMasterError() {
            $(document).find('.msax-PaymentAddressCitySBError').show();
            $(document).find('#PaymentAddressCity').attr("msax-isValid", false); // Bug fix - 107014
            $(document).find('.msax-PaymentAddressStateSBError').show();
            $(document).find('#PaymentAddressState').attr("msax-isValid", false); // Bug fix - 107014
            $(document).find('.msax-PaymentAddressZipCodeSBError').show();
            $(document).find('#PaymentAddressZipCode').attr("msax-isValid", false); // Bug fix - 107014
        }

        private hideZipCodeMasterError() {
            $(document).find('.msax-PaymentAddressCitySBError').hide();
            $(document).find('#PaymentAddressCity').attr("msax-isValid", true); // Bug fix - 107014
            $(document).find('.msax-PaymentAddressStateSBError').hide();
            $(document).find('#PaymentAddressState').attr("msax-isValid", true); // Bug fix - 107014
            $(document).find('.msax-PaymentAddressZipCodeSBError').hide();
            $(document).find('#PaymentAddressZipCode').attr("msax-isValid", true); // Bug fix - 107014
        }
        //CE 306 - Address validation off - CS Developed by spriya Dated 09/14/2015

        private paymentInformationNextClick() {
        CartBase.showError(false);
        this.userCartInfo.PaymentAddress = this.PaymentAddress;
        this.userCartInfo.ShippingAddress = this.ShippingAddress;
        LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...
        AFMExtendedService.ValidateZipcode(this.PaymentAddress.Country, this.PaymentAddress.ZipCode).done((data) => {

            if (data) {
                // NC CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
                this.userCartInfo.IsOkToText = this.isOktoText();

                //NS developed by v-hapat for BUG-63467 on dated 11/13/2014
                this.userCartInfo.IsOptinDeals = this.isOptinDeals();
                //NE developed by v-hapat for BUG-63467 on dated 11/13/2014

                //this.SetUserCartDataInfowithRedirect(this.userCartInfo);
                //CS Developed by spriya - Phone Format Dated 1/15/2014
                if (!Utils.isNullOrEmpty(this.userCartInfo.PaymentAddress)) {
                    this.userCartInfo.PaymentAddress.AddressType = AddressType.Invoice;
                    this.userCartInfo.PaymentAddress.Phone = Utils.SetPhoneFormat(this.userCartInfo.PaymentAddress.Phone);
                }
                //CE Developed by spriya
                AFMExtendedService.ValidateAddress(this.userCartInfo.PaymentAddress).done((data) => {
                    this.hideZipCodeMasterError();
                    //CS Developed by spriya Dated 12/07/2014
                    if (data.IsAddressValid && data.IsServiceVerified) {
                        this.avaSuggestedAddress = data.suggestedAddress;                        
                        //NS - DMND0026203 - FDD - Shipping Address Validation - Developed by Kishore
                        this.avaSuggestedAddress.Street2 = data.Street2; 
                        //Assigning empty value to street2 if make it pass in the next if condition else tolowercase will raise exception
                        if (Utils.isNullOrEmpty(this.PaymentAddress.Street2)) {
                            this.PaymentAddress.Street2 = "";
                        }
                        // Commented this code for new address service change as this is a part of the Avalra.Added Street2 for validations
                        //var fullStreetAddress = this.PaymentAddress.Street
                        //     if (!Utils.isNullOrEmpty(this.PaymentAddress.Street) && !Utils.isNullOrEmpty(this.PaymentAddress.Street2)) {
                        //    fullStreetAddress = this.PaymentAddress.Street + " " + this.PaymentAddress.Street2;
                        //}
                        //if (fullStreetAddress.toLowerCase() != data.suggestedAddress.Street.toLowerCase()
                        if (this.PaymentAddress.Street.toLowerCase() != data.suggestedAddress.Street.toLowerCase()
                            || this.PaymentAddress.Street2.toLowerCase() != data.Street2.toLowerCase() 
                            || this.PaymentAddress.City.toLowerCase() != data.suggestedAddress.City.toLowerCase()
                            || this.PaymentAddress.State.toLowerCase() != data.suggestedAddress.State.toLowerCase()
                            || this.PaymentAddress.ZipCode.toLowerCase() != data.suggestedAddress.ZipCode.toLowerCase()) {
                            this.isShippingFromDialog = false;
                            
                            //Added if condition for accomodating street2 in the suggested address popup
                            if (!Utils.isNullOrEmpty(this.avaSuggestedAddress.Street2)) {
                                this.suggestedAddressText = '<br/>' + this.avaSuggestedAddress.Street + '<br/>' + this.avaSuggestedAddress.Street2 + '<br/>' + this.avaSuggestedAddress.City + '<br/>' +
                                this.avaSuggestedAddress.State + '<br/>' + this.avaSuggestedAddress.ZipCode;
                            }
                            else {
                                this.suggestedAddressText = '<br/>' + this.avaSuggestedAddress.Street + '<br/>' + this.avaSuggestedAddress.City + '<br/>' +
                                this.avaSuggestedAddress.State + '<br/>' + this.avaSuggestedAddress.ZipCode;
                            }
                            //NE - DMND0026203 - FDD - Shipping Address Validation - Developed by Kishore
                            //LoadingOverlay.openAddressItemDialog(this.addressConfirmDialog);
                            // NS - Changed by Aniket for UI change bug fix - winter release - on 29/12/2015 - added two parameter for button text
                            LoadingOverlay.openAddressDialog(this.addressConfirmDialog, this.ReplacerSuggestedAddress, this.CancelSuggested, this, this.suggestedAddressText, Resources.String_1419, Resources.String_123);
                            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        }
                        else {
                            //AFMExtendedService.SendOrderSummary(CartData.AFMOrderSummaryData).done((data) => {
                            this.SetUserCartDataInfowithRedirect(this.userCartInfo);
                            //});
                        }
                    } else if (data.IsAddressValid == false && data.IsServiceVerified == false){
                        errorMessage = Resources.String_1258;
                        CartBase.showError(true);
                        this.showZipCodeMasterError();
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    } else if (data.IsAddressValid == true && data.IsServiceVerified == false) {
                        CartBase.showError(false);
                        this.SetUserCartDataInfowithRedirect(this.userCartInfo);
                    }
                    else {
                        errorMessage = Resources.String_109;
                        CartBase.showError(true);
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    }
                });

                //NS Developed By v-hapat for Bug:79338
                (!this.CartMode.isBillinggModePaased)
                this.CartMode.isBillinggModePaased = true;
                //NE Developed By v-hapat for Bug:79338
            }
            else {
                errorMessage = Resources.String_1150;
                CartBase.showError(true);
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            }
        }).fail((error) => {
                errorMessage = Resources.String_1151;
                CartBase.showError(true);
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            });
        }

        private SetUserCartDataInfowithRedirect(userCartInfo: UserCartData) {
        LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); //Start Validating Billing Address
        AFMExtendedService.SetUserCartInfo(userCartInfo)
            .done((data) => {
                if (IsConfirmedit) {
                    CartBase.UpdateFocusPageModeValue(PageModeOptions.ConfirmView);
                }
                else {
                    var cartIdVal = CartData().CartId;
                    //CartBase.UpdateFocusPageModeValue(PageModeOptions.PaymentView);
                    AFMExtendedService.RedirectToPayment().done((data) => {
                        window.location.href = data;
                    });;
                }
            })
            .fail((errors) => {
                errorMessage = Resources.String_66;
                CartBase.showError(true);
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            });
        }


        private paymentInformationPreviousClick() {
        CartBase.showError(false);
        CartBase.UpdateFocusPageModeValue(PageModeOptions.ShippingViewCompleted);
            // $(document).find('.msax-Tax').show();
        }


        private shippingOptionsNextClick() {
        CartBase.showError(false);


        LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...
        AFMExtendedService.ValidateZipcode(this.ShippingAddress.Country, this.ShippingAddress.ZipCode).done((data) => {
            if (data) {

                //NS Developed By v-hapat for Bug:79338
                if (!this.CartMode.isShippingModePaased)
                    this.CartMode.isShippingModePaased = true;
                //NE Developed By v-hapat for Bug:79338
                if (!Utils.isNullOrEmpty(this.ShippingAddress.Phone)) {
                    this.ShippingAddress.Phone = Utils.SetPhoneFormat(this.ShippingAddress.Phone);
                }
                if (!Utils.isNullOrEmpty(this.ShippingAddress.Phone2)) {
                    this.ShippingAddress.Phone2 = Utils.SetPhoneFormat(this.ShippingAddress.Phone2);
                }
                if (!Utils.isNullOrEmpty(this.ShippingAddress.Phone3)) {
                    this.ShippingAddress.Phone3 = Utils.SetPhoneFormat(this.ShippingAddress.Phone3);
                }
                //CS Developed by spriya Dated 12/08/2014 , suggested adddress
                if (this.ShippingAddress != null) {
                    this.ShippingAddress.AddressType = AddressType.Delivery;
                }
                AFMExtendedService.ValidateAddress(this.ShippingAddress).done((data) => {
                    if (data.IsAddressValid) {
                        this.avaSuggestedAddress = data.suggestedAddress; 
                        //NS - DMND0026203 - FDD - Shipping Address Validation - Developed by Kishore
                        this.avaSuggestedAddress.Street2 = data.Street2; 
                        //Assigning empty value to street2 if make it pass in the next if condition else tolowercase will raise exception
                        if (Utils.isNullOrEmpty(this.ShippingAddress.Street2)) {
                               this.ShippingAddress.Street2 = "";
                        }
                        // Commented this code for new address service change as this is a part of the Avalra . Added Street2 for validations
                        //var fullStreetAddress = this.ShippingAddress.Street
                        //     if (!Utils.isNullOrEmpty(this.ShippingAddress.Street) && !Utils.isNullOrEmpty(this.ShippingAddress.Street2)) {
                        //    fullStreetAddress = this.ShippingAddress.Street + " " + this.ShippingAddress.Street2;
                        //}
                        //if (fullStreetAddress.toLowerCase() != data.suggestedAddress.Street.toLowerCase()
                        if (this.ShippingAddress.Street.toLowerCase() != data.suggestedAddress.Street.toLowerCase()
                            || this.ShippingAddress.Street2.toLowerCase() != data.Street2.toLowerCase()
                            || this.ShippingAddress.City.toLowerCase() != data.suggestedAddress.City.toLowerCase()
                            || this.ShippingAddress.State.toLowerCase() != data.suggestedAddress.State.toLowerCase()
                            || this.ShippingAddress.ZipCode.toLowerCase() != data.suggestedAddress.ZipCode.toLowerCase()) {
                            this.isShippingFromDialog = true;
                            
                            //Added if condition for accomodating street2 in the suggested address popup
                            if (!Utils.isNullOrEmpty(this.avaSuggestedAddress.Street2)) {
                                this.suggestedAddressText = '<br/>' + this.avaSuggestedAddress.Street + '<br/>' + this.avaSuggestedAddress.Street2 + '<br/>' + this.avaSuggestedAddress.City + '<br/>' +
                                this.avaSuggestedAddress.State + '<br/>' + this.avaSuggestedAddress.ZipCode;
                            }
                            else {
                                this.suggestedAddressText = '<br/>' + this.avaSuggestedAddress.Street + '<br/>' + this.avaSuggestedAddress.City + '<br/>' +
                                this.avaSuggestedAddress.State + '<br/>' + this.avaSuggestedAddress.ZipCode;
                            }
                            //NE - DMND0026203 - FDD - Shipping Address Validation - Developed by Kishore                           
                            //LoadingOverlay.openAddressItemDialog(this.addressConfirmDialog);
                            // NS - Changed by Aniket for UI change bug fix - winter release - on 29/12/2015 - added two parameter for button text
                            LoadingOverlay.openAddressDialog(this.addressConfirmDialog, this.ReplacerSuggestedAddress, this.CancelSuggested, this, this.suggestedAddressText, Resources.String_1419, Resources.String_123);
                            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        }
                        else {
                            this.getDeliveryMethods();
                        }
                    } else {
                        if (!AFM.Utils.isNullOrEmpty(data) && !AFM.Utils.isNullOrEmpty(data.ErrorMessage) && data.ErrorMessage.toLowerCase() == Resources.String_1167)
                            errorMessage = Resources.String_1166;
                        else
                            errorMessage = Resources.String_1111;
                        CartBase.showError(true);
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        //CE Developed by spriya Dated 12/08/2014
                    }
                }).fail((error) => {
                        errorMessage = Resources.String_1151;
                        CartBase.showError(true);
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    });
            }
            else {
                errorMessage = Resources.String_1150;
                CartBase.showError(true);
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            }
        }).fail((error) => {
                errorMessage = Resources.String_1151;
                CartBase.showError(true);
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            });
        }

        private shippingOptionsPreviousClick() {
        CartBase.showError(false);
        if (!Utils.isNullOrEmpty(msaxValues.msax_GoBackUrl))
            window.location.href = msaxValues.msax_GoBackUrl;
        }

        private getDeliveryMethods() {
        this.resetDeliveryMethods();
        var shippingOptions = this.orderShippingOptions();
        shippingOptions.CustomAddress = this.ShippingAddress;
        this.userCartInfo.ShippingAddress = this.ShippingAddress;
        this.userCartInfo.PaymentAddress = this.PaymentAddress;

        // NC CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
        this.userCartInfo.IsOkToText = this.isOktoText();
        ////NS IsOptinDeals is enabled by default -Sakalija for bug:72453
        this.userCartInfo.IsOptinDeals = this.isOptinDeals();
        //NE IsOptinDeals is enabled by default

        //NS Developed By v-hapat for Bug:79338
        AFMExtendedService.SetUserCartInfo(this.userCartInfo).
            done((tempUSer) => {
                this.AFMZipCode(this.ShippingAddress.ZipCode.substr(0, 5));
                this.updateZipCodeInfo();
                shippingOptions.LineId = CartData().Items[0].LineId;
                shippingOptions.DeliveryPreferenceId = "1";
                this.orderShippingOptions(shippingOptions);
                CheckoutService.GetDeliveryOptionsInfo()
                    .done((data) => {
                        if (data.AddressVerficationResult) {
                            this.availableDeliveryMethods(data.DeliveryOptions);
                            var shippingOptions = this.orderShippingOptions();
                            if (this.availableDeliveryMethods().length == 1) {
                                shippingOptions.DeliveryModeText = this.availableDeliveryMethods()[0].Description;
                                shippingOptions.DeliveryModeId = this.availableDeliveryMethods()[0].Id;
                            }
                            this.orderShippingOptions(shippingOptions);
                            this.syncDeliveryOptionsNextClick();
                            this.shippingAddressView.hide();
                            $('.msax-ShippingAddressCollection').css('display', 'none');
                            //this.shippingAddressCollectionView.hide();
                            CartBase.showError(false);
                            //CartBase.UpdateFocusPageModeValue(PageModeOptions.BillingView);
                            if (IsConfirmedit)
                                CartBase.UpdateFocusPageModeValue(PageModeOptions.ConfirmView);
                            else
                                CartBase.UpdateFocusPageModeValue(PageModeOptions.BillingView);
                            //$(document).find('.msax-Tax').show();
                            this.IsEditAllowed = true;
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        }
                        else {
                            errorMessage = Resources.String_1111;
                            CartBase.showError(true);
                            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        }
                    })
                    .fail((errors) => {
                        errorMessage = Resources.String_66;
                        CartBase.showError(true);
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    });
            });
        }


        //NS Developed by ysrini for AFM_TFS_40686 dated  07/01/2014
        private syncDeliveryOptionsNextClick() {
        this.orderShippingOptions().DeliveryModeId = "HD";
        //this.selectShippingOption(99);
        this.setShippingOptions();
            //this.showFragment(this._fragments.PaymentInformation);
        }

        private selectShippingOption(selectedShippingOption: DeliveryOption) {
        this.orderShippingOptions().DeliveryModeText = selectedShippingOption.Description;
        return true;
        }

        private setShippingOptions() {
        LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...

        AFMExtendedService.SetShippingtoLineItems(this.orderShippingOptions(), ShoppingCartDataLevel.All)
            .done((data) => {
                this.updateSelectedShippingOptions(data.ShoppingCart);
                this.IsEditAllowed = true;
            })
            .fail((errors) => {
                errorMessage = Resources.String_67;
                CartBase.showError(true);
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            });
        }

        private updateSelectedShippingOptions(cart: ShoppingCart) {
        cart.SelectedDeliveryOption = this.orderShippingOptions();
        for (var i = 0; i < cart.Items.length; i++) {
            cart.Items[i].SelectedDeliveryOption = this.orderShippingOptions();
        }

        CartData(cart);
        }

        private updateZipCodeInfo() {

        AFMExtendedService.SetZipCode(this.AFMZipCode(), false)
            .done((data) => {
                if (this.AFMZipCode() != AFMZipCodeData()) {
                    errorMessage = Resources.String_1174;
                    CartBase.showInfo(true);
                }
                else
                    CartBase.showInfo(false);

                AFMZipCodeData(this.AFMZipCode());
            })
            .fail((errors) => {
                errorMessage = Resources.String_113;
                CartBase.showError(true);
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            });
        }

        private getZipCodeInfo() {
        AFMExtendedService.GetZipCode().done((data) => {
            this.AFMZipCode(data.AFMZipCode.substr(0, 5));
            AFMZipCodeData(this.AFMZipCode())
                // If using checkout control in demo mode auto fill data.
                if (msaxValues.msax_IsDemoMode.toLowerCase() == "true")
                this.autoFillCheckout();
            else {
                this.tempCountry('USA');
                this.tempBillingCountry('USA');
                this.tempZipCode(this.AFMZipCode());
            }

            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
        }).fail((error) => {
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            });
        }

        private loadXMLDoc(filename) {
        var xhttp;

        if (XMLHttpRequest) {
            xhttp = new XMLHttpRequest();
        }
        else {
            xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhttp.open("GET", filename, false);
        xhttp.send();
        return xhttp.responseXML;
        }

        private autoFillCheckout() {
        var xmlDoc = this.loadXMLDoc("/Common/DemoData/CheckoutData.xml");

        var address = xmlDoc.getElementsByTagName(Resources.String_22);
        var name = address[0].getElementsByTagName(Resources.String_29);
        var firstname = address[0].getElementsByTagName(Resources.String_1186.replace(' ', ''));
        var lastname = address[0].getElementsByTagName(Resources.String_1188.replace(' ', ''));
        var street = address[0].getElementsByTagName(Resources.String_55);
        var city = address[0].getElementsByTagName(Resources.String_23);
        var state = address[0].getElementsByTagName(Resources.String_56);
        var country = address[0].getElementsByTagName(Resources.String_1122);
        var zipcode = address[0].getElementsByTagName(Resources.String_57);
        var email = xmlDoc.getElementsByTagName(Resources.String_58);

        var payment = xmlDoc.getElementsByTagName(Resources.String_59);
        var cardNumber = payment[0].getElementsByTagName(Resources.String_60);
        var ccid = payment[0].getElementsByTagName(Resources.String_39);

        var cartdata = CartData();
        if (cartdata != null) {
            cartdata.SelectedDeliveryOption = new SelectedDeliveryOptionClass(null);
            cartdata.SelectedDeliveryOption.CustomAddress = new AddressClass(null);
            cartdata.SelectedDeliveryOption.CustomAddress.FirstName = firstname[0].textContent;
            cartdata.SelectedDeliveryOption.CustomAddress.LastName = lastname[0].textContent;
            CartData(cartdata);
        }
        //this.tempEmail(email[0].textContent);
        //this.confirmEmailValue(email[0].textContent);
        this.tempStreet(street[0].textContent);
        this.tempCity(city[0].textContent);
        this.tempState(state[0].textContent);
        this.tempCountry(country[0].textContent);
        this.tempFirstName(firstname[0].textContent);
        this.tempLastName(lastname[0].textContent);
        if (this.AFMZipCode() != null)
            this.tempZipCode(this.AFMZipCode());
        else
            this.tempZipCode(zipcode[0].textContent);

            //var billingAddress = new AddressClass(null);
            //billingAddress.Email = email[0].textContent;
            //this.confirmEmailValue(email[0].textContent);
            //billingAddress.FirstName = firstname[0].textContent;
            //billingAddress.LastName = lastname[0].textContent;
            //billingAddress.Street = street[0].textContent;
            //billingAddress.City = city[0].textContent;
            //billingAddress.State = state[0].textContent;
            //billingAddress.ZipCode = zipcode[0].textContent;
            //this.PaymentAddress(billingAddress);
        }

        //private resetAll() {
        //    this.resetDeliveryMethods();

        //    var payment = new PaymentClass(null);
        //    payment.PaymentAddress = new AddressClass(null);
        //    payment.ExpirationYear = 2060;
        //    payment.ExpirationMonth = 1;
        //    this.paymentCard(payment);
        //    this.tempCity('');
        //    this.tempState('');
        //    this.tempStreet('');
        //    this.tempZipCode('');
        //    this.isBillingAddressSameAsShippingAddress(false);
        //    this.confirmEmailValue('');
        //}

        private validateShippingConfirmEmail(srcElement: Element) {
        var $element = $(srcElement);
        var value: string = $element.val();

        if (Utils.isNullOrEmpty($element.val()) && Utils.isNullOrUndefined(this.ShippingAddress.Email)) {
            $(srcElement).attr("msax-isValid", false);
            $(document).find('.msax-ShippingConfirmEmailError1').show();
            return false;
        }
        else if (Utils.isNullOrUndefined(this.ShippingAddress.Email) && !Utils.isNullOrEmpty($element.val())) {
            $(srcElement).attr("msax-isValid", false);
            $(document).find('.msax-ShippingConfirmEmailError1').show();
            return false;
        }
        else if (Utils.isNullOrEmpty($element.val()) && Utils.isNullOrEmpty(this.ShippingAddress.Email)) {
            $(srcElement).attr("msax-isValid", false);
            $(document).find('.msax-ShippingConfirmEmailError1').show();
            return false;
        }
        else if ($element.val().toString().toLowerCase() !== this.ShippingAddress.Email.toString().toLowerCase()) {
            // NC HXM Bug Id - 65194. Error message when e-mail's don't match is not customer friendly
            $(srcElement).attr("msax-isValid", false);
            $(document).find('.msax-ShippingConfirmEmailError2').show();
            return false;
        }
            $element.attr("msax-isValid", true);
            $(document).find('.msax-ShippingConfirmEmailError1').hide();
            $(document).find('.msax-ShippingConfirmEmailError2').hide();
        CartBase.showError(false);
        return true;
        }
        private validateBillingConfirmEmail(srcElement: Element) {
        var $element = $(srcElement);
        var value: string = $element.val();

        if (Utils.isNullOrEmpty($element.val()) && Utils.isNullOrUndefined(this.tempBillingEmail())) {
            $(srcElement).attr("msax-isValid", false);
            $(document).find('.msax-BillingConfirmEmailError1').show();
            return false;
        }
        else if (Utils.isNullOrUndefined(this.tempBillingEmail()) && !Utils.isNullOrEmpty($element.val())) {
            $(srcElement).attr("msax-isValid", false);
            $(document).find('.msax-BillingConfirmEmailError1').show();
            return false;
        }
        else if (Utils.isNullOrEmpty($element.val()) && Utils.isNullOrEmpty(this.tempBillingEmail())) {
            $(srcElement).attr("msax-isValid", false);
            $(document).find('.msax-BillingConfirmEmailError1').show();
            return false;
        }
        else if ($element.val().toString().toLowerCase() !== this.tempBillingEmail().toString().toLowerCase()) {
            // NC HXM Bug Id - 65194. Error message when e-mail's don't match is not customer friendly
            $(srcElement).attr("msax-isValid", false);
            $(document).find('.msax-BillingConfirmEmailError2').show();
            return false;
        }
        $element.attr("msax-isValid", true);
            CartBase.showError(false);
            $(document).find('.msax-BillingConfirmEmailError1').hide();
            $(document).find('.msax-BillingConfirmEmailError2').hide();
        return true;
        }
         //Event added by Sakalija for bug no 72474
        private validateShippingConfirmEmailTextBox(element, valueAccessor) {
        var srcElement = valueAccessor.target;
        if (Utils.isNullOrEmpty(srcElement.value) && Utils.isNullOrUndefined(this.ShippingAddress.Email)) {
            $("#" + srcElement.id).attr("msax-isValid", false);
            $(document).find('.msax-ShippingConfirmEmailError').show();
            return false;
        }
        else if (Utils.isNullOrUndefined(this.ShippingAddress.Email) && !Utils.isNullOrEmpty(srcElement.value)) {
            $("#" + srcElement.id).attr("msax-isValid", false);
            $(document).find('.msax-ShippingConfirmEmailError').show();
            return false;
        }
        else if (Utils.isNullOrEmpty(srcElement.value) && Utils.isNullOrEmpty(this.ShippingAddress.Email)) {
            $("#" + srcElement.id).attr("msax-isValid", false);
            $(document).find('.msax-ShippingConfirmEmailError').show();
            return false;
        }
        else if (srcElement.value.toString().toLowerCase() !== this.ShippingAddress.Email.toString().toLowerCase()) {
            // NC HXM Bug Id - 65194. Error message when e-mail's don't match is not customer friendly
            $("#" + srcElement.id).attr("msax-isValid", false);
            $(document).find('.msax-ShippingConfirmEmailError1').show();
            return false;
        }
            $("#" + srcElement.id).attr("msax-isValid", true);
            $(document).find('.msax-ShippingConfirmEmailError').hide();
            $(document).find('.msax-ShippingConfirmEmailError1').hide();
        CartBase.showError(false);
        return true;
        }
        //Event added by Sakalija for bug no 72474
        private validateBillingConfirmEmailTextBox(selement, valueAccessor) {
            var srcElement = valueAccessor.target;
            $("#" + srcElement.id).removeClass("msax-RequiredError");
        if (Utils.isNullOrEmpty(srcElement.value) && Utils.isNullOrUndefined(this.tempBillingEmail())) {
            $("#" + srcElement.id).attr("msax-isValid", false);
            $(document).find('.msax-ConfirmBillingEmailTextBoxError').show();
            return false;
        }
        else if (Utils.isNullOrUndefined(this.tempBillingEmail()) && !Utils.isNullOrEmpty(srcElement.value)) {
            $("#" + srcElement.id).attr("msax-isValid", false);
            $(document).find('.msax-ConfirmBillingEmailTextBoxError').show();
            return false;
        }
        else if (Utils.isNullOrEmpty(srcElement.value) && Utils.isNullOrEmpty(this.tempBillingEmail())) {
            $("#" + srcElement.id).attr("msax-isValid", false);
            $(document).find('.msax-ConfirmBillingEmailTextBoxError').show();
            return false;
        }
        else if (srcElement.value.toString().toLowerCase() !== this.tempBillingEmail().toString().toLowerCase()) {
            // NC HXM Bug Id - 65194. Error message when e-mail's don't match is not customer friendly
            $("#" + srcElement.id).attr("msax-isValid", false);
            $(document).find('.msax-ConfirmBillingEmailTextBoxError1').show();
            return false;
        }
        $("#" + srcElement.id).attr("msax-isValid", true);
            CartBase.showError(false);
            $(document).find('.msax-ConfirmBillingEmailTextBoxError').hide();
            $(document).find('.msax-ConfirmBillingEmailTextBoxError1').hide();
        return true;
        }


        private InitateTempAddressDetails() {

        this.tempRecordId = ko.observable<string>('');
        this.tempRecordId.subscribe((newValue) => {
            if (this.ShippingAddress.RecordId != newValue) {
                this.ShippingAddress.RecordId = newValue;
                //this.resetDeliveryMethods();
            }
        }, this);

        this.tempAddressType = ko.observable<string>('');
        this.tempAddressType.subscribe((newValue) => {
            this.ShippingAddress.AddressType = AddressType.Delivery;
        }, this);

        this.tempStreet = ko.observable<string>('');
        this.tempStreet.subscribe((newValue) => {
            if (this.ShippingAddress.Street != newValue) {
                this.ShippingAddress.Street = Utils.Trim(newValue);
                //this.resetDeliveryMethods();
            }
        }, this);

        this.tempStreet2 = ko.observable<string>('');
        this.tempStreet2.subscribe((newValue) => {
            if (this.ShippingAddress.Street2 != newValue) {
                this.ShippingAddress.Street2 = Utils.Trim(newValue);
                //this.resetDeliveryMethods();
            }
        }, this);

        this.tempCity = ko.observable<string>('');
        this.tempCity.subscribe((newValue) => {
            if (this.ShippingAddress.City != newValue) {
                this.ShippingAddress.City = Utils.Trim(newValue);
                //this.resetDeliveryMethods();
            }
        }, this);
        this.tempState = ko.observable<string>('');
        this.tempState.subscribe((newValue) => {
            if (this.ShippingAddress.State != newValue) {
                this.ShippingAddress.State = newValue;
                //this.resetDeliveryMethods();
            }
        }, this);
        this.tempZipCode = ko.observable<string>('');
        this.tempZipCode.subscribe((newValue) => {
            if (this.ShippingAddress.ZipCode != newValue) {
                this.ShippingAddress.ZipCode = Utils.Trim(newValue);
                //this.resetDeliveryMethods();
            }
        }, this);
        this.tempFirstName = ko.observable<string>('');
        this.tempFirstName.subscribe((newValue) => {
            if (!Utils.isNullOrUndefined(newValue))
                newValue = newValue.replace(/[^a-zA-Z ]/gi, '');
            if (this.tempFirstName() != newValue)
                this.tempFirstName(newValue);
            if (this.ShippingAddress.FirstName != newValue) {
                this.ShippingAddress.FirstName = Utils.Trim(newValue);
                //this.resetDeliveryMethods();
            }
        }, this);
        this.tempLastName = ko.observable<string>('');
        this.tempLastName.subscribe((newValue) => {
            if (!Utils.isNullOrUndefined(newValue))
                newValue = newValue.replace(/[^a-zA-Z ]/gi, '');
            if (this.tempLastName() != newValue)
                this.tempLastName(newValue);
            if (this.ShippingAddress.LastName != newValue) {
                this.ShippingAddress.LastName = Utils.Trim(newValue);
                //this.resetDeliveryMethods();
            }
        }, this);

        this.tempCountry = ko.observable<string>('');
        this.tempCountry.subscribe((newValue) => {
            if (this.ShippingAddress.Country != newValue) {
                this.ShippingAddress.Country = newValue;
            }
        }, this);

        this.tempEmail = ko.observable<string>('');
        this.tempEmail.subscribe((newValue) => {
            if (this.ShippingAddress.Email != newValue) {
                this.ShippingAddress.Email = Utils.Trim(newValue);
            }
        }, this);

        this.tempEmail2 = ko.observable<string>('');
        this.tempEmail2.subscribe((newValue) => {
            if (this.ShippingAddress.Email2 != newValue) {
                this.ShippingAddress.Email2 = Utils.Trim(newValue);
            }
        }, this);

        this.tempPhone = ko.observable<string>('');
        this.tempPhone.subscribe((newValue) => {
            if (this.ShippingAddress.Phone != newValue) {
                if (!Utils.isNullOrEmpty(newValue))
                    this.ShippingAddress.Phone = Utils.Trim(newValue);
            }
        }, this);

        this.tempPhone2 = ko.observable<string>('');
        this.tempPhone2.subscribe((newValue) => {
            if (this.ShippingAddress.Phone2 != newValue) {
                if (!Utils.isNullOrUndefined(newValue))
                    this.ShippingAddress.Phone2 = Utils.Trim(newValue);
            }
        }, this);

        this.tempPhone3 = ko.observable<string>('');
        this.tempPhone3.subscribe((newValue) => {
            if (this.ShippingAddress.Phone3 != newValue) {
                if (!Utils.isNullOrUndefined(newValue))
                    this.ShippingAddress.Phone3 = Utils.Trim(newValue);
            }
        }, this);

        //billing  Address

        this.tempBillingFirstName = ko.observable<string>('');
        this.tempBillingFirstName.subscribe((newValue) => {
            if (!Utils.isNullOrUndefined(newValue))
                newValue = newValue.replace(/[^a-zA-Z ]/gi, '');
            if (this.tempBillingFirstName() != newValue)
                this.tempBillingFirstName(newValue);
            if (this.PaymentAddress.FirstName != newValue) {
                this.PaymentAddress.FirstName = Utils.Trim(newValue);
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);

        this.tempBillingLastName = ko.observable<string>('');
        this.tempBillingLastName.subscribe((newValue) => {
            if (!Utils.isNullOrUndefined(newValue))
                newValue = newValue.replace(/[^a-zA-Z ]/gi, '');
            if (this.tempBillingLastName() != newValue)
                this.tempBillingLastName(newValue);
            if (this.PaymentAddress.LastName != newValue) {
                this.PaymentAddress.LastName = Utils.Trim(newValue);
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);

        this.tempBillingStreet = ko.observable<string>('');
        this.tempBillingStreet.subscribe((newValue) => {
            if (this.PaymentAddress.Street != newValue) {
                this.PaymentAddress.Street = Utils.Trim(newValue);
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);

        this.tempBillingStreet2 = ko.observable<string>('');
        this.tempBillingStreet2.subscribe((newValue) => {
            if (this.PaymentAddress.Street2 != newValue) {
                this.PaymentAddress.Street2 = Utils.Trim(newValue);
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);

        this.tempBillingCity = ko.observable<string>('');
        this.tempBillingCity.subscribe((newValue) => {
            if (this.PaymentAddress.City != newValue) {
                this.PaymentAddress.City = Utils.Trim(newValue);
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);

        this.tempBillingState = ko.observable<string>('');
        this.tempBillingState.subscribe((newValue) => {
            if (this.PaymentAddress.State != newValue) {
                this.PaymentAddress.State = newValue;
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);

        this.tempBillingZipcode = ko.observable<string>('');
        this.tempBillingZipcode.subscribe((newValue) => {
            if (this.PaymentAddress.ZipCode != newValue) {
                this.PaymentAddress.ZipCode = Utils.Trim(newValue);
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);

        this.tempBillingCountry = ko.observable<string>('');
        this.tempBillingCountry.subscribe((newValue) => {
            if (this.PaymentAddress.Country != newValue) {
                this.PaymentAddress.Country = newValue;
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);

        this.tempBillingEmail = ko.observable<string>('');
        this.tempBillingEmail.subscribe((newValue) => {
            if (this.PaymentAddress.Email != newValue) {
                this.PaymentAddress.Email = Utils.Trim(newValue);
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);

        this.tempBillingPhone = ko.observable<string>('');
        this.tempBillingPhone.subscribe((newValue) => {
            if (this.PaymentAddress.Phone != newValue) {
                if (!Utils.isNullOrEmpty(newValue))
                    this.PaymentAddress.Phone = Utils.Trim(newValue);

                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);

        this.tempBillingRecordId = ko.observable<string>('');
        this.tempBillingRecordId.subscribe((newValue) => {
            if (this.PaymentAddress.RecordId != newValue) {
                this.PaymentAddress.RecordId = newValue;
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);


        this.tempBillingAddressType = ko.observable<string>('');
        this.tempBillingAddressType.subscribe((newValue) => {
            if (this.PaymentAddress.AddressType != newValue) {
                this.PaymentAddress.AddressType = AddressType.Invoice;
                if (this.IsEditAllowed) {
                    this.IsEditAllowed = false;
                    this.isBillingAddressSameAsShippingAddress(false);
                    this.IsEditAllowed = true;
                }
            }
        }, this);


        /*   this.tempPhone1Extension = ko.observable<string>('');
           this.tempPhone1Extension.subscribe((newValue) => {
               if (this.ShippingAddress.tempPhone1Extension != newValue) {
                   this.ShippingAddress.tempPhone1Extension = newValue;
                   this.ShippingAddress.tempPhone1Extension = newValue;
               }
           }, this);   

           this.tempPhone2Extension = ko.observable<string>('');
           this.tempPhone2Extension.subscribe((newValue) => {
               if (this.ShippingAddress.tempPhone2Extension != newValue) {
                   this.ShippingAddress.tempPhone2Extension = newValue;
                   this.ShippingAddress.tempPhone2Extension = newValue;
               }
           }, this);   

           this.tempPhone3Extension = ko.observable<string>('');
           this.tempPhone3Extension.subscribe((newValue) => {
               if (this.ShippingAddress.tempPhone3Extension != newValue) {
                   this.ShippingAddress.tempPhone3Extension = newValue;
                   this.ShippingAddress.tempPhone3Extension = newValue;
               }
           }, this);   */

        this.isBillingAddressSameAsShippingAddress.subscribe((newValue) => {
            if (this.IsEditAllowed) {
                if (newValue) {
                    //this.PaymentAddress(this.ShippingAddress)
                    this.HideAddressFieldErrors();
                    this.hideZipCodeMasterError();
                    this.IsEditAllowed = false;
                    this.tempBillingFirstName(this.tempFirstName());
                    this.tempBillingLastName(this.tempLastName());
                    this.tempBillingStreet(this.tempStreet());
                    this.tempBillingStreet2(this.tempStreet2());
                    this.tempBillingState(this.tempState());
                    this.tempBillingCity(this.tempCity());
                    this.tempBillingCountry(this.tempCountry());
                    this.tempBillingZipcode(this.tempZipCode());
                    this.tempBillingPhone(this.tempPhone());
                    this.tempBillingEmail(this.tempEmail());
                    this.BillingconfirmEmailValue(this.tempEmail());
                    //CS:Bug no 89541 Same as Shipping Red border error - by sakalija Dated 8/11/2015
                    // selecting form to invoke validators
                    var $wrapper = $(".msax-BillingAddressform");

                    // Trigger change to invoke validators.
                    $wrapper.find("input,select").each((index, elem) => {
                        $(elem).change();
                        $(elem).blur();
                    });
                    //CE:Bug no 89541

                }
                else {
                    // this.PaymentAddress=new AddressClass(null);
                    // this.PaymentAddress.Country = "USA";
                    this.tempBillingFirstName(null);
                    this.tempBillingLastName(null);
                    this.tempBillingStreet(null);
                    this.tempBillingStreet2(null);
                    this.tempBillingCity(null);
                    // HXM to fix bug 64792
                    //this.tempBillingState(this._StateInfoClassList()[0].StateId);
                    this.tempBillingState('');
                    this.tempBillingCountry('USA');
                    this.tempBillingZipcode(null);
                    this.tempBillingEmail(null);
                    this.tempBillingPhone(null);
                    this.BillingconfirmEmailValue(null);

                }
            }
            this.IsEditAllowed = true;
            return newValue;
        }, this);

        this.isOptinDeals.subscribe((newValue) => {
            return newValue;
        }, this);

        // HXM C - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey 
        this.isOktoText.subscribe((newValue) => {
            return newValue;
        }, this);

        this.selectedAddress.subscribe((newValue) => {
            if (!Utils.isNullOrUndefined(newValue)) {
                this.tempFirstName(newValue.FirstName);
                this.tempLastName(newValue.LastName);
                this.tempCountry(newValue.Country);
                this.tempStreet(newValue.Street);
                this.tempStreet2(newValue.Street2);
                this.tempCity(newValue.City);
                this.tempState(newValue.State);
                this.tempZipCode(newValue.ZipCode);
                this.tempPhone(Utils.RemovePhoneFormat(newValue.Phone));
                this.tempPhone2(Utils.RemovePhoneFormat(newValue.Phone2));
                this.tempPhone3(Utils.RemovePhoneFormat(newValue.Phone3));

                if (!Utils.isNullOrEmpty(newValue.Phone))
                    this.ShippingAddress.Phone = this.tempPhone();
                if (!Utils.isNullOrEmpty(newValue.Phone2))
                    this.ShippingAddress.Phone2 = this.tempPhone2();
                if (!Utils.isNullOrEmpty(newValue.Phone3))
                    this.ShippingAddress.Phone3 = this.tempPhone3();

                this.tempEmail(newValue.Email);
                this.ShippingconfirmEmailValue(newValue.Email);
                this.tempEmail2(newValue.Email2);
                this.tempAddressType(newValue.AddressType);
                if (newValue.RecordId != null && newValue.RecordId != 0)
                    this.tempRecordId(newValue.RecordId);
                //this.tempPhone(newValue.Phone);
                //this.tempPhone2(newValue.Phone2);
                //this.tempPhone3(newValue.Phone3);
                //this.tempEmail(newValue.Email);
                //this.ShippingconfirmEmailValue(newValue.Email);
                //this.tempEmail2(newValue.Email2);
            }
        }, this);



    }
    }
} 