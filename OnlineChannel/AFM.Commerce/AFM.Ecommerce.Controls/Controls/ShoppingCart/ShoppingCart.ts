﻿/*
NS by muthait dated 12 Sep 2014
*/


/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../Common/Helpers/Core.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../../Resources/Resources.ts" />



module AFM.Ecommerce.Controls {
    "use strict";
    export class Cart {
        private cart;
        private _editRewardCardDialog;
        private isShoppingCartEnabled;
        private isOrderConfirmationenabled;
        private kitCartItemTypeValue;
        private dialogOverlay;
        private supportDiscountCodes;
        private supportLoyaltyReward;
        private displayPromotionBanner;
        private mypageMode = "Confirm";
        private ShoppingCartView;
        private CheckoutDisable;
        private SubmitOrderDisable;
        private removeItemConfirmDialog;
        public LineItems;
        public orderNumber;
        public IsReadOnly;
        public currentCartMode;
        private itemtobeRemove;
        private isLoggedIn;
        private IsDisplayWishList;//N by Hardik on date 08/11/2015 : Bug 95497:WSR - Add "move to wishList" to the cart
        private IsSynchronyCart;
        private IsSynchronyPaymentCart;
        private CheckSynchronyCart; // NS Bug fix by Aniket on 07/01/2015 
        private CheckSynchronyPaymentCart;

        public AFMZipCode;
        public IsPageModeSubscribersCalled;
        private shoppingcartresponseTemp;
        private isConfimMode;//N by Hardik on date 08/11/2015 : Bug 95497:WSR - Add "move to wishList" to the cart

        // NS - AxL - UI changes - winter release
        public ShoppingCartPageMode;
        // NE - AxL - UI changes - winter release

        constructor(element) {
            this.ShoppingCartView = $(document).find('.msax-ShoppingCart');
            this._editRewardCardDialog = CartBase._cartView.find('.msax-EditRewardCard');
            this.CheckoutDisable = $(document).find('.msax-CheckoutButton');
            this.SubmitOrderDisable = $(document).find('.msax-SubmitOrder');
            this.kitCartItemTypeValue = ko.observable<TransactionItemType>(TransactionItemType.Kit);
            this.LineItems = [];
            this.orderNumber = ko.observable<string>("");
            this.supportDiscountCodes = ko.observable<boolean>(Utils.isNullOrUndefined(msaxValues.msax_CartDiscountCodes) ? true : msaxValues.msax_CartDiscountCodes.toLowerCase() == "true");
            this.supportLoyaltyReward = ko.observable<boolean>(Utils.isNullOrUndefined(msaxValues.msax_CartLoyaltyReward) ? true : msaxValues.msax_CartLoyaltyReward.toLowerCase() == "true");
            this.displayPromotionBanner = ko.observable<boolean>(Utils.isNullOrUndefined(msaxValues.msax_CartDisplayPromotionBanner) ? true : msaxValues.msax_CartDisplayPromotionBanner.toLowerCase() == "true");
            this.currentCartMode = false;
            // If displaying promotion banner call GetPromotionBanner service which gets the cart with promotion banners.
            // Otherwise call GetShoppingCart service which gets the cart without promotion banners.
            //if (this.displayPromotionBanner()) {
            //    this.getPromotions();
            //}
            //else {
            //    this.getShoppingCart();
            //}

            this.cart = CartData;


            this.getZipCodeInfo();
            this.AFMZipCode = ko.observable<string>(null);

            // NS by spriya - Bug 65682 : Firefox backspace delete issue
            // Handles the keypress event on the control.
            CartBase._cartView.keypress(function (event) {
                if (event.keyCode == 13 || event.keyCode == 27) {
                    event.preventDefault();
                    return false;
                }
                if (event.keyCode == 8 && !Utils.isNullOrUndefined(event.target.tagName) && event.target.tagName.toLowerCase() == "select") {
                    event.preventDefault();
                    return false;
                }
                return true;
            });
            // NE by spriya - Bug 65682 

            // Computed observables.
            this.isShoppingCartEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().Items);
            });
            //CS Venkata - Bug 74020 dated 18Mar 2015
            this.isLoggedIn = ko.computed(() => {
                if (msaxValues.msax_LoginMode == "True")
                    return true;
                else
                    return false;
                //CE Venkata - Bug 74020
            });
            //NS by Hardik on date 08/11/2015 : Bug 95497:WSR - Add "move to wishList" to the cart
            this.isConfimMode = ko.observable<boolean>(false);
            this.IsDisplayWishList = ko.computed(() => {
                if (this.isConfimMode()) {
                    if (msaxValues.msax_LoginMode == "True")
                        return true;
                    else
                        return false;
                }
                else {
                    return true;
                }                
            });
            //NE by Hardik on date 08/11/2015 : Bug 95497:WSR - Add "move to wishList" to the cart
            //CS By spriya - synchrony online
            this.CheckSynchronyCart = ko.observable<boolean>(false); // NS bug fix by Aniket on 07/01/2015
            this.CheckSynchronyPaymentCart = ko.observable<boolean>(false);
            //this.IsSynchronyCart = ko.computed(() => {
            //    if (this.isConfimMode()) {
            //        if (msaxValues.msax_MinimumPurchaseSynchronyCart == "True") {
            //            return true;
            //        }
            //        else {
            //            return false;
            //        }
            //    }
            //    return false;
            //});
            this.IsSynchronyCart = ko.computed(() => {
                if (this.isConfimMode()) {
                    if (msaxValues.msax_MinimumPurchaseSynchronyCart == true) {
                        this.CheckSynchronyCart(true);
                    }
                    else {
                        this.CheckSynchronyCart(false);
                    }
                }
                return this.CheckSynchronyCart();
            });
            this.IsSynchronyPaymentCart = ko.computed(() => {
                if (this.isConfimMode()) {
                    if (msaxValues.msax_Synchronycart == true) {
                        this.CheckSynchronyPaymentCart(true);
                    }
                    else {
                        this.CheckSynchronyPaymentCart(false);
                    }
                }
                return this.CheckSynchronyPaymentCart();
            });
            //CE by spriya - Synchrony Online
            this.IsReadOnly = ko.computed(() => {
                if (msaxValues.msax_ShoppingCartReadMode == "True")
                    return true;
                else
                    return false;
            });

            this.isOrderConfirmationenabled = ko.computed(() => {
                if (msaxValues.msax_PageMode == PageModeOptions.ThankYou)
                    return true;
                else
                    return false;
            });

            this.removeItemConfirmDialog = $(element).find(".msax-RemoveItemConfrimPopup");
            LoadingOverlay.CreateRemoveItemDialog(this.removeItemConfirmDialog, this, this.RemoveItemFromCart, this, this.RemoveItemCancel, Resources.String_1154, Resources.String_123);

            // Subscribing to the UpdateShoppingCart event.
            ShoppingCartService.OnUpdateShoppingCart(this, this.UpdateShoppingCart);
            ShoppingCartService.OnUpdateCheckoutCart(this, this.UpdateCheckoutCart);
            CartBase.OnClearPageMode(this, this.ClearPageMode);
            CartBase.OnPageModeSubscribers(this, this.PageModeSubscribers);
            this.IsPageModeSubscribersCalled = false;

            // NS - AxL - UI changes - winter release
            this.ShoppingCartPageMode = msaxValues.msax_PageMode == PageModeOptions.CartView ? true : false;
            // NE - AxL - UI changes - winter release
        }

        private PageModeSubscribers(event, data) {
            if (!this.IsPageModeSubscribersCalled) {
                CartBase.OnUpdateFocusPageMode(PageModeOptions.CartView, this, this.CartViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ThankYou, this, this.ThankYouPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmView, this, this.ConfirmViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmViewCompleted, this, this.ConfirmViewCompletedPageMode); this.IsPageModeSubscribersCalled = true;
            }
        }

        private ClearPageMode(event, data) {
            CartBase.showError(false);
            this.ShoppingCartView.hide();
        }

        private CartViewPageMode(event, data) {
            this.currentCartMode = false;
            CartBase.showError(false);
            this.cart = CartData;
            this.ShoppingCartView.show();
            //this.shoppingcartresponseTemp = CartData;
            this.isConfimMode(false);//N by Hardik on date 08/11/2015 : Bug 95497:WSR - Add "move to wishList" to the cart
            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
        }

        private ThankYouPageMode(event, data) {
            if (!Utils.isNullOrUndefined(this.cart())) {
                CartBase.showError(false);
                this.cart = CartData;
                this.isOrderConfirmationenabled = true;

                var cart = this.cart();

                //AFMExtendedService.GetUserCartInfo().done((dt) => {
                //    if (!Utils.isNullOrUndefined(dt) && dt.LineItems != undefined) {
                //        if (dt.LineItems.length > 0) {
                //            this.LineItems = dt.LineItems;

                //            for (var itemVal in this.LineItems) {
                //                var itemData = this.LineItems[itemVal];
                //                var lineOrdernumber = itemData["lineOrderNumber"];
                //                for (var i = 0; i < cart.Items.length; i++) {

                //                    if (!Utils.isNullOrUndefined(cart.Items[i].AFMKitSequenceNumber) && !Utils.isNullOrUndefined(cart.Items[i].KitComponents)) {
                //                        for (var j = 0; j < cart.Items[i].KitComponents.length; j++) {
                //                            if (cart.Items[i].KitComponents[j].ItemId === itemData["cartItemId"]) {

                //                                if (!Utils.isNullOrEmpty(lineOrdernumber))
                //                                    cart.Items[i].KitComponents[j].LineOrderNumber = lineOrdernumber.substr(0, 4) + " " + lineOrdernumber.substr(4, 8) + " " + lineOrdernumber.substr(12);
                //                            }
                //                        }
                //                    }

                //                    if (cart.Items[i].ItemId === itemData["cartItemId"]) {
                //                        if (!Utils.isNullOrEmpty(lineOrdernumber))
                //                            cart.Items[i].LineOrderNumber = lineOrdernumber.substr(0, 4) + " " + lineOrdernumber.substr(4, 8) + " " + lineOrdernumber.substr(12);
                //                    }

                //                }

                //            }

                //            this.cart(cart);

                //        }
                //    }
                //    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                //    AFMExtendedService.ClearAllCookies();
                //    CartBase.CreateDDO();
                //});
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                //AFMExtendedService.ClearAllCookies();
                this.ShoppingCartView.show();
            }
        }

        private ConfirmViewPageMode(event, data) {
            this.currentCartMode = true;
            CartBase.showError(false);            
            this.cart = CartData;
            this.isConfimMode(true);//N by Hardik on date 08/11/2015 : Bug 95497:WSR - Add "move to wishList" to the cart
            //this.ShoppingCartView.show();
        }

        private ConfirmViewCompletedPageMode(event, data) {
            this.currentCartMode = true;
            //CartBase.showError(false); //Review confirm message and ATP Call Checkin Review Confirm Message
            this.cart = CartData;
            this.isConfimMode(true);//N by Hardik on date 08/11/2015 : Bug 95497:WSR - Add "move to wishList" to the cart
            //this.ShoppingCartView.show();
        }

        private addToWishList(item: TransactionItem) {
            $(document).trigger('AddtoWishlist', item.Listing);
        }

        private getResx(key: string) {
            // Gets the resource value.
            return Resources[key];
        }

        private quantityMinusClick(item: TransactionItem) {
            // Handles quantity minus click.
            if (item.Quantity == 1) {
                this.removeFromCartClick(item);
            } else {
                item.Quantity = item.Quantity - 1;
                this.updateQuantity([item]);
            }
        }

        private quantityPlusClick(item: TransactionItem) {
            // Handles quantity plus click.
            item.Quantity = item.Quantity + 1;
            this.updateQuantity([item]);
        }

        private quantityTextBoxChanged(item: TransactionItem, valueAccesor) {
            // Handles quantity text box change event.
            var srcElement = valueAccesor.target;
            if (!Utils.isNullOrUndefined(srcElement) &&  srcElement.value != item.Quantity) {
                var param = new RegExp("^(0?[0-9]{1,3}|[0-4][0-9][0-9][0-9]|5000)$");
                if (param.test(srcElement.value)) {
                    item.Quantity = srcElement.value;
                    CartBase.showError(false);
                }
                else {
                    this.DisableSubmitOrderProcessdButton();
                    errorMessage = Resources.String_1148;
                    CartBase.showError(true);
                }
                if (item.Quantity <= 0 || item.Quantity > 5000) {
                    //item.Quantity = 1;
                    errorMessage = Resources.String_1148;
                    this.DisableSubmitOrderProcessdButton();
                    CartBase.showError(true);
                    return;
                }
            }

            //if (!Utils.isNullOrUndefined(this.shoppingcartresponseTemp.Errors) || this.shoppingcartresponseTemp.Errors.length > 0) {
            //        errorMessage = this.shoppingcartresponseTemp.Errors[0].ErrorMessage;
            //        CartBase.showError(true);
            //        $(document).find('.msax-SubmitOrder').hide();
            //    }
            //    else {
            //        //this.EnableSubmitOrderProcessdButton();
            //        if (!this.checkItemQuantity()) {
            //            return;
            //        }
            //    }

        }
        private checkItemQuantity(): boolean {
            var cart = this.cart();
            var flag = true;
            for (var i = 0; i < cart.CartCount; i++) {
                if (cart.Items[i].Quantity <= 0 || cart.Items[i].Quantity > 5000) {
                    errorMessage = Resources.String_1148;
                    this.DisableSubmitOrderProcessdButton();
                    CartBase.showError(true);
                    flag = false;
                    break;
                }
            }
            if (flag) {
                this.EnableSubmitOrderProcessdButton();
            }
            return flag;

        }

        // NS CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
        private quantityDropDownChanged(item: TransactionItem, valueAccesor) {
            var srcElement = valueAccesor.target;
            if (!Utils.isNullOrUndefined(srcElement) && srcElement.value != item.Quantity) {
                var param = new RegExp("^(0?[0-9]{1,3}|[0-4][0-9][0-9][0-9]|5000)$");
                if (param.test(srcElement.value)) {
                    item.Quantity = srcElement.value;
                    CartBase.showError(false);
                }
                else {
                    this.DisableSubmitOrderProcessdButton();
                    errorMessage = Resources.String_1148;
                    CartBase.showError(true);
                }
                if (item.Quantity <= 0 || item.Quantity > 99) {
                    //item.Quantity = 1;
                    errorMessage = Resources.String_1148;
                    CartBase.showError(true);
                    this.DisableSubmitOrderProcessdButton();
                }
            }
            //if (srcElement.value == item.Quantity) {
            //    if (!Utils.isNullOrUndefined(this.shoppingcartresponseTemp.Errors)) {
            //        errorMessage = this.shoppingcartresponseTemp.Errors[0].ErrorMessage;
            //        CartBase.showError(true);
            //        $(document).find('.msax-SubmitOrder').hide();
            //    }
            //    else {
            //    if (!this.checkItemQuantity()) {
            //        return;
            //    }
            //    }
            //}
        }


        // NE CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box

        //NS by muthait dated 16Sep2014
        private updateQuantityonClick(item: TransactionItem) {
            if (!(item.AFMMultipleQty > 1 || item.AFMLowestQty > 1)) {
                var qtyTestBox = $(document).find('.' + item.LineId);
                var param = new RegExp("^(0?[0-9]{1,3}|[0-4][0-9][0-9][0-9]|5000)$");
                if (!param.test(qtyTestBox.val())) {
                    errorMessage = Resources.String_1148;
                    CartBase.showError(true);
                    return;
                }
                else {
                    item.Quantity = qtyTestBox.val();
                }
            }
            //if (item.Quantity <= 0 || item.Quantity > 5000) {
            //    //item.Quantity = 1;
            //    errorMessage = Resources.String_1148;
            //    CartBase.showError(true);
            //    return;
            //}
            if (!this.checkItemQuantity()) {
                return;
            }

            if (item.Quantity == 0) {
                this.removeFromCartClick(item);
            } else {
                //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
                if (item.ItemType == TransactionItemType.Kit) {
                    for (var index in item.KitComponents) {
                        var originalKitQuantity = item.KitComponents[index].AFMKitItemQuantity;
                        item.KitComponents[index].Quantity = (item.KitComponents[index].Quantity / originalKitQuantity) * item.Quantity;
                        item.KitComponents[index].AFMKitItemQuantity = item.Quantity;
                    }
                    this.updateQuantity(item.KitComponents);
                }
                else {
                    this.updateQuantity([item]);
                }
                //NE - RxL
            }
        }
        //NE by muthait dated 16Sep2014


        private editRewardCardOverlayClick() {
            this.dialogOverlay = $('.ui-widget-overlay');
            this.dialogOverlay.on('click', $.proxy(this.closeEditRewardCardDialog, this));
        }

        private createEditRewardCardDialog() {
            // Creates the edit reward card dialog box.
            this._editRewardCardDialog.dialog({
                modal: true,
                autoOpen: false,
                draggable: true,
                resizable: false,
                position: ['top', 100],
                show: { effect: "fadeIn", duration: 500 },
                hide: { effect: "fadeOut", duration: 500 },
                width: 500
            });

            $('.ui-dialog').addClass('msax-Control');
        }

        private showEditRewardCardDialog() {
            // Displays the edit reward card dialog.
            this._editRewardCardDialog.dialog('open');
            this.editRewardCardOverlayClick();
        }

        private closeEditRewardCardDialog() {
            // Close the dialog.
            this._editRewardCardDialog.dialog('close');
        }

        private UpdateShoppingCart(event, data) {
            this.UpdateShoppingCartData(data);
        }

        private UpdateCheckoutCart(event, data) {
            this.UpdateShoppingCartData(data);
        }
        private UpdateShoppingCartData(data) {
            // this.shoppingcartresponseTemp = data;
            // Hanldes the UpdateShoppingCart event.
            if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                errorMessage = data.Errors[0].ErrorMessage;
                CartBase.showError(true);
                $(document).find('.msax-SubmitOrder').hide();
                if (data.ShoppingCart != null) {
                    this.cart(data.ShoppingCart);
                }
            }
            else {
                $(document).find('.msax-SubmitOrder').show();
                this.cart(data.ShoppingCart);
                if (msaxValues.msax_PageMode == PageModeOptions.ThankYou) {
                    if (!Utils.isNullOrUndefined(data.ShoppingCart.SalesOrderNumber))
                        CartBase.CreateDDO(data.ShoppingCart.SalesOrderNumber);
                }
                //CS by spriya - Synchrony Online
                if (msaxValues.msax_MinimumPurchaseSynchronyCart == true) {
                    errorMessage = Resources.String_1405;
                    CartBase.showInfo(true);
                    //this.IsSynchronyCart(true);
                    this.CheckSynchronyCart(true); // NS Bug fix by Aniket on 07/01/2015
                }
                else {
                    //this.IsSynchronyCart(false);
                    this.CheckSynchronyCart(false); // NS Bug fix by Aniket on 07/01/2015
                    CartBase.showInfo(false);
                }

                if (msaxValues.msax_Synchronycart == true) {
                    this.CheckSynchronyPaymentCart(true); 
                }
                else {
                    this.CheckSynchronyPaymentCart(false);
                }
                //CE by spriya - Synchrony Online
                CartBase.showError(false);
            }
        }

        private DisableSubmitOrderProcessdButton() {
            this.SubmitOrderDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
            this.SubmitOrderDisable.addClass("disableButton");
            this.CheckoutDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
            this.CheckoutDisable.addClass("disableButton");
        }

        private EnableSubmitOrderProcessdButton() {
            this.SubmitOrderDisable.removeAttr('disabled').css('mouse', 'cursor');
            this.SubmitOrderDisable.removeClass("disableButton");
            this.CheckoutDisable.removeAttr('disabled').css('mouse', 'cursor');
            this.CheckoutDisable.removeClass("disableButton");
        }
        // Service calls

        private getShoppingCart() {
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText);

            ShoppingCartService.GetShoppingCart(ShoppingCartDataLevel.All, this.currentCartMode)
                .done((data) => {

                    this.createEditRewardCardDialog();
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                })
                .fail((errors) => {
                    errorMessage = Resources.String_63; // Sorry, something went wrong. The shopping cart information couldn't be retrieved. Please refresh the page and try again.
                    CartBase.showError(true);
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                });
        }

        private getPromotions() {
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText);

            ShoppingCartService.GetPromotions(this.currentCartMode, ShoppingCartDataLevel.All)
                .done((data) => {
                    this.createEditRewardCardDialog();
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                })
                .fail((errors) => {
                    errorMessage = Resources.String_177; // Sorry, something went wrong. The cart promotions information couldn't be retrieved. Please refresh the page and try again.
                    CartBase.showError(true);
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                });
        }

        private removeFromCartClick(item) {
            //this.itemtobeRemove = item;
            //LoadingOverlay.openRemoveItemDialog(this.removeItemConfirmDialog, $(document).find(".msax-RemoveItemConfrimText"), Resources.String_1154);
            //this.RemoveItemFromCart();
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...                        //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
            var lineIds = [];
            if (item.ItemType == TransactionItemType.Kit) {
                for (var component in item.KitComponents) {
                    lineIds.push(item.KitComponents[component].LineId);
                }
                ShoppingCartService.RemoveKitFromCart(this.currentCartMode, lineIds, ShoppingCartDataLevel.All)
                    .done((data) => {
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

                        if (this.displayPromotionBanner()) {
                            this.getPromotions();
                        }
                        if (!Utils.isNullOrUndefined(data.ShoppingCart) && data.ShoppingCart.TotalAmount < SynchronyMinPurchaseAmount() && this.IsSynchronyCart() ==true) {
                            errorMessage = Resources.String_1406;
                            CartBase.showError(true);
                            $(document).find('.msax-SubmitOrder').hide();
                            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        }
                    })
                    .fail((errors) => {
                        errorMessage = Resources.String_64;
                        CartBase.showError(true);
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    });
            }
            else {
                //NE - RxL
                ShoppingCartService.RemoveFromCart(this.currentCartMode, item.LineId, ShoppingCartDataLevel.All)
                    .done((data) => {
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

                        if (this.displayPromotionBanner()) {
                            this.getPromotions();
                        }
                        if (!Utils.isNullOrUndefined(data.ShoppingCart) && data.ShoppingCart.TotalAmount < SynchronyMinPurchaseAmount() && this.IsSynchronyCart() == true) {
                            errorMessage = Resources.String_1406;
                            CartBase.showError(true);
                            $(document).find('.msax-SubmitOrder').hide();
                            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        }
                    })
                    .fail((errors) => {
                        errorMessage = Resources.String_64;
                        CartBase.showError(true);
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    });
            }
            this.itemtobeRemove = null;
        }
        private RemoveItemCancel() {
            this.itemtobeRemove = null;
            LoadingOverlay.closeRemoveItemDialog(this.removeItemConfirmDialog);
        }

        private RemoveItemFromCart() {
            LoadingOverlay.closeRemoveItemDialog(this.removeItemConfirmDialog);
            var item = this.itemtobeRemove;
            if (Utils.isNullOrUndefined(item))
                return;
            //LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...                        //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
            //var lineIds = [];
            //if (item.ItemType == TransactionItemType.Kit) {
            //    for (var component in item.KitComponents) {
            //        lineIds.push(item.KitComponents[component].LineId);
            //    }
            //    ShoppingCartService.RemoveKitFromCart(this.currentCartMode, lineIds, ShoppingCartDataLevel.All)
            //        .done((data) => {
            //            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

            //            if (this.displayPromotionBanner()) {
            //                this.getPromotions();
            //            }
            //        })
            //        .fail((errors) => {
            //            errorMessage = Resources.String_64;
            //            CartBase.showError(true);
            //            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            //        });
            //}
            //else {
            //    //NE - RxL
            //    ShoppingCartService.RemoveFromCart(this.currentCartMode, item.LineId, ShoppingCartDataLevel.All)
            //        .done((data) => {
            //            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

            //            if (this.displayPromotionBanner()) {
            //                this.getPromotions();
            //            }
            //        })
            //        .fail((errors) => {
            //            errorMessage = Resources.String_64;
            //            CartBase.showError(true);
            //            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            //        });
            //}
            //this.itemtobeRemove = null;            
        }

        private updateQuantity(items: TransactionItem[]) {
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...
            this.EnableSubmitOrderProcessdButton();
            ShoppingCartService.UpdateQuantity(this.currentCartMode, items, ShoppingCartDataLevel.All)
                .done((data) => {
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

                    if (this.displayPromotionBanner()) {
                        this.getPromotions();
                    }
                })
                .fail((errors) => {
                    errorMessage = Resources.String_65;
                    CartBase.showError(true);
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                });
        }


        private updateLoyaltyCardId() {
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...
            var loyaltyCardId = this._editRewardCardDialog.find('#RewardCardTextBox').val();

            if (!Utils.isNullOrWhiteSpace(loyaltyCardId)) {
                LoyaltyService.UpdateLoyaltyCardId(false, loyaltyCardId, ShoppingCartDataLevel.All)
                    .done((data) => {
                        this.closeEditRewardCardDialog();
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

                        if (this.displayPromotionBanner()) {
                            this.getPromotions();
                        }
                    })
                    .fail((errors) => {
                        errorMessage = Resources.String_164; // Sorry, something went wrong. An error occurred while trying to update reward card id in cart. Please refresh the page and try again. 
                        CartBase.showError(true);
                        this.closeEditRewardCardDialog();
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    });
            }
        }

        private updateZipCodeInfo() {
            AFMExtendedService.SetZipCode(this.AFMZipCode(), this.currentCartMode)
                .done((data) => {
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

                    if (this.displayPromotionBanner()) {
                        this.getPromotions();
                    }
                })
                .fail((errors) => {
                    errorMessage = Resources.String_1113;
                    CartBase.showError(true);
                    this.closeEditRewardCardDialog();
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                });
        }

        private getZipCodeInfo() {
            AFMExtendedService.GetZipCode().done((data) => {
                this.AFMZipCode(data.AFMZipCode);
            });;
        }

        private continueShoppingClick() {
            if (!Utils.isNullOrWhiteSpace(msaxValues.msax_ContinueShoppingUrl)) {
                window.location.href = msaxValues.msax_ContinueShoppingUrl;
            }
            else {
            }
        }

        private signInClick() {
            if (!Utils.isNullOrWhiteSpace(msaxValues.msax_SigInUrl)) {
                window.location.href = msaxValues.msax_SigInUrl;
            }
            else {
            }
        }

    }
} 