﻿//NS by muthait dated 11 Sep 2014
using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;

namespace AFM.Ecommerce.Controls
{

    /// <summary>
    /// Checkout control.
    /// </summary>
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    [ToolboxData("<{0}:ShoppingCart runat=server></{0}:ShoppingCart>")]
    [ComVisible(false)]
    public class ShoppingCart : RetailWebControl
    {
        private bool supportDiscountCodes = true;
        private bool supportLoyaltyReward = true;
        private bool cartDisplayPromotionBanner = true;
        private bool isReadOnly = false;
        private string signinurl = string.Empty;


        //NS by muthait dated 26/08/2014
        public string TransactionId { get; set; }
        public string ErrorPageUrl { get; set; }
        public bool LoginMode { get; set; }
        //NE by muthait dated 26/08/2014

        public bool IsReadOnly
        {
            get { return this.isReadOnly; }
            set { this.isReadOnly = value; }
        }

        /// <summary>
        /// Order Summary Constructor
        /// </summary>
        public ShoppingCart()
        { }


        /// <summary>
        /// Gets or sets the value indicating whether cart supports adding or removing discount codes.
        /// </summary>
        public bool SupportDiscountCodes
        {
            get { return this.supportDiscountCodes; }
            set { this.supportDiscountCodes = value; }
        }

        /// <summary>
        /// Gets or sets the value indicating whether cart supports adding loyalty for earning reward points.
        /// </summary>
        public bool SupportLoyaltyReward
        {
            get { return this.supportLoyaltyReward; }
            set { this.supportLoyaltyReward = value; }
        }

        /// <summary>
        /// Gets or sets the value indicating whether cart displays promotion banners.
        /// </summary>
        public bool CartDisplayPromotionBanner
        {
            get { return this.cartDisplayPromotionBanner; }
            set { this.cartDisplayPromotionBanner = value; }
        }

        public string SignInURL {
            get { return this.signinurl; }
            set { this.signinurl = value; }
        }
        /// <summary>
        /// Gets the markup to include control scripts, css, startup scripts in the page.
        /// </summary>
        /// <returns>The header markup.</returns>
        internal string GetHeaderMarkup()
        {
            string output = string.Empty;

            this.GetCssUrls();
            this.GetScriptUrls();

            // Preventing markup getting registered multiple times in a page when using multiple controls.
            string existingHeaderMarkup = string.Empty;
            if (this.Page != null && this.Page.Header != null)
            {
                foreach (Control control in this.Page.Header.Controls)
                {
                    if (control.GetType() == typeof(LiteralControl))
                    {
                        LiteralControl literalControl = (LiteralControl)control;
                        existingHeaderMarkup += literalControl.Text;
                    }
                }
            }

            foreach (string cssUrl in this.cssUrls)
            {
                string cssLink = String.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />", cssUrl);

                // If page header is visible here, check if the markup is present in the header already.
                if (this.Page == null || this.Page.Header == null || !existingHeaderMarkup.Contains(cssLink))
                {
                    output += cssLink;
                }
            }

            foreach (string scriptUrl in this.scriptUrls)
            {
                string scriptLink = String.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", scriptUrl);

                // If page header is visible here, check if the markup is present in the header already.
                if (this.Page == null || this.Page.Header == null || !existingHeaderMarkup.Contains(scriptLink))
                {
                    output += scriptLink;
                }
            }

            output += this.GetStartupMarkup(existingHeaderMarkup);

            return output;
        }

        /// <summary>
        /// Gets the control html markup with the control class wrapper added.
        /// </summary>
        /// <returns>The control markup.</returns>
        internal string GetControlMarkup()
        {
            string htmlContent = this.GetHtml();

            // Adding wrapper with the control cssClass. 
            return "<div class=\"" + cssClass + "\">" + htmlContent + "</div>";
        }

        /// <summary>
        /// Gets the script urls.
        /// </summary>
        /// <returns>The script urls.</returns>
        protected override Collection<string> GetScriptUrls()
        {
            base.GetScriptUrls();
            return this.scriptUrls;
        }

        /// <summary>
        /// Gets the css urls.
        /// </summary>
        /// <returns>The css urls.</returns>
        protected override Collection<string> GetCssUrls()
        {
            base.GetCssUrls();
            this.AddCssUrl("/css/Controls/ShoppingCart.css", false);
            this.AddCssUrl("/css/Controls/PromotionCode.css", false);

            // If rendering mobile view then register ShoppingCart.mobile.css in addition
            if (IsMobileView)
            {
                this.AddCssUrl("/css/Controls/ShoppingCart.mobile.css", false);
            }

            return this.cssUrls;
        }

        /// <summary>
        /// Gets the startup markup.
        /// </summary>
        /// <param name="existingHeaderMarkup">>Existing header markup to determine if startup script registration is required.</param>
        /// <returns>The startup markup.</returns>
        protected override string GetStartupMarkup(string existingHeaderMarkup)
        {
            string startupScript = base.GetStartupMarkup(existingHeaderMarkup);

            string cartStartupScript = string.Format(@"<script type='text/javascript'> 
                msaxValues['msax_CartDiscountCodes'] = '{0}'; 
                msaxValues['msax_CartLoyaltyReward'] = '{1}'; 
                msaxValues['msax_CartDisplayPromotionBanner'] = '{2}';
                msaxValues['msax_TransactionId'] = '{3}';
                msaxValues['msax_ShoppingCartReadMode'] = '{4}';
                msaxValues['msax_SigInUrl'] = '{5}';
                msaxValues['ErrorPageUrl'] = '{6}';
                msaxValues['msax_LoginMode'] = '{7}';
            </script>", SupportDiscountCodes, SupportLoyaltyReward, CartDisplayPromotionBanner, TransactionId, IsReadOnly, SignInURL, ErrorPageUrl, LoginMode);

            // If page header is visible here, check if the markup is present in the header already.
            if (this.Page == null || this.Page.Header == null || (existingHeaderMarkup != null && !existingHeaderMarkup.Contains(cartStartupScript)))
            {
                startupScript += cartStartupScript;
            }

            return startupScript;
        }

        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            string headerMarkup = this.GetHeaderMarkup();
            base.RegisterHeaderMarkup(headerMarkup);
        }

        /// <summary>
        /// Renders the contents of the control to the specified writer. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client. </param>
        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (writer != null)
            {
                string htmlContent = this.GetHtml();
                writer.Write(htmlContent);
            }
        }



        /// <summary>
        /// Gets the control html markup.
        /// </summary>
        /// <returns>The control markup.</returns>
        private string GetHtml()
        {
            // If in mobile mode, render ShoppingCart.mobile.html
            return (IsMobileView) ? this.GetHtmlFragment("Controls.ShoppingCart.ShoppingCart.html") : this.GetHtmlFragment("Controls.ShoppingCart.ShoppingCart.html");
        }
    }
}
