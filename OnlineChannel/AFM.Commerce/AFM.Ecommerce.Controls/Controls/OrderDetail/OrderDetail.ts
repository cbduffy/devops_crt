﻿/*
NS by muthait dated 12 Sep 2014
*/
/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../Common/Helpers/Core.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../../Resources/Resources.ts" />

module AFM.Ecommerce.Controls {

    export var showNavigatingAwayPopUp; // NS Added by AxL for popup change in synchrony FDD on 17/12/2015
    export var navigateAway; // NS Added by AxL for popup change in synchrony FDD on 17/12/2015
    "use strict";
    export class OrderDetail {

        private cart;
        private isShoppingCartEnabled;
        private isEditableEnabled;
        private findParent;
        private AddressView;
        private ShippingAddress;
        private PaymentAddress;
        private paymentCard;
        public tenderLines: TenderDataLine[] = [];
        private cardToken;
        private IsPageModeSubscribersCalled;
        private IsShoppingCartHasError;
        private IsMinimumPurchase;
        

        // NS - AxL - UI changes - winter release
        public orderConfirmPageMode;
        // NE - AxL - UI changes - winter release
        public thankYouPageMode;

        private ShoppingCartView;
        constructor(element) {
            this.AddressView = $(document).find('.msax-EditAddress');
            this.ShoppingCartView = $(document).find('.msax-ShoppingCart');
            this.cart = CartData;
            PaymentCardData = ko.observable<Payment>(null);
            this.paymentCard = PaymentCardData;
            this.cardToken = CardToken;
            this.ShippingAddress = ko.observable<Address>(new AddressClass(null));
            this.PaymentAddress = ko.observable<Address>(new AddressClass(null));
            this.isShoppingCartEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().Items);
            });

            this.findParent = $(element).parent().attr("id");
            var response = this.checkEditMode();


            if (response == "True")
                this.isEditableEnabled = true;
            else
                this.isEditableEnabled = false;

            // Subscribing to the UpdateShoppingCart event.
            ShoppingCartService.OnUpdateShoppingCart(this, this.updateShoppingCart);
            ShoppingCartService.OnUpdateCheckoutCart(this, this.updateShoppingCart);

            //Subscribe to PageMode
            CartBase.OnClearPageMode(this, this.ClearPageMode);
            CartBase.OnPageModeSubscribers(this, this.PageModeSubscribers);

            this.IsPageModeSubscribersCalled = false;
            this.IsShoppingCartHasError = ko.observable<boolean>(false);
            if (ErrorCount != 0)
                this.IsShoppingCartHasError(true);

            // NS - AxL - UI changes - winter release
            this.orderConfirmPageMode = msaxValues.msax_PageMode == PageModeOptions.ConfirmView || msaxValues.msax_PageMode == PageModeOptions.ConfirmViewCompleted ? true : false;
            showNavigatingAwayPopUp = false;
            navigateAway = true;
            // NE - AxL - UI changes - winter release
            this.thankYouPageMode = msaxValues.msax_PageMode == PageModeOptions.ThankYou ? true : false;
            this.IsMinimumPurchase = false;
        }

        private PageModeSubscribers(event, data) {
            if (!this.IsPageModeSubscribersCalled) {
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmView, this, this.ConfirmViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmViewCompleted, this, this.ConfirmViewCompletedPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ThankYou, this, this.ThankYouPageMode);
                this.IsPageModeSubscribersCalled = true;
            }
        }

        private checkEditMode() {
            if (!Utils.isNullOrUndefined(msaxValues.msax_IsEditable)) {
                for (var i = 0; i < msaxValues.msax_IsEditable.length; i++) {
                    if (this.findParent.split('_').indexOf(msaxValues.msax_IsEditable[i].key) > -1)
                        return msaxValues.msax_IsEditable[i].value;
                }
            }
            return false;
        }

        private updateShoppingCart(event, data) {
            // Hanldes the UpdateShoppingCart event.
            if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                this.IsShoppingCartHasError(true);
                errorMessage = data.Errors[0].ErrorMessage;
                CartBase.showError(true);
            }
            else {
                this.cart(data.ShoppingCart);
                CartBase.showError(false);
                if (msaxValues.msax_PageMode == PageModeOptions.ThankYou) {
                    var tempPhoneValue = data.ShoppingCart.BillingAddress.Phone;
                    if (!Utils.isNullOrEmpty(tempPhoneValue))
                        data.ShoppingCart.BillingAddress.Phone = Utils.SetPhoneFormat(tempPhoneValue);
                    tempPhoneValue = data.ShoppingCart.ShippingAddress.Phone;
                    if (!Utils.isNullOrEmpty(tempPhoneValue))
                        data.ShoppingCart.ShippingAddress.Phone = Utils.SetPhoneFormat(tempPhoneValue);
                    tempPhoneValue = data.ShoppingCart.ShippingAddress.Phone2;
                    if (!Utils.isNullOrEmpty(tempPhoneValue))
                        data.ShoppingCart.ShippingAddress.Phone2 = Utils.SetPhoneFormat(tempPhoneValue);
                    tempPhoneValue = data.ShoppingCart.ShippingAddress.Phone3;
                    if (!Utils.isNullOrEmpty(tempPhoneValue))
                        data.ShoppingCart.ShippingAddress.Phone3 = Utils.SetPhoneFormat(tempPhoneValue);
                    this.PaymentAddress(data.ShoppingCart.BillingAddress);
                    this.ShippingAddress(data.ShoppingCart.ShippingAddress);
                    var paymentString = data.ShoppingCart.TenderLines[0].PaymentCard.CardNumber;
                    data.ShoppingCart.TenderLines[0].PaymentCard.Last4Digits = data.ShoppingCart.TenderLines[0].PaymentCard.CardNumber.substr(paymentString.length - 4, paymentString.length);
                    this.paymentCard(data.ShoppingCart.TenderLines[0].PaymentCard);

                    if (!Utils.isNullOrUndefined(this.PaymentAddress().Email) && msaxValues.msax_PageMode == PageModeOptions.ThankYou)
                        $(document).trigger('SendPaymentEmail', this.PaymentAddress().Email);
                }
            }
        }

        private EditShippingClick() {
            IsConfirmedit = true;//N Developed By v-hapat for Bug:79338            
            CartBase.UpdateFocusPageModeValue(PageModeOptions.ShippingViewCompleted);
        }

        private EditBillingDetailClick() {
            IsConfirmedit = true;//N Developed By v-hapat for Bug:79338            
            CartBase.UpdateFocusPageModeValue(PageModeOptions.BillingViewcompleted);
        }

        private EditPaymentTyepClick() {
            var userpagemode = new UserPageMode(null);
            userpagemode.PageMode = PageModeOptions.BillingView;
            msaxValues.msax_LocalPageMode = PageMode.Billing;
            AFMExtendedService.SetUserPageMode(userpagemode).done((data) => {
                CartBase.UpdateFocusPageModeValue(PageModeOptions.PaymentViewCompleted);
            });
        }

        private ClearPageMode(event, data) {
            CartBase.showError(false);
            this.AddressView.hide();
        }

        private ConfirmViewPageMode(event, data) {
            this.getUserCartInfo();
            this.AddressView.show();
            this.ShoppingCartView.show();
        }

        private ConfirmViewCompletedPageMode(event, data) {
            this.AddressView.show();
            this.ShoppingCartView.show();
        }

        private ThankYouPageMode(event, data) {
            //this.getUserCartInfoForThankyouPage();
            this.AddressView.show();
        }

        private getUserCartInfo() {
            AFMExtendedService.GetUserCartInfo()
                .done((data) => {
                    if (!Utils.isNullOrEmpty(data.ShippingAddress.Phone))
                        data.ShippingAddress.Phone = Utils.SetPhoneFormat(data.ShippingAddress.Phone);
                    if (!Utils.isNullOrEmpty(data.ShippingAddress.Phone2))
                        data.ShippingAddress.Phone2 = Utils.SetPhoneFormat(data.ShippingAddress.Phone2);
                    if (!Utils.isNullOrEmpty(data.ShippingAddress.Phone3))
                        data.ShippingAddress.Phone3 = Utils.SetPhoneFormat(data.ShippingAddress.Phone3);
                    if (!Utils.isNullOrEmpty(data.PaymentAddress.Phone))
                        data.PaymentAddress.Phone = Utils.SetPhoneFormat(data.PaymentAddress.Phone);
                    this.ShippingAddress(data.ShippingAddress);
                    this.PaymentAddress(data.PaymentAddress);
                    this.getCardTokenData();
                    //this.LoadTempPaymentData();
                })
                .fail((errors) => {
                });
        }

        private getUserCartInfoForThankyouPage() {
            AFMExtendedService.GetUserCartInfo()
                .done((data) => {
                    this.ShippingAddress(data.ShippingAddress);
                    this.PaymentAddress(data.PaymentAddress);
                    //this.LoadTempPaymentData();
                })
                .fail((errors) => {
                });
        }

        //NS by muthait
        private getCardTokenData() {
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179);
            this.tenderLines = [];
            if (!AFM.Utils.isNullOrEmpty(msaxValues.msax_TransactionId)) {
                AFMExtendedService.GetCardTokenData(msaxValues.msax_TransactionId)
                    .done((data) => {
                        if (Utils.isNullOrEmpty(data.Error)) {
                            var payment = new PaymentClass(null);
                            var minPurchaseAmount = 0;
                            payment.NameOnCard = data.Name;
                            //Below line To be commented - muthait
                            //payment.CardNumber = "411111111111" + data.Last4Digits;
                            payment.Last4Digits = data.Last4Digits;
                            payment.ExpirationYear = parseInt(data.ExpirationYear);
                            payment.ExpirationMonth = parseInt(data.ExpirationMonth);
                            payment.CardType = data.CardType;
                            payment.SynchronyPromoDesc = data.SynchronyPromoDesc;
                            payment.FinancingOptionId = data.FinancingOptionId;
                            payment.MinimumPurchase = data.SynchronyMinPurchase;
                            payment.RemoveCartDiscount = data.RemoveCartDiscount;
                            if (!Utils.isNullOrUndefined(payment.MinimumPurchase) && payment.MinimumPurchase > 0)
                                minPurchaseAmount = payment.MinimumPurchase;
                            //payment.CardType = "Visa";
                            //Below line To be commented - muthait
                            //payment.CCID = "000";
                            payment.PaymentAddress = this.PaymentAddress();
                            this.cardToken(data.CardToken);
                            CardToken = this.cardToken;
                            this.paymentCard(payment);
                            PaymentCardData = this.paymentCard;
                            this.tenderLines = [];
                            var tenderLine = new TenderDataLineClass(null);
                            //Below line To be commented - muthait
                            tenderLine.PaymentCard = new PaymentClass(this.paymentCard());
                            // We need to swap this once PG code is fixed to submit order with TokenizedPaymentcard object
                            var tokenizedPaymentCard = new TokenizedPaymentCardClass(null);
                            tokenizedPaymentCard.CardToken = data.CardToken;
                            tokenizedPaymentCard.CardType = data.CardType;
                            tokenizedPaymentCard.ExpirationMonth = parseInt(data.ExpirationMonth);
                            tokenizedPaymentCard.ExpirationYear = parseInt(data.ExpirationYear);
                            tokenizedPaymentCard.MaskedCardNumber = data.Last4Digits;
                            tokenizedPaymentCard.NameOnCard = data.Name;
                            tokenizedPaymentCard.PaymentAddress = this.PaymentAddress();
                            tokenizedPaymentCard.UniqueCardId = data.UniqueCardId;
                            tokenizedPaymentCard.SynchronyPromoDesc = data.SynchronyPromoDesc;
                            tokenizedPaymentCard.FinancingOptionId = data.FinancingOptionId;
                            tokenizedPaymentCard.MinimumPurchase = data.SynchronyMinPurchase;
                            tokenizedPaymentCard.RemoveCartDiscount = data.RemoveCartDiscount;
                            if (!Utils.isNullOrUndefined(tokenizedPaymentCard.MinimumPurchase) && tokenizedPaymentCard.MinimumPurchase > 0)
                                minPurchaseAmount = tokenizedPaymentCard.MinimumPurchase;
                            //tenderLine.TokenizedPaymentCard = new TokenizedPaymentCardClass(tokenizedPaymentCard); 
                            // NS Added by AxL for pop-up change in synchrony financing FDD on 16/12/2015
                            if (!Utils.isNullOrUndefined(data.FinancingOptionId) && data.FinancingOptionId.trim().length > 0 && data.RemoveCartDiscount == true)
                                showNavigatingAwayPopUp = true;
                            else
                                showNavigatingAwayPopUp = false;
                            // NE Added by AxL for pop-up change in synchrony financing FDD on 16/12/2015
                            tenderLine.Amount = this.cart().TotalAmount;
                            this.tenderLines.push(tenderLine);
                            TenderLinesData = this.tenderLines;
                            CartBase.UpdateFocusPageModeValue(PageModeOptions.ConfirmViewCompleted);
                            if (payment.CardType == Resources.String_1404 || tokenizedPaymentCard.CardType == Resources.String_1404) {
                                msaxValues.msax_Synchronycart = true;
                                if (payment.MinimumPurchase > 0) {
                                    msaxValues.msax_MinimumPurchaseSynchronyCart = true;
                                    errorMessage = Resources.String_1405;
                                    CartBase.showInfo(true);
                                }
                            }
                            else {
                                msaxValues.msax_Synchronycart = false;
                                msaxValues.msax_MinimumPurchaseSynchronyCart = false;
                                CartBase.showInfo(false);
                                CartBase.showError(false);
                            }
                            if (!Utils.isNullOrUndefined(data.FinancingOptionId) && data.FinancingOptionId.trim().length > 0) {
                                this.LoadInitialData();
                                SynchronyMinPurchaseAmount(minPurchaseAmount);
                            }
                            else
                            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                            $(document).trigger('ShowPasswordAndTACInputs', "Success");
                        }
                        else {
                            errorMessage = data.Error;
                            CartBase.showError(true);
                            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                            $(document).trigger('ShowPasswordAndTACInputs', "Failure");
                        }
                    })
                    .fail((errors) => {
                        errorMessage = Resources.String_1113;
                        CartBase.showError(true);
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        $(document).trigger('ShowPasswordAndTACInputs', "Failure");
                    });
            }
        }

        //NS by spriya : DMND0010113 - Synchrony Financing online
        public LoadInitialData() {
            ShoppingCartService.GetShoppingCart(ShoppingCartDataLevel.All, true).done((data) => {
                CartData(data.ShoppingCart);
                if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                    errorMessage = data.Errors[0].ErrorMessage;
                    ErrorCount = data.Errors.length;
                    if (!Utils.isNullOrUndefined(data.ShoppingCart.ATPError) && (msaxValues.msax_PageMode == PageModeOptions.BillingView || msaxValues.msax_PageMode == PageModeOptions.BillingViewcompleted))
                        $(document).find('.msax - ErrorPanel').hide();
                    else
                        CartBase.showError(true);
                    $(document).find('.msax-SubmitOrder').hide();
                }
                else {
                    $(document).find('.msax-SubmitOrder').show();
                    CartBase.showError(false);
                }
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            }).fail((error) => {
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    if (!Utils.isNullOrEmpty(msaxValues.msax_ErrorPageUrl))
                        window.location.href = msaxValues.msax_ErrorPageUrl;
                });
        }
        //NE by spriya : DMND0010113 - Synchrony Financing online
        private LoadTempPaymentData() {
            if (AFM.Utils.isNullOrUndefined(this.paymentCard())) {
                var payment = new PaymentClass(null);
                payment.NameOnCard = "Jane Doe";
                payment.Last4Digits = "4566";
                payment.CardNumber = "4111111111111111";
                payment.ExpirationYear = 1;
                payment.ExpirationMonth = 2016;
                payment.CardType = "Visa";
                payment.CCID = "024";
                this.cardToken(45);
                this.paymentCard(payment);
            }
            if (this.tenderLines.length == 0) {
                this.tenderLines = [];
                var tenderLine = new TenderDataLineClass(null);
                tenderLine.PaymentCard = new PaymentClass(this.paymentCard());
                tenderLine.Amount = this.cart().GrandTotal;
                this.tenderLines.push(tenderLine);
            }
        }

    }
} 