﻿/*
NS by muthait dated 12 Sep 2014
*/
/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../Common/Helpers/Core.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../../Resources/Resources.ts" />

$(document).ready(() => {
    /* event for close the popup */
    $("div.close").hover(
        function () {
            $('span.ecs_tooltip').show();
        },
        function () {
            $('span.ecs_tooltip').hide();
        }
        );
});

module AFM.Ecommerce.Controls {
    "use strict";


    export class PromotionCode {
        public popupStatus;
        private cart;
        public PromotionCode;
        private isPromotionCodesEnabled;
        private isShoppingCartEnabled;
        private DiscountCodes;
        private PromotionCodeTextBox;
        private PromotionCodeView;
        
        private IsPageModeSubscribersCalled;
        constructor(element) {
            this.cart = CartData;
            if (this.cart() != null)
                this.DiscountCodes = this.cart().DiscountCodes;
            this.PromotionCodeTextBox = ko.observable<string>(null);
            this.PromotionCode = ko.observable<string>(null);
            this.PromotionCodeView = $(document).find('.msax-PromotionCode');
            this.isShoppingCartEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().Items);
            });
            this.isPromotionCodesEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().DiscountCodes);
            });
            // Subscribing to the UpdateShoppingCart event.
            ShoppingCartService.OnUpdateShoppingCart(this, this.updateShoppingCart);

            //CartBase.OnClearPageMode(this, this.ClearPageMode);
            CartBase.OnPageModeSubscribers(this, this.PageModeSubscribers);
            this.IsPageModeSubscribersCalled = false;

            this.popupStatus = false;
        }

        private ClearPageMode(event, data) {
            CartBase.showError(false);
           // this.PromotionCodeView.hide();
        }

        private PageModeSubscribers(event, data) {
            if (!this.IsPageModeSubscribersCalled) {
                CartBase.OnUpdateFocusPageMode(PageModeOptions.CartView, this, this.CartViewPageMode);
                this.IsPageModeSubscribersCalled = true;
            }
        }

        private CartViewPageMode(event, data) {
            this.PromotionCodeView.show();
        }


        private updateShoppingCart(event, data) {
            // Hanldes the UpdateShoppingCart event.
            //if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
            //    errorMessage = data.Errors[0].ErrorMessage;
            //    CartBase.showError(true);
            //}
            //else {
            //    this.cart(data.ShoppingCart);
            //    CartBase.showError(false);
            //}

            if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                errorMessage = data.Errors[0].ErrorMessage;
                CartBase.showError(true);
                if (data.ShoppingCart != null) {
                    this.cart(data.ShoppingCart);
                }
            }
            else {
                this.cart(data.ShoppingCart);
                errorPanel.hide();
            }
        }

        // NS by spriya - Bug 65682 : Firefox backspace delete issue
        private preventKeyPress(data, event) {
                if (event.keyCode == 13 /* enter */ || event.keyCode == 27 /* esc */) {
                    event.preventDefault();
                    return false;
                }
            if (event.keyCode == 8 && !Utils.isNullOrUndefined(event.target.tagName) && event.target.tagName.toLowerCase() == "select") {
                    event.preventDefault();
                    return false;
                }
                return true;
        }
        // NE by spriya - Bug 65682 
        private updatePromotionCodeTextBoxChanged(viewModel: PromotionCode, valueAccesor) {

            this.PromotionCode(valueAccesor.target.value);
        }

        private getPromotions() {
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText);
            $(document).find('.msax-PromotionCodeTextBox').removeClass("msax-RequiredError");
            ShoppingCartService.GetPromotions(false, ShoppingCartDataLevel.All)
                .done((data) => {
                    //NS developed by v-hapat on date 12/29/2014
                    if (!Utils.isNullOrEmpty(this.PromotionCode())) {
                        var tempshowPromoValidError = true;
                        for (var i = 0; i < data.ShoppingCart.DiscountCodes.length; i++) {
                            var tempdiscountDiscountcode = data.ShoppingCart.DiscountCodes[i];
                            if (tempdiscountDiscountcode.toLowerCase() == this.PromotionCode().toLowerCase()) {
                                tempshowPromoValidError = false;
                            }
                        }
                        if (tempshowPromoValidError) {
                            errorMessage = Resources.String_1164; //Promotion Code is not applied to any item in cart
                            //CartBase.showError(true);                            
                            this.showErrorPanel(true, errorMessage);
                            $(document).find('.msax-PromotionCodeTextBox').addClass("msax-RequiredError");
                        }
                        else {
                            this.showErrorPanel(false, errorMessage);
                        }
                    }
                    this.PromotionCodeTextBox("");
                    this.PromotionCode("");
                    //NS developed by v-hapat on date 12/29/2014
                    $(document).find('.msax-ProceedtoCartButtons').show();
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                })
                .fail((errors) => {
                    $(document).find('.msax-ProceedtoCartButtons').show();
                    errorMessage = Resources.String_177; // Sorry, something went wrong. The cart promotions information couldn't be retrieved. Please refresh the page and try again.
                    CartBase.showError(true);
                    this.disablePopup();
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                });
        }

        private applyPromotionCode(cart: ShoppingCart, valueAccesor) {
            $(document).find('.msax-PromotionCodeTextBox').removeClass("msax-RequiredError");
            CartBase.showError(false);
            if (!Utils.isNullOrUndefined(this.PromotionCode())) {

                if (!Utils.isNullOrWhiteSpace(this.PromotionCode())) {
                    LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...
                    var promoCode = this.PromotionCode();
                    var discountCodeArryr = [];
                    $.each(this.cart().DiscountCodes, function (index, value) {
                        discountCodeArryr.push(value.toLowerCase())
                    });
                    if ($.inArray(promoCode.toLowerCase(), discountCodeArryr) > -1) {
                        errorMessage = Resources.String_1165;
                        this.showErrorPanel(true, errorMessage);
                        $(document).find('.msax-PromotionCodeTextBox').addClass("msax-RequiredError");
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        return;
                    }
                    AFMExtendedService.ValidatePromocode(this.PromotionCode()).done((data) => {
                        if (data) {
                            ShoppingCartService.AddOrRemovePromotion(false, promoCode, true, ShoppingCartDataLevel.All)
                                .done((data) => {
                                    //this.PromotionCodeTextBox("");
                                    $(document).find('.msax-ProceedtoCartButtons').show();
                                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                                    this.getPromotions();
                                })
                                .fail((errors) => {
                                    errorMessage = Resources.String_93; /* Sorry, something went wrong. The promotion code could not be added successfully. Please refresh the page and try again. */
                                    CartBase.showError(true);
                                    $(document).find('.msax-ProceedtoCartButtons').show();
                                    this.disablePopup();
                                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                                });
                        }
                        else
                        {
                            errorMessage = Resources.String_1302;
                            this.showErrorPanel(true, errorMessage);
                            $(document).find('.msax-PromotionCodeTextBox').addClass("msax-RequiredError");
                            //CartBase.showError(true);
                            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        }
                    }).fail((errors) => {
                        $(document).find('.msax-ProceedtoCartButtons').show();
                        errorMessage = Resources.String_177; // Sorry, something went wrong. The cart promotions information couldn't be retrieved. Please refresh the page and try again.
                        CartBase.showError(true);
                        this.disablePopup();
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        });
                }
                else {
                    errorMessage = Resources.String_97; /* Please enter a promotion code */
                    this.showErrorPanel(true, errorMessage);
                    $(document).find('.msax-PromotionCodeTextBox').addClass("msax-RequiredError");
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                   // $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
            else {
                errorMessage = Resources.String_97;/* Please enter a promotion code*/
                this.showErrorPanel(true, errorMessage);
                $(document).find('.msax-PromotionCodeTextBox').addClass("msax-RequiredError");
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
               // $("html, body").animate({ scrollTop: 0 }, "slow");
            }
        }

        private removePromotionCode(code, valueAccesor) {
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...
            var srcElement = valueAccesor.target;

            if (!Utils.isNullOrUndefined(code)) {

                ShoppingCartService.AddOrRemovePromotion(false, code, false, ShoppingCartDataLevel.All)
                    .done((data) => {
                        //this.PromotionCodeTextBox("");
                        this.PromotionCode("");
                        $(document).find('.msax-ProceedtoCartButtons').show();
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        this.getPromotions();
                        this.showErrorPanel(false, "");
                    })
                    .fail((errors) => {
                        $(document).find('.msax-ProceedtoCartButtons').show();
                        errorMessage = Resources.String_94; /* Sorry, something went wrong. The promotion code could not be removed successfully. Please refresh the page and try again. */
                        CartBase.showError(true);
                        this.disablePopup();
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    });
            }
            else {
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                this.showErrorPanel(false, "");
            }
        }

        private updatePromoCodeButton() {
            if (this.popupStatus == false) {
                var topopupwidth = $("#toPopup1").width() / 2;
                $("#toPopup1").css({ "margin-left": "-" + topopupwidth + "px" });
                $("#toPopup1").fadeIn(500);
                $("#backgroundPopup").css("opacity", "0.7");
                $("#backgroundPopup").fadeIn(100);
                this.popupStatus = true;
                this.showErrorPanel(false, "");
                $(document).find('.msax-PromotionCodeTextBox').removeClass("msax-RequiredError");
            }
        }

        private disablePopup() {
            if (this.popupStatus == true) {
                $("#toPopup1").fadeOut("normal");
                $("#backgroundPopup").fadeOut("normal");
                this.popupStatus = false;
            }
        }

        private showErrorPanel(isError: boolean, error: string) {

            // Shows the error message on the error panel.
            //CS by spriya Dated 10/09/2015 Bug:101893
            if (isError) {
                $(document).find('#lblError1').text(error);
                $(document).find('#lblError1').show();
            }
            else {
                $(document).find('#lblError1').text("");
                $(document).find('#lblError1').hide();
            }
            //CE by spriya Dated 10/09/2015 Bug:101893
        }
    }
} 