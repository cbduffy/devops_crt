﻿ /*
NS by muthait dated 12 Sep 2014
*/
/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../Common/Helpers/Core.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../../Resources/Resources.ts" />

module AFM.Ecommerce.Controls {
    "use strict";


    export class RsaId {
        private cart;
        public rsaId;
        public RSAID;
        private hasRsaId;
        private isShoppingCartEnabled;        
        private RsaIdTextBox;
        private RsaIdView;
        private RsaEditMode;
        private IsPageModeSubscribersCalled;
        constructor(element) {
            this.cart = CartData;
            
            this.RsaIdTextBox = ko.observable<string>(null);
            this.RSAID = ko.observable<string>(null);
            this.RsaEditMode = ko.observable<boolean>(false);
            this.rsaId = ko.observable<string>(null);
            if (this.cart() != null)
                this.rsaId(this.cart().RsaId);
            this.RsaIdView = $(document).find('.msax-RsaId');
            this.isShoppingCartEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().Items);
            });
            this.hasRsaId = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.rsaId()) && !Utils.isNullOrEmpty(this.rsaId());
            });
            // Subscribing to the UpdateShoppingCart event.
            ShoppingCartService.OnUpdateShoppingCart(this, this.updateShoppingCart);

            CartBase.OnClearPageMode(this, this.ClearPageMode);
            CartBase.OnPageModeSubscribers(this, this.PageModeSubscribers);
            this.IsPageModeSubscribersCalled = false;
        }

        private updateShoppingCart(event, data) {
            // Hanldes the UpdateShoppingCart event.
            if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                errorMessage = data.Errors[0].ErrorMessage;
                CartBase.showError(true);
            }
            else {
                this.cart(data.ShoppingCart);
                CartBase.showError(false);
            }
        }

        private ClearPageMode(event, data) {
            CartBase.showError(false);
            this.RsaIdView.hide();
        }

        private PageModeSubscribers(event, data) {
            if (!this.IsPageModeSubscribersCalled) {
                CartBase.OnUpdateFocusPageMode(PageModeOptions.CartView, this, this.CartViewPageMode); this.IsPageModeSubscribersCalled = true;
            }
        }

        private CartViewPageMode(event, data) {
            this.RsaIdView.show();
        }

        // NS by spriya - Bug 65682 : Firefox backspace delete issue
        private preventKeyPress(data, event) {
                if (event.keyCode == 13 /* enter */ || event.keyCode == 27 /* esc */) {
                    event.preventDefault();
                    return false;
                }
            if (event.keyCode == 8 && !Utils.isNullOrUndefined(event.target.tagName) && event.target.tagName.toLowerCase() == "select") {
                    event.preventDefault();
                    return false;
                }
                return true;
        }
        // NE by spriya - Bug 65682 
        private updateRsaIdTextBoxChanged(viewModel: PromotionCode, valueAccesor) {
            this.RSAID(valueAccesor.target.value);
        }

        private applyRsaId(cart: ShoppingCart, valueAccesor) {
            CartBase.showError(false);
            $(document).find('.msax-RsaIdTextBox').removeClass("msax-RequiredError");
            var RsaTextValue = $(document).find('.msax-RsaIdTextBox').val();
            this.RSAID(RsaTextValue);
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...                             
            if (!Utils.isNullOrUndefined(this.RSAID()) && !Utils.isNullOrWhiteSpace(this.RSAID())) {
                if (cart.RsaId.toLowerCase() == RsaTextValue.toLowerCase()) {
                    errorMessage = Resources.String_1313;
                    this.showErrorPanel(true, errorMessage);
                    $(document).find('.msax-RsaIdTextBox').addClass("msax-RequiredError");
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                }else{
                AFMExtendedService.ValidateRsaId(this.RSAID(), false).done((data) => {
                    if (data) {
                        this.rsaId(this.RSAID());
                        this.cart().RsaId = this.rsaId();
                        this.RsaEditMode(false);
                        $(document).find('.msax-ProceedtoCartButtons').show();
                        this.showErrorPanel(false, "");
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

                    }
                    else {
                        errorMessage = Resources.String_1309;
                        this.showErrorPanel(true, errorMessage);
                        $(document).find('.msax-RsaIdTextBox').addClass("msax-RequiredError");
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    }
                }).fail((errors) => {
                        errorMessage = Resources.String_1310;
                        CartBase.showError(true);
                        $(document).find('.msax-ProceedtoCartButtons').show();
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    });
            }}
            else {
                errorMessage = Resources.String_1311;/* Please enter a promotion code*/
                this.showErrorPanel(true, errorMessage);
                $(document).find('.msax-RsaIdTextBox').addClass("msax-RequiredError");
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            }
        }

        private removeRsaId(cart: ShoppingCart, valueAccesor) {
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...
            var srcElement = valueAccesor.target;
            if (cart) {
                AFMExtendedService.RemoveRsaIdFromCart(false)
                    .done((data) => {
                        //this.PromotionCodeTextBox("");
                        this.RSAID("");
                        this.rsaId(null);
                        this.RsaEditMode(false);
                        this.cart().RsaId = "";
                        $(document).find('.msax-ProceedtoCartButtons').show();
                        $(document).find('.msax-ApplyRsaIdBlock').show();
                        $(document).find('.msax-RsaCodeItem').hide();
                        this.showErrorPanel(false, "");
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);

                    })
                    .fail((errors) => {
                        $(document).find('.msax-ProceedtoCartButtons').show();
                        $(document).find('.msax-RsaIdBlock').hide();
                        errorMessage = Resources.String_1310;
                        CartBase.showError(true);
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    });
            }
            else {
                $(document).find('.msax-RsaIdBlock').hide();
                this.showErrorPanel(false, "");
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            }
        }

        private editRsaId(cart: ShoppingCart, valueAccesor) {
            if (cart) {  
                this.rsaId(cart.RsaId);                
                cart.RsaId = this.rsaId();
                this.RsaEditMode(true);
                this.RSAID(this.rsaId);               
            }
            this.showErrorPanel(false, "");
        }

        private showErrorPanel(isError: boolean, error: string) {

            // Shows the error message on the error panel.
            if (isError) {
                $(document).find('.msax-divError2').text(error);
                $(document).find('.msax-divError2').show();
            }
            else {
                $(document).find('.msax-divError2').text("");
                $(document).find('.msax-divError2').hide();
            }
        }
    }
} 