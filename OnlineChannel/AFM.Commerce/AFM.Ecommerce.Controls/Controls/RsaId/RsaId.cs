﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;

namespace AFM.Ecommerce.Controls
{

    /// <summary>
    /// Checkout control.
    /// </summary>
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    [ToolboxData("<{0}:RsaId runat=server></{0}:RsaId>")]
    [ComVisible(false)]
    public class RsaId : RetailWebControl
    {
        public RsaId()
        { }

        /// <summary>
        /// Gets the script urls.
        /// </summary>
        /// <returns>The script urls.</returns>
        protected override Collection<string> GetScriptUrls()
        {
            base.GetScriptUrls();
            return this.scriptUrls;
        }

        /// <summary>
        /// Gets the css urls.
        /// </summary>
        /// <returns>The css urls.</returns>
        protected override Collection<string> GetCssUrls()
        {
            base.GetCssUrls();
            this.AddCssUrl("/css/Controls/RsaId.css", false);
                      
            if (IsMobileView)
            {
                this.AddCssUrl("/css/Controls/RsaId.mobile.css", false);
            }

            return this.cssUrls;
        }
        /// <summary>
        /// Gets the markup to include control scripts, css, startup scripts in the page.
        /// </summary>
        /// <returns>The header markup.</returns>
        internal string GetHeaderMarkup()
        {
            string output = string.Empty;

            this.GetCssUrls();
            this.GetScriptUrls();

            // Preventing markup getting registered multiple times in a page when using multiple controls.
            string existingHeaderMarkup = string.Empty;
            if (this.Page != null && this.Page.Header != null)
            {
                foreach (Control control in this.Page.Header.Controls)
                {
                    if (control.GetType() == typeof(LiteralControl))
                    {
                        LiteralControl literalControl = (LiteralControl)control;
                        existingHeaderMarkup += literalControl.Text;
                    }
                }
            }

            foreach (string cssUrl in this.cssUrls)
            {
                string cssLink = String.Format("<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />", cssUrl);

                // If page header is visible here, check if the markup is present in the header already.
                if (this.Page == null || this.Page.Header == null || !existingHeaderMarkup.Contains(cssLink))
                {
                    output += cssLink;
                }
            }

            foreach (string scriptUrl in this.scriptUrls)
            {
                string scriptLink = String.Format("<script type=\"text/javascript\" src=\"{0}\"></script>", scriptUrl);

                // If page header is visible here, check if the markup is present in the header already.
                if (this.Page == null || this.Page.Header == null || !existingHeaderMarkup.Contains(scriptLink))
                {
                    output += scriptLink;
                }
            }

            output += this.GetStartupMarkup(existingHeaderMarkup);

            return output;
        }


        /// <summary>
        /// Raises the PreRender event.
        /// </summary>
        /// <param name="e">An <see cref="System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            string headerMarkup = this.GetHeaderMarkup();
            base.RegisterHeaderMarkup(headerMarkup);
        }

        /// <summary>
        /// Renders the contents of the control to the specified writer. This method is used primarily by control developers.
        /// </summary>
        /// <param name="writer">A <see cref="System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client. </param>
        protected override void RenderContents(HtmlTextWriter writer)
        {
            if (writer != null)
            {
                // If we are in mobile mode, so that we render RsaId.mobile.html
                string htmlContent = (IsMobileView) ? this.GetHtmlFragment("Controls.RsaId.RsaId.html") : this.GetHtmlFragment("Controls.RsaId.RsaId.html");
               // writer.Write(htmlContent);
            }
        }
    }
}
