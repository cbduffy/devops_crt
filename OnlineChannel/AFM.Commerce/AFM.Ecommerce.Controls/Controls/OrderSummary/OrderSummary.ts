﻿/*
NS by muthait dated 12 Sep 2014
*/
/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../Common/Helpers/Core.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../../Resources/Resources.ts" />

module AFM.Ecommerce.Controls {
    "use strict";
    export class OrderSummary {
        private cart;
        private isShoppingCartEnabled;
        private showTax;
        private isDiscountViewEnabled: boolean;
        private pageElement;
        private estimateShippingLable;
        private estimateHDLabel;
        private estimateTotalLable;
        private OrderSummaryView;
        private IsPageModeSubscribersCalled;
        private IsShoppingCartHasError;

        // NS - AxL - UI changes - winter release
        public ShoppingCartPageMode;
        // NE - AxL - UI changes - winter release

        constructor(element) {
            this.cart = CartData;
            this.pageElement = $(element);
            this.pageElement.show();
            this.OrderSummaryView = $(document).find('.msax-OrderSummary');
            this.isShoppingCartEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().Items);
            });

            this.estimateShippingLable = ko.observable<string>(Resources.String_1106);
            this.estimateHDLabel = ko.observable<string>(Resources.String_1107);
            this.estimateTotalLable = ko.observable<string>(Resources.String_12);

            this.showTax = ko.observable<boolean>(false);
            // HXM fix for 66569
            this.CheckforDiscountDisplay();
            //this.estimateShippingLable = Resources.String_1106;
            //this.estimateHDLabel = Resources.String_1107;
            //this.estimateTotalLable = Resources.String_12;

            // Subscribing to the UpdateShoppingCart event.
            ShoppingCartService.OnUpdateShoppingCart(this, this.updateShoppingCart);
            ShoppingCartService.OnUpdateCheckoutCart(this, this.updateShoppingCart);

            //Subscribe to PageMode
            CartBase.OnClearPageMode(this, this.ClearPageMode);


            CartBase.OnPageModeSubscribers(this, this.PageModeSubscribers);
            this.IsPageModeSubscribersCalled = false;

            this.IsShoppingCartHasError = ko.observable<boolean>(false);
            if (ErrorCount != 0)
                this.IsShoppingCartHasError(true);
            this.LoadTax();

            // NS - AxL - UI changes - winter release
            this.ShoppingCartPageMode = msaxValues.msax_PageMode == PageModeOptions.CartView ? true : false;
            // NE - AxL - UI changes - winter release
        }

        private PageModeSubscribers(event, data) {
            if (!this.IsPageModeSubscribersCalled) {
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmViewCompleted, this, this.ConfirmViewCompletedPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmView, this, this.ConfirmViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.BillingView, this, this.BillingViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.BillingViewcompleted, this, this.BillingViewCompletedPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ShippingView, this, this.ShippingViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ShippingViewCompleted, this, this.ShippingViewCompletedPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ThankYou, this, this.ThankYouPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.CartView, this, this.CartViewPageMode);
                this.IsPageModeSubscribersCalled = true;
            }
        }

        private updateShoppingCart(event, data) {
            // Hanldes the UpdateShoppingCart event.
            if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                this.IsShoppingCartHasError(true);
                errorMessage = data.Errors[0].ErrorMessage;
                CartBase.showError(true);
                if (data.ShoppingCart != null)
                    this.cart(data.ShoppingCart);
            }
            else {
                this.cart(data.ShoppingCart);
                CartBase.showError(false);
                if (msaxValues.msax_PageMode == PageModeOptions.ThankYou) {
                    var orderSummaryData = new AFMOrderSummary(null);
                    if (!Utils.isNullOrUndefined(this.cart())) {
                        orderSummaryData.EstHomeDelivery = this.cart().EstHomeDelivery;
                        orderSummaryData.EstShipping = this.cart().EstShipping;
                        orderSummaryData.SubtotalWithCurrency = this.cart().SubtotalWithCurrency;
                        orderSummaryData.TaxAmountWithCurrency = this.cart().TaxAmountWithCurrency;
                        orderSummaryData.TotalAmountWithCurrency = this.cart().TotalAmountWithCurrency;
                    }
                    CartData.AFMOrderSummaryData = orderSummaryData;
                }
            }
            this.LoadTax();
        }

        private LoadTax() {
            if (!Utils.isNullOrUndefined(this.cart()) && this.cart().TaxAmountWithCurrency == "$0.00") {
                if (window.location.hash.split('#').length > 1
                    && (window.location.hash.split('#')[1].toLowerCase() == PageMode.Billing || window.location.hash.split('#')[1].toLowerCase() == PageMode.Confirm))
                    this.showTax = ko.observable<boolean>(true);
                else
                    this.showTax = ko.observable<boolean>(false);
            }
            else
                this.showTax = ko.observable<boolean>(true);
        }

        private ConfirmViewPageMode(event, data) {
            this.cart = CartData;
            this.estimateShippingLable(Resources.String_1211);
            this.estimateHDLabel(Resources.String_1215);
            this.estimateTotalLable(Resources.String_5);
            this.OrderSummaryView.show();
            //this.isViewTaxEnabled.show();
        }

        private ConfirmViewCompletedPageMode(event, data) {
            this.cart = CartData;
            this.estimateShippingLable(Resources.String_1212);
            this.estimateHDLabel(Resources.String_1216);
            this.estimateTotalLable(Resources.String_5);
            this.OrderSummaryView.show();
            //this.isViewTaxEnabled.show();
        }

        private BillingViewPageMode(event, data) {
            this.cart = CartData;

            var orderSummaryData = new AFMOrderSummary(null);
            if (!Utils.isNullOrUndefined(this.cart())) {
                orderSummaryData.EstHomeDelivery = this.cart().EstHomeDelivery;
                orderSummaryData.EstShipping = this.cart().EstShipping;
                orderSummaryData.SubtotalWithCurrency = this.cart().SubtotalWithCurrency;
                orderSummaryData.TaxAmountWithCurrency = this.cart().TaxAmountWithCurrency;
                orderSummaryData.TotalAmountWithCurrency = this.cart().TotalAmountWithCurrency;
            }
            CartData.AFMOrderSummaryData = orderSummaryData;

            this.estimateShippingLable(Resources.String_1106);
            this.estimateHDLabel(Resources.String_1107);
            this.estimateTotalLable(Resources.String_12);
            this.OrderSummaryView.show();
            //this.isViewTaxEnabled.show();
        }

        private BillingViewCompletedPageMode(event, data) {
            this.cart = CartData;


            var orderSummaryData = new AFMOrderSummary(null);
            if (!Utils.isNullOrUndefined(this.cart())) {
                orderSummaryData.EstHomeDelivery = this.cart().EstHomeDelivery;
                orderSummaryData.EstShipping = this.cart().EstShipping;
                orderSummaryData.SubtotalWithCurrency = this.cart().SubtotalWithCurrency;
                orderSummaryData.TaxAmountWithCurrency = this.cart().TaxAmountWithCurrency;
                orderSummaryData.TotalAmountWithCurrency = this.cart().TotalAmountWithCurrency;
            }
            CartData.AFMOrderSummaryData = orderSummaryData;

            this.estimateShippingLable(Resources.String_1106);
            this.estimateHDLabel(Resources.String_1107);
            this.estimateTotalLable(Resources.String_12);
            this.OrderSummaryView.show();
            //this.isViewTaxEnabled.show();
        }
        private ShippingViewPageMode(event, data) {
            this.cart = CartData;
            this.estimateShippingLable(Resources.String_1106);
            this.estimateHDLabel(Resources.String_1107);
            this.estimateTotalLable(Resources.String_12);
            this.OrderSummaryView.show();
            //this.isViewTaxEnabled.hide();

        }

        private ShippingViewCompletedPageMode(event, data) {
            this.cart = CartData;
            this.estimateShippingLable(Resources.String_1106);
            this.estimateHDLabel(Resources.String_1107);
            this.estimateTotalLable(Resources.String_12);
            this.OrderSummaryView.show();
            //this.isViewTaxEnabled.hide();
        }
        private ThankYouPageMode(event, data) {
            //if (!Utils.isNullOrUndefined(this.cart())) {
            this.cart = CartData;
            var orderSummaryData = new AFMOrderSummary(null);
            orderSummaryData.EstHomeDelivery = this.cart().EstHomeDelivery;
            orderSummaryData.EstShipping = this.cart().EstShipping;
            orderSummaryData.SubtotalWithCurrency = this.cart().SubtotalWithCurrency;
            orderSummaryData.TaxAmountWithCurrency = this.cart().TaxAmountWithCurrency;
            orderSummaryData.TotalAmountWithCurrency = this.cart().TotalAmountWithCurrency;
            CartData.AFMOrderSummaryData = orderSummaryData;
            this.estimateShippingLable(Resources.String_1213);
            this.estimateHDLabel(Resources.String_1217);
            this.estimateTotalLable(Resources.String_5);
            this.OrderSummaryView.show();
            //this.isViewTaxEnabled.show();
            //}
        }
        private CartViewPageMode(event, data) {
            this.cart = CartData;
            this.estimateShippingLable(Resources.String_1106);
            this.estimateHDLabel(Resources.String_1107);
            this.estimateTotalLable(Resources.String_12);
            this.OrderSummaryView.show();
            //this.isViewTaxEnabled.hide();

        }

        private ClearPageMode(event, data) {
            this.cart = CartData;
            this.OrderSummaryView.hide();
        }

        //private CheckforTaxDataDisplay() {
        //    if (msaxValues.msax_ViewTax == "True")
        //        this.isViewTaxEnabled.show();
        //    else
        //        this.isViewTaxEnabled.hide();
        //}

        private CheckforDiscountDisplay() {
            if (msaxValues.msax_ViewDiscount == "True")
                this.isDiscountViewEnabled = true;
            else
                this.isDiscountViewEnabled = false;
        }


    }
} 