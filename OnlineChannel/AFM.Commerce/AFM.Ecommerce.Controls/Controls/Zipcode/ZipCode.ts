﻿/*
NS by muthait dated 12 Sep 2014
*/


/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../Common/Helpers/Core.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../../Resources/Resources.ts" />

$(document).ready(() => {
    /* event for close the popup */
    $("div.close").hover(
        function () {
            $('span.ecs_tooltip').show();
        },
        function () {
            $('span.ecs_tooltip').hide();
        }
        );
});
module AFM.Ecommerce.Controls {
    "use strict";
    export class ZipCode {
        private cart;
        private isShoppingCartEnabled;

        public AFMZipCode;
        public ZipCodeValue;
        public popupStatus;
        public currentCartMode;
        private IsPageModeSubscribersCalled;
        constructor(element) {

            this.cart = CartData;
            this.currentCartMode = false;

            this.getZipCodeInfo();
            this.AFMZipCode = ko.observable<string>('');
            if(!Utils.isNullOrEmpty(AFMZipCodeData()))
                this.AFMZipCode(AFMZipCodeData());
            this.ZipCodeValue = ko.observable<string>('');
            this.popupStatus = false;
            // Handles the keypress event on the control.
           // CS by spriya - Bug 65682 : Firefox backspace delete issue
           CartBase._cartView.keypress(function (event) {
               if (event.keyCode == 13 || event.keyCode == 27) {
                   event.preventDefault();
                   return false;
               }
               if (event.keyCode == 8 && !Utils.isNullOrUndefined(event.target.tagName) && event.target.tagName.toLowerCase() == "select") {
                   event.preventDefault();
                   return false;
               }
               return true;
            });
            // CS by spriya - Bug 65682 
            // Computed observables.
            this.isShoppingCartEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(this.cart()) && Utils.hasElements(this.cart().Items);
            });

            //CS Venkata - Bug 144275 dated 13Mar 2015
            this.ZipCodeValue.subscribe((newValue) => {
                if (!Utils.isNullOrUndefined(newValue))
                    newValue = newValue.replace(/[^0-9 ]/gi, '');
                if (this.ZipCodeValue() != newValue)
                    this.ZipCodeValue(newValue);
            }, this);
            //CE Venkata - Bug 144275

            // Subscribing to the UpdateShoppingCart event.
            ShoppingCartService.OnUpdateShoppingCart(this, this.UpdateShoppingCart);
            ShoppingCartService.OnUpdateCheckoutCart(this, this.UpdateShoppingCart);
            CartBase.OnPageModeSubscribers(this, this.PageModeSubscribers);
            this.IsPageModeSubscribersCalled = false;
        }

        private PageModeSubscribers(event, data) {
            if (!this.IsPageModeSubscribersCalled) {
                CartBase.OnUpdateFocusPageMode(PageModeOptions.CartView, this, this.CartViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ShippingView, this, this.ShippingViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ShippingViewCompleted, this, this.ShippingViewCompletedPageMode);
                this.IsPageModeSubscribersCalled = true;
            }
        }

        private CartViewPageMode(event, data) {
            this.currentCartMode = false;
        }

        private ShippingViewPageMode(event, data) {
            this.currentCartMode = true;
        }

        private ShippingViewCompletedPageMode(event, data) {
            this.currentCartMode = true;
        }
        private UpdateShoppingCart(event, data) {
            // Hanldes the UpdateShoppingCart event.
            if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                errorMessage = data.Errors[0].ErrorMessage;
                CartBase.showError(true);
                if (data.ShoppingCart != null) {
                    this.cart(data.ShoppingCart);
                }
            }
            else {
                this.cart(data.ShoppingCart);
                errorPanel.hide();
            }
        }


        private updateZipCodeInfo() {

            AFMExtendedService.ValidateZipcode(Resources.String_1152, this.ZipCodeValue()).done((data) => {
                if (data) {
                    LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_179); // Updating shopping cart ...
                    if (this.ZipCodeValue() != AFMZipCodeData()) {

                        AFMExtendedService.SetZipCode(this.ZipCodeValue(), this.currentCartMode)
                            .done((data) => {
                                this.AFMZipCode(this.ZipCodeValue());
                                AFMZipCodeData(this.AFMZipCode());
                                $(document).trigger('DeleteOnZipcodeCookieUpdate');                                
                                this.disablePopup();
                                
                                ShoppingCartService.GetShoppingCart(ShoppingCartDataLevel.All, false)
                                    .done((data) => {
                                        this.showErrorPane(false);
                                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                                        this.disablePopup();
                                        if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                                            errorMessage = data.Errors[0].ErrorMessage;
                                            CartBase.showError(true);
                                        }
                                        else {
                                            errorMessage = Resources.String_1173;
                                            CartBase.showInfo(true);
                                        }
                                    })
                                    .fail((errors) => {
                                        errorMessage = Resources.String_63; // Sorry, something went wrong. The shopping cart information couldn't be retrieved. Please refresh the page and try again.
                                        CartBase.showError(true);
                                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                                    });
                            })
                            .fail((errors) => {
                                errorMessage = Resources.String_1113;
                                CartBase.showError(true);
                                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                            });
                    }
                    else {
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        this.disablePopup();
                    }
                }
                else {
                    this.showErrorPane(true);
                    // LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                }
            })
                .fail((errors) => {
                    errorMessage = Resources.String_63; // Sorry, something went wrong. The shopping cart information couldn't be retrieved. Please refresh the page and try again.
                    CartBase.showError(true);
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                });
        }

        private getZipCodeInfo() {
            AFMExtendedService.GetZipCode().done((data) => {
                this.AFMZipCode(data.AFMZipCode);
                AFMZipCodeData(this.AFMZipCode());
                this.ZipCodeValue(this.AFMZipCode());
            });;
        }


        private updateZipCodeButton() {
            if (this.popupStatus == false) {
                this.ZipCodeValue(this.AFMZipCode());
                var topopupwidth = $("#toPopup").width() / 2;
                $("#toPopup").css({ "margin-left": "-" + topopupwidth + "px" });
                $("#toPopup").fadeIn(500);
                $("#backgroundPopup").css("opacity", "0.7");
                $("#backgroundPopup").fadeIn(100);
                this.popupStatus = true;
                this.showErrorPane(false);
            }
        }

        private updateZipCodeonChange() {
            this.updateZipCodeInfo();
            //    $("#topopup").fadeout("normal");
            //    $("#backgroundpopup").fadeout("normal");
            //    this.popupstatus = false;
        }

        private disablePopup() {
            if (this.popupStatus == true) {
                $("#toPopup").fadeOut("normal");
                $("#backgroundPopup").fadeOut("normal");
                this.popupStatus = false;
            }
        }

        private showErrorPane(isError: boolean) {

            // Shows the error message on the error panel.
            if (isError) {
                errorMessage = Resources.String_1150; // Zipcode is not configured in System. Please contact your coustomer Care.
                $(document).find('#lblError').text(errorMessage);
                $(document).find('#divError').removeClass('msax-DisplayNone');
                $(document).find('#divError').addClass("msax-Error");
            }
            else {
                $(document).find('#lblError').text("");
                $(document).find('#divError').addClass('msax-DisplayNone');
                $(document).find('#divError').removeClass("msax-Error");
            }

        }
    }
} 