﻿/*
NS by muthait dated 12 Sep 2014
*/
/// <reference path="../../ExternalScripts//JQuery.d.ts" />
/// <reference path="../../ExternalScripts//KnockoutJS.d.ts" />
/// <reference path="../../Common/Helpers/Core.ts" />
/// <reference path="../../Common/Helpers/EcommerceTypes.ts" />
/// <reference path="../../Resources/Resources.ts" />

module AFM.Ecommerce.Controls {
    "use strict";
    export class ButtonControls {
        private cart;
        private pageElement;
        private isShoppingCartEnabled;
        private ProceedtoCartButtons;
        private SubmitOrderButtons;
        private SubmitOrderDisable;
        private CheckoutDisable;
        private PreviousButton;
        public paymentCard;
        public tenderLines;
        public orderNumber;
        // NC HXM CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
        private headerValues;

        private IsPageModeSubscribersCalled;

        // NS - AxL - UI changes - winter release
        public orderReviewPageMode;
        // NE - AxL - UI changes - winter release

        constructor(element) {
            this.cart = CartData;
            this.tenderLines = TenderLinesData;
            this.paymentCard = PaymentCardData;
            this.pageElement = $(element);
            this.ProceedtoCartButtons = $(document).find('.msax-ProceedtoCartButtons');
            this.SubmitOrderButtons = $(document).find('.msax-SubmitOrderButtons');
            this.SubmitOrderDisable = $(document).find('.msax-SubmitOrder');
            this.CheckoutDisable = $(document).find('.msax-CheckoutButton');
            this.PreviousButton = $(document).find('.msax-Previous');
            // NC HXM CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
            this.headerValues = ko.observable<AFMSalesTransHeader>(null);
            this.isShoppingCartEnabled = ko.computed(() => {
                return !Utils.isNullOrUndefined(CartData()) && Utils.hasElements(CartData().Items);
            });

            // Subscribing to the UpdateShoppingCart event.
            ShoppingCartService.OnUpdateShoppingCart(this, this.UpdateShoppingCart);
            ShoppingCartService.OnUpdateCheckoutCart(this, this.UpdateCheckoutCart);

            //Subscribe to PageMode
            CartBase.OnClearPageMode(this, this.ClearPageMode);
            CartBase.OnPageModeSubscribers(this, this.PageModeSubscribers);
            this.IsPageModeSubscribersCalled = false;

            // NS - AxL - UI changes - winter release
            this.orderReviewPageMode = msaxValues.msax_PageMode == PageModeOptions.ConfirmView || msaxValues.msax_PageMode == PageModeOptions.ConfirmViewCompleted ? true : false;
            // NE - AxL - UI changes - winter release
        }

        private PageModeSubscribers(event, data) {
            if (!this.IsPageModeSubscribersCalled) {
                CartBase.OnUpdateFocusPageMode(PageModeOptions.CartView, this, this.CartViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmView, this, this.ConfirmViewPageMode);
                CartBase.OnUpdateFocusPageMode(PageModeOptions.ConfirmViewCompleted, this, this.ConfirmViewCompletedPageMode);
                this.IsPageModeSubscribersCalled = true;
            }
        }

        private UpdateShoppingCart(event, data) {
            this.UpdateShoppingCartData(data);
        }

        private UpdateCheckoutCart(event, data) {
            this.UpdateShoppingCartData(data);
        }

        private UpdateShoppingCartData(data) {
            this.cart(data.ShoppingCart);
            if (msaxValues.msax_PageMode == PageModeOptions.CartView) {
                this.ProceedtoCartButtons.show();
                this.CheckoutDisable.css('display', 'inline');
            }
            // Hanldes the UpdateShoppingCart event.
            if (!Utils.isNullOrUndefined(data.Errors) && data.Errors.length > 0) {
                CartBase.showError(true);
                this.SubmitOrderDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
                this.SubmitOrderDisable.addClass("disableButton");
                this.CheckoutDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
                this.CheckoutDisable.addClass("disableButton");
            }
            //CS Developed by spriya - Show Avalara Tax Error Dated 12/26/2014
            else if (CartData() != null && !Utils.isNullOrEmpty(CartData().AvalaraTaxError)) {
                errorMessage = Resources.String_1161;
                CartBase.showError(true);
                this.SubmitOrderDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
                this.SubmitOrderDisable.addClass("disableButton");
                this.CheckoutDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
                this.CheckoutDisable.addClass("disableButton");
            }//CE Developed by spriya
            else {
                this.cart(data.ShoppingCart);
                CartBase.showError(false);
                this.SubmitOrderDisable.removeAttr('disabled').css('mouse', 'cursor');
                this.SubmitOrderDisable.removeClass("disableButton");
                this.CheckoutDisable.removeAttr('disabled').css('mouse', 'cursor');
                this.CheckoutDisable.removeClass("disableButton");
            }
        }

        private ClearPageMode(event, data) {
            this.SubmitOrderButtons.hide();
            this.ProceedtoCartButtons.hide();
        }

        private CartViewPageMode(event, data) {

            this.SubmitOrderButtons.hide();
            this.CheckoutDisable.css('display', 'inline');
            this.ProceedtoCartButtons.show();


        }

        private ConfirmViewPageMode(event, data) {
            this.ProceedtoCartButtons.hide();
            this.SubmitOrderButtons.hide();
        }

        private ConfirmViewCompletedPageMode(event, data) {
            if (!Utils.isNullOrEmpty(msaxValues.msax_TransactionId))
                this.ProceedtoCartButtons.hide();
            //CS Developed by spriya
            if (CartData() != null && !Utils.isNullOrEmpty(CartData().AvalaraTaxError)) {
                errorMessage = Resources.String_1161;
                CartBase.showError(true);
                this.DisableSubmitOrder();
            }else if (CartData() != null && !Utils.isNullOrEmpty(CartData().ATPError)) {
                errorMessage = CartData().ATPError;
                CartBase.showError(true);
                this.DisableSubmitOrder();
            }
            else {
                this.SubmitOrderButtons.show();
            }
            //CE Developed by spriya
        }


        private shoppingCartNextClick(viewModel: ButtonControls, event) {
            this.CheckoutDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
            this.CheckoutDisable.addClass("disableButton");
            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_176);
            if (!Utils.isNullOrWhiteSpace(msaxValues.msax_CheckoutUrl)) {
                var userpagemode = new UserPageMode(null);
                userpagemode.PageMode = PageModeOptions.ShippingViewCompleted;
                msaxValues.msax_LocalPageMode = PageMode.Shipping;
                AFMExtendedService.SetUserPageMode(userpagemode).done((data) => {
                    window.location.href = msaxValues.msax_CheckoutUrl;
                });
            }
            else {
                this.CheckoutDisable.removeAttr('disabled').css('mouse', 'cursor');
                this.CheckoutDisable.removeClass("disableButton");
            }
            // If redirection url is specified redirect to the url on button click.
            //this.commenceCheckout();
        }

        private continueShoppingClick(viewModel: ButtonControls, event) {
            if (!Utils.isNullOrWhiteSpace(msaxValues.msax_ContinueShoppingUrl)) {
                window.location.href = msaxValues.msax_ContinueShoppingUrl;
            }
            else {
            }
        }

        private commenceCheckout() {
            AFMExtendedService.CommenceCheckout(ShoppingCartDataLevel.All)
                .done((data) => {

                    CartBase.showError(false);
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    if (!Utils.isNullOrWhiteSpace(msaxValues.msax_CheckoutUrl)) {
                    }
                })
                .fail((errors) => {

                    CartBase.showError(true);
                    LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                });
        }


        private reviewPreviousClick() {
            IsConfirmedit = true;
            $(document).trigger('ShowPasswordAndTACInputs', "");
            $(document).trigger('ShowNavigatingAwayPopUp', null);
            if (navigateAway) {
                msaxValues.msax_LocalPageMode = PageMode.Billing;
                var userpagemode = new UserPageMode(null);
                userpagemode.PageMode = PageModeOptions.BillingView;
                AFMExtendedService.SetUserPageMode(userpagemode).done((data) => {
                    CartBase.UpdateFocusPageModeValue(PageModeOptions.PaymentView)
            });
                //CartBase.UpdateFocusPageModeValue(PageModeOptions.PaymentView);//NS Developed By v-hapat for Bug:79338
            }
        }

        private submitOrder() {

            this.SubmitOrderButtons.hide();
            this.SubmitOrderDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
            this.SubmitOrderDisable.addClass("disableButton");
            this.PreviousButton.attr('disabled', 'disabled').css('mouse', 'pointer');
            this.PreviousButton.addClass("disableButton");

            LoadingOverlay.ShowLoadingDialog(CartBase._loadingDialog, CartBase._loadingText, Resources.String_1123);
            // NS HXM CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
            if (!Utils.isNullOrUndefined(msaxValues.msax_SurveyTermsAndConditions))
                this.headerValues(msaxValues.msax_SurveyTermsAndConditions);
            else {
                this.headerValues = ko.observable<AFMSalesTransHeader>(null);
                msaxValues.msax_SurveyTermsAndConditions = {
                    'OkToTextUniqueId': '1', 'OkToTextVersionId': '1', 'IsAcceptTermsAndConditions': msaxValues.msax_IsAcceptTermsAndConditions, 'TermsAndConditionsUniqueId': '1', 'TermsAndConditionsVersionId': '1',
                    'IsOptinDealsUniqueId': '1',
                    'IsOptinDealsVersionId': '1'
                };
                this.headerValues(msaxValues.msax_SurveyTermsAndConditions);
            }

            // NS HGL CR - 73994
            msaxValues.msax_IsAcceptTermsAndConditions = true;
            // NS HGL CR - 73994

            if (!Utils.isNullOrUndefined(msaxValues.msax_IsAcceptTermsAndConditions) && msaxValues.msax_IsAcceptTermsAndConditions) {
                CartBase.showError(false);
                AFMExtendedService.SubmitOrder(TenderLinesData, this.headerValues(), PaymentCardData().PaymentAddress.Email, CardToken())
                    .done((data) => {
                        var orderNumber = data.OrderNumber;
                        if (data.Errors.length == 0) {
                            this.orderNumber = data.OrderNumber;

                            if (Utils.isNullOrWhiteSpace(msaxValues.msax_OrderConfirmationUrl)) {
                            }
                            else {
                                AFMExtendedService.ClearAllCookies().done((data) => {
                                    window.location.href = msaxValues.msax_OrderConfirmationUrl += '?confirmationId=' + orderNumber;
                                });
                            }
                        }
                        else {
                            this.EnableSubmitOrder();
                            if (data.Errors[0].ErrorCode == "PaymentError")
                                errorMessage = data.Errors[0].ErrorMessage;//Resources.String_69;
                            else
                                errorMessage = Resources.String_69;
                            CartBase.showError(true);
                            LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                        }
                    })
                    .fail((errors) => {
                        this.EnableSubmitOrder();
                        errorMessage = Resources.String_69;
                        CartBase.showError(true);
                        LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
                    });
            }
            else {
                this.EnableSubmitOrder();
                errorMessage = Resources.String_1149;
                CartBase.showError(true);
                LoadingOverlay.CloseLoadingDialog(CartBase._loadingDialog);
            }
            // NE HXM CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
        }

        private EnableSubmitOrder() {
            this.SubmitOrderDisable.removeAttr('disabled').css('mouse', 'cursor');
            this.SubmitOrderDisable.removeClass("disableButton");
            this.PreviousButton.removeAttr('disabled').css('mouse', 'cursor');
            this.PreviousButton.removeClass("disableButton");
            this.SubmitOrderButtons.show();
        }

        private DisableSubmitOrder() {
            this.SubmitOrderDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
            this.SubmitOrderDisable.addClass("disableButton");
            this.CheckoutDisable.attr('disabled', 'disabled').css('mouse', 'pointer');
            this.CheckoutDisable.addClass("disableButton");
            this.SubmitOrderButtons.show();
        }
    }
} 