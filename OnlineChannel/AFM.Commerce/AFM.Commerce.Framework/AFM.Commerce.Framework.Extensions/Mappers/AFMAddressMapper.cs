﻿using AFM.Commerce.Runtime.Services;


namespace AFM.Commerce.Framework.Extensions.Mappers
{
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using System;
    using System.Text;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;    
    using AFMDataModel = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Map DataModel address to ViewModel address.
    /// </summary>
    public static class AFMAddressMapper
    {
        /// <summary>
        /// Convert the Data Model Address to a View Model Address.
        /// </summary>
        /// <param name="address">The data model address</param>
        /// <returns>Address</returns>
        public static Address ConvertToViewModel(CrtDataModel.Address address)
        {
            if (address == null)
            {
                return null;
            }
            string[] adressStreet = null;
            if (address.Street.Contains(Environment.NewLine))
            {
                string[] splitchar = { Environment.NewLine };
                adressStreet = address.Street.Split(splitchar, StringSplitOptions.None);
            }
            else
            {
                adressStreet = new string[2];
                adressStreet[0]=address.Street;
                adressStreet[1]=string.Empty;
            }

            Address vmAddress = new Address()
            {
                StreetNumber = address.StreetNumber,
                Street = adressStreet[0],
                Street2 = adressStreet[1],
                City = address.City,
                State = address.State,
                County = address.County,
                DistrictName = address.DistrictName,
                ZipCode = address.ZipCode,
                Country = address.ThreeLetterISORegionName,
                Email = address.Email,
                EmailContent = address.EmailContent,
                Phone = address.Phone,
                AttentionTo = address.AttentionTo,
                AddressType = (AddressType)address.AddressTypeValue,
                RecordId = address.RecordId,
                AddressFriendlyName = address.Name,
                IsPrimary = address.IsPrimary,
                Name = address.Name,
                //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                FirstName = Convert.ToString(address[CustomerConstants.AddressFirstName]),
                LastName = Convert.ToString(address[CustomerConstants.AddressLastName]),
                Email2 = Convert.ToString(address[CustomerConstants.Email2]),
                Phone1Extension = Convert.ToString(address[CustomerConstants.Phone1Extension]),
                Phone2 = Convert.ToString(address[CustomerConstants.Phone2]),
                Phone2Extension = Convert.ToString(address[CustomerConstants.Phone2Extension]),
                Phone3 = Convert.ToString(address[CustomerConstants.Phone3]),
                Phone3Extension = Convert.ToString(address[CustomerConstants.Phone3Extension])
                //NE - RxL
            };
            vmAddress.Email = !string.IsNullOrEmpty(vmAddress.Email) ? vmAddress.Email : (!string.IsNullOrEmpty(Convert.ToString(address[CustomerConstants.Email1])) ? Convert.ToString(address[CustomerConstants.Email1]) : string.Empty);
            vmAddress.Phone = !string.IsNullOrEmpty(vmAddress.Phone) ? vmAddress.Phone : (!string.IsNullOrEmpty(Convert.ToString(address[CustomerConstants.Phone])) ? Convert.ToString(address[CustomerConstants.Phone]) : string.Empty);

            return vmAddress;
        }

        /// <summary>
        /// Converts Address view model to CRT Address.
        /// </summary>
        /// <param name="address">The view model address.</param>
        /// <returns>The CRT address.</returns>
        public static CrtDataModel.Address ConvertToDataModel(Address address)
        {
            if (address == null)
            {
                return null;
            }

            return new CrtDataModel.Address()
            {
                StreetNumber = address.StreetNumber,
                Street = address.Street,
                City = address.City,
                State = address.State,
                County = address.County,
                DistrictName = address.DistrictName,
                ZipCode = address.ZipCode,
                ThreeLetterISORegionName = address.Country,
                Email = address.Email,
                EmailContent = address.EmailContent,
                Phone = address.Phone,
                AttentionTo = address.AttentionTo,
                AddressTypeValue = (int)address.AddressType,
                Name = address.Name,
                IsPrimary = address.IsPrimary
            };
        }

        //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
        public static AddressValidationDll.DataModels.Address ConvertToAddressValidationDataModel(AFMDataModel.Address address)
        {
            if (address == null)
            {
                return null;
            }
            AddressValidationDll.DataModels.Address addressValidationAddress = new AddressValidationDll.DataModels.Address() {

                City = address.City,
                Country = CustomerConstants.TwoLetterCountryCode,
                State = address.State,
                StreetLine1 = address.Street,
                StreetLine2 = address.Street2,
                Type = ((CrtDataModel.AddressType)address.AddressType).ToString(),
                Zip = address.ZipCode
            };
            return addressValidationAddress;
        }

        public static CrtDataModel.Address ConvertToCommerceDataModel(AddressValidationDll.DataModels.Address address)
        {
            if(address == null)
            {
                return null;
            }

            CrtDataModel.Address commerceDataModelAddress = new CrtDataModel.Address()
            {
                City = address.City,
                ThreeLetterISORegionName = CustomerConstants.ThreeLetterCountryCode,
                TwoLetterISORegionName = address.Country,
                State = address.State,
                Street = address.StreetLine1,
                AddressType = (CrtDataModel.AddressType)2,
                ZipCode = address.Zip
            };
            return commerceDataModelAddress;
        }

        //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
    }
}
