﻿using CRTModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using System.Collections.ObjectModel;
using Microsoft.Dynamics.Retail.PaymentSDK;
using Microsoft.Dynamics.Retail.PaymentSDK.Constants;
using System.Dynamic;
namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    public partial class OrderMapper
    {
        private const string ListingPropTitle = "Title";
        private const string ListingPropPath = "Path";
        public static readonly string[] UnitOfMeasure = { "Carton", "CN" };

        //NS - RxL - FDD0258-ECS-Review Customer Profile History Wish List V3 Phased-signed
        private static void AFMPopulateCustomSalesItemDetails(CrtDataModel.SalesLine salesLine, TransactionItem salesOrderItem)
        {
            salesOrderItem.AFMKitItemId = salesLine["AFMKITITEMID"] == null ? string.Empty : salesLine["AFMKITITEMID"].ToString();
            salesOrderItem.AFMKitProductId = Convert.ToInt64(salesLine["AFMKITPRODUCTID"]);
            salesOrderItem.AFMKitSequenceNumber = Convert.ToInt32(salesLine["AFMKITSEQUENCENUMBER"]);
            if (!string.IsNullOrEmpty(salesOrderItem.AFMKitItemId))
            {
                string kitProductDetail=salesLine["AFMKITPRODUCTDETAILS"] == null ? string.Empty : salesLine["AFMKITPRODUCTDETAILS"].ToString();
                dynamic productKitDetail = new ExpandoObject();
                productKitDetail = Newtonsoft.Json.JsonConvert.DeserializeObject(kitProductDetail);
                if (productKitDetail.KitComponentDetail != null)
                {
                    salesOrderItem.ProductDetails = Newtonsoft.Json.JsonConvert.SerializeObject(productKitDetail.KitComponentDetail);
                    productKitDetail.KitComponentDetail = null;
                }
                else
                {
                    dynamic productListing = new ExpandoObject();
                    productListing.Name = salesLine.Description;
                    productListing.Description = salesLine.Description;
                    salesOrderItem.ProductDetails = Newtonsoft.Json.JsonConvert.SerializeObject(productListing);
                }
                salesOrderItem.AFMKitItemProductDetails = kitProductDetail;
            }
            else
            {
                salesOrderItem.AFMKitItemProductDetails = string.Empty;
                salesOrderItem.ProductDetails = salesLine["AFMKITPRODUCTDETAILS"] == null ? string.Empty : salesLine["AFMKITPRODUCTDETAILS"].ToString();
            }
            salesOrderItem.AFMKitItemQuantity = Convert.ToInt32(salesLine["AFMKITITEMQUANTITY"]);

            salesOrderItem.AFMVendorId = salesLine["AFMVENDACCOUNT"] == null ? string.Empty : salesLine["AFMVENDACCOUNT"].ToString();
            salesOrderItem.AFMVendorName = salesLine["AFMVENDORNAME"] == null ? string.Empty : salesLine["AFMVENDORNAME"].ToString();
            salesOrderItem.AFMLineConfirmationNumber = salesLine["AFMLINECONFIRMATIONID"] == null ? string.Empty : salesLine["AFMLINECONFIRMATIONID"].ToString();
            salesOrderItem.AFMScheduledDeliveryDate = ConvertToDateTime(salesLine["AFMSCHEDULEDDELIVERYDATE"]);
            salesOrderItem.AFMActualDeliveryDate = ConvertToDateTime(salesLine["AFMACTUALDELIVERYDATE"]);
            salesOrderItem.AFMShippingDate = ConvertToDateTime(salesLine["AFMSHIPPINGDATE"]);
            salesOrderItem.AFMCarrier = salesLine["AFMCARRIER"] == null ? string.Empty : salesLine["AFMCARRIER"].ToString();
            salesOrderItem.AFMTrackingNumber = salesLine["AFMTRACKINGNUMBER"] == null ? string.Empty : salesLine["AFMTRACKINGNUMBER"].ToString();
            salesOrderItem.AFMModelId = salesLine["AFMMODELID"] == null ? string.Empty : salesLine["AFMMODELID"].ToString();
            salesOrderItem.AFMItemType = salesLine["AFMITEMTYPE"] == null ? 0 : Convert.ToInt32(salesLine["AFMITEMTYPE"]);
            salesOrderItem.AFMColor = salesLine["AFMCOLORID"] == null ? string.Empty : salesLine["AFMCOLORID"].ToString();
            salesOrderItem.AFMSize = salesLine["AFMSIZE"] == null ? string.Empty : salesLine["AFMSIZE"].ToString();

            //NS by v-hapat for thankyou page            


            salesOrderItem.LineId = salesLine.LineId;
            if (salesLine.Variant != null)
                salesOrderItem.VariantInventoryDimensionId = salesLine.Variant.InventoryDimensionId;

            salesOrderItem.Comment = salesLine.Comment;


            salesOrderItem.AFMItemType = Convert.ToInt32(salesLine["AFMITEMTYPE"]);
            if (salesOrderItem.AFMItemType == 1)
                salesOrderItem.AFMShippingCost = salesLine.NetAmount;
            salesOrderItem.AFMCartLineATP = new AFM.Commerce.Entities.AFMATPResponseItem()
            {
                ItemId = salesLine.ItemId,
                Message = (salesLine["AFMMESSAGE"] == null ? string.Empty : salesLine["AFMMESSAGE"].ToString()),
                IsAvailable = (Convert.ToInt32(salesLine["AFMISAVAILABLE"]) == 1 ? true : false),
                BestDate = ConvertToDateTime(salesLine["AFMBESTDATE"]),
                EndDateRange = ConvertToDateTime(salesLine["AFMENDDATERANGE"]),
                BeginDateRange = ConvertToDateTime(salesLine["AFMBEGINDATERANGE"]),
                EarliestDeliveryDate = ConvertToDateTime(salesLine["AFMEARLIESTDELIVERYDATE"]),
                LatestDeliveryDate = ConvertToDateTime(salesLine["AFMLATESTDELIVERYDATE"]),

                //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
                MessageMOD = (salesLine["AFMMESSAGEMOD"] == null ? string.Empty : salesLine["AFMMESSAGEMOD"].ToString()),
                BestDateMOD = ConvertToDateTime(salesLine["AFMBESTDATEMOD"]),
                EndDateRangeMOD = ConvertToDateTime(salesLine["AFMENDDATERANGEMOD"]),
                BeginDateRangeMOD = ConvertToDateTime(salesLine["AFMBEGINDATERANGEMOD"]),
                EarliestDeliveryDateMOD = ConvertToDateTime(salesLine["AFMEARLIESTDELIVERYDATEMOD"]),
                LatestDeliveryDateMOD = ConvertToDateTime(salesLine["AFMLATESTDELIVERYDATEMOD"])
                //NE by RxL
            };
            var lineordernumber = salesLine["AFMLINECONFIRMATIONID"] == null ? string.Empty : salesLine["AFMLINECONFIRMATIONID"].ToString();
            salesOrderItem.AFMShippingMode = salesLine["AFMSHIPPINGMODE"] == null ? string.Empty : salesLine["AFMSHIPPINGMODE"].ToString();
            if (lineordernumber.Length > 4)
            {
                lineordernumber = lineordernumber.Insert(4, " ");
                salesOrderItem.LineOrderNumber = lineordernumber.Insert(lineordernumber.Length - 2, " ");
            }
            else
                salesOrderItem.LineOrderNumber = lineordernumber;
            //NE by v-hapat for thankyou page            
            if (UnitOfMeasure.Contains(salesLine["UNIT"]))
                salesOrderItem.AFMQtyPerBox = Convert.ToInt32(salesLine["AFMQTYPERBOX"]);

            //NS spriya - Synchrony Online
          /*  salesOrderItem.AfmFinancingOptionId = salesLine["AFMFINANCINGOPTIONID"] != null ? (salesLine["AFMFINANCINGOPTIONID"]).ToString() : string.Empty;
            salesOrderItem.CardName = salesLine["CARDNAME"] != null ? (salesLine["CARDNAME"]).ToString() : string.Empty;
            salesOrderItem.PromoDesc = salesLine["PROMODESC"] != null ? (salesLine["PROMODESC"]).ToString() : string.Empty;*/
            //NE spriya - synchrony Online

            /*salesOrderItem.AFMShippingMode = salesLine["AFMShippingMode"] == null ? string.Empty : salesLine["AFMShippingMode"].ToString();
            salesOrderItem.AFMShippingCost = Convert.ToDecimal(salesLine["AFMShippingCost"]);
            salesOrderItem.PriceAfterDiscount = salesLine["PriceAfterDiscount"] == null ? string.Empty : salesLine["PriceAfterDiscount"].ToString();
            salesOrderItem.LineOrderNumber = salesLine["LineOrderNumber"] == null ? string.Empty : salesLine["LineOrderNumber"].ToString();
            salesOrderItem.AFMMultipleQty = Convert.ToDecimal(salesLine["AFMMultipleQty"]);
            salesOrderItem.AFMLowestQty = Convert.ToDecimal(salesLine["AFMLowestQty"]);*/
        }

        private static DateTime ConvertToDateTime(object value)
        {
            DateTime convertedDate = DateTime.MinValue;
            try
            {
                if (value != null && !string.IsNullOrEmpty(value.ToString()))
                    convertedDate = Convert.ToDateTime(value.ToString());
            }
            catch
            {
            }
            return convertedDate;
        }


        private Collection<TenderDataLine> ConvertToDataModel(Collection<CrtDataModel.TenderLine> tenderLines)
        {
            Collection<TenderDataLine> returnTenderLines = new Collection<TenderDataLine>();

            foreach (CrtDataModel.TenderLine tenderLine in tenderLines)
            {
                TenderDataLine tenderDataLine = new TenderDataLine();
                tenderDataLine.Amount = tenderLine.Amount;
                tenderDataLine.GiftCardId = tenderLine.GiftCardId;
                tenderDataLine.LoyaltyCardId = tenderLine.LoyaltyCardId;
                tenderDataLine.TokenizedPaymentCard = new TokenizedPaymentCard();
                tenderDataLine.TokenizedPaymentCard.CardType = tenderLine.CardTypeId;
                tenderDataLine.TokenizedPaymentCard.MaskedCardNumber = tenderLine.MaskedCardNumber;
                returnTenderLines.Add(tenderDataLine);
            }

            return returnTenderLines;
        }
        //NS - RxL

        private void AFMPopulateCustomSalesOrderDetails(CrtDataModel.SalesOrder order, SalesOrder vmOrder)
        {
            //NS by v-hapat for Thank you page
            vmOrder.TotalAmount = order.TotalAmount;
            vmOrder.DiscountAmount = order.DiscountAmount;
            vmOrder.Subtotal = order.SubtotalAmount.ToString();
            vmOrder.CustomerId = order.CustomerId;
            //NE by v-hapat for Thank you page

            //NS by spriya - SynchronyOnline
            vmOrder.AfmFinancingOptionId = order["FINANCINGOPTIONID"] != null ? order["FINANCINGOPTIONID"].ToString() : string.Empty;
            vmOrder.AFMCardName = order["CARDNAME"] != null ? order["CARDNAME"].ToString() : string.Empty;
            vmOrder.AFMPromoDesc = order["PROMODESC"] != null ? order["PROMODESC"].ToString() : string.Empty;
            //NE by spriya - SynchronyOnline


            //NS by v-hapat for Thank you page
            vmOrder.TenderLines = new Collection<TenderDataLine>();
            foreach (var tempTenderLine in order.TenderLines)
            {
                PaymentProperty[] propertySet = PaymentProperty.ConvertXMLToPropertyArray(tempTenderLine.Authorization);
                System.Collections.Hashtable tableResponse = new System.Collections.Hashtable();
                if (propertySet != null)
                    tableResponse = PaymentProperty.ConvertToHashtable(propertySet);
                else
                    tableResponse = null;

                vmOrder.TenderLines.Add(new TenderDataLine()
                {
                    Amount = tempTenderLine.Amount,
                    GiftCardId = tempTenderLine.GiftCardId,
                    LoyaltyCardId = tempTenderLine.LoyaltyCardId,
                    PaymentCard = new Payment()
                    {
                        CardNumber = tempTenderLine.MaskedCardNumber,
                        CardType = tempTenderLine.CardTypeId,
                        SynchronyPromoDesc = vmOrder.AFMPromoDesc,
                        FinancingOptionId = vmOrder.AfmFinancingOptionId,
                        NameOnCard = string.IsNullOrEmpty(vmOrder.AfmFinancingOptionId) ? (tableResponse != null ? (PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.Name) != null ? PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.Name).ToString() : string.Empty) : string.Empty) : vmOrder.AFMCardName
                    },
                    TokenizedPaymentCard = new TokenizedPaymentCard()
                    {

                        MaskedCardNumber = tempTenderLine.MaskedCardNumber,
                        CardType = tempTenderLine.CardTypeId,
                        SynchronyPromoDesc = vmOrder.AFMPromoDesc,
                        FinancingOptionId = vmOrder.AfmFinancingOptionId,
                        NameOnCard = string.IsNullOrEmpty(vmOrder.AfmFinancingOptionId) ? (tableResponse != null ? (PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.Name) != null ? PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.Name).ToString() : string.Empty) : string.Empty) : vmOrder.AFMCardName
                    }
                });
            }

            if (vmOrder.DiscountCodes.Count == 0)
            {
                foreach (var salesline in order.SalesLines)
                {
                    if (salesline.DiscountLines != null)
                    {
                        foreach (var discLine in salesline.DiscountLines)
                        {
                            if (!vmOrder.DiscountCodes.Contains(discLine.DiscountCode) && !string.IsNullOrEmpty(discLine.DiscountCode))
                            {
                                vmOrder.DiscountCodes.Add(discLine.DiscountCode);
                            }
                        }
                    }
                }
            }

            //NE by v-hapat for Thank you page

            CustomerController co = new CustomerController();
            var custAddresses = co.GetAddresses(order.CustomerId);
            if (custAddresses != null)
            {
                vmOrder.BillingAddress = custAddresses.FirstOrDefault(temp => temp.AddressType == AddressType.Invoice);

                if (order.ShippingAddress != null)
                    vmOrder.ShippingAddress = custAddresses.FirstOrDefault(temp => temp.RecordId == order.ShippingAddress.RecordId);
            }
        }
    }
}
