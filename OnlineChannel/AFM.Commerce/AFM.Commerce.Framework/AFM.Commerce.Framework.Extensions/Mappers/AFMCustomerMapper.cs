﻿using AFM.Commerce.Runtime.Services;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
namespace AFM.Commerce.Framework.Extensions.Mappers
{
    public static class AFMCustomerMapper
    {
        /// <summary>
        /// Convert the Data Model Customer to a View Model Customer.
        /// </summary>
        /// <param name="customer">The data model customer.</param>
        /// <returns>A view model customer.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the customer is null.</exception>
        internal static Customer ConvertToViewModel(CrtDataModel.Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }
            Customer vmCustomer = new Customer()
            {
                //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                SourceInfo = Convert.ToString(customer[CustomerConstants.SourceInfo]),
                IsGuestCheckout = Convert.ToBoolean(customer[CustomerConstants.IsGuestCheckout]),
                IsMarketingOptIn = Convert.ToBoolean(customer[CustomerConstants.IsMarketingOptin]),
                IsOlderThan13 = Convert.ToBoolean(customer[CustomerConstants.IsOlderThan13]),
                //NE - RxL
                //NS developed by v-hapat for CR 72099 
                IsMarketingOptInDate = Convert.ToDateTime(customer[CustomerConstants.IsMarketingOptinDate]),
                IsMarketingOptInUniqueId = Convert.ToString(customer[CustomerConstants.IsMarketingOptinUniqueId]),
                IsMarketingOptInVersionId = Convert.ToString(customer[CustomerConstants.IsMarketingOptinVersionId]),
                //NE developed by v-hapat for CR 72099

                AccountNumber = customer.AccountNumber,
                Addresses = ConvertToViewModel(customer.Addresses),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                MiddleName = customer.MiddleName,
                Phone = customer.Phone,
                PhoneExt = customer.PhoneExt,
                RecordId = customer.RecordId,
                Url = customer.Url,
                PrimaryAddress = AFMAddressMapper.ConvertToViewModel(customer.GetPrimaryAddress())
            };
            return vmCustomer;
        }

        /// <summary>
        /// Convert a ViewModel customer to a Data Model customer.
        /// </summary>
        /// <param name="dataModelCustomer">The data model customer.</param>
        /// <param name="customer">The view model customer.</param>
        /// <returns>The data model customer.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the customer is null.</exception>
        internal static CrtDataModel.Customer UpdateDataModel(CrtDataModel.Customer dataModelCustomer, Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }

            if (dataModelCustomer == null)
            {
                throw new ArgumentNullException("dataModelCustomer");
            }

            //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
            dataModelCustomer[CustomerConstants.SourceInfo] = customer.SourceInfo == null ? string.Empty : customer.SourceInfo;
            dataModelCustomer[CustomerConstants.IsGuestCheckout] = Convert.ToInt32(customer.IsGuestCheckout);
            dataModelCustomer[CustomerConstants.IsMarketingOptin] = Convert.ToInt32(customer.IsMarketingOptIn);
            dataModelCustomer[CustomerConstants.IsOlderThan13] = Convert.ToInt32(customer.IsOlderThan13);
            //NE - RxL
            //NS developed by v-hapat for CR 72099            
            dataModelCustomer[CustomerConstants.IsMarketingOptinUniqueId] = customer.IsMarketingOptInUniqueId == null ? string.Empty : customer.IsMarketingOptInUniqueId;
            dataModelCustomer[CustomerConstants.IsMarketingOptinVersionId] = customer.IsMarketingOptInVersionId == null ? string.Empty : customer.IsMarketingOptInVersionId;
            dataModelCustomer[CustomerConstants.IsMarketingOptinDate] = new DateTimeOffset(customer.IsMarketingOptInDate == null ? Convert.ToDateTime("1900-01-01") : customer.IsMarketingOptInDate);
            //NS developed by v-hapat for CR 72099 


            dataModelCustomer.Email = customer.Email;
            dataModelCustomer.FirstName = customer.FirstName;
            dataModelCustomer.LastName = customer.LastName;
            dataModelCustomer.MiddleName = customer.MiddleName;
            dataModelCustomer.Name = customer.FirstName + customer.LastName;
            dataModelCustomer.Phone = customer.Phone;
            dataModelCustomer.PhoneExt = customer.PhoneExt;
            dataModelCustomer.Url = customer.Url;
            dataModelCustomer.Language = "en-us"; // only support en-Us in the UI at this time
            if (customer.Addresses != null)
            {
                //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                /*Address primaryAddressInCollection = null;
                if (customer.PrimaryAddress != null)
                    primaryAddressInCollection = customer.Addresses.FirstOrDefault(a => a.RecordId == customer.PrimaryAddress.RecordId);

                if (primaryAddressInCollection != null)
                {
                    //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                    primaryAddressInCollection.FirstName = customer.PrimaryAddress.FirstName;
                    primaryAddressInCollection.LastName = customer.PrimaryAddress.LastName;
                    primaryAddressInCollection.Email2 = customer.PrimaryAddress.Email2;
                    primaryAddressInCollection.Phone1Extension = customer.PrimaryAddress.Phone1Extension;
                    primaryAddressInCollection.Phone2 = customer.PrimaryAddress.Phone2;
                    primaryAddressInCollection.Phone2Extension = customer.PrimaryAddress.Phone2Extension;
                    primaryAddressInCollection.Phone3 = customer.PrimaryAddress.Phone3;
                    primaryAddressInCollection.Phone3Extension = customer.PrimaryAddress.Phone3Extension;
                    //NE - RxL

                    primaryAddressInCollection.AddressFriendlyName = customer.PrimaryAddress.AddressFriendlyName;
                    primaryAddressInCollection.AttentionTo = customer.PrimaryAddress.AttentionTo;
                    primaryAddressInCollection.City = customer.PrimaryAddress.City;
                    primaryAddressInCollection.Country = customer.PrimaryAddress.Country;
                    primaryAddressInCollection.Deactivate = customer.PrimaryAddress.Deactivate;
                    primaryAddressInCollection.DistrictName = customer.PrimaryAddress.DistrictName;
                    primaryAddressInCollection.Email = customer.PrimaryAddress.Email;
                    primaryAddressInCollection.EmailContent = customer.PrimaryAddress.EmailContent;
                    primaryAddressInCollection.Name = customer.PrimaryAddress.FirstName + customer.PrimaryAddress.LastName;
                    primaryAddressInCollection.IsPrimary = customer.PrimaryAddress.IsPrimary;
                    primaryAddressInCollection.Phone = customer.PrimaryAddress.Phone;
                    primaryAddressInCollection.State = customer.PrimaryAddress.State;
                    primaryAddressInCollection.Street = customer.PrimaryAddress.Street;
                    primaryAddressInCollection.StreetNumber = customer.PrimaryAddress.StreetNumber;
                    primaryAddressInCollection.AddressType = customer.PrimaryAddress.AddressType;
                    primaryAddressInCollection.ZipCode = customer.PrimaryAddress.ZipCode;
                }
                else
                {
                   
                    //If the primary address is added to the addresses collection before this line of code this needs to be removed.
                    foreach (Address address in customer.Addresses)
                    {
                        address.IsPrimary = false;
                    }
                   
                    if (customer.PrimaryAddress != null)
                    {
                        customer.PrimaryAddress.IsPrimary = true;

                        ((List<Address>)customer.Addresses).Add(customer.PrimaryAddress);
                    }                    
                }*/
                //NE - RxL

                dataModelCustomer = AFMCustomerMapper.ConvertToDataModel(dataModelCustomer, customer.Addresses);
            }
            return dataModelCustomer;
        }


        /// <summary>
        /// Convert a View Model address to a Data Model address.
        /// </summary>
        /// <param name="dmAddress">The data model address.</param>
        /// <param name="address">The view model address.</param>
        /// <returns>The data model address.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the address is null.</exception>
        internal static CrtDataModel.Address UpdateDataModel(CrtDataModel.Address dmAddress, Address address)
        {
            if (address == null)
            {
                throw new ArgumentNullException("address");
            }

            if (dmAddress == null)
            {
                throw new ArgumentNullException("dmAddress");
            }

            //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
            dmAddress[CustomerConstants.AddressFirstName] = address.FirstName;
            dmAddress[CustomerConstants.AddressLastName] = address.LastName;
            dmAddress[CustomerConstants.Email2] = address.Email2;
            // dmAddress[CustomerConstants.Phone1] = address.Phone;
            dmAddress[CustomerConstants.Phone1Extension] = address.Phone1Extension;
            dmAddress[CustomerConstants.Phone2] = address.Phone2;
            dmAddress[CustomerConstants.Phone2Extension] = address.Phone2Extension;
            dmAddress[CustomerConstants.Phone3] = address.Phone3;
            dmAddress[CustomerConstants.Phone3Extension] = address.Phone3Extension;
            dmAddress.Email = address.Email;
            dmAddress.EmailContent = address.EmailContent;
            dmAddress.PhoneExt = address.Phone1Extension;
            //NE - RxL

            dmAddress.AttentionTo = address.AttentionTo;
            dmAddress.City = address.City;
            dmAddress.ThreeLetterISORegionName = address.Country;
            dmAddress.County = address.County;
            dmAddress.DistrictName = address.DistrictName;
            dmAddress.Phone = address.Phone;
            dmAddress.State = address.State;
            if (!string.IsNullOrEmpty(address.Street2) && !address.Street.Contains(Environment.NewLine))
                dmAddress.Street = address.Street + Environment.NewLine + address.Street2;
            else
                dmAddress.Street = address.Street;
            dmAddress.StreetNumber = address.StreetNumber;
            dmAddress.ZipCode = address.ZipCode;
            dmAddress.Deactivate = address.Deactivate;
            dmAddress.AddressType = (CrtDataModel.AddressType)Enum.Parse(address.AddressType.GetType(), address.AddressType.ToString());
            dmAddress.IsPrimary = address.IsPrimary;
            //NS Developed by spriya
            address.Name = address.FirstName +" "+ address.LastName;
            dmAddress.Name = address.Name;
            //NE Developed by spriya
            return dmAddress;
        }

        /// <summary>
        /// Convert a Data Model list of Addresses to a View Model list of Addresses.
        /// </summary>
        /// 
        /// <param name="addresses">The data model addresses.</param>
        /// <returns>Collection of view model address objects.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the addresses are null.</exception>
        public static Collection<Address> ConvertToViewModel(IEnumerable<CrtDataModel.Address> addresses)
        {
            if (addresses == null)
            {
                throw new ArgumentNullException("addresses");
            }

            Collection<Address> vmAddresses = new Collection<Address>();

            foreach (CrtDataModel.Address address in addresses)
            {
                if (address != null)
                {
                    vmAddresses.Add(AFMAddressMapper.ConvertToViewModel(address));
                }
            }

            return vmAddresses;
        }

        /// <summary>
        /// Convert a View Model list of addresses to a Data Model list of addresses.
        /// </summary>
        /// <param name="dmCustomer">The data model model to be updated.</param>
        /// <param name="addresses">The view model addresses.</param>
        /// <returns>The updated data model customer.</returns>
        public static CrtDataModel.Customer ConvertToDataModel(CrtDataModel.Customer dmCustomer, IEnumerable<Address> addresses)
        {
            List<CrtDataModel.Address> dmAddresses = new List<CrtDataModel.Address>();

            if (addresses != null)
            {
                foreach (Address address in addresses)
                {
                    if (address != null)
                    {
                        CrtDataModel.Address tempAdddress = new CrtDataModel.Address();

                        // If the address exists already find the matching record.
                        if (address.RecordId > 0)
                        {
                            foreach (CrtDataModel.Address dmAddress in dmCustomer.Addresses)
                            {
                                if (dmAddress.RecordId == address.RecordId)
                                {
                                    tempAdddress = dmAddress;
                                    break;
                                }
                                if(address.AddressType == AddressType.Invoice && dmAddress.AddressType == CrtDataModel.AddressType.Invoice)
                                {
                                    tempAdddress = dmAddress;
                                    address.RecordId = dmAddress.RecordId;
                                    break;
                                }
                            }
                        }

                        dmAddresses.Add(UpdateDataModel(tempAdddress, address));
                    }
                }
            }

            dmCustomer.Addresses = dmAddresses;

            return dmCustomer;
        }
    }
}
