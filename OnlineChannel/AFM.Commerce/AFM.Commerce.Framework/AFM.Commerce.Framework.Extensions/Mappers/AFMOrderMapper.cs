﻿using CRTModel= Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using System.Dynamic;
using Newtonsoft.Json;

namespace AFM.Commerce.Framework.Extensions.Mappers
{
    public static class AFMOrderMapper
    {
        private const string ListingPropTitle = "Title";
        private const string ListingPropPath = "Path";
               

        public static Listing ConvertToViewModel(CRTModel.Product product, long productId, string dimensionValues = "")
        {
            string productDetails = string.Empty;
            
            //CS by muthait dated 21JAN2015
            dynamic dimensionData = new ExpandoObject();
            dynamic productListing = new ExpandoObject();
            bool isDimensionData = false;

            if (string.IsNullOrEmpty(dimensionValues) && product.IsMasterProduct)
            {
                CRTModel.ProductVariant kitVariant = product.CompositionInformation.VariantInformation.Variants.Where(v => v.DistinctProductVariantId == productId).SingleOrDefault();

                if (!string.IsNullOrEmpty(kitVariant.Color)){
                    dimensionData.Color = kitVariant.Color;
                    isDimensionData = true;
                }
                    //dimensionValues += String.Format("Color: {0} ", kitVariant.Color);
                if (!string.IsNullOrEmpty(kitVariant.Style)){
                    dimensionData.Style = kitVariant.Style;
                    isDimensionData = true;
                }
                    //dimensionValues += String.Format("Style: {0} ", kitVariant.Style);
                if (!string.IsNullOrEmpty(kitVariant.Size)){
                    dimensionData.Size = kitVariant.Size;
                    isDimensionData = true;
                }
                   //dimensionValues += String.Format("Size: {0} ", kitVariant.Size);
            }       

            productListing.Name = product.ProductName;
            productListing.Description = product.Description;
            if(isDimensionData)
                productListing.DimensionValues = dimensionData;

            productDetails = JsonConvert.SerializeObject(productListing);

            //    productDetails = String.Format("{{ \"Name\": \"{0}\", \"Description\": \"{1}\",  \"DimensionValues\": \"{2}\" }}", product.ProductName, product.Description, dimensionValues);
            //else
            //    productDetails = String.Format("{{ \"Name\": \"{0}\", \"Description\": \"{1}\"}}", product.ProductName, product.Description);
            
            // CE by muthait dated 21JAN2015
            return new Listing
            {
                ListingId = productId,
                ItemId = product.ItemId,
                Quantity = 1,
                ProductDetails = productDetails
            };
        }
    }
}