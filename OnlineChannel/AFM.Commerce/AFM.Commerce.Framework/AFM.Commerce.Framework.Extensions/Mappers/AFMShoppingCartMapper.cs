﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace AFM.Commerce.Framework.Extensions.Mappers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Controllers;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CartType = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.CartType;
    using Listing = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using AFM.Commerce.Framework.Extensions.Utils;
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.CRTServices;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;

    /// <summary>
    /// Performs mappings between the Shopping Cart view model and Commerce Runtime Sdk entities.
    /// </summary>
    public class AFMShoppingCartMapper : ShoppingCartMapper
    {
        private IConfiguration configurationValue;
        public AFMShoppingCartMapper(IConfiguration configuration)
            : base(configuration)
        {
            this.configurationValue = configuration;
        }
        //NS by spriya : DMND0010113 - Synchrony Financing online
        public List<AFMSynchronyOptions> ConvertToViewModel(List<AFM.Commerce.Framework.Core.Models.AFMSynchronyOptions> synchronyOptionList, decimal cartTotalAmount,decimal cartSubTotalAmount,decimal cartDiscountAmount)
       {
        List<AFMSynchronyOptions> synchronyOptions = new List<AFMSynchronyOptions>();
        foreach (AFM.Commerce.Framework.Core.Models.AFMSynchronyOptions syncOpt in synchronyOptionList)
        {
            AFMSynchronyOptions syncOption = new AFMSynchronyOptions();
            syncOption.CardType = syncOpt.CardType;
            syncOption.DefaultOrderTotal = syncOpt.DefaultOrderTotal;
            syncOption.EffectiveDate = syncOpt.EffectiveDate;
            syncOption.ExpirationDate = syncOpt.ExpirationDate;
            syncOption.EstimatedPercentage = syncOpt.EstimatedPercentage;
            if (syncOpt.RemoveCartDiscount)
            {
                syncOption.EstimatedTotal = cartTotalAmount + cartDiscountAmount;
                syncOption.SubTotal = cartSubTotalAmount + cartDiscountAmount;
            }
            else
            {
                syncOption.EstimatedTotal = cartTotalAmount;
                syncOption.SubTotal = cartSubTotalAmount;
            }
            syncOption.ThirdPartyTermCode = syncOpt.ThirdPartyTermCode;
            syncOption.FinancingOptionId = syncOpt.FinancingOptionId;
            syncOption.LearnMore = syncOpt.LearnMore;
            syncOption.MinimumPurchaseAmount = syncOpt.MinimumPurchaseAmount;
            syncOption.PaymentFee = syncOpt.PaymentFee;
            syncOption.PromoDesc1 = syncOpt.PromoDesc1;
            syncOption.PromoDesc2 = syncOpt.PromoDesc2;
            syncOption.PromoDesc3 = syncOpt.PromoDesc3;
            syncOption.RemoveCartDiscount = syncOpt.RemoveCartDiscount;
            syncOption.RemoveDiscountPopUp = syncOpt.RemoveDiscountPopUp;
            syncOption.ShowLearnMore = syncOpt.ShowLearnMore;
            syncOption.Sortorder = syncOpt.Sortorder;
            syncOption.Status = syncOpt.Status;
            syncOption.PaymTermId = syncOpt.PaymTermId;
            syncOption.PaymMode = syncOpt.PaymMode;
            if (syncOpt.MinimumPurchaseAmount == 0 || syncOpt.MinimumPurchaseAmount < cartTotalAmount)
                syncOption.ShowOption = true;
            else
                syncOption.ShowOption = false;
            synchronyOptions.Add(syncOption);
        }
        return synchronyOptions;
       }
        //NE by spriya : DMND0010113 - Synchrony Financing online
        /// <summary>
        /// Converts from CRT Cart to view model ShoppingCart.
        /// </summary>
        /// <param name="shoppingCart">The CRT shopping cart.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>A view model Shopping Cart.</returns>
        public ShoppingCart ConvertToViewModel(CrtDataModel.Cart shoppingCart, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, bool includeTax)
        {
            if (shoppingCart == null)
            {
                return null;
            }

            CrtDataModel.Customer dmCustomer = null;
            if (!string.IsNullOrWhiteSpace(shoppingCart.CustomerId))
            {
                var customerManager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
                dmCustomer = customerManager.GetCustomer(shoppingCart.CustomerId);
            }

            ShoppingCart cartViewModel = new ShoppingCart();
            cartViewModel.CartId = shoppingCart.Id.ToString();
            cartViewModel.LastModifiedDate = shoppingCart.ModifiedDateTime.HasValue ? shoppingCart.ModifiedDateTime.Value.DateTime : DateTime.UtcNow;
            //NS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/28/2014 - Modified by muthait dated 09/20/2014
            var custAddress = dmCustomer != null ? dmCustomer.Addresses.OrderByDescending(c => c.IsPrimary = true).FirstOrDefault(c => !string.IsNullOrEmpty(c.ZipCode)) : null;

            cartViewModel.CustomerId = dmCustomer != null ? dmCustomer.AccountNumber : string.Empty;
            //cartViewModel.ShippingAddress = AFMAddressMapper.ConvertToViewModel(custAddress);
            //NS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/24/2014 
            //CS by muthait dated 20Oct2014
            cartViewModel.ShippingAddress = AddressMapper.ConvertToViewModel(shoppingCart.ShippingAddress != null ? shoppingCart.ShippingAddress : custAddress);

            if (dataLevel > ShoppingCartDataLevel.Minimal)
            {
                cartViewModel.DiscountAmountWithCurrency = Decimal.Round(shoppingCart.DiscountAmount, 2).ToCurrencyString();
                // HXM to resolve 68137 ECS - You save text is missing on item price
                cartViewModel.DiscountAmount = Decimal.Round(shoppingCart.DiscountAmount, 2);
                cartViewModel.ChargeAmountWithCurrency = shoppingCart.ChargeAmount.ToCurrencyString();
                cartViewModel.SubtotalWithCurrency = shoppingCart.SubtotalAmount.ToCurrencyString();
                cartViewModel.CartType = (CartType)shoppingCart.CartType;
                cartViewModel.Name = shoppingCart.Name;
                cartViewModel.LoyaltyCardId = shoppingCart.LoyaltyCardId;
                cartViewModel.TaxAmountWithCurrency = shoppingCart.TaxAmount.ToCurrencyString();
                if (shoppingCart.DiscountCodes != null)
                {
                    foreach (var discountCode in shoppingCart.DiscountCodes)
                    {
                        //NS developed by v-hapat on date 12/29/2014
                        foreach (var cartItem in shoppingCart.CartLines)
                        {
                            if (cartItem.DiscountLines.FirstOrDefault(temp => temp.DiscountCode == discountCode) != null)
                            {
                                cartViewModel.DiscountCodes.Add(discountCode);
                                break;
                            }
                        }
                        //NE developed by v-hapat on date 12/29/2014
                        //cartViewModel.DiscountCodes.Add(discountCode);
                    }
                }
                //CS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 - removed by muthait, as i see it twice calculated in the logic, confirm to delete
                //cartViewModel.GrandTotal = (shoppingCart.TotalAmount + cartViewModel.EstShipping + cartViewModel.EstHomeDelivery).ToString();
                cartViewModel.TotalAmountWithCurrency = shoppingCart.SubtotalAmount.ToCurrencyString();
                if (includeTax)
                    cartViewModel.TotalAmountWithCurrency = (shoppingCart.SubtotalAmount + shoppingCart.TaxAmount).ToCurrencyString();
            }

            if (dataLevel == ShoppingCartDataLevel.All)
            {
                foreach (var promotionLine in shoppingCart.PromotionLines)
                {
                    cartViewModel.PromotionLines.Add(promotionLine);
                }
            }
            //CS Developed by  muthait for "Discounts Pricing and Promotions" dated 7/20/2014 
            string custZipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
            //CE Developed by  muthait for "Discounts Pricing and Promotions" dated 7/20/2014 
            //CS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/28/2014 
            //NS Developed by spriya Bug 59347
            //if (cartViewModel.ShippingAddress!=null && cartViewModel.ShippingAddress.ZipCode != null && cartViewModel.ShippingAddress.ZipCode.Length > 0)
            //custZipCode = cartViewModel.ShippingAddress != null ? cartViewModel.ShippingAddress.ZipCode.Substring(0,cartViewModel.ShippingAddress.ZipCode.IndexOf('-')) : string.Empty;
            var items = ConvertToViewModel(shoppingCart.CartLines, dataLevel, productValidator, custZipCode);
            if (items != null)
            {
                foreach (var item in items)
                {
                    cartViewModel.Items.Add(item);
                }
                ////NS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 
                cartViewModel.TotalAmount = (shoppingCart.TotalAmount + decimal.Parse(!string.IsNullOrEmpty(cartViewModel.EstHomeDelivery) ? cartViewModel.EstHomeDelivery : "0", NumberStyles.AllowCurrencySymbol | NumberStyles.Number)
                     + decimal.Parse(!string.IsNullOrEmpty(cartViewModel.EstShipping) ? cartViewModel.EstShipping : "0", NumberStyles.AllowCurrencySymbol | NumberStyles.Number));
                cartViewModel.TotalAmountWithCurrency = cartViewModel.TotalAmount.ToCurrencyString();
            }

            //N - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
            //Retrieve RSA ID from cart object and assign it to Sales Order.
            cartViewModel.RsaId = shoppingCart[CartConstant.CartRsaId] == null ? string.Empty : Convert.ToString( shoppingCart[CartConstant.CartRsaId]);
            //NS by spriya : DMND0010113 - Synchrony Financing online
            cartViewModel.AfmFinancingOptionId = shoppingCart[CartConstant.FinancingOptionId] == null ? string.Empty : Convert.ToString( shoppingCart[CartConstant.FinancingOptionId]);
            cartViewModel.RemoveCartDiscount = Convert.ToBoolean(shoppingCart[CartConstant.RemoveDiscount]);
            //NE by spriya : DMND0010113 - Synchrony Financing online
            return cartViewModel;
        }


        /// <summary>
        /// Converts CRT cart lines to view model shopping cart items.
        /// </summary>
        /// <param name="cartLines">The CRT cart lines.</param>
        /// 
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>A view model collection of shopping cart items.</returns>
        public Collection<TransactionItem> ConvertToViewModel(IEnumerable<CrtDataModel.CartLine> cartLines, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, string zipCode)
        {
            if (cartLines == null)
            {
                return null;
            }

            Collection<TransactionItem> cartItems = new Collection<TransactionItem>();
            foreach (CrtDataModel.CartLine cartLine in cartLines)
            {
                cartItems.Add(ConvertToViewModel(cartLine, dataLevel, productValidator, zipCode));
            }

            return cartItems;
        }

        /// <summary>
        /// Converts CRT cart line to view model shopping cart item.
        /// </summary>
        /// <param name="cartLine">The CRT cart line.</param>
        /// 
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>A view model shopping cart item.</returns>
        public TransactionItem ConvertToViewModel(CrtDataModel.CartLine cartLine, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, string zipCode)
        {
            if (cartLine == null)
            {
                return null;
            }

            TransactionItem cartItem = new TransactionItem();

            cartItem.LineId = cartLine.LineId;
            cartItem.ItemId = cartLine.LineData.ItemId;
            cartItem.VariantInventoryDimensionId = cartLine.LineData.InventoryDimensionId;
            cartItem.ProductId = cartLine.LineData.ProductId;
            cartItem.ProductDetails = (string)cartLine.LineData["ProductDetails"];
            cartItem.Quantity = (int)cartLine.LineData.Quantity;

            //NS by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
            cartItem.NetAmountWithCurrency = cartLine.NetAmountWithoutTax.ToCurrencyString();

            //Get the actual Net amount. NetAmountWithCurrency contains Rounded amount
            cartItem.AFMNetAmount = cartLine.NetAmountWithoutTax;
            //NE by RxL

            if (cartLine.LineData["AFMItemType"] != null)
                cartItem.AFMItemType = (int)cartLine.LineData["AFMItemType"];
            //N - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
            cartItem.AFMUnit = cartLine.UnitOfMeasureSymbol;

            //NS developed by hardik for Bug # 83173 dated 04/24/2015
            cartItem.DeliveryModeId = cartLine.DeliveryMode;
            //NE developed by hardik for Bug # 83173 dated 04/24/2015

            //NS Developed by muthait for AFM_TFS_40686 dated 06/24/2014
            //Comment for Performance and add this filed to cart line Dat Model
            //AFMVendorDetailsService vendorDetailService = new AFMVendorDetailsService();
            //CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();

            //GetVendorDetailsServiceRequest vendorDetailRequest =
            //  new GetVendorDetailsServiceRequest()
            //  {
            //      OperatingUnitNumber = cartLine.StoreNumber,
            //      ZipCode = AFMDataUtilities.GetAFMZipCodeFromCookie(),
            //      ItemId = cartItem.ItemId
            //  };
            //var reqContext = new RequestContext(runtime);
            //GetVendorDetailsServiceResponse vendorDetailResponse = runtime.Execute<GetVendorDetailsServiceResponse>(
            //        vendorDetailRequest, reqContext,
            //        true);
            //cartItem.IsEcommOwned = vendorDetailRequest.isEcomOwned;
            //Comment for Performance and add this filed to cart line Dat Model
            //GetVendorDetailsServiceResponse vendorDetailResponse = null;
            //if (cartItem.IsEcommOwned)
            //{
            //    vendorDetailResponse = vendorDetailService.GetVendorDetailsforEComOwnedItem(vendorDetailRequest);
            //}
            //else
            //{
            //    vendorDetailResponse = vendorDetailService.GetVendorDetailsbyZipCode(vendorDetailRequest);
            //}
            //if (vendorDetailResponse.VendorDetails.Count > 0)
            //{
            //    if (!string.IsNullOrEmpty(vendorDetailResponse.VendorDetails[0].AFMVendorId))
            //    {
            //        cartItem.AFMVendorId = vendorDetailResponse.VendorDetails[0].AFMVendorId;
            //        cartItem.AFMVendorName = vendorDetailResponse.VendorDetails[0].AFMVendorName;
            //    }
            //    else
            //    {
            //        cartItem.AFMVendorId = vendorDetailResponse.VendorDetails[0].AFIVendorId;
            //        cartItem.AFMVendorName = vendorDetailResponse.VendorDetails[0].AFIVendorName;
            //    }
            //}
            //NE Developed by muthait for AFM_TFS_40686 dated 06/24/2014

            cartItem.IsEcommOwned = Convert.ToBoolean(cartLine.LineData[CartConstant.IsEcommOwned]);
            cartItem.AFMVendorId = Convert.ToString(cartLine.LineData[CartConstant.AFMVendorId]);
            cartItem.AFMVendorName = Convert.ToString(cartLine.LineData[CartConstant.AFMVendorName]);

            cartItem.AFMShippingMode = Convert.ToString(cartLine.LineData[CartConstant.AFMShippingMode]);
            cartItem.AFMDirectShip = Convert.ToInt32(cartLine.LineData[CartConstant.AFMDirectShip]);
            cartItem.AFMSupplierDirectShip = Convert.ToInt32(cartLine.LineData[CartConstant.AFMSupplierDirectShip]);
            cartItem.DeliveryModeId = cartLine.DeliveryMode;

            if (dataLevel > ShoppingCartDataLevel.Minimal)
            {
                cartItem.TaxAmountWithCurrency = cartLine.LineData.TaxAmount.ToCurrencyString();

                //N by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
                //Get the actual Tax amount. TaxAmountWithCurrency contains Rounded amount
                cartItem.AFMTaxAmount = cartLine.LineData.TaxAmount;

                // HXM to resolve 68137 ECS - You save text is missing on item price
                cartItem.DiscountAmount = Decimal.Round(cartLine.LineData.DiscountAmount, 2);
                cartItem.DiscountAmountWithCurrency = Decimal.Round(cartLine.LineData.DiscountAmount, 2).ToCurrencyString();
                cartItem.OfferNames = String.Join(",", cartLine.LineData.DiscountLines.Select(dl => dl.OfferName));
                cartItem.PriceWithCurrency = cartLine.LineData.Price.ToCurrencyString();

                cartItem.ShippingAddress = AFMAddressMapper.ConvertToViewModel(cartLine.LineData.ShippingAddress);
            }

            //NE Developed by muthait dated 08/31/2014
            if (dataLevel == ShoppingCartDataLevel.All)
            {
                foreach (var promotionLine in cartLine.PromotionLines)
                {
                    cartItem.PromotionLines.Add(promotionLine);
                }
            }

            // Throws an exception if the Product Details blob is malformed.
            productValidator.ValidateProductDetails(cartItem.ProductDetails);


            cartItem.PriceAfterDiscount = Decimal.Round((cartLine.LineData.Price - (cartLine.LineData.DiscountAmount / cartLine.LineData.Quantity)), 2).ToCurrencyString();

            //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
            cartItem.AFMKitItemId = (string)cartLine.LineData["AFMKitItemId"];
            cartItem.AFMKitSequenceNumber = Convert.ToInt32(cartLine.LineData["AFMKitSequenceNumber"]);
            cartItem.AFMKitProductId = Convert.ToInt64(cartLine.LineData["AFMKitProductId"]);
            cartItem.AFMKitItemProductDetails = (string)cartLine.LineData["AFMKitItemProductDetails"];
            cartItem.AFMKitItemQuantity = Convert.ToInt32(cartLine.LineData["AFMKitItemQuantity"]);
            //NE - RxL

            //Update Order Setting To Cart Add to Cart Performance issue
            //BUG 94731 - Hardik Fix
            var QuantityPerBoxUpperBound = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["QuantityPerBoxUpperBound"]);
            cartItem.AFMMultipleQty = (Convert.ToInt32(cartLine.LineData["AFMMultipleQty"]) > 1) ? Convert.ToInt32(cartLine.LineData["AFMMultipleQty"]) : 1;
            cartItem.AFMLowestQty = (Convert.ToInt32(cartLine.LineData["AFMLowestQty"]) > 1) ? Convert.ToInt32(cartLine.LineData["AFMLowestQty"]) : 1;
            cartItem.AFMQtyPerBox = Convert.ToInt32(cartLine.LineData["AFMQuantityPerBox"]);

            if (cartItem.AFMMultipleQty > 1 || !OrderMapper.UnitOfMeasure.Contains(cartItem.AFMUnit))
            {
                cartItem.AFMQtyPerBox = 0;
            }
            if (cartItem.AFMMultipleQty > 1 || cartItem.AFMLowestQty > 1)
            {
                cartItem.AFMQtyOptions = new Collection<decimal>();
                decimal m = 0;
                for (int i = 1; m < QuantityPerBoxUpperBound; i++)
                {
                    if (cartItem.AFMLowestQty != 0)
                    {
                        if ((cartItem.AFMMultipleQty * i) >= cartItem.AFMLowestQty)
                        {
                            m = (cartItem.AFMMultipleQty * i);
                            if (m < QuantityPerBoxUpperBound)
                                cartItem.AFMQtyOptions.Add(m);
                        }
                    }
                    else
                    {
                        m = (cartItem.AFMMultipleQty * i);
                        if (m < QuantityPerBoxUpperBound)
                            cartItem.AFMQtyOptions.Add(m);
                    }
                }
            }
            //BUG 94731 - Hardik Fix
            //Update Order Setting To Cart
            return cartItem;
        }


        /// <summary>
        /// Gets the cart lines from listings.
        /// </summary>
        /// <param name="listings">The listings.</param>
        /// <returns>Cart lines.</returns>
        public Collection<CrtDataModel.CartLine> GetCartLinesFromListing(IEnumerable<Listing> listings, IProductValidator productValidator)
        {
            if (listings == null)
            {
                return null;
            }

            IDictionary<long, Listing> indexedListings = new Dictionary<long, Listing>();

            foreach (var listing in listings)
            {
                if (listing.Quantity <= 0)
                {
                    string message = string.Format("The quantity of a listing cannot be negative - Listing {0} Quantity {1}", listing.ListingId, listing.Quantity);
                    throw new DataValidationException(DataValidationErrors.InvalidQuantity, message);
                }

                long listingId = listing.ListingId;
                if (!indexedListings.ContainsKey(listingId))
                {
                    indexedListings.Add(listingId, listing);
                }
            }

            // From a perf view, it might be better to cache the gift card item id, instead of fetching the channel configuration everytime.
            ChannelManager channelManager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());
            string giftCardItemId = channelManager.GetChannelConfiguration().GiftCardItemId;

            IEnumerable<CrtDataModel.Product> dmProducts = GetProducts(indexedListings.Keys);
            Collection<CrtDataModel.CartLine> cartLines = new Collection<CrtDataModel.CartLine>();

            foreach (CrtDataModel.Product dmProduct in dmProducts)
            {

                // First process all potential standalone products.
                if (!dmProduct.IsMasterProduct)
                {
                    // Finds the first listing matching the given record id.
                    if (!indexedListings.ContainsKey(dmProduct.RecordId))
                    {
                        continue;
                    }
                    Listing listing = indexedListings[dmProduct.RecordId];

                    // Throws an exception if the Product Details blob is malformed.
                    productValidator.ValidateProductDetails(listing.ProductDetails);

                    CrtDataModel.CartLine cartLine = new CrtDataModel.CartLine();

                    cartLine.LineData = GetCartItemFromProductListing(dmProduct, listing);

                    // This is required if the gift card can be a standalone product.
                    if (string.Equals(dmProduct.ItemId, giftCardItemId, StringComparison.InvariantCultureIgnoreCase))
                    {
                        SetCartLineGiftCardProperties(cartLine, listing.GiftCardAmount);
                    }

                    //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
                    cartLine.LineData["AFMKitItemId"] = listing.AFMKitItemId;
                    cartLine.LineData["AFMKitProductId"] = listing.AFMKitProductId;
                    cartLine.LineData["AFMKitSequenceNumber"] = listing.AFMKitSequenceNumber;
                    cartLine.LineData["AFMKitItemProductDetails"] = listing.AFMKitItemProductDetails;
                    cartLine.LineData["AFMKitItemQuantity"] = listing.AFMKitItemQuantity;
                    //NE - RxL

                    // CS by muthait dated 31Oct2014
                    cartLine.LineData["AFMItemType"] = listing.AFMItemType;

                    //Start : Adding Order Setting for item detail to List  : Hardik 
                    cartLine.LineData["AFMMultipleQty"] = listing.AFMMultipleQty;
                    cartLine.LineData["AFMLowestQty"] = listing.AFMLowestQty;
                    cartLine.LineData["AFMQuantityPerBox"] = listing.AFMQuantityPerBox;
                    //End : Adding Order Setting for item detail to List : Hardik 

                    cartLines.Add(cartLine);

                    continue;
                }

                // otherwise we have to identify which of this product's variants were part of the original set of listings
                List<long> lookupIds = new List<long>(dmProduct.CompositionInformation.VariantInformation.IndexedVariants.Keys);
                IEnumerable<long> intersection = indexedListings.Keys.Intersect(lookupIds);

                // Loop to process all listings that map to variants of a product.
                foreach (var variantId in intersection)
                {
                    // Finds the first listing matching the given record id.
                    if (!indexedListings.ContainsKey(variantId))
                    {
                        continue;
                    }
                    Listing listing = indexedListings[variantId];

                    // Throws an exception if the Product Details blob is malformed.
                    productValidator.ValidateProductDetails(listing.ProductDetails);

                    CrtDataModel.CartLine cartLine = new CrtDataModel.CartLine();

                    cartLine.LineData = GetCartItemFromProductListing(
                            dmProduct.CompositionInformation.VariantInformation.IndexedVariants[variantId],
                            listing);

                    // This is required if we support variants of a gift card.
                    if (string.Equals(dmProduct.ItemId, giftCardItemId, StringComparison.InvariantCultureIgnoreCase))
                    {
                        SetCartLineGiftCardProperties(cartLine, listing.GiftCardAmount);
                    }

                    //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
                    cartLine.LineData["AFMKitItemId"] = listing.AFMKitItemId;
                    cartLine.LineData["AFMKitProductId"] = listing.AFMKitProductId;
                    cartLine.LineData["AFMKitSequenceNumber"] = listing.AFMKitSequenceNumber;
                    cartLine.LineData["AFMKitItemProductDetails"] = listing.AFMKitItemProductDetails;
                    cartLine.LineData["AFMKitItemQuantity"] = listing.AFMKitItemQuantity;
                    //NE - RxL


                    //Start : Adding Order Setting for item detail to List  : Hardik 
                    cartLine.LineData["AFMMultipleQty"] = listing.AFMMultipleQty;
                    cartLine.LineData["AFMLowestQty"] = listing.AFMLowestQty;
                    cartLine.LineData["AFMQuantityPerBox"] = listing.AFMQuantityPerBox;
                    //End : Adding Order Setting for item detail to List : Hardik 

                    cartLines.Add(cartLine);
                }
            }

            return cartLines;
        }

        /// <summary>
        /// Gets the products from cart item.
        /// </summary>
        /// <param name="shoppingCart">The shopping cart.</param>
        /// <returns>Listings.</returns>
        public ICollection<Listing> GetListingsFromCart(ShoppingCart shoppingCart)
        {
            if (shoppingCart == null)
            {
                throw new ArgumentNullException("shopping cart");
            }

            ICollection<Listing> listings = new Collection<Listing>();
            Collection<TransactionItem> Items = shoppingCart.Items;

            foreach (TransactionItem item in Items)
            {
                if (item.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                {
                    Listing listing = new Listing();
                    listing.ListingId = item.ProductId;
                    listing.ProductDetails = item.ProductDetails;
                    listing.Quantity = item.Quantity;
                    listing.Comment = item.Comment;
                    listing.ItemId = item.ItemId;
                    listing.AFMKitItemId = item.AFMKitItemId;
                    listing.AFMKitItemProductDetails = item.AFMKitItemProductDetails;
                    listing.AFMKitItemQuantity = item.AFMKitItemQuantity;
                    listing.AFMKitProductId = item.AFMKitProductId;
                    listing.AFMKitSequenceNumber = item.AFMKitSequenceNumber;
                    listings.Add(listing);
                }
            }

            return listings;
        }
    }
}