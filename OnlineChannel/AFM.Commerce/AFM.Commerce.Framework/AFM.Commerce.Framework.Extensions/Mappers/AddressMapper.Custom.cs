﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers
{
    using AFM.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using System;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
    public static partial class AddressMapper
    {
        /// <summary>
        /// Convert the Data Model Address to a View Model Address.
        /// </summary>
        /// <param name="address">The data model address</param>
        /// <returns>Address</returns>
        public static Address ConvertToViewModel(CrtDataModel.Address address)
        {
            if (address == null)
            {
                return null;
            }
            string[] adressStreet = null;
            if (address.Street.Contains(Environment.NewLine))
            {
                string[] splitchar = { Environment.NewLine };
                adressStreet = address.Street.Split(splitchar, StringSplitOptions.None);
            }
            else
            {
                adressStreet = new string[2];
                adressStreet[0] = address.Street;
                adressStreet[1] = string.Empty;
            }

            Address vmAddress = new Address()
            {
                StreetNumber = address.StreetNumber,
                Street = adressStreet[0],
                Street2 = adressStreet[1],
                City = address.City,
                State = address.State,
                County = address.County,
                DistrictName = address.DistrictName,
                ZipCode = address.ZipCode,
                Country = address.ThreeLetterISORegionName,
                Email = address.Email,
                EmailContent = address.EmailContent,
                Phone = address.Phone,
                AttentionTo = address.AttentionTo,
                AddressType = (AddressType)address.AddressTypeValue,
                RecordId = address.RecordId,
                AddressFriendlyName = address.Name,
                IsPrimary = address.IsPrimary,
                Name = address.Name,
                //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                FirstName = Convert.ToString(address[CustomerConstants.AddressFirstName]),
                LastName = Convert.ToString(address[CustomerConstants.AddressLastName]),
                Email2 = Convert.ToString(address[CustomerConstants.Email2]),
                Phone1Extension = Convert.ToString(address[CustomerConstants.Phone1Extension]),
                Phone2 = Convert.ToString(address[CustomerConstants.Phone2]),
                Phone2Extension = Convert.ToString(address[CustomerConstants.Phone2Extension]),
                Phone3 = Convert.ToString(address[CustomerConstants.Phone3]),
                Phone3Extension = Convert.ToString(address[CustomerConstants.Phone3Extension])
            };
            vmAddress.Email = !string.IsNullOrEmpty(vmAddress.Email) ? vmAddress.Email : (!string.IsNullOrEmpty(Convert.ToString(address[CustomerConstants.Email1])) ? Convert.ToString(address[CustomerConstants.Email1]) : string.Empty);
            vmAddress.Phone = !string.IsNullOrEmpty(vmAddress.Phone) ? vmAddress.Phone : (!string.IsNullOrEmpty(Convert.ToString(address[CustomerConstants.Phone])) ? Convert.ToString(address[CustomerConstants.Phone]) : string.Empty);

            //NE - RxL
            return vmAddress;
        }
    }
}
