﻿using AFM.Commerce.Framework.Core.ClientManager;

namespace AFM.Commerce.Framework.Extensions.Controllers
{
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Publishing;
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Extensions.Controllers;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Framework.Extensions.Utils;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Diagnostics;

    public class AFMPricingController : PricingController
    {
        /// <summary>
        /// This method fetches the AffiliationIds for the each of the product and then the price for each of them
        /// based on the affiliationIds
        /// </summary>
        /// <param name="itemIds"></param>
        public ReadOnlyCollection<AFMAllAffiliationPrices> GetAllPrices(List<string> itemIds)
        {
            if (itemIds == null)
            {
                throw new ArgumentNullException("itemIds");
            }
            try
            {
                // Get All the prices for all the Affiliations Use GetAffiliationsByProduct from the earlier DataManager class to receive the afffiliations and then 
                // search for the price.
                CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
                ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
                long channelRecId = channelManager.GetDefaultChannelId();

                List<ProductLookupClause> plc = itemIds.Select(pid => new ProductLookupClause(pid)).ToList();


                ProductSearchCriteria criteria = new ProductSearchCriteria(channelRecId)
                {
                    ItemIds = plc,
                    DataLevel = CommerceEntityDataLevel.Complete
                };

                ReadOnlyCollection<Product> products = new ReadOnlyCollection<Product>(new List<Product>());
                ProductManager productManager = ProductManager.Create(commerceRuntime);

                List<AFMAllAffiliationPrices> affPrices = new List<AFMAllAffiliationPrices>();

                foreach (string pid in itemIds)
                {
                    GetAFMAffiliationServiceResponse affResponse = AFMAffiliationController.GetAffiliationsByProduct(pid);
                    if (affResponse.AfmAffiliations.Count > 0)
                    {
                        foreach (AfmVendorAffiliation aff in affResponse.AfmAffiliations)
                        {
                            ReadOnlyCollection<AfmAffiliationPrice> priceForProductIds =
                                GetPriceByAffiliation(aff.AffiliationId, new List<string>() { pid }, products);
                            foreach (AfmAffiliationPrice listprice in priceForProductIds)
                            {
                                //if (listprice.ItemId == pid)
                                affPrices.Add(new AFMAllAffiliationPrices
                                {
                                    BestPrice = listprice.SalePrice.ToString(CultureInfo.InvariantCulture),
                                    AffiliationId = aff.AffiliationId,
                                    ItemId = listprice.ItemId
                                });
                            }
                        }
                    }
                    else
                    {
                        long defaultAffiliation = AFMAffiliationController.GetDefaultAffiliationId().AffiliationId;
                        ReadOnlyCollection<AfmAffiliationPrice> priceForProductIds =
                               GetPriceByAffiliation(defaultAffiliation, new List<string>() { pid }, products);
                        foreach (AfmAffiliationPrice listprice in priceForProductIds)
                        {
                            //if (listprice.ItemId == pid)
                            affPrices.Add(new AFMAllAffiliationPrices
                            {
                                BestPrice = listprice.SalePrice.ToString(CultureInfo.InvariantCulture),
                                AffiliationId = defaultAffiliation,
                                ItemId = listprice.ItemId
                            });
                        }
                    }
                }

                ReadOnlyCollection<AFMAllAffiliationPrices> retPrices =
                    new ReadOnlyCollection<AFMAllAffiliationPrices>(affPrices);
                return retPrices;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "itemIds : " + itemIds.Count + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// This method fetches the AffiliationIds for the each of the product and then the price for each of them
        /// based on the affiliationIds
        /// </summary>
        /// <param name="itemIds"></param>
        public ReadOnlyCollection<AfmAffiliationPrice> GetAllProductPrices(List<string> itemIds)
        {
            if (itemIds == null)
            {
                throw new ArgumentNullException("itemIds");
            }
            try
            {
                // Get All the prices for all the Affiliations Use GetAffiliationsByProduct from the earlier DataManager class to receive the afffiliations and then 
                // search for the price.
                NetTracer.Information("Method : GetAllProductPrices : Start getAllProduct Price for {0} products", itemIds.Count); //Add Log for Event Viwer -- Hardik 
                CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
                ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
                long channelRecId = channelManager.GetDefaultChannelId();

                long defaultAffiliation = AFMAffiliationController.GetDefaultAffiliationId().AffiliationId;
                NetTracer.Information("Method : GetAllProductPrices :  Fetch Default Affiliation {0}", defaultAffiliation); //Add Log for Event Viwer -- Hardik 
                List<AfmAffiliationPrice> affPrices = new List<AfmAffiliationPrice>();
                //var response = getAllPriceProductIds(itemIds, channelRecId, defaultAffiliation);
                //affPrices.AddRange(response);
                for (int i = 0; i < itemIds.Count; i += 1000)
                {
                    var itemidList = itemIds.Skip(i).Take(1000).ToList();
                    if (itemidList != null)
                    {
                        NetTracer.Information("Method : GetAllProductPrices :  Fetch Price Product {0} Price", itemidList.Count); //Add Log for Event Viwer -- Hardik 
                        var response = getAllPriceProductIds(itemidList, channelRecId, defaultAffiliation);
                        affPrices.AddRange(response);
                        NetTracer.Information("Method : GetAllProductPrices : Completed Fetch Price Product {0} Price", itemidList.Count); //Add Log for Event Viwer -- Hardik 
                    }
                }

                ReadOnlyCollection<AfmAffiliationPrice> retPrices =
                    new ReadOnlyCollection<AfmAffiliationPrice>(affPrices);
                return retPrices;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "itemIds : " + itemIds.Count + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        private ReadOnlyCollection<AfmAffiliationPrice> getAllPriceProductIds(List<string> itemIds, long channelRecId, long defaultAffiliation)
        {
            if (itemIds == null)
            {
                throw new ArgumentNullException("itemIds");
            }
            try
            {
                CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
                NetTracer.Information("Method : getAllPriceProductIds : Start Product Detail Fetch  For {0} items IDes", itemIds.Count); //Add Log for Event Viwer -- Hardik 
                List<ProductLookupClause> plc = itemIds.Select(pid => new ProductLookupClause(pid)).ToList();

                ProductSearchCriteria criteria = new ProductSearchCriteria(channelRecId)
                {
                    ItemIds = plc,
                    DataLevel = CommerceEntityDataLevel.Extended
                };

                ProductManager productManager = ProductManager.Create(commerceRuntime);

                ReadOnlyCollection<Product> products = new ReadOnlyCollection<Product>(new List<Product>());
                var queryResultSetting = new QueryResultSettings();
                queryResultSetting.Paging.Skip = 0;
                queryResultSetting.Paging.Top = itemIds.Count;
                products = productManager.SearchProducts(criteria, queryResultSetting).Results;
                NetTracer.Information("Method : getAllPriceProductIds : Completed Product Detail Fetch  For {0} items IDs", itemIds.Count); //Add Log for Event Viwer -- Hardik 
                List<AfmAffiliationPrice> affPrices = new List<AfmAffiliationPrice>();
                //long defaultAffiliation = AFMAffiliationController.GetDefaultAffiliationId().AffiliationId;

                #region Hardik Update for Performance
                GetAFMAffiliationServiceResponse affResponse = AFMAffiliationController.GetAllAffiliations();
                ReadOnlyCollection<AfmAffiliationPrice> priceForProductIds;
                NetTracer.Information("Method : getAllPriceProductIds : Start product Affiliaction For {0} items IDs", itemIds.Count); //Add Log for Event Viwer -- Hardik 
                if (affResponse.AfmAffiliations.FirstOrDefault(temp => temp.AffiliationId == defaultAffiliation) == null)
                {
                    var tempList = new List<AfmVendorAffiliation>();
                    tempList.AddRange(affResponse.AfmAffiliations);
                    tempList.Add(new AfmVendorAffiliation() { AffiliationId = defaultAffiliation });
                    affResponse.AfmAffiliations = tempList.AsReadOnly();
                }
                NetTracer.Information("Method : getAllPriceProductIds : Complete product Affiliaction For {0} items IDs", itemIds.Count); //Add Log for Event Viwer -- Hardik 
                foreach (var aff in affResponse.AfmAffiliations.Select(temp => temp.AffiliationId).Distinct())
                {                    
                    try
                    {
                        NetTracer.Information("Method : getAllPriceProductIds : Start Price by Affiliaction For id {0}", aff); //Add Log for Event Viwer -- Hardik 
                        priceForProductIds =
                            GetPriceByAffiliation(aff, new List<string>(), products.AsReadOnly());//                        
                        foreach (AfmAffiliationPrice listprice in priceForProductIds)
                        {
                            //if (listprice.ItemId == pid)
                            affPrices.Add(listprice);
                        }
                        NetTracer.Information("Method : getAllPriceProductIds : Completed Price by Affiliaction For id {0}", aff); //Add Log for Event Viwer -- Hardik 
                    }
                    catch (Exception ex)
                    {
                        Logging.LoggerUtilities.ProcessLogMessage(
                           "ItemId : " + products.Count() + " Affiliation : " + aff + Environment.NewLine +
                           ex.Message + Environment.NewLine +
                           ex.StackTrace);
                    }

                }

                #endregion

                ReadOnlyCollection<AfmAffiliationPrice> retPrices =
                        new ReadOnlyCollection<AfmAffiliationPrice>(affPrices);
                return retPrices;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "itemIds : " + itemIds.Count + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        public List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> itemIds, long affiliationId)
        {
            if (itemIds == null)
            {
                throw new ArgumentNullException("itemIds");
            }

            try
            {
                long defaultAffiliation = AFMAffiliationController.GetDefaultAffiliationId().AffiliationId;
                if (affiliationId <= 0)
                    affiliationId = defaultAffiliation;
                List<AFMItemPriceSnapshot> priceSnapshotList = new List<AFMItemPriceSnapshot>();
                AFMPriceSnapshotManager priceSnapshotClientManager =
                      AFMPriceSnapshotManager.Create(CrtUtilities.GetCommerceRuntime());
                priceSnapshotList = priceSnapshotClientManager.GetPriceSnapshot(itemIds, affiliationId);
                return priceSnapshotList;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                      "itemIds : " + itemIds.Count + Environment.NewLine +
                      ex.Message + Environment.NewLine +
                      ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }

        }

        /// <summary>
        /// Overload method to get prices when only item ids are passed without affilaition
        /// </summary>
        /// <param name="itemIds"></param>
        /// <returns></returns>
        public List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> itemIds)
        {
            if (itemIds == null)
            {
                throw new ArgumentNullException("itemIds");
            }

            try
            {
                List<AFMItemPriceSnapshot> priceSnapshotList = new List<AFMItemPriceSnapshot>();
                AFMPriceSnapshotManager priceSnapshotClientManager =
                      AFMPriceSnapshotManager.Create(CrtUtilities.GetCommerceRuntime());
                priceSnapshotList = priceSnapshotClientManager.GetPriceSnapshot(itemIds);
                return priceSnapshotList;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                      "itemIds : " + itemIds.Count + Environment.NewLine +
                      ex.Message + Environment.NewLine +
                      ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }

        }

        public List<AFMFulFillerInformation> GetAllFulfillerInformation()
        {
            try
            {
                List<AFMFulFillerInformation> fulfillerInformationList = new List<AFMFulFillerInformation>();
                AFMFulfillerInformationManager fulFillerInformationManager =
                      AFMFulfillerInformationManager.Create(CrtUtilities.GetCommerceRuntime());
                fulfillerInformationList = fulFillerInformationManager.GetAllFulfillerInformation();
                return fulfillerInformationList;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(ex.Message + Environment.NewLine +
                      ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }

        }

        /// <summary>
        /// This method would fetch the price for each of the product based on the affiliationId passed
        /// </summary>
        /// <param name="affiliationId"></param>
        /// <param name="itemIds"></param>
        /// <param name="products"></param>
        /// <param name="channelId"></param>
        public ReadOnlyCollection<AfmAffiliationPrice> GetPriceByAffiliation(long affiliationId, List<string> itemIds, ReadOnlyCollection<Product> products = null)
        {
            if (itemIds == null)
            {
                throw new ArgumentNullException("itemIds");
            }

            try
            {
                // CS by muthait dated 12Nov2014
                ReadOnlyCollection<Product> kitProducts = null;
                List<Product> TotalProducts = new List<Product>();
                // CE by muthait dated 12Nov2014
                // NC Change by HXM to address NAP Issue 81280 

                //TO DO Default affiliation is already retrieved, extra call
                if (affiliationId == 0)
                    affiliationId = AFMAffiliationController.GetDefaultAffiliationId().AffiliationId;
                var affPrices = new List<AfmAffiliationPrice>();
                CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
                ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
                long channelId = channelManager.GetCurrentChannelId();

                var criteria = new ProductSearchCriteria(channelId) { DataLevel = CommerceEntityDataLevel.Complete };
                ProductManager productManager = ProductManager.Create(commerceRuntime);

                DateTime activeDate = DateTime.Now;
                var context = new ProjectionDomain(channelId, null);
                string customerId = AFMDataUtilities.GetCustomerIdFromCookie();
                //Hitesh
                List<AffiliationLoyaltyTier> tiers = new List<AffiliationLoyaltyTier> {  new AffiliationLoyaltyTier
                    {
                        AffiliationId = affiliationId,
                        AffiliationType = RetailAffiliationType.General
                    }  
                 };

                if (products.IsNullOrEmpty())
                {
                    List<ProductLookupClause> items = itemIds.Select(itemid => new ProductLookupClause(itemid)).ToList();

                    criteria.ItemIds = items;
                    products = productManager.SearchProducts(criteria, new QueryResultSettings()).Results;
                }
                //CS by muthait dated 12Nov2014
                NetTracer.Information("Method : GetPriceByAffiliation : Fetching Kit component by Items For id {0}", products.Count); //Add Log for Event Viwer -- Hardik 
                //CS by muthait dated 12Nov2014
                var kitCriteria = new ProductSearchCriteria(channelId) { DataLevel = CommerceEntityDataLevel.Minimal };
                foreach (var pr in products)
                {
                    if (pr.IsKit)
                    {
                        var kitComponentList = pr.CompositionInformation.KitDefinition.KitLineDefinitions;

                        foreach (KitLineDefinition kitComponent in kitComponentList)
                        {
                            kitCriteria.Ids.Add(kitComponent.ComponentProductId);
                        }
                        if (kitCriteria.Ids.Count == 1000)
                        {
                            kitProducts = productManager.SearchProducts(kitCriteria, new QueryResultSettings()).Results;
                            if (kitProducts != null)
                                TotalProducts.AddRange<Product>(kitProducts.ToList());
                            kitCriteria.Ids.Clear();
                        }
                    }
                }
                kitProducts = productManager.SearchProducts(kitCriteria, new QueryResultSettings()).Results;
                if (kitProducts != null)
                    TotalProducts.AddRange<Product>(kitProducts.ToList());
                
                TotalProducts.AddRange<Product>(products.ToList());
                NetTracer.Information("Method : GetPriceByAffiliation : Completed Fetching Kit component by Now Total Product is {0}", TotalProducts.Count); //Add Log for Event Viwer -- Hardik 
                //if (kitProducts != null)
                //    TotalProducts.AddRange<Product>(kitProducts.ToList());
                //CE by muthait dated 12Nov2014
                if (TotalProducts != null)
                {
                    NetTracer.Information("Method : GetPriceByAffiliation : Fetching Active Price for Product {0}", TotalProducts.Count); //Add Log for Event Viwer -- Hardik 
                    //ReadOnlyCollection<ProductPrice> productPrices = productManager.GetActiveProductPrice(context, TotalProducts.Where(temp => !string.IsNullOrEmpty(temp.Rules.DefaultUnitOfMeasure)).Select(p => p.RecordId).ToList(), activeDate, customerId, tiers);

                    //foreach (Product pr in TotalProducts)
                    //{
                    //    ProductPrice productPrice = productPrices.FirstOrDefault(p => p.ItemId == pr.ItemId);
                    //    if (productPrice == null) continue;

                    //    if (productPrice.AdjustedPrice > 0)
                    //        pr.AdjustedPrice = productPrice.AdjustedPrice;
                    //    if (productPrice.TradeAgreementPrice > 0)
                    //        pr.BasePrice = productPrice.TradeAgreementPrice;
                    //}

                    ReadOnlyCollection<Product> discProducts = GetDiscountedProducts(channelId,
                        0, new ReadOnlyCollection<Product>(TotalProducts/*eligibleproducts*/), tiers);


                    foreach (Product pr in products)
                    {
                        Product prdt = discProducts.FirstOrDefault(p => p.ItemId == pr.ItemId);
                        if (prdt == null) continue;
                        if (prdt.AdjustedPrice > 0)
                            pr.AdjustedPrice = prdt.AdjustedPrice;
                        if (prdt.BasePrice > 0)
                            pr.BasePrice = prdt.BasePrice;

                        // NS CR159 - HXM - Change kit price to zero if any component is zero. Else kit price should be sum of the component prices
                        if (pr.IsKit) // if product is a kit we will loop through the prices of the components 
                        {
                            var kitComponentList = pr.CompositionInformation.KitDefinition.KitLineDefinitions;

                            pr.AdjustedPrice = 0;//fix price issue in production Bug 98903:ECS-NAP wrong on the site
                            pr.BasePrice = 0;//fix price issue in production Bug 98903:ECS-NAP wrong on the site

                            //// Fetch all the components ids along with their positions
                            foreach (KitLineDefinition kitComponent in kitComponentList)
                            {
                                long compRecId = kitComponent.ComponentProductId;
                                Product discProd = discProducts.FirstOrDefault(p => p.RecordId == compRecId);
                                decimal compAdjustedPrice = discProd != null ? discProd.AdjustedPrice : 0M;
                                compAdjustedPrice = compAdjustedPrice * Convert.ToInt32(kitComponent.IndexedComponentProperties[kitComponent.ComponentProductId].Quantity);

                                // HXM Fix for Bug 67030:ECS - Regular Price is wrong when Kit component has a promotion.
                                decimal compBasePrice = discProd != null ? discProd.BasePrice : 0M;
                                compBasePrice = compBasePrice * Convert.ToInt32(kitComponent.IndexedComponentProperties[kitComponent.ComponentProductId].Quantity);
                                if (compAdjustedPrice == 0 || compBasePrice == 0)
                                {
                                    pr.AdjustedPrice = 0;
                                    pr.BasePrice = 0;
                                    break;
                                }
                                else
                                {
                                    pr.AdjustedPrice += compAdjustedPrice;
                                    pr.BasePrice += compBasePrice;
                                }
                            }
                        }
                        // NE CR159 - HXM - Change kit price to zero if any component is zero. Else kit price should be sum of the component prices
                        affPrices.Add(new AfmAffiliationPrice { AffiliationId = affiliationId, ItemId = pr.ItemId, RegularPrice = pr.BasePrice, SalePrice = pr.AdjustedPrice });
                    }
                }

                var retAffPrices = new ReadOnlyCollection<AfmAffiliationPrice>(affPrices);
                return retAffPrices;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "affiliationId : " + affiliationId + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        //CS by Hitesh dated 07/19/2014
        public ReadOnlyCollection<Product> GetDiscountedProducts(long channelId, long catalogId, ReadOnlyCollection<Product> productList, List<AffiliationLoyaltyTier> affiliationids)
        {

            if (productList.Count == 0)
            {
                throw new ArgumentNullException("productList");
            }
            if (affiliationids.Count == 0)
            {
                throw new ArgumentNullException("affiliationids");
            }
            try
            {
                var runtime = CrtUtilities.GetCommerceRuntime();
                var manager = AFMPromotionsUtilityManager.Create(runtime);

                if (channelId == 0)
                {
                    var channelManager = ChannelManager.Create(runtime);
                    channelId = channelManager.GetCurrentChannelId();
                }

                SalesTransaction inputTrans = ConvertProductToTransaction(catalogId, productList.Where(temp => !string.IsNullOrEmpty(temp.Rules.DefaultUnitOfMeasure)).AsReadOnly());
                SalesTransaction outputTrans = manager.GetDiscountedProducts(catalogId, inputTrans, affiliationids);

                foreach (Product p in productList)
                {
                    Product p1 = p;
                    foreach (SalesLine l in outputTrans.SalesLines.Where(l => l.ProductId == p1.RecordId))
                    {
                        p.AdjustedPrice = l.AdjustedPrice - l.DiscountAmount;
                        p.BasePrice = l.Price;
                    }
                }
                return productList;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "channelId : " + channelId + "catalogId" + catalogId + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }

        }
        public SalesTransaction ConvertProductToTransaction(long catalogId, ReadOnlyCollection<Product> list)
        {
            if (list.Count == 0)
            {
                throw new ArgumentNullException("list");
            }
            try
            {
                SalesTransaction trans = new SalesTransaction();
                foreach (Product product in list)
                {
                    var sl = new SalesLine
                    {
                        ProductSource = product.IsRemote ? ProductSource.Remote : ProductSource.Local,
                        ItemId = product.ItemId,
                        ProductId = product.RecordId,
                        Quantity = 1,
                        Price = product.AdjustedPrice,
                        BasePrice = product.BasePrice,
                        AgreementPrice = product.BasePrice,
                        AdjustedPrice = product.AdjustedPrice,
                        LineId = System.Guid.NewGuid().ToString("N"),
                        SalesOrderUnitOfMeasure = product.Rules.DefaultUnitOfMeasure
                    };
                    ProductVariant variant = null;
                    if (product.IsMasterProduct && product.TryGetVariant(sl.ProductId, out variant))
                    {
                        sl.InventoryDimensionId = variant.InventoryDimensionId;
                        sl.Variant = variant;
                    }
                    else
                    {
                        sl.InventoryDimensionId = string.Empty;
                    }
                    sl.CatalogId = catalogId;
                    trans.SalesLines.Add(sl);
                }

                return trans;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       " catalogid : " + catalogId + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }


        //CE by Hitesh dated 07/19/2014


        //TO DO spriya added for merge
        /// <summary>
        /// Get latest prices for provided listings.
        /// </summary>
        /// <param name="productIds">Product identifiers.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="activeDate">Date that the price is needed for.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="affiliationLoyaltyTierIds">Affiliation loyalty tiers to be taken into account while calculating prices.</param>
        /// <returns>Price of listing.</returns>
        public ReadOnlyCollection<ListingPrice> GetActiveListingPrice(IEnumerable<long> productIds, long channelId, long? catalogId, DateTime activeDate, string customerId, IEnumerable<long> affiliationLoyaltyTierIds)
        {
            if (productIds.Count() == 0)
            {
                throw new ArgumentNullException("productIds");
            }

            if (catalogId == null)
            {
                throw new ArgumentNullException("catalogId");
            }
            if (activeDate == null)
            {
                throw new ArgumentNullException("activeDate");
            }
            if (affiliationLoyaltyTierIds.Count() == 0)
            {
                throw new ArgumentNullException("affiliationLoyaltyTierIds");
            }


            try
            {
                var runtime = CrtUtilities.GetCommerceRuntime();
                var manager = ProductManager.Create(runtime);

                if (channelId == 0)
                {
                    var channelManager = ChannelManager.Create(runtime);
                    channelId = channelManager.GetCurrentChannelId();
                }

                var context = new ProjectionDomain(channelId, catalogId);

                IList<AffiliationLoyaltyTier> tiers = null;
                if (affiliationLoyaltyTierIds != null)
                {
                    tiers = new List<AffiliationLoyaltyTier>();
                    foreach (long affiliationLoyaltyTierId in affiliationLoyaltyTierIds)
                    {
                        tiers.Add(new AffiliationLoyaltyTier { AffiliationId = affiliationLoyaltyTierId, AffiliationType = RetailAffiliationType.General });
                    }
                }
                var productPrices = manager.GetActiveProductPrice(context, productIds, activeDate, customerId, tiers);

                return new PricingMapper(this.Configuration).ConvertToViewModel(productPrices).AsReadOnly();
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       " catalogid : " + catalogId + " ChannelId : " + channelId + " activedate : " + activeDate + " CustomerId : " + customerId + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

    }
}
