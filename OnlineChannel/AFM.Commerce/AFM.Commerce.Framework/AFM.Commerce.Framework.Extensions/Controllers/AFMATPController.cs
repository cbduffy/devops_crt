﻿using AFM.Commerce.Framework.Core.ClientManager;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;

namespace AFM.Commerce.Framework.Extensions.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Extensions.Mappers;
    //using AFM.Commerce.Framework.Extensions.ATPService;

    /// <summary>
    /// The ATP Controller class
    /// </summary>
    public static class AFMATPController
    {
        /// <summary>
        /// Fetch's ATP for ecommerce 
        /// </summary>
        /// <param name="request">request object</param>
        /// <returns>response object</returns>
        public static AFMATPResponse GetAtpForEcommerce(AFMATPRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("productId");
            }
            try
            {
                AFMATPServiceManager atpMgr = AFMATPServiceManager.Create(CrtUtilities.GetCommerceRuntime());
                AFMATPResponse responseEntity = null;
                List<AFMATPItemGroupingResponse> itemGroupings = atpMgr.GetAtpForEcommerce(request);
                if (itemGroupings != null)
                {
                    responseEntity = new AFMATPResponse();
                    responseEntity.ItemGroupings = itemGroupings;
                }

                return responseEntity;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "AFMATPRequest " + request.ToString() + Environment.NewLine +
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Fetch's ATP for ecommerce asynchronously
        /// </summary>
        /// <param name="request">request object</param>
        /// <returns>response object</returns>
        public static Task<AFMATPResponse> GetAtpForEcommerceAsync(AFMATPRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("productId");
            }
            return null; //_atpClient.GetAtpForEcommerceAsync(request);
        }
    }
}


