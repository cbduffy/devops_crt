﻿namespace AFM.Commerce.Framework.Extensions.Controllers
{
    using System.Globalization;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AFM.Commerce.Entities;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using AFM.Commerce.Framework.Core.CRTServices;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Extensions.Utils;
    public static class AFMAffiliationController
    {
        public static AFMVendorAffiliation GetAffiliationByZipCode(string zipCode)
        {
           
            
            if (!AFMDataUtilities.IsValidZipCode(zipCode))
            {
                throw new ArgumentException("Invalid zip code", "zipCode");
            }

            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                ChannelManager channelManager = ChannelManager.Create(runtime);
                AFMAffiliationService affiliationDetailService = new AFMAffiliationService();
                GetAFMAffiliationServiceRequest affiliationDetailRequest = new GetAFMAffiliationServiceRequest()
                {
                    ZipCode = zipCode,
                    ChannelId = channelManager.GetDefaultChannelId(),
                    RequestType = GetAFMAffiliationServiceRequest.AffiliationRequestType.GetAffiliationIdByZipCode
                };
                RequestContext requestContext = new RequestContext(runtime);
               GetAFMAffiliationServiceResponse affiliationDetailResponse = runtime.Execute<GetAFMAffiliationServiceResponse>(affiliationDetailRequest, requestContext, true);
                
                if (affiliationDetailResponse.AfmAffiliation == null)
                {
                    return GetDefaultAffiliationId();
                }

                var afmAffiliationdata = affiliationDetailResponse.AfmAffiliation;

                return new AFMVendorAffiliation
                {
                    AffiliationId = afmAffiliationdata.AffiliationId,
                    InvoiceVendorId = afmAffiliationdata.InvoiceVendorId,
                    ShipToId = afmAffiliationdata.ShipToId,
                    VendorId = afmAffiliationdata.VendorId
                };
            }
            catch(Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "Zip code " + zipCode + Environment.NewLine +
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);
                throw;
            }
        }

        public static GetAFMAffiliationServiceResponse GetAffiliationsByProduct(string productId)
        {
            if (String.IsNullOrEmpty(productId))
            {
                throw new ArgumentNullException("productId");
            }
           
            if (!AFMDataUtilities.IsValidString(productId))
            {
                throw new ArgumentException("Invalid product id", "productId");
            }
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                AFMAffiliationService affiliationDetailService = new AFMAffiliationService();
                ChannelManager channelManager = ChannelManager.Create(runtime);
                GetAFMAffiliationServiceRequest affiliationDetailRequest = new GetAFMAffiliationServiceRequest()
                {
                    ChannelId = channelManager.GetDefaultChannelId(),
                    ProductId = productId
                };


                //affiliationDetailRequest.ChannelId = channelManager.GetDefaultChannelId();
                affiliationDetailRequest.RequestType = GetAFMAffiliationServiceRequest.AffiliationRequestType.GetAffiliationIdByProduct;
                //workflow call changed for RTM
                RequestContext requestContext = new RequestContext(runtime);
                GetAFMAffiliationServiceResponse affiliationDetailResponse = runtime.Execute<GetAFMAffiliationServiceResponse>(affiliationDetailRequest, requestContext, true);
               
                return affiliationDetailResponse;
            }
            catch(Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "ProductId" + productId + Environment.NewLine +
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);
                throw;
            }
        }

        public static GetAFMAffiliationServiceResponse GetAllAffiliations()
        {
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                AFMAffiliationService affiliationDetailService = new AFMAffiliationService();
                ChannelManager channelManager = ChannelManager.Create(runtime);
                GetAFMAffiliationServiceRequest affiliationDetailRequest = new GetAFMAffiliationServiceRequest()
                {
                    ChannelId = channelManager.GetDefaultChannelId()
                };

                affiliationDetailRequest.RequestType = GetAFMAffiliationServiceRequest.AffiliationRequestType.GetAllAffiliations;
                //workflow call changed for RTM
                RequestContext requestContext = new RequestContext(runtime);
                GetAFMAffiliationServiceResponse affiliationDetailResponse = runtime.Execute<GetAFMAffiliationServiceResponse>(affiliationDetailRequest, requestContext, true);
                return affiliationDetailResponse;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);
                throw;
            }
        }

        public static GetAFMAffiliationServiceResponse GetAffiliationsByProduct(List<string> productId)
        {
            if (productId==null)
            {
                throw new ArgumentNullException("productId");
            }
           
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                AFMAffiliationService affiliationDetailService = new AFMAffiliationService();
                ChannelManager channelManager = ChannelManager.Create(runtime);
                GetAFMAffiliationServiceRequest affiliationDetailRequest = new GetAFMAffiliationServiceRequest()
                {
                    ChannelId = channelManager.GetDefaultChannelId(),
                    ProductIds = productId
                };


                //affiliationDetailRequest.ChannelId = channelManager.GetDefaultChannelId();
                affiliationDetailRequest.RequestType = GetAFMAffiliationServiceRequest.AffiliationRequestType.GetAffiliationIdByProductIds;
                //workflow call changed for RTM
                RequestContext requestContext = new RequestContext(runtime);
                GetAFMAffiliationServiceResponse affiliationDetailResponse = runtime.Execute<GetAFMAffiliationServiceResponse>(affiliationDetailRequest, requestContext, true);

                return affiliationDetailResponse;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "ProductId" + productId + Environment.NewLine +
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);
                throw;
            }
        }

        public static AFMVendorAffiliation GetDefaultAffiliationId()
        {
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                ChannelManager channelManager = ChannelManager.Create(runtime);
                long channelId = channelManager.GetCurrentChannelId();

                AFMAffiliationService affiliationDetailService = new AFMAffiliationService();

                GetAFMAffiliationServiceRequest affiliationDetailRequest = new GetAFMAffiliationServiceRequest()
                {
                    ChannelId = channelId,
                    RequestType = GetAFMAffiliationServiceRequest.AffiliationRequestType.GetDefaultAffiliationId
                };

                RequestContext requestContext = new RequestContext(runtime);
                GetAFMAffiliationServiceResponse affiliationDetailResponse = runtime.Execute<GetAFMAffiliationServiceResponse>(affiliationDetailRequest, requestContext,true);
               
                //mapper needs to be written here - comment by muthait on 01/08/2014            
                var afmAffiliationdata = affiliationDetailResponse.AfmAffiliation;

                return afmAffiliationdata != null
                    ? new AFMVendorAffiliation
                    {
                        AffiliationId = afmAffiliationdata.AffiliationId,
                        InvoiceVendorId = afmAffiliationdata.InvoiceVendorId,
                        ShipToId = afmAffiliationdata.ShipToId,
                        VendorId = afmAffiliationdata.VendorId
                    }
                    : null;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);
                throw;
            }
        }
    }
}
