﻿namespace AFM.Commerce.Framework.Extensions.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine.DiscountData;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using System.Configuration;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using AFM.Commerce.Framework.Extensions.Utils;
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices;
    public static class AFMProductsController
    {
        public static ReadOnlyCollection<Product> GetProducts()
        {
            try
            {
                CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
                ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
                long channelId = channelManager.GetCurrentChannelId();
                var criteria = new ProductSearchCriteria(channelId);
                criteria.DataLevel = CommerceEntityDataLevel.Complete;
                ReadOnlyCollection<Category> categories = GetChannelNavigationalHierarchy();
                criteria.CategoryIds = categories.Select(i => i.RecordId).ToList();
                ProductManager productManager = ProductManager.Create(commerceRuntime);
                var quaryresultSetting = new QueryResultSettings();
                //quaryresultSetting.Paging.
                quaryresultSetting.Paging.Top = Int32.MaxValue;
                ReadOnlyCollection<Product> products = productManager.SearchProducts(criteria, quaryresultSetting).Results;
                products = GetProductAffiliation(products);

                return products;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        public static ReadOnlyCollection<Category> GetChannelNavigationalHierarchy()
        {
            try
            {
                CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
                ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
                long channelId = channelManager.GetCurrentChannelId();
                ReadOnlyCollection<Category> categories = channelManager.GetChannelCategoryHierarchy(new QueryResultSettings()).Results;
                return categories;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        //CS by Hitesh and muthait dated 07/21/2014
        public static ReadOnlyCollection<Product> GetProductAffiliation(ReadOnlyCollection<Product> products)
        {
            if (products.Count < 1)
            {
                throw new ArgumentNullException("products");
            }
            try
            {
                string zipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
                AFMVendorAffiliation afmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(zipCode);

                long currentAffiliationId = 0;
                if ((afmVendorAffiliation != null) && afmVendorAffiliation.AffiliationId > 0)
                {
                    currentAffiliationId = afmVendorAffiliation.AffiliationId;
                }

                ReadOnlyCollection<AfmAffiliationPrice> productPrices =
                new AFMPricingController().GetPriceByAffiliation(currentAffiliationId,
                    products.Select(p => p.ItemId).ToList(), products);

                foreach (Product pr in products)
                {
                    AfmAffiliationPrice afPrice = productPrices.FirstOrDefault(p => p.ItemId == pr.ItemId);
                    if (afPrice == null) continue;
                    pr.AdjustedPrice = afPrice.SalePrice;
                    //HXM AdjustedPrice is the Sales Price for the customization
                    pr.BasePrice = afPrice.RegularPrice;
                    // HXM BasePrice is the Regular Price for the customization
                }
                //var common = products.Where(p => productPrices.Any(a => a.ItemId == p.ItemId));
                return products;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }
        //CE by Hitesh and muthait dated 07/21/2014

        public static List<AFMPrdtNAP> GetPrdtNAPListData(List<long> productIds)
        {
            if (productIds == null)
            {
                throw new ArgumentNullException("productsIds");
            }
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                var getPrdtNAPRequest =
                    new GetPrdtNAPRequest()
                    {
                        ProductIds = productIds
                    };

                var afmProductService = new AFMProductService();
                var GetPrdtNAPList = afmProductService.GetPrdtNAPListData(getPrdtNAPRequest);
                if (GetPrdtNAPList.PrdtNAPList != null)
                    return GetPrdtNAPList.PrdtNAPList;
                else
                    return null;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
        //This method will return products details for given item ids
        public static ReadOnlyCollection<Product> GetProductsByItemIds(IList<string> itemIds)
        {
            if (itemIds == null)
            {
                throw new ArgumentNullException("itemIds");
            }

            try
            {
                return GetProductsByItemIds(itemIds, CommerceEntityDataLevel.Complete, true);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        public static ReadOnlyCollection<Product> GetProductsByItemIds(IList<string> itemIds, CommerceEntityDataLevel dataLevel = CommerceEntityDataLevel.Complete, bool IsPriceUpdate = true)
        {
            if (itemIds == null)
            {
                throw new ArgumentNullException("itemIds");
            }

            try
            {
                CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
                ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
                long channelId = channelManager.GetCurrentChannelId();
                ProductSearchCriteria criteria = new ProductSearchCriteria(channelId);
                criteria.DataLevel = dataLevel;
                List<ProductLookupClause> iids = new List<ProductLookupClause>();
                foreach (var item in itemIds)
                {
                    iids.Add(new ProductLookupClause(item));
                }
                criteria.ItemIds = iids;
                ProductManager productManager = ProductManager.Create(commerceRuntime);
                ReadOnlyCollection<Product> products = new ReadOnlyCollection<Product>(new List<Product>());

                products = productManager.SearchProducts(criteria, new QueryResultSettings()).Results;

                // NS CR159 - HXM - Change kit price to zero if any component is zero. Else kit price should be sum of the component prices                

                foreach (Product selectedProduct in products.Where(p => p.IsKit == true))
                {
                    List<long> kitComponentRecIds = new List<long>();
                    List<Product> productList = new List<Product>(products);
                    foreach (var component in selectedProduct.CompositionInformation.KitDefinition.KitLineDefinitions)
                    {
                        kitComponentRecIds.Add(component.ComponentProductId);
                    }

                    productList.AddRange(GetProductsByIds(kitComponentRecIds,CommerceEntityDataLevel.Minimal));
                    products = productList.AsReadOnly();
                }               

                // NS CR159 - HXM - Change kit price to zero if any component is zero. Else kit price should be sum of the component prices
                if (IsPriceUpdate)
                    products = GetProductAffiliation(products);
                return products;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }
        //NE - RxL - FDD0238-ECS-Manage Shopping Cart v3 0

        public static ReadOnlyCollection<Product> GetProductsByIds(IList<long> productIds, CommerceEntityDataLevel dataLevel = CommerceEntityDataLevel.Complete)
        {
            CommerceRuntime commerceRuntime = CrtUtilities.GetCommerceRuntime();
            ChannelManager channelManager = ChannelManager.Create(commerceRuntime);
            long channelId = channelManager.GetCurrentChannelId();
            ProductManager manager = ProductManager.Create(commerceRuntime);
            ProductSearchCriteria criteria = new ProductSearchCriteria(channelId);
            criteria.Ids = productIds;
            criteria.DataLevel = dataLevel;

            return manager.SearchProducts(criteria, new QueryResultSettings()).Results;
        }
    }
}
