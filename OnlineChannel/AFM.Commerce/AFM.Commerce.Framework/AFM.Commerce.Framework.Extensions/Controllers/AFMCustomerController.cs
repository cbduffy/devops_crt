﻿using System.Globalization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Entities;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Microsoft.Dynamics.Commerce.Runtime.Client;
using AFM.Commerce.Framework.Core.CRTServices;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
using AFM.Commerce.Framework.Extensions.Mappers;
using AFM.Commerce.Runtime.Services;
using AFM.Commerce.Framework.Extensions.Utils;
namespace AFM.Commerce.Framework.Extensions.Controllers
{
    public class AFMCustomerController : CustomerController
    {
        //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
        /// <summary>
        /// Create a new customer.
        /// </summary>
        /// <param name="customer">An existing customer.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// A new customer, based on existing customer.
        /// </returns>
        public override Customer CreateCustomer(Customer customer, string userId)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }
            if (String.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException("userid");
            }

            if (!AFMDataUtilities.IsValidString(userId))
            {
                throw new ArgumentException("Invalid user id", "userId");
            }

            try
            {


                var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

                CrtDataModel.Customer newDMCustomer = CustomerMapper.UpdateDataModel(new CrtDataModel.Customer(), customer);

                //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                //Customer should be created without default address
                // Create a default address, if one not present
                //CustomerController.EnsureDefaultAddress(newDMCustomer);
                //NS - RxL

                CrtDataModel.Customer dmCustomer = manager.CreateCustomer(newDMCustomer);

                Customer vmCustomer = CustomerMapper.ConvertToViewModel(dmCustomer);
                return vmCustomer;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
       "customer : " + customer.FirstName + " userId: " + userId + Environment.NewLine +
       ex.Message + Environment.NewLine +
       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }


        }
        //NE - RxL

        /// <summary>
        /// Create a new customer.
        /// </summary>
        /// <param name="emailAddress">Email addres for an existing customer who is in 3rd party company.</param>
        /// <param name="activationToken">Activation token.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns>
        /// A new customer in current company, based on existing customer.
        /// </returns>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">There are no customer that exists with the provided email address.</exception>
        public Customer CreateCustomerByEmail(string emailAddress, string activationToken, string userId)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }
            if (string.IsNullOrWhiteSpace(userId))
            {
                throw new ArgumentNullException("userId");
            }
            if (!AFMDataUtilities.IsValidEmail(emailAddress))
            {
                throw new ArgumentException("Invalid email id", "emailAddress");
            }
            if (!AFMDataUtilities.IsValidString(userId))
            {
                throw new ArgumentException("Invalid user id", "userId");
            }
            if (string.IsNullOrWhiteSpace(activationToken))
            {
                throw new ArgumentNullException("activationToken");
            }
            try
            {
                var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

                // Validate the request
                manager.ValidateAccountActivationRequest(emailAddress, activationToken);

                // Search customer by email address
                CrtDataModel.CustomerSearchCriteria criteria = new CrtDataModel.CustomerSearchCriteria() { Keyword = emailAddress };
                CrtDataModel.GlobalCustomer newDMGlobalCustomer = manager.SearchCustomers(criteria, new CrtDataModel.QueryResultSettings()).Results.FirstOrDefault();

                if (newDMGlobalCustomer == null)
                {
                    string message = string.Format(CultureInfo.CurrentCulture, "Customer not found for the provided email address: {0}.", emailAddress);
                    throw new DataValidationException(NoCustomerExistsWithGivenEmailError, message);
                }

                CrtDataModel.Customer dmCustomer = null;
                if (string.IsNullOrEmpty(newDMGlobalCustomer.AccountNumber))
                {
                    CrtDataModel.Customer newCustomer = new CrtDataModel.Customer();
                    newCustomer.NewCustomerPartyNumber = newDMGlobalCustomer.PartyNumber;
                    dmCustomer = manager.CreateCustomer(newCustomer);
                }
                else
                {
                    dmCustomer = manager.GetCustomer(newDMGlobalCustomer.AccountNumber);
                }

                // Associate the newly created customer w/the current identity.
                CreateUserCustomerMap(dmCustomer, userId);

                // Update channel DB to mark the request as 'finished'
                manager.FinalizeAccountActivation(emailAddress, activationToken);

                return CustomerMapper.ConvertToViewModel(dmCustomer);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "EmailAddress : " + emailAddress + " ActivactionToken: " + activationToken + " UserId: " + userId + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }

        /// <summary>
        /// Search for a customer given the corresponding email address.
        /// </summary>
        /// <param name="emailAddress">email address of the customer.</param>
        /// <returns>True if the customer exists, false otherwise.</returns>
        public bool SearchCustomers(string emailAddress)
        {
            if (String.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }



            if (!AFMDataUtilities.IsValidEmail(emailAddress))
            {
                throw new ArgumentException("Invalid email id", "emailAddress");
            }

            try
            {
                // Validate that there is a customer associated with the email address
                CustomerManager manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

                CrtDataModel.CustomerSearchCriteria criteria = new CrtDataModel.CustomerSearchCriteria() { Keyword = emailAddress };
                PagedResult<CrtDataModel.GlobalCustomer> customers = manager.SearchCustomers(criteria, new CrtDataModel.QueryResultSettings());

                return customers.Results.Any();
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "EamilAddress: " + emailAddress + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }

        /// <summary>
        /// Search for a customer given the corresponding email address.
        /// </summary>
        /// <param name="emailAddress">email address of the customer.</param>
        /// <returns>The customer Id if the customer was found, null otherwise.</returns>
        public string SearchCustomersAndReturnCustomerId(string emailAddress)
        {
            if (String.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }

            if (!AFMDataUtilities.IsValidEmail(emailAddress))
            {
                throw new ArgumentException("Invalid email id", "emailAddress");
            }
            try
            {
                // Validate that there is a customer associated with the email address
                CustomerManager manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

                CrtDataModel.CustomerSearchCriteria criteria = new CrtDataModel.CustomerSearchCriteria() { Keyword = emailAddress };
                PagedResult<CrtDataModel.GlobalCustomer> customers = manager.SearchCustomers(criteria, new CrtDataModel.QueryResultSettings());

                if (customers.Results.Any())
                {
                    return customers.Results[0].AccountNumber;
                }

                return null;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "customer : " + emailAddress + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }

        /// <summary>
        /// Update an existing customer.
        /// </summary>
        /// <param name="customer">An existing customer.</param>
        public Customer UpdateCustomer(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException("customer");
            }
            try
            {
                var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

                CrtDataModel.Customer dmCustomer = manager.GetCustomer(customer.AccountNumber);
                dmCustomer = AFMCustomerMapper.UpdateDataModel(dmCustomer, customer);

                dmCustomer = manager.UpdateCustomer(dmCustomer);

                return CustomerMapper.ConvertToViewModel(dmCustomer);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "customer : " + customer.FirstName + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }

        /// <summary>
        /// Update a customer's addresses.
        /// </summary>
        /// <param name="accountNumber">The customer account number.</param>
        /// <param name="updatedAddresses">The address.</param>
        public Customer UpdateAddresses(string accountNumber, IEnumerable<Address> updatedAddresses)
        {
            if (String.IsNullOrEmpty(accountNumber))
            {
                throw new ArgumentNullException("accountNumber");
            }
            if (updatedAddresses.Count() == 0)
            {
                throw new ArgumentNullException("address");
            }


            if (!AFMDataUtilities.IsValidString(accountNumber))
            {
                throw new ArgumentException("Invalid account Number", "accountNumber");
            }

            Customer vmCustomer = null;
            try
            {
                var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
                CrtDataModel.Customer dmCustomer = manager.GetCustomer(accountNumber);

                if (dmCustomer == null)
                {
                    // Customer not found
                    return null;
                }

                dmCustomer = CustomerMapper.ConvertToDataModel(dmCustomer, updatedAddresses);

                dmCustomer = manager.UpdateCustomer(dmCustomer);
                vmCustomer = CustomerMapper.ConvertToViewModel(dmCustomer);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "accountNumberr : " + accountNumber + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
            return vmCustomer;
        }

        /// <summary>
        /// Update a customer's addresses.
        /// </summary>
        /// <param name="accountNumber">The customer account number.</param>
        /// <param name="updatedAddresses">The address.</param>
        public Customer UpdateAddresses(Customer customer, IEnumerable<Address> updatedAddresses)
        {
            if (customer == null)
            {
                throw new ArgumentException("Invalid account Number", "accountNumber");
            }
            if (updatedAddresses.Count() == 0)
            {
                throw new ArgumentNullException("address");
            }

            Customer vmCustomer = null;
            try
            {
                var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
                CrtDataModel.Customer dmCustomer = manager.GetCustomer(customer.AccountNumber);

                if (dmCustomer == null)
                {
                    // Customer not found
                    return null;
                }
                dmCustomer = CustomerMapper.UpdateDataModel(dmCustomer, customer);

                dmCustomer = CustomerMapper.ConvertToDataModel(dmCustomer, updatedAddresses);

                dmCustomer = manager.UpdateCustomer(dmCustomer);
                vmCustomer = CustomerMapper.ConvertToViewModel(dmCustomer);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "accountNumberr : " + customer.AccountNumber + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
            return vmCustomer;
        }

        /// <summary>
        /// Get a customer's addresses.
        /// </summary>
        /// <param name="accountNumber">The customer account number.</param>
        /// <returns>
        /// The customer's addresses.
        /// </returns>
        public Collection<Address> GetAddresses(string accountNumber)
        {
            if (String.IsNullOrEmpty(accountNumber))
            {
                throw new ArgumentNullException("accountNumber");
            }



            if (!AFMDataUtilities.IsValidString(accountNumber))
            {
                throw new ArgumentException("Invalid account Number", "accountNumber");
            }

            try
            {
                var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
                CrtDataModel.Customer dmCustomer = manager.GetCustomer(accountNumber);

                if (dmCustomer == null)
                {
                    // Customer not found
                    return null;
                }

                Collection<Address> allAddresses = CustomerMapper.ConvertToViewModel(dmCustomer.Addresses);

                return allAddresses;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "accountNumber : " + accountNumber + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }


        //CS Developed by spriya Dated 12/12/2014
        /// <summary>
        /// Get signed in customer's addresses based on IsShipping flag
        /// </summary>
        /// <param name="accountNumber">The customer account number.</param>
        /// <returns>
        /// The customer's addresses.
        /// </returns>
        public Collection<Address> GetShippingAddresses(string accountNumber, bool IsShipping)
        {
            if (String.IsNullOrEmpty(accountNumber))
            {
                throw new ArgumentNullException("accountNumber");
            }
            if (!AFMDataUtilities.IsValidString(accountNumber))
            {
                throw new ArgumentException("Invalid account Number", "accountNumber");
            }

            try
            {
                var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
                CrtDataModel.Customer dmCustomer = manager.GetCustomer(accountNumber);

                if (dmCustomer == null)
                {
                    // Customer not found
                    return null;
                }
                var shippingAddressesList = dmCustomer.Addresses.Where(p => p.AddressType == CrtDataModel.AddressType.Invoice).Take(1);
                if (IsShipping)
                {
                    shippingAddressesList = dmCustomer.Addresses.Where(p => p.AddressType == CrtDataModel.AddressType.Delivery).Take(4);
                    if (shippingAddressesList != null && shippingAddressesList.Count() > 1)
                        shippingAddressesList = shippingAddressesList.OrderByDescending(o => o.IsPrimary);                    
                }
                //v-hapat for  Bug :  64868 on 04/13/2015
                var usercartinfo = AFMDataUtilities.GetUserCartInfo();
                if (string.IsNullOrEmpty(usercartinfo.PaymentAddress.FirstName))
                {
                    usercartinfo.IsOptinDeals = CustomerMapper.ConvertToViewModel(dmCustomer).IsMarketingOptIn;
                    AFMDataUtilities.SetUserCartInfo(usercartinfo);
                }
                //v-hapat for  Bug :  64868 on 04/13/2015
                Collection<Address> shippingAddresses = CustomerMapper.ConvertToViewModel(shippingAddressesList);

                return shippingAddresses;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "accountNumber : " + accountNumber + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }
        //CE Developed by spriya

        /// <summary>
        /// Associate an existing customer to the current credentials if the criteria matches.
        /// </summary>
        /// <param name="email">The customer's e-mail address.</param>
        /// <param name="givenName">Name of the given.</param>
        /// <param name="surname">Name of the sur.</param>
        /// <param name="userId">The user identifier.</param>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Thrown when the customer cannot be found.</exception>
        public string AssociateCustomer(string email, string givenName, string surname, string userId)
        {
            if (String.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException("email");
            }


            if (String.IsNullOrEmpty(givenName))
            {
                throw new ArgumentNullException("givenName");
            }


            if (String.IsNullOrEmpty(surname))
            {
                throw new ArgumentNullException("surname");
            }


            if (String.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException("userId");
            }



            if (!AFMDataUtilities.IsValidEmail(email))
            {
                throw new ArgumentException("Invalid email id", "email");
            }
            if (!AFMDataUtilities.IsValidString(givenName))
            {
                throw new ArgumentException("Invalid given name", "givenName");
            }
            if (!AFMDataUtilities.IsValidString(surname))
            {
                throw new ArgumentException("Invalid surname", "surname");
            }
            if (!AFMDataUtilities.IsValidString(userId))
            {
                throw new ArgumentException("Invalid user id", "userId");
            }
            try
            {
                // get customer by email and loyalty card number
                var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());

                CrtDataModel.Customer dmCustomer;

                string firstName = !string.IsNullOrWhiteSpace(givenName) ? givenName : email;
                dmCustomer = new CrtDataModel.Customer()
                {
                    Email = email,
                    FirstName = firstName,
                    LastName = surname,
                    Language = CultureInfo.CurrentUICulture.ToString(),
                };

                new AFMCustomerController().EnsureDefaultAddress(dmCustomer);

                dmCustomer = manager.CreateCustomer(dmCustomer);

                new AFMCustomerController().CreateUserCustomerMap(dmCustomer, userId);

                return dmCustomer.AccountNumber;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "email : " + email + " givenName : " + givenName + " surName : " + surname + " userId : " + userId + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }

        //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
        /// <summary>
        /// Ensures there is a default address for the customer.
        /// </summary>
        /// <param name="dmCustomer">The customer data model.</param>
        protected void EnsureDefaultAddress(CrtDataModel.Customer dmCustomer)
        {
            if (dmCustomer == null)
            {
                throw new ArgumentNullException("Customer");
            }



            if (!dmCustomer.Addresses.Any())
            {
                // have to have one address field (country) otherwise AX won't save the address.
                CrtDataModel.Address address = new CrtDataModel.Address()
                {
                    AddressType = CrtDataModel.AddressType.Delivery,
                    IsPrimary = true,
                    Name = string.Format(CultureInfo.CurrentCulture, "{0} {1} {2}", dmCustomer.FirstName, dmCustomer.MiddleName, dmCustomer.LastName),
                    ThreeLetterISORegionName = "USA"
                };

                //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                address[CustomerConstants.AddressFirstName] = dmCustomer.FirstName;
                address[CustomerConstants.AddressLastName] = dmCustomer.LastName;
                //NE - RxL

                dmCustomer.Addresses = new List<CrtDataModel.Address>() { address };
            }
        }
        //NE - RxL

        /// <summary>
        /// Creates the user customer map.
        /// </summary>
        /// <param name="dmCustomer">The data model customer.</param>
        /// <param name="userId">The user identifier.</param>
        private void CreateUserCustomerMap(CrtDataModel.Customer dmCustomer, string userId)
        {
            if (dmCustomer == null)
            {
                throw new ArgumentNullException("Customer");
            }
            if (String.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException("userId");
            }
            if (!AFMDataUtilities.IsValidString(userId))
            {
                throw new ArgumentException("Invalid user id", "userId");
            }

            UserCustomerMap userCustomerMap = new UserCustomerMap()
            {
                CustomerId = dmCustomer.AccountNumber,
                CustomerVerifiedDateTime = DateTime.UtcNow,
                Status = UserCustomerMapStatus.Active,
                UserId = userId,
            };

            this.AuthorizationManager.CreateUserCustomerMap(userCustomerMap);
        }

        /// <summary>
        /// Gets the customer account number using the specified e-mail address.
        /// </summary>
        /// <param name="emailAddress">The e-mail address.</param>
        /// <returns>
        /// The customer identifier.
        /// </returns>
        public string GetRegisteredCustomerIdByEmailAddress(string emailAddress)
        {
            if (String.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }
            if (!AFMDataUtilities.IsValidEmail(emailAddress))
            {
                throw new ArgumentException("Invalid email id", "emailAddress");
            }
            try
            {
                string userId = string.Concat("f|%|", emailAddress);
                UserCustomerMap userCustomerMap;
                if (this.AuthorizationManager.TryReadCustomerMap(userId, out userCustomerMap))
                {
                    if (userCustomerMap.Status == UserCustomerMapStatus.Active)
                    {
                        return userCustomerMap.CustomerId;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "emailAddress : " + emailAddress + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Sends an email with an activation Url to the customer.
        /// </summary>
        /// <param name="customerId">The customer Id.</param>
        /// <param name="templateId">The email template Id.</param>
        /// <param name="activationUrl">The activation Url.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "templateId", Justification = "The parameter will be used in future.")]
        public void SendEmailToCustomer(string customerId, string templateId, string activationUrl)
        {
            if (String.IsNullOrEmpty(customerId))
            {
                throw new ArgumentNullException("customerId");
            }
            if (String.IsNullOrEmpty(templateId))
            {
                throw new ArgumentNullException("templateId");
            }
            if (String.IsNullOrEmpty(activationUrl))
            {
                throw new ArgumentNullException("activationUrl");
            }
            try
            {
                var properties = new Collection<CrtDataModel.NameValuePair>
            {
                new CrtDataModel.NameValuePair
                {
                    Name = "ActivationUrl",
                    Value = activationUrl,
                }
            };

                var manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
                manager.SendEmailToCustomer(customerId, ResetPasswordEmailTemplate, properties);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "customerId : " + customerId + " templateId : " + templateId + " activationUrl : " + activationUrl + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Sends email to the customer with the activation Url.
        /// </summary>
        /// <param name="emailAddress">Customer's email address.</param>
        /// <param name="emailTemplateId">Template Id of the email.</param>
        /// <param name="customerName">Customer's name.</param>
        /// <param name="activationUrl">The activation Url.</param>
        /// <param name="activationToken">The activation token.</param>
        /// <param name="storefrontName">The storefront name.</param>
        public override void SendAccountActivationEmailToCustomer(string emailAddress, string emailTemplateId, string customerName, string activationUrl, string activationToken, string storefrontName)
        {
            if (String.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }
            if (String.IsNullOrEmpty(emailTemplateId))
            {
                throw new ArgumentNullException("emailTemplateId");
            }
            if (String.IsNullOrEmpty(customerName))
            {
                throw new ArgumentNullException("customerName");
            }
            if (String.IsNullOrEmpty(activationUrl))
            {
                throw new ArgumentNullException("activationUrl");
            }
            if (String.IsNullOrEmpty(activationToken))
            {
                throw new ArgumentNullException("activationToken");
            }
            if (String.IsNullOrEmpty(storefrontName))
            {
                throw new ArgumentNullException("storefrontName");
            }
            try
            {
                var properties = new Collection<Microsoft.Dynamics.Commerce.Runtime.DataModel.NameValuePair>
            {
                new Microsoft.Dynamics.Commerce.Runtime.DataModel.NameValuePair
                {
                    Name = "customername",
                    Value = customerName
                },
                new Microsoft.Dynamics.Commerce.Runtime.DataModel.NameValuePair
                {
                    Name = "ActivationLink",
                    Value = activationUrl
                },
                new Microsoft.Dynamics.Commerce.Runtime.DataModel.NameValuePair
                {
                    Name = "CompanyName",
                    Value = storefrontName
                },
                new Microsoft.Dynamics.Commerce.Runtime.DataModel.NameValuePair
                {
                    Name = "ActivationToken",
                    Value = activationToken
                }                    
            };

                CustomerManager manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
                manager.SendAccountActivationEmailToCustomer(emailAddress, emailTemplateId, properties);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "eamilAddress : " + emailAddress + " emailtemplateId: " + emailTemplateId +
                       "customerName : " + customerName + "activactionUrl" + activationUrl +
                       "CompanyName" + storefrontName + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Checks that the activation token is valid given an email address.
        /// </summary>
        /// <param name="emailAddress">The user's email address.</param>
        /// <param name="activationToken">The activation token.</param>
        public override void ValidateAccountActivationRequest(string emailAddress, string activationToken)
        {
            if (String.IsNullOrEmpty(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }
            if (String.IsNullOrEmpty(activationToken))
            {
                throw new ArgumentNullException("activationToken");
            }
            try
            {
                CustomerManager manager = CustomerManager.Create(CrtUtilities.GetCommerceRuntime());
                manager.ValidateAccountActivationRequest(emailAddress, activationToken);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "emailAddres : " + emailAddress + " activationToken" + activationToken + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }
    }
}
