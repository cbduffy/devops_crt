﻿
namespace AFM.Commerce.Framework.Extensions.Controllers
{
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.ClientManager;
    using AFM.Commerce.Framework.Core.CRTServices;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Framework.Extensions.Mappers;
    using AFM.Commerce.Framework.Extensions.Utils;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Globalization;
    using System.Linq;
    using System.Xml;
    using System.Xml.Serialization;
    using System.Xml.XPath;
    using ViewModel = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    public class AFMShoppingCartController : ShoppingCartController
    {

        private const string PropertyKeyImage = "Image";

        /// <summary>
        /// Represents the term after which a authenticated users' cart expires 
        /// </summary>
        private const string ShoppingCartExpiryTermPropertyName = "StoreFront_ShoppingCartExpiryTerm";

        /// <summary>
        /// Gets the shopping cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null or empty.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} was not found.</exception>
        public ShoppingCart GetShoppingCart(string shoppingCartId, string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine, bool includeTax, bool calculateShippingCharge = true)
        {

            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                OrderManager manager = OrderManager.Create(runtime);
                // HXM To improve performance changing calculation mode to None in this call 
                //Cart cart = manager.GetCart(shoppingCartId, customerId, CalculationModes.All);
                Cart cart = manager.GetCart(shoppingCartId, customerId, CalculationModes.None);

                ShoppingCart shoppingCart;

                if (cart == null)
                {
                    throw new DataValidationException(DataValidationErrors.CartNotFound, Resource.ShoppingCartNotFound, shoppingCartId);
                }               
                /*else if (cart.CartLines != null && cart.CartLines.Count > 0 && userCartData != null && userCartData.RemoveCartDiscount == true)
                {
                    cart[CartConstant.RemoveDiscount] = true;
                    cart.DiscountCodes.Clear();
                    cart = manager.CreateOrUpdateCart(cart, CalculationModes.All);
                }*/
                //NS Hardik Bug : 80575 : Barstool Problem
                if (string.IsNullOrEmpty(Convert.ToString(cart[CartConstant.AFMReasonCodeSplit])))
                {
                    cart[CartConstant.AFMReasonCodeSplit] = AFMDataUtilities.GetReasonCodeSplit().InforCodeId;
                    cart[CartConstant.AFMReasonCodeSplitDescription] = AFMDataUtilities.GetReasonCodeSplit().InforCodeDescription;
                    cart = manager.CreateOrUpdateCart(cart, CalculationModes.None);
                }
                //NS Hardik Bug : 80575 : Barstool Problem

                //NE by spriya : DMND0010113 - Synchrony Financing online

                //CS by Hardik Add to Cart taken care in Add item method by shri on new bug
                //string zipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
                //AFMVendorAffiliation afmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(zipCode);
                //cart.AffiliationLines.Clear();
                //cart.AffiliationLines.Add(new AffiliationLoyaltyTier { AffiliationId = afmVendorAffiliation.AffiliationId, AffiliationType = RetailAffiliationType.General });
                //cart = manager.CreateOrUpdateCart(cart);
                //CS by Hardik 
                string zipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
                if (!string.IsNullOrEmpty(zipCode) && Convert.ToString(cart[CartConstant.CartZipCode]) != zipCode)
                {
                    UpdateZipCodeAffiliationtoCart(ref cart);
                }

                AFMUserCartData userCartData = AFMDataUtilities.GetUserCartInfo();
                //NS spriya Synchrony online
                //NS by spriya : DMND0010113 - Synchrony Financing online
                bool discountFlag = false;
                if (cart.CartType == Microsoft.Dynamics.Commerce.Runtime.DataModel.CartType.Checkout)
                {
                    if (cart.CartLines != null && cart.CartLines.Count > 0 && userCartData != null && userCartData.RemoveCartDiscount == true)
                    {
                        cart[CartConstant.FinancingOptionId] = userCartData.FinanceOptionId;
                        cart[CartConstant.RemoveDiscount] = true;
                        var discountcode = new Collection<string>();
                        discountcode.AddRange(cart.DiscountCodes);
                        if (cart.DiscountCodes.Count != 0)
                            manager.RemoveDiscountCodesFromCart(AFMDataUtilities.GetShoppingCartId(), customerId, discountcode);
                        cart.DiscountCodes.Clear();
                        cart = manager.CreateOrUpdateCart(cart, CalculationModes.All);
             
                    }
                }

                if (dataLevel == ShoppingCartDataLevel.All)
                {
                    shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                }
                else
                {
                    shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, ShoppingCartDataLevel.All, productValidator, includeTax);
                }

                //cart = AddEstShippingCostToCart(shoppingCart, cart, customerId, includeTax);
                // CS by muthait dated 01Nov2014
                //CalculateShippingCharges(shoppingCart, cart, includeTax);//Comment for Performence  Change to item To Cart by hardik
                if (!shoppingCart.Items.IsNullOrEmpty() && calculateShippingCharge)
                {
                    cart = AddEstShippingCostToCart(shoppingCart, cart, customerId, includeTax);//Add Line for Performence  Change to item To Cart by hardik
                    shoppingCart.Items.Clear();
                    shoppingCart.Items.AddRange((new AFMShoppingCartMapper(new SiteConfiguration())).ConvertToViewModel(cart.CartLines.AsEnumerable(), ShoppingCartDataLevel.All, productValidator, zipCode).AsEnumerable());
                }
                // CE by muthait dated 01Nov2014 
                AddATPforEcommerce(shoppingCart, cart);

                if (cart[CartConstant.AvaTaxError] != null && includeTax)
                {
                    if (!string.IsNullOrEmpty(cart[CartConstant.AvaTaxError].ToString()))
                        shoppingCart.AvalaraTaxError = cart[CartConstant.AvaTaxError].ToString();
                }

                foreach (TransactionItem item in shoppingCart.Items)
                {
                    if (item.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                    {
                        if (item.AFMCartLineATP != null)
                        {
                            if (!item.AFMCartLineATP.IsAvailable)
                                shoppingCart.ATPError = CartConstant.AtpItemNotAvailableMrssage;
                        }
                        else
                        {
                            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassATP"]))
                                shoppingCart.ATPError = CartConstant.AtpServiceNotAvailableErrorMrssage;
                        }
                    }
                }

                if (cart[CartConstant.ChargeTypeError] != null && !string.IsNullOrEmpty(cart[CartConstant.ChargeTypeError].ToString()))
                {
                    shoppingCart.ChargeTypeError = cart[CartConstant.ChargeTypeError].ToString();
                }
                var ServiceItem = shoppingCart.Items.Where(p => p.AFMItemType == Convert.ToInt32(AFMItemType.DeliveryServiceItem)).ToList();
                if ((ServiceItem == null || ServiceItem.Count == 0) && (shoppingCart.Items != null && shoppingCart.Items.Count > 0))
                    shoppingCart.ChargeTypeError = CartConstant.AtpItemNotAvailableMrssage;

                return shoppingCart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCartId + " CustomerId: " + customerId + " ShoppinCartDataLevel: " + dataLevel + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        public List<AFM.Commerce.Entities.AFMSynchronyOptions> UpdateSynchronyOptions(string shoppingCartId, Cart cart)
        {
            try
            {
                List<AFM.Commerce.Entities.AFMSynchronyOptions> synchronyOptionList = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(AFMDataUtilities.GetSynchronyOptions(), cart.TotalAmount, cart.SubtotalAmount, cart.DiscountAmount);
                return synchronyOptionList;
            }

            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                            ex.Message + Environment.NewLine +
                                            ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Adds the items to the shopping cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="listings">The listings.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>A shopping cart.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when listings is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} was not found.</exception>
        public ShoppingCart AddItems(string shoppingCartId, string customerId,
            IEnumerable<Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing> itemListings, ShoppingCartDataLevel dataLevel, IProductValidator productValidator,
            ISearchEngine searchEngine, bool includeTax, bool isMergedCart = false)
        {
            if (itemListings == null)
            {
                throw new ArgumentNullException("listings");
            }
            try
            {
                Collection<ViewModel.Listing> listings = new Collection<Listing>();
                if (!isMergedCart)
                {
                    listings = GetProductListings(itemListings.ToList()[0]);
                }
                else
                {
                    foreach (var item in itemListings)
                    {
                        listings.Add(item);
                    }
                }
                // N CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
                listings = GetListingsOrderSettings(listings);


                //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
                bool isKitComponents = false;
                foreach (var listing in listings)
                {
                    if (listing.AFMKitProductId > 0)
                    {
                        isKitComponents = true;
                        break;
                    }
                }
                //NE - RxL

                ShoppingCart shoppingCart = null;

                if (!string.IsNullOrWhiteSpace(customerId) || !string.IsNullOrEmpty(customerId))
                {
                    //Always get the active shopping cart before adding items for an authenticated user.
                    shoppingCart = GetActiveShoppingCart(customerId, dataLevel, productValidator, searchEngine);
                    // if active shopping cart does not exist create an new cart Id.
                    shoppingCartId = shoppingCart == null ? string.Empty : shoppingCart.CartId;
                }

                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                Cart cart = null;
                if (string.IsNullOrWhiteSpace(shoppingCartId) || string.IsNullOrEmpty(shoppingCartId))
                {
                    shoppingCartId = TokenHelper.GenerateTransactionId();

                    //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
                    cart = new Cart() { Id = shoppingCartId, CustomerId = customerId };

                    cart[CartConstant.AFMKitSequenceNumber] = 1;
                    //NE - RxL

                    UpdateZipCodeAffiliationtoCart(ref cart);
                }

                //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
                if (isKitComponents)
                {
                    if (null == cart)
                    {
                        cart = manager.GetCart(shoppingCartId, CalculationModes.None);
                    }

                    int kitSequenceNumber = Convert.ToInt32(cart[CartConstant.AFMKitSequenceNumber]);
                    if (kitSequenceNumber < 1)
                        kitSequenceNumber = 1;


                    foreach (var listing in listings)
                    {
                        listing.AFMKitSequenceNumber += kitSequenceNumber;
                    }

                    kitSequenceNumber = listings.Max(d => d.AFMKitSequenceNumber);
                    kitSequenceNumber++;
                    cart[CartConstant.AFMKitSequenceNumber] = kitSequenceNumber;

                    manager.CreateOrUpdateCart(cart, CalculationModes.None);
                }

                //NE -RxL

                CalculationModes modes = CalculationModes.Prices | CalculationModes.Discounts | CalculationModes.Totals;
                ICollection<CartLine> cartLines = new AFMShoppingCartMapper(new SiteConfiguration()).GetCartLinesFromListing(listings, productValidator);

                //NS -Performance- Add Ecom and Vendor Detail in Line
                UpdateEcomstatusCartLine(ref cartLines);
                UpdateATPDataCartLine(ref cartLines);
                //NE -Performance- Add Ecom and Vendor Detail in Line

                cart = manager.AddCartLines(shoppingCartId, customerId, cartLines, modes);

                if (cart == null)
                {
                    throw new DataValidationException(DataValidationErrors.CartNotFound,
                        Resource.ShoppingCartNotFound,
                        shoppingCartId);
                }

                if (dataLevel == ShoppingCartDataLevel.All)
                {
                    shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                }
                else
                {
                    shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, false);
                }

                CalculateCartCount(shoppingCart);
                //Removed : ATP call Add Item Performance : Hardik
                //AddATPforEcommerce(shoppingCart, cart);

                //Removed : AddEstimating ShippingCast to Cart  : Hardik
                //NS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 
                //NE Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 

                if (cart[CartConstant.AvaTaxError] != null && includeTax)
                {
                    if (!string.IsNullOrEmpty(cart[CartConstant.AvaTaxError].ToString()))
                        shoppingCart.AvalaraTaxError = cart[CartConstant.AvaTaxError].ToString();
                }
                //Comment for Performance : Hardik
                //foreach (TransactionItem item in shoppingCart.Items)
                //{
                //    if (item.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                //    {
                //        if (item.AFMCartLineATP != null)
                //        {
                //            if (!item.AFMCartLineATP.IsAvailable)
                //                //shoppingCart.ATPError = string.IsNullOrEmpty(shoppingCart.ATPError) ? item.AFMCartLineATP.ItemId + " : " + item.AFMCartLineATP.Message + Environment.NewLine + "Available Quantity is :" + item.AFMCartLineATP.Quantity :
                //                //shoppingCart.ATPError + Environment.NewLine + item.AFMCartLineATP.ItemId + " : " + item.AFMCartLineATP.Message + Environment.NewLine + "Available Quantity is :" + item.AFMCartLineATP.Quantity;
                //                shoppingCart.ATPError = "Some item(s) you have selected are no longer available or prices have changed. These unavailable products are noted with red buttons. To proceed to checkout, please remove these unavailable items from your shopping cart, or add them to your wish list";
                //        }
                //        else
                //        {
                //            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassATP"]))
                //                shoppingCart.ATPError = "Something went wrong on product availability verification. Try again!!";
                //        }
                //    }
                //}
                //Comment for Performance : Hardik
                return shoppingCart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCartId + " CustomerId: " + customerId + " ShoppinCartDataLevel: " + dataLevel + " IncludeTax:" + includeTax + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        #region Add item performance

        //Add new method for Add Item Perforamnce : Hardik Patel
        /// <summary>
        /// Update Affiliaion Based on ZipCode to Cart
        /// </summary>
        /// <param name="cart"></param>
        public void UpdateZipCodeAffiliationtoCart(ref Cart cart)
        {
            //Crate Or Update Cart Based wiht affiliation 
            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            string zipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
            AFMVendorAffiliation afmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(zipCode);
            cart.AffiliationLines = new List<AffiliationLoyaltyTier>();
            cart.AffiliationLines.Add(new AffiliationLoyaltyTier { AffiliationId = afmVendorAffiliation.AffiliationId, AffiliationType = RetailAffiliationType.General });
            cart[CartConstant.CartZipCode] = zipCode;
            cart = manager.CreateOrUpdateCart(cart, CalculationModes.None);
        }

        //Add New Method for add item performance : Hardik Patel
        /// <summary>
        /// Update Ecomm Status to Cart Line
        /// </summary>
        /// <param name="cartLines"></param>
        private void UpdateEcomstatusCartLine(ref ICollection<CartLine> cartLines)
        {
            //Update Item Ecomm status to CartLine List for Update on Cart when you add item only
            AFMVendorDetailsService vendorDetailService = new AFMVendorDetailsService();
            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();

            GetItemEcomDetailServiceRequest vendorDetailRequest =
              new GetItemEcomDetailServiceRequest(cartLines.Select(temp => temp.ProductId).Distinct().AsEnumerable());
            var reqContext = new RequestContext(runtime);
            GetVendorDetailsServiceResponse vendorDetailResponse = runtime.Execute<GetVendorDetailsServiceResponse>(
                    vendorDetailRequest, reqContext,
                    true);
            foreach (var cartline in cartLines)
            {
                var itemflag = vendorDetailResponse.ProductFlagDetail.FirstOrDefault(temp => temp.ItemId == cartline.ProductId);
                if (itemflag != null)
                {
                    cartline.LineData[CartConstant.IsEcommOwned] = itemflag.IsEcommOwned;
                    cartline.LineData[CartConstant.IsSDSO] = itemflag.IsSDSO;
                }
                else
                {
                    cartline.LineData[CartConstant.IsEcommOwned] = 1;
                    cartline.LineData[CartConstant.IsSDSO] = 0;
                }
            }
        }

        //Add New Method for add item performance : Hardik Patel
        /// <summary>
        /// Update ATP Detail to Cart Line
        /// </summary>
        /// <param name="cartLines"></param>
        public void UpdateATPDataCartLine(ref ICollection<CartLine> cartLines)
        {
            //Update AFI and AFM Vendor based on Item Status.
            var afmvednorDetail = AFMDataUtilities.getZipCodeVendorDetail(AFMDataUtilities.GetAFMZipCodeFromCookie());
            var afivednorDetail = AFMDataUtilities.getAfiVendorDetail();
            foreach (var cartItem in cartLines)
            {
                if (!Convert.ToBoolean(cartItem.LineData[CartConstant.IsEcommOwned]))
                {
                    if (afmvednorDetail != null)
                    {
                        cartItem.LineData[CartConstant.AFMVendorId] = afmvednorDetail.AFMVendorId;
                        cartItem.LineData[CartConstant.AFMVendorName] = afmvednorDetail.AFMVendorName;
                        cartItem.LineData[CartConstant.InvoiceVendorId] = afmvednorDetail.InvoiceVendorId;
                        cartItem.LineData[CartConstant.ShipToId] = afmvednorDetail.ShipToId;
                    }
                    else
                    {
                        cartItem.LineData[CartConstant.AFMVendorId] = string.Empty;
                        cartItem.LineData[CartConstant.AFMVendorName] = string.Empty;
                        cartItem.LineData[CartConstant.InvoiceVendorId] = string.Empty;
                        cartItem.LineData[CartConstant.ShipToId] = string.Empty;
                    }
                }
                else
                {
                    cartItem.LineData[CartConstant.AFMVendorId] = afivednorDetail.AFIVendorId;
                    cartItem.LineData[CartConstant.AFMVendorName] = afivednorDetail.AFIVendorName;
                    cartItem.LineData[CartConstant.InvoiceVendorId] = afivednorDetail.InvoiceVendorId;
                    cartItem.LineData[CartConstant.ShipToId] = afivednorDetail.ShipToId;
                }
            }
        }
        #endregion

        /// <summary>
        /// Removes the items from the shopping cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="lineIds">The line ids.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// A shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} was not found.</exception>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null.</exception>
        public ShoppingCart RemoveItems(string shoppingCartId, string customerId, IEnumerable<string> lineIds, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine, bool includeTax)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }
            if (lineIds == null)
            {
                throw new ArgumentNullException("lineIds");
            }
            try
            {
                CalculationModes modes = CalculationModes.Prices | CalculationModes.Discounts | CalculationModes.Totals;
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

                Cart cart = manager.GetCart(shoppingCartId, customerId, CalculationModes.None);
                //comment Hardik Add Item performance
                //var lineids = new List<string>();
                //foreach (var item in cart.CartLines)
                //{
                //    if (item.LineData["AFMItemType"] != null && (int)item.LineData["AFMItemType"] == (int)AFMItemType.DeliveryServiceItem)
                //        lineids.Add(item.LineId);
                //}
                //lineids.AddRange(lineIds);
                //comment Hardik Add Item performance
                if (lineIds.Count() == cart.CartLines.Where(p => AFMItemType.DeliveryServiceItem != (AFMItemType)Enum.Parse(typeof(AFMItemType), Convert.ToString(p.LineData["AFMItemType"]))).Count())
                {
                    lineIds = cart.CartLines.Select(t => t.LineId);
                }
                cart = manager.DeleteCartLines(shoppingCartId, customerId, lineIds, modes);

                if (cart == null)
                {
                    throw new DataValidationException(DataValidationErrors.CartNotFound, Resource.ShoppingCartNotFound, shoppingCartId);
                }

                ShoppingCart shoppingCart;
                if (dataLevel == ShoppingCartDataLevel.All)
                {
                    shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                }
                else
                {
                    shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, includeTax);
                }
                // Cs by muthait dated 16Nov2014
                if (shoppingCart == null || shoppingCart.Items.Count == 0)
                    return null;

                AddATPforEcommerce(shoppingCart, cart);

                //NS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 
                cart = AddEstShippingCostToCart(shoppingCart, cart, customerId, includeTax);
                //NE Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 
                if (cart[CartConstant.AvaTaxError] != null && includeTax)
                {
                    if (!string.IsNullOrEmpty(cart[CartConstant.AvaTaxError].ToString()))
                        shoppingCart.AvalaraTaxError = cart[CartConstant.AvaTaxError].ToString();
                }

                foreach (TransactionItem item in shoppingCart.Items)
                {
                    if (item.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                    {
                        if (item.AFMCartLineATP != null)
                        {
                            if (!item.AFMCartLineATP.IsAvailable)
                                //shoppingCart.ATPError = string.IsNullOrEmpty(shoppingCart.ATPError) ? item.AFMCartLineATP.ItemId + " : " + item.AFMCartLineATP.Message + Environment.NewLine + "Available Quantity is :" + item.AFMCartLineATP.Quantity :
                                //shoppingCart.ATPError + Environment.NewLine + item.AFMCartLineATP.ItemId + " : " + item.AFMCartLineATP.Message + Environment.NewLine + "Available Quantity is :" + item.AFMCartLineATP.Quantity;
                                shoppingCart.ATPError = CartConstant.AtpItemNotAvailableMrssage;
                        }
                        else
                        {
                            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassATP"]))
                                shoppingCart.ATPError = CartConstant.AtpServiceNotAvailableErrorMrssage;
                        }
                    }
                }
                return shoppingCart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCartId + " CustomerId: " + customerId + " ShoppinCartDataLevel: " + dataLevel + " IncludeTax:" + includeTax + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        //TO DO spriya NS merge for upgrade
        public void SetCartAffiliationLoyaltyTiers(string cartId, IEnumerable<long> affiliationLoyaltyTierIds)
        {
            if (string.IsNullOrWhiteSpace(cartId))
            {
                throw new ArgumentNullException("cartId");
            }
            if (affiliationLoyaltyTierIds.Count() < 0)
            {
                throw new ArgumentNullException("affiliationLoyaltyTierIds");
            }
            try
            {
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                Cart cart = manager.GetCart(cartId, CalculationModes.None);
                cart.AffiliationLines.Clear();

                IList<AffiliationLoyaltyTier> tiers = new List<AffiliationLoyaltyTier>();
                foreach (long affiliationLoyaltyTierId in affiliationLoyaltyTierIds)
                {
                    cart.AffiliationLines.Add(new AffiliationLoyaltyTier { AffiliationId = affiliationLoyaltyTierId, AffiliationType = RetailAffiliationType.General });
                }

                manager.CreateOrUpdateCart(cart);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "CartId " + cartId + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }
        //NE Merge for upgrade

        /// <summary>
        /// Updates the items.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="items">The items.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when items is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">
        /// Shopping cart {0} was not found.
        /// </exception>
        public ShoppingCart UpdateItems(string shoppingCartId, string customerId, IEnumerable<TransactionItem> items, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine, bool includeTax)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (items == null)
            {
                throw new ArgumentNullException("items");
            }
            try
            {
                // TODO: Bug 893642:The price field is not nullable on the cartline and so it does not preserve the value if not explicity set
                // so we need to fetch the cart and update the cartline
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                Cart currentCart = manager.GetCart(shoppingCartId, customerId, CalculationModes.None);
                List<CartLine> cartLineToUpdate = new List<CartLine>();
                foreach (TransactionItem i in items)
                {
                    if (i.Quantity <= 0)
                    {
                        string message = string.Format(Resource.QuantityOfItemMustBePositive, i.LineId, i.ProductId, i.Quantity);
                        throw new DataValidationException(DataValidationErrors.InvalidQuantity, message);
                    }

                    CartLine cartLine = currentCart.CartLines.Where(l => l.LineId == i.LineId).Single();
                    if (cartLine != null)
                    {
                        cartLine.Quantity = i.Quantity;
                        //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
                        cartLine.LineData["AFMKitItemQuantity"] = i.AFMKitItemQuantity;
                        //NE - RxL
                        cartLineToUpdate.Add(cartLine);
                    }
                }

                Cart cart = manager.UpdateCartLines(shoppingCartId, customerId, cartLineToUpdate, CalculationModes.All);

                if (cart == null)
                {
                    throw new DataValidationException(DataValidationErrors.CartNotFound, Resource.ShoppingCartNotFound, shoppingCartId);
                }

                ShoppingCart shoppingCart;
                if (dataLevel == ShoppingCartDataLevel.All)
                {
                    shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                }
                else
                {
                    shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, false);
                }

                AddATPforEcommerce(shoppingCart, cart);

                //NS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 
                cart = AddEstShippingCostToCart(shoppingCart, cart, customerId, includeTax);
                //NE Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 

                if (cart[CartConstant.AvaTaxError] != null && includeTax)
                {
                    if (!string.IsNullOrEmpty(cart[CartConstant.AvaTaxError].ToString()))
                        shoppingCart.AvalaraTaxError = cart[CartConstant.AvaTaxError].ToString();
                }

                foreach (TransactionItem item in shoppingCart.Items)
                {
                    if (item.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                    {
                        if (item.AFMCartLineATP != null)
                        {
                            if (!item.AFMCartLineATP.IsAvailable)
                                //shoppingCart.ATPError = string.IsNullOrEmpty(shoppingCart.ATPError) ? item.AFMCartLineATP.ItemId + " : " + item.AFMCartLineATP.Message + Environment.NewLine + "Available Quantity is :" + item.AFMCartLineATP.Quantity :
                                //shoppingCart.ATPError + Environment.NewLine + item.AFMCartLineATP.ItemId + " : " + item.AFMCartLineATP.Message + Environment.NewLine + "Available Quantity is :" + item.AFMCartLineATP.Quantity;
                                shoppingCart.ATPError = CartConstant.AtpItemNotAvailableMrssage;
                        }
                        else
                        {
                            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassATP"]))
                                shoppingCart.ATPError = CartConstant.AtpServiceNotAvailableErrorMrssage;
                        }
                    }
                }
                return shoppingCart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCartId + " CustomerId: " + customerId + " ShoppinCartDataLevel: " + dataLevel + " IncludeTax:" + includeTax + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Moves the items between carts.
        /// </summary>
        /// <param name="sourceShoppingCartId">The source shopping cart identifier.</param>
        /// <param name="destinationShoppingCartId">The destination shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">The data level.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// sourceShoppingCartId
        /// or
        /// destinationShoppingCartId
        /// </exception>
        public ShoppingCart MoveItemsBetweenCarts(string sourceShoppingCartId, string destinationShoppingCartId, string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(sourceShoppingCartId))
            {
                throw new ArgumentNullException("sourceShoppingCartId");
            }

            //if (string.IsNullOrWhiteSpace(destinationShoppingCartId))
            //{
            //    throw new ArgumentNullException("destinationShoppingCartId");
            //}
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                OrderManager manager = OrderManager.Create(runtime);

                Cart cart = manager.GetCart(sourceShoppingCartId, string.Empty, CalculationModes.None);

                ShoppingCart sourceShoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, false);

                if (sourceShoppingCart != null && sourceShoppingCart.Items.Any())
                {
                    var listingsToMove = new AFMShoppingCartMapper(new SiteConfiguration()).GetListingsFromCart(sourceShoppingCart);
                    ShoppingCart destinationShoppingCart = AddItems(destinationShoppingCartId, customerId, listingsToMove, dataLevel, productValidator, searchEngine, true, true);

                    //Adding Promo code guest Cart to Sigin Cart
                    if (sourceShoppingCart.DiscountCodes != null)
                    {
                        if (!sourceShoppingCart.DiscountCodes.IsNullOrEmpty())
                        {
                            cart = manager.AddDiscountCodesToCart(destinationShoppingCart.CartId, sourceShoppingCart.DiscountCodes);
                            destinationShoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, false);
                        }
                    }
                    //Edn Adding Promo Code

                    // Line ids to delete from source.
                    IEnumerable<string> lineIdsToDelete = sourceShoppingCart.Items.Select(i => i.LineId);
                    sourceShoppingCart = RemoveItems(sourceShoppingCartId, string.Empty, lineIdsToDelete, dataLevel, productValidator, searchEngine, true);

                    return destinationShoppingCart;
                }
                else
                {
                    if (!string.IsNullOrEmpty(destinationShoppingCartId))
                    {
                        ShoppingCart destinationShoppingCart = GetShoppingCart(destinationShoppingCartId, customerId, dataLevel, productValidator, searchEngine, true);
                        return destinationShoppingCart;
                    }
                    else
                        return null;
                }
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "sourceShoppingCartId " + sourceShoppingCartId + " destinationShoppingCartId: " + destinationShoppingCartId + " CustomerId: " + customerId + " ShoppinCartDataLevel: " + dataLevel + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Get all the promotions for the items in the cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The shopping cart.</returns>
        public ShoppingCart GetAllPromotionsForShoppingCart(string shoppingCartId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine, bool isCheckoutSession)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                OrderManager manager = OrderManager.Create(runtime);

                Cart cart = manager.GetPromotionsForCart(shoppingCartId);

                if (cart == null)
                {
                    throw new DataValidationException(DataValidationErrors.CartNotFound, Resource.ShoppingCartNotFound, shoppingCartId);
                }

                ShoppingCart shoppingCart;
                if (dataLevel == ShoppingCartDataLevel.All)
                {
                    shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                }
                else
                {
                    shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, isCheckoutSession);
                }

                AddATPforEcommerce(shoppingCart, cart);
                // CS by muthait dated 01Nov2014
                CalculateShippingCharges(shoppingCart, cart, isCheckoutSession);
                // CE by muthait dated 01Nov2014

                return shoppingCart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCartId + " ShoppinCartDataLevel: " + dataLevel + " isCheckoutSession:" + isCheckoutSession + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Starts the checkout process by creating a Clone of the shoping cart to commence cart and returning it.
        /// </summary>
        /// <param name="shoppingCartId">The current cart id to be used for the checkout process.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="previousCheckoutCartId">The identifier for any previous checkout cart that was abandoned.</param>
        /// <param name="productValidator">The product validator.</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// A new cart with a random cart id that should be used during the secure checkout process.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when shoppingCartId is null.</exception>
        public ShoppingCart AFMCommenceCheckout(string shoppingCartId, string customerId, string previousCheckoutCartId, IProductValidator productValidator, ISearchEngine searchEngine, ShoppingCartDataLevel dataLevel)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }
            try
            {
                //NS by muthait dataed 02Jan2015 - Clone ShoppingCart to Commence Cart
                var checkoutCartId = CreateCommenceCheckoutCart(shoppingCartId);
                while (string.IsNullOrEmpty(checkoutCartId))
                {
                    checkoutCartId = CreateCommenceCheckoutCart(shoppingCartId);
                };

                ShoppingCart shoppingCart;
                Cart cart;
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                cart = manager.GetCart(checkoutCartId, customerId, CalculationModes.None);
                cart.Id = checkoutCartId;
                cart = manager.CreateOrUpdateCart(cart);
                if (dataLevel == ShoppingCartDataLevel.All)
                {
                    shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                }
                else
                {
                    shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, true);
                }

                // Delete any previous checkout cart.
                if (!string.IsNullOrWhiteSpace(previousCheckoutCartId))
                {
                    try
                    {
                        manager.DeleteCarts(customerId, new string[] { previousCheckoutCartId });
                    }
                    catch (CommerceRuntimeException e)
                    {
                        NetTracer.Warning(e, "ShoppingCartController::AFMCommenceCheckout - Failed to delete a previous checkout cart with id {0}.", previousCheckoutCartId);
                    }
                }
                CalculateShippingCharges(shoppingCart, cart, false);
                return shoppingCart;
                //NE by muthait dataed 02Jan2015 
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCartId + "previouscheckout: " + previousCheckoutCartId + " CustomerId: " + customerId + " ShoppinCartDataLevel: " + dataLevel + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Starts the checkout process by creating a secure cart and returning it.
        /// </summary>
        /// <param name="shoppingCartId">The current cart id to be used for the checkout process.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="previousCheckoutCartId">The identifier for any previous checkout cart that was abandoned.</param>
        /// <param name="productValidator">The product validator.</param>
        /// <param name="searchEngine">The search engine.</param>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>
        /// A new cart with a random cart id that should be used during the secure checkout process.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when shoppingCartId is null.</exception>
        public ShoppingCart CommenceCheckout(string shoppingCartId, string customerId, string previousCheckoutCartId, IProductValidator productValidator, ISearchEngine searchEngine, ShoppingCartDataLevel dataLevel)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }
            try
            {
                // Get the saved cart
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                Cart shoppingCart = manager.GetCart(shoppingCartId, customerId, CalculationModes.None);

                string checkoutCartId = TokenHelper.GenerateTransactionId();
                Cart checkoutCart = new Cart { Id = checkoutCartId, CustomerId = customerId, CartType = Microsoft.Dynamics.Commerce.Runtime.DataModel.CartType.Checkout };
                //NS developed by v-hapat on dated 20/11/2014 Adding Affiliaction from shopping Cart to checkout Cart
                var affiliationList = new List<AffiliationLoyaltyTier>();
                affiliationList.AddRange(shoppingCart.AffiliationLines);
                checkoutCart.AffiliationLines = affiliationList;
                //NE developed by v-hapat on dated 20/11/2014 Adding Affiliaction from shopping Cart to checkout Cart
                checkoutCart = manager.CreateOrUpdateCart(checkoutCart, CalculationModes.None);
                List<CartLine> cartLinetoAdd = new List<CartLine>();
                foreach (var item in shoppingCart.CartLines)
                {
                    if ((int)item.LineData["AFMItemType"] != (int)AFMItemType.DeliveryServiceItem)
                        cartLinetoAdd.Add(item);
                }
                // Add the cart lines from the shopping cart to checkout cart such that the original cart line ids are retained.
                checkoutCart = manager.AddCartLines(checkoutCartId, checkoutCart.CustomerId, cartLinetoAdd.AsEnumerable());

                //Add DiccountCode to Cart retail dicount
                if (shoppingCart.DiscountCodes != null && shoppingCart.DiscountCodes.Count != 0)
                    checkoutCart = manager.AddDiscountCodesToCart(checkoutCartId, customerId, new Collection<string>(shoppingCart.DiscountCodes.ToList()));


                checkoutCart = shoppingCart.Clone<Cart>();
                var cartLineData = checkoutCart.CartLines.Where(p => (int)p.LineData["AFMItemType"] != (int)AFMItemType.DeliveryServiceItem).ToList();
                checkoutCart.CartLines.Clear();
                foreach (var item in cartLineData)
                {
                    checkoutCart.CartLines.Add(item);
                }
                checkoutCart.Id = checkoutCartId;

                ShoppingCart checkoutCartVM;
                if (dataLevel == ShoppingCartDataLevel.All)
                {
                    checkoutCartVM = AddKitDetailsToShoppingCart(checkoutCart, productValidator, searchEngine);
                }
                else
                {
                    checkoutCartVM = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(checkoutCart, dataLevel, productValidator, true);
                }
                // Delete any previous checkout cart.
                if (!string.IsNullOrWhiteSpace(previousCheckoutCartId))
                {
                    try
                    {
                        manager.DeleteCarts(customerId, new string[] { previousCheckoutCartId });
                    }
                    catch (CommerceRuntimeException e)
                    {
                        NetTracer.Warning(e, "ShoppingCartController::CommenceCheckout - Failed to delete a previous checkout cart with id {0}.", previousCheckoutCartId);
                    }
                }

                // CS by muthait dated 01Nov2014
                checkoutCart = AddEstShippingCostToCart(checkoutCartVM, checkoutCart, customerId, false);
                // CE by muthait dated 01Nov2014

                return checkoutCartVM;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCartId + "previouscheckout: " + previousCheckoutCartId + " CustomerId: " + customerId + " ShoppinCartDataLevel: " + dataLevel + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        public string CreateCommenceCheckoutCart(string shoppingCartId)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }
            string checkoutCartId = TokenHelper.GenerateTransactionId();
            var cartManager = AFMCartManager.Create(CrtUtilities.GetCommerceRuntime());
            var response = cartManager.CloneCartData(checkoutCartId, shoppingCartId);
            return response;
        }

        /// <summary>
        /// Adds or Removes the discount codes from the cart.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="promotionCode">The promotion code.</param>
        /// <param name="isAdd">Indicates whether the operation is addition or removal of discount codes.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <param name="productValidator">Product validator.</param>
        /// <param name="searchEngine">Search engine.</param>
        /// <returns>
        /// A shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when shoppingCartId or promotionCode is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} was not found.</exception>
        public ShoppingCart AddOrRemovePromotionCode(string shoppingCartId, string customerId, string promotionCode, bool isAdd, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (string.IsNullOrWhiteSpace(promotionCode))
            {
                throw new ArgumentNullException("promotionCode");
            }
            try
            {
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                Cart cart;

                if (isAdd)
                {
                    cart = manager.AddDiscountCodesToCart(shoppingCartId, customerId, new Collection<string>() { promotionCode });
                }
                else
                {
                    cart = manager.RemoveDiscountCodesFromCart(shoppingCartId, customerId, new Collection<string>() { promotionCode });
                }

                if (cart == null)
                {
                    throw new DataValidationException(DataValidationErrors.CartNotFound, Resource.ShoppingCartNotFound, shoppingCartId);
                }

                ShoppingCart shoppingCart;

                if (dataLevel == ShoppingCartDataLevel.All)
                {
                    shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                }
                else
                {
                    shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, true);
                }

                AddATPforEcommerce(shoppingCart, cart);
                //Add for Recalculate Shipping Charges Bug :77027
                AddEstShippingCostToCart(shoppingCart, cart, customerId, false);
                //Add for Recalculate Shipping Charges  Bug :77027

                //NS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 
                CalculateShippingCharges(shoppingCart, cart, false);
                //NE Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/30/2014 

                if (cart[CartConstant.AvaTaxError] != null)
                {
                    if (!string.IsNullOrEmpty(cart[CartConstant.AvaTaxError].ToString()))
                        shoppingCart.AvalaraTaxError = cart[CartConstant.AvaTaxError].ToString();
                }

                foreach (TransactionItem item in shoppingCart.Items)
                {
                    if (item.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                    {
                        if (item.AFMCartLineATP != null)
                        {
                            if (!item.AFMCartLineATP.IsAvailable)
                                //shoppingCart.ATPError = string.IsNullOrEmpty(shoppingCart.ATPError) ? item.AFMCartLineATP.ItemId + " : " + item.AFMCartLineATP.Message + Environment.NewLine + "Available Quantity is :" + item.AFMCartLineATP.Quantity :
                                //shoppingCart.ATPError + Environment.NewLine + item.AFMCartLineATP.ItemId + " : " + item.AFMCartLineATP.Message + Environment.NewLine + "Available Quantity is :" + item.AFMCartLineATP.Quantity;
                                shoppingCart.ATPError = CartConstant.AtpItemNotAvailableMrssage;
                        }
                        else
                        {
                            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassATP"]))
                                shoppingCart.ATPError = CartConstant.AtpServiceNotAvailableErrorMrssage;
                        }
                    }
                }

                return shoppingCart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCartId + " CustomerId: " + customerId + " promotionCode: " + promotionCode + " ShoppinCartDataLevel: " + dataLevel + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// validate discount code.
        /// </summary>
        /// <param name="promotionCode">discount code.</param>        
        /// <returns>
        /// true/false
        /// </returns>
        public bool IsDicountCodeValid(string promotionCode)
        {
            if (string.IsNullOrWhiteSpace(promotionCode))
            {
                throw new ArgumentNullException("promotionCode");
            }
            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            var dicountcode = manager.GetDiscountCodes(new QueryResultSettings()).Results.FirstOrDefault(temp => temp.Code.ToLower() == promotionCode.ToLower());
            return (dicountcode != null ? true : false);
        }


        /// <summary>
        /// Gets the latest modified shopping cart associated with the current user.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// The active shoppping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">customerId</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.SecurityException">Thrown when the user is not authorized to get shopping carts.</exception>
        public ShoppingCart GetActiveShoppingCart(string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (!AFMDataUtilities.IsValidString(customerId))
            {
                throw new ArgumentException("Invalid customer id", "customerId");
            }
            try
            {

                ShoppingCart shoppingCart;

                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                Cart cart = manager.GetActiveCart(customerId, CalculationModes.All);

                if (cart != null && dataLevel == ShoppingCartDataLevel.All)
                {
                    shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                }
                else
                {
                    shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, false);
                }

                if ((shoppingCart == null) || (new ShoppingCartController().HasCartExpired(shoppingCart.LastModifiedDate)))
                {
                    return null;
                }

                AddATPforEcommerce(shoppingCart, cart);

                //CS by muthait dated 01Nov2014
                CalculateShippingCharges(shoppingCart, cart, false);
                //CS by muthait dated 01Nov2014


                if (cart != null && cart[CartConstant.AvaTaxError] != null)
                {
                    if (!string.IsNullOrEmpty(cart[CartConstant.AvaTaxError].ToString()))
                        shoppingCart.AvalaraTaxError = cart[CartConstant.AvaTaxError].ToString();
                }

                foreach (TransactionItem item in shoppingCart.Items)
                {
                    if (item.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                    {
                        if (item.AFMCartLineATP != null)
                        {
                            if (!item.AFMCartLineATP.IsAvailable)
                                //shoppingCart.ATPError = string.IsNullOrEmpty(shoppingCart.ATPError) ? item.AFMCartLineATP.ItemId + " : " + item.AFMCartLineATP.Message + Environment.NewLine + "Available Quantity is :" + item.AFMCartLineATP.Quantity :
                                //shoppingCart.ATPError + Environment.NewLine + item.AFMCartLineATP.ItemId + " : " + item.AFMCartLineATP.Message + Environment.NewLine + "Available Quantity is :" + item.AFMCartLineATP.Quantity;
                                shoppingCart.ATPError = CartConstant.AtpItemNotAvailableMrssage;
                        }
                        else
                        {
                            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassATP"]))
                                shoppingCart.ATPError = CartConstant.AtpServiceNotAvailableErrorMrssage;
                        }
                    }
                }

                return shoppingCart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "CustomerId: " + customerId + " ShoppinCartDataLevel: " + dataLevel + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Gets the shopping carts.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>
        /// A collection of shoppping carts.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">customerId</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart of customer {0} was not found.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.SecurityException">Thrown when the user is not authorized to get shopping carts.</exception>
        public Collection<ShoppingCart> GetShoppingCarts(string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentNullException("customerId");
            }
            if (!AFMDataUtilities.IsValidString(customerId))
            {
                throw new ArgumentException("Invalid customer id", "customerId");
            }
            try
            {
                var shoppingCarts = new Collection<ShoppingCart>();

                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                ReadOnlyCollection<Cart> carts = manager.GetCartsByCustomer(customerId, CalculationModes.All);

                foreach (Cart cart in carts)
                {
                    if (cart == null)
                    {
                        throw new DataValidationException(DataValidationErrors.CartNotFound, Resource.ShopingCartForCustomerNotFound, customerId);
                    }

                    ShoppingCart shoppingCart;

                    if (dataLevel == ShoppingCartDataLevel.All)
                    {
                        shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                    }
                    else
                    {
                        shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, true);
                    }

                    AddATPforEcommerce(shoppingCart, cart);
                    // CS by muthait dated 01Nov2014 
                    CalculateShippingCharges(shoppingCart, cart, true);
                    // CE by muthait dated 01Nov2014

                    shoppingCarts.Add(shoppingCart);
                }

                return shoppingCarts;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                      "CustomerId: " + customerId + " ShoppinCartDataLevel: " + dataLevel + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Claims the anonymous cart.
        /// </summary>
        /// <param name="shoppingCartIdToBeClaimed">The shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>A shopping cart.</returns>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} of customer {1} was not found.</exception>
        public ShoppingCart ClaimAnonymousCart(string shoppingCartIdToBeClaimed, string customerId, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine, bool includeTax)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartIdToBeClaimed))
            {
                throw new ArgumentNullException("shoppingCartIdToBeClaimed");
            }

            try
            {
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

                // First validate the cart to be claimed
                Cart cartToClaim = manager.GetCart(shoppingCartIdToBeClaimed, customerId, CalculationModes.None);

                // Make sure only anonymous cart can be claimed. CustomerId check is needed for defence in depth. 
                if (cartToClaim == null || !string.IsNullOrWhiteSpace(cartToClaim.CustomerId))
                {
                    return null;
                }

                Cart cart = new Cart
                {
                    Id = shoppingCartIdToBeClaimed,
                    CustomerId = customerId,
                    CartType = Microsoft.Dynamics.Commerce.Runtime.DataModel.CartType.Shopping,
                };

                cart = manager.CreateOrUpdateCart(cart, CalculationModes.None);

                if (cart == null)
                {
                    throw new DataValidationException(DataValidationErrors.CartNotFound, Resource.ShoppingCartForCustNotFound, shoppingCartIdToBeClaimed, customerId);
                }

                ShoppingCart shoppingCart;

                if (dataLevel == ShoppingCartDataLevel.All)
                {
                    shoppingCart = AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                }
                else
                {
                    shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, dataLevel, productValidator, true);
                }

                AddATPforEcommerce(shoppingCart, cart);
                // CS by muthait dated 01Nov2014
                CalculateShippingCharges(shoppingCart, cart, includeTax);
                // CE by muthait dated 01Nov2014 
                return shoppingCart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCartIdToBeClaimed + " CustomerId: " + customerId + " ShoppinCartDataLevel: " + dataLevel + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Populates the cart item with kit details.
        /// </summary>
        /// <param name="cart">The shopping cart.</param>
        internal ShoppingCart AddKitDetailsToShoppingCart(Cart cart, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (cart == null)
            {
                throw new ArgumentNullException("cart");
            }
            //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
            /*
            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            OrderManager manager = OrderManager.Create(runtime);

            // Get the product details for all the products in shopping cart.
            ReadOnlyCollection<Product> productDetails = manager.GetProductsInCart(cart.Id);
            ShoppingCart shoppingCart = AFMShoppingCartMapper.ConvertToViewModel(cart, ShoppingCartDataLevel.All, productValidator, true);            
            ShoppingCartController.PopulateKitItemDetails(shoppingCart, productDetails, searchEngine);*/
            ShoppingCart shoppingCart = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(cart, ShoppingCartDataLevel.All, productValidator, true);
            //NE - RxL
            return shoppingCart;
        }

        public void AFMSetShippingModeAndCostToItem(TransactionItem cartItem)
        {
            if (cartItem == null)
            {
                throw new ArgumentNullException("cartItem");
            }
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                AFMShippingDetailsService shippingDetailsService = new AFMShippingDetailsService();
                ShippingDetailsRequest shippingDetailsRequest = new ShippingDetailsRequest()
                {
                    ItemId = cartItem.ItemId
                };

                AFMShippingDetails shippingDetails = shippingDetailsService.GetShippingDetailsByItemId(shippingDetailsRequest).ShippingDetails;

                if (shippingDetails != null)
                {
                    cartItem.AFMShippingMode = shippingDetails.ShippingMode;
                    cartItem.AFMShippingCost = shippingDetails.ShippingCost;
                }
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        public void AddATPforEcommerce(ShoppingCart shoppingCart, Cart cart)
        {
            if (shoppingCart == null)
            {
                throw new ArgumentNullException("shoppingCart");
            }
            if (cart == null)
            {
                throw new ArgumentNullException("cart");
            }
            try
            {
                if (!Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassATP"]))
                {
                    //NS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/24/2014 
                    string custZipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
                    if (string.IsNullOrEmpty(custZipCode) && shoppingCart.ShippingAddress != null)
                        custZipCode = shoppingCart.ShippingAddress.ZipCode;
                    //Atp Zipcode error Hardik Patel
                    if (string.IsNullOrEmpty(custZipCode))
                    {
                        custZipCode = Convert.ToString(cart[CartConstant.CartZipCode]);
                        AFMDataUtilities.SetAFMZipCodeInCookie(custZipCode);
                    }
                    //Atp Zipcode error Hardik Patel
                    List<AFMATPItemGroupingResponse> atpCartLineItems = AFMGetAtpForEcommerceForCart(cart, custZipCode);

                    if (atpCartLineItems != null)
                    {
                        foreach (var cartLine in shoppingCart.Items)
                        {
                            foreach (var atpCartLine in atpCartLineItems)
                            {
                                AFMATPResponseItem itemResp = atpCartLine.Items.FirstOrDefault(t => t.ItemId == cartLine.ItemId);

                                if (itemResp != null)
                                {
                                    if (!itemResp.IsAvailable)
                                    {
                                        cartLine.AFMCartLineATP = new AFMATPResponseItem()
                                        {
                                            ItemId = itemResp.ItemId,
                                            IsAvailable = itemResp.IsAvailable,
                                            Message = itemResp.Message,
                                            Quantity = itemResp.Quantity
                                        };
                                    }
                                    else
                                    {
                                        cartLine.AFMCartLineATP = itemResp;
                                    }

                                    break;
                                }
                            }
                        }
                        //N by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
                        SetATPbyMOD(shoppingCart);
                    }
                    //NE Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/24/2014                     
                }
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        //N by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
        public void SetATPbyMOD(ShoppingCart shoppingCart)
        {
            Collection<TransactionItem> nonKitHDItems = new Collection<TransactionItem>();
            Collection<TransactionItem> kitItems = new Collection<TransactionItem>();
            var hdDeliveryMode = ConfigurationManager.AppSettings["DeliveryMode"];
            foreach (var item in shoppingCart.Items)
            {
                //CS Bug:96442 ATP data should be left blank on the Serivce/Delivery lines on the sales order
                if (item.AFMItemType == (int)AFMItemType.DeliveryServiceItem)
                {
                    item.AFMCartLineATP = null;
                    continue;
                }
                //CE Bug:96442
                if (!string.IsNullOrEmpty(item.AFMKitItemId))
                {
                    kitItems.Add(item);
                    continue;
                }

                if (item.DeliveryModeId == hdDeliveryMode)
                {
                    nonKitHDItems.Add(item);
                    continue;
                }

                if (item.AFMCartLineATP != null)
                    AssignATPDates(item.AFMCartLineATP, item.AFMCartLineATP);
            }

            AssignATPDatesByMOD(nonKitHDItems.ToList());

            var kitGroups = from item in kitItems
                            group item by item.AFMKitSequenceNumber into g
                            select new { GroupName = g.Key, Members = g };

            foreach (var g in kitGroups)
                AssignATPDatesByMOD(g.Members.ToList());
        }


        void AssignATPDatesByMOD(List<TransactionItem> items)
        {
            if (items.Count > 0)
            {
                var sortedList = from item in items
                                 orderby item.AFMCartLineATP.BestDate descending, item.AFMCartLineATP.EndDateRange descending
                                 select item;

                AFMATPResponseItem bestDateATP = sortedList.FirstOrDefault().AFMCartLineATP;

                if (bestDateATP != null)
                    foreach (var item in items)
                    {
                        if (item.AFMCartLineATP != null)
                            AssignATPDates(item.AFMCartLineATP, bestDateATP);
                    }
            }
        }

        void AssignATPDates(AFMATPResponseItem currentDateATP, AFMATPResponseItem bestDateATP)
        {
            currentDateATP.BeginDateRangeMOD = bestDateATP.BeginDateRange;
            currentDateATP.BestDateMOD = bestDateATP.BestDate;
            currentDateATP.EarliestDeliveryDateMOD = bestDateATP.EarliestDeliveryDate;
            currentDateATP.EndDateRangeMOD = bestDateATP.EndDateRange;
            currentDateATP.LatestDeliveryDateMOD = bestDateATP.LatestDeliveryDate;
            currentDateATP.MessageMOD = bestDateATP.Message;
        }
        //NE by RxL

        public void CalculateCartCount(ShoppingCart shoppingCart)
        {
            if (shoppingCart == null)
            {
                throw new ArgumentNullException("shoppingCart");
            }
            int counter = shoppingCart.Items.Count(p => string.IsNullOrEmpty(p.AFMKitItemId) && p.AFMItemType != (int)AFMItemType.DeliveryServiceItem);
            if (shoppingCart.Items.Count > 0)
                counter += shoppingCart.Items.Where(p => !string.IsNullOrEmpty(p.AFMKitItemId)).Select(c => c.AFMKitSequenceNumber).Distinct().Count();
            shoppingCart.CartCount = counter;
        }

        // NS by muthait dated 31Oct2014
        public void CalculateShippingCharges(ShoppingCart shoppingCart, Cart cart, bool includeTax, bool addOrderSettingsToCart = true)
        {
            if (shoppingCart == null)
            {
                throw new ArgumentNullException("shoppingCart");
            }
            if (cart == null)
            {
                throw new ArgumentNullException("cart");
            }
            shoppingCart.EstHomeDelivery = string.Empty;
            shoppingCart.EstShipping = string.Empty;
            shoppingCart.EstShippingDiscount = string.Empty;
            shoppingCart.EstHomeDeliveryDiscount = string.Empty;
            decimal tempEstHomeDelivery = new decimal(0);
            decimal tempEstShipping = new decimal(0);
            decimal tempEstHomeDeliveryDiscount = new decimal(0);
            decimal tempEstShippingDiscount = new decimal(0);
            decimal shippingCharges = 0.0M;
            foreach (var item in cart.CartLines)
            {
                if (item.LineData[CartConstant.AFMItemType] != null && (int)item.LineData[CartConstant.AFMItemType] == (int)AFMItemType.DeliveryServiceItem)
                {
                    if (item.DeliveryMode == ConfigurationManager.AppSettings["DeliveryMode"])
                    {
                        tempEstHomeDelivery += item.ExtendedPrice;
                        tempEstHomeDeliveryDiscount += item.DiscountAmount;
                    }
                    else
                    {
                        tempEstShipping += item.ExtendedPrice;
                        tempEstShippingDiscount += item.DiscountAmount;
                    }
                    shippingCharges += item.ExtendedPrice;
                }
                var ShoppingCartItem = shoppingCart.Items.FirstOrDefault(p => p.LineId == item.LineId);
                if (ShoppingCartItem != null)
                {
                    ShoppingCartItem.AFMShippingMode = item.LineData[CartConstant.AFMShippingMode] != null ? item.LineData[CartConstant.AFMShippingMode].ToString() : string.Empty;
                    ShoppingCartItem.AFMDirectShip = item.LineData[CartConstant.AFMDirectShip] != null ? (int)item.LineData[CartConstant.AFMDirectShip] : 0;
                    ShoppingCartItem.AFMSupplierDirectShip = item.LineData[CartConstant.AFMSupplierDirectShip] != null ? (int)item.LineData[CartConstant.AFMSupplierDirectShip] : 0;
                }
            }

            //CS Hardik - Bug 78800 dated 20Mar 2015
            List<string> AllDeliveryMode = cart.CartLines.Select(temp => temp.DeliveryMode).Distinct().ToList();
            foreach (var tempDlvMode in AllDeliveryMode)
            {
                var delivetyItem = cart.CartLines.FirstOrDefault(temp => (int)temp.LineData[CartConstant.AFMItemType] == (int)AFMItemType.DeliveryServiceItem && temp.DeliveryMode == tempDlvMode);
                if (delivetyItem == null)
                    shoppingCart.ChargeTypeError = CartConstant.AtpItemNotAvailableMrssage;
            }
            //CE Hardik - Bug 78800

            shoppingCart.EstHomeDelivery = tempEstHomeDelivery.ToCurrencyString();
            shoppingCart.EstShipping = tempEstShipping.ToCurrencyString();
            //NS developed by v-hapat on 05/03/2015 for bug:
            shoppingCart.EstHomeDeliveryDiscount = tempEstHomeDeliveryDiscount.ToCurrencyString();
            shoppingCart.EstShippingDiscount = tempEstShippingDiscount.ToCurrencyString();
            //NE developed by v-hapat on 05/03/2015
            shoppingCart.ChargeAmountWithCurrency = shippingCharges.ToCurrencyString();
            var subTotal = cart.SubtotalAmountWithoutTax - shippingCharges;
            shoppingCart.Subtotal = subTotal.ToString();
            shoppingCart.SubtotalWithCurrency = subTotal.ToCurrencyString();
            if (!includeTax)
            {
                shoppingCart.TaxAmountWithCurrency = new decimal(0).ToCurrencyString();
                shoppingCart.TotalAmount = cart.SubtotalAmountWithoutTax; // developed by v-hapat on 12/07/2014 for Remove item bug.
            }
            else
            {

                shoppingCart.TaxAmountWithCurrency = cart.TaxAmount.ToCurrencyString();
                shoppingCart.TotalAmount = cart.TotalAmount;
            }

            //Update discount Value on cart if shipping has direct discount
            shoppingCart.DiscountAmountWithCurrency = Decimal.Round(cart.DiscountAmount, 2).ToCurrencyString();
            shoppingCart.DiscountAmount = Decimal.Round(cart.DiscountAmount, 2);
            //Update discount Value on cart if shipping has direct discount

            shoppingCart.TotalAmountWithCurrency = shoppingCart.TotalAmount.ToCurrencyString();
            string financeId = !string.IsNullOrEmpty(AFMDataUtilities.GetFinanceOptionIdFromCookie()) ? AFMDataUtilities.GetFinanceOptionIdFromCookie() : string.Empty;
            // N CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
            if (addOrderSettingsToCart)
                AddOrderSettingsToCart(shoppingCart, cart, includeTax);
            //NS: By Spriya: Bug#61496: ECS - Subtotal, Home Delivery and Sales tax charges are $0.00 under Order Summary on Payment screen 
            dynamic OrderSummary = new
            {
                //subtotal
                st = shoppingCart.SubtotalWithCurrency,
                //shipping
                s = shoppingCart.EstShipping,
                //home delivery
                h = shoppingCart.EstHomeDelivery,
                //tax
                tx = shoppingCart.TaxAmountWithCurrency,
                //total price
                tl = shoppingCart.TotalAmountWithCurrency,
                //discount amount
                da = string.IsNullOrEmpty(shoppingCart.DiscountAmountWithCurrency) ? null : shoppingCart.DiscountAmountWithCurrency,
                //shipping discount
                ds = shoppingCart.EstShippingDiscount,
                //home delivery discount
                dh = shoppingCart.EstHomeDeliveryDiscount,
                sfid = financeId
            };

            var jsonData = JsonConvert.SerializeObject(OrderSummary);

            AFMDataUtilities.SetPaymentOrderSummaryInCookie(jsonData);
            //NE: By Spriya
            CalculateCartCount(shoppingCart);
        }

        // NE by muthait dated 31Oct2014
        //public Cart AddEstShippingCostToCart_Old(ShoppingCart shoppingCart, Cart cart, string customerId, bool includeTax)
        //{
        //    if (shoppingCart == null)
        //    {
        //        throw new ArgumentNullException("shoppingCart");
        //    }
        //    if (cart == null)
        //    {
        //        throw new ArgumentNullException("cart");
        //    }
        //    // CS by muthait dated 31Oct2014
        //    OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());

        //    var lineids = new List<string>();
        //    foreach (var item in cart.CartLines)
        //    {
        //        if (item.LineData["AFMItemType"] != null && (int)item.LineData["AFMItemType"] == (int)AFMItemType.DeliveryServiceItem)
        //            lineids.Add(item.LineId);
        //    }

        //    if (lineids.Count > 0)
        //    {
        //        CalculationModes modes = CalculationModes.Prices | CalculationModes.Discounts | CalculationModes.Totals;
        //        cart = manager.DeleteCartLines(shoppingCart.CartId, customerId, lineids, modes);
        //    }
        //    // CE by muthait dated 31Oct2014

        //    try
        //    {
        //        //NS Developed by  ysrini for "FDD0240-ECS-Calculate Shipping Charges " dated 7/15/2014 
        //        string cartItemsXmlString = AFMConvertCartItemsListToXmlString(shoppingCart.Items);
        //        CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
        //        AFMShippingDetailsService shippingDetailsService = new AFMShippingDetailsService();
        //        DeliveryChargesAndModeRequest deliveryChargesAndModeRequest = new DeliveryChargesAndModeRequest()
        //        {
        //            CartItemsXml = cartItemsXmlString
        //        };
        //        var reqContext = new RequestContext(runtime);
        //        DeliveryChargesAndModeResponse deliveryChargesAndModeResponse = runtime.Execute<DeliveryChargesAndModeResponse>(
        //             deliveryChargesAndModeRequest, reqContext, true);


        //        List<AFMShippingDetails> shippingDetailsList = deliveryChargesAndModeResponse.ShippingDetails;
        //        //NS Developed by  ysrini for "FDD0240-ECS-Calculate Shipping Charges " dated 7/15/2014 

        //        List<long> serviceItemsProdId = new List<long>();
        //        List<AFMShippingDetails> serviceItemsDetails = new List<AFMShippingDetails>();
        //        bool isInvalidServiceItem = false;

        //        if (shippingDetailsList != null)
        //        {
        //            foreach (var lineItems in shippingDetailsList)
        //            {
        //                if (Convert.ToBoolean(lineItems.IsServiceItem))
        //                {
        //                    serviceItemsDetails.Add(lineItems);
        //                    serviceItemsProdId.Add(lineItems.ProductId);

        //                    if (lineItems.ChargeType == 0)
        //                    {
        //                        isInvalidServiceItem = true;
        //                    }
        //                }
        //                else
        //                {
        //                    //CS by muthait dated 10Oct2014
        //                    var cartItemtoUpdate = cart.CartLines.Where(x => x.ItemId == lineItems.ItemId);
        //                    foreach (var item in cartItemtoUpdate)
        //                    {
        //                        item.DeliveryMode = lineItems.DeliveryMode;
        //                        item.LineData["AFMShippingMode"] = lineItems.ShippingMode;
        //                        item.LineData["AFMDirectShip"] = lineItems.DirectShip;
        //                        item.LineData["AFMSupplierDirectShip"] = lineItems.SupplierDirectShip;
        //                    }
        //                    //CE by muthait dated 10Oct2014
        //                    var toUpdate = shoppingCart.Items.Where(x => x.ItemId == lineItems.ItemId);
        //                    foreach (var item in toUpdate)
        //                    {
        //                        item.AFMShippingMode = lineItems.ShippingMode;
        //                        //NS Developed by spriya 
        //                        item.AFMDirectShip = lineItems.DirectShip;
        //                        item.AFMSupplierDirectShip = lineItems.SupplierDirectShip;
        //                        //NE Developed by spriya
        //                    }
        //                }
        //            }
        //        }

        //        //CS by muthait dated 10Oct2014
        //        if (cart.CartLines.Count > 0)
        //        {
        //            cart.DeliveryMode = cart.CartLines[0].DeliveryMode;
        //            cart.DeliveryModeChargeAmount = decimal.Parse(!string.IsNullOrEmpty(shoppingCart.EstHomeDelivery) ? shoppingCart.EstHomeDelivery : "0", NumberStyles.AllowCurrencySymbol | NumberStyles.Number) +
        //                decimal.Parse(!string.IsNullOrEmpty(shoppingCart.EstShipping) ? shoppingCart.EstShipping : "0", NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
        //        }
        //        manager.UpdateCartLines(shoppingCart.CartId, customerId, cart.CartLines, CalculationModes.All);
        //        // CE by muthait dated 31Oct2014
        //        cart = GetCartLinesForServiceItems(cart, serviceItemsProdId, serviceItemsDetails, customerId, shoppingCart.CartId);
        //        CalculateShippingCharges(shoppingCart, cart, includeTax);
        //        // CS by muthait dated 31OCt2014         

        //        if (isInvalidServiceItem)
        //        {
        //            shoppingCart.ChargeTypeError = CartConstant.ChargeTypeErrorMessage;
        //            //cart["ChargeTypeError"] = shoppingCart.ChargeTypeError;
        //            // cart = manager.CreateOrUpdateCart(cart);
        //        }
        //        else
        //        {
        //            shoppingCart.ChargeTypeError = string.Empty;
        //            //cart["ChargeTypeError"] = shoppingCart.ChargeTypeError;
        //            //cart = manager.CreateOrUpdateCart(cart);
        //        }
        //        return cart;
        //    }
        //    catch (Exception ex)
        //    {
        //        Logging.LoggerUtilities.ProcessLogMessage(
        //               "ShoppingCartId " + shoppingCart.CartId + " CustomerId: " + customerId + " IncludeTax:" + includeTax + Environment.NewLine +
        //               ex.Message + Environment.NewLine +
        //               ex.StackTrace);
        //        throw;
        //    }
        //}

        #region Add Item Performance
        //Add Item Performance : Hardik 
        public Cart AddEstShippingCostToCart(ShoppingCart shoppingCart, Cart cart, string customerId, bool includeTax)
        {
            if (shoppingCart == null)
            {
                throw new ArgumentNullException("shoppingCart");
            }
            if (cart == null)
            {
                throw new ArgumentNullException("cart");
            }

            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            try
            {
                //NS Developed by  ysrini for "FDD0240-ECS-Calculate Shipping Charges " dated 7/15/2014 
                var cartItemsXmlString = AFMConvertCartItemsListToXmlString(shoppingCart.Items);
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                AFMShippingDetailsService shippingDetailsService = new AFMShippingDetailsService();
                DeliveryChargesAndModeRequest deliveryChargesAndModeRequest = new DeliveryChargesAndModeRequest()
                {
                    CartItems = cartItemsXmlString
                };
                var reqContext = new RequestContext(runtime);
                DeliveryChargesAndModeResponse deliveryChargesAndModeResponse = runtime.Execute<DeliveryChargesAndModeResponse>(
                     deliveryChargesAndModeRequest, reqContext, true);


                List<AFMShippingDetails> shippingDetailsList = deliveryChargesAndModeResponse.ShippingDetails;
                //NS Developed by  ysrini for "FDD0240-ECS-Calculate Shipping Charges " dated 7/15/2014 

                List<long> serviceItemsProdId = new List<long>();
                List<AFMShippingDetails> serviceItemsDetails = new List<AFMShippingDetails>();
                bool isInvalidServiceItem = false;

                if (shippingDetailsList != null)
                {
                    foreach (var lineItems in shippingDetailsList)
                    {
                        if (Convert.ToBoolean(lineItems.IsServiceItem))
                        {
                            serviceItemsDetails.Add(lineItems);
                            serviceItemsProdId.Add(lineItems.ProductId);

                            if (lineItems.ChargeType == 0)
                            {
                                isInvalidServiceItem = true;
                            }
                        }
                        else
                        {
                            //CS by muthait dated 10Oct2014
                            var cartItemtoUpdate = cart.CartLines.Where(x => x.ItemId == lineItems.ItemId);
                            foreach (var item in cartItemtoUpdate)
                            {
                                item.DeliveryMode = lineItems.DeliveryMode;
                                item.LineData[CartConstant.AFMShippingMode] = lineItems.ShippingMode;
                                item.LineData[CartConstant.AFMDirectShip] = lineItems.DirectShip;
                                item.LineData[CartConstant.AFMSupplierDirectShip] = lineItems.SupplierDirectShip;
                            }
                            //CE by muthait dated 10Oct2014
                            var toUpdate = shoppingCart.Items.Where(x => x.ItemId == lineItems.ItemId);
                            foreach (var item in toUpdate)
                            {
                                item.DeliveryModeId = lineItems.DeliveryMode;
                                item.AFMShippingMode = lineItems.ShippingMode;
                                //NS Developed by spriya 
                                item.AFMDirectShip = lineItems.DirectShip;
                                item.AFMSupplierDirectShip = lineItems.SupplierDirectShip;
                                //NE Developed by spriya
                            }
                        }
                    }
                }

                //CS by muthait dated 10Oct2014
                if (cart.CartLines.Count > 0)
                {
                    cart.DeliveryMode = cart.CartLines[0].DeliveryMode;
                    cart.DeliveryModeChargeAmount = decimal.Parse(!string.IsNullOrEmpty(shoppingCart.EstHomeDelivery) ? shoppingCart.EstHomeDelivery : "0", NumberStyles.AllowCurrencySymbol | NumberStyles.Number) +
                        decimal.Parse(!string.IsNullOrEmpty(shoppingCart.EstShipping) ? shoppingCart.EstShipping : "0", NumberStyles.AllowCurrencySymbol | NumberStyles.Number);
                }

                //Prod Issue Update Ecom Status so it will refreash at all time -- Hardik
                ICollection<CartLine> cartLines = cart.CartLines;
                UpdateEcomstatusCartLine(ref cartLines);
                UpdateATPDataCartLine(ref cartLines);
                //Prod Issue Update Ecom Status so it will refreash at all time -- Hardik

                cart = manager.UpdateCartLines(shoppingCart.CartId, customerId, cartLines, CalculationModes.None);
                // CE by muthait dated 31Oct2014
                cart = AddDeliverryServiceLineToCart(cart, serviceItemsProdId, serviceItemsDetails, customerId);
                // CS by muthait dated 31OCt2014                               
                CalculateShippingCharges(shoppingCart, cart, includeTax);

                if (isInvalidServiceItem)
                {
                    shoppingCart.ChargeTypeError = CartConstant.AtpItemNotAvailableMrssage;
                }
                else
                {
                    shoppingCart.ChargeTypeError = string.Empty;
                }

                return cart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCart.CartId + " CustomerId: " + customerId + " IncludeTax:" + includeTax + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        //Add Item Performance : Hardik 
        /// <summary>
        /// Add Delivery Service Line to cart
        /// </summary>
        /// <param name="cart"></param>
        /// <param name="serviceItems"></param>
        /// <param name="serviceItemsDetails"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>        
        private Cart AddDeliverryServiceLineToCart(Cart cart, List<long> serviceItems, List<AFMShippingDetails> serviceItemsDetails, string customerId)
        {
            if (cart == null)
            {
                throw new ArgumentNullException("cart");
            }
            if (serviceItems.Count == 0)
            {
                throw new ArgumentNullException("serviceItems");
            }
            if (serviceItemsDetails.Count == 0)
            {
                throw new ArgumentNullException("serviceItemsDetails");
            }
            OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
            List<Product> serviceItemDetails = CrtUtilities.GetDevliveryServiceProduct(serviceItems);
            //Create Listing for Item
            Collection<Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing> listings = new Collection<Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing>();
            foreach (var serviceItem in serviceItemDetails)
            {
                //Check Item already Exist in List
                var cartLineItem = cart.CartLines.FirstOrDefault(temp => temp.ItemId == serviceItem.ItemId);
                if (cartLineItem == null)
                {
                    string productDetails = String.Format("{{ \"Name\": \"{0}\", \"Description\": \"{1}\" }}", serviceItem.ProductName, serviceItem.Description);
                    Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing listing = new Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing
                    {
                        ListingId = serviceItem.RecordId,
                        Quantity = 1,
                        ProductDetails = productDetails,
                        AFMItemType = (int)AFMItemType.DeliveryServiceItem
                    };
                    listings.Add(listing);
                }
                else
                {
                    ///Override Price if item is already exist
                    var cartItem = serviceItemsDetails.Where(x => x.ItemId == cartLineItem.ItemId).FirstOrDefault();
                    //CS Bug:94508 Tax calculation mode added for recalculation of service line charge override
                    if (cartItem != null && cartLineItem.Price != cartItem.TotalValue)
                        cart = manager.OverridePrice(cart.Id, cartLineItem.LineId, cartItem.TotalValue, CalculationModes.Prices | CalculationModes.Discounts | CalculationModes.Totals | CalculationModes.Taxes);
                    //CE bug:94508 
                }

            }
            //Add item in cart Line is not exist
            if (!listings.IsNullOrEmpty())
            {
                ICollection<CartLine> cartLines = new AFMShoppingCartMapper(new SiteConfiguration()).GetCartLinesFromListing(listings, new AFMProductValidator());
                foreach (var cartLineItem in cartLines)
                {
                    var cartItem = serviceItemsDetails.Where(x => x.ItemId == cartLineItem.ItemId).FirstOrDefault();
                    cartLineItem.DeliveryMode = cartItem.DeliveryMode;
                    cartLineItem.LineData[CartConstant.AFMShippingMode] = cartItem.ShippingMode;
                    cartLineItem.LineData[CartConstant.AFMDirectShip] = cartItem.DirectShip;
                    cartLineItem.LineData[CartConstant.AFMSupplierDirectShip] = cartItem.SupplierDirectShip;
                    cartLineItem.IsPriceOverridden = true;
                    cartLineItem.Price = cartItem.TotalValue;
                }
                UpdateEcomstatusCartLine(ref cartLines);
                UpdateATPDataCartLine(ref cartLines);
                cart = manager.AddCartLines(cart.Id, customerId, cartLines, CalculationModes.Prices | CalculationModes.Discounts | CalculationModes.Totals);
            }
            //Remove Service item Cart Line if that Devliery mode is not Exist            
            var alldeliveryMode = cart.CartLines.ToList().Where(temp => (AFMItemType)Enum.Parse(typeof(AFMItemType), temp.LineData[CartConstant.AFMItemType].ToString()) != AFMItemType.DeliveryServiceItem).Select(temp => temp.DeliveryMode);
            var tempremoveLineIs = cart.CartLines.Where(temp => !alldeliveryMode.Where(p => p == temp.DeliveryMode).Any() && (AFMItemType)Enum.Parse(typeof(AFMItemType), temp.LineData[CartConstant.AFMItemType].ToString()) == AFMItemType.DeliveryServiceItem).Select(p => p.LineId);
            if (!tempremoveLineIs.IsNullOrEmpty())
                cart = manager.DeleteCartLines(cart.Id, cart.CustomerId, tempremoveLineIs);
            //End Remove Service item Cart Line if that Devliery mode is not Exist            

            return cart;
        }

        #endregion
        /// <summary>
        /// Adds the order settings for the cart items to the cart
        /// </summary>
        /// <param name="shoppingCart"></param>
        /// <param name="cart"></param>
        /// <param name="customerId"></param>
        /// <param name="shoppingCartId"></param>
        /// <param name="includeTax"></param>
        /// <param name="addCartLine"></param>
        public void AddOrderSettingsToCart(ShoppingCart shoppingCart, Cart cart, bool includeTax)
        {
            if (shoppingCart == null)
            {
                throw new ArgumentNullException("shoppingCart");
            }
            if (cart == null)
            {
                throw new ArgumentNullException("cart");
            }
            try
            {
                var distinctItemIds = shoppingCart.Items.Select(item => item.ItemId).Distinct().ToList();
                if (distinctItemIds != null && distinctItemIds.Count > 0)
                {
                    List<AFMProductOrderSettings> orderSettings = GetOrderSettingsForItemIds(distinctItemIds);
                    var QuantityPerBoxUpperBound = Convert.ToInt32(ConfigurationManager.AppSettings["QuantityPerBoxUpperBound"]);
                    if (orderSettings != null)
                    {
                        foreach (var item in orderSettings)
                        {
                            var cartItemToUpdate = shoppingCart.Items.Where(x => x.ItemId == item.ItemId);
                            foreach (var itemToUpdate in cartItemToUpdate)
                            {
                                itemToUpdate.AFMMultipleQty = (item.MultipleQty > 1) ? item.MultipleQty : 1;
                                itemToUpdate.AFMLowestQty = (item.LowestQty > 1) ? item.LowestQty : 1;

                                if (itemToUpdate.AFMMultipleQty > 1 || itemToUpdate.AFMLowestQty > 1)
                                {
                                    itemToUpdate.AFMQtyOptions = new Collection<decimal>();
                                    decimal m = 0;
                                    for (int i = 1; m < QuantityPerBoxUpperBound; i++)
                                    {

                                        //m = itemToUpdate.AFMLowestQty + (itemToUpdate.AFMMultipleQty * i);
                                        //itemToUpdate.AFMQtyOptions.Add(m);

                                        if (itemToUpdate.AFMLowestQty != 0)
                                        {
                                            if ((itemToUpdate.AFMMultipleQty * i) >= itemToUpdate.AFMLowestQty)
                                            {
                                                m = (itemToUpdate.AFMMultipleQty * i);
                                                if (m < QuantityPerBoxUpperBound)
                                                    itemToUpdate.AFMQtyOptions.Add(m);
                                            }
                                        }
                                        else
                                        {
                                            m = (itemToUpdate.AFMMultipleQty * i);
                                            if (m < QuantityPerBoxUpperBound)
                                                itemToUpdate.AFMQtyOptions.Add(m);
                                        }
                                    }
                                }
                                itemToUpdate.AFMQtyPerBox = item.QtyPerBox;
                                if (itemToUpdate.AFMMultipleQty > 1 || !OrderMapper.UnitOfMeasure.Contains(itemToUpdate.AFMUnit))
                                {
                                    itemToUpdate.AFMQtyPerBox = 0;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "ShoppingCartId " + shoppingCart.CartId + " IncludeTax:" + includeTax + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
            }
        }

        //private Cart GetCartLinesForServiceItems(Cart cart, List<long> serviceItems, List<AFMShippingDetails> serviceItemsDetails, string customerId, string shoppingCartId)
        //{
        //    if (String.IsNullOrEmpty(shoppingCartId))
        //    {
        //        throw new ArgumentNullException("shoppingCartId");
        //    }
        //    if (cart == null)
        //    {
        //        throw new ArgumentNullException("cart");
        //    }
        //    if (serviceItems.Count == 0)
        //    {
        //        throw new ArgumentNullException("serviceItems");
        //    }
        //    if (serviceItemsDetails.Count == 0)
        //    {
        //        throw new ArgumentNullException("serviceItemsDetails");
        //    }

        //    CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
        //    ProductManager pm = ProductManager.Create(runtime);
        //    ProductSearchCriteria criteria = new ProductSearchCriteria(cart.ChannelId);
        //    criteria.Ids = serviceItems;
        //    criteria.DataLevel = CommerceEntityDataLevel.Minimal;
        //    List<Product> serviceItemDetails = pm.SearchProducts(criteria, new QueryResultSettings()).Results.ToList();

        //    if (serviceItemDetails.Count > 0)
        //    {
        //        Collection<Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing> listings = new Collection<Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing>();

        //        foreach (var serviceItem in serviceItemDetails)
        //        {
        //            string productDetails = String.Format("{{ \"Name\": \"{0}\", \"Description\": \"{1}\" }}", serviceItem.ProductName, serviceItem.Description);
        //            Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing listing = new Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing
        //            {
        //                ListingId = serviceItem.RecordId,
        //                Quantity = 1,
        //                ProductDetails = productDetails,
        //                AFMItemType = (int)AFMItemType.DeliveryServiceItem
        //            };
        //            listings.Add(listing);
        //        }

        //        ICollection<CartLine> cartLines = new AFMShoppingCartMapper(new SiteConfiguration()).GetCartLinesFromListing(listings, new AFMProductValidator());
        //        foreach (var cartLineItem in cartLines)
        //        {
        //            var cartItem = serviceItemsDetails.Where(x => x.ItemId == cartLineItem.ItemId).FirstOrDefault();
        //            cartLineItem.DeliveryMode = cartItem.DeliveryMode;
        //            cartLineItem.LineData["AFMShippingMode"] = cartItem.ShippingMode;
        //            cartLineItem.LineData["AFMDirectShip"] = cartItem.DirectShip;
        //            cartLineItem.LineData["AFMSupplierDirectShip"] = cartItem.SupplierDirectShip;
        //        }
        //        CalculationModes modes = CalculationModes.Prices | CalculationModes.Discounts | CalculationModes.Totals | CalculationModes.Taxes;
        //        OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
        //        cart = manager.AddCartLines(shoppingCartId, customerId, cartLines, modes);
        //        foreach (var cartLineItem in cart.CartLines)
        //        {
        //            var cartItem = serviceItemsDetails.FirstOrDefault(x => x.ItemId == cartLineItem.ItemId);
        //            if (cartItem != null && cartLineItem.ItemId == cartItem.ItemId)
        //                cart = manager.OverridePrice(cart.Id, cartLineItem.LineId, cartItem.TotalValue, CalculationModes.All);
        //        }
        //        //cart.CartLines.AddRange(cartLines.ToList());
        //    }
        //    return cart;
        //}

        private List<AFMShippingDetails> AFMConvertCartItemsListToXmlString(Collection<TransactionItem> cartItems)
        {
            if (cartItems.Count == 0)
            {
                throw new ArgumentNullException("cartItems");
            }
            List<AFMShippingDetails> cartItemShippingInfoList = new List<AFMShippingDetails>();
            foreach (var cartItem in cartItems)
            {
                AFMShippingDetails lineItem = new AFMShippingDetails();
                //lineItem.AFMVendorId = string.IsNullOrEmpty(cartItem.AFMVendorId) ? string.Empty : cartItem.AFMVendorId;
                //lineItem.AFMVendorName = string.IsNullOrEmpty(cartItem.AFMVendorName) ? string.Empty : cartItem.AFMVendorName;
                lineItem.ItemId = string.IsNullOrEmpty(cartItem.ItemId) ? string.Empty : cartItem.ItemId;
                lineItem.LineId = string.IsNullOrEmpty(cartItem.LineId) ? string.Empty : cartItem.LineId;
                //lineItem.Name = string.IsNullOrEmpty(cartItem.ProductName) ? string.Empty : cartItem.ProductName;
                //lineItem.ProductDetails = string.IsNullOrEmpty(cartItem.ProductDetails) ? string.Empty : cartItem.ProductDetails;
                lineItem.ProductId = cartItem.ProductId;
                lineItem.Quantity = cartItem.Quantity.ToString();
                if (cartItem.ShippingAddress != null)
                    lineItem.ZipCode = string.IsNullOrEmpty(cartItem.ShippingAddress.ZipCode)
                        ? AFMDataUtilities.GetAFMZipCodeFromCookie()
                        : cartItem.ShippingAddress.ZipCode.Substring(0, 5);
                else
                    lineItem.ZipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
                //lineItem.TotalValue = decimal.Parse(cartItem.NetAmountWithCurrency, NumberStyles.Currency);
                lineItem.TotalValue = decimal.Parse(cartItem.NetAmountWithCurrency, NumberStyles.Currency, CultureInfo.GetCultureInfo("en-us"));
                lineItem.ShippingCost = cartItem.AFMShippingCost;
                lineItem.ShippingMode = string.IsNullOrEmpty(cartItem.AFMShippingMode) ? string.Empty : cartItem.AFMShippingMode;
                lineItem.IsServiceItem = Convert.ToBoolean(cartItem.AFMItemType);
                if (lineItem.IsServiceItem == false)
                    cartItemShippingInfoList.Add(lineItem);
            }
            return cartItemShippingInfoList;

        }


        //NS Developed by  ysrini for "0238-FDD Manage Shopping Cart" dated 6/24/2014 
        /// <summary>
        /// Fetch's ATP Ecommerce for the cart
        /// </summary>
        /// <param name="cart">customer cart</param>
        /// <returns>Atp Cart Lines</returns>
        private List<AFMATPItemGroupingResponse> AFMGetAtpForEcommerceForCart(Cart cart, string zipCode)
        {
            if (cart == null)
            {
                throw new ArgumentNullException("cart");
            }
            if (String.IsNullOrEmpty(zipCode))
            {
                throw new ArgumentNullException("zipCode");
            }
            if (!AFMDataUtilities.IsValidZipCode(zipCode))
            {
                throw new ArgumentException("Invalid zip code", "zipCode");
            }
            var atpCartRequest = new AFMATPRequest();
            var itemGroupings = new List<AFMATPItemGroupingRequest>();
            var atpItemGroup = new AFMATPItemGroupingRequest();
            //GetVendorDetailsServiceResponse vendorEcommDetailResponse = null;
            List<AFMItemInfo> ecommItems = new List<AFMItemInfo>();
            List<AFMItemInfo> nonEcommItems = new List<AFMItemInfo>();
            //AFMVendorDetailsService vendorDetailService = new AFMVendorDetailsService();
            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            RequestContext reqContext = new RequestContext(runtime);

            string ecomAccountNumber = string.Empty;
            string ecommShipTo = string.Empty;

            foreach (var cartLine in cart.CartLines)
            {
                if ((int)cartLine.LineData["AFMItemType"] != (int)AFMItemType.DeliveryServiceItem)
                {
                    //GetVendorDetailsServiceRequest vendorDetailRequest =
                    //                       new GetVendorDetailsServiceRequest()
                    //                       {
                    //                           ZipCode = zipCode,
                    //                           ItemId = cartLine.ItemId
                    //                       };

                    //GetVendorDetailsServiceResponse vendorDetailResponse = runtime.Execute<GetVendorDetailsServiceResponse>(
                    //       vendorDetailRequest,
                    //       reqContext, true);
                    bool isItemEcommOwned = Convert.ToBoolean(cartLine.LineData[CartConstant.IsEcommOwned]);

                    if (isItemEcommOwned)
                    {
                        ecomAccountNumber = Convert.ToString(cartLine.LineData[CartConstant.InvoiceVendorId]);
                        ecommShipTo = Convert.ToString(cartLine.LineData[CartConstant.ShipToId]);
                        ecommItems.Add(new AFMItemInfo() { ItemId = cartLine.ItemId, IsSDSO = Convert.ToBoolean(cartLine.LineData[CartConstant.IsSDSO]), Quantity = cartLine.Quantity });
                    }
                    else
                        nonEcommItems.Add(new AFMItemInfo() { ItemId = cartLine.ItemId, IsSDSO = Convert.ToBoolean(cartLine.LineData[CartConstant.IsSDSO]), Quantity = cartLine.Quantity });
                }
            }


            //Adding items in request for Ecomm owned items
            if (ecommItems.Count > 0)
            {
                //if (vendorEcommDetailResponse != null)
                //{
                atpItemGroup.AccountNumber = ecomAccountNumber;
                atpItemGroup.ShipTo = ecommShipTo;
                atpItemGroup.CustomerZip = zipCode;
                //}
                //List<AFMATPRequestItem> atReqEcommItems = new List<AFMATPRequestItem>();
                //foreach (var ecommItem in ecommItems)
                //{
                //    AFMATPRequestItem item = new AFMATPRequestItem();
                //    item.ItemId = ecommItem.ItemId;
                //    item.Quantity = Convert.ToInt32(ecommItem.Quantity);
                //    item.ThirdPartyItem = ecommItem.IsSDSO;
                //    atReqEcommItems.Add(item);
                //}
                var atReqEcommItems = ecommItems.GroupBy(tempitem => new { itemid = tempitem.ItemId, isthirdparty = tempitem.IsSDSO }).Select(tempReq => new AFMATPRequestItem() { ItemId = tempReq.Key.itemid, Quantity = Convert.ToInt32(tempReq.Sum(p => p.Quantity)), ThirdPartyItem = tempReq.Key.isthirdparty }).ToList();

                atpItemGroup.Items = atReqEcommItems;
                itemGroupings.Add(atpItemGroup);
            }

            //Adding items in request for non-Ecomm owned items
            if (nonEcommItems.Count > 0)
            {
                atpItemGroup = new AFMATPItemGroupingRequest();
                AFMVendorAffiliation afmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(zipCode);
                atpItemGroup.AccountNumber = afmVendorAffiliation != null ? afmVendorAffiliation.InvoiceVendorId : string.Empty;
                atpItemGroup.ShipTo = afmVendorAffiliation != null ? afmVendorAffiliation.ShipToId : string.Empty;

                //List<AFMATPRequestItem> atReqItems = new List<AFMATPRequestItem>();
                //foreach (var nonEcommItem in nonEcommItems)
                //{
                //    AFMATPRequestItem item = new AFMATPRequestItem();
                //    item.ItemId = nonEcommItem.ItemId;
                //    item.Quantity = Convert.ToInt32(nonEcommItem.Quantity);

                //    atReqItems.Add(item);
                //}
                var atReqItems = nonEcommItems.GroupBy(tempitem => new { itemid = tempitem.ItemId }).Select(tempReq => new AFMATPRequestItem() { ItemId = tempReq.Key.itemid, Quantity = Convert.ToInt32(tempReq.Sum(p => p.Quantity)) }).ToList();

                atpItemGroup.Items = atReqItems;
                itemGroupings.Add(atpItemGroup);
            }


            atpCartRequest.ItemGroupings = itemGroupings;
            GetAtpDetailsServiceRequest atpDetailsServiceRequest = new GetAtpDetailsServiceRequest() { ItemGroupings = itemGroupings };
            GetATPDetailsServiceResponse atpDetailResponse = runtime.Execute<GetATPDetailsServiceResponse>(
                 atpDetailsServiceRequest,
                 reqContext, true);
            //AFMATPResponse atpForEcommerceResponse = AFMATPController.GetAtpForEcommerce(atpCartRequest);

            return atpDetailResponse != null ? atpDetailResponse.ItemGroupings : null;
        }

        /// <summary>
        /// Checks the availability of the items to be added.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="listings"></param>
        /// <returns></returns>
        public AFMAtpOutputContainer CheckAtpItemAvailability(string customerId,
            IEnumerable<Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing> listings, string zipCode)
        {
            if (listings == null)
            {
                throw new ArgumentNullException("listings");
            }
            if (!AFMDataUtilities.IsValidZipCode(zipCode))
            {
                throw new ArgumentException("Invalid zip code", "zipCode");
            }
            if (String.IsNullOrEmpty(zipCode))
            {
                throw new ArgumentNullException("zipCode");
            }
            try
            {
                Collection<ViewModel.Listing> currentItemListing = GetProductListings(listings.ToList()[0]);
                #region CreateDataforAtp
                var atpCartRequest = new AFMATPRequest();
                var itemGroupings = new List<AFMATPItemGroupingRequest>();


                var atpItemGroup = new AFMATPItemGroupingRequest();
                GetVendorDetailsServiceResponse vendorEcommDetailResponse = null;
                List<AFMItemInfo> ecommItems = new List<AFMItemInfo>();
                List<AFMItemInfo> nonEcommItems = new List<AFMItemInfo>();
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                RequestContext reqContext = new RequestContext(runtime);

                foreach (var productItem in currentItemListing)
                {
                    GetVendorDetailsServiceRequest vendorDetailRequest =
                                           new GetVendorDetailsServiceRequest()
                                           {
                                               ZipCode = zipCode,
                                               ItemId = productItem.ItemId
                                           };

                    GetVendorDetailsServiceResponse vendorDetailResponse = runtime.Execute<GetVendorDetailsServiceResponse>(
                           vendorDetailRequest,
                           reqContext, true);
                    bool isItemEcommOwned = vendorDetailRequest.isEcomOwned;
                    if (isItemEcommOwned)
                    {
                        vendorEcommDetailResponse = vendorDetailResponse;
                        ecommItems.Add(new AFMItemInfo() { ItemId = productItem.ItemId, IsSDSO = vendorDetailRequest.IsSDSO, Quantity = productItem.Quantity });
                    }
                    else
                        nonEcommItems.Add(new AFMItemInfo() { ItemId = productItem.ItemId, IsSDSO = vendorDetailRequest.IsSDSO, Quantity = productItem.Quantity });
                }

                //Adding items in request for Ecomm owned items
                if (ecommItems.Count > 0)
                {
                    if (vendorEcommDetailResponse != null)
                    {
                        atpItemGroup.AccountNumber = vendorEcommDetailResponse.VendorDetails != null ? (vendorEcommDetailResponse.VendorDetails[0].InvoiceVendorId) : string.Empty;
                        atpItemGroup.ShipTo = vendorEcommDetailResponse.VendorDetails != null ? (vendorEcommDetailResponse.VendorDetails[0].ShipToId) : string.Empty;
                        atpItemGroup.CustomerZip = zipCode;
                    }
                    List<AFMATPRequestItem> atReqEcommItems = new List<AFMATPRequestItem>();
                    foreach (var ecommItem in ecommItems)
                    {
                        AFMATPRequestItem item = new AFMATPRequestItem();
                        item.ItemId = ecommItem.ItemId;
                        item.Quantity = Convert.ToInt32(ecommItem.Quantity);
                        item.ThirdPartyItem = ecommItem.IsSDSO;
                        atReqEcommItems.Add(item);
                    }

                    atpItemGroup.Items = atReqEcommItems;
                    itemGroupings.Add(atpItemGroup);
                }

                //Adding items in request for non-Ecomm owned items
                if (nonEcommItems.Count > 0)
                {
                    atpItemGroup = new AFMATPItemGroupingRequest();
                    AFMVendorAffiliation afmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(zipCode);
                    atpItemGroup.AccountNumber = afmVendorAffiliation != null ? afmVendorAffiliation.InvoiceVendorId : string.Empty;
                    atpItemGroup.ShipTo = afmVendorAffiliation != null ? afmVendorAffiliation.ShipToId : string.Empty;

                    List<AFMATPRequestItem> atReqItems = new List<AFMATPRequestItem>();
                    foreach (var nonEcommItem in nonEcommItems)
                    {
                        AFMATPRequestItem item = new AFMATPRequestItem();
                        item.ItemId = nonEcommItem.ItemId;
                        item.Quantity = Convert.ToInt32(nonEcommItem.Quantity);

                        atReqItems.Add(item);
                    }

                    atpItemGroup.Items = atReqItems;
                    itemGroupings.Add(atpItemGroup);
                }


                atpCartRequest.ItemGroupings = itemGroupings;
                #endregion
                GetAtpDetailsServiceRequest atpDetailsServiceRequest = new GetAtpDetailsServiceRequest() { ItemGroupings = itemGroupings };
                GetATPDetailsServiceResponse atpDetailResponse = runtime.Execute<GetATPDetailsServiceResponse>(
                     atpDetailsServiceRequest,
                     reqContext, true);

                if (atpDetailResponse != null)
                {
                    if (atpDetailResponse.ItemGroupings != null)
                    {
                        string errorText = string.Empty;
                        foreach (var itemGrouping in atpDetailResponse.ItemGroupings)
                        {
                            foreach (var item in itemGrouping.Items)
                            {
                                if (!item.IsAvailable)
                                {
                                    errorText = string.IsNullOrEmpty(errorText) ? item.ItemId + " : " + item.Message : errorText + Environment.NewLine + item.ItemId + " : " + item.Message;
                                }
                            }
                        }
                        //NE ATP update in PDP Page --Hardik
                        //Update Listing List wiht AtpResponse
                        Collection<AFMATPResponseItem> listRepoonse = new Collection<AFMATPResponseItem>();

                        foreach (var itemGrouping in atpDetailResponse.ItemGroupings)
                        {
                            foreach (var item in itemGrouping.Items)
                            {
                                listRepoonse.Add(item);
                            }
                        }
                        AssignATPDatesByMOD(listRepoonse);
                        foreach (var itemGrouping in atpDetailResponse.ItemGroupings)
                        {
                            foreach (var item in itemGrouping.Items)
                            {
                                var tempITem = listRepoonse.FirstOrDefault(temp => temp.ItemId == item.ItemId);
                                tempITem.Message = tempITem.MessageMOD;
                                AssignATPDates(item, tempITem);
                            }
                        }
                        //NE ATP update in PDP Page --Hardik
                        AFMAtpOutputContainer atpOC = new AFMAtpOutputContainer()
                        {
                            ItemGroupings = atpDetailResponse.ItemGroupings,
                            ErrorMsg = errorText
                        };
                        return atpOC;
                    }
                    else
                        return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "CustomerId: " + customerId + " Zipcode : " + zipCode + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        void AssignATPDatesByMOD(Collection<AFMATPResponseItem> items)
        {
            if (items.Count > 0)
            {
                var sortedList = from item in items
                                 orderby item.BestDate descending, item.EndDateRange descending
                                 select item;

                AFMATPResponseItem bestDateATP = sortedList.FirstOrDefault();

                if (bestDateATP != null)
                    foreach (var item in items)
                    {
                        AssignATPDates(item, bestDateATP);
                    }
            }
        }


        public Collection<ViewModel.Listing> GetProductListings(Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing itemlisting)
        {
            if (itemlisting == null)
            {
                throw new ArgumentNullException("itemListing");
            }
            Dictionary<long, int> componentQuantity = new Dictionary<long, int>();
            IList<string> itemList = new List<string> { itemlisting.ItemId };
            List<Product> productList = new List<Product>();
            Collection<ViewModel.Listing> listings = null;

            ReadOnlyCollection<Product> selectedProduct = AFMProductsController.GetProductsByItemIds(itemList, CommerceEntityDataLevel.Complete, false);
            if (selectedProduct[0].IsKit)
            {
                foreach (var component in selectedProduct[0].CompositionInformation.KitDefinition.KitLineDefinitions)
                {
                    componentQuantity.Add(component.ComponentProductId, itemlisting.Quantity * Convert.ToInt32(component.IndexedComponentProperties[component.ComponentProductId].Quantity));
                }
                productList = AFMProductsController.GetProductsByIds(componentQuantity.Keys.ToList<long>(), CommerceEntityDataLevel.Minimal).ToList();  //DemoController.GetProductsByIds(componentQuantity.Keys.ToList<long>()).ToList();
            }
            else
            {
                productList = selectedProduct.ToList();
            }

            listings = new Collection<ViewModel.Listing>() { itemlisting };

            if (selectedProduct[0].IsKit)
            {
                listings = AFMGetKitComponentProductListing(productList, componentQuantity, itemlisting);
            }
            return listings;
        }
        // NS CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box

        public Collection<ViewModel.Listing> GetListingsOrderSettings(Collection<ViewModel.Listing> listings)
        {
            if (listings.Count < 0)
            {
                throw new ArgumentNullException("listings");
            }

            try
            {
                var distinctitemids = listings.Select(p => p.ItemId).Distinct<string>().ToList();

                List<AFMProductOrderSettings> orderSettings = GetOrderSettingsForItemIds(distinctitemids);
                int QuantityPerBoxUpperBound = Convert.ToInt32(ConfigurationManager.AppSettings["QuantityPerBoxUpperBound"]);
                if (orderSettings != null)
                {
                    foreach (var listing in listings)
                    {
                        AFMProductOrderSettings os = orderSettings.FirstOrDefault(p => p.ItemId == listing.ItemId);
                        if (os != null && ((os.MultipleQty > 1 || os.LowestQty > 1) && listing.AFMKitProductId <= 0))
                        {
                            if (listing.Quantity > os.LowestQty && listing.Quantity > os.MultipleQty)
                            {
                                //we need to do rounding off, if later request comes for the same , currently if not divisible by multipleqty, then it will pick the lowest or multiple Qty value

                                if ((listing.Quantity - os.LowestQty) % os.MultipleQty == 0 && listing.Quantity > QuantityPerBoxUpperBound)
                                {
                                    listing.Quantity = os.LowestQty + (os.MultipleQty * 9);
                                }
                                else if ((listing.Quantity - os.LowestQty) % os.MultipleQty != 0)
                                {
                                    listing.Quantity = (int)os.LowestQty;
                                }
                            }
                            else if ((listing.Quantity > os.LowestQty && listing.Quantity < os.MultipleQty) || listing.Quantity < os.LowestQty)
                                listing.Quantity = (int)os.LowestQty;
                        }
                        //BUG 94731 - Hardik Fix
                        if (os != null)
                        {
                            listing.AFMLowestQty = os.LowestQty;
                            listing.AFMMultipleQty = os.MultipleQty;
                            listing.AFMQuantityPerBox = os.QtyPerBox;
                        }
                        //BUG 94731 - Hardik Fix
                    }
                }
                return listings;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }

        public List<AFMProductOrderSettings> GetOrderSettingsForItemIds(List<string> distinctitemids)
        {
            if (distinctitemids.Count == 0)
            {
                throw new ArgumentNullException("distinctitemids");
            }
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                AFMProductService productService = new AFMProductService();
                List<ProductLookupClause> plc = new List<ProductLookupClause>();
                foreach (var itemid in distinctitemids)
                {
                    plc.Add(new ProductLookupClause(itemid));
                }

                GetOrderSettingsForProductRequest orderSettingsRequest = new GetOrderSettingsForProductRequest()
                {
                    ItemIds = plc
                };
                var reqContext = new RequestContext(runtime);
                List<AFMProductOrderSettings> orderSettings = runtime.Execute<GetOrderSettingsForProductResponse>(orderSettingsRequest, reqContext, true).ProductOrderSettingsList;
                return orderSettings;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }
        // NE CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box       

        //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
        public Collection<ViewModel.Listing> AFMGetKitComponentProductListing(List<Product> componentList, Dictionary<long, int> componentQuantity, ViewModel.Listing kitMasterListing)
        {
            if (kitMasterListing == null)
            {
                throw new ArgumentNullException("kitMasterListing");
            }
            if (componentList.Count == 0)
            {
                throw new ArgumentNullException("componentList");
            }
            if (componentQuantity.Count == 0)
            {
                throw new ArgumentNullException("componentQuantity");
            }
            try
            {
                Collection<ViewModel.Listing> listings = new Collection<ViewModel.Listing>();
                foreach (var component in componentQuantity)
                {
                    Product componentProduct = null;
                    foreach (var product in componentList)
                    {
                        if (product.IsMasterProduct)
                        {
                            foreach (var variant in product.CompositionInformation.VariantInformation.Variants)
                            {
                                if (variant.DistinctProductVariantId == component.Key)
                                {
                                    componentProduct = product;
                                    break;
                                }
                            }
                            if (componentProduct != null)
                                break;
                        }
                        else
                        {
                            if (product.RecordId == component.Key)
                            {
                                componentProduct = product;
                                break;
                            }
                        }
                    }
                    ViewModel.Listing listing = AFMOrderMapper.ConvertToViewModel(componentProduct, component.Key);
                    listing.Quantity *= componentQuantity.Where(p => p.Key == component.Key).FirstOrDefault().Value;
                    listing.AFMKitItemId = kitMasterListing.ItemId;
                    listing.AFMKitItemProductDetails = kitMasterListing.ProductDetails;
                    listing.AFMKitItemQuantity = kitMasterListing.Quantity;
                    listing.AFMKitProductId = kitMasterListing.ListingId;
                    listings.Add(listing);
                }
                return listings;
                //NS - RxL
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                            ex.Message + Environment.NewLine +
                            ex.StackTrace);
                throw;
            }
        }


        public AFMOrderConfirmationResponse GetOrderConfirmationDetails(ISearchEngine searchEngine)
        {
            try
            {
                AFMOrderConfirmationResponse ocr = new AFMOrderConfirmationResponse();
                OrderController orderController = new OrderController();
                orderController.SetConfiguration(this.Configuration);
                string channelReferenceId = AFM.Commerce.Framework.Extensions.Utils.AFMDataUtilities.GetOrderrIdFromCookie();

                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.SalesOrder salesOrder = orderController.GetCommittedSalesOrderByConfirmationId(channelReferenceId, true, searchEngine);

                if (salesOrder != null)
                {
                    ShoppingCart cart = new ShoppingCart();
                    cart.SalesOrderNumber = salesOrder.ConfirmationId;
                    cart.SubtotalWithCurrency = salesOrder.SubtotalWithCurrency;
                    cart.TaxAmountWithCurrency = salesOrder.TaxAmountWithCurrency;
                    cart.TotalAmountWithCurrency = salesOrder.TotalAmountWithCurrency;
                    cart.DiscountAmountWithCurrency = salesOrder.DiscountAmountWithCurrency;
                    cart.DiscountAmount = salesOrder.DiscountAmount;
                    cart.EstHomeDelivery = salesOrder.EstHomeDelivery;
                    cart.EstShipping = salesOrder.EstShipping;
                    cart.EstHomeDeliveryDiscount = salesOrder.EstHomeDeliveryDiscount;
                    cart.EstShippingDiscount = salesOrder.EstShippingDiscount;
                    cart.CustomerId = salesOrder.CustomerId;
                    cart.Subtotal = salesOrder.Subtotal;
                    cart.EstHomeDelivery = string.Empty;
                    cart.EstShipping = string.Empty;
                    decimal tempEstHomeDelivery = new decimal(0);
                    decimal tempEstShipping = new decimal(0);
                    decimal shippingCharges = 0.0M;
                    decimal tempEstHomeDeliveryDiscount = new decimal(0);
                    decimal tempEstShippingDiscount = new decimal(0);
                    foreach (TransactionItem tempItem in salesOrder.Items)
                    {
                        if (tempItem.AFMItemType == (int)AFMItemType.DeliveryServiceItem)
                        {
                            if (tempItem.DeliveryModeId == ConfigurationManager.AppSettings["DeliveryMode"])
                            {
                                tempEstHomeDelivery += Convert.ToDecimal(tempItem.AFMShippingCost);
                                tempEstHomeDeliveryDiscount += tempItem.DiscountAmount;
                            }
                            else
                            {
                                tempEstShipping += Convert.ToDecimal(tempItem.AFMShippingCost);
                                tempEstShippingDiscount += tempItem.DiscountAmount;
                            }
                            shippingCharges += Convert.ToDecimal(tempItem.AFMShippingCost);
                        }
                        if (tempItem.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                        {
                            cart.Items.Add(tempItem);
                        }
                    }

                    cart.EstHomeDelivery = tempEstHomeDelivery.ToCurrencyString();
                    cart.EstShipping = tempEstShipping.ToCurrencyString();
                    cart.EstHomeDeliveryDiscount = tempEstHomeDeliveryDiscount.ToCurrencyString();
                    cart.EstShippingDiscount = tempEstShippingDiscount.ToCurrencyString();
                    cart.ChargeAmountWithCurrency = shippingCharges.ToCurrencyString();
                    var subTotal = Convert.ToDecimal(cart.Subtotal) - shippingCharges;
                    cart.Subtotal = subTotal.ToString();
                    cart.SubtotalWithCurrency = subTotal.ToCurrencyString();
                    if (salesOrder.TenderLines != null)
                        cart.TenderLines = new Collection<TenderDataLine>();
                    cart.TenderLines.AddRange(salesOrder.TenderLines);
                    if (salesOrder.DiscountCodes != null)
                        cart.DiscountCodes.AddRange(salesOrder.DiscountCodes);
                    cart.ShippingAddress = salesOrder.ShippingAddress;
                    cart.BillingAddress = salesOrder.BillingAddress;
                    //AddOrderSettingsToCart(cart, new Cart(), true);                   
                    ocr.ShoppingCart = cart;
                    ocr.ShippingAddress = salesOrder.ShippingAddress;
                    ocr.BillingAddress = salesOrder.BillingAddress;
                    //CS by spriya - Synchrony Online
                    cart.AfmFinancingOptionId = salesOrder.AfmFinancingOptionId;
                    cart.AFMPromoDesc = salesOrder.AFMPromoDesc;
                    cart.AFMCardName = salesOrder.AFMCardName;
                    //CE by spriya - Synchrony Online 
                }
                return ocr;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                            ex.Message + Environment.NewLine +
                                            ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
        /// <summary>
        /// This method returns true if it finds the given RSA ID in rsa id list returned from AFMDataUtilities.GetRsaIds method.
        /// It also updates the cart with valid RSA ID
        /// </summary>
        /// <param name="shoppingCartId">Shopping cart id</param>
        /// <param name="rsaId">RSA ID entered by user</param>
        /// <returns>true if valid rsa id else false</returns>
        public bool IsValidRsaId(string shoppingCartId, string rsaId)
        {
            try
            {
                List<AFMRsaId> rsaIdList = AFMDataUtilities.GetRsaIds();
                foreach (var rsa in rsaIdList)
                {
                    if (string.Equals(rsa.RsaId, rsaId, StringComparison.OrdinalIgnoreCase))
                    {
                        OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                        Cart cart = manager.GetCart(shoppingCartId, CalculationModes.None);
                        cart[CartConstant.CartRsaId] = rsaId;
                        cart = manager.CreateOrUpdateCart(cart, CalculationModes.None);
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                            ex.Message + Environment.NewLine +
                                            ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
            return false;
        }

        /// <summary>
        /// This method removes the RSA ID associated with the cart.
        /// </summary>
        /// <param name="shoppingCartId"></param>
        public void RemoveRsaIdFromCart(string shoppingCartId)
        {
            try
            {
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                Cart cart = manager.GetCart(shoppingCartId, CalculationModes.None);
                cart[CartConstant.CartRsaId] = null;
                cart = manager.CreateOrUpdateCart(cart, CalculationModes.None);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                            ex.Message + Environment.NewLine +
                                            ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }
        //NE - RxL   

        public void RemoveFinanceOption(string shoppingCartId)
        {
            try
            {
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                Cart cart = manager.GetCart(shoppingCartId, CalculationModes.None);
                cart[CartConstant.FinancingOptionId] = null;
                cart[CartConstant.RemoveDiscount] = false;
                cart = manager.CreateOrUpdateCart(cart, CalculationModes.None);
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                            ex.Message + Environment.NewLine +
                                            ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }
    }

    public class AFMProductValidator : IProductValidator
    {
        public void ValidateProductDetails(string productDetails)
        {
            //TODO: Implement proper validation.
        }
    }

}
