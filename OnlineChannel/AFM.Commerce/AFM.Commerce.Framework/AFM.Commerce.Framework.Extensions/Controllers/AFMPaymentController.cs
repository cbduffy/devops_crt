﻿
namespace AFM.Commerce.Framework.Extensions.Controllers
{

    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.CRTServices;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.PaymentSDK;
    using Microsoft.Dynamics.Retail.PaymentSDK.Constants;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Linq;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.SDKManager;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using AFM.Commerce.Framework.Extensions.Utils;

    /// <summary>
    /// Controller for PaymentGateway operations.
    /// </summary>
    public static class AFMPaymentController
    {

        /// <summary>
        /// Gets the card Token Data for the payment processing
        /// </summary>
        /// <param name="cartId">The cart Id Data</param>
        /// <returns>
        /// The updated card token data.
        /// </returns>
        public static AFMCardTokenData GetCardToken(string tranId)
        {
            if (string.IsNullOrWhiteSpace(tranId))
            {
                throw new ArgumentNullException("tranId");
            }

            var cardTokenData = new AFMCardTokenData();
            try
            {
                //PaymentProcessorManager.Create(".");
                // CS by muthait 14Nov2014
                // Passing dummy value, but will be ignored by the Ashley Payment Controller
                var tenderLine = new TenderLine()
                {
                    CardTypeId = "Visa",
                    Currency = "USD",
                };
                // Create a payment card.
                var paymentCard = new PaymentCard()
                {
                    //AdditionalSecurityData = checkOutCart.CartId,
                    CardNumber = "0000000000000000",
                    Country = "US",
                };
                paymentCard["TranId"] = tranId;
                paymentCard["CardToken"] = "00";
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                RequestContext reqContext = new RequestContext(runtime);
                var generateCardTokenRequest = new GenerateCardTokenPaymentServiceRequest(tenderLine: tenderLine, paymentCard: paymentCard);
                reqContext.SetPrincipal(runtime.CurrentPrincipal);
                var CardTokenResponse = runtime.Execute<GenerateCardTokenPaymentServiceResponse>(generateCardTokenRequest, reqContext,true);
                
                // CE by muthait dated 14Nov2014
                if (CardTokenResponse.TenderLine != null)
                {
                        PaymentProperty[] propertySet = PaymentProperty.ConvertXMLToPropertyArray(CardTokenResponse.TenderLine.Authorization);
                        Hashtable tableResponse = PaymentProperty.ConvertToHashtable(propertySet);
                        cardTokenData.Last4Digits = PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.Last4Digits).ToString();
                        cardTokenData.ExpirationMonth = PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.ExpirationMonth).ToString();
                        cardTokenData.ExpirationYear = PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.ExpirationYear).ToString();
                        cardTokenData.Name = PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.Name).ToString();
                        cardTokenData.CardToken = PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.CardToken).ToString();
                        cardTokenData.CardType = PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.CardType).ToString();
                        cardTokenData.UniqueCardId = PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, PaymentCardProperties.UniqueCardId).ToString();
                        // Get FinacingOptionId from payment properties
                        //NS by spriya : DMND0010113 - Synchrony Financing online
                        cardTokenData.FinancingOptionId = PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, "FinancingOptionId").ToString();
                        IEnumerable<AFMSynchronyOptions> SyncOptions =null;
                        if (!string.IsNullOrEmpty(cardTokenData.FinancingOptionId))
                        SyncOptions = AFMDataUtilities.GetSynchronyOptions().Where(t => t.FinancingOptionId == cardTokenData.FinancingOptionId).Select
                            (t => new AFMSynchronyOptions{PromoDesc1 = t.PromoDesc1 , RemoveCartDiscount = t.RemoveCartDiscount, MinimumPurchaseAmount=t.MinimumPurchaseAmount,CardName=t.CardName,
                            PromoDesc2=t.PromoDesc2,PromoDesc3=t.PromoDesc3,LearnMore=t.LearnMore,PaymMode=t.PaymMode,PaymentFee=t.PaymentFee,PaymTermId=t.PaymTermId,
                            CardType=t.CardType,ThirdPartyTermCode=t.ThirdPartyTermCode});                        
                        AFMSynchronyOptions syncOptionData = null;
                        if(SyncOptions != null)
                           syncOptionData = SyncOptions.ToList().First();
                        if (!string.IsNullOrEmpty(cardTokenData.FinancingOptionId))
                        cardTokenData.Name = syncOptionData != null ? syncOptionData.CardName : string.Empty;
                        cardTokenData.SynchronyPromoDesc = syncOptionData != null ? syncOptionData.PromoDesc1 : string.Empty;
                        cardTokenData.SynchronyMinPurchase = syncOptionData != null ? syncOptionData.MinimumPurchaseAmount : 0;
                        cardTokenData.IsMultiAuth = Convert.ToBoolean(PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, "SupportsMultipleAuthorizations").ToString());
                        cardTokenData.SynchronyTnC = PaymentProperty.GetPropertyFromHashtable(tableResponse, GenericNamespace.PaymentCard, "FinancingTerms").ToString();
                        cardTokenData.RemoveCartDiscount = syncOptionData != null ? syncOptionData.RemoveCartDiscount : false;
                        AFMDataUtilities.SetFinanceOptionId(cardTokenData.FinancingOptionId);
                        AFMUserCartData userCartInfo = AFMDataUtilities.GetUserCartInfo();
                        userCartInfo.FinanceOptionId = cardTokenData.FinancingOptionId;
                        userCartInfo.IsMultiAuth = cardTokenData.IsMultiAuth;
                        userCartInfo.SynchronyTnC = cardTokenData.SynchronyTnC;
                        userCartInfo.PromoDesc1 = cardTokenData.SynchronyPromoDesc;
                        userCartInfo.CardName = cardTokenData.Name;
                        userCartInfo.RemoveCartDiscount = cardTokenData.RemoveCartDiscount;
                        userCartInfo.MinimumPurchaseAmount = syncOptionData != null ? syncOptionData.MinimumPurchaseAmount : 0;
                        userCartInfo.PromoDesc2 = syncOptionData != null ? syncOptionData.PromoDesc2: string.Empty;
                        userCartInfo.PromoDesc3 = syncOptionData != null ? syncOptionData.PromoDesc3 : string.Empty;
                        userCartInfo.LearnMore = syncOptionData != null ? syncOptionData.LearnMore : string.Empty;
                        userCartInfo.PaymentFee = syncOptionData != null ? syncOptionData.PaymentFee : string.Empty;
                        userCartInfo.PaymMode = syncOptionData != null ? syncOptionData.PaymMode : string.Empty;
                        userCartInfo.PaymTermId = syncOptionData != null ? syncOptionData.PaymTermId : string.Empty;
                        userCartInfo.ThirdPartyTermCode = syncOptionData != null ? syncOptionData.ThirdPartyTermCode : string.Empty;
                        userCartInfo.CardType = syncOptionData != null ? syncOptionData.CardType : string.Empty; 
                        AFMDataUtilities.SetUserCartInfo(userCartInfo);
                        
                        //NE by spriya : DMND0010113 - Synchrony Financing online
                    //cardTokenData.Last4Digits = CardTokenResponse.TokenizedPaymentCard.CardTokenInfo.MaskedCardNumber;
                    //cardTokenData.ExpirationYear = CardTokenResponse.TokenizedPaymentCard.ExpirationYear.ToString();
                    //cardTokenData.ExpirationMonth = CardTokenResponse.TokenizedPaymentCard.ExpirationMonth.ToString();
                    //cardTokenData.Name = CardTokenResponse.TokenizedPaymentCard.NameOnCard;
                    //cardTokenData.CardToken = CardTokenResponse.TokenizedPaymentCard.CardTokenInfo.CardToken;
                    //cardTokenData.CardType = CardTokenResponse.TokenizedPaymentCard.CardTypes;
                }

                return cardTokenData;
            }
            catch(PaymentException ex)
            {
               
                cardTokenData.Error = ex.Message;
                Logging.LoggerUtilities.ProcessLogMessage(
                       " transId : " + tranId + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                return cardTokenData;

            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       " transId : " + tranId + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                throw;
            }
        }
    }
}