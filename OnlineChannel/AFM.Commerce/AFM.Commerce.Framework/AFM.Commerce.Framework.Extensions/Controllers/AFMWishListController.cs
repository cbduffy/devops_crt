﻿using AFM.Commerce.Entities;
using AFM.Commerce.Framework.Extensions.Mappers;
using AFM.Commerce.Framework.Extensions.Utils;
using Microsoft.Dynamics.Commerce.Runtime.Client;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Retail.Diagnostics;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Extensions.Controllers
{
    /// <summary>
    /// Customized Wishlist controler class
    /// </summary>
    public class AFMWishListController : WishListController
    {
        /// <summary>
        /// Adds items from wish list to Cart.
        /// </summary>
        /// <param name="shoppingCartId">Cart Id</param>
        /// <param name="customerId">Logged in Customer</param>
        /// <param name="listings">item listings</param>
        /// <param name="dataLevel">Shopping cart datalevel</param>
        /// <param name="productValidator">product validator class</param>
        /// <param name="searchEngine">Search engine</param>
        /// <param name="wishListId">selected wishlist id</param>
        public ShoppingCart AddItemToCartFromWhishList(string shoppingCartId, string customerId,
            Collection<ViewModel.Listing> listings, ShoppingCartDataLevel dataLevel, IProductValidator productValidator,
            ISearchEngine searchEngine, string wishListId)
        {
            if (!AFMDataUtilities.IsValidString(customerId))
            {
                throw new ArgumentException("Invalid customer id", "customerId");
            }
            if (!AFMDataUtilities.IsValidString(wishListId))
            {
                throw new ArgumentException("Invalid wishlist id", "wishListId");
            }

            if (listings == null)
            {
                throw new ArgumentNullException("listings");
            }
            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }
            if (productValidator == null)
            {
                throw new ArgumentNullException("productValidator");
            }
            try
            {
                AFMShoppingCartController afmShoppingCartController = new AFMShoppingCartController();
                //AFMAtpOutputContainer atpOC = null;
                string zipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
                ShoppingCart cart = new ShoppingCart();

                //Collection<ViewModel.Listing> selectedlistings = afmShoppingCartController.GetListingsOrderSettings(listings);
                cart = afmShoppingCartController.AddItems(shoppingCartId,
                    customerId, listings, dataLevel,
                    productValidator, searchEngine, false);

                //Remove the item from the WishList
                List<ViewModel.Listing> selectedItemList = listings.ToList();
                RemoveItemFromWishList(wishListId, customerId, selectedItemList[0].ListingId.ToString());

                return cart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "ShoppingCartId : " + shoppingCartId + " Customer ID : " + customerId + " Items : " +
                                    listings.Count + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Gets the wish list.
        /// </summary>
        /// <param name="wishListId">The wish list identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// The wish list.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// wishListId
        /// or
        /// customerId
        /// </exception>
        public override WishList GetWishList(string wishListId, string customerId, ISearchEngine searchEngine)
        {
            if (!AFMDataUtilities.IsValidString(wishListId))
            {
                throw new ArgumentException("Invalid wishlist id", "wishListId");
            }

            if (!AFMDataUtilities.IsValidString(customerId))
            {
                throw new ArgumentException("Invalid customer id", "customerId");
            }
            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();

                CommerceListManager commerceListManager = CommerceListManager.Create(runtime);
                CrtDataModel.CommerceList wishListDM = commerceListManager.GetCommerceList(wishListId, customerId);

                WishList wishList = AFMWishListMapper.ConvertToViewModel(wishListDM);
                IEnumerable<long> productIds = wishList.WishListLines.Select(wll => wll.ProductId);
                IEnumerable<CrtDataModel.Product> products = new AFMShoppingCartMapper(new SiteConfiguration()).GetProducts(productIds);

                PopulateWishListDetailsFromProducts(wishList, products, searchEngine);

                return wishList;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage("WishList Id : " + wishListId + " Customer Id : " + customerId + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Gets the wish lists corresponding to customer.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>
        /// The wish lists.
        /// </returns>
        public override Collection<WishList> GetWishListsForCustomer(string customerId, ISearchEngine searchEngine)
        {
            if (!AFMDataUtilities.IsValidString(customerId))
            {
                throw new ArgumentException("Invalid customer id", "customerId");
            }
            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                CommerceListManager manager = CommerceListManager.Create(runtime);

                Collection<WishList> wishLists = new Collection<WishList>();
                ReadOnlyCollection<CrtDataModel.CommerceList> wishListsDM = manager.GetCommerceListsByCustomer(customerId, false, false);

                Collection<long> productIds = new Collection<long>();
                foreach (CrtDataModel.CommerceList wishListDM in wishListsDM)
                {
                    WishList wishList = AFMWishListMapper.ConvertToViewModel(wishListDM);
                    wishLists.Add(wishList);

                    var wishListLines = wishList.WishListLines;
                    if (wishListLines != null)
                    {
                        productIds.AddRange(wishListLines.Select(item => item.ProductId));
                    }
                }

                IEnumerable<CrtDataModel.Product> products = new AFMShoppingCartMapper(new SiteConfiguration()).GetProducts(productIds);
                PopulateWishListDetailsFromProducts(wishLists, products, searchEngine);

                return wishLists;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage("Customer ID : " + customerId + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        protected override void PopulateWishListDetailsFromProducts(IEnumerable<WishList> wishLists, IEnumerable<CrtDataModel.Product> products, ISearchEngine searchEngine)
        {
            if (wishLists == null)
            {
                throw new ArgumentNullException("wishLists");
            }

            if (products == null)
            {
                throw new ArgumentNullException("products");
            }

            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }
            try
            {
                foreach (WishList wishList in wishLists)
                {
                    PopulateWishListDetailsFromProducts(wishList, products, searchEngine);
                }
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "No of wishlists : " + wishLists.Count() + " No of Products : " + products.Count() + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                throw;
            }
        }

        protected override void PopulateWishListDetailsFromProducts(WishList wishList, IEnumerable<CrtDataModel.Product> products, ISearchEngine searchEngine)
        {
            if (wishList == null)
            {
                throw new ArgumentNullException("wishList");
            }

            if (products == null)
            {
                throw new ArgumentNullException("products");
            }

            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }
            try
            {
                string currentCulture = new SiteConfiguration().GetCurrentCulture();

                foreach (WishListLine wishListLine in wishList.WishListLines)
                {
                    CrtDataModel.Product wishListLineProduct;

                    // Check if the wish list line product is a standalone non variant product
                    wishListLineProduct = products.Where(p => p.RecordId == wishListLine.ProductId).FirstOrDefault();

                    if (wishListLineProduct == null)
                    {
                        // This means that the wish list product could be a product variant.
                        CrtDataModel.ProductVariant wishListLineProductVariant = null;

                        foreach (Product product in products)
                        {
                            if (product.IsMasterProduct)
                            {
                                wishListLineProductVariant = product.CompositionInformation.VariantInformation.Variants.Where(v => v.DistinctProductVariantId == wishListLine.ProductId).SingleOrDefault();
                                if (wishListLineProductVariant != null)
                                {
                                    wishListLineProduct = product;
                                    break;
                                }
                            }
                        }

                        if (wishListLineProduct == null || wishListLineProductVariant == null)
                        {
                            // This is a valid scenario. This would happen if the wish list line product belongs to a different channel or is no longer availalbe on this channel.
                            // Assigning default values.
                            // [Todo][anandjo][1016961] Wishlist line should have the product name on it, or we should have some method of resolving the product id into a name.
                            wishListLine.Name = string.Format(AFM.Commerce.Framework.Extensions.Resource.ProductNameUnavailableTemplate, wishListLine.ProductId);
                            wishListLine.Description = AFM.Commerce.Framework.Extensions.Resource.ProductUnavailableDescription;

                            NetTracer.Warning("Provided product {0} could not be resolved into product details.", wishListLine.ProductId);
                        }
                        else
                        {
                            wishListLine.Name = wishListLineProduct.ProductName;
                            wishListLine.Description = wishListLineProduct.Description;
                            wishListLine.ProductNumber = wishListLineProduct.ProductNumber;
                            wishListLine.PriceWithCurrency = wishListLineProductVariant.BasePrice.ToCurrencyString(currentCulture);
                            wishListLine.TotalWithCurrency = (wishListLineProductVariant.AdjustedPrice * wishListLine.Quantity).ToCurrencyString(currentCulture);
                            wishListLine.Color = wishListLineProductVariant.Color;
                            wishListLine.Style = wishListLineProductVariant.Style;
                            wishListLine.Size = wishListLineProductVariant.Size;
                        }
                    }
                    else
                    {
                        wishListLine.Name = wishListLineProduct.ProductName;
                        wishListLine.PriceWithCurrency = wishListLineProduct.BasePrice.ToCurrencyString(currentCulture);
                        wishListLine.TotalWithCurrency = (wishListLineProduct.AdjustedPrice * wishListLine.Quantity).ToCurrencyString(currentCulture);
                    }

                    PopulateWishListLineDetailsFromSearchResults(wishListLine, searchEngine);
                }
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage("Wishlist Name : " + wishList.Name + " No of Products : " + products.Count() + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Populates the wish list line details from search results.
        /// </summary>
        /// <param name="wishListLine">The wish list line.</param>
        protected override void PopulateWishListLineDetailsFromSearchResults(WishListLine wishListLine, ISearchEngine searchEngine)
        {
            if (wishListLine == null)
            {
                throw new ArgumentNullException("wishListLine");
            }

            if (searchEngine == null)
            {
                throw new ArgumentNullException("searchEngine");
            }
            try
            {
                // If the product details could not be found, then it is a valid scenario. 
                // Reason: The product could no longer be available on the current channel; or the product could have been added from a different channel by the same customer.
                bool isResultManadatory = false;
                IDictionary<string, object> properties = searchEngine.GetListingProperties(isResultManadatory, wishListLine.ProductId,
                    searchEngine.PathSearchPropertyName,
                    searchEngine.ImageSearchPropertyName,
                    searchEngine.DescriptionSearchPropertyName,
                    searchEngine.TitleSearchPropertyName,
                    searchEngine.ColorSearchPropertyName,
                    searchEngine.SizeSearchPropertyName,
                    searchEngine.StyleSearchPropertyName,
                    searchEngine.ConfigurationSearchPropertyName);

                if (properties != null)
                {
                    // Update product url.
                    object pathValue;
                    if (properties.TryGetValue(searchEngine.PathSearchPropertyName, out pathValue))
                    {
                        wishListLine.ProductUrl = (string)pathValue;
                    }

                    // Update image url.
                    object imageSearchValue;
                    if (properties.TryGetValue(searchEngine.ImageSearchPropertyName, out imageSearchValue))
                    {
                        wishListLine.Image = (ImageInfo)imageSearchValue;
                    }

                    // Update product description.
                    object descriptionValue;
                    if (properties.TryGetValue(searchEngine.DescriptionSearchPropertyName, out descriptionValue))
                    {
                        wishListLine.Description = (string)descriptionValue;
                    }

                    // Update product title.
                    object titleSearchValue;
                    if (properties.TryGetValue(searchEngine.TitleSearchPropertyName, out titleSearchValue))
                    {
                        wishListLine.Name = (string)titleSearchValue;
                    }

                    // Update product color.
                    object colorSearchValue;
                    if (properties.TryGetValue(searchEngine.ColorSearchPropertyName, out colorSearchValue))
                    {
                        wishListLine.Color = (string)colorSearchValue;
                    }

                    // Update product size.
                    object sizeSearchValue;
                    if (properties.TryGetValue(searchEngine.SizeSearchPropertyName, out sizeSearchValue))
                    {
                        wishListLine.Size = (string)sizeSearchValue;
                    }

                    // Update product style.
                    object styleSearchValue;
                    if (properties.TryGetValue(searchEngine.StyleSearchPropertyName, out styleSearchValue))
                    {
                        wishListLine.Style = (string)styleSearchValue;
                    }

                    // Update product configuration.
                    object configurationSearchValue;
                    if (properties.TryGetValue(searchEngine.ConfigurationSearchPropertyName, out configurationSearchValue))
                    {
                        wishListLine.Configuration = (string)configurationSearchValue;
                    }
                }
                else
                {
                    // This is a valid scenario. This would happen if the wish list line product belongs to a different channel or is no longer available on this channel.
                    // Assigning default values.
                    wishListLine.ProductUrl = string.Empty;
                    wishListLine.Image = new ImageInfo();

                    NetTracer.Information("No search results were found for product {0} while getting wish list line details. Using fall back values.", wishListLine.ProductId);
                }
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage("Wishlist ProductNumber : " + wishListLine.ProductNumber + Environment.NewLine + ex.Message + Environment.NewLine + ex.StackTrace);
                throw;
            }
        }
    }
}
