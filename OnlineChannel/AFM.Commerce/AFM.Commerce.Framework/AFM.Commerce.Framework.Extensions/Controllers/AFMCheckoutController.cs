﻿namespace AFM.Commerce.Framework.Extensions.Controllers
{

    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Framework.Core.CRTServices;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Mappers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using CustomDataModel = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Linq;
    using System.Resources;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using AFM.Commerce.Framework.Extensions.Mappers;
    using AFM.Commerce.Framework.Extensions.Utils;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using System.Web;
    using AFM.Commerce.Framework.Core.ClientManager;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using System.Web.Script.Serialization;
    using System.Transactions;
    using System.Dynamic;
    using System.Text.RegularExpressions;
    using System.Globalization;

    /// <summary>
    /// Controller for Checkout operations.
    /// </summary>
    public class AFMCheckoutController : CheckoutController
    {
        public const string ErrorCheck = "Error:";
        // <summary>
        /// Gets the delivery methods.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart id.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="selectOrderShippingOptions">The selected order shipping options.</param>
        /// <returns>
        /// The available delivery methods.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId is null or empty.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "This specific type is required here.")]

        public AFMDeliveryOptions GetDeliveryMethods(string shoppingCartId, string customerId, SelectedDeliveryOption selectOrderShippingOptions)
        {

            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (!AFMDataUtilities.IsValidString(customerId))
            {
                throw new ArgumentException("Invalid customer id", "customerId");
            }

            if (selectOrderShippingOptions == null)
            {
                throw new ArgumentNullException("selectOrderShippingOptions");
            }
            try
            {
                AFMDeliveryOptions resultSet = new AFMDeliveryOptions();

                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                Collection<DeliveryOption> deliveryMethods = new Collection<DeliveryOption>();

                CrtDataModel.Address address = new CheckoutController().GetDeliveryAddress(selectOrderShippingOptions);

                manager.UpdateCartShippingAddress(shoppingCartId, customerId, address, selectOrderShippingOptions.DeliveryModeId, CrtDataModel.CalculationModes.None);
                /*NS Developed by  spriya for AFM_TFS_46529 dated 6/19/2014 */
                IEnumerable<CrtDataModel.DeliveryOption> deliveryOptions = null;
                deliveryOptions = manager.GetOrderDeliveryOptions(shoppingCartId, customerId);
                resultSet.DeliveryMethods = new ShoppingCartMapper(this.Configuration).ConvertToViewModel(deliveryOptions);
                resultSet.AddressVerficationResult = true;

                return resultSet;

            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "ShoppingCartId : " + shoppingCartId + " Customer ID : " + customerId + " DeliveryMethodId : " +
                                    selectOrderShippingOptions.DeliveryModeId + " DeliveryPreferanceID : " + selectOrderShippingOptions.DeliveryPreferenceId + Environment.NewLine +
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Gets delivery options info.
        /// </summary>
        /// <returns>
        /// The <see cref="AFMDeliveryOptions"/>.
        /// </returns>
        public AFMDeliveryOptions AFMGetDeliveryOptionsInfo()
        {
            try
            {
                var resultSet = new AFMDeliveryOptions();
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                IEnumerable<CrtDataModel.DeliveryOption> deliveryOptions = manager.GetAllDeliveryOptions();
                resultSet.DeliveryMethods = new ShoppingCartMapper(this.Configuration).ConvertToViewModel(deliveryOptions);
                /*NS Developed by  spriya for AFM_TFS_46529 dated 6/19/2014 */
                resultSet.AddressVerficationResult = true;
                return resultSet;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);
                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }


        // NS by muthait dated 10Oct2014

        /// <summary>
        /// Sets the shipping option per item.
        /// </summary>
        /// <param name="shoppingCartId">
        /// The shopping cart id.
        /// </param>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <param name="shippingOptions">
        /// The shipping options.
        /// </param>
        /// <param name="productValidator">
        /// The product Validator.
        /// </param>
        /// <param name="searchEngine">
        /// The search Engine.
        /// </param>
        /// <param name="dataLevel">
        /// The data Level.
        /// </param>
        /// <returns>
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// shoppingCartId
        /// or
        /// shippingOptions
        /// </exception>
        public ShoppingCart SetShippingOptionPerItem(string shoppingCartId, string customerId, SelectedLineDeliveryOption shippingOptions, IProductValidator productValidator, ISearchEngine searchEngine, ShoppingCartDataLevel dataLevel)
        {
            if (shippingOptions == null)
            {
                throw new ArgumentNullException("shippingOptions");
            }
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (!AFMDataUtilities.IsValidString(customerId))
            {
                throw new ArgumentException("Invalid customer id", "customerId");
            }
            try
            {
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                AFMShoppingCartController objshoppingCartController = new AFMShoppingCartController();

                // TODO: Bug 893642:The price field is not nullable on the cartline and so it does not preserve the value if not explicity set
                // so we need to fetch the cart and update the cartline
                CrtDataModel.Cart currentCart = manager.GetCart(shoppingCartId, customerId, CrtDataModel.CalculationModes.None);

                //var shippingOptionList = new List<SelectedLineDeliveryOption> { shippingOptions }.AsEnumerable();
                UpdateCartLineShippingAddressesData(shippingOptions, currentCart.CartLines);

                //NS Developed by spriya Bug:59347
                AFMDataUtilities.SetAFMZipCodeInCookie(shippingOptions.CustomAddress.ZipCode);
                shippingOptions.CustomAddress.ZipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();

                //NE Developed by spriya
                base.SetOrderDeliveryOption(shoppingCartId, customerId, shippingOptions, productValidator, searchEngine, dataLevel);

                //Update when ZipCode is Changed at time of Shipping to Billling --Add by Hardik for performance Update
                ICollection<CrtDataModel.CartLine> cartLinetoUpdate = currentCart.CartLines;
                objshoppingCartController.UpdateATPDataCartLine(ref cartLinetoUpdate);

                //Update when ZipCode is Changed
                CrtDataModel.Cart cart = manager.UpdateCartLines(shoppingCartId, customerId, currentCart.CartLines, CrtDataModel.CalculationModes.All);
                ShoppingCart shoppingCart = new AFMShoppingCartMapper(this.Configuration).ConvertToViewModel(cart, dataLevel, productValidator, false);

                //NS v-hapat Calculate Shipping Chage when you update shipping option                
                objshoppingCartController.AddEstShippingCostToCart(shoppingCart, cart, customerId, true);
                //NS v-hapat Calculate Shipping Chage when you update shipping option
                objshoppingCartController.AddATPforEcommerce(shoppingCart, cart);

                //CS spriya Dated 10/07/2015 Bug:101577 "See Store for Availability" message but still able to submit sale order
                if (cart[CartConstant.AvaTaxError] != null)
                {
                    if (!string.IsNullOrEmpty(cart[CartConstant.AvaTaxError].ToString()))
                        shoppingCart.AvalaraTaxError = cart[CartConstant.AvaTaxError].ToString();
                }

                foreach (TransactionItem item in shoppingCart.Items)
                {
                    if (item.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                    {
                        if (item.AFMCartLineATP != null)
                        {
                            if (!item.AFMCartLineATP.IsAvailable)
                                shoppingCart.ATPError = CartConstant.AtpItemNotAvailableMrssage;
                        }
                        else
                        {
                            if (!Convert.ToBoolean(ConfigurationManager.AppSettings["ByPassATP"]))
                                shoppingCart.ATPError = CartConstant.AtpServiceNotAvailableErrorMrssage;
                        }
                    }
                }

                if (cart[CartConstant.ChargeTypeError] != null && !string.IsNullOrEmpty(cart[CartConstant.ChargeTypeError].ToString()))
                {
                    shoppingCart.ChargeTypeError = cart[CartConstant.ChargeTypeError].ToString();
                }
                var ServiceItem = shoppingCart.Items.Where(p => p.AFMItemType == Convert.ToInt32(AFMItemType.DeliveryServiceItem)).ToList();
                if ((ServiceItem == null || ServiceItem.Count == 0) && (shoppingCart.Items != null && shoppingCart.Items.Count > 0))
                    shoppingCart.ChargeTypeError = CartConstant.AtpItemNotAvailableMrssage;

                //CE spriya Bug:101577 Dated 10/07/2015 
                return shoppingCart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "ShoppingCartId : " + shoppingCartId + " Customer ID : " + customerId + " DeliveryMethodId : " +
                                    shippingOptions.DeliveryModeId + " DeliveryPreferanceID : " + shippingOptions.DeliveryPreferenceId + Environment.NewLine +
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        // NE by muthait dated 10Oct2014

        // NS by muthait dated 05Nov2014
        /// <summary>
        /// Updates cart lines shipping addresses.
        /// </summary>
        /// <param name="selectedLineDeliveryOptions">The selected shipping options.</param>   
        /// <param name="lines">Cartlines to be updated.</param>
        protected void UpdateCartLineShippingAddressesData(SelectedLineDeliveryOption lineDeliveryOption, IEnumerable<CrtDataModel.CartLine> lines)
        {
            if (lineDeliveryOption == null)
            {
                throw new ArgumentNullException("shippingOptions");
            }
            if (lines.Count() < 1 || lines == null)
            {
                throw new ArgumentNullException("lines");
            }

            ValidateSelectedDeliveryOption(lineDeliveryOption);
            CrtDataModel.Address shippingAddress = GetDeliveryAddress(lineDeliveryOption);
            CrtDataModel.DeliveryPreferenceType selectedDeliveryPreferenceType = GetDeliveryPreferenceType(lineDeliveryOption);
            foreach (var item in lines)
            {
                item.LineData.ShippingAddress = shippingAddress;
                if (selectedDeliveryPreferenceType == CrtDataModel.DeliveryPreferenceType.PickupFromStore)
                {
                    item.LineData.StoreNumber = lineDeliveryOption.StoreAddress.StoreId;
                }
                if (selectedDeliveryPreferenceType == CrtDataModel.DeliveryPreferenceType.ElectronicDelivery)
                {
                    item.LineData.ElectronicDeliveryEmail = lineDeliveryOption.ElectronicDeliveryEmail;
                    item.LineData.ElectronicDeliveryEmailContent = lineDeliveryOption.ElectronicDeliveryEmailContent;
                }
            }
        }
        // NE by muthait dated 05Nov2014

        /// <summary>
        /// Sets the shipping option for the whole shopping cart.
        /// </summary>
        /// <param name="shoppingCartId">
        /// The shopping cart identifier.
        /// </param>
        /// <param name="customerId">
        /// The customer id.
        /// </param>
        /// <param name="lineDeliveryOptions">
        /// The line Delivery Options.
        /// </param>
        /// <param name="productValidator">
        /// The product Validator.
        /// </param>
        /// <param name="searchEngine">
        /// The search Engine.
        /// </param>
        /// <param name="dataLevel">
        /// The shopping cart data level.
        /// </param>
        /// <returns>
        /// The updated shopping cart.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">
        /// Thrown when the shoppingCartId or shippingOptions is null or empty.
        /// </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "This specific type is required here.")]
        public override ShoppingCart SetLineDeliveryOptions(string shoppingCartId, string customerId, IEnumerable<SelectedLineDeliveryOption> lineDeliveryOptions, IProductValidator productValidator, ISearchEngine searchEngine, ShoppingCartDataLevel dataLevel)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }

            if (!AFMDataUtilities.IsValidString(customerId))
            {
                throw new ArgumentException("Invalid customer id", "customerId");
            }

            if (lineDeliveryOptions == null)
            {
                throw new ArgumentNullException("shippingOptions");
            }
            try
            {
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                CrtDataModel.Cart currentCart = manager.GetCart(shoppingCartId, customerId, CrtDataModel.CalculationModes.None);
                UpdateCartLineShippingAddresses(lineDeliveryOptions, currentCart.CartLines);
                CrtDataModel.Cart cart = manager.UpdateCartLines(shoppingCartId, customerId, currentCart.CartLines, CrtDataModel.CalculationModes.All);

                ShoppingCart shoppingCart;
                if (dataLevel == ShoppingCartDataLevel.All)
                {
                    shoppingCart = this.ShoppingCartController.AddKitDetailsToShoppingCart(cart, productValidator, searchEngine);
                }
                else
                {
                    ShoppingCartMapper mapper = new ShoppingCartMapper(this.Configuration);
                    shoppingCart = mapper.ConvertToViewModel(cart, dataLevel, productValidator, searchEngine);
                }

                // CS by muthait dated 01NOV2014
                new AFMShoppingCartController().CalculateShippingCharges(shoppingCart, cart, true);
                // CE by muthait dated 01Nov2014

                return shoppingCart;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "ShoppingCartId : " + shoppingCartId + " Customer ID : " + customerId + " DeliveryMethodId : " + Environment.NewLine +
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);
                throw;
            }
        }

        //NS Developed by spriya Dated 10/10/2014
        CustomDataModel.Customer AFMCreateCustomer(AFMSalesTransHeader headerValues)
        {
            if (headerValues == null)
            {
                throw new ArgumentNullException("headerValues");
            }

            CustomDataModel.Customer customer = new CustomDataModel.Customer();
            AFMUserCartData userCartData = AFMDataUtilities.GetUserCartInfo();

            if (userCartData != null)
            {
                if (userCartData.ShippingAddress != null)
                {
                    userCartData.ShippingAddress.Phone = AFMDataUtilities.SetPhoneFormat(userCartData.ShippingAddress.Phone);
                    userCartData.ShippingAddress.Phone2 = AFMDataUtilities.SetPhoneFormat(userCartData.ShippingAddress.Phone2);
                    userCartData.ShippingAddress.Phone3 = AFMDataUtilities.SetPhoneFormat(userCartData.ShippingAddress.Phone3);
                }
                if (userCartData.PaymentAddress != null)
                {
                    userCartData.PaymentAddress.Phone = AFMDataUtilities.SetPhoneFormat(userCartData.PaymentAddress.Phone);
                    userCartData.PaymentAddress.Phone2 = AFMDataUtilities.SetPhoneFormat(userCartData.PaymentAddress.Phone2);
                    userCartData.PaymentAddress.Phone3 = AFMDataUtilities.SetPhoneFormat(userCartData.PaymentAddress.Phone3);
                }
                userCartData.PaymentAddress.AddressType = CustomDataModel.AddressType.Invoice;
                userCartData.ShippingAddress.AddressType = CustomDataModel.AddressType.Delivery;
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["IsShippingAddressAsPrimary"]))
                {
                    userCartData.ShippingAddress.IsPrimary = true;
                    userCartData.PaymentAddress.IsPrimary = false;
                }
                else
                {
                    userCartData.ShippingAddress.IsPrimary = false;
                    userCartData.PaymentAddress.IsPrimary = true;
                }
            }

            if (userCartData.PaymentAddress != null)
            {
                customer.Phone = userCartData.PaymentAddress.Phone;
                customer.Email = userCartData.PaymentAddress.Email;
                customer.FirstName = userCartData.PaymentAddress.FirstName;
                customer.LastName = userCartData.PaymentAddress.LastName;
            }

            //NS developed by v-hapat for BUG-63467 on dated 11/13/2014
            if (userCartData.IsOptinDeals != null)
            {
                customer.IsMarketingOptIn = false; //Hardik : Bug 94142:WSR - Remove "Opt-in" Check Box (Billing Page) - CR 320 (INC0132820) //userCartData.IsOptinDeals;
                //NS developed by v-hapat for CR 72099 
                if (customer.IsMarketingOptIn)
                {
                    customer.IsMarketingOptInVersionId = headerValues.IsOptinDealsVersionId == null ? string.Empty : headerValues.IsOptinDealsVersionId;
                    customer.IsMarketingOptInUniqueId = headerValues.IsOptinDealsUniqueId == null ? string.Empty : headerValues.IsOptinDealsUniqueId;
                    customer.IsMarketingOptInDate = DateTime.Now;
                }
                else
                {
                    customer.IsMarketingOptInVersionId = string.Empty;
                    customer.IsMarketingOptInUniqueId = string.Empty;
                    customer.IsMarketingOptInDate = Convert.ToDateTime("1900-01-01");
                }
                //NE developed by v-hapat for CR 72099 

            }
            //NE developed by v-hapat for BUG-63467 on dated 11/13/2014

            if (string.IsNullOrEmpty(AFMDataUtilities.GetCustomerIdFromCookie()))
            {

                customer.IsGuestCheckout = true;

                customer = new AFMCustomerController().CreateCustomer(customer, customer.Email);

                if (customer != null)
                {
                    AFMDataUtilities.SetCustomerIdTempInCookie(customer.AccountNumber);
                    List<CustomDataModel.Address> addresses = new List<CustomDataModel.Address>();
                    addresses.Add(userCartData.ShippingAddress);
                    addresses.Add(userCartData.PaymentAddress);
                    customer = new AFMCustomerController().UpdateAddresses(customer, addresses);
                    userCartData.ShippingAddress = customer.Addresses.SingleOrDefault(p => p.AddressType == AddressType.Delivery);
                    userCartData.PaymentAddress = customer.Addresses.SingleOrDefault(p => p.AddressType == AddressType.Invoice);
                }
            }

            if (userCartData != null)
                AFMDataUtilities.SetUserCartInfo(userCartData);
            return customer;
        }

        /// <summary>
        /// Update Signed in Customer
        /// </summary>
        /// <returns></returns>
        CustomDataModel.Customer AFMUpdateSignedCustomer(string customerId, AFMSalesTransHeader headerValues)
        {
            if (String.IsNullOrEmpty(customerId))
            {
                throw new ArgumentNullException("customerId");
            }
            if (headerValues == null)
            {
                throw new ArgumentNullException("headerValues");
            }


            AFMCustomerController afmCustomerController = new AFMCustomerController();
            CustomDataModel.Customer signedInCustomer = afmCustomerController.GetCustomer(customerId);
            AFMUserCartData userCartData = AFMDataUtilities.GetUserCartInfo();
            //CS Hardik : Bug 94142:WSR - Remove "Opt-in" Check Box (Billing Page) - CR 320 (INC0132820) 
            //NS developed by v-hapat for CR 72099             
            //signedInCustomer.IsMarketingOptIn = userCartData.IsOptinDeals;
            //if (headerValues != null) 
            //{
            //signedInCustomer.IsMarketingOptInUniqueId = headerValues.IsOptinDealsUniqueId;
            //signedInCustomer.IsMarketingOptInVersionId = headerValues.IsOptinDealsVersionId;
            //signedInCustomer.IsMarketingOptInDate = DateTime.Now;
            //}
            //NE developed by v-hapat for CR 72099             
            //CE Hardik : Bug 94142:WSR - Remove "Opt-in" Check Box (Billing Page) - CR 320 (INC0132820) 
            userCartData.PaymentAddress.AddressType = CustomDataModel.AddressType.Invoice;
            userCartData.PaymentAddress.Name = userCartData.PaymentAddress.FirstName + userCartData.PaymentAddress.LastName;


            //Assign invoice address recordID if it exists for profiled customer
            if (signedInCustomer != null && signedInCustomer.Addresses != null)
            {
                Address existingAddress = signedInCustomer.Addresses.FirstOrDefault(p => p.AddressType == AddressType.Invoice);
                if (existingAddress != null && existingAddress.RecordId > 0)
                    userCartData.PaymentAddress.RecordId = existingAddress.RecordId;
            }

            userCartData.ShippingAddress.AddressType = CustomDataModel.AddressType.Delivery;
            userCartData.ShippingAddress.Name = userCartData.ShippingAddress.FirstName + userCartData.ShippingAddress.LastName;
            List<CustomDataModel.Address> address = new List<CustomDataModel.Address>();
            if (userCartData.ShippingAddress != null)
            {
                userCartData.ShippingAddress.Phone = AFMDataUtilities.SetPhoneFormat(userCartData.ShippingAddress.Phone);
                userCartData.ShippingAddress.Phone2 = AFMDataUtilities.SetPhoneFormat(userCartData.ShippingAddress.Phone2);
                userCartData.ShippingAddress.Phone3 = AFMDataUtilities.SetPhoneFormat(userCartData.ShippingAddress.Phone3);
            }
            if (userCartData.PaymentAddress != null)
            {
                userCartData.PaymentAddress.Phone = AFMDataUtilities.SetPhoneFormat(userCartData.PaymentAddress.Phone);
                userCartData.PaymentAddress.Phone2 = AFMDataUtilities.SetPhoneFormat(userCartData.PaymentAddress.Phone2);
                userCartData.PaymentAddress.Phone3 = AFMDataUtilities.SetPhoneFormat(userCartData.PaymentAddress.Phone3);
            }
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["IsShippingAddressAsPrimary"]))
            {
                userCartData.ShippingAddress.IsPrimary = true;
                userCartData.PaymentAddress.IsPrimary = false;
            }
            else
            {
                userCartData.ShippingAddress.IsPrimary = false;
                userCartData.PaymentAddress.IsPrimary = true;
            }

            address.Add(userCartData.PaymentAddress);
            address.Add(userCartData.ShippingAddress);

            IEnumerable<CustomDataModel.Address> customerAddresses = address;
            signedInCustomer = afmCustomerController.UpdateAddresses(signedInCustomer.AccountNumber, customerAddresses);//Change to update address only Hardik : Bug 94142:WSR - Remove "Opt-in" Check Box (Billing Page) - CR 320 (INC0132820) 
            //signedInCustomer = afmCustomerController.UpdateCustomer(signedInCustomer);
            AFMDataUtilities.SetCustomerIdTempInCookie(signedInCustomer.AccountNumber);
            return signedInCustomer;
        }
        //NS Developed by spriya Dated 10/10/2014


        void AFMPreCreateOrder(ref CrtDataModel.Cart cart, OrderManager manager, string customerId, out CustomDataModel.Customer customer, AFMSalesTransHeader headerValues)
        {
            //check signed in customerId
            string custId = !string.IsNullOrEmpty(customerId) ? customerId : AFMDataUtilities.GetCustomerIdFromCookie();
            //Check customer Id for guest customers in case of failed order submission
            if (string.IsNullOrEmpty(custId))
                custId = AFMDataUtilities.GetCustomerIdTempFromcookie();


            if (string.IsNullOrEmpty(custId))
                customer = AFMCreateCustomer(headerValues);
            else
                customer = AFMUpdateSignedCustomer(custId, headerValues);

            AFMDataUtilities.SetCustomerIdTempInCookie(customer.AccountNumber);

            cart.CustomerId = customer.AccountNumber;
            cart.ReceiptEmail = string.IsNullOrEmpty(cart.ReceiptEmail) ? customer.Email : cart.ReceiptEmail;
            Address addressBM = customer.Addresses.FirstOrDefault(p => p.AddressType == AddressType.Delivery && p.IsPrimary);
            CrtDataModel.Address addressDM = CustomerMapper.UpdateDataModel(new CrtDataModel.Address(), addressBM);
            cart.ShippingAddress = addressDM;
            cart.DeliveryMode = ConfigurationManager.AppSettings["DeliveryMode"];
            cart = manager.CreateOrUpdateCart(cart, CrtDataModel.CalculationModes.None);

            foreach (var cartLine in cart.CartLines)
            {
                cartLine.ShippingAddress = addressDM;
            }

            cart = manager.UpdateCartLines(cart.Id, cart.CustomerId, cart.CartLines, CrtDataModel.CalculationModes.None);
        }

        //NS by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
        //Remove the LineconfirmationID logic from Post create order method
        //Create new method to assign ATP information and call it in this method

        void AFMPostCreateOrder(string shoppingCartId, string customerId, ShoppingCart currentCart, CrtDataModel.SalesOrder salesOrder, AFMSalesTransHeader headerValues)
        {
            //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0

            //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
            //Retrieve and assign RSA recId associated with the order to RsaRecId field of AFMSalesTransHeader 
            if (headerValues != null)
            {
                if (!string.IsNullOrEmpty(currentCart.RsaId))
                    headerValues.RsaRecId = AFMDataUtilities.GetRsaRecId(currentCart.RsaId);
            }
            //NE - RxL
            //CS by spriya : DMND0010113 - Synchrony Financing online
            // NC CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
            AFMUserCartData tempuserCartData = AFMDataUtilities.GetUserCartInfo();
            if (tempuserCartData != null) {
                headerValues.FinancingOptionId = tempuserCartData.FinanceOptionId == null ? string.Empty:tempuserCartData.FinanceOptionId;
                headerValues.IsMultiAuthAllowed = tempuserCartData.IsMultiAuth;
                headerValues.SynchronyTnC = tempuserCartData.SynchronyTnC == null ? string.Empty : tempuserCartData.SynchronyTnC;
                headerValues.PromoDesc1 = tempuserCartData.PromoDesc1 == null ? string.Empty: tempuserCartData.PromoDesc1  ;
                headerValues.RemoveCartDiscount = tempuserCartData.RemoveCartDiscount;
                headerValues.MinimumPurchaseAmount = tempuserCartData.MinimumPurchaseAmount;
                headerValues.PromoDesc2 = tempuserCartData.PromoDesc2 == null ? string.Empty: tempuserCartData.PromoDesc2;
                headerValues.PromoDesc3 = tempuserCartData.PromoDesc3 == null ? string.Empty: tempuserCartData.PromoDesc3;
                headerValues.LearnMore = tempuserCartData.LearnMore == null ? string.Empty: tempuserCartData.LearnMore;
                headerValues.PaymentFee = tempuserCartData.PaymentFee == null ? string.Empty: tempuserCartData.PaymentFee;
                headerValues.PaymMode = tempuserCartData.PaymMode == null ? string.Empty:tempuserCartData.PaymMode ;
                headerValues.PaymTermId = tempuserCartData.PaymTermId == null ? string.Empty:tempuserCartData.PaymTermId ;
                headerValues.ThirdPartyTermCode = tempuserCartData.ThirdPartyTermCode == null ? string.Empty: tempuserCartData.ThirdPartyTermCode;
                headerValues.CardType = tempuserCartData.CardType == null ? string.Empty: tempuserCartData.CardType; 
                       
            }
            //CE by spriya : DMND0010113 - Synchrony Financing online
            UpdateSalesOrderHeader(new ShoppingCart(), headerValues, salesOrder.ChannelReferenceId);

            /*CS Developed by muthait for AFM_TFS_40686 dated 06/29/2014*/
            decimal lineNumber = 1;
            //List<AFMLineItems> lineItems = new List<AFMLineItems>();
            var transLineManager = AFMTransLineManager.Create(CrtUtilities.GetCommerceRuntime());
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            foreach (var cartLineItems in currentCart.Items)
            {
                AFMSalesTransItems afmSalesTransItems = new AFMSalesTransItems();
                afmSalesTransItems.OrderNumber = string.IsNullOrEmpty(salesOrder.ChannelReferenceId) ? string.Empty : salesOrder.ChannelReferenceId;
                afmSalesTransItems.LineNumber = Convert.ToString(lineNumber++);
                afmSalesTransItems.VendorId = string.IsNullOrEmpty(cartLineItems.AFMVendorId) ? string.Empty : cartLineItems.AFMVendorId;
                afmSalesTransItems.KitItemId = string.IsNullOrEmpty(cartLineItems.AFMKitItemId) ? string.Empty : cartLineItems.AFMKitItemId;
                afmSalesTransItems.KitProductId = cartLineItems.AFMKitProductId;
                if (!string.IsNullOrEmpty(cartLineItems.AFMKitItemId))
                {
                    dynamic productKitDetail = new ExpandoObject();
                    productKitDetail = Newtonsoft.Json.JsonConvert.DeserializeObject(cartLineItems.AFMKitItemProductDetails);
                    productKitDetail.KitComponentDetail = Newtonsoft.Json.JsonConvert.DeserializeObject(cartLineItems.ProductDetails);
                    string AfmKitProductDetailwithComp = Newtonsoft.Json.JsonConvert.SerializeObject(productKitDetail);
                    afmSalesTransItems.KitItemProductDetails = string.IsNullOrEmpty(AfmKitProductDetailwithComp) ? string.Empty : AfmKitProductDetailwithComp;
                }
                else
                {
                    afmSalesTransItems.KitItemProductDetails = string.IsNullOrEmpty(cartLineItems.ProductDetails) ? string.Empty : cartLineItems.ProductDetails;
                }
                afmSalesTransItems.KitSequenceNumber = cartLineItems.AFMKitSequenceNumber;
                afmSalesTransItems.KitItemQuantity = cartLineItems.AFMKitItemQuantity;

                // HXM - Start to resolve 67447 Color and Size not populating
                afmSalesTransItems.Color = string.Empty;
                afmSalesTransItems.Size = string.Empty;

                dynamic itemProductDetails = jsonSerializer.DeserializeObject(cartLineItems.ProductDetails);
                if (itemProductDetails.ContainsKey("DimensionValues"))
                {
                    if (itemProductDetails["DimensionValues"].ContainsKey("Color"))
                    {
                        afmSalesTransItems.Color = itemProductDetails["DimensionValues"]["Color"];
                    }
                    if (itemProductDetails["DimensionValues"].ContainsKey("Size"))
                    {
                        afmSalesTransItems.Size = itemProductDetails["DimensionValues"]["Size"];
                    }
                }
                // HXM - End to resolve 67447 Color and Size not populating

                afmSalesTransItems.AFMItemType = cartLineItems.AFMItemType;
                afmSalesTransItems.LineConfirmationID = salesOrder.ChannelReferenceId + cartLineItems.AFMLineConfirmationGroup.ToString("00");
                afmSalesTransItems.ShippingMode = string.IsNullOrEmpty(cartLineItems.AFMShippingMode) ? string.Empty : cartLineItems.AFMShippingMode;

                AFMAssignATP(afmSalesTransItems, cartLineItems);
                transLineManager.UpdateAFMSalesTransLines(afmSalesTransItems);
                // lineItems.Add(new AFMLineItems { cartItemId = cartLineItems.ItemId, lineOrderNumber = afmSalesTransItems.LineConfirmationID });
            }

            AFMUserCartData userCartInfo = new AFMUserCartData();
            userCartInfo.ShippingAddress = tempuserCartData.ShippingAddress;
            //userCartInfo.PaymentAddress = tempuserCartData.PaymentAddress;
            // userCartInfo.LineItems = lineItems;
            userCartInfo.OrderNumber = salesOrder.ChannelReferenceId;
            userCartInfo.FinanceOptionId = tempuserCartData.FinanceOptionId;
            AFMDataUtilities.SetUserCartInfo(userCartInfo);
            /*CE Developed by muthait for AFM_TFS_40686 dated 06/29/2014*/
            //NE - RxL
            //AFMDataUtilities.ClearUserCartInfoFromCookie();

            /*CE Developed by muthait for AFM_TFS_40686 dated 06/29/2014*/

            var cartManager = AFMCartManager.Create(CrtUtilities.GetCommerceRuntime());
            cartManager.UpdateCartData(true, shoppingCartId);
        }

        void AFMCartLineConfirmation(string orderNumber, TransactionItem cartLineItems, List<AFMLineConfirmationItems> lineConfirmationItemsList)
        {
            bool serviceItemFlag = false;
            if (cartLineItems.AFMItemType == (int)AFMItemType.DeliveryServiceItem)
                serviceItemFlag = true;

            if (cartLineItems.IsEcommOwned)
            {
                cartLineItems.AFMLineConfirmationGroup = ECommLineConfirmationID(orderNumber, cartLineItems.AFMVendorId,
                    cartLineItems.AFMDirectShip, cartLineItems.AFMSupplierDirectShip, lineConfirmationItemsList, serviceItemFlag);
                AFMLineConfirmationItems lineConfirmationItems = new AFMLineConfirmationItems();
                lineConfirmationItems.ItemId = cartLineItems.ItemId;
                lineConfirmationItems.LineConfirmationNumber = cartLineItems.AFMLineConfirmationGroup;
                lineConfirmationItems.IsECommOwned = cartLineItems.IsEcommOwned;
                lineConfirmationItems.VendorId = cartLineItems.AFMVendorId;
                lineConfirmationItems.DirectShip = cartLineItems.AFMDirectShip;
                lineConfirmationItems.SupplierDirectShip = cartLineItems.AFMSupplierDirectShip;
                lineConfirmationItemsList.Add(lineConfirmationItems);
            }
            else
            {
                cartLineItems.AFMLineConfirmationGroup = NonECommLineConfirmationID(orderNumber, cartLineItems.AFMVendorId, lineConfirmationItemsList, serviceItemFlag);
                AFMLineConfirmationItems lineConfirmationItems = new AFMLineConfirmationItems();
                lineConfirmationItems.ItemId = cartLineItems.ItemId;
                lineConfirmationItems.LineConfirmationNumber = cartLineItems.AFMLineConfirmationGroup;
                lineConfirmationItems.IsECommOwned = cartLineItems.IsEcommOwned;
                lineConfirmationItems.VendorId = cartLineItems.AFMVendorId;
                lineConfirmationItems.DirectShip = -1;
                lineConfirmationItems.SupplierDirectShip = -1;
                lineConfirmationItemsList.Add(lineConfirmationItems);
            }
        }

        //This method assigns ATP inforamtion to cart lines
        void AFMAssignATP(AFMSalesTransItems afmSalesTransItems, TransactionItem cartLineItems)
        {
            if (cartLineItems.AFMCartLineATP != null)
            {
                afmSalesTransItems.BeginDateRange = cartLineItems.AFMCartLineATP.BeginDateRange;
                afmSalesTransItems.BestDate = cartLineItems.AFMCartLineATP.BestDate;
                afmSalesTransItems.EndDateRange = cartLineItems.AFMCartLineATP.EndDateRange;
                afmSalesTransItems.EarliestDeliveryDate = cartLineItems.AFMCartLineATP.EarliestDeliveryDate;
                afmSalesTransItems.LatestDeliveryDate = cartLineItems.AFMCartLineATP.LatestDeliveryDate;
                afmSalesTransItems.IsAvailable = cartLineItems.AFMCartLineATP.IsAvailable;
                afmSalesTransItems.Message = cartLineItems.AFMCartLineATP.Message;

                //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
                afmSalesTransItems.BeginDateRangeMOD = cartLineItems.AFMCartLineATP.BeginDateRangeMOD;
                afmSalesTransItems.BestDateMOD = cartLineItems.AFMCartLineATP.BestDateMOD;
                afmSalesTransItems.EndDateRangeMOD = cartLineItems.AFMCartLineATP.EndDateRangeMOD;
                afmSalesTransItems.EarliestDeliveryDateMOD = cartLineItems.AFMCartLineATP.EarliestDeliveryDateMOD;
                afmSalesTransItems.LatestDeliveryDateMOD = cartLineItems.AFMCartLineATP.LatestDeliveryDateMOD;
                afmSalesTransItems.MessageMOD = cartLineItems.AFMCartLineATP.MessageMOD;
                //NE by RxL
            }
            else
            {
                afmSalesTransItems.BeginDateRange = Convert.ToDateTime("1900-01-01");
                afmSalesTransItems.BestDate = Convert.ToDateTime("1900-01-01");
                afmSalesTransItems.EndDateRange = Convert.ToDateTime("1900-01-01");
                afmSalesTransItems.EarliestDeliveryDate = Convert.ToDateTime("1900-01-01");
                afmSalesTransItems.LatestDeliveryDate = Convert.ToDateTime("1900-01-01");
                afmSalesTransItems.IsAvailable = false;
                afmSalesTransItems.Message = string.Empty;

                //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
                afmSalesTransItems.BeginDateRangeMOD = Convert.ToDateTime("1900-01-01");
                afmSalesTransItems.BestDateMOD = Convert.ToDateTime("1900-01-01");
                afmSalesTransItems.EndDateRangeMOD = Convert.ToDateTime("1900-01-01");
                afmSalesTransItems.EarliestDeliveryDateMOD = Convert.ToDateTime("1900-01-01");
                afmSalesTransItems.LatestDeliveryDateMOD = Convert.ToDateTime("1900-01-01");
                afmSalesTransItems.MessageMOD = string.Empty;
                //NE by RxL
            }
        }

        /// <summary>
        /// This method Calls AFMCartLineConfirmation method to create and assign line confirmation groups to cart lines
        /// Then it will create tender lines depending on line conforamtion group and assign Due Amount as aggregate of lines in same line confirmation group
        /// </summary>
        /// <param name="shoppingCartId">Shopping cart or transaction ID</param>
        /// <param name="currentCart"> current  cart</param>
        /// <param name="cartTenderLines">Original tender line </param>
        /// <param name="address">billing address</param>
        /// <param name="cardToken">card token</param>
        /// <returns>Splitted tender lines</returns>
        List<CrtDataModel.CartTenderLine> AFMSplitTenderLine(string shoppingCartId, ShoppingCart currentCart, List<CrtDataModel.CartTenderLine> cartTenderLines, Address address, string cardToken)
        {
            List<AFMLineConfirmationItems> lineConfirmationItemsList = new List<AFMLineConfirmationItems>();
            //CS Multi Auth Assign DS service item the same confirmation group as the AFI Vendor
            int afiVendorLineConfirmation = -1;
            foreach (var cartline in currentCart.Items)
            {
                AFMCartLineConfirmation(currentCart.CartId, cartline, lineConfirmationItemsList);
                if (((cartline.IsEcommOwned==true && cartline.AFMDirectShip==1 && cartline.AFMSupplierDirectShip==0) ||
                    (cartline.IsEcommOwned == true && cartline.AFMDirectShip == 0 && cartline.AFMSupplierDirectShip == 0)) && cartline.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                {
                    afiVendorLineConfirmation = cartline.AFMLineConfirmationGroup;
            }
            }
            if (afiVendorLineConfirmation > 0)
            {
                 foreach (var cartline in currentCart.Items)
                {
                    if (cartline.AFMItemType == (int)AFMItemType.DeliveryServiceItem && cartline.IsEcommOwned == true) { 
                        cartline.AFMLineConfirmationGroup = afiVendorLineConfirmation;
                    }
                 }
            }

            //CS by spriya : DMND0010113 - Synchrony Financing online
            //Split tender lines only for non synchrony payment transactions
            if (AFMDataUtilities.GetUserCartInfo().IsMultiAuth == false)
            {
                if (cartTenderLines.First().PaymentCard != null)
                {
                    cartTenderLines.First().PaymentCard["CardToken"] = cardToken;
                    // NS Changed by AxL for synchrony FDD on 15/12/2015 
                    //cartTenderLines.First().PaymentCard["TranId"] = HttpUtility.UrlPathEncode(AFM.Commerce.Framework.Extensions.Utils.SecurityManager.HashData(shoppingCartId)); // shoppingCartId;
                    cartTenderLines.First().PaymentCard["TranId"] = AFM.Commerce.Framework.Extensions.Utils.SecurityManager.EncryptCartID(shoppingCartId); // shoppingCartId;
                    // NE Changed by AxL for synchrony FDD on 15/12/2015
                    cartTenderLines.First().PaymentCard["LineConfirmationGroupId"] = 1;
                    cartTenderLines.First()["LineConfirmationGroupId"] = 1;
                    if (address != null)
                    {
                        cartTenderLines.First().PaymentCard.Address1 = address.Street;
                        cartTenderLines.First().PaymentCard.City = address.City;
                        cartTenderLines.First().PaymentCard.State = address.State;
                        cartTenderLines.First().PaymentCard.Country = address.Country;
                        cartTenderLines.First().PaymentCard.Zip = address.ZipCode;
                    }
                }
                else if (cartTenderLines.First().TokenizedPaymentCard != null)
                {
                    cartTenderLines.First().TokenizedPaymentCard["CardToken"] = cardToken;
                    // NS Changed by AxL for synchrony FDD on 15/12/2015
                    //cartTenderLines.First().PaymentCard["TranId"] = HttpUtility.UrlPathEncode(AFM.Commerce.Framework.Extensions.Utils.SecurityManager.HashData(shoppingCartId)); // shoppingCartId;
                    cartTenderLines.First().PaymentCard["TranId"] = AFM.Commerce.Framework.Extensions.Utils.SecurityManager.EncryptCartID(shoppingCartId); // shoppingCartId;
                    // NE Changed by AxL for synchrony FDD on 15/12/2015
                    cartTenderLines.First().PaymentCard["LineConfirmationGroupId"] = 1;
                    cartTenderLines.First()["LineConfirmationGroupId"] = 1;
                    if (address != null)
                    {
                        cartTenderLines.First().TokenizedPaymentCard.Address1 = address.Street;
                        cartTenderLines.First().TokenizedPaymentCard.City = address.City;
                        cartTenderLines.First().TokenizedPaymentCard.State = address.State;
                        cartTenderLines.First().TokenizedPaymentCard.Country = address.Country;
                        cartTenderLines.First().TokenizedPaymentCard.Zip = address.ZipCode;
                    }
                }
                return cartTenderLines;
            }
            else
            {
                //CE by spriya : DMND0010113 - Synchrony Financing online

                var lcGroups = from item in currentCart.Items
                               group item by item.AFMLineConfirmationGroup into itemg
                               select new
                               {
                                   LineConfirmationGroup = itemg.Key,
                                   Amount = itemg.Sum(
                                       x => (x.AFMNetAmount + x.AFMTaxAmount))
                               };

                List<CrtDataModel.CartTenderLine> newCartTenderLines = new List<CrtDataModel.CartTenderLine>();
                CrtDataModel.CartTenderLine tenderLineCopy = cartTenderLines.FirstOrDefault();
                foreach (var grp in lcGroups)
                {
                    CrtDataModel.CartTenderLine tenderLine = new CrtDataModel.CartTenderLine();
                    tenderLine.CopyFrom<CrtDataModel.CartTenderLine>(tenderLineCopy);
                    tenderLine.CopyPropertiesFrom(tenderLineCopy);
                    if (tenderLineCopy.PaymentCard != null)
                    {
                        tenderLine.PaymentCard = new CrtDataModel.PaymentCard();
                        tenderLine.PaymentCard.CopyFrom(tenderLineCopy.PaymentCard);
                        tenderLine.PaymentCard.CopyPropertiesFrom(tenderLineCopy.PaymentCard);
                    }

                    if (tenderLineCopy.TokenizedPaymentCard != null)
                    {
                        tenderLine.TokenizedPaymentCard = new CrtDataModel.TokenizedPaymentCard();
                        tenderLine.TokenizedPaymentCard.CopyFrom(tenderLineCopy.TokenizedPaymentCard);
                        tenderLine.TokenizedPaymentCard.CopyPropertiesFrom(tenderLineCopy.TokenizedPaymentCard);
                    }
                    if (tenderLine.PaymentCard != null)
                    {
                        tenderLine.PaymentCard["CardToken"] = cardToken;
                        // NS Changed by AxL for synchrony FDD on 15/12/2015
                        //tenderLine.PaymentCard["TranId"] = HttpUtility.UrlPathEncode(AFM.Commerce.Framework.Extensions.Utils.SecurityManager.HashData(shoppingCartId)); // shoppingCartId;
                        tenderLine.PaymentCard["TranId"] = AFM.Commerce.Framework.Extensions.Utils.SecurityManager.EncryptCartID(shoppingCartId); // shoppingCartId;
                        // NE Changed by AxL for synchrony FDD on 15/12/2015
                        tenderLine.PaymentCard["LineConfirmationGroupId"] = grp.LineConfirmationGroup;
                        if (address != null)
                        {
                            tenderLine.PaymentCard.Address1 = address.Street;
                            tenderLine.PaymentCard.City = address.City;
                            tenderLine.PaymentCard.State = address.State;
                            tenderLine.PaymentCard.Country = address.Country;
                            tenderLine.PaymentCard.Zip = address.ZipCode;
                        }
                    }
                    if (tenderLine.TokenizedPaymentCard != null)
                    {
                        tenderLine.TokenizedPaymentCard["CardToken"] = cardToken;
                        // NS Changed by AxL for synchrony FDD on 15/12/2015
                        //tenderLine.TokenizedPaymentCard["TranId"] = HttpUtility.UrlPathEncode(AFM.Commerce.Framework.Extensions.Utils.SecurityManager.HashData(shoppingCartId)); // shoppingCartId;
                        tenderLine.TokenizedPaymentCard["TranId"] = AFM.Commerce.Framework.Extensions.Utils.SecurityManager.EncryptCartID(shoppingCartId); // shoppingCartId;
                        // NE Changed by AxL for synchrony FDD on 15/12/2015
                        tenderLine.TokenizedPaymentCard["LineConfirmationGroupId"] = grp.LineConfirmationGroup;
                        if (address != null)
                        {
                            tenderLine.TokenizedPaymentCard.Address1 = address.Street;
                            tenderLine.TokenizedPaymentCard.City = address.City;
                            tenderLine.TokenizedPaymentCard.State = address.State;
                            tenderLine.TokenizedPaymentCard.Country = address.Country;
                            tenderLine.TokenizedPaymentCard.Zip = address.ZipCode;
                        }
                    }
                    tenderLine["LineConfirmationGroupId"] = grp.LineConfirmationGroup;
                    tenderLine.Amount = grp.Amount;
                    newCartTenderLines.Add(tenderLine);
                }

                return newCartTenderLines;
            }
        }
        //NE by RxL: DMND0026205-FDD-FIN-Multiple Authorizations

        void ValidateCartBeforeCreateOrder(CrtDataModel.Cart cart, ShoppingCart currentCart)
        {
            // Validate ATP Detail 
            foreach (TransactionItem item in currentCart.Items)
            {
                if (item.AFMItemType != (int)AFMItemType.DeliveryServiceItem)
                {
                    if (item.AFMCartLineATP != null)
                    {
                        if (!item.AFMCartLineATP.IsAvailable)
                        {
                            var datavalidationExp = new PaymentException("ATPValidation", CartConstant.AtpItemNotAvailableMrssage + " Please Refreash Page and try again");
                            throw datavalidationExp;
                        }
                    }
                    else
                    {
                        var datavalidationExp = new PaymentException("ATPValidation", CartConstant.AtpServiceNotAvailableErrorMrssage + " Please Refreash Page and try again");
                        throw datavalidationExp;
                    }
                }
            }

            //NS Validate Delviery Service Line to Cart : PMA : INC0132275 : Order with out Service Line Issue in Production -- Hardik 
            List<string> AllDeliveryMode = cart.CartLines.Select(temp => temp.DeliveryMode).Distinct().ToList();
            foreach (var tempDlvMode in AllDeliveryMode)
            {
                var delivetyItemCount = cart.CartLines.Where(temp => (int)temp.LineData[CartConstant.AFMItemType] == (int)AFMItemType.DeliveryServiceItem && temp.DeliveryMode == tempDlvMode).Count();
                if (delivetyItemCount == 0)
                {
                    var datavalidationExp = new PaymentException("ShippingChargeValidation", CartConstant.AtpItemNotAvailableMrssage + "Please Refreash Page and try again");
                    throw datavalidationExp;
                }
            }
            //NE Validate Delviery Service Line to Cart -- Hardik 

            //NS Validateing Vendor Detail in Cart Before Create Order -- Hardik -- PMA : INC0156841 : Production orders created without a PO Vendor
            foreach (var cartline in cart.CartLines)
            {
                if (string.IsNullOrEmpty(cartline.DeliveryMode)
                    //|| string.IsNullOrEmpty(Convert.ToString(cartline.LineData[CartConstant.AFMVendorId]))
                    || (string.IsNullOrEmpty(Convert.ToString(cartline.LineData[CartConstant.AFMShippingMode])) && (int)cartline.LineData["AFMItemType"] != (int)AFMItemType.DeliveryServiceItem))
                {
                    var datavalidationExp = new DataValidationException("DataValidationError", "Validation Fail for Cart Line Item " + cartline.ItemId
                        + ".\n Some of this value is missing Delivery Mode : " + cartline.DeliveryMode +
                        " Vendor Detail : " + Convert.ToString(cartline.LineData[CartConstant.AFMVendorId] +
                         " Shipping Mode : " + Convert.ToString(cartline.LineData[CartConstant.AFMShippingMode]))
                    );
                    throw datavalidationExp;
                }
            }
            //NE Validateing Vendor Detail in Cart Before Create Order
        }

        /// <summary>
        /// Get the types of payment cards.
        /// </summary>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        /// <param name="customerId">The customer id.</param>
        /// <param name="cardToken">The cardToken</param>
        /// <param name="tenderDataLines">The tenderline date for payment.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="userName">Name of the user.</param>
        /// <returns>The channel reference identifier for the order.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the shoppingCartId or emailAddress is null or empty.  Thrown when payments is null.</exception>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.DataValidationException">Shopping cart {0} for customer {1} was not found.</exception>
        /*CS Developed by muthait for AFM_TFS_40686 dated 06/29/2014 - 2 new parameters added */
        public string CreateOrder(string shoppingCartId, string customerId, string cardToken, IEnumerable<TenderDataLine> tenderDataLines, AFMSalesTransHeader headerValues, string emailAddress, string userName, IProductValidator productValidator, ISearchEngine searchEngine)
        {
            if (string.IsNullOrWhiteSpace(shoppingCartId))
            {
                throw new ArgumentNullException("shoppingCartId");
            }
            if (!AFMDataUtilities.IsValidString(customerId))
            {
                throw new ArgumentException("Invalid customer id", "customerId");
            }

            if (!AFMDataUtilities.IsValidEmail(emailAddress))
            {
                throw new ArgumentException("Invalid email id", "emailAddress");
            }

            if (tenderDataLines == null)
            {
                throw new ArgumentNullException("tenderDataLines");
            }
            try
            {
                OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                

                //Hardik : Create Shopping Cart from Cart and Pass to Post Create order after order
                AFMShoppingCartController shoppingCartController = new AFMShoppingCartController();
                shoppingCartController.SetConfiguration(this.Configuration);
                ShoppingCart currentCart = shoppingCartController.GetShoppingCart(shoppingCartId, customerId, ShoppingCartDataLevel.Minimal, productValidator, searchEngine, true, false);
                //Get correct amount due
                CrtDataModel.Cart cart = manager.GetCart(shoppingCartId, customerId, CrtDataModel.CalculationModes.AmountDue);
                if (cart == null)
                {
                    throw new DataValidationException(DataValidationErrors.CartNotFound, Resource.ShopingCartForCustomerNotFound, shoppingCartId, customerId);
                }
                //shoppingCartController.AddATPforEcommerce(shoppingCart, cart);
                shoppingCartController.CalculateShippingCharges(currentCart, cart, true, false);
                //Hardik : Create Shopping Cart from Cart and Pass to Post Create order after order

                //Hardik : added for data validation before submit order  
                ValidateCartBeforeCreateOrder(cart, currentCart);

                //NS Developed by spriya writeweb
                CustomDataModel.Customer customer = null;
                Address address = null;
                AFMPreCreateOrder(ref cart, manager, customerId, out customer, headerValues);
                if (customer != null)
                    address = customer.Addresses.FirstOrDefault(c => c.AddressType == AddressType.Invoice);
                customerId = cart.CustomerId;

                IEnumerable<CrtDataModel.CartTenderLine> cartTenderLines = base.GetCartTenderLines(tenderDataLines, cart.TotalAmount, cart.Id);
                //N by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
                cartTenderLines = AFMSplitTenderLine(shoppingCartId, currentCart, cartTenderLines.ToList(), address, cardToken);

                CrtDataModel.SalesOrder salesOrder = null;

                using (TransactionScope tsScope = TransactionUtils.CreateTransactionScope())
                {
                    salesOrder = manager.CreateOrderFromCart(shoppingCartId, customerId, cartTenderLines, cart.ReceiptEmail);

                    //NS Developed by spriya writeweb
                    AFMPostCreateOrder(shoppingCartId, customerId, currentCart, salesOrder, headerValues);
                    tsScope.Complete();
                }

                AFMDataUtilities.SetOrderIdInCookie(salesOrder.ChannelReferenceId);
                AFMDataUtilities.ClearFinanceOptionIdFromCookie();
                return salesOrder.ChannelReferenceId;
            }
            catch (PaymentException ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "ShoppingCartId : " + shoppingCartId + " Customer ID : " + customerId +
                                    " CardToken : " + cardToken + " emailAddress : " + emailAddress + " userName : " + userName + Environment.NewLine +
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);



                //return ErrorCheck + ex.Message;
                throw;

            }
            catch (Exception ex)
            {
                //, string cardToken, IEnumerable<TenderDataLine> tenderDataLines, string emailAddress, string userName
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "ShoppingCartId : " + shoppingCartId + " Customer ID : " + customerId +
                                    " CardToken : " + cardToken + " emailAddress : " + emailAddress + " userName : " + userName + Environment.NewLine +
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                //return "Error:" + ex.Message;
                throw;
            }
        }
        /// <summary>
        /// Update the sales order header information
        /// </summary>
        /// <returns></returns>
        static bool UpdateSalesOrderHeader(ShoppingCart shoppingCart, AFMSalesTransHeader headerValues, string orderNumber)
        {

            if (!AFMDataUtilities.IsValidString(orderNumber))
            {
                throw new ArgumentException("Invalid order number", "orderNumber");
            }

            try
            {
                // NS CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
                AFMUserCartData userCartData = AFMDataUtilities.GetUserCartInfo();
                var afmSalesTransHeader = new AFMSalesTransHeader
                {
                    OrderNumber = orderNumber,
                    IsOkToText = userCartData.IsOkToText,
                    OkToTextUniqueId = headerValues.OkToTextUniqueId,
                    OkToTextVersionId = headerValues.OkToTextVersionId,
                    IsAcceptTermsAndConditions =
                        headerValues.IsAcceptTermsAndConditions,
                    TermsAndConditionsUniqueId =
                        headerValues.TermsAndConditionsUniqueId,
                    TermsAndConditionsVersionId =
                        headerValues.TermsAndConditionsVersionId,
                    //N - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
                    RsaRecId = headerValues.RsaRecId,
                    //CS by spriya : DMND0010113 - Synchrony Financing online
                    FinancingOptionId = headerValues.FinancingOptionId,
                    SynchronyTnC = headerValues.SynchronyTnC,
                    IsMultiAuthAllowed = headerValues.IsMultiAuthAllowed,
                    PromoDesc1 = headerValues.PromoDesc1,
                    PromoDesc2 = headerValues.PromoDesc2,
                    PromoDesc3 = headerValues.PromoDesc3,
                    RemoveCartDiscount = headerValues.RemoveCartDiscount,
                    MinimumPurchaseAmount = headerValues.MinimumPurchaseAmount,
                    PaymMode = headerValues.PaymMode,
                    PaymTermId = headerValues.PaymTermId,
                    PaymentFee = headerValues.PaymentFee,
                    LearnMore = headerValues.LearnMore,
                    CardType = headerValues.CardType,
                    ThirdPartyTermCode = headerValues.ThirdPartyTermCode
                    //CE by spriya : DMND0010113 - Synchrony Financing online
                };
                var transHeaderRequest = new SetTransHeaderServiceRequest
                {
                    transHeader = afmSalesTransHeader
                };
                var runtime = CrtUtilities.GetCommerceRuntime();
                var requestContext = new RequestContext(runtime);
                return runtime.Execute<SetTransHeaderServiceResponse>(transHeaderRequest, requestContext, true).Result;

                // NE CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                                    "ShoppingCartId : " + shoppingCart.CartId + " Order number : " + orderNumber + Environment.NewLine +
                                    ex.Message + Environment.NewLine +
                                    ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }

        //NS by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
        //ECommLineConfirmationID and NonECommLineConfirmationID methods are updated to return just line confiramtion group ids

        //NS Developed by spriya
        /// <summary>
        /// Calculate Ecomm Owned Item Line Confirmation ID
        /// </summary>
        /// <param name="cartOrderNumber"></param>
        /// <param name="vendorID"></param>
        /// <param name="directShip"></param>
        /// <param name="supplierDirectShip"></param>
        /// <param name="lineConfirmationItemsList"></param>
        /// <param name="serviceItemFlag"></param>
        /// <returns></returns>
        static int ECommLineConfirmationID(string cartOrderNumber, string vendorID, int directShip, int supplierDirectShip, List<AFMLineConfirmationItems> lineConfirmationItemsList, bool serviceItemFlag)
        {
            //if (!AFMDataUtilities.IsValidString(cartOrderNumber))
            //{
            //    throw new ArgumentException("Invalid cart order number", "cartOrderNumber");
            //}
            if (!AFMDataUtilities.IsValidString(vendorID))
            {
                throw new ArgumentException("Invalid vendor id", "vendorID");
            }

            try
            {
                string lineConfirmation = string.Empty;
                if (lineConfirmationItemsList != null & lineConfirmationItemsList.Count == 0)
                    return 1;
                else
                {
                    IEnumerable<AFMLineConfirmationItems> matchVendorLineItems = lineConfirmationItemsList.Where(obj => obj.VendorId == vendorID && obj.IsECommOwned == true);
                    if (matchVendorLineItems.IsNullOrEmpty() || matchVendorLineItems.Count() == 0)
                    {
                        int maxLineConfirmationNumber = lineConfirmationItemsList.Max(obj => obj.LineConfirmationNumber);
                        return ++maxLineConfirmationNumber;
                    }
                    else
                    {
                        IEnumerable<AFMLineConfirmationItems> shipMatchVendor;
                        IEnumerable<AFMLineConfirmationItems> sdsoMatchVendor;
                        if (serviceItemFlag)
                        {
                            shipMatchVendor = matchVendorLineItems.Where(obj => obj.DirectShip == 1).OrderBy(obj => obj.LineConfirmationNumber);
                            //Line Confirmation unchecked DS and SDSO siteowned item scenario
                            if (shipMatchVendor.IsNullOrEmpty() || shipMatchVendor.Count() == 0)
                                shipMatchVendor = matchVendorLineItems.Where(obj => obj.IsECommOwned == true).OrderBy(obj => obj.LineConfirmationNumber);

                            if (!shipMatchVendor.IsNullOrEmpty() || shipMatchVendor.Count() != 0)
                            {
                                return shipMatchVendor.First<AFMLineConfirmationItems>().LineConfirmationNumber;
                            }
                        }
                        else
                        {
                            if (directShip == 1 && supplierDirectShip == 1)
                            {
                                shipMatchVendor = matchVendorLineItems.Where(obj => obj.DirectShip == 1 && obj.SupplierDirectShip == 1);
                                if (!shipMatchVendor.IsNullOrEmpty() || shipMatchVendor.Count() != 0)
                                {
                                    return shipMatchVendor.First<AFMLineConfirmationItems>().LineConfirmationNumber;
                                }
                            }
                            else if (directShip == 1 && supplierDirectShip == 0)
                            {
                                shipMatchVendor = matchVendorLineItems.Where(obj => obj.DirectShip == 1 && obj.SupplierDirectShip == 0);
                                if (!shipMatchVendor.IsNullOrEmpty() || shipMatchVendor.Count() != 0)
                                {
                                    int dsConfirmationnumber = shipMatchVendor.First<AFMLineConfirmationItems>().LineConfirmationNumber;
                                    sdsoMatchVendor = matchVendorLineItems.Where(obj => obj.LineConfirmationNumber.Equals(dsConfirmationnumber)).Where(obj => obj.SupplierDirectShip == 1 && obj.DirectShip == 1);
                                    if (sdsoMatchVendor.IsNullOrEmpty() || sdsoMatchVendor.Count() == 0)
                                        sdsoMatchVendor = matchVendorLineItems.Where(obj => obj.LineConfirmationNumber.Equals(dsConfirmationnumber)).Where(obj => obj.SupplierDirectShip == 0 && obj.DirectShip == 0);
                                    if (sdsoMatchVendor.IsNullOrEmpty() || sdsoMatchVendor.Count() == 0)
                                        return dsConfirmationnumber;
                                    else if (sdsoMatchVendor.Count() > 0 && shipMatchVendor.Count() > 1)
                                        return shipMatchVendor.ElementAt<AFMLineConfirmationItems>(1).LineConfirmationNumber;
                                }
                            }
                            else if (directShip == 0 && supplierDirectShip == 0)
                            {
                                shipMatchVendor = matchVendorLineItems.Where(obj => obj.DirectShip == 0 && obj.SupplierDirectShip == 0);
                                if (!shipMatchVendor.IsNullOrEmpty() || shipMatchVendor.Count() != 0)
                                {
                                    return shipMatchVendor.First<AFMLineConfirmationItems>().LineConfirmationNumber;
                                }
                            }
                            else if (directShip == 0 && supplierDirectShip == 1)
                            {
                                shipMatchVendor = matchVendorLineItems.Where(obj => obj.DirectShip == 0 && obj.SupplierDirectShip == 1);
                                if (!shipMatchVendor.IsNullOrEmpty() || shipMatchVendor.Count() != 0)
                                {
                                    return shipMatchVendor.First<AFMLineConfirmationItems>().LineConfirmationNumber;
                                }
                            }
                        }
                        int maxLineConfirmationNumber = lineConfirmationItemsList.Max(obj => obj.LineConfirmationNumber);
                        return ++maxLineConfirmationNumber;
                    }

                }
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "cartOrderNumber : " + cartOrderNumber + " VendorId : " + vendorID +
                       " DirectShip : " + directShip + " supplierDirectShip : " + supplierDirectShip + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }

        /// <summary>
        /// Calculate Non Ecomm owned item Line confirmation ID
        /// </summary>
        /// <param name="cartOrderNumber"></param>
        /// <param name="vendorID"></param>
        /// <param name="lineConfirmationItemsList"></param>
        /// <param name="serviceItemFlag"></param>
        /// <returns></returns>
        static int NonECommLineConfirmationID(string cartOrderNumber, string vendorID, List<AFMLineConfirmationItems> lineConfirmationItemsList, bool serviceItemFlag)
        {
            //if (!AFMDataUtilities.IsValidString(cartOrderNumber))
            //{
            //    throw new ArgumentException("Invalid cart order number", "cartOrderNumber");
            //}

            try
            {
                string lineConfirmation = string.Empty;
                if (lineConfirmationItemsList != null & lineConfirmationItemsList.Count == 0)
                    return 1;
                else
                {
                    IEnumerable<AFMLineConfirmationItems> matchVendorLineItems = lineConfirmationItemsList.Where(obj => obj.VendorId == vendorID && obj.IsECommOwned == false);
                    if (matchVendorLineItems.IsNullOrEmpty() || matchVendorLineItems.Count() == 0)
                    {
                        int maxLineConfirmationNumber = lineConfirmationItemsList.Max(obj => obj.LineConfirmationNumber);
                        return ++maxLineConfirmationNumber;
                    }
                    else
                    {
                        return matchVendorLineItems.First<AFMLineConfirmationItems>().LineConfirmationNumber;
                    }
                }
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       "cartOrderNumber : " + cartOrderNumber + " VendorId : " + vendorID + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }
        //NE by RxL

        /// <summary>
        /// ValidateAddress
        /// </summary>
        /// <returns>bool</returns>
        public static AFMAddressData ValidateAddress(Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Address address)
        {
            if (address == null)
            {
                throw new ArgumentNullException("address");
            }

            try
            {
                AFMCheckoutController.ValidateAddressName(address);
                AFMAddressValidationManager afmAddressValidationManager = AFMAddressValidationManager.Create(CrtUtilities.GetCommerceRuntime());

                //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
                AFMAddressData afmAddressData = new AFMAddressData();
                afmAddressData = afmAddressValidationManager.ValidateAddress(AFMAddressMapper.ConvertToAddressValidationDataModel(address));

                if (afmAddressData.addressValidationsuggestedAddress != null)
                {
                    afmAddressData.suggestedAddress = AFMAddressMapper.ConvertToCommerceDataModel(afmAddressData.addressValidationsuggestedAddress);
                    afmAddressData.Street2 = afmAddressData.addressValidationsuggestedAddress.StreetLine2;
                }
                return afmAddressData;
                //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore               
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }

        public const string AddressFirstNameLastNameNotEmptyNullError = "AddressFirstNameLastNameNotEmptyNullError";
        public const string AddressFirstNameLastNameLenghtValidationError = "AddressFirstNameLastNameLenghtValidationError";
        public const string AddressFirstNameLastNameFormatError = "AddressFirstNameLastNameFormatError";
        public const string AddressNameValidationRegEx = "[a-zA-Z ]+";

        static void ValidateAddressName(Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Address address)
        {
            if (address != null)
            {

                if (string.IsNullOrEmpty(address.FirstName) || string.IsNullOrEmpty(address.LastName))
                {
                    throw new ArgumentException("Address First Name or Last Name cannot be empty.");
                }

                address.FirstName = address.FirstName.Trim();
                address.LastName = address.LastName.Trim();

                if (string.IsNullOrWhiteSpace(address.FirstName) || string.IsNullOrWhiteSpace(address.LastName))
                {
                    throw new ArgumentException("Address First Name or Last Name cannot be empty.");
                }

                if (address.FirstName.Length > 25 || address.LastName.Length > 25)
                {
                    throw new ArgumentException("Address First Name or Last Name cannot have more than 25 characters.");
                }

                if (!Regex.IsMatch(address.FirstName, AddressNameValidationRegEx)
                    || !Regex.IsMatch(address.LastName, AddressNameValidationRegEx))
                {
                    throw new ArgumentException("Address First Name or Last Name can have only alphabets.");
                }
            }
        }


        /// <summary>
        /// ValidateZipcode
        /// </summary>
        /// <returns>bool</returns>
        public static bool ValidateZipcode(string zipCode, string countryRegioncId)
        {
            if (string.IsNullOrEmpty(zipCode))
            {
                throw new ArgumentNullException("Zipcode");
            }

            if (string.IsNullOrEmpty(countryRegioncId))
            {
                throw new ArgumentNullException("CountryRegionId");
            }
            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                AFMValidateZipcodeRequest afmValidateZipcodeRequest = new AFMValidateZipcodeRequest();
                afmValidateZipcodeRequest.ZipCode = zipCode.Split('-')[0];
                afmValidateZipcodeRequest.CountryRegionId = countryRegioncId;
                var requestContext = new RequestContext(runtime);
                AFMValidateZipcodeResponse afmGetAddressStateResponse = runtime.Execute<AFMValidateZipcodeResponse>(
                     afmValidateZipcodeRequest, requestContext, true);
                return afmGetAddressStateResponse.IsValidZipcode;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }

        /// <summary>
        /// GetAddressList
        /// </summary>
        /// <returns>bool</returns>
        public static List<AFMAddressState> GetAddressList(string countryRegion)
        {
            if (string.IsNullOrEmpty(countryRegion))
            {
                throw new ArgumentNullException("countryRegion");
            }

            try
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                AFMGetAddressStateRequest afmGetAddressStateRequest = new AFMGetAddressStateRequest(countryRegion);
                var reqContext = new RequestContext(runtime);
                AFMGetAddressStateResponse afmGetAddressStateResponse = runtime.Execute<AFMGetAddressStateResponse>(
                     afmGetAddressStateRequest, reqContext, true);
                return afmGetAddressStateResponse.AddressStateList;
            }
            catch (Exception ex)
            {
                Logging.LoggerUtilities.ProcessLogMessage(
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);

                if (ex.InnerException != null)
                    Logging.LoggerUtilities.ProcessLogMessage(ex.InnerException);

                throw;
            }
        }
    }
}

