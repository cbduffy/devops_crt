﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AFM.Commerce.Framework.Extensions.Utils
{
    [Serializable]
    public class SessionException: Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SessionException"/> class.
        /// </summary>
        public SessionException()
            : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public SessionException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public SessionException(string message, Exception innerException)
            : base(message, innerException)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization info.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected SessionException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        { }
    }
    /// <summary>
    /// Manages the session data for the current user calls
    /// </summary>
    public static class SessionManager
    {
        /// <summary>
        /// Gets the specified Session value from the request.
        /// </summary>
        /// <param name="context">An instance of the HttpContext.</param>
        /// <param name="Key">A string containing the name of the Session.</param>
        /// <returns>a string containing the Session value.</returns>
        public static string GetRequestSessionValue(HttpContext context, string SessionKey)
        {
            try
            {
                if (context != null && context.Request != null && context.Session[SessionKey] != null)
                {
                    return context.Session[SessionKey].ToString();
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new SessionException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Sets the Session value with the specified Session name in the response.
        /// </summary>
        /// <param name="context">An instance of the HttpContext.</param>
        /// <param name="SessionKey">A string containing the key in the Session.</param>
        /// <param name="SessionValue">A string containing the value of the Session.</param>
        /// <param name="expiry">The date of Session expiration.</param>
        /// <param name="isSecure">A value indicating whether the Session is secure.</param>
        /// <param name="isHttpOnly">A value indicating whether the Session is HttpOnly.</param>
        /// <exception cref="System.ArgumentException">The Session name cannot be null or contain white space only.</exception>
        /// <exception cref="Microsoft.Dynamics.Retail.SharePoint.Web.Common.SessionException"></exception>
        /// <exception cref="ArgumentException">ArgumentException is thrown when invalid Session name is specified.</exception>
        public static void SetResponseSessionValue(HttpContext context, string SessionKey, string SessionValue)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(SessionKey))
                {
                    throw new ArgumentException("The Session name cannot be null or contain white space only.");
                }

                if (context != null && context.Response != null)
                {
                    context.Session[SessionKey] = SessionValue;
                }
            }
            catch (Exception ex)
            {
                throw new SessionException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Remove the specified Session Key from the current session.
        /// </summary>
        /// <param name="context">An instance of the HttpContext.</param>
        /// <param name="SessionName">A string containing the name of the Session.</param>
        public static void DeleteSessionKey(HttpContext context, string SessionKey)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(SessionKey))
                {
                    throw new ArgumentException(Resource.SessionCantNullOrSpace);
                }

                if (context != null)
                {
                    context.Session.Remove(SessionKey);
                }
            }
            catch (Exception ex)
            {
                throw new SessionException(ex.Message, ex);
            }
        }

        /*
         * //NS: By Spriya: Bug#61496: ECS - Subtotal, Home Delivery and Sales tax charges are $0.00 under Order Summary on Payment screen 
        public static void SetResponseSessionValue(HttpContext context, string sessionKey, dynamic sessionValue)
        {
            try
            {
                if (sessionKey == null)
                {
                    throw new ArgumentException("The Session name cannot be null or contain white space only.");
                }

                if (context != null && context.Response != null)
                {
                    context.Session[sessionKey] = sessionValue;
                }
            }
            catch (Exception ex)
            {
                throw new SessionException(ex.Message, ex);
            }
        }
         //NE: By Spriya
         */

    }
}
