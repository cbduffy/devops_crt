﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Extensions.Utils
{
    public static class CartConstant
    {
        public const string IsEcommOwned = "IsEcommOwned";

        public const string AFMVendorId = "AFMVendorId";

        public const string AFMVendorName = "AFMVendorName";

        public const string IsSDSO = "AFMIsSDSO";

        public const string InvoiceVendorId = "AFMInvoiceVendorId";

        public const string ShipToId = "AFMShipToId";

        public const string AFMKitSequenceNumber = "AFMKitSequenceNumber";

        public const string AFMItemType = "AFMItemType";

        public const string ChargeTypeError = "ChargeTypeError";

        public const string AFMShippingMode = "AFMShippingMode";

        public const string AFMDirectShip = "AFMDirectShip";

        public const string AFMSupplierDirectShip = "AFMSupplierDirectShip";

        public const string CartZipCode = "CartZipCode";

        public const string ChargeTypeErrorMessage = "Sorry, the item you selected is not currently available online. Your local store may have access to the product.";

        public const string AvaTaxError="AvaTax-Error";

        public const string AtpItemNotAvailableMrssage = "Some item(s) you have selected are no longer available or prices have changed. These unavailable products are noted with red buttons. To proceed to checkout, please remove these unavailable items from your shopping cart, or add them to your wish list.";

        public const string AtpServiceNotAvailableErrorMrssage = "Something went wrong on product availability verification. Try again!!";

        //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
        public const string RSACacheKey = "RSAIDS";
        public const string CartRsaId = "CartRsaId";
        //NE - RxL
        //NS by spriya : DMND0010113 - Synchrony Financing online
        public const string FinancingOptionId = "CartFinanceOptionId";
        public const string RemoveDiscount = "CartRemoveDiscount";
        //NE by spriya : DMND0010113 - Synchrony Financing online
        //NS Hardik Bug : 80575 : Barstool Problem
        public const string AFMReasonCodeSplit = "AFMReasonCodeSplit";
        public const string AFMReasonCodeSplitDescription = "AFMReasonCodeSplitDescription";
        //NE Hardik Bug : 80575 : Barstool Problem
    }
}
