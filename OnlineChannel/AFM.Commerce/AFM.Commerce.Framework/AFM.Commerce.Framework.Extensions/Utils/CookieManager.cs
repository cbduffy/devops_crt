﻿
namespace AFM.Commerce.Framework.Extensions.Utils
{
    using System;
    using System.Runtime.Serialization;
    using System.Web;

    /// <summary>
    /// Cookie exception.
    /// </summary>
    [Serializable]
    public class CookieException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CookieException"/> class.
        /// </summary>
        public CookieException()
            : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CookieException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public CookieException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CookieException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public CookieException(string message, Exception innerException)
            : base(message, innerException)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CookieException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization info.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected CookieException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        { }
    }

    /// <summary>
    /// Manages the domain cookies for the SharePoint storefront site.
    /// </summary>
    public static class CookieManager
    {
        /// <summary>
        /// Gets the specified cookie value from the request.
        /// </summary>
        /// <param name="context">An instance of the HttpContext.</param>
        /// <param name="cookieName">A string containing the name of the cookie.</param>
        /// <returns>a string containing the cookie value.</returns>
        public static string GetRequestCookieValue(HttpContext context, string cookieName)
        {
            try
            {
                if (context != null && context.Request != null && !string.IsNullOrWhiteSpace(cookieName))
                {
                    return GetCookieValue(context.Request.Cookies, cookieName);
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new CookieException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Sets the cookie value with the specified cookie name in the response.
        /// </summary>
        /// <param name="context">An instance of the HttpContext.</param>
        /// <param name="cookieName">A string containing the name of the cookie.</param>
        /// <param name="cookieValue">A string containing the value of the cookie.</param>
        /// <param name="expiry">The date of cookie expiration.</param>
        /// <param name="isSecure">A value indicating whether the cookie is secure.</param>
        /// <param name="isHttpOnly">A value indicating whether the cookie is HttpOnly.</param>
        /// <exception cref="System.ArgumentException">The cookie name cannot be null or contain white space only.</exception>
        /// <exception cref="Microsoft.Dynamics.Retail.SharePoint.Web.Common.CookieException"></exception>
        /// <exception cref="ArgumentException">ArgumentException is thrown when invalid cookie name is specified.</exception>
        public static void SetResponseCookieValue(HttpContext context, string cookieName, string cookieValue, DateTime expiry, bool isSecure, bool isHttpOnly)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(cookieName))
                {
                    throw new ArgumentException("The cookie name cannot be null or contain white space only.");
                }

                if (context != null && context.Response != null)
                {
                    SetCookieValue(context, cookieName, cookieValue, expiry, isSecure, isHttpOnly);
                }
            }
            catch (Exception ex)
            {
                throw new CookieException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Expire the cookie with the specified cookie name in the response.
        /// </summary>
        /// <param name="context">An instance of the HttpContext.</param>
        /// <param name="cookieName">A string containing the name of the cookie.</param>
        public static void DeleteCookie(HttpContext context, string cookieName)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(cookieName))
                {
                    throw new ArgumentException(Resource.CookieCantNullOrSpace);
                }

                if (context != null && context.Response != null)
                {
                    HttpCookie cookie = context.Request.Cookies[cookieName];

                    if (cookie != null)
                    {
                        context.Response.Cookies.Set(new HttpCookie(cookieName)
                        {
                            Path = "/",
                            Value = "",
                            Expires = DateTime.Now.AddYears(-1),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw new CookieException(ex.Message, ex);
            }
        }

        public static bool IsSessionOnlyCookieEnabled()
        {
            bool hasSessionCookie = false;
            bool hasPersistentCookie = false;

            foreach (string key in HttpContext.Current.Request.Cookies.AllKeys)
            {
                hasSessionCookie ^= key.Equals(CookieConstants.MSAXSessionCookieCheck, StringComparison.Ordinal);
                hasPersistentCookie ^= key.Equals(CookieConstants.MSAXPersistentCookieCheck, StringComparison.Ordinal);

                if (hasSessionCookie && hasPersistentCookie)
                {
                    break;
                }
            }
            return hasSessionCookie && !hasPersistentCookie;
        }

        public static string GetCookieValue(HttpCookieCollection collection, string cookieName)
        {
            if (collection != null)
            {
                HttpCookie cookie = collection[cookieName];
                if (cookie != null)
                {
                    return cookie.Value;
                }
            }
            return null;
        }

        /// <summary>
        /// Sets the cookie value.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="cookieName">Name of the cookie.</param>
        /// <param name="cookieValue">The cookie value.</param>
        /// <param name="expiry">The expiry.</param>
        /// <param name="isSecure">if set to <c>true</c> [is secure].</param>
        /// <param name="isHttpOnly">if set to <c>true</c> [is httpOnly].</param>
        public static void SetCookieValue(HttpContext context, string cookieName, string cookieValue, DateTime expiry, bool isSecure, bool isHttpOnly)
        {
            if (context.Response.Cookies != null)
            {
                HttpCookie cookie = new HttpCookie(cookieName)
                {
                    Value = cookieValue,
                    Secure = isSecure,
                    HttpOnly = isHttpOnly,
                };

                // Set date only if persistence is allowed.
                if (!IsSessionOnlyCookieEnabled() && !isSecure)
                {
                    cookie.Expires = expiry;
                }

                // Set will ensure no duplicate keys are persisted.
                context.Response.Cookies.Set(cookie);
                context.Request.Cookies.Set(cookie);
            }
        }
    }
}