﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Extensions.Utils
{
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;

    public class SiteConfiguration : IConfiguration
    {
        /// <summary>
        /// Gets the culture name from the cookie.
        /// </summary>
        /// <returns>The culture name as string.</returns>
        public string GetCurrentCulture()
        {
            return AFMDataUtilities.GetCultureIdFromCookie();
        }
    }
}
