﻿using System;
using System.Transactions;

namespace AFM.Commerce.Framework.Extensions.Utils
{
    public class TransactionUtils
    {
        public static TransactionScope CreateTransactionScope()
        {
            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            transactionOptions.Timeout = new TimeSpan(0, 10, 0);
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }
    }
}
