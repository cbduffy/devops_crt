﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace AFM.Commerce.Framework.Extensions.Utils
{
    [Serializable]
    public class SecurityException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityException"/> class.
        /// </summary>
        public SecurityException()
            : base()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public SecurityException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityException"/> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception, or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public SecurityException(string message, Exception innerException)
            : base(message, innerException)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityException"/> class.
        /// </summary>
        /// <param name="serializationInfo">The serialization info.</param>
        /// <param name="streamingContext">The streaming context.</param>
        protected SecurityException(SerializationInfo serializationInfo, StreamingContext streamingContext)
            : base(serializationInfo, streamingContext)
        { }
    }
    public static class SecurityManager
    {
        private static string EncryptionKeyValue = WebConfigurationManager.AppSettings["CartCompareHashKey"];

        // "salt" value for the PasswordDeriveBytes function calls
        // TODO:: Exception handling
        private static readonly byte[] initVectorBytes = Encoding.ASCII.GetBytes(WebConfigurationManager.AppSettings["PDBSaltValue"]);

        // This constant is used to determine the keysize of the encryption algorithm.
        private const int keysize = 256;

        public static string HashData(string cartId)
        {
            if (string.IsNullOrEmpty(cartId))
            {
                throw new ArgumentNullException("cartId");
            }
            try
            {
                var sessionId = HttpContext.Current.Session.SessionID;
                var hasdableData = cartId.Substring(0, cartId.Length / 2) + sessionId + cartId.Substring((cartId.Length / 2) - 1);
                // step 1, calculate MD5 hash from input
                MD5 md5 = MD5.Create();
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(hasdableData);
                byte[] hash = md5.ComputeHash(inputBytes);

                // step 2, convert byte array to hex string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new SecurityException(ex.Message, ex);
            }

        }

        public static bool CompareHash(string tranId)
        {
            if (string.IsNullOrEmpty(tranId))
            {
                throw new ArgumentNullException("tranId");
            }
            try
            {
                // NS Changes by AxL for Synchrony FDD on 14/12/2015
                //var receivedHash = HashData(AFMDataUtilities.GetCheckoutCartId());
                var receivedHash = EncryptCartID(AFMDataUtilities.GetCheckoutCartId());
                return receivedHash == tranId ? true : false;
                // NE Changes by AxL for Synchrony FDD on 14/12/2015
            }
            catch (Exception ex)
            {
                throw new SecurityException(ex.Message, ex);
            }
        }

        public static string CalculateHashedPassword(string data)
        {
            var _salt = WebConfigurationManager.AppSettings["PaymentGatewayHashKey"];
            using (var sha = SHA256.Create())
            {
                var computedHash = sha.ComputeHash(Encoding.Unicode.GetBytes(data + _salt));
                return ToRestFriendlyFromBase64(Convert.ToBase64String(computedHash));
            }
        }

        public static string ToRestFriendlyFromBase64(string value)
        {
            return value.Trim('=').Replace('+', '-').Replace('/', '_');
        }

        public static string Encrypt(string clearText)
        {
            if (string.IsNullOrEmpty(clearText))
            {
                throw new ArgumentNullException("clearText");
            }
            string EncryptionKey = EncryptionKeyValue;
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                using (Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 }))
                {
                    encryptor.Padding = PaddingMode.PKCS7;
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            if (string.IsNullOrEmpty(cipherText))
            {
                throw new ArgumentNullException("cipherText");
            }
            string EncryptionKey = EncryptionKeyValue;
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                using (Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 }))
                {
                    encryptor.Padding = PaddingMode.PKCS7;
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            return cipherText;
        }

        //TODO::Exception handling
        /// <summary>
        /// Method to encrypt the data
        /// </summary>
        /// <param name="plainText">plain text</param>
        /// <param name="passPhrase">password</param>
        /// <returns></returns>
        public static string EncryptCartID(string plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(EncryptionKeyValue, null))
            {
               
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.Zeros;

                    using (ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                byte[] cipherTextBytes = memoryStream.ToArray();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        

        
        /// <summary>
        /// Method to decrypt the data
        /// </summary>
        /// <param name="cipherText">cipher text</param>
        /// <param name="passPhrase">password</param>
        /// <returns></returns>
        public static string DecryptCartID(string cipherText)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
            using (PasswordDeriveBytes password = new PasswordDeriveBytes(EncryptionKeyValue, null))
            {
                byte[] keyBytes = password.GetBytes(keysize / 8);
                using (RijndaelManaged symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.Zeros;
                    using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
                    {
                        using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                try
                                {
                                    byte[] plainTextBytes = new byte[cipherTextBytes.Length];
                                    int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                    string decryptedValue = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                                    decryptedValue = decryptedValue.TrimEnd('\0');
                                    return decryptedValue;
                                }
                                catch (Exception ex)
                                {

                                    throw ex;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
