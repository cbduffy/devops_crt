﻿namespace AFM.Commerce.Framework.Extensions.Utils
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;
    using System.Xml.Linq;
    using System.Configuration;
    using System.Web.ModelBinding;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.Models;
    using System.Web.Script.Serialization;
    using System.Text.RegularExpressions;
    using System.Net;   
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using System.Web.Caching;
    using AFM.Commerce.Framework.Core.ClientManager;
    public static class AFMDataUtilities
    {

        internal static readonly double SessionTokenCookieMinutesUntilExpiration = TimeSpan.FromDays(30).TotalMinutes;
        internal static readonly string SuccessMessage = "success";
        internal static readonly string FailureMessage = "failed";
        internal static readonly string PaymentErrroCode = "PaymentError";

        private static AFMVendorDetails afmafivendordetail;

        private static AFMRetailParameters ReasonCodeForSplit;//N Hardik Bug : 80575 : Barstool Problem
        /// <summary>
        /// Gets the cart identifier from request cookie.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <param name="cartType">Type of the cart.</param>
        /// <returns></returns>
        /// <exception cref="System.InvalidOperationException">Only a shopping or checkout cart type is supported.</exception>
        public static string GetCartIdFromRequestCookie(SessionType sessionType, CartType cartType)
        {

            if (cartType == CartType.Checkout)
            {
                return GetCheckoutCartIdFromCookie(sessionType);
            }
            else if (cartType == CartType.Shopping)
            {
                return GetShoppingCartIdFromCookie(sessionType);
            }
            else
            {
                string message = string.Format("CartType '{0}' not recognized.", cartType);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Gets the checkout cart identifier from cookie.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <returns>The checkout cart identifier.</returns>
        public static string GetCheckoutCartIdFromCookie(SessionType sessionType)
        {
            string cookieName = GetCartTokenCookieName(sessionType, CartType.Checkout);
            //string checkoutCartId = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieName);
            string checkoutCartId = SessionManager.GetRequestSessionValue(HttpContext.Current, cookieName);
            return checkoutCartId;
        }

        // NS by muthait dated 26 SEP 2014
        /// <summary>
        /// Gets the checkout cart identifier from cookie.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <returns>The checkout cart identifier.</returns>
        public static string GetAnonymousShoppingCartId(string cookieValue)
        {
            string checkoutCartId = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieValue);
            return checkoutCartId;
        }
        // NE by muthait dated 26 SEP 2014


        /// <summary>
        /// Gets the name of the cart token cookie.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <param name="cartType">Type of the cart.</param>
        /// <returns>Name of the cookie that stores the cart identifier.</returns>
        /// <exception cref="System.InvalidOperationException"> Only specific cart types and session types are supported.
        /// </exception>
        public static string GetCartTokenCookieName(SessionType sessionType, CartType cartType)
        {
            string cookieName = string.Empty;
            string message = string.Empty;

            switch (sessionType)
            {
                case SessionType.Anonymous:
                    if (cartType == CartType.Shopping)
                    {
                        cookieName = CookieConstants.AnonymousShoppingCartToken;
                    }
                    else if (cartType == CartType.Checkout)
                    {
                        cookieName = CookieConstants.AnonymousCheckoutCartToken;
                    }
                    else
                    {
                        message = string.Format("CartType '{0}' is invalid.", cartType);
                        throw new InvalidOperationException(message);
                    }
                    break;

                case SessionType.SignedIn:
                    if (cartType == CartType.Shopping)
                    {
                        cookieName = CookieConstants.SignedInShoppingCartToken;
                    }
                    else if (cartType == CartType.Checkout)
                    {
                        cookieName = CookieConstants.SignedInCheckoutCartToken;
                    }
                    else
                    {
                        message = string.Format("CartType '{0}' is invalid.", cartType);
                        throw new InvalidOperationException(message);
                    }
                    break;

                default: message = string.Format("SessionType '{0}' not recognized.", sessionType);
                    throw new InvalidOperationException(message);
            }

            return cookieName;
        }

        /// <summary>
        /// Gets the shopping cart identifier from cookie.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <returns>The shopping cart identifer.</returns>
        public static string GetShoppingCartIdFromCookie(SessionType sessionType)
        {
            string cookieName = GetCartTokenCookieName(sessionType, CartType.Shopping);
            string shoppingCartId = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieName);
            return shoppingCartId;
        }

        /// <summary>
        /// Sets the cart identifier in response cookie.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <param name="cartType">Type of the cart.</param>
        /// <param name="cartId">The cart identifier.</param>
        /// <exception cref="System.InvalidOperationException">Only a shopping or checkout cart type is supported.</exception>
        public static void SetCartIdInResponseCookie(SessionType sessionType, CartType cartType, string cartId)
        {
            if (cartType == CartType.Checkout)
            {
                SetCheckoutCartIdInCookie(sessionType, cartId);
            }
            else if (cartType == CartType.Shopping)
            {
                SetShoppingCartIdInCookie(sessionType, cartId);
            }
            else
            {
                string message = string.Format("CartType '{0}' not recognized.", cartType);
                throw new InvalidOperationException(message);
            }
        }

        /// <summary>
        /// Sets the checkout cart identifier in cookie.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <param name="checkoutCartId">The checkout cart identifier.</param>
        public static void SetCheckoutCartIdInCookie(SessionType sessionType, string checkoutCartId)
        {
            CartType cartType = CartType.Checkout;
            string cookieName = GetCartTokenCookieName(sessionType, cartType);

            //CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, checkoutCartId, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), false, true);
            SessionManager.SetResponseSessionValue(HttpContext.Current, cookieName, checkoutCartId);
        }

        /// <summary>
        /// Determines whether [is cart token cookie secure] for [the specified session type].
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <returns>
        ///   <c>true</c> if [is cart token cookie secure] for [the specified session type]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsCartTokenCookieSecure(SessionType sessionType)
        {
            bool isSecure = true;

            if (sessionType == SessionType.Anonymous)
            {
                isSecure = false;
            }

            return isSecure;
        }

        /// <summary>
        /// Sets the shopping cart identifier in cookie.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <param name="shoppingCartId">The shopping cart identifier.</param>
        public static void SetShoppingCartIdInCookie(SessionType sessionType, string shoppingCartId)
        {
            CartType cartType = CartType.Shopping;
            string cookieName = GetCartTokenCookieName(sessionType, cartType);

            CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, shoppingCartId, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), false, true);
        }

        /// <summary>
        /// Gets the shopping cart identifier based on the current session type.
        /// </summary>
        /// <returns>The shopping cart identifier that corresponds to the current session.</returns>
        public static string GetShoppingCartId()
        {
            SessionType sessionType = GetSessionType();
            string shoppingCartId = GetShoppingCartIdFromCookie(sessionType);
            return shoppingCartId;
        }

        /// <summary>
        /// Gets the type of the current session.
        /// </summary>
        /// <returns>The type of the current session.</returns>
        public static SessionType GetSessionType()
        {
            SessionType sessionType = SessionType.Anonymous;

            if (IsCurrentUserSignedIn())
            {
                sessionType = SessionType.SignedIn;
            }

            return sessionType;
        }

        /// <summary>
        /// Gets the checkout cart identifier based on the current session type.
        /// </summary>
        /// <returns>The checkout cart identifier that corresponds to the current session.</returns>
        public static string GetCheckoutCartId()
        {
            SessionType sessionType = GetSessionType();
            string checkoutCartId = GetCheckoutCartIdFromCookie(sessionType);
            return checkoutCartId;
        }

        /// <summary>
        /// Clears the identifiers for both the shopping cart and checkout cart from the response cookies.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        public static void ClearCartIdResponseCookies(SessionType sessionType)
        {
            ClearCheckoutCartIdResponseCookies(sessionType);
            ClearShoppingCartIdResponseCookies(sessionType);
        }

        /// <summary>
        /// Clears the checkout cart identifier in cookies.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        public static void ClearCheckoutCartIdResponseCookies(SessionType sessionType)
        {
            switch (sessionType)
            {
                case SessionType.Anonymous:
                    //CookieManager.DeleteCookie(HttpContext.Current, CookieConstants.AnonymousCheckoutCartToken);
                    SessionManager.DeleteSessionKey(HttpContext.Current, CookieConstants.AnonymousCheckoutCartToken);
                    break;

                case SessionType.SignedIn:
                    //CookieManager.DeleteCookie(HttpContext.Current, CookieConstants.SignedInCheckoutCartToken);
                     SessionManager.DeleteSessionKey(HttpContext.Current, CookieConstants.SignedInCheckoutCartToken);
                    break;

                default:
                    //CookieManager.DeleteCookie(HttpContext.Current, CookieConstants.AnonymousCheckoutCartToken);
                     SessionManager.DeleteSessionKey(HttpContext.Current, CookieConstants.AnonymousCheckoutCartToken);
                    //CookieManager.DeleteCookie(HttpContext.Current, CookieConstants.SignedInCheckoutCartToken);
                     SessionManager.DeleteSessionKey(HttpContext.Current, CookieConstants.SignedInCheckoutCartToken);
                    break;
            }
        }

        /// <summary>
        /// Clears the shopping cart identifier in cookies.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        public static void ClearShoppingCartIdResponseCookies(SessionType sessionType)
        {
            switch (sessionType)
            {
                case SessionType.Anonymous:
                    CookieManager.DeleteCookie(HttpContext.Current, CookieConstants.AnonymousShoppingCartToken);

                    break;

                case SessionType.SignedIn:

                    CookieManager.DeleteCookie(HttpContext.Current, CookieConstants.SignedInShoppingCartToken);
                    break;

                default:
                    CookieManager.DeleteCookie(HttpContext.Current, CookieConstants.AnonymousShoppingCartToken);
                    CookieManager.DeleteCookie(HttpContext.Current, CookieConstants.SignedInShoppingCartToken);
                    break;
            }
        }

        /// <summary>
        /// Sets the customer identifier in cookie.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        public static void SetCustomerIdInCookie(string customerId)
        {
            string cookieName = CookieConstants.CustomerIdToken;
            if (!string.IsNullOrEmpty(customerId))
                customerId = AFM.Commerce.Framework.Extensions.Utils.SecurityManager.Encrypt(customerId);
            CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, customerId, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), false, true);
            //SessionManager.SetResponseSessionValue(HttpContext.Current, cookieName, customerId);
        }

        /// <summary>
        /// Gets the customer identifier from cookie.
        /// </summary>
        /// <returns>The customer identifier.</returns>
        public static string GetCustomerIdFromCookie()
        {
            string cookieName = CookieConstants.CustomerIdToken;
            string customerId = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieName);
            if (!string.IsNullOrEmpty(customerId))
                customerId = AFM.Commerce.Framework.Extensions.Utils.SecurityManager.Decrypt(customerId);
           // string customerId = SessionManager.GetRequestSessionValue(HttpContext.Current, cookieName);

            return (string.IsNullOrWhiteSpace(customerId) ? string.Empty : customerId);
        }

       
        /// <summary>
        /// Clears the customer identifier from cookie.
        /// </summary>
        public static void ClearCustomerIdFromCookie()
        {
            string cookieName = CookieConstants.CustomerIdToken;
            CookieManager.DeleteCookie(HttpContext.Current, cookieName);
            //SessionManager.DeleteSessionKey(HttpContext.Current, cookieName);
        }

        /// <summary>
        /// Sets the customer temp identifier in cookie.
        /// </summary>
        /// <param name="customerId">The customer identifier.</param>
        public static void SetCustomerIdTempInCookie(string customerId)
        {
            string cookieName = CookieConstants.CustomerIdTempToken;
            //CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, customerId, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), false, true);
            SessionManager.SetResponseSessionValue(HttpContext.Current, cookieName, customerId);
        }

        /// <summary>
        /// Gets the customer temp identifier from cookie.
        /// </summary>
        /// <returns>The customer temp identifier.</returns>
        public static string GetCustomerIdTempFromcookie()
        {
            string cookieName = CookieConstants.CustomerIdTempToken;
            //string customerId = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieName);
            string customerId = SessionManager.GetRequestSessionValue(HttpContext.Current, cookieName);
            return (string.IsNullOrWhiteSpace(customerId) ? string.Empty : customerId);
        }
        

        /// <summary>
        /// Clears the customer temp identifier from cookie.
        /// </summary>
        public static void ClearCustomerIdTempFromCookie()
        {
            string cookieName = CookieConstants.CustomerIdTempToken;
            //CookieManager.DeleteCookie(HttpContext.Current, cookieName);
             SessionManager.DeleteSessionKey(HttpContext.Current, cookieName);
        }


        /// <summary>
        /// Sets the culture Id in cookie.
        /// </summary>
        /// <param name="language">The culture Id.</param>
        public static void SetCultureIdInCookie(string language)
        {
            string cookieName = CookieConstants.CultureIdToken;
            // Setting HttpOnly (last parameter) to false so that we can access the cookies from the client 
            // (eg. Resources.ts is using cookies to retrieve UICulture).
            CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, language, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), isSecure: false, isHttpOnly: true);
        }

        /// <summary>
        /// Gets the culture Id from cookie.
        /// </summary>
        /// <returns>The Culture Id.</returns>
        public static string GetCultureIdFromCookie()
        {
            string cookieName = CookieConstants.CultureIdToken;
            string language = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieName);

            return (string.IsNullOrWhiteSpace(language) ? string.Empty : language);
        }

        /// <summary>
        /// Clears the culture Id from cookie.
        /// </summary>
        public static void ClearCultureIdFromCookie()
        {
            string cookieName = CookieConstants.CultureIdToken;
            CookieManager.DeleteCookie(HttpContext.Current, cookieName);
        }

        /// <summary>
        /// Determines whether the current user is signed in.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if [is current user signed in]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsCurrentUserSignedIn()
        {
            string customerId = GetCustomerIdFromCookie();
            return !string.IsNullOrWhiteSpace(customerId);
        }

        #region [NotCompiling]
        /// <summary>
        /// Executes the given service action and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="action">Service action.</param>
        public static void ExecuteAndHandleExceptions(AFMServiceResponse response, System.Action action)
        {
            if (response == null || response.Errors == null)
            {
                return;
            }

            try
            {
                action();
            }
            catch (Exception ex)
            {
                response.Errors.AddRange(HandleException(ex));
            }
        }

        /// <summary>
        /// Checks for type of exception, gets proper exception message from resource and logs it if necessary.
        /// </summary>
        /// <param name="ex">Exception.</param>
        /// <returns>Response errors.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown when the exception parameter is null.</exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        public static Collection<ResponseError> HandleException(Exception ex)
        {
            if (ex == null)
            {
                ex = new ArgumentNullException("ex", "Exception cannot be null.");
            }

            Collection<ResponseError> responseErrors = new Collection<ResponseError>();
            Type exceptionType = ex.GetType();

            if (exceptionType == typeof(NotificationException))
            {
                responseErrors.AddRange(ProcessNotificationException((NotificationException)ex));
            }
            else if (exceptionType == typeof(DataValidationException))
            {
                responseErrors.AddRange(ProcessDataValidationException((DataValidationException)ex));
            }
            else if (exceptionType == typeof(PaymentException))
            {
                // For now we do not have any special handling for a PaymentException type. Hence, treating it like any other commerce runtime exception.
                responseErrors.AddRange(ProcessPaymentException((PaymentException)ex));
            }
            else if (exceptionType == typeof(CommerceRuntimeException))
            {
                responseErrors.AddRange(ProcessCommerceRuntimeException((CommerceRuntimeException)ex));
            }
            else if (exceptionType == typeof(AggregateException))
            {
                responseErrors.AddRange(ProcessAggregateException((AggregateException)ex));
            }
            else
            {
                responseErrors.AddRange(ProcessUnrecognizedException(ex));
            }

            responseErrors = GetUniqueErrorMessageResponseErrors(responseErrors);
            return responseErrors;
        }

        /// <summary>
        /// Processes the notification exception.
        /// </summary>
        /// <param name="notificationException">The notification exception.</param>
        /// <returns>Collection of the response errors extracted from the exception.</returns>
        public static Collection<ResponseError> ProcessNotificationException(NotificationException notificationException)
        {
            Collection<ResponseError> responseErrors = new Collection<ResponseError>();

            var innerExceptionType = notificationException.InnerException.GetType();

            if (innerExceptionType == typeof(DataValidationException))
            {
                responseErrors = ProcessDataValidationException((DataValidationException)notificationException.InnerException);
            }
            else if (innerExceptionType == typeof(Microsoft.Dynamics.Commerce.Runtime.ConfigurationException) || innerExceptionType == typeof(CommerceRuntimeException))
            {
                responseErrors = ProcessCommerceRuntimeException((CommerceRuntimeException)notificationException.InnerException);
            }
            else
            {
                responseErrors = ProcessUnrecognizedException(notificationException.InnerException);
            }

            return responseErrors;
        }

        /// <summary>
        /// Processes the commerce runtime exception.
        /// </summary>
        /// <param name="ex">The commerce runtime exception.</param>
        /// <returns>Collection of the response errors extracted from the exception.</returns>
        public static Collection<ResponseError> ProcessCommerceRuntimeException(CommerceRuntimeException ex)
        {
            Collection<ResponseError> responseErrors = new Collection<ResponseError>();

            var errorMessage = GetErrorMessage(ex.ErrorResourceId, typeof(CommerceRuntimeException));

            ResponseError responseError = new ResponseError()
            {
                ErrorCode = ex.ErrorResourceId,
                ErrorMessage = errorMessage
            };

            responseErrors.Add(responseError);

            return responseErrors;
        }

        /// <summary>
        /// Processes the commerce runtime exception.
        /// </summary>
        /// <param name="ex">The commerce runtime exception.</param>
        /// <returns>Collection of the response errors extracted from the exception.</returns>
        public static Collection<ResponseError> ProcessPaymentException(PaymentException ex)
        {
            Collection<ResponseError> responseErrors = new Collection<ResponseError>();

            var errorMessage = ex.Message; //GetErrorMessage(ex.ErrorResourceId, typeof(CommerceRuntimeException));

            ResponseError responseError = new ResponseError()
            {
                ErrorCode = PaymentErrroCode,
                ErrorMessage = errorMessage
            };

            responseErrors.Add(responseError);

            return responseErrors;
        }

        /// <summary>
        /// Processes the aggregate exception.
        /// </summary>
        /// <param name="aggregrateException">The aggregrate exception.</param>
        /// <returns>Collection of the response errors extracted from the exception.</returns>
        public static Collection<ResponseError> ProcessAggregateException(AggregateException aggregrateException)
        {
            Collection<ResponseError> responseErrors = new Collection<ResponseError>();
            foreach (Exception innerException in aggregrateException.InnerExceptions)
            {
                Collection<ResponseError> responseErrorsFromInnerException = null;

                Type exceptionType = innerException.GetType();
                if (exceptionType == typeof(CommerceRuntimeException))
                {
                    responseErrorsFromInnerException = ProcessCommerceRuntimeException((CommerceRuntimeException)innerException);
                }
                else
                {
                    responseErrorsFromInnerException = ProcessUnrecognizedException(innerException);
                }

                responseErrors.AddRange(responseErrorsFromInnerException);
            }

            return responseErrors;
        }

        /// <summary>
        /// Processes an unrecognized exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>A collection of response errors after processing the exception.</returns>
        public static Collection<ResponseError> ProcessUnrecognizedException(Exception exception)
        {
            Collection<ResponseError> responseErrors = new Collection<ResponseError>();

            // Get generic error message and log exception
            var responseError = new ResponseError()
            {
                ErrorMessage = ServiceResources.GenericErrorMessage
            };

            responseErrors.Add(responseError);

            return responseErrors;
        }

        /// <summary>
        /// Processes a data validation exception.
        /// </summary>
        /// <param name="exception">The data validation exception.</param>
        /// <returns>
        /// A collection of response errors.
        /// </returns>
        public static Collection<ResponseError> ProcessDataValidationException(DataValidationException exception)
        {
            Collection<ResponseError> responseErrors = new Collection<ResponseError>();

            if (exception.ValidationResults.Count == 0)
            {
                ResponseError responseError = ProcessDataValidationExceptionHelper(exception.ErrorResourceId, exception.Message, exception);
                responseErrors.Add(responseError);
            }
            else
            {
                foreach (DataValidationFailure validationResult in exception.ValidationResults)
                {
                    string extendedExceptionMessage = string.Format(CultureInfo.CurrentCulture, "{0} - [ValidationFailure]:{1}", exception.Message, validationResult.ToString());
                    ResponseError responseError;
                    string errorResourceId = validationResult.ErrorResourceId;
                    if (errorResourceId.Equals("Microsoft_Dynamics_Commerce_Runtime_InvalidShippingAddress"))
                    {
                        errorResourceId = GetSpecificResourceCodeForInvalidShippingAddressException(validationResult);
                    }
                    responseError = ProcessDataValidationExceptionHelper(errorResourceId, extendedExceptionMessage, exception);
                    responseErrors.Add(responseError);
                }
            }

            return responseErrors;
        }

        /// <summary>
        /// Gets the specific resource code for invalid shipping address based on the member name of the validation result.
        /// </summary>
        /// <param name="validationResult">The validation result.</param>
        /// <returns>The specific resource code for the invalid shipping address.</returns>
        public static string GetSpecificResourceCodeForInvalidShippingAddressException(DataValidationFailure validationResult)
        {
            string memberName = string.Empty;

            if (validationResult.MemberNames.Count > 0)
            {
                memberName = validationResult.MemberNames.ToList()[0];
            }
            else
            {
                return validationResult.ErrorResourceId;
            }

            string errorResourceId = string.Empty;

            switch (memberName)
            {
                case "STATEPROVINCEID":
                    errorResourceId = "Microsoft_Dynamics_Commerce_Runtime_InvalidState";
                    break;
                case "City":
                    errorResourceId = "Microsoft_Dynamics_Commerce_Runtime_InvalidCity";
                    break;
                case "ZIPPOSTALCODE":
                    errorResourceId = "Microsoft_Dynamics_Commerce_Runtime_InvalidZipCode";
                    break;
                default:
                    errorResourceId = validationResult.ErrorResourceId;
                    break;
            }

            return errorResourceId;
        }

        /// <summary>
        /// Gets the localized error message for a given error identifier. Returns a generic message for an empty input.
        /// </summary>
        /// <param name="errorResourceId">The error resource identifier.</param>
        /// <param name="exceptionType">Type of the exception (which is used to calculate the generic error message for fallback purposes).</param>
        /// <returns>
        /// Localized error message.
        /// </returns>
        public static string GetErrorMessage(string errorResourceId, Type exceptionType)
        {
            string errorMessage = string.Empty;

            if (string.IsNullOrWhiteSpace(errorResourceId))
            {
                if (exceptionType == typeof(DataValidationException))
                {
                    errorMessage = ServiceResources.GenericValidationError;
                }
                else if (exceptionType == typeof(CommerceRuntimeException))
                {
                    errorMessage = ServiceResources.GenericCommerceError;
                }
                else
                {
                    errorMessage = ServiceResources.GenericErrorMessage;
                }
            }
            else
            {
                errorMessage = ServiceResources.ResourceManager.GetString(errorResourceId);
            }

            return errorMessage;
        }

        /// <summary>
        /// Processes the error resource identifier.
        /// </summary>
        /// <param name="errorResourceId">The error resource identifier.</param>
        /// <param name="extendedErrorMessage">The extended error message.</param>
        /// <param name="exception">The data validation exception.</param>
        /// <returns>
        /// A response error object for the specified error resource identifier.
        /// </returns>
        public static ResponseError ProcessDataValidationExceptionHelper(string errorResourceId, string extendedErrorMessage, DataValidationException exception)
        {
            string errorMessage = GetErrorMessage(errorResourceId, typeof(DataValidationException));
            ResponseError responseError = new ResponseError()
            {
                ErrorCode = errorResourceId,
                ErrorMessage = errorMessage
            };

            return responseError;
        }

        /// <summary>
        /// Gets response errors that have unique error messages.
        /// We filter based on error message instead of the error code, because different error codes can map to the same error message.
        /// Since we ultimately surface the error message to the C2 user, we want to display only unique error messages.
        /// </summary>
        /// <param name="responseErrors">The response errors.</param>
        /// <returns>Response errors with non-duplicate error codes.</returns>
        public static Collection<ResponseError> GetUniqueErrorMessageResponseErrors(IEnumerable<ResponseError> responseErrors)
        {
            Collection<ResponseError> uniqueResponseErrors = new Collection<ResponseError>();
            HashSet<string> errorMessageHashSet = new HashSet<string>();

            foreach (var responseError in responseErrors)
            {
                string errorMessage = responseError.ErrorMessage;

                if (errorMessage == null)
                {
                    errorMessage = string.Empty;
                }

                if (!errorMessageHashSet.Contains(errorMessage))
                {
                    uniqueResponseErrors.Add(responseError);
                    errorMessageHashSet.Add(errorMessage);
                }
            }

            return uniqueResponseErrors;
        }

        #endregion

        //NS by muthait
        /// <summary>
        /// Sets the AFMZipCode identifier in cookie.
        /// </summary>
        /// <param name="afmZipCode">The AFMZipCode identifier.</param>
        public static string SetAFMZipCodeInCookie(string afmZipCode)
        {
            if (afmZipCode.Length > 5)
            {
                afmZipCode = afmZipCode.Substring(0, 5);
            }
            string cookieName = CookieConstants.AFMZipCode;
            CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, afmZipCode, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), false, true);
            return SuccessMessage;
        }

        public static string SendOrderSummaryInCookie(AFMOrderSummary orderSummary)
        {
            string insertJson = new JavaScriptSerializer().Serialize(orderSummary);

            string cookieName = CookieConstants.OrderSummary;
            try
            {

                CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, insertJson, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), false, true);
                return SuccessMessage;
            }
            catch (Exception)
            {
                return FailureMessage;
            }

        }

        /// <summary>
        /// Gets the customer identifier from cookie.
        /// </summary>
        /// <returns>The customer identifier.</returns>
        public static string GetAFMZipCodeFromCookie()
        {
            string cookieName = CookieConstants.AFMZipCode;
            string afmZipCode = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieName);
            return (string.IsNullOrWhiteSpace(afmZipCode) ? string.Empty : afmZipCode);
        }

        /// <summary>
        /// Clears the customer identifier from cookie.
        /// </summary>
        public static void ClearAFMZipCodeFromCookie()
        {
            string cookieName = CookieConstants.AFMZipCode;
            CookieManager.DeleteCookie(HttpContext.Current, cookieName);
        }

        /// <summary>
        /// Clears the customer identifier from cookie.
        /// </summary>
        public static void ClearUserCartInfoFromCookie()
        {
            string cookieName = CookieConstants.UserCartInfo;
            //CookieManager.DeleteCookie(HttpContext.Current, cookieName);
            SessionManager.DeleteSessionKey(HttpContext.Current, cookieName);
        }

        public static void ClearFinanceOptionIdFromCookie()
        {
            string cookieName = CookieConstants.FinancingOptionId;
            CookieManager.DeleteCookie(HttpContext.Current, cookieName);
        }

        /// <summary>
        /// Gets the user cart info from cookie.
        /// </summary>
        /// <returns>The user cart info identifier.</returns>
        public static AFMUserCartData GetUserCartInfo()
        {
            string cookieName = CookieConstants.UserCartInfo;
            //string userCartInfo = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieName);
            string userCartInfo = SessionManager.GetRequestSessionValue(HttpContext.Current, cookieName);
            if (!string.IsNullOrWhiteSpace(userCartInfo))
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                if (userCartInfo.Contains("%2C"))
                    userCartInfo = userCartInfo.Replace("%2C", ",");
                AFMUserCartData response = js.Deserialize<AFMUserCartData>(userCartInfo);

                return response;
            }
            //NS IsOptinDeals is enabled by default -Sakalija for bug:72453
            return new AFMUserCartData() { ShippingAddress = new Address(), PaymentAddress = new Address(),IsOptinDeals = true};
            //NE IsOptinDeals is enabled by default
        }

        /// <summary>
        /// Set the user cart info from cookie.
        /// </summary>
        public static string SetUserCartInfo(AFMUserCartData userCartInfo)
        {
            if (userCartInfo.PaymentAddress != null)
            {
                userCartInfo.PaymentAddress.ExtensionData = null;
            }
            if (userCartInfo.ShippingAddress != null)
            {
                userCartInfo.ShippingAddress.ExtensionData = null;
            }

            string insertJson = new JavaScriptSerializer().Serialize(userCartInfo);
            string cookieName = CookieConstants.UserCartInfo;
            try
            {
                //CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, insertJson, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), false, true);
                SessionManager.SetResponseSessionValue(HttpContext.Current, cookieName, insertJson);
                return SuccessMessage;
            }
            catch (Exception)
            {
                return FailureMessage;
            }

        }

        public static string SetPhoneFormat(string phone)
        {
            if (!string.IsNullOrEmpty(phone))
            {
                phone = phone.Trim().Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
                return phone;
            }
            return phone;
        }


        /// <summary>
        /// Clears the customer identifier from cookie.
        /// </summary>
        public static void ClearUserPageModeFromCookie()
        {
            string cookieName = CookieConstants.UserPageMode;
            //CookieManager.DeleteCookie(HttpContext.Current, cookieName);
            SessionManager.DeleteSessionKey(HttpContext.Current, cookieName);
        }

        /// <summary>
        /// Gets the user cart info from cookie.
        /// </summary>
        /// <returns>The user cart info identifier.</returns>
        public static AFMUserPageMode GetUserPageMode()
        {
            string cookieName = CookieConstants.UserPageMode;
            //string userCartInfo = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieName);
            string userCartInfo = SessionManager.GetRequestSessionValue(HttpContext.Current, cookieName);
            if (!string.IsNullOrWhiteSpace(userCartInfo))
            {
                var js = new JavaScriptSerializer();
                var response = js.Deserialize<AFMUserPageMode>(userCartInfo);

                return response;
            }
            return null;
        }

        /// <summary>
        /// Set the user cart info from cookie.
        /// </summary>
        public static string SetUserPageMode(AFMUserPageMode userPageMode)
        {
            string insertJson = new JavaScriptSerializer().Serialize(userPageMode);

            string cookieName = CookieConstants.UserPageMode;
            try
            {
                //CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, insertJson, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), false, true);
                SessionManager.SetResponseSessionValue(HttpContext.Current, cookieName, insertJson);
                return SuccessMessage;
            }
            catch (Exception)
            {
                return FailureMessage;
            }

        }

        private const string emailPattern = @"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$";
        private const string zipCodePattern = @"^\d{5}$|^\d{5}-\d{4}$";
        private const string specialCharacterPattern = @"^(a-z|A-Z|0-9)*[^#'$%^&*()~!+={}<>|]*$";

        /// <summary>
        /// Validate the input string against pattern
        /// </summary>
        /// <param name="inputString">validating string</param>
        /// <returns>bool</returns>
        public static bool IsValidString(string sourceString, string pattern = specialCharacterPattern)
        {
            if (!string.IsNullOrEmpty(sourceString))
            {
                Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
                Match match = regex.Match(sourceString);
                if (!match.Success)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsValidEmail(string email)
        {
            return IsValidString(email, emailPattern);
        }

        public static bool IsValidZipCode(string zipCode)
        {
            return IsValidString(zipCode, zipCodePattern);
        }

        public static void SetFinanceOptionId(string optionId)
        {
            string cookieName = CookieConstants.FinancingOptionId;
            //SessionManager.SetResponseSessionValue(HttpContext.Current, cookieName, optionId);
            CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, optionId, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), false, true);        
        }
        public static string GetFinanceOptionIdFromCookie()
        {
            string cookieName = CookieConstants.FinancingOptionId;
            //string financingOptionId = SessionManager.GetRequestSessionValue(HttpContext.Current, cookieName);
            string financingOptionId = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieName);
            return (string.IsNullOrWhiteSpace(financingOptionId) ? string.Empty : financingOptionId);
        }

        /// <summary>
        /// Sets the order identifier in cookie.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <param name="salesOrderrId">The order identifier.</param>
        public static void SetOrderIdInCookie(string salesOrderId)
        {            
            string cookieName = CookieConstants.SalesOrderId;

            //CookieManager.SetResponseCookieValue(HttpContext.Current, cookieName, salesOrderId, DateTime.Now.AddMinutes(SessionTokenCookieMinutesUntilExpiration), false, true);
            SessionManager.SetResponseSessionValue(HttpContext.Current, cookieName, salesOrderId);
        }

        /// <summary>
        /// Sets the order identifier in cookie.
        /// </summary>
        /// <param name="sessionType">Type of the session.</param>
        /// <param name="salesOrderrId">The order identifier.</param>
        public static void ClearOrderIdFromCookie()
        {
            string cookieName = CookieConstants.SalesOrderId;

            //CookieManager.DeleteCookie(HttpContext.Current, cookieName);
            SessionManager.DeleteSessionKey(HttpContext.Current, cookieName);
        }

        /// <summary>
        /// Gets the order identifier from cookie.
        /// </summary>
        /// <returns>The customer identifier.</returns>
        public static string GetOrderrIdFromCookie()
        {
            string cookieName = CookieConstants.SalesOrderId;
            //string salesOrderId = CookieManager.GetRequestCookieValue(HttpContext.Current, cookieName);
            string salesOrderId = SessionManager.GetRequestSessionValue(HttpContext.Current, cookieName);
            return (string.IsNullOrWhiteSpace(salesOrderId) ? string.Empty : salesOrderId);
        }        
        //NS: By Spriya: Bug#61496: ECS - Subtotal, Home Delivery and Sales tax charges are $0.00 under Order Summary on Payment screen 
        public static void SetPaymentOrderSummaryInCookie(dynamic orderSummary)
        {
            string cookieName = CookieConstants.PaymentOrderSummary;

            SessionManager.SetResponseSessionValue(HttpContext.Current, cookieName, orderSummary);
        }
        public static void ClearPaymentOrderSummaryFromCookie()
        {
            string cookieName = CookieConstants.PaymentOrderSummary;

            SessionManager.DeleteSessionKey(HttpContext.Current, cookieName);
        }
        public static dynamic GetPaymentOrderSummaryFromCookie()
        {
            string cookieName = CookieConstants.PaymentOrderSummary;
            dynamic orderSummary = SessionManager.GetRequestSessionValue(HttpContext.Current, cookieName);
            return orderSummary; 
        }
        //NE: By Spriya Bug#61496

        public static AFMVendorDetails getAfiVendorDetail()
        {
            if (afmafivendordetail == null)
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                GetAFIVendorDetailsServiceRequest vendorDetailRequest =
                new GetAFIVendorDetailsServiceRequest()
                ;
                var reqContext = new RequestContext(runtime);
                GetVendorDetailsServiceResponse vendorDetailResponse = runtime.Execute<GetVendorDetailsServiceResponse>(
                        vendorDetailRequest, reqContext,
                        true);
                afmafivendordetail = vendorDetailResponse.VendorDetails[0];
            }
            return afmafivendordetail;
        }

        public static AFMVendorDetails getZipCodeVendorDetail(string Zipcode)
        {
            CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
            GetZipCodeVendorDetailsServiceRequest vendorDetailRequest =
            new GetZipCodeVendorDetailsServiceRequest(Zipcode)
            ;
            var reqContext = new RequestContext(runtime);
            GetVendorDetailsServiceResponse vendorDetailResponse = runtime.Execute<GetVendorDetailsServiceResponse>(
                    vendorDetailRequest, reqContext,
                    true);
            if (vendorDetailResponse.VendorDetails.Count > 0)
                return vendorDetailResponse.VendorDetails[0];
            else
                return null;
        }

        //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
        /// <summary>
        /// This method provides list of RSA Ids from cahche. If cache is null then it fecthes the list by calling GetRsaIds of AFMCartManager class
        /// </summary>
        /// <returns></returns>
        public static List<AFMRsaId> GetRsaIds(string rsaId = "")
        {
            List<AFMRsaId> rsaIdList = null;//HttpContext.Current.Cache[CartConstant.RSACacheKey] as List<AFMRsaId>;
            if (rsaIdList == null)
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                AFMCartManager cartManager = new AFMCartManager(runtime);
                rsaIdList = cartManager.GetRsaIds(rsaId); 
                //if(rsaIdList != null)
                //HttpContext.Current.Cache.Insert(CartConstant.RSACacheKey, rsaIdList, null, DateTime.Now.AddDays(1), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            return rsaIdList;
        }

        public static List<AFM.Commerce.Framework.Core.Models.AFMSynchronyOptions> GetSynchronyOptions()
        {
            List<AFM.Commerce.Framework.Core.Models.AFMSynchronyOptions> synchronyOptionsList = null;
            if (synchronyOptionsList == null)
            {
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                AFMCartManager cartManager = new AFMCartManager(runtime);
                synchronyOptionsList = cartManager.GetSynchronyOptions();
             }
            return synchronyOptionsList;
        }

        /// <summary>
        /// This method returns Recid of given rsa id
        /// </summary>
        /// <param name="rsaId">RSA ID</param>
        /// <returns>Record Id</returns>
        public static long GetRsaRecId(string rsaId)
        {
            List<AFMRsaId> rsaIdList = AFMDataUtilities.GetRsaIds(rsaId);
            foreach(var rsa in rsaIdList)
            {
                if (string.Equals(rsa.RsaId, rsaId, StringComparison.OrdinalIgnoreCase))
                    return rsa.RecId;
            }
            return 0;
        }
        //NE - RxL

        //NS Hardik Bug : 80575 : Barstool Problem
        public static AFMRetailParameters GetReasonCodeSplit()
        {
            if (ReasonCodeForSplit==null)
            {
                AFMRetailParametersClientManager retailparamManager = AFMRetailParametersClientManager.Create(CrtUtilities.GetCommerceRuntime());                
                ReasonCodeForSplit = retailparamManager.getRetailParameters();
            }
            return ReasonCodeForSplit;
        }
        //NS Hardik Bug : 80575 : Barstool Problem
    }
}
