﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Extensions.Utils
{
   /// <summary>
    /// Centralized location for managing all of the constants.
    /// </summary>
    public static class CommonConstants
    {
        /// <summary>
        /// Represents the name of the search property that contains the item's title.
        /// </summary>
        public static readonly string TitleSearchPropertyName = "Title";

        /// <summary>
        /// Represents the name of the search property that contains the item's configuration.
        /// </summary>
        public static readonly string ConfigurationSearchPropertyName = "RetConfiguration";

        /// <summary>
        /// Represents the name of the search property that contains the item's size.
        /// </summary>
        public static readonly string SizeSearchPropertyName = "Size";

        /// <summary>
        /// Represents the name of the search property that contains the item's color.
        /// </summary>
        public static readonly string ColorSearchPropertyName = "Color";

        /// <summary>
        /// Represents the name of the search property that contains the item's style.
        /// </summary>
        public static readonly string StyleSearchPropertyName = "Style";

        /// <summary>
        /// Represents the name of the search property that contains the item's image.
        /// </summary>
        public static readonly string ImageSearchPropertyName = "Image";

        /// <summary>
        /// Represents the name of the search property that contains the item's path.
        /// </summary>
        public static readonly string PathSearchPropertyName = "Path";

        /// <summary>
        /// Represents the name of the search property that contains the item's description.
        /// </summary>
        public static readonly string DescriptionSearchPropertyName = "Description";

        /// <summary>
        /// Represents the image location key name in app settings.
        /// </summary>
        public static readonly string ImagesLocationKeyName = "Images_Location";

        //public static readonly string[] UnitOfMeasure = { "Carton", "CN" };
    }
}