﻿namespace AFM.Commerce.Framework.Extensions.Utils
{
    /// <summary>
    /// Centralized location for managing all of the cookies.
    /// </summary>
    public class CookieConstants
    {
        /// <summary>
        /// Cookie name for the Retail request digest.
        /// </summary>
        public static readonly string RetailRequestDigestCookieName = "rrd";

        /// <summary>
        /// Cookie name for shopping cart token for the signed-in user.
        /// </summary>
        public static readonly string SignedInShoppingCartToken = "sisct";

        /// <summary>
        /// Cookie name for checkout cart token for the signed-in user.
        /// </summary>
        public static readonly string SignedInCheckoutCartToken = "sicct";

        /// <summary>
        /// Cookie name for shopping cart token for the anonymous user.
        /// </summary>
        public static readonly string AnonymousShoppingCartToken = "sct";

        /// <summary>
        /// Cookie name for checkout cart token for the anonymous user.
        /// </summary>
        public static readonly string AnonymousCheckoutCartToken = "cct";

        /// <summary>
        /// Token to test session cookie.
        /// </summary>
        public const string MSAXSessionCookieCheck = "MSAXSCC";

        /// <summary>
        /// Token to test regular cookie.
        /// </summary>
        public const string MSAXPersistentCookieCheck = "MSAXPCC";

        /// <summary>
        /// Cookie name for the Default Store Channel Id.
        /// </summary>
        public static readonly string DefaultStoreChannelIdentifier = "dscid";

        /// <summary>
        /// Cookie name for the url of the product being reviewed.
        /// </summary>
        public static readonly string ProductReviewProductUrl = "ProductReviewProductUrl";

        /// <summary>
        /// Cookie name for the image url of the product being reviewed.
        /// </summary>
        public static readonly string ProductReviewImageUrl = "ProductReviewImageUrl";

        /// <summary>
        /// Cookie name for the name of the product being reviewed.
        /// </summary>
        public static readonly string ProductReviewProductName = "ProductReviewProductName";

        /// <summary>
        /// Cookie name for the description of the product being reviewed.
        /// </summary>
        public static readonly string ProductReviewProductDescription = "ProductReviewProductDescription";

        /// <summary>
        /// Cookie name for the customer id.
        /// </summary>
        public static readonly string CustomerIdToken = "cid";

        /// <summary>
        /// Cookie name for the culture Id.
        /// </summary>
        public static readonly string CultureIdToken = "cuid";

        //NS by muthait
        /// <summary>
        /// Cookie name for the AFMZipCode.
        /// </summary>
        public static readonly string AFMZipCode = "AFMZipCode";

        //NS by muthait
        /// <summary>
        /// Cookie name for the AFMZipCode.
        /// </summary>
        public static readonly string CustomerIdTempToken = "CustomerID";

        /// <summary>
        /// Cookie name for the AFMZipCode.
        /// </summary>
        public static readonly string AFMIsOlderThan13 = "AFMIsOlderThan13";

        /// <summary>
        /// Cookie name for the AFMZipCode.
        /// </summary>
        public static readonly string AFMIsGuestCheckOut = "AFMIsGuestCheckOut";

        /// <summary>
        /// Cookie name for the AFMZipCode.
        /// </summary>
        public static readonly string AFMGuestCustomerId = "AFMGuestCustomerId";

        /// <summary>
        /// Cookie name for the UserCartInfo.
        /// </summary>
        public static readonly string UserCartInfo = "UserCartInfo";

        /// <summary>
        /// Cookie name for the UserCartInfo.
        /// </summary>
        public static readonly string OrderSummary = "OrderSummary";

        /// <summary>
        /// Cookie name for the UserCartInfo.
        /// </summary>
        public static readonly string UserPageMode = "UserPageMode";
        
        //NE by muthait

        /// <summary>
        /// Cookie name for the SalesOrder Id.
        /// </summary>
        public static readonly string SalesOrderId = "soid";

        //NS: By Spriya: Bug#61496: ECS - Subtotal, Home Delivery and Sales tax charges are $0.00 under Order Summary on Payment screen 
        public static readonly string PaymentOrderSummary = "PaymentOrderSummary";

        public static readonly string FinancingOptionId = "FinanceOptionId";
    }
}
