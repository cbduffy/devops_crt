﻿using AFM.Commerce.Framework.Core.CRTServices;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.Workflows.Request;
using AFM.Commerce.Framework.Core.Workflows.Response;
using AFM.Commerce.Runtime.Services.Common;
using Microsoft.Dynamics.Commerce.Runtime.Data;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.Workflows
{
     [Export(typeof(IRequestHandler))]
    class AFMConfirmationNumberRequestHandler : WorkflowRequestHandler<GetAFMConfirmationNumberRequest, GetAFMConfirmationNumberResponse>
    {
        public override IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(GetAFMConfirmationNumberRequest);
            }
        }
        /// <summary>
        /// This method handles workflow request
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        protected override GetAFMConfirmationNumberResponse Process(GetAFMConfirmationNumberRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            /*AFMConfirmationNumberService confirmationNumberService = new AFMConfirmationNumberService();
            return new GetAFMConfirmationNumberResponse(confirmationNumberService.Execute<GetAFMConfirmationNumberServiceResponse>
                ((ServiceRequest)new GetAFMConfirmationNumberServiceRequest(this.Context) { }).ConfirmationNumber);*/
            
            SalesOrderService salesOrderService = new SalesOrderService();
           /* ChannelDataManager channelDataManager = new ChannelDataManager(this.Context);
            OrgUnit orgUnit = channelDataManager.GetStoreById(this.Context.Principal.ChannelId);*/
          // TODO: Check the second,third,fourth parameter - muthait
            var req = (ServiceRequest) new GetNextReceiptIdServiceRequest((SalesTransactionType)SalesTransactionTypeExtension.orderConfirmationSequence, 0,string.Empty,CustomerOrderMode.Return)
                {
                    //IsReturnByReceipt = true,
                    TransactionType = (SalesTransactionType)SalesTransactionTypeExtension.orderConfirmationSequence
                };

            //string confirmationNumber = ((GetNextReceiptIdServiceResponse) (salesOrderService.Execute(req))).NextReceiptId;

            string confirmationNumber = this.Context.Runtime.Execute<GetNextReceiptIdServiceResponse>(req, this.Context,true).NextReceiptId;
            return new GetAFMConfirmationNumberResponse(confirmationNumber);
        }
    }
}
