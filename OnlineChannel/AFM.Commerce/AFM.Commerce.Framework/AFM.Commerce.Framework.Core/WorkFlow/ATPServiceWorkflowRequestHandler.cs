﻿namespace AFM.Commerce.Framework.Core.Workflows
{
    using AFM.Commerce.Framework.Core.Workflows.Request;
    using AFM.Commerce.Framework.Core.Workflows.Response;
    using AFM.Commerce.Framework.Core.CRTServices;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using Microsoft.Dynamics.Commerce.Runtime.Services;

    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;

    [Export(typeof(IRequestHandler))]
    public class ATPServiceWorkflowRequestHandler : WorkflowRequestHandler<ATPRequest, ATPResponse>
    {
        /// <summary>
        /// Gets the supported request types.
        /// </summary>
        public override IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(ATPRequest);
            }
        }

        /// <summary>
        /// Executes the workflow to get the discounted prices.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override ATPResponse Process(ATPRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            var req = new GetAtpDetailsServiceRequest(){
                ItemGroupings = request.ItemGroupings
            };
            
            return new ATPResponse(this.Context.Runtime.Execute<GetATPDetailsServiceResponse>(req, this.Context,true).ItemGroupings);

        }
    }
}
