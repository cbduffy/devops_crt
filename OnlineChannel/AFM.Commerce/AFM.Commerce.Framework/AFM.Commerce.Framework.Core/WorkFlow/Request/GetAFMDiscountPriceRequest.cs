﻿namespace AFM.Commerce.Framework.Core.Workflows.Request
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
using Microsoft.Dynamics.Commerce.Runtime;
    public class GetAFMDiscountPriceRequest : Request
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GetAFMDiscountPriceServiceRequest"/> class.
        /// </summary>
        /// <param name="context">The context of the request.</param>
        public GetAFMDiscountPriceRequest()
        {
        }

        /// <summary>
        /// Gets or sets the SalesTransaction
        /// </summary>
        [DataMember]
        public SalesTransaction SalesTransactionData { get; set; }

        [DataMember]
        public ProjectionDomain Context { get; set; }

    }
}
