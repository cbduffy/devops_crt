﻿
namespace AFM.Commerce.Framework.Core.Workflows.Request
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using AFM.Commerce.Entities;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime;

    public class ATPRequest : Request
    {
        /// <summary>
        /// Gets or sets item Groupings
        /// </summary>
        [DataMember]
        public List<AFMATPItemGroupingRequest> ItemGroupings
        {
            get;
            set;
        }

    }
}
