﻿namespace AFM.Commerce.Framework.Core.Workflows.Request
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime;
    using System;
using AFM.Commerce.Entities;
    public class GetAFMTransLineRequest : Request
    {
        [DataMember]
        public string VendorId { get; set; }

        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        public string LineNumber { get; set; }
        
        [DataMember]
        public string KitItemId { get; set; }

        [DataMember]
        public long KitProductId { get; set; }

        [DataMember]
        public int KitSequenceNumber { get; set; }

        [DataMember]
        public string KitItemProductDetails { get; set; }
        [DataMember]
        public int KitItemQuantity { get; set; }


        [DataMember]
        public RequestContext Context { get; set; }


        [DataMember]
        public DateTime BeginDateRange
        {
            get;
            set;
        }

        [DataMember]
        public DateTime BestDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime EndDateRange
        {
            get;
            set;
        }
        [DataMember]
        public DateTime EarliestDeliveryDate
        {
            get;
            set;
        }

        [DataMember]
        public DateTime LatestDeliveryDate
        {
            get;
            set;
        }


        [DataMember]
        public bool IsAvailable
        {
            get;
            set;
        }

        [DataMember]
        public string Message
        {
            get;
            set;
        }
         [DataMember]
        public string LineConfirmationID
        {
            get;
            set;
        }

         [DataMember]
         public int AFMItemType
         {
             get;
             set;
         }
         // HXM To resolve 67447 Color and Size not populating
         [DataMember]
         public string Color
         {
             get;
             set;
         }

         [DataMember]
         public string Size
         {
             get;
             set;
         }

         [DataMember]
         public string ShippingMode
         {
             get;
             set;
         }

         //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
         [DataMember]
         public DateTime BeginDateRangeMOD
         {
             get;
             set;
         }

         [DataMember]
         public DateTime BestDateMOD
         {
             get;
             set;
         }

         [DataMember]
         public DateTime EndDateRangeMOD
         {
             get;
             set;
         }
         [DataMember]
         public DateTime EarliestDeliveryDateMOD
         {
             get;
             set;
         }

         [DataMember]
         public DateTime LatestDeliveryDateMOD
         {
             get;
             set;
         }

         [DataMember]
         public string MessageMOD
         {
             get;
             set;
         }
        //NE by RxL
    }
}
