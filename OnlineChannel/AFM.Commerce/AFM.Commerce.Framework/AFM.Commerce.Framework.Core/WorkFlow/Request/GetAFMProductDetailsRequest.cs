﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime;

namespace AFM.Commerce.Framework.Core.Workflows.Request
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    class GetAFMProductDetailsRequest : Request
    {
     

    
        [DataMember]
        public List<long> ProductIdsList { get; set; }


    }
}
