﻿namespace AFM.Commerce.Framework.Core.Workflows
{
    using CRTServices;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.Workflows.Request;
    using AFM.Commerce.Framework.Core.Workflows.Response;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;

    [Export(typeof(IRequestHandler))]
    class AFMDiscountWorkflowRequestHandler : WorkflowRequestHandler<GetAFMDiscountPriceRequest, GetAFMDiscountPriceResponse>
    {
        /// <summary>
        /// Gets the supported request types.
        /// </summary>
        public override IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(GetAFMDiscountPriceRequest);
            }
        }

        /// <summary>
        /// Executes the workflow to get the discounted prices.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetAFMDiscountPriceResponse Process(GetAFMDiscountPriceRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            GetAFMDiscountPriceServiceRequest serviceRequest = new GetAFMDiscountPriceServiceRequest()
            {
                SalesTransactionData = request.SalesTransactionData,
                requestContext  = this.Context
            };

            var service = this.Context.Runtime.Execute<GetAFMDiscountPriceServiceResponse>(serviceRequest, this.Context,true);

            return new GetAFMDiscountPriceResponse(service.SalesTransactionData);

        }
    }
}
