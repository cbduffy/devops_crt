﻿namespace AFM.Commerce.Framework.Core.Workflows.Response
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    public class GetAFMDiscountPriceResponse : Response
    {
        public GetAFMDiscountPriceResponse(SalesTransaction trans)
        {
            this.SalesTransactionData = trans;
        }

        /// <summary>
        /// Gets the response transaction
        /// </summary>
        public SalesTransaction SalesTransactionData { get; set; }

    }
}
