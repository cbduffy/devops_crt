﻿namespace AFM.Commerce.Framework.Core.Workflows.Response
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
  public class GetAFMTransLineResponse : Response
  {
    public bool VendorUpdateResult { get; set; }

    public GetAFMTransLineResponse(bool vendorUpdateResult)
    {
      this.VendorUpdateResult = vendorUpdateResult;
    }
  }
}