﻿
namespace AFM.Commerce.Framework.Core.Workflows.Response
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using AFM.Commerce.Entities;
    public class ATPResponse : Response
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="ATPResponse"/> class.
        /// </summary>
        /// <param name="itemGroupings">The Shipping Details.</param>
        public ATPResponse(List<AFMATPItemGroupingResponse> itemGroupings)
         {
             this.ItemGroupings = itemGroupings;
         }
        /// <summary>
        /// Gets or sets item Groupings
        /// </summary>
        public List<AFMATPItemGroupingResponse> ItemGroupings
        {
            get;
            set;
        }
    }
}
