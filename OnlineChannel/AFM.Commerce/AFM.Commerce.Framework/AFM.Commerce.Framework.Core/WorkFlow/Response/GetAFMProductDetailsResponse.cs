﻿
using AFM.Commerce.Framework.Core.Models;


namespace AFM.Commerce.Framework.Core.Workflows.Response
{
   
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    public class GetAFMProductDetailsResponse : Response
    {
        

        public GetAFMProductDetailsResponse(AFMProductDetails productDetails)
        {
            this.ProductDetails = productDetails;
        }

     
        public AFMProductDetails ProductDetails { get; set; }

    }


    }

