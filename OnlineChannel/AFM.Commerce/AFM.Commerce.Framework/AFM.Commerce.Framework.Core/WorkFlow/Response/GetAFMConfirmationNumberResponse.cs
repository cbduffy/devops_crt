﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.Workflows.Response
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    class GetAFMConfirmationNumberResponse : Response
    {

        public GetAFMConfirmationNumberResponse(string confirmationNumber)
        {
            this.ConfirmationNumber = confirmationNumber;
        }

     
        public string ConfirmationNumber { get; set; }
    }
}
