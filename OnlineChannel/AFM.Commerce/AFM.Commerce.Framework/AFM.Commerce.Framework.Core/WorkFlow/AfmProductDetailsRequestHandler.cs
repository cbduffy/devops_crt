﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Framework.Core.CRTServices;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.Workflows.Request;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using AFM.Commerce.Framework.Core.Workflows.Response;

namespace AFM.Commerce.Framework.Core.Workflows
{

    /// <summary>
    /// This class implements WorkflowRequestHandler and handles workflow request and response to pass product ids to services layer
    /// </summary>
    [Export(typeof(IRequestHandler))]
    internal class AFMProductDetailsRequestHandler : WorkflowRequestHandler<GetAFMProductDetailsRequest, GetAFMProductDetailsResponse>
    {
        public override IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(GetAFMProductDetailsRequest);
            }
        }
        /// <summary>
        /// This method handles workflow request, calls AFMProductDetailsService and returns response with custom entities
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        protected override GetAFMProductDetailsResponse Process(GetAFMProductDetailsRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            AFMProductDetailsService productDetailsService = new AFMProductDetailsService();
            var req = new AFMProductDetailsServiceRequest() { ProductIds = request.ProductIdsList};
            //return (GetAFMProductDetailsResponse) productDetailsService.Execute((Microsoft.Dynamics.Commerce.Runtime.Messages.Request) req);
            var res = this.Context.Runtime.Execute<AFMProductDetailsServiceResponse>(req, this.Context,true);
            return new GetAFMProductDetailsResponse(res.ProductDetails);
        }
        

    }
}
