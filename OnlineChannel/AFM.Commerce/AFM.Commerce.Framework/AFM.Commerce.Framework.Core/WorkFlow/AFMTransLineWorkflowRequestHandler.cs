﻿using AFM.Commerce.Framework.Core.CRTServices;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.Workflows.Request;
using AFM.Commerce.Framework.Core.Workflows.Response;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace AFM.Commerce.Framework.Core.Workflows
{
    [Export(typeof(IRequestHandler))]
    internal class AFMTransLineWorkflowRequestHandler : WorkflowRequestHandler<GetAFMTransLineRequest, GetAFMTransLineResponse>
    {
        public override IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(GetAFMTransLineRequest);
            }
        }

        protected  override GetAFMTransLineResponse Process(GetAFMTransLineRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");
            var req = new SetVendorIdServiceRequest()
            {
                VendorId = request.VendorId,
                LineNumber = request.LineNumber,
                OrderNumber = request.OrderNumber,
                KitItemId = request.KitItemId,
                KitSequenceNumber = request.KitSequenceNumber,
                KitProductId = request.KitProductId,
                KitItemProductDetails = request.KitItemProductDetails,
                KitItemQuantity = request.KitItemQuantity,
                BeginDateRange = request.BeginDateRange,
                BestDate = request.BestDate,
                EndDateRange = request.EndDateRange,
                EarliestDeliveryDate = request.EarliestDeliveryDate,
                LatestDeliveryDate = request.LatestDeliveryDate,
                IsAvailable = request.IsAvailable,
                Message = request.Message,
                LineConfirmationID = request.LineConfirmationID,
                AFMItemType = request.AFMItemType,
                // To resolve 67447 Color and Size not populating
                Color = request.Color,
                Size = request.Size,
                ShippingMode=request.ShippingMode,

                //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
                BeginDateRangeMOD = request.BeginDateRangeMOD,
                BestDateMOD = request.BestDateMOD,
                EndDateRangeMOD = request.EndDateRangeMOD,
                EarliestDeliveryDateMOD = request.EarliestDeliveryDateMOD,
                LatestDeliveryDateMOD = request.LatestDeliveryDateMOD,
                MessageMOD = request.MessageMOD
                //NE by RxL
            };

            var response = this.Context.Runtime.Execute<SetVendorIdServiceResponse>(req, this.Context,true);
            return new GetAFMTransLineResponse(response.Result);
        }
    }
}