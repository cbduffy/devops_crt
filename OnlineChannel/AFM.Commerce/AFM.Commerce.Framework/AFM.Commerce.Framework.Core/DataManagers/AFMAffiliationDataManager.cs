﻿
namespace AFM.Commerce.Framework.Core.DataManagers
{
    using System.Collections.ObjectModel;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Framework.Core.DataTypes;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// 
    /// </summary>
    public class AFMAffiliationDataManager : DatabaseAccessor
    {
        private const string GetAffiliationIdByZipCodeSprocName = "GETAFFILIATIONIDBYZIPCODE";
        private const string GetAffiliationIdByProductCodeSprocName = "GETAFFILIATIONSBYPRODUCTCODE";
        private const string GetDefaultAffiliationIdForChannelSprocName = "GETDEFAULTAFFILIATIONIDBYCHANNEL";
        private const string GetAffiliationIdByProductIdsSprocName = "GETAFFILIATIONSBYPRODUCTIDES";
        private const string GetAllAffiliationsSprocName = "AFMGETALLAFFILIATIONS";

        public AFMAffiliationDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }

        /// <summary>
        /// Make a DB call using the parameters specified and the Stored Proc given 
        /// </summary>
        /// <param name="zipCode"></param>
        /// <param name="channelId"></param>
        /// <returns></returns>
        public AfmVendorAffiliation GetAffiliationByZipCode(string zipCode, long channelId)
        {
            ParameterSet parameters = new ParameterSet();
            parameters["@ChannelId"] = channelId;
            parameters["@ZipCode"] = zipCode;

            var result = base.ExecuteStoredProcedure<AfmVendorAffiliation>(GetAffiliationIdByZipCodeSprocName, parameters);
            return result.Count > 0 ? result[0] : null;
        }

        public ReadOnlyCollection<AfmVendorAffiliation> GetAffiliationsByProduct(string productId, long channelId)
        {
            ParameterSet parameters = new ParameterSet();
            parameters["@ProductCode"] = productId;
            parameters["@ChannelId"] = channelId;
            ReadOnlyCollection<AfmVendorAffiliation> result = null;
            result = base.ExecuteStoredProcedure<AfmVendorAffiliation>(GetAffiliationIdByProductCodeSprocName, parameters);

            return result != null && result.Count > 0 ? result : null;
        }

        public ReadOnlyCollection<AfmVendorAffiliation> GetAffiliationsByProductIds(List<string> productId, long channelId)
        {
            ParameterSet parameters = new ParameterSet();
            System.Data.DataTable dt = new System.Data.DataTable("ITEMIDSEARCHTABLETYPE");
            dt.Columns.Add("ITEMID", typeof(string));
            dt.Columns.Add("INVENTDIMID", typeof(string));
            foreach(var tempitemid in productId)
            {
                dt.Rows.Add(tempitemid, "");
                //itemids.Add(new ProductLookupClause() { ItemId = tempitemid });
            }            
            //ItemIdSearchTableType type = new ItemIdSearchTableType(itemids.AsEnumerable());
            parameters["@ProductCode"] = dt;
            parameters["@ChannelId"] = channelId;
            ReadOnlyCollection<AfmVendorAffiliation> result = null;            
            result = base.ExecuteStoredProcedure<AfmVendorAffiliation>(GetAffiliationIdByProductIdsSprocName, parameters);

            return result != null && result.Count > 0 ? result : null;
        }


        public AfmVendorAffiliation GetDefaultAffiliationIdForChannel(long channelId)
        {
            ParameterSet parameters = new ParameterSet();
            parameters["@ChannelId"] = channelId;

            var result = base.ExecuteStoredProcedure<AfmVendorAffiliation>(GetDefaultAffiliationIdForChannelSprocName, parameters);

            return result.Count > 0 ? result[0] : null;
        }


        public ReadOnlyCollection<AfmVendorAffiliation> GetAllAffiliationsForChannel(long channelId)
        {
            ParameterSet parameters = new ParameterSet();
            parameters["@ChannelId"] = channelId;
            ReadOnlyCollection<AfmVendorAffiliation> result = null;  
            result = base.ExecuteStoredProcedure<AfmVendorAffiliation>(GetAllAffiliationsSprocName, parameters);

            return result != null && result.Count > 0 ? result : null;
        }
    }
}
