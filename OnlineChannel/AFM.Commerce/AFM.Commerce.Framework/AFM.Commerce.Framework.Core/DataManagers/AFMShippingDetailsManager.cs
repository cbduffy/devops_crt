﻿/*NS Developed by ysrini for AFM_TFS_40686 dated 06/29/2014*/

namespace AFM.Commerce.Framework.Core.DataManagers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Entities;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data.Types;
    using AFM.Commerce.Runtime.Services.DataTypes;
    public class AFMShippingDetailsManager : DatabaseAccessor
    {
        private const string GetDeliveryChargesSprocName = "AFMGetDeliveryCharges";

        public AFMShippingDetailsManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }

        public List<AFMShippingDetails> GetShippingDetailsForCartItems(List<AFMShippingDetails> cartItems)
        {
            if (cartItems == null)
                return null;


            List<AFMShippingDetails> lintItemsList = new List<AFMShippingDetails>();
            DataSet cartItemShippingDetails;
            using( AFMDeliveryServiceLineTableType tabletype = new AFMDeliveryServiceLineTableType(cartItems.AsEnumerable()))
            {
                ParameterSet parameters = new ParameterSet();
                parameters["@LineItems"] = tabletype.DataTable; 
                parameters["@OnlineChannelRecId"] = this.ChannelId;
                cartItemShippingDetails = this.ExecuteStoredProcedureDataSet(GetDeliveryChargesSprocName, parameters, new QueryResultSettings());
            }            
            foreach (DataRow row in cartItemShippingDetails.Tables[0].Rows)
            {
                AFMShippingDetails lineItem = new AFMShippingDetails();
                lineItem.LineId = row["LineId"] == null ? string.Empty : row["LineId"].ToString();
                //lineItem.ProductDetails = row["ProductDetails"] == null ? string.Empty : row["ProductDetails"].ToString();
                //lineItem.Name = row["Name"] == null ? string.Empty : row["Name"].ToString();
                lineItem.ProductId = DBNull.Value == row["ProductId"] ? 0 : Convert.ToInt64(row["ProductId"]);
                lineItem.ItemId = row["ItemId"] == null ? string.Empty : row["ItemId"].ToString();
                lineItem.Quantity = row["Quantity"] == null ? string.Empty : row["Quantity"].ToString();
                lineItem.ZipCode = row["ZipCode"] == null ? string.Empty : row["ZipCode"].ToString();
                lineItem.TotalValue = DBNull.Value == row["TotalValue"] ? 0 : Convert.ToDecimal(row["TotalValue"]);
                //lineItem.AFMVendorId = row["AFMVendorId"] == null ? string.Empty : row["AFMVendorId"].ToString();
                //lineItem.AFMVendorName = row["AFMVendorName"] == null ? string.Empty : row["AFMVendorName"].ToString();
                lineItem.ShippingMode = row["AFMShippingMode"] == null ? string.Empty : row["AFMShippingMode"].ToString();
                lineItem.ShippingCost = DBNull.Value == row["AFMShippingCost"] ? 0 : Convert.ToDecimal(row["AFMShippingCost"]);
                lineItem.ChargeName = row["ChargeName"] == null ? string.Empty : row["ChargeName"].ToString();
                lineItem.ChargeType = row["ChargeType"] == null ? 0 : Convert.ToInt32(row["ChargeType"]);
                lineItem.DeliveryMethod = row["DeliveryMethod"] == null ? 0 : Convert.ToInt32(row["DeliveryMethod"]);
                lineItem.IsServiceItem = Convert.ToBoolean(row["IsServiceItem"]);
                lineItem.DeliveryMode = DBNull.Value == row["DeliveryMode"] ? string.Empty : row["DeliveryMode"].ToString();
                lineItem.DirectShip = DBNull.Value == row["DirectShip"] ? 0 : Convert.ToInt32(row["DirectShip"]);
                lineItem.SupplierDirectShip = DBNull.Value == row["SupplierDirectShip"] ? 0 : Convert.ToInt32(row["SupplierDirectShip"]);
                lintItemsList.Add(lineItem);
            }
            return lintItemsList;



        }


    }
}
