﻿using AFM.Commerce.Framework.Core.Models;
using AFM.Commerce.Runtime.Services.DataTypes;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Data;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AFM.Commerce.Framework.Core.DataManagers
{
    /// <summary>
    /// This is a data manager class which has logic to call the stored procedure and get result sets from channel database
    /// </summary>
    public class AFMProductDetailsDataManager : DatabaseAccessor
    {
        private const string AFMGetProductDetailsSprocName = "AFMGETPRODUCTDETAILS";

        public AFMProductDetailsDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }
        /// <summary>
        /// This function calls the AFMGETPRODUCTDETAILS procedure in database to retrieve additional product entities
        /// </summary>
        /// <param name="productIds">The product record Ids for which custom entities needs to be retrieved</param>
        /// <param name="dateTime"></param>
        /// <returns>Datasets Encapsulated in AFMProductDetails model</returns>
        public AFMProductDetails SetProductDetails(List<long> productIds, DateTime dateTime)
        {
            ParameterSet parameters = new ParameterSet();

            if (productIds != null)
            {
                Tuple<ReadOnlyCollection<AFMItemCovers>, ReadOnlyCollection<AFMProductCountryMap>, ReadOnlyCollection<AFMUpCrossSell>, ReadOnlyCollection<AFMAlsoKnownAs>, ReadOnlyCollection<AFMECommOwnedData>, ReadOnlyCollection<AFMAdditionalDimensions>, ReadOnlyCollection<AFMOrderQuantity>, Tuple<ReadOnlyCollection<AFMNapData>>> dataSets;
                using (RecordIdTableType recordIdTableType = new RecordIdTableType(productIds))
                {
                    parameters["@tvp_ProductIds"] = recordIdTableType.DataTable;
                    parameters["@dt_ChannelDate"] = dateTime;
                    dataSets = this.ExecuteStoredProcedure<AFMItemCovers, AFMProductCountryMap, AFMUpCrossSell, AFMAlsoKnownAs, AFMECommOwnedData, AFMAdditionalDimensions, AFMOrderQuantity, AFMNapData>(AFMGetProductDetailsSprocName, parameters);
                    AFMProductDetails prdtDetails = new AFMProductDetails();
                    prdtDetails.ItemCovers = dataSets.Item1;
                    prdtDetails.ProductCountryMap = dataSets.Item2;
                    prdtDetails.UpCrossSell = dataSets.Item3;
                    prdtDetails.AlsoKnownAs = dataSets.Item4;
                    prdtDetails.ECommOwnedData = dataSets.Item5;
                    prdtDetails.AdditionalDimensions = dataSets.Item6;
                    prdtDetails.OrderQuantity = dataSets.Item7;
                    prdtDetails.NapData = dataSets.Rest.Item1;
                    return prdtDetails;
                }
            }
            return null;
        }

        public ReadOnlyCollection<Microsoft.Dynamics.Commerce.Runtime.DataModel.LinkedProduct> GetLinkedProducts(IEnumerable<long> productIds)
        {
            throw new NotImplementedException();
        }

        public ReadOnlyCollection<Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductCatalogAssociation> GetProductCatalogAssociations(IEnumerable<long> productIds)
        {
            throw new NotImplementedException();
        }

        public ReadOnlyCollection<Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductCatalog> GetProductCatalogs(Microsoft.Dynamics.Commerce.Runtime.DataModel.CatalogSearchCriteria queryCriteria, QueryResultSettings querySettings)
        {
            throw new NotImplementedException();
        }

        public void SaveProductData(System.Xml.Linq.XDocument productsXml)
        {
            throw new NotImplementedException();
        }

        public Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductSearchResult SearchProducts(Microsoft.Dynamics.Commerce.Runtime.DataModel.ProductSearchCriteria queryCriteria, QueryResultSettings querySettings)
        {
            throw new NotImplementedException();
        }
    }
}

