﻿/*
 * NS Developed by muthait for AFM_TFS_40686 dated 06/25/2014
 */

namespace AFM.Commerce.Framework.Core.DataManagers
{
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using System.Collections.Generic;
    using System.Linq;
    using Models;
    using System.Configuration;
    using AFM.Commerce.Entities;
    using AFM.Commerce.Runtime.Services.DataTypes;

    /// <summary>
    /// Encapsulates vendor details information.
    /// </summary>
    public class AFMVendorDetailsDataManager : DatabaseAccessor
    {
        private const string GetVendorDetailsbyZipCodeSprocName = "GetVendorDetailsbyZipCode";
        private const string GetAfiVendorIdSprocName = "GETAFIVendorId";
        private const string IsEcommOwnedSprocName = "IsEcommOwned";
        private const string GetProductEcomSprocName = "AFMGETPRODUCTECOMSTATUSBYIDS";

        /// <summary>
        /// Initializes a new instance of the <see cref="AFMVendorDetailsDataManager"/> class.
        /// </summary>
        /// <param name="context">Context of this request.</param>
        public AFMVendorDetailsDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }

        public List<AFMVendorDetails> GetVendorDetailsbyZipCode(string zipCode, string itemId)
        {
            if (!string.IsNullOrEmpty(zipCode))
            {
                ParameterSet parameters = new ParameterSet();
                parameters["@ZipCode"] = zipCode;
                ReadOnlyCollection<AFMVendorDetails> vendorInfo = this.ExecuteStoredProcedure<AFMVendorDetails>(GetVendorDetailsbyZipCodeSprocName, parameters);

                return vendorInfo.ToList();
            }
            return null;
        }

        public List<AFMVendorDetails> GetVendorDetailsforEComOwnedItem()
        {
            ParameterSet parameters = new ParameterSet();
            parameters["@OnlineChannelRecId"] = ChannelId;

            ReadOnlyCollection<AFMVendorDetails> vendorInfo = this.ExecuteStoredProcedure<AFMVendorDetails>(GetAfiVendorIdSprocName, parameters);

            return vendorInfo.ToList();
        }

        public AFMItemFlags GetEcommOwnedStatus(string itemId)
        {
            if (!string.IsNullOrEmpty(itemId))
            {
                ParameterSet parameters = new ParameterSet();
                parameters["@OnlineChannelRecId"] = ChannelId;
                parameters["@ItemId"] = itemId;
                ReadOnlyCollection<AFMItemFlags> itemFlags = this.ExecuteStoredProcedure<AFMItemFlags>(IsEcommOwnedSprocName, parameters);

                return itemFlags[0];
            }
            else
            {
                return null;
            }
        }

        public List<AFMItemFlags> GetEomOwnedItemDetail(List<long> productId)
        {
            ReadOnlyCollection<AFMItemFlags> vendorInfo;            
            using(RecordIdTableType type=new RecordIdTableType(productId.AsEnumerable()))
            {
                ParameterSet parameters = new ParameterSet();
                parameters["@OnlineChannelRecId"] = ChannelId;
                parameters["@ProductCode"] = type.DataTable;
                vendorInfo = this.ExecuteStoredProcedure<AFMItemFlags>(GetProductEcomSprocName, parameters);
            }
            return vendorInfo.ToList();
        }
    }
}
