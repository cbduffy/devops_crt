﻿/*NS Developed by muthait dated 22Oct2014*/

namespace AFM.Commerce.Framework.Core.DataManagers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using AFM.Commerce.Framework.Core.Models;

    public class AFMCartDataManager : DatabaseAccessor
    {

        private const string UpdateSalesTransactionSprocName = "UpdateSalesTransactionData";
        private const string CloneSalesTransactionSprocName = "AFMCLONESALESTRANSACTIONDATA";
        private const string GetSynchronyOptionSprocName = "AFMGetSynchronyOnlineOptions";

        /// <summary>
        /// Initializes a new instance of the <see cref="AFMVendorDetailsDataManager"/> class.
        /// </summary>
        /// <param name="context">Context of this request.</param>
        public AFMCartDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }

        /// <summary>
        /// The update Sales Transaction table in CRT
        /// </summary>
        /// <param name="isDelete">
        /// is soft Delete 
        /// </param>
        /// <param name="cartId">
        /// cart id
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool UpdateSalesTransaction(bool isDelete, string cartId)
        {
            ParameterSet parameters = new ParameterSet();
            parameters["@IsDelete"] = Convert.ToInt16(isDelete);
            parameters["@CartId"] = cartId;
            this.ExecuteStoredProcedureNonQuery(UpdateSalesTransactionSprocName, parameters);

            return true;
        }

        /// <summary>
        /// The clone Sales Transaction to a new commence cart
        /// </summary>
        /// <param name="newCartId">
        /// new commence Cart Id to be created
        /// </param>
        /// <param name="sourceCartId">
        /// existing shopping Cart Id to be created
        /// </param>
        /// <returns>
        /// The Created the Cart Id <see cref="string"/>.
        /// </returns>
        public string CloneSalesTransaction(string newCartId, string sourceCartId)
        {
            ParameterSet parameters = new ParameterSet();
            parameters["@CartId"] = newCartId;
            parameters["@SourceCartId"] = sourceCartId;
            var response = this.ExecuteStoredProcedureNonQuery(CloneSalesTransactionSprocName, parameters);
            if (response == 1)
                return newCartId;
            else
                return string.Empty;
        }


        //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
        /// <summary>
        /// This method returns the RSA information of guven rsa id
        /// If Rsa ID is empty then it fetches all Valid RSA IDs from channel database 
        /// </summary>
        /// <param name="context"></param>
        /// <returns>list of AFMRsaId</returns>
        public List<AFMRsaId> GetRsaIds(string rsaId)
        {
            SqlQuery query = new SqlQuery();

            if (string.IsNullOrEmpty(rsaId))
            {
                query.QueryString = "SELECT [RECID], [RSAID] FROM [ax].[AFMRSATABLE]";
            }
            else
            {
                query.QueryString = "SELECT [RECID], [RSAID] FROM [ax].[AFMRSATABLE] WHERE RSAID = @rsaId";                
                query.Parameters.Add( "@rsaId", rsaId);
            }
            var result = this.ExecuteSelect<AFMRsaId>(query);
            return result == null ? null : result.ToList();
        }
        //NE - RxL
        //NS by spriya : DMND0010113 - Synchrony Financing online
        //This method gets the synchrony options from channel database
        public List<AFM.Commerce.Framework.Core.Models.AFMSynchronyOptions> GetSynchronyOptions()
        {
            ParameterSet parameters = new ParameterSet();

            ReadOnlyCollection<AFMSynchronyOptions> SynchronyInfo = this.ExecuteStoredProcedure<AFMSynchronyOptions>(GetSynchronyOptionSprocName, parameters);

            return SynchronyInfo == null ? null : SynchronyInfo.ToList();
        }
        //NE by spriya : DMND0010113 - Synchrony Financing online
    }
}
