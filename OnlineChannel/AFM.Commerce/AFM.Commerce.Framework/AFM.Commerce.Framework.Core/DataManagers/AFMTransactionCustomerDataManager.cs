﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Data;
using AFM.Commerce.Entities;

namespace AFM.Commerce.Framework.Core.DataManagers
{
    public class AFMTransactionCustomerDataManager : DatabaseAccessor
    {
        private const string UpdateTransactionCustomer = "AFMUpdateTransactionCustomer";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public AFMTransactionCustomerDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }


        public bool UpdateAFMTransactionCustomerID(string cctTransactionID, string sctTransactionID, string customerId)
        {
            if (!string.IsNullOrEmpty(cctTransactionID) && !string.IsNullOrEmpty(sctTransactionID) && !string.IsNullOrEmpty(customerId))
            {
                ParameterSet parameters = new ParameterSet();
                parameters["@cctTransactionID"] = cctTransactionID;
                parameters["@sctTransactionID"] = sctTransactionID;
                parameters["@customerId"] = customerId;

                this.ExecuteStoredProcedureNonQuery(UpdateTransactionCustomer, parameters);

                return true;
            }
            return false;
        }
    }
}
