﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/29/2014*/

namespace AFM.Commerce.Framework.Core.DataManagers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using AFM.Commerce.Entities;

    public class AFMTransHeaderDataManager : DatabaseAccessor
    {

        private const string UpdateTransHeaderSprocName = "AFMUpdateSalesTransHeader";

        /// <summary>
        /// Initializes a new instance of the <see cref="AFMVendorDetailsDataManager"/> class.
        /// </summary>
        /// <param name="context">Context of this request.</param>
        public AFMTransHeaderDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }


        public bool UpdateSalesTransHeader(AFMSalesTransHeader afmSalesTransHeader)
        {
            if (!string.IsNullOrEmpty(afmSalesTransHeader.OrderNumber))
            {
                ParameterSet parameters = new ParameterSet();
                parameters["@IsOkToText"] = Convert.ToInt16(afmSalesTransHeader.IsOkToText);
                parameters["@OkToTextUniqueId"] = afmSalesTransHeader.OkToTextUniqueId;
                parameters["@OkToTextVersionId"] = afmSalesTransHeader.OkToTextVersionId;

                parameters["@IsAcceptTermsAndConditions"] = Convert.ToInt16(afmSalesTransHeader.IsAcceptTermsAndConditions);
                parameters["@TermsAndConditionsUniqueId"] = afmSalesTransHeader.TermsAndConditionsUniqueId;
                parameters["@TermsAndConditionsVersionId"] = afmSalesTransHeader.TermsAndConditionsVersionId;

                parameters["@ChannelReferenceId"] = afmSalesTransHeader.OrderNumber;

                //N - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
                //update RecId of RSA ID associated with cart
                parameters["@RsaRecId"] = afmSalesTransHeader.RsaRecId;
                //NS by spriya : DMND0010113 - Synchrony Financing online
                //update synchrony options in sale order header
                parameters["@afmFinancingOptionId"] = afmSalesTransHeader.FinancingOptionId;
                parameters["@afmSynchronyTermsAndConditions"] = afmSalesTransHeader.SynchronyTnC;
                parameters["@afmIsMultiAuthAllowed"] = afmSalesTransHeader.IsMultiAuthAllowed;
                parameters["@afmPromoDesc1"] = afmSalesTransHeader.PromoDesc1;
                parameters["@afmPromoDesc2"] = afmSalesTransHeader.PromoDesc2;
                parameters["@afmPromoDesc3"] = afmSalesTransHeader.PromoDesc3;
                parameters["@afmRemoveCartDiscounts"] = afmSalesTransHeader.RemoveCartDiscount;
                parameters["@afmMinimumPurchaseAmount"] = afmSalesTransHeader.MinimumPurchaseAmount;
                parameters["@afmPaymMode"] = afmSalesTransHeader.PaymMode;
                parameters["@afmPaymTermId"] = afmSalesTransHeader.PaymTermId;
                parameters["@afmLearnMore"] = afmSalesTransHeader.LearnMore;
                parameters["@afmCardType"] = afmSalesTransHeader.CardType;
                parameters["@afmPaymentFee"] = afmSalesTransHeader.PaymentFee;
                parameters["@afmThirdPartyTermCode"] = afmSalesTransHeader.ThirdPartyTermCode;
                //NE by spriya : DMND0010113 - Synchrony Financing online
                this.ExecuteStoredProcedureNonQuery(UpdateTransHeaderSprocName, parameters);

                return true;
            }
            return false;
        }

    }
}
