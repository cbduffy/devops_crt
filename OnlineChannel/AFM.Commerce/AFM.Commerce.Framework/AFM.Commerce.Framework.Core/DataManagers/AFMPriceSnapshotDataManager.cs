﻿using AFM.Commerce.Framework.Core.Models;
using AFM.Commerce.Runtime.Services.DataTypes;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Data;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using SystemDataTable = System.Data.DataTable;
using SystemDataRow = System.Data.DataRow;

namespace AFM.Commerce.Framework.Core.DataManagers
{
    public class AFMPriceSnapshotDataManager
    {
        private const string GetPriceSnapshotprocName = "AFMGETITEMSPRICESNAPSHOT";

        public AFMPriceSnapshotDataManager()
        {
            
        }

        /// <summary>
        /// This datamanager calls the database method to get prices for given affilaition for list of itemIds
        /// </summary>
        /// <param name="ItemIds"></param>
        /// <param name="affiliationId"></param>
        /// <returns></returns>
        public List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> ItemIds,long affiliationId)
        {
            SystemDataTable dt = new SystemDataTable();
            SystemDataTable priceSnapshotDataTable = new SystemDataTable();
            List<AFMItemPriceSnapshot> priceSnapshotList = new List<AFMItemPriceSnapshot>();
            dt.Columns.Add("ItemId", typeof(string));
            SystemDataRow dr;
            foreach (var item in ItemIds)
            {
                dr = dt.NewRow();
                dr["ItemId"] = item;
                dt.Rows.Add(dr);
            }
            using (SqlConnection oConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["PriceCacheConnectionString"].ToString()))
            {
                oConnection.Open();
                using (SqlCommand oCommand = new SqlCommand(GetPriceSnapshotprocName, oConnection))
                {
                    SqlParameter sqlParameter = oCommand.Parameters.AddWithValue("@PriceSnapshotItems", dt);
                    sqlParameter.SqlDbType = SqlDbType.Structured;
                    sqlParameter.TypeName = "dbo.PriceSnapshotItemsTableType";
                    oCommand.CommandType = CommandType.StoredProcedure;
                   // oCommand.ExecuteNonQuery();
                    oCommand.Parameters.AddWithValue("@AffiliationId", affiliationId);
                    using (var da = new SqlDataAdapter(oCommand))
                    {
                        da.Fill(priceSnapshotDataTable);
                    }
                }
            }
            AFMItemPriceSnapshot itemPriceSnapshot;
            foreach (DataRow drow in priceSnapshotDataTable.Rows)
            {
                itemPriceSnapshot = new AFMItemPriceSnapshot();
                itemPriceSnapshot.ItemId = drow["ItemId"].ToString();
                itemPriceSnapshot.AffiliationId = (long)(drow["AffiliationId"]);
                itemPriceSnapshot.BasePrice = (decimal)(drow["BasePrice"]);
                itemPriceSnapshot.BestPrice = (decimal)(drow["BestPrice"]);
                itemPriceSnapshot.LastModified = (DateTime)(drow["LastModified"]);
                priceSnapshotList.Add(itemPriceSnapshot);
            }

            return priceSnapshotList;
        }

        /// <summary>
        /// Datamanager call to get prices for all affilaitions when only itemIDs passed in parameter
        /// </summary>
        /// <param name="ItemIds"></param>
        /// <returns></returns>
        public List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> ItemIds)
        {
            SystemDataTable dt = new SystemDataTable();
            SystemDataTable priceSnapshotDataTable = new SystemDataTable();
            List<AFMItemPriceSnapshot> priceSnapshotList = new List<AFMItemPriceSnapshot>();
            dt.Columns.Add("ItemId", typeof(string));
            SystemDataRow dr;
            foreach (var item in ItemIds)
            {
                dr = dt.NewRow();
                dr["ItemId"] = item;
                dt.Rows.Add(dr);
            }
            using (SqlConnection oConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["PriceCacheConnectionString"].ToString()))
            {
                oConnection.Open();
                using (SqlCommand oCommand = new SqlCommand(GetPriceSnapshotprocName, oConnection))
                {
                    SqlParameter sqlParameter = oCommand.Parameters.AddWithValue("@PriceSnapshotItems", dt);
                    sqlParameter.SqlDbType = SqlDbType.Structured;
                    sqlParameter.TypeName = "dbo.PriceSnapshotItemsTableType";
                    oCommand.CommandType = CommandType.StoredProcedure;
                    oCommand.Parameters.AddWithValue("@AffiliationId", -1);
                    using (var da = new SqlDataAdapter(oCommand))
                    {
                        da.Fill(priceSnapshotDataTable);
                    }
                }
            }
            AFMItemPriceSnapshot itemPriceSnapshot;
            foreach (DataRow drow in priceSnapshotDataTable.Rows)
            {
                itemPriceSnapshot = new AFMItemPriceSnapshot();
                itemPriceSnapshot.ItemId = drow["ItemId"].ToString();
                itemPriceSnapshot.AffiliationId = (long)(drow["AffiliationId"]);
                itemPriceSnapshot.BasePrice = (decimal)(drow["BasePrice"]);
                itemPriceSnapshot.BestPrice = (decimal)(drow["BestPrice"]);
                itemPriceSnapshot.LastModified = (DateTime)(drow["LastModified"]);
                priceSnapshotList.Add(itemPriceSnapshot);
            }

            return priceSnapshotList;
        }
    }
}
