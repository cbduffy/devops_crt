﻿
namespace AFM.Commerce.Framework.Core.DataManagers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Entities;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data.Types;

    public class AFMAddressDataManager : DatabaseAccessor
    {
        private const string GetAddressStateSprocName = "GETSTATEPROVINCES";
        private const string GetAddressZipcodeSprocName = "GetFromZipCode";
        private const string GetAddressValidationDataSprocName = "AFMGetAddressValidationData";

        public AFMAddressDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }

        public List<AFMAddressState> GetAddressState(string CountryRegionId)
        {
            if (string.IsNullOrEmpty(CountryRegionId))
            {
                throw new ArgumentNullException(CountryRegionId);
            }
            ParameterSet parameters = new ParameterSet();
            parameters["@CountryRegionId"] = CountryRegionId;
            var addressList = new List<AFMAddressState>();
            var addressListDataSet = this.ExecuteStoredProcedureDataSet(GetAddressStateSprocName, parameters, null);
            foreach (DataRow row in addressListDataSet.Tables[0].Rows)
            {
                AFMAddressState objaddstate = new AFMAddressState();
                objaddstate.CountryRegionId = row["COUNTRYREGIONID"].ToString();
                objaddstate.StateId = row["STATEID"].ToString();
                objaddstate.StateName = row["STATENAME"].ToString();
                addressList.Add(objaddstate);
            }
            return addressList;
        }

        public bool ValidateZipCode(string Zipcode, string CountryRegionId)
        {
            if (string.IsNullOrEmpty(Zipcode))
            {
                throw new ArgumentNullException(Zipcode);
            }

            if (string.IsNullOrEmpty(CountryRegionId))
            {
                throw new ArgumentNullException(CountryRegionId);
            }
            ParameterSet parameters = new ParameterSet();
            parameters["@Zipcode"] = Zipcode;
            parameters["@CountryRegionId"] = CountryRegionId;  
            var addressZipcodeListDataSet = this.ExecuteStoredProcedureDataSet(GetAddressZipcodeSprocName, parameters, null);
            if (addressZipcodeListDataSet.Tables[0].Rows.Count != 0)
                return true;
            else
                return false;
        }

        //CR 306 - Address validation off - CS Developed by spriya Dated 09/14/2015
        /// <summary>
        /// This method executes the stored procedure to get the validation flags and zipcode master validated data
        /// </summary>
        /// <param name="zipCodeMasterData"></param>
        /// <returns></returns>
        public AFMAddressValidationData GetAddressValidationData(AFMZipCodeMasterData zipCodeMasterData)
        {
            ParameterSet parameters = new ParameterSet();
            parameters["@City"] = zipCodeMasterData.City;
            parameters["@State"] = zipCodeMasterData.State;
            parameters["@ZipCode"] = zipCodeMasterData.ZipCode;
            parameters["@CountryRegionId"] = zipCodeMasterData.CountryRegionId;
            parameters["@ValidationAddressType"] = (int)zipCodeMasterData.ValidationAddressType;

            ReadOnlyCollection<AFMAddressValidationData> addressValidationData = this.ExecuteStoredProcedure<AFMAddressValidationData>(GetAddressValidationDataSprocName, parameters);

            if (addressValidationData != null && addressValidationData.Count() > 0)
                return addressValidationData.ToList<AFMAddressValidationData>()[0];
            else
                return new AFMAddressValidationData();
        }

        //CR 306 - Address validation off - CE Developed by spriya Dated 09/14/2015
    }
}
