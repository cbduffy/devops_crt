﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/29/2014*/

namespace AFM.Commerce.Framework.Core.DataManagers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using AFM.Commerce.Entities;

    public class AFMTransLineDataManager : DatabaseAccessor
    {

        private const string UpdateEvenDorInfoToTransLinesSprocName = "UpdateAFMSalesTransLines";

        /// <summary>
        /// Initializes a new instance of the <see cref="AFMVendorDetailsDataManager"/> class.
        /// </summary>
        /// <param name="context">Context of this request.</param>
        public AFMTransLineDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }

        /// <summary>
        /// The update afm sales trans lines.
        /// </summary>
        /// <param name="afmSalesTransItems">
        /// The afm sales trans items.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool UpdateAFMSalesTransLines(AFMSalesTransItems afmSalesTransItems)
        {
            ParameterSet parameters = new ParameterSet();
            parameters["@AFMVENDACCOUNT"] = afmSalesTransItems.VendorId;
            parameters["@AFMITEMTYPE"] = afmSalesTransItems.AFMItemType;
            parameters["@ChannelReferenceId"] = afmSalesTransItems.OrderNumber;
            parameters["@LineNumber"] = afmSalesTransItems.LineNumber;
            parameters["@KitItemId"] = afmSalesTransItems.KitItemId;
            parameters["@KitProductId"] = afmSalesTransItems.KitProductId;
            parameters["@KitSequenceNumber"] = afmSalesTransItems.KitSequenceNumber;
            parameters["@KitItemProductDetails"] = afmSalesTransItems.KitItemProductDetails;
            parameters["@KitItemQuantity"] = afmSalesTransItems.KitItemQuantity;

            parameters["@BeginDateRange"] = afmSalesTransItems.BeginDateRange;
            parameters["@BestDate"] = afmSalesTransItems.BestDate;
            parameters["@EndDateRange"] = afmSalesTransItems.EndDateRange;
            parameters["@IsAvailable"] = Convert.ToInt32(afmSalesTransItems.IsAvailable);
            parameters["@Message"] = string.IsNullOrEmpty(afmSalesTransItems.Message) ? string.Empty : afmSalesTransItems.Message;

            parameters["@LineConfirmationID"] = afmSalesTransItems.LineConfirmationID;
            // HXM To resolve 67447 Color and Size not populating
            parameters["@Color"] = afmSalesTransItems.Color;
            parameters["@Size"] = afmSalesTransItems.Size;
            parameters["@ShippingMode"] = afmSalesTransItems.ShippingMode;
            parameters["@EarliestDeliveryDate"] = afmSalesTransItems.EarliestDeliveryDate;
            parameters["@LatestDeliveryDate"] = afmSalesTransItems.LatestDeliveryDate;

            //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
            parameters["@BeginDateRangeMOD"] = afmSalesTransItems.BeginDateRangeMOD;
            parameters["@BestDateMOD"] = afmSalesTransItems.BestDateMOD;
            parameters["@EndDateRangeMOD"] = afmSalesTransItems.EndDateRangeMOD;
            parameters["@MessageMOD"] = string.IsNullOrEmpty(afmSalesTransItems.MessageMOD) ? string.Empty : afmSalesTransItems.MessageMOD;
            parameters["@EarliestDeliveryDateMOD"] = afmSalesTransItems.EarliestDeliveryDateMOD;
            parameters["@LatestDeliveryDateMOD"] = afmSalesTransItems.LatestDeliveryDateMOD;
            //NS by RxL

            this.ExecuteStoredProcedureNonQuery(UpdateEvenDorInfoToTransLinesSprocName, parameters);

            return true;
        }

    }
}
