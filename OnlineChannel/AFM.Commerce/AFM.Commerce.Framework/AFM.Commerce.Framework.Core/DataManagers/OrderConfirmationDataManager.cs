﻿namespace AFM.Commerce.Runtime.Services.DataManagers
{
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using System.Collections.Generic;
    using System.Linq;
    using Models;
    using System.Configuration;

    public class AFMOrderConfirmationDataManager : DatabaseAccessor
    {
        private const string GETCONFIRMATIONNUMBERMASKSprocName = "AFMGETCONFIRMATIONNUMBERMASK";

        public AFMOrderConfirmationDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }
        public List<AFMConfirmationNumberMaskData> SetConfirmationNumberMask(RequestContext context)
        {
            if (context != null && context.GetChannelConfiguration()!= null && context.GetChannelConfiguration().RecordId > 0)
            {
                ParameterSet parameters = new ParameterSet();
                parameters["@RETAILONLINECHANNELTABLE"] = context.GetChannelConfiguration().RecordId;
                ReadOnlyCollection<AFMConfirmationNumberMaskData> confirmationNumberMaskData = this.ExecuteStoredProcedure<AFMConfirmationNumberMaskData>(GETCONFIRMATIONNUMBERMASKSprocName, parameters);

                return confirmationNumberMaskData.ToList();
            }
            return null;
        }
    }
}

