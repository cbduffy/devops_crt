﻿using AFM.Commerce.Runtime.Services.Common;
using AFM.Commerce.Runtime.Services.DataTypes;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Data;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace AFM.Commerce.Runtime.Services.DataManagers
{
    /// <summary>
    /// This class contains methods to create/update/Fetch customer related custom information to/from CRT Database
    /// </summary>
    public class AFMCustomerDataManager : DatabaseAccessor
    {
        private const string CustomerElectronicAddressTableTypeVariableName = "@TVP_CUSTOMERELECTRONICADDRESSTABLETYPE";
        private const string LogisticLocation = "@logisticLocation";
        private const string LogisticLocationRecId = "@bi_LogisticLocationRecId";
        private const string FirstName = "@nvc_FirstName";
        private const string LastName = "@nvc_LastName";

        public AFMCustomerDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }

        private bool UpdateAddressInfo(Address address)
        {
            bool isUpdated = false;
            if (address.LogisticsLocationRecordId != 0)
            {
                ParameterSet parameters = new ParameterSet();
                parameters[LogisticLocationRecId] = address.LogisticsLocationRecordId;
                parameters[FirstName] = string.IsNullOrEmpty(address[CustomerConstants.AddressFirstName] as string)
                    ? string.Empty
                    : address[CustomerConstants.AddressFirstName];
                parameters[LastName] = string.IsNullOrEmpty(address[CustomerConstants.AddressLastName] as string)
                   ? string.Empty
                   : address[CustomerConstants.AddressLastName];

                this.ExecuteStoredProcedureNonQuery(CustomerConstants.UpdateAddressInfoStoredProcedure, parameters);
                isUpdated = true;
            }
            return isUpdated;
        }

        /// <summary>
        /// This method creates/Updates additional address and electronic address information
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public Customer CreateOrUpdateAdditonalAddressInfo(Customer customer)
        {
            //Iterate through the Customer address and get additional electroni contacts from each address
            if (customer.Addresses != null && Enumerable.Count<Address>((IEnumerable<Address>)customer.Addresses) > 0)
            {
                foreach (Address address in (IEnumerable<Address>)customer.Addresses)
                {
                    UpdateAddressInfo(address);
                }
            }

            return CreateOrUpdateElectronicAddress(customer);
        }


        /// <summary>
        /// This method creates/Updates additional electronic contacts of an address.
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        private Customer CreateOrUpdateElectronicAddress(Customer customer)
        {
            ParameterSet parameters = new ParameterSet();
            List<ContactInfo> list = new List<ContactInfo>();

            //Iterate through the Customer address and get additional electroni contacts from each address
            if (customer.Addresses != null && Enumerable.Count<Address>((IEnumerable<Address>)customer.Addresses) > 0)
            {
                foreach (Address address in (IEnumerable<Address>)customer.Addresses)
                {
                    list.AddRange((IEnumerable<ContactInfo>)this.GetAddressContacts(address));
                }
            }
            if (list.Count > 0)
            {
                using (ElectronicAddressTableType addressTableType = new ElectronicAddressTableType((IEnumerable<ContactInfo>)list))
                    parameters[CustomerElectronicAddressTableTypeVariableName] = (object)addressTableType.DataTable;

                //Execute existing 
                int errorCode = this.ExecuteStoredProcedureScalar(CustomerConstants.CreateUpdateElectronicAddressStoredProcedure, parameters);
                if (errorCode != 0)
                    throw new StorageException("Microsoft_Dynamics_Commerce_Runtime_CriticalStorageError", errorCode, "Unable to create or update Electronic Address.", new object[0]);
            }
            return customer;
        }

        /// <summary>
        /// This method creates and returns additional electronic contacts saved as Extension properties of Address object
        /// </summary>
        /// <param name="address">Address</param>
        /// <returns>List of electornics contacts</returns>
        private List<ContactInfo> GetAddressContacts(Address address)
        {
            List<ContactInfo> list = new List<ContactInfo>();

            //Phon 1
            if (!string.IsNullOrWhiteSpace(address.Phone))
            {
                ContactInfo contactInfo = new ContactInfo(ContactInfoType.Phone)
                {
                    Value = address.Phone,
                    ValueExtension = address.PhoneExt,
                    Description = CustomerConstants.Phone1,
                    RecordId = address.PhoneRecordId,
                    LogisticsLocationId = address.PhoneLogisticsLocationId,
                    LogisticsLocationRecordId = address.PhoneLogisticsLocationRecordId,
                    IsPrimary = true,
                    ParentLocation = address.LogisticsLocationRecordId,
                    PartyLocationRecordId = address.DirectoryPartyLocationRecordId,
                    PartyNumber = address.PartyNumber,
                    PartyRecordId = address.DirectoryPartyTableRecordId
                };
                list.Add(contactInfo);
            }

            //Phone 2
            if (Convert.ToInt64(address[CustomerConstants.Phone2RecordId]) > 0)
            {
                ContactInfo contactInfo = new ContactInfo(ContactInfoType.Phone)
                {
                    Value = address[CustomerConstants.Phone2] as string,
                    Description = CustomerConstants.Phone2,
                    ValueExtension = address[CustomerConstants.Phone2Extension] as string,
                    RecordId = Convert.ToInt64(address[CustomerConstants.Phone2RecordId]),
                    LogisticsLocationId = address.PhoneLogisticsLocationId,
                    LogisticsLocationRecordId = address.PhoneLogisticsLocationRecordId,
                    IsPrimary = false,
                    ParentLocation = address.LogisticsLocationRecordId,
                    PartyLocationRecordId = address.DirectoryPartyLocationRecordId,
                    PartyNumber = address.PartyNumber,
                    PartyRecordId = address.DirectoryPartyTableRecordId
                };
                list.Add(contactInfo);
            }

            //Phone 3
            if (Convert.ToInt64(address[CustomerConstants.Phone3RecordId]) > 0)
            {
                ContactInfo contactInfo = new ContactInfo(ContactInfoType.Phone)
                {
                    Value = address[CustomerConstants.Phone3] as string,
                    Description = CustomerConstants.Phone3,
                    ValueExtension = address[CustomerConstants.Phone3Extension] as string,
                    RecordId = Convert.ToInt64(address[CustomerConstants.Phone3RecordId]),
                    LogisticsLocationId = address.PhoneLogisticsLocationId,
                    LogisticsLocationRecordId = address.PhoneLogisticsLocationRecordId,
                    IsPrimary = false,
                    ParentLocation = address.LogisticsLocationRecordId,
                    PartyLocationRecordId = address.DirectoryPartyLocationRecordId,
                    PartyNumber = address.PartyNumber,
                    PartyRecordId = address.DirectoryPartyTableRecordId
                };
                list.Add(contactInfo);
            }

            //Email 1
            if (!string.IsNullOrWhiteSpace(address.Email))
            {
                ContactInfo contactInfo = new ContactInfo(ContactInfoType.Email)
                {
                    Value = address.Email,
                    Description = CustomerConstants.Email1,
                    RecordId = address.EmailRecordId,
                    LogisticsLocationId = address.EmailLogisticsLocationId,
                    LogisticsLocationRecordId = address.EmailLogisticsLocationRecordId,
                    IsPrimary = true,
                    ParentLocation = address.LogisticsLocationRecordId,
                    PartyLocationRecordId = address.DirectoryPartyLocationRecordId,
                    PartyNumber = address.PartyNumber,
                    PartyRecordId = address.DirectoryPartyTableRecordId
                };
                list.Add(contactInfo);
            }

            //Email 2
            if (!string.IsNullOrWhiteSpace(address[CustomerConstants.Email2] as string))
            {
                ContactInfo contactInfo = new ContactInfo(ContactInfoType.Email)
                {
                    Value = address[CustomerConstants.Email2] as string,
                    Description = CustomerConstants.Email2,
                    RecordId = Convert.ToInt64(address[CustomerConstants.Email2RecordId]),
                    LogisticsLocationId = address.EmailLogisticsLocationId,
                    LogisticsLocationRecordId = address.EmailLogisticsLocationRecordId,
                    IsPrimary = false,
                    ParentLocation = address.LogisticsLocationRecordId,
                    PartyLocationRecordId = address.DirectoryPartyLocationRecordId,
                    PartyNumber = address.PartyNumber,
                    PartyRecordId = address.DirectoryPartyTableRecordId
                };
                list.Add(contactInfo);
            }
            return list;
        }

        /// <summary>
        /// This method fetches Additional electronic contacts for given address using GetContactInfo method .
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public Customer GetElectronicAddresses(Customer customer)
        {
            ReadOnlyCollection<ContactInfo> contactInfoList;
            foreach (Address address in customer.Addresses)
            {
                contactInfoList = this.GetContactInfo(address);
                this.ParseContactInfo(address, contactInfoList);
            }

            return customer;
        }

        /// <summary>
        /// This method fetches Additional electronic contacts for given address and returns contact info list
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private ReadOnlyCollection<ContactInfo> GetContactInfo(Address address)
        {
            if (address.RecordId == 0L)
                return new ReadOnlyCollection<ContactInfo>((IList<ContactInfo>)new Collection<ContactInfo>());

            ParameterSet parameters = new ParameterSet();
            parameters[LogisticLocation] = (object)(Convert.ToBoolean(address.PhoneLogisticsLocationRecordId) ? address.PhoneLogisticsLocationRecordId : address.EmailLogisticsLocationRecordId);
            return this.ExecuteStoredProcedure<ContactInfo>(CustomerConstants.GetElectronicAddressesStoredProcedure, parameters);
        }

        /// <summary>
        /// This method populates the address object with additional electronic addresses by stroing it as extension properties
        /// </summary>
        /// <param name="address">Address object</param>
        /// <param name="contacts">List of electronic contacts</param>
        private void ParseContactInfo(Address address, ReadOnlyCollection<ContactInfo> contacts)
        {
            foreach (ContactInfo contact in contacts)
            {
                switch (contact.AddressType)
                {
                    case ContactInfoType.Email:
                        if (contact.Description == CustomerConstants.Email)
                        {
                            address[CustomerConstants.EmailDescription] = CustomerConstants.Email;
                            address[CustomerConstants.Email] = contact.Value;
                            address[CustomerConstants.EmailRecordId] = contact.RecordId;
                        }
                        if (contact.Description == CustomerConstants.Email1)
                        {
                            address[CustomerConstants.Email1Description] = CustomerConstants.Email1;
                            address[CustomerConstants.Email1] = contact.Value;
                            address[CustomerConstants.Email1RecordId] = contact.RecordId;
                        }

                        if (contact.Description == CustomerConstants.Email2)
                        {
                            address[CustomerConstants.Email2Description] = CustomerConstants.Email2;
                            address[CustomerConstants.Email2] = contact.Value;
                            address[CustomerConstants.Email2RecordId] = contact.RecordId;
                        }

                        if (Convert.ToBoolean(contact.LogisticsLocationRecordId))
                        {
                            address[CustomerConstants.LogisticElectronicLocationRecordId] = contact.LogisticsLocationRecordId;
                        }
                        break;
                    case ContactInfoType.Phone:

                        if (contact.Description == CustomerConstants.Phone1)
                        {
                            address[CustomerConstants.Phone1Description] = CustomerConstants.Phone1;
                            address[CustomerConstants.Phone1] = contact.Value;
                            address[CustomerConstants.Phone1Extension] = contact.ValueExtension;
                            address[CustomerConstants.Phone1RecordId] = contact.RecordId;
                        }

                        if (contact.Description == CustomerConstants.Phone2)
                        {
                            address[CustomerConstants.Phone2Description] = CustomerConstants.Phone2;
                            address[CustomerConstants.Phone2] = contact.Value;
                            address[CustomerConstants.Phone2Extension] = contact.ValueExtension;
                            address[CustomerConstants.Phone2RecordId] = contact.RecordId;
                        }

                        if (contact.Description == CustomerConstants.Phone3)
                        {
                            address[CustomerConstants.Phone3Description] = CustomerConstants.Phone3;
                            address[CustomerConstants.Phone3] = contact.Value;
                            address[CustomerConstants.Phone3Extension] = contact.ValueExtension;
                            address[CustomerConstants.Phone3RecordId] = contact.RecordId;
                        }

                        if (Convert.ToBoolean(contact.LogisticsLocationRecordId))
                        {
                            address[CustomerConstants.LogisticElectronicLocationRecordId] = contact.LogisticsLocationRecordId;
                        }

                        break;
                    default:
                        break;
                }
            }
        }
    }
}
