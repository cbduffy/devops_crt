﻿/*
 * NS Developed by muthait for AFM_TFS_40686 dated 06/25/2014
 */

using AFM.Commerce.Runtime.Services.DataTypes;

namespace AFM.Commerce.Framework.Core.DataManagers
{
    using AFM.Commerce.Framework.Core.DataTypes;
    using AFM.Commerce.Framework.Core.Models;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    /// Encapsulates vendor details information.
    /// </summary>
    public class AFMProductDataManager : DatabaseAccessor
    {

       
        private const string GetNapDataForProductsSprocName = "GETNAPDATAFORPRODUCTS";
        private const string GetOrderSettingsForProductsSprocName = "GETORDERSETTINGSFORPRODUCTS";

        /// <summary>
        /// Initializes a new instance of the <see cref="AFMVendorDetailsDataManager"/> class.
        /// </summary>
        /// <param name="context">Context of this request.</param>
        public AFMProductDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }

        public List<AFMPrdtNAP> GetPrdtNAPList(List<long> productIds, DateTime dateTime)
        {
            ReadOnlyCollection<AFMPrdtNAP> NAPPrdtList = null;
            if (productIds != null)
            {
                using (RecordIdTableType type = new RecordIdTableType(productIds))
                {
                    ParameterSet parameters = new ParameterSet();
                    parameters["@tvp_ProductIds"] = type.DataTable;
                    parameters["@dt_ChannelDate"] = dateTime;
                    NAPPrdtList =
                        this.ExecuteStoredProcedure<AFMPrdtNAP>(GetNapDataForProductsSprocName, parameters);
                    
                }
            }
            return NAPPrdtList.ToList();
        }

        // NS 
        /// <summary>
        /// This method will get the order settings like Min Order Qty and Multiple Qty for the Product
        /// </summary>
        /// <param name="itemIds"></param>
        /// <returns></returns>
        public List<AFMProductOrderSettings> GetOrderSettingsForProduct(IEnumerable<ProductLookupClause> itemIds)
        {
            ReadOnlyCollection<AFMProductOrderSettings> ProductList = null;
            if (itemIds == null) return null;
            using (ItemIdSearchTableType type = new ItemIdSearchTableType(itemIds))
            {
                ParameterSet parameters = new ParameterSet();
                parameters["@tvp_ItemIds"] = type.DataTable;
                ProductList =
                    this.ExecuteStoredProcedure<AFMProductOrderSettings>(GetOrderSettingsForProductsSprocName, parameters);

            }
            return ProductList.ToList();
        }
    }
}
