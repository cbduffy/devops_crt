﻿using AFM.Commerce.Framework.Core.Models;
using AFM.Commerce.Runtime.Services.DataTypes;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Data;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace AFM.Commerce.Framework.Core.DataManagers
{
    public class AFMRetailParamaterDataManger : DatabaseAccessor
    {
        private const string AFMGetRetailParamInforCodeSprocName = "afmgetretailparminforcode";

        public AFMRetailParamaterDataManger(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
            this.Context = context;
        }

        public AFMRetailParameters getAfmRetailParamater()
        {
            ParameterSet parameters = new ParameterSet();
            parameters["@Dataareaid"] = this.Context.GetChannelConfiguration().InventLocationDataAreaId;
            var result = this.ExecuteStoredProcedure<AFMRetailParameters>(AFMGetRetailParamInforCodeSprocName, parameters);
            if (result.Count != 0)
                return result[0];
            else
                return new AFMRetailParameters() { InforCodeId = string.Empty, InforCodeDescription = string.Empty };
        }
    }
}
