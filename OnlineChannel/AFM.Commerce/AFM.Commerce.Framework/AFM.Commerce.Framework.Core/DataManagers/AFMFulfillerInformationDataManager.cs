﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Data;
using AFM.Commerce.Framework.Core.Models;
using AFM.Commerce.Entities;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.DataManagers
{
    public class AFMFulfillerInformationDataManager : DatabaseAccessor
    {
        private const string GetFulfillerInformationprocName = "AFMGETALLFULFILLERINFORMATION";

        public AFMFulfillerInformationDataManager(RequestContext context)
            : base(DataStoreManager.DataStores[DataStoreType.Database], context)
        {
        }

 
        /// <summary>
        /// Datamanager call to get fulfiller information
        /// </summary>
        /// <param name="ItemIds"></param>
        /// <returns></returns>
        public List<AFMFulFillerInformation> GetAllFulfillerInformation()
        {
            
            ReadOnlyCollection<AFMFulFillerInformation> fulfillerInformationList = this.ExecuteStoredProcedure<AFMFulFillerInformation>(GetFulfillerInformationprocName,new ParameterSet());

            return fulfillerInformationList.ToList();
        }
    }
}
