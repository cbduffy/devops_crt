﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.Models
{
    /// <summary>
    /// This class inherits commerce entity and represents properties for NAP Data related Information  related information
    /// </summary>
    [DataContract]
    public class AFMNapData : CommerceEntity
    {
        private const string ProductRecordIdColumnName = "PRODUCTID";
        private const string ProductNumberColumnName = "PRODUCTNUMBER";
        private const string NapItemColumnName = "NAPITEM";
        private const string NapStartDateTimeColumnName = "NAPSTARTDATETIME";
        private const string NapEndDateTimeColumnName = "NAPENDDATETIME";
        private const string OmOperatingUnitColumnName = "OMOPERATINGUNIT";


         public AFMNapData()
            : base("AFMNapData")
        {

        }

        [DataMember]
        public long ProductRecordId
        {
            get
            {
                return (long)(this["PRODUCTID"] ?? (object)0);
            }
            set
            {
                this["PRODUCTID"] = (object)value;
            }

        }

        [DataMember]
        public string ProductNumber
        {
            get
            {
                return (string)(this["PRODUCTNUMBER"] ?? (object)string.Empty);
            }
            set
            {
                this["PRODUCTNUMBER"] = (object)value;
            }

        }

        [DataMember]
        public long NapItem
        {
            get
            {
                return (long)(this["NAPITEM"] ?? (object)0);
            }
            set
            {
                this["NAPITEM"] = (object)value;
            }

        }

        [DataMember]
        public DateTime NapStartDateTime
        {
            get
            {
                return (DateTime)(this["NAPSTARTDATETIME"] ?? (object)DateTime.MinValue);
            }
            set
            {
                this["NAPSTARTDATETIME"] = (object)value;
            }

        }

        [DataMember]
        public DateTime NapEndDateTime
        {
            get
            {
                return (DateTime)(this["NAPENDDATETIME"] ?? (object)DateTime.MinValue);
            }
            set
            {
                this["NAPENDDATETIME"] = (object)value;
            }

        }

        [DataMember]
        public string OmOperatingUnit
        {
            get
            {
                return (string)(this["OMOPERATINGUNIT"] ?? (object)string.Empty);
            }
            set
            {
                this["OMOPERATINGUNIT"] = (object)value;
            }

        }
    }
}
