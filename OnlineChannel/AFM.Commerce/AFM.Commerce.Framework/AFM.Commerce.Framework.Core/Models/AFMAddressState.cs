﻿namespace AFM.Commerce.Framework.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    [DataContract]
    public class AFMAddressState 
    {
        public AFMAddressState()
        {

        }

        [DataMember]
        public string CountryRegionId
        {
            get;
            set;
        }

        [DataMember]
        public string StateId
        {
            get;
            set;
        }

        [DataMember]
        public string StateName
        {
            get;
            set;
        }
    }
}
