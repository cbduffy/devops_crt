﻿namespace AFM.Commerce.Framework.Core.Models
{
    using System;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Stores all the discounts related details for a {Channel, Catalog} pair.
    /// </summary>
    public class AFMPromotionDetails : CommerceEntity
    {
        private const string ItemIdColumn = "ITEMID";
        private const string ProductNameColumn = "PRODUCTNAME";
        private const string ProductOfferDescColumn = "PRODOFFERDESC";
        private const string MinQtyColumn = "MinQty";
        private const string DiscountMethodColumn = "DISCOUNTMETHOD";
        private const string DiscountPercentColumn = "DISCPERCENT";
        private const string DiscountAmountColumn = "DISCAMOUNT";
        private const string OfferPriceColumn = "OFFERPRICE";
        private const string TextValueColumn = "TEXTVALUE";
        private const string OfferPriceInclTaxColumn = "OFFERPRICEINCLTAX";
        private const string CurrencyColumn = "CURRENCY";
        private const string DiscountNameColumn = "DISCOUNTNAME";
        private const string DiscountDescColumn = "DISCOUNTDESC";
        private const string DisclaimerColumn = "DISCLAIMER";
        private const string OfferIdColumn = "OFFERID";
        private const string DiscCodeReqdColumn = "ISDISCOUNTCODEREQUIRED";
        private const string DiscountCodeColumn = "DISCOUNTCODE";

        /// <summary>
        /// Initializes a new instance of the <see cref="AFMPromotionDetails"/> class.
        /// </summary>
        public AFMPromotionDetails()
            : base("ChannelPromotionsData")
        {
        }

        /// <summary>
        /// Gets or sets the item identifier.
        /// </summary>
        public string ItemId
        {
            get { return (string)this[ItemIdColumn]; }
            set { this[ItemIdColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the name of the product being promoted.
        /// </summary>
        public string ProductName
        {
            get { return (string)this[ProductNameColumn]; }
            set { this[ProductNameColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the description of the offer present on the product.
        /// </summary>
        public string ProductOfferDescription
        {
            get { return (string)this[ProductOfferDescColumn]; }
            set { this[ProductOfferDescColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the the Image URL.
        /// </summary>
        public string ProductImageLink
        {
            get
            {
                string url = (string)this[TextValueColumn];
                if (string.IsNullOrWhiteSpace(url))
                {
                    return string.Empty;
                }
                else
                {
                    return url.Contains("^") ? url.Substring(0, url.IndexOf('^')) : url;
                }
            }

            set
            {
                this[TextValueColumn] = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum quantity required to trigger the offer.
        /// </summary>
        public decimal MinimumQuantity
        {
            get { return (decimal)(this[MinQtyColumn] ?? 0M); }
            set { this[MinQtyColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the discount is available on the product (e.g. Amount Off / % Off / Offer Price / incl tax).
        /// </summary>
        public int DiscountMethod
        {
            get { return (int)(this[DiscountMethodColumn] ?? 0); }
            set { this[DiscountMethodColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the discount percentage.
        /// </summary>
        public decimal DiscountPercent
        {
            get { return (decimal)(this[DiscountPercentColumn] ?? 0M); }
            set { this[DiscountPercentColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the amount Off on the product.
        /// </summary>
        public decimal DiscountAmount
        {
            get { return (decimal)(this[DiscountAmountColumn] ?? 0M); }
            set { this[DiscountAmountColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the final offer price.
        /// </summary>
        public decimal OfferPrice
        {
            get { return (decimal)(this[OfferPriceColumn] ?? 0M); }
            set { this[OfferPriceColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the final offer price inclusive of taxes.
        /// </summary>
        public decimal OfferPriceInclusiveOfTax
        {
            get { return (decimal)(this[OfferPriceInclTaxColumn] ?? 0M); }
            set { this[OfferPriceInclTaxColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the currency for the amount.
        /// </summary>
        public string Currency
        {
            get { return (string)this[CurrencyColumn]; }
            set { this[CurrencyColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the name for the discount.
        /// </summary>
        public string DiscountName
        {
            get { return (string)this[DiscountNameColumn]; }
            set { this[DiscountNameColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the Description of the discount.
        /// </summary>
        public string DiscountDescription
        {
            get { return (string)this[DiscountDescColumn]; }
            set { this[DiscountDescColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the discount disclaimer.
        /// </summary>
        public string Disclaimer
        {
            get { return (string)this[DisclaimerColumn]; }
            set { this[DisclaimerColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the offer identifier associated to the discount.
        /// </summary>
        public string OfferId
        {
            get { return (string)this[OfferIdColumn]; }
            set { this[OfferIdColumn] = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether a discount code is required.
        /// </summary>
        public bool IsDiscountCodeRequired
        {
            get { return Convert.ToBoolean(Convert.ToInt32(this[DiscCodeReqdColumn], CultureInfo.InvariantCulture)); }
            set { this[DiscCodeReqdColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the discount code (if any).
        /// </summary>
        public string DiscountCode
        {
            get { return (string)this[DiscountCodeColumn]; }
            set { this[DiscountCodeColumn] = value; }
        }
    }
}