﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.Models
{
    /// <summary>
    /// This class inherits commerce entity and represents properties for ItemCovers product entity
    /// </summary>
    [DataContract]
    public class AFMItemCovers:CommerceEntity
    {

        private const string ProductRecordIdColumnName = "PRODUCTID";
        private const string ProductNumberColumnName = "PRODUCTNUMBER";
        private const string CoverIdColumnName = "COVERID";
        private const string CoverNameColumnName = "COVERNAME";
        private const string LocationColumnName = "LOCATION";
        private const string CoverColorColumnName = "COVERCOLOR";
        private const string ContentsColumnName = "CONTENTS";
        private const string CleaningIdColumnName = "CLEANINGID";
        private const string CleaningDescriptionColumnName = "CLEANINGDESCRIPTION";
        private const string LonDescriptionColumnName = "LONGDESCRIPTION";

        public AFMItemCovers()
            : base("ItemCovers")
        {

        }

        [DataMember]
        public long ProductRecordId
        {
            get
            {
                return (long)(this["PRODUCTID"] ?? (object)0);
            }
            set
            {
                this["PRODUCTID"] = (object)value;
            }

        }

        [DataMember]
        public string ProductNumber
        {
            get
            {
                return (string)(this["PRODUCTNUMBER"] ?? (object)string.Empty);
            }
            set
            {
                this["PRODUCTNUMBER"] = (object)value;
            }

        }

        [DataMember]
        public string CoverId
        {
            get
            {
                return (string)(this["COVERID"] ?? (object)string.Empty);
            }
            set
            {
                this["COVERID"] = (object)value;
            }

        }

        [DataMember]
        public string CoverName
        {
            get
            {
                return (string)(this["COVERNAME"] ?? (object)string.Empty);
            }
            set
            {
                this["COVERNAME"] = (object)value;
            }

        }


        [DataMember]
        public string Location
        {
            get
            {
                return (string)(this["LOCATION"] ?? (object)string.Empty);
            }
            set
            {
                this["LOCATION"] = (object)value;
            }

        }

        [DataMember]
        public string CoverColor
        {
            get
            {
                return (string)(this["COVERCOLOR"] ?? (object)string.Empty);
            }
            set
            {
                this["COVERCOLOR"] = (object)value;
            }

        }

        [DataMember]
        public string Contents
        {
            get
            {
                return (string)(this["CONTENTS"] ?? (object)string.Empty);
            }
            set
            {
                this["CONTENTS"] = (object)value;
            }

        }

        [DataMember]
        public string CleaningId
        {
            get
            {
                return (string)(this["CLEANINGID"] ?? (object)string.Empty);
            }
            set
            {
                this["CLEANINGID"] = (object)value;
            }

        }

        [DataMember]
        public string CleaningDescription
        {
            get
            {
                return (string)(this["CLEANINGDESCRIPTION"] ?? (object)string.Empty);
            }
            set
            {
                this["CLEANINGDESCRIPTION"] = (object)value;
            }

        }
        [DataMember]
        public string LongDescription
        {
            get
            {
                return (string)(this["LONGDESCRIPTION"] ?? (object)string.Empty);
            }
            set
            {
                this["LONGDESCRIPTION"] = (object)value;
            }

        }
    }
}
