﻿/*NS Developed by ysrini for AFM_TFS_40686 dated 06/29/2014*/

namespace AFM.Commerce.Framework.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    public class AFMVendorDetails : CommerceEntity
    {
        private const string VendorIdColumn = "ACCOUNTNUM";
        private const string VendorNameColumn = "NAME";
        private const string AFIVendorIdColumn = "AFIVendorId";
        private const string AFIVendorNameColumn = "VendorName";
        private const string ShipToIdColumn = "ShipToId";
        private const string InvoiceVendorIdColumn = "InvoiceVendorId";

        public AFMVendorDetails() : base("ChannelVendorDetails")
        {
        }

        /// <summary>
        /// Gets or sets the VendorId identifier.
        /// </summary>
        public string AFMVendorId
        {
            get { return (string)this[VendorIdColumn]; }
            set { this[VendorIdColumn] = value; }
        }

   
        /// <summary>
        /// Gets or sets the VendorId identifier.
        /// </summary>
        public string AFMVendorName
        {
            get { return (string)this[VendorNameColumn]; }
            set { this[VendorNameColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the AFIVendorId identifier.
        /// </summary>
        public string AFIVendorId
        {
            get { return (string)this[AFIVendorIdColumn]; }
            set { this[AFIVendorIdColumn] = value; }
        }


        /// <summary>
        /// Gets or sets the AFIVendorName identifier.
        /// </summary>
        public string AFIVendorName
        {
            get { return (string)this[AFIVendorNameColumn]; }
            set { this[AFIVendorNameColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the ShipToId identifier.
        /// </summary>
        public string ShipToId
        {
            get { return (string)this[ShipToIdColumn]; }
            set { this[ShipToIdColumn] = value; }
        }
        
        /// <summary>
        /// Gets or sets the Invoice VendorId identifier.
        /// </summary>
        public string InvoiceVendorId
        {
            get { return (string)this[InvoiceVendorIdColumn]; }
            set { this[InvoiceVendorIdColumn] = value; }
        }
    }
}
