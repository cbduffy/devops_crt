﻿using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.Models
{
    //NS by spriya : DMND0010113 - Synchrony Financing online
   
    public class AFMSynchronyOptions : CommerceEntity
    {
        private const string FinancingIdColumnName = "FINANCINGOPTIONID";
        private const string CardTypeColumnName = "CARDTYPE";
        private const string DefaultOrderTotalColumnName = "DEFAULTORDERTOTAL";
        private const string SortOrderColumnName = "SORTORDER";
        private const string PromoDesc1ColumnName = "PROMODESC1";
        private const string PromoDesc2ColumnName = "PROMODESC2";
        private const string PromoDesc3ColumnName = "PROMODESC3";
        private const string LearnMoreColumnName = "LEARNMORE";
        private const string ShowLearnMoreColumnName = "SHOWLEARNMORE";
        private const string ThirdPartyTermCodeColumnName ="ThirdPartyTermCode";
        private const string RemoveDiscountPopUpColumnName = "RemoveDiscountPopUp";
        private const string EffectiveDateColumnName = "EffectiveDate";
        private const string ExpirationDateColumnName = "ExpirationDate";
        private const string StatusColumnName = "Status";
        private const string RemoveCartDiscountColumnName = "RemoveCartDiscounts";
        private const string MinimumPurchaseAmountColumnName = "MinimumPurchaseAmount";
        private const string PaymentFeeColumnName = "PaymentFee";
        private const string EstimatedPercentageColumnName = "ESTIMATEDPERCENTAGE";
        private const string PaymTermIdColumnName = "PAYMTERMID";
        private const string PaymModeColumnName = "PAYMMODE";
        private const string CardNameColumnName = "CardName";

        public AFMSynchronyOptions()
            : base("AFMSynchronyOptions")
        {
        }


        public string FinancingOptionId
        {
            get
            {
                return (string)(this[FinancingIdColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[FinancingIdColumnName] = (object)value;
            }
        }

        public string CardType
        {
            get
            {
                return (string)(this[CardTypeColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[CardTypeColumnName] = (object)value;
            }
        }

        public bool DefaultOrderTotal
        {
            get
            {
                return Convert.ToBoolean(this[DefaultOrderTotalColumnName]);
            }
            set
            {
                this[DefaultOrderTotalColumnName] = (object)value;
            }
        }

        public int Sortorder
        {
            get
            {
                return Convert.ToInt32(this[SortOrderColumnName]);
            }
            set
            {
                this[SortOrderColumnName] = (object)value;
            }
        }

        public string PromoDesc1
        {
            get
            {
                return (string)(this[PromoDesc1ColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[PromoDesc1ColumnName] = (object)value;
            }
        }

        public string PromoDesc2
        {
            get
            {
                return (string)(this[PromoDesc2ColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[PromoDesc2ColumnName] = (object)value;
            }
        }

        public string PromoDesc3
        {
            get
            {
                return (string)(this[PromoDesc3ColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[PromoDesc3ColumnName] = (object)value;
            }
        }

        public string LearnMore
        {
            get
            {
                return (string)(this[LearnMoreColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[LearnMoreColumnName] = (object)value;
            }
        }

        public bool ShowLearnMore
        {
            get
            {
                return Convert.ToBoolean(this[ShowLearnMoreColumnName]);
            }
            set
            {
                this[ShowLearnMoreColumnName] = (object)value;
            }
        }

        public string ThirdPartyTermCode { 
            get
            {
                return (string)(this[ThirdPartyTermCodeColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[ThirdPartyTermCodeColumnName] = (object)value;
            } 
        }

        public bool RemoveDiscountPopUp {
            get
            {
                return Convert.ToBoolean(this[RemoveDiscountPopUpColumnName]);
            }
            set
            {
                this[RemoveDiscountPopUpColumnName] = (object)value;
            }
        }

        public DateTime EffectiveDate { 
            get
            {
                return (DateTime)(this[EffectiveDateColumnName] ?? (object)DateTime.MinValue);
            }
            set
            {
                this[EffectiveDateColumnName] = (object)value;
            } 
        }

        public DateTime ExpirationDate { 
            get
            {
                return (DateTime)(this[ExpirationDateColumnName] ?? (object)DateTime.MinValue);
            }
            set
            {
                this[ExpirationDateColumnName] = (object)value;
            }  }

        
        public bool Status { 
            get
            {
                return Convert.ToBoolean(this[StatusColumnName]);
            }
            set
            {
                this[StatusColumnName] = (object)value;
            }
        }

        public bool RemoveCartDiscount { 
            get
            {
                return Convert.ToBoolean(this[RemoveCartDiscountColumnName]);
            }
            set
            {
                this[RemoveCartDiscountColumnName] = (object)value;
            }
        }

     
        public Decimal MinimumPurchaseAmount { 
            get
            {
                return Convert.ToDecimal(this[MinimumPurchaseAmountColumnName]);
            }
            set
            {
                this[MinimumPurchaseAmountColumnName] = value;
            } }

      
        public string PaymentFee {
            get
            {
                return (string)(this[PaymentFeeColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[PaymentFeeColumnName] = (object)value;
            }
        }

        
        public Decimal EstimatedPercentage {
            get
            {
                return Convert.ToDecimal(this[EstimatedPercentageColumnName]);
            }
            set
            {
                this[EstimatedPercentageColumnName] = value;
            } }

        public string PaymTermId
        {
            get
            {
                return (string)(this[PaymTermIdColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[PaymTermIdColumnName] = (object)value;
            }
        }

        public string PaymMode
        {
            get
            {
                return (string)(this[PaymModeColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[PaymModeColumnName] = (object)value;
            }
        }

        public string CardName
        {
            get
            {
                return (string)(this[CardNameColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[CardNameColumnName] = (object)value;
            }
        }


    }
}
