﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.Models
{
     [DataContract]
    public class AFMItemPriceSnapshot
    {
        public AFMItemPriceSnapshot()
         {

         }

          [DataMember]
        public string ItemId {
            get;
            set;
        }

          [DataMember]
        public decimal BestPrice 
        { 
            get;
            set; 
        }

          [DataMember]
        public decimal BasePrice 
        { 
            get; 
            set; 
        }

          [DataMember]
        public long AffiliationId 
        { 
            get;
            set;
        }

          [DataMember]
          public DateTime LastModified
          {
              get;
              set;
          }
    }
}
