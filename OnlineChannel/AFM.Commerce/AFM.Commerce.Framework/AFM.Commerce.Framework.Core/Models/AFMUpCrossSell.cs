﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.Models
{
    /// <summary>
    /// This class inherits commerce entity and represents properties for Up Cross Sell product entity
    /// </summary>
    [DataContract]
    public class AFMUpCrossSell : CommerceEntity
    {

        private const string ProductRecordIdColumnName = "PRODUCTID";
        private const string ProductNumberColumnName = "PRODUCTNUMBER";
        private const string LinkedItemColumnName = "LINKEDITEM";
        private const string PriorityColumnName = "PRIORITY";
        private const string UpSellTypeColumnName = "UPSELLTYPE";
        private const string RuleIdColumnName = "RULEID";
  
        public AFMUpCrossSell()
            : base("UpCrossSell")
        {
        }

        [DataMember]
        public long ProductRecordId
        {
            get
            {
                return (long)(this["PRODUCTID"] ?? (object)0);
            }
            set
            {
                this["PRODUCTID"] = (object)value;
            }

        }

        [DataMember]
        public string ProductNumber
        {
            get
            {
                return (string)(this["PRODUCTNUMBER"] ?? (object)string.Empty);
            }
            set
            {
                this["PRODUCTNUMBER"] = (object)value;
            }

        }


        [DataMember]
        public string LinkedItem
        {
            get
            {
                return (string)(this["LINKEDITEM"] ?? (object)string.Empty);
            }
            set
            {
                this["LINKEDITEM"] = (object)value;
            }

        }


        [DataMember]
        public int Priority
        {
            get
            {
                return (int)(this["PRIORITY"] ?? (object)0);
            }
            set
            {
                this["PRIORITY"] = (object)value;
            }

        }

        [DataMember]
        public int UpSellType
        {
            get
            {
                return (int)(this["UPSELLTYPE"] ?? (object)0);
            }
            set
            {
                this["UPSELLTYPE"] = (object)value;
            }

        }

        [DataMember]
        public string RuleId
        {
            get
            {
                return (string)(this["RULEID"] ?? (object)string.Empty);
            }
            set
            {
                this["RULEID"] = (object)value;
            }

        }
    }
}
