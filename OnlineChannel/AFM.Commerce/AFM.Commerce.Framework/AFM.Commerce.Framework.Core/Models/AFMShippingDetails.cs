﻿/*NS Developed by ysrini for AFM_TFS_40686 dated 06/29/2014*/

namespace AFM.Commerce.Framework.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    public class AFMShippingDetails //: CommerceEntity
    {
        //private const string ShippingModeColumn = "ShippingMode";
        //private const string ShippingCostColumn = "ShippingCost";
        //public ShippingDetails()
        //    : base("ChannelShippingDetails")
        //{
        //}
        /// <summary>
        /// Gets or sets the DeliveryMethod identifier.
        /// </summary>
        public int DeliveryMethod
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the IsServiceItem identifier.
        /// </summary>
        public bool IsServiceItem
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ChargeName identifier.
        /// </summary>
        public string ChargeName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ChargeType identifier.
        /// </summary>
        public int ChargeType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the TotalValue identifier.
        /// </summary>
        public decimal TotalValue
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets the LineId identifier.
        /// </summary>
        public string LineId
        {
            get;
            set;
        }

        ///// <summary>
        ///// Gets or sets the ProductDetails identifier.
        ///// </summary>
        //public string ProductDetails
        //{
        //    get;
        //    set;
        //}

        ///// <summary>
        ///// Gets or sets the Name identifier.
        ///// </summary>
        //public string Name
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// Gets or sets the ProductId identifier.
        /// </summary>
        public long ProductId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ItemId identifier.
        /// </summary>
        public string ItemId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Quantity identifier.
        /// </summary>
        public string Quantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ZipCode identifier.
        /// </summary>
        public string ZipCode
        {
            get;
            set;
        }

        ///// <summary>
        ///// Gets or sets the AFMVendorId identifier.
        ///// </summary>
        //public string AFMVendorId
        //{
        //    get;
        //    set;
        //}

        ///// <summary>
        ///// Gets or sets the AFMVendorName identifier.
        ///// </summary>
        //public string AFMVendorName
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// Gets or sets the Delivery Mode identifier.
        /// </summary>
        public string DeliveryMode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Shipping Mode identifier.
        /// </summary>
        public string ShippingMode
        {
            get;
            set;
        }

   
        /// <summary>
        /// Gets or sets the Shipping Cost identifier.
        /// </summary>
        public decimal ShippingCost
        {
            get;
            set;
        }

        // NS Developed by spriya

        /// <summary>
        /// Gets or sets Direct Ship value
        /// </summary>
        public int DirectShip
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or Sets Supplier Direct Ship value
        /// </summary>
        public int SupplierDirectShip
        {
            get;
            set;
        }

        // NE Developed by spriya
    }
}
