﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;



namespace AFM.Commerce.Framework.Core.Models
{
    /// <summary>
    /// This class inherits commerce entity and represents properties for AlsoAlsoKnown product entity
    /// </summary>
    [DataContract]
    public class AFMAlsoKnownAs : CommerceEntity
    {

        private const string ProductRecordIdColumnName = "PRODUCTID";
        private const string ProductNumberColumnName = "PRODUCTNUMBER";
        private const string AkaGroupDescriptionColumnName = "AKAGROUPDESCRIPTION";
        private const string AkaGroupIdColumnName = "AKAGROUPID";
        private const string SynonymsColumnName = "SYNONYMS";
 

        public AFMAlsoKnownAs()
            : base("AlsoKnownAs")
        {
        }

        [DataMember]
        public long ProductRecordId
        {
            get
            {
                return (long)(this["PRODUCTID"] ?? (object)0);
            }
            set
            {
                this["PRODUCTID"] = (object)value;
            }

        }

        [DataMember]
        public string ProductNumber
        {
            get
            {
                return (string)(this["PRODUCTNUMBER"] ?? (object)string.Empty);
            }
            set
            {
                this["PRODUCTNUMBER"] = (object)value;
            }

        }


        [DataMember]
        public string AkaGroupDescription
        {
            get
            {
                return (string)(this["AKAGROUPDESCRIPTION"] ?? (object)string.Empty);
            }
            set
            {
                this["AKAGROUPDESCRIPTION"] = (object)value;
            }

        }

        [DataMember]
        public string AkaGroupId
        {
            get
            {
                return (string)(this["AKAGROUPID"] ?? (object)string.Empty);
            }
            set
            {
                this["AKAGROUPID"] = (object)value;
            }

        }

        [DataMember]
        public string Synonyms
        {
            get
            {
                return (string)(this["SYNONYMS"] ?? (object)string.Empty);
            }
            set
            {
                this["SYNONYMS"] = (object)value;
            }

        }
    }
}
