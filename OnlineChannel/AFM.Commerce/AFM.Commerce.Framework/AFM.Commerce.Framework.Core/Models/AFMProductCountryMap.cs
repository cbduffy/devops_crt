﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.Models
{
    /// <summary>
    /// This class inherits commerce entity and represents properties for Product Country Mapping data
    /// </summary>
    [DataContract]
    public class AFMProductCountryMap: CommerceEntity
    {
        private const string ProductRecordIdColumnName = "PRODUCTID";
        private const string ProductNumberColumnName = "PRODUCTNUMBER";
        private const string CountryRegionIdColumnName = "COUNTRYREGIONID";
        private const string IsoCodeColumnName = "ISOCODE";

        public AFMProductCountryMap()
            : base("ProductCountryMap")
        {
        }

        [DataMember]
        public long ProductRecordId
        {
            get
            {
                return (long)(this["PRODUCTID"] ?? (object)0);
            }
            set
            {
                this["PRODUCTID"] = (object)value;
            }

        }


        [DataMember]
        public string ProductNumber
        {
            get
            {
                return (string)(this["PRODUCTNUMBER"] ?? (object)string.Empty);
            }
            set
            {
                this["PRODUCTNUMBER"] = (object)value;
            }

        }
        [DataMember]
        public string CountryRegionId
        {
            get
            {
                return (string)(this["COUNTRYREGIONID"] ?? (object)string.Empty);
            }
            set
            {
                this["COUNTRYREGIONID"] = (object)value;
            }

        }

        [DataMember]
        public string IsoCode
        {
            get
            {
                return (string)(this["ISOCODE"] ?? (object)string.Empty);
            }
            set
            {
                this["ISOCODE"] = (object)value;
            }

        }
    }
}
