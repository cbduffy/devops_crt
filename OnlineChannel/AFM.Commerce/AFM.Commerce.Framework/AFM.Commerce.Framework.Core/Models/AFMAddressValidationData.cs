﻿

namespace AFM.Commerce.Framework.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    
    [DataContract]
    public class AFMAddressValidationData : CommerceEntity
    {
        private const string ServiceByPassFlagColumnName = "ServiceByPassFlag";
        private const string ZipCodeMasterFlagColumnName = "ZipCodeMasterFlag";
        private const string ValidationAddressTypeColumnName = "ValidationAddressType";
        private const string AddressNameColumnName = "AddressName";
        private const string IsZipCodeMasterValidColumnName = "IsZipCodeMasterValid";

        public AFMAddressValidationData()
            : base("AddressValidationData")
        {

        }

        [DataMember]
        public bool ServiceByPassFlag {
            get
            {
                return Convert.ToBoolean(this[ServiceByPassFlagColumnName]);
            }
            set
            {
                this[ServiceByPassFlagColumnName] = (object)value;
            }
        }

        [DataMember]
        public bool ZipCodeMasterFlag {
            get
            {                
                    return Convert.ToBoolean(this[ZipCodeMasterFlagColumnName]);
            }
            set
            {
                this[ZipCodeMasterFlagColumnName] = (object)value;
            }
        }

        [DataMember]
        public AddressType ValidationAddressType
        {
            get
            {
                return (AddressType)(Convert.ToInt32(this[ValidationAddressTypeColumnName]));
            }
            set
            {
                this[ValidationAddressTypeColumnName] = (object)value;
            }
        }

        [DataMember]
        public string AddressName
        {
            get
            {
                return (string)(this[AddressNameColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[AddressNameColumnName] = (object)value;
            }
        }

        [DataMember]
        public bool IsZipCodeMasterValid
        {
            get
            {
                return Convert.ToBoolean(this[IsZipCodeMasterValidColumnName]);
            }
            set
            {
                this[IsZipCodeMasterValidColumnName] = (object)value;
            }
        }
    }
}
