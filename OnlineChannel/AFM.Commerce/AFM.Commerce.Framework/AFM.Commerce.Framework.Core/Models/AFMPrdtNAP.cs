﻿namespace AFM.Commerce.Framework.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    public class AFMPrdtNAP : CommerceEntity
    {

        public AFMPrdtNAP(): base("PrdtNAP")
        {
        }

        private const string productID = "PRODUCTID";
        private const string isNAP = "ISNAP";

        public long ProductId
        {
            get { return Convert.ToInt64(this[productID]); }
            set { this[productID] = value; }
        }

        public bool IsNAP
        {
            get
            {
                if (this[isNAP].ToString() == "0")
                    return true;
                else
                    return false;
            }
            set { this[productID] = value; }
        } 
    }
}
