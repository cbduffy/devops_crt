﻿
namespace AFM.Commerce.Framework.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    public class AFMItemFlags : CommerceEntity
    {
        private const string IsEcommOwnedColumn = "IsEcomm";
        private const string IsSDSOColumn = "IsSDSO";
        private const string ItemIdColumn = "ItemId";

        public AFMItemFlags()
            : base("ChannelVendorDetails")
        {
        }

        /// <summary>
        /// Gets or sets the IsEcommOwned identifier.
        /// </summary>
        public int IsEcommOwned
        {
            get { return Convert.ToInt32(this[IsEcommOwnedColumn]); }
            set { this[IsEcommOwnedColumn] = value; }
        }


        /// <summary>
        /// Gets or sets the IsSDSO identifier.
        /// </summary>
        public int IsSDSO
        {
            get { return Convert.ToInt32(this[IsSDSOColumn]); }
            set { this[IsSDSOColumn] = value; }
        }

        /// <summary>
        /// Gets or sets the ItemID identifier
        /// </summary>
        public long ItemId
        {
            get { return Convert.ToInt64(this[ItemIdColumn]); }
            set { this[ItemIdColumn] = value; }
        }
    }
}
