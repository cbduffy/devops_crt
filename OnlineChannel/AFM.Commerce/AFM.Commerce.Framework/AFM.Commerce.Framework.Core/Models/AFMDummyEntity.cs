﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.Models
{
    /// <summary>
    /// This class inherits commerece entity and is a dummy entity for stored procedure call
    /// </summary>
    class AFMDummyEntity: CommerceEntity
    {
        
        public AFMDummyEntity()
            : base("dummyEntity")
        {
        }

    }
}
