﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.Models
{
    /// <summary>
    /// This class inherits commerce entity and represents properties for ECommOwned related information
    /// </summary>
    [DataContract]
    public class AFMECommOwnedData : CommerceEntity
    {

        private const string ProductRecordIdColumnName = "PRODUCTID";
        private const string ProductNumberColumnName = "PRODUCTNUMBER";
        private const string OmOperatingUnitColumnName = "OMOPERATINGUNIT";
        private const string EcommOwnedFlagColumnName = "ECOMMOWNEDFLAG";
    
        public AFMECommOwnedData()
            : base("ECommOwnedData")
        {

        }

        [DataMember]
        public long ProductRecordId
        {
            get
            {
                return (long)(this["PRODUCTID"] ?? (object)0);
            }
            set
            {
                this["PRODUCTID"] = (object)value;
            }

        }

        [DataMember]
        public string ProductNumber
        {
            get
            {
                return (string)(this["PRODUCTNUMBER"] ?? (object)string.Empty);
            }
            set
            {
                this["PRODUCTNUMBER"] = (object)value;
            }

        }

        [DataMember]
        public string OmOperatingUnit
        {
            get
            {
                return (string)(this["OMOPERATINGUNIT"] ?? (object)string.Empty);
            }
            set
            {
                this["OMOPERATINGUNIT"] = (object)value;
            }

        }

        [DataMember]
        public int EcommOwnedFlag
        {
            get
            {
                return (int)(this["ECOMMOWNEDFLAG"] ?? (object)0);
            }
            set
            {
                this["ECOMMOWNEDFLAG"] = (object)value;
            }

        }

    }
}
