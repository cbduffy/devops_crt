﻿namespace AFM.Commerce.Framework.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    [DataContract]
    public class AFMZipCodeMasterData
    {

        public AFMZipCodeMasterData(string city, string state, string zipCode, string countryRegionId, AddressType validationAddressType)
        {
            this.City = city;
            this.State = state;
            this.ZipCode = zipCode;
            this.CountryRegionId = countryRegionId;
            this.ValidationAddressType = validationAddressType;
        }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string ZipCode { get; set; }

        [DataMember]
        public string CountryRegionId { get; set; }

        [DataMember]
        public AddressType ValidationAddressType { get; set; }


    }
}
