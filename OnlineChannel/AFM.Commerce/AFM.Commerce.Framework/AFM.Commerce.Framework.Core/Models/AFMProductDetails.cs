﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Framework.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.Models
{
    /// <summary>
    /// This class is a data model and encapsulates the properties for AlsoKnownAs,ItemCovers,Product Country mapping, Up Cross Sell, ECommOwned data
    /// </summary>
    public class AFMProductDetails
    {
        public AFMProductDetails()
        {
        }

        [DataMember]
        public ICollection<AFMItemCovers> ItemCovers { get; set; }

        [DataMember]
        public ICollection<AFMAlsoKnownAs> AlsoKnownAs { get; set; }

        [DataMember]
        public ICollection<AFMProductCountryMap> ProductCountryMap { get; set; }

        [DataMember]
        public ICollection<AFMUpCrossSell> UpCrossSell { get; set; }

        [DataMember]
        public ICollection<AFMECommOwnedData> ECommOwnedData { get; set; }

        [DataMember]
        public ICollection<AFMAdditionalDimensions> AdditionalDimensions { get; set; }

        [DataMember]
        public ICollection<AFMOrderQuantity> OrderQuantity { get; set; }

        [DataMember]
        public ICollection<AFMNapData> NapData { get; set; }

    }
}
    


