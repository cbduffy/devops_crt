﻿
using AFM.Commerce.Runtime.Services.Common;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AFM.Commerce.Runtime.Services.Models
{
  public static class DataModelExtensions
  {
    public static Customer MergeDatabaseCustomerIntoInputCustomer(Customer inputCustomer, Customer databaseCustomer)
    {
      ThrowIf.Null<Customer>(inputCustomer, "inputCustomer");
      ThrowIf.Null<Customer>(databaseCustomer, "databaseCustomer");
      inputCustomer.CustomerGroup = string.IsNullOrWhiteSpace(inputCustomer.CustomerGroup) ? databaseCustomer.CustomerGroup : inputCustomer.CustomerGroup;
      inputCustomer.CurrencyCode = string.IsNullOrWhiteSpace(inputCustomer.CurrencyCode) ? databaseCustomer.CurrencyCode : inputCustomer.CurrencyCode;
      inputCustomer.Addresses = (IList<Address>) DataModelExtensions.GetUpdatedCustomerAddresses(inputCustomer, databaseCustomer);
      return inputCustomer;
    }

    private static List<Address> GetUpdatedCustomerAddresses(Customer inputCustomer, Customer databaseCustomer)
    {
      ThrowIf.Null<Customer>(inputCustomer, "inputCustomer");
      ThrowIf.Null<Customer>(databaseCustomer, "databaseCustomer");
      List<Address> list = new List<Address>();
      IEnumerable<Address> enumerable = Enumerable.Where<Address>((IEnumerable<Address>) inputCustomer.Addresses, (Func<Address, bool>) (a => a.RecordId == 0L));
      bool flag = false;
      foreach (Address address in enumerable)
      {
        if (flag)
          address.IsPrimary = false;
        else
          flag = address.IsPrimary;
        address.PartyNumber = inputCustomer.PartyNumber;
        list.Add(address);
      }
      foreach (Address address1 in (IEnumerable<Address>) databaseCustomer.Addresses)
      {
        Address existingAddress = address1;
        if (existingAddress != null)
        {
          Address address2 = Enumerable.FirstOrDefault<Address>(Enumerable.Where<Address>((IEnumerable<Address>) inputCustomer.Addresses, (Func<Address, bool>) (addrs => addrs.RecordId == existingAddress.RecordId)));
          if (address2 != null)
          {
            existingAddress.AttentionTo = address2.AttentionTo;
            existingAddress.BuildingCompliment = address2.BuildingCompliment;
            existingAddress.City = address2.City;
            existingAddress.County = address2.County;
            existingAddress.Deactivate = address2.Deactivate;
            existingAddress.DistrictName = address2.DistrictName;
            existingAddress.Email = address2.Email;
            existingAddress.EmailContent = address2.EmailContent;
            existingAddress.IsPrivate = address2.IsPrivate;
            existingAddress.Name = address2.Name;
            existingAddress.Phone = address2.Phone;
            existingAddress.PhoneExt = address2.PhoneExt;
            existingAddress.Postbox = address2.Postbox;
            existingAddress.State = address2.State;
            existingAddress.Street = address2.Street;
            existingAddress.StreetNumber = address2.StreetNumber;
            existingAddress.TaxGroup = address2.TaxGroup;
            existingAddress.ThreeLetterISORegionName = address2.ThreeLetterISORegionName;
            existingAddress.TwoLetterISORegionName = address2.TwoLetterISORegionName;
            existingAddress.Url = address2.Url;
            existingAddress.AddressTypeValue = address2.AddressTypeValue;
            existingAddress.ZipCode = address2.ZipCode;
            if (flag)
              existingAddress.IsPrimary = false;
            else if (address2.IsPrimary)
            {
              if (existingAddress.IsPrimary != address2.IsPrimary)
              {
                flag = address2.IsPrimary;
                existingAddress.IsPrimary = address2.IsPrimary;
              }
            }
            else
              existingAddress.IsPrimary = address2.IsPrimary;

            //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
            //Assign extension properties as well
            if (address2 != null && address2.ExtensionProperties != null)
                existingAddress.ExtensionProperties = address2.ExtensionProperties;
            //NE - RxL
            list.Add(existingAddress);
          }
          else if (flag)
          {
              existingAddress.IsPrimary = false;
              list.Add(existingAddress);
          }
        }
      }
      return list;
    }
  }
}
