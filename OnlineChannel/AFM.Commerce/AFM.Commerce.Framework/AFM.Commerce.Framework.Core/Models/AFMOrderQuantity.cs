﻿using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.Models
{
    public class AFMOrderQuantity : CommerceEntity
    {
        private const string ProductRecordIdColumnName = "PRODUCTID";
        private const string ProductNumberColumnName = "PRODUCTNUMBER";
        private const string MultipleQuantityColumnName = "MULTIPLEQUANTITY";
        private const string LowestQuantityColumnName = "LOWESTQUANTITY";

        public AFMOrderQuantity()
            : base("AFMOrderQuantity")
        {
        }

        [DataMember]
        public long ProductRecordId
        {
            get
            {
                return (long)(this["PRODUCTID"] ?? (object)0);
            }
            set
            {
                this["PRODUCTID"] = (object)value;
            }

        }

        [DataMember]
        public string ProductNumber
        {
            get
            {
                return (string)(this["PRODUCTNUMBER"] ?? (object)string.Empty);
            }
            set
            {
                this["PRODUCTNUMBER"] = (object)value;
            }

        }

        [DataMember]
        public Decimal MultipleQuantity
        {
            get
            {
                return Convert.ToDecimal(this["MULTIPLEQUANTITY"] );
            }
            set
            {
                this["MULTIPLEQUANTITY"] = value;
            }

        }

        [DataMember]
        public Decimal LowestQuantity
        {
            get
            {
                return Convert.ToDecimal(this["LOWESTQUANTITY"]);
            }
            set
            {
                this["LOWESTQUANTITY"] = value;
            }

        }
    }
}
