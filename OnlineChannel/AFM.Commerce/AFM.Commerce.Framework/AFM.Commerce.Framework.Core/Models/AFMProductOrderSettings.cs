﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.Models
{
    public class AFMProductOrderSettings : CommerceEntity
    {
        public AFMProductOrderSettings()
            : base("ProductOrderSettings")
        {
        }

        private const string itemId = "ITEMID";
        private const string multipleQty = "MULTIPLEQTY";
        private const string lowestQty = "LOWESTQTY";
        private const string qtyPerBox = "QTYPERBOX";

        /// <summary>
        /// Identifier of the item
        /// </summary>
        public string ItemId
        {
            get { return this[itemId].ToString(); }
            set { this[itemId] = value; }
        }

        /// <summary>
        /// Item sold in multiples of qty
        /// </summary>
        public int MultipleQty
        {
            get { return Convert.ToInt32(Math.Round(decimal.Parse(this[multipleQty].ToString()))); }
            set { this[multipleQty] = value; }
        }

        /// <summary>
        /// Item lowest qty to be sold
        /// </summary>
        public int LowestQty
        {
            get { return Convert.ToInt32(Math.Round(decimal.Parse(this[lowestQty].ToString()))); }
            set { this[lowestQty] = value; }
        }

        public int QtyPerBox
        {
            get { return Convert.ToInt32(Math.Round(decimal.Parse(this[qtyPerBox].ToString()))); }
            set { this[qtyPerBox] = value; }
        } 
    }
}
