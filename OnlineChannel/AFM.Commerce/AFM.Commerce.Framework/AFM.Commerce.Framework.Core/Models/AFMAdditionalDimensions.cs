﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;


namespace AFM.Commerce.Framework.Core.Models
{
    [DataContract]
    public class AFMAdditionalDimensions : CommerceEntity
    {
        private const string ProductRecordIdColumnName = "PRODUCTID";
        private const string ProductNumberColumnName = "PRODUCTNUMBER";
        private const string AlternativeDimensionCMColumnName = "ALTERNATIVEDIMENSIONCM";
        private const string AlternativeDimensionInchColumnName = "ALTERNATIVEDIMENSIONINCH";
        private const string DepthCMColumnName = "DEPTHCM";
        private const string DepthINCHColumnName = "DEPTHINCH";
        private const string DimensionDescriptionColumnName = "DIMENSIONDESCRIPTION";
        private const string HeightCMColumnName = "HEIGHTCM";
        private const string HeightINCHColumnName = "HEIGHTINCH";
        private const string WidthCMColumnName = "WIDTHCM";
        private const string WidthINCHColumnName = "WIDTHINCH";

        public AFMAdditionalDimensions()
            : base("AFMAdditionalDimensions")
        {
        }

        [DataMember]
        public long ProductRecordId
        {
            get
            {
                return (long)(this["PRODUCTID"] ?? (object)0);
            }
            set
            {
                this["PRODUCTID"] = (object)value;
            }

        }

        [DataMember]
        public string ProductNumber
        {
            get
            {
                return (string)(this["PRODUCTNUMBER"] ?? (object)string.Empty);
            }
            set
            {
                this["PRODUCTNUMBER"] = (object)value;
            }

        }

        [DataMember]
        public Decimal AlternativeDimensionCM
        {
            get
            {
                return Convert.ToDecimal(this["ALTERNATIVEDIMENSIONCM"]);
            }
            set
            {
                this["ALTERNATIVEDIMENSIONCM"] = value;
            }

        }

        [DataMember]
        public Decimal AlternativeDimensionInch
        {
            get
            {
                return Convert.ToDecimal(this["ALTERNATIVEDIMENSIONINCH"]);
            }
            set
            {
                this["ALTERNATIVEDIMENSIONINCH"] = value;
            }

        }

        [DataMember]
        public Decimal DepthCM
        {
            get
            {
                return Convert.ToDecimal(this["DEPTHCM"]);
            }
            set
            {
                this["DEPTHCM"] = value;
            }

        }

        [DataMember]
        public Decimal DepthInch
        {
            get
            {
                return Convert.ToDecimal(this["DEPTHINCH"]);
            }
            set
            {
                this["DEPTHINCH"] = value;
            }

        }

        [DataMember]
        public string DimensionDescription
        {
            get
            {
                return (string)(this["DIMENSIONDESCRIPTION"] ?? (object)string.Empty);
            }
            set
            {
                this["DIMENSIONDESCRIPTION"] = (object)value;
            }

        }

        [DataMember]
        public Decimal HeightCM
        {
            get
            {
                return Convert.ToDecimal(this["HEIGHTCM"]);
            }
            set
            {
                this["HEIGHTCM"] = value;
            }

        }

        [DataMember]
        public Decimal HeightInch
        {
            get
            {
                return Convert.ToDecimal(this["HEIGHTINCH"]);
            }
            set
            {
                this["HEIGHTINCH"] = value;
            }

        }

        [DataMember]
        public Decimal WidthCM
        {
            get
            {
                return Convert.ToDecimal(this["WIDTHCM"]);
            }
            set
            {
                this["WIDTHCM"] = value;
            }

        }

        [DataMember]
        public Decimal WidthInch
        {
            get
            {
                return Convert.ToDecimal(this["WIDTHINCH"]);
            }
            set
            {
                this["WIDTHINCH"] = value;
            }

        }

    }
}
