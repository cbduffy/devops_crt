﻿namespace AFM.Commerce.Framework.Core.Models
{
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Commerce Entity to get the Affiliation Details
    /// IExtensibleDataObject
    /// </summary>
    [Serializable]
    [DataContract]
    public class AfmVendorAffiliation : CommerceEntity
    {
        public const string AffiliationIdColumn = "AffiliationId";
        public const string VendorIdColumn = "VendorId";
        public const string ShipToIdColumn = "ShipToId";
        public const string InvoiceVendorIdColumn = "InvoiceVendorId";
        public const string ItemIdColumn = "ItemId";


        public AfmVendorAffiliation()
            : base("AFMVendorAffiliation")
        {
        }

        /// <summary>
        /// Gets or sets the AffiliationID.
        /// </summary>
        [DataMember]
        public long AffiliationId
        {
            get
            {
                try
                {
                    return Convert.ToInt64(this[AffiliationIdColumn]);
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set { this[AffiliationIdColumn] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string VendorId
        {
            get { return this[VendorIdColumn].ToString(); }
            set { this[VendorIdColumn] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string InvoiceVendorId
        {
            get
            {
                try
                {
                    return this[InvoiceVendorIdColumn].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[InvoiceVendorIdColumn] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string ShipToId
        {
            get { return this[ShipToIdColumn].ToString(); }
            set { this[ShipToIdColumn] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string ItemId
        {
            get
            {
                if (this[ItemIdColumn] != DBNull.Value)
                    return Convert.ToString(this[ItemIdColumn]);
                else return string.Empty;
            }
            set { this[ItemIdColumn] = value; }
        }
    }
}
