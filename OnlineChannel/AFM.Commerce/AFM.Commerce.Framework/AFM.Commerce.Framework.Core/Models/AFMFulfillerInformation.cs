﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.Data;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System.Runtime.Serialization;

namespace AFM.Commerce.Framework.Core.Models
{
    [Serializable]
    [DataContract]
    public class AFMFulFillerInformation : CommerceEntity
    {
        public const string EdiIntegratedColumn = "EDIINTEGRATED";
        public const string ShipToColumn = "SHIPTO";
        public const string VendTypeColumn = "VENDTYPE";
        public const string AllowAutoInvoiceColumn = "ALLOWAUTOINVOICE";
        public const string CustServiceCloseTimeColumn = "CUSTSERVICECLOSETIME";
        public const string CustServiceCloseTime2_Column = "CUSTSERVICECLOSETIME2_";
        public const string CustServiceCloseTime3_Column = "CUSTSERVICECLOSETIME3_";
        public const string CustServiceCloseTime4_Column = "CUSTSERVICECLOSETIME4_";
        public const string CustServiceCloseTime5_Column = "CUSTSERVICECLOSETIME5_";
        public const string CustServiceCloseTime6_Column = "CUSTSERVICECLOSETIME6_";
        public const string CustServiceCloseTime7_Column = "CUSTSERVICECLOSETIME7_";
        public const string CustServiceOpenTimeColumn = "CUSTSERVICEOPENTIME";
        public const string CustServiceOpenTime2_Column = "CUSTSERVICEOPENTIME2_";
        public const string CustServiceOpenTime3_Column = "CUSTSERVICEOPENTIME3_";
        public const string CustServiceOpenTime4_Column = "CUSTSERVICEOPENTIME4_";
        public const string CustServiceOpenTime5_Column = "CUSTSERVICEOPENTIME5_";
        public const string CustServiceOpenTime6_Column = "CUSTSERVICEOPENTIME6_";
        public const string CustServiceOpenTime7_Column = "CUSTSERVICEOPENTIME7_";
        public const string FulfillerEmailColumn = "FULFILLEREMAIL";
        public const string FulfillerPhoneNumberColumn = "FULFILLERPHONENUMBER";
        public const string LicenseeAccountColumn = "LICENSEEACCOUNT";
        public const string StoreServiceCloseTimeColumn = "STORESERVICECLOSETIME";
        public const string StoreServiceCloseTime2_Column = "STORESERVICECLOSETIME2_";
        public const string StoreServiceCloseTime3_Column = "STORESERVICECLOSETIME3_";
        public const string StoreServiceCloseTime4_Column = "STORESERVICECLOSETIME4_";
        public const string StoreServiceCloseTime5_Column = "STORESERVICECLOSETIME5_";
        public const string StoreServiceCloseTime6_Column = "STORESERVICECLOSETIME6_";
        public const string StoreServiceCloseTime7_Column = "STORESERVICECLOSETIME7_";
        public const string StoreServiceOpenTimeColumn = "STORESERVICEOPENTIME";
        public const string StoreServiceOpenTime2_Column = "STORESERVICEOPENTIME2_";
        public const string StoreServiceOpenTime3_Column = "STORESERVICEOPENTIME3_";
        public const string StoreServiceOpenTime4_Column = "STORESERVICEOPENTIME4_";
        public const string StoreServiceOpenTime5_Column = "STORESERVICEOPENTIME5_";
        public const string StoreServiceOpenTime6_Column = "STORESERVICEOPENTIME6_";
        public const string StoreServiceOpenTime7_Column = "STORESERVICEOPENTIME7_";
        public const string RecIdColumn = "RECID";
        public const string DoNotUpdateFullfillerInfoColumn = "DONOTUPDATEFULLFILLERINFO";
        public const string AccountNumberColumn = "ACCOUNTNUM";
        public const string InvoiceAccountColumn = "INVOICEACCOUNT";


        public AFMFulFillerInformation()
            : base("AFMFulfillerInformation")
        {
        }


        [DataMember]
        public int EdiIntegrated
        {
            get { return Convert.ToInt32(this[EdiIntegratedColumn]); }
            set { this[EdiIntegratedColumn] = value; }
        }

        [DataMember]
        public string ShipTo
        {
            get
            {
                try
                {
                    return this[ShipToColumn].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[ShipToColumn] = value; }
        }


        [DataMember]
        public int VendType
        { 
            get { return Convert.ToInt32(this[VendTypeColumn]); }
            set { this[VendTypeColumn] = value; } 
        }

        

        [DataMember]
        public int AllowAutoInvoice
         {
            get { return Convert.ToInt32(this[AllowAutoInvoiceColumn]); }
            set { this[AllowAutoInvoiceColumn] = value; } 
         }

        [DataMember]
        public string CustServiceCloseTime
        {
             get
            {
                try
                {
                    return this[CustServiceCloseTimeColumn].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceCloseTimeColumn] = value; }
        }

         [DataMember]
        public string CustServiceCloseTime2_
        {
             get
            {
                try
                {
                    return this[CustServiceCloseTime2_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceCloseTime2_Column] = value; }
        }

         [DataMember]
        public string CustServiceCloseTime3_
        {
             get
            {
                try
                {
                    return this[CustServiceCloseTime3_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceCloseTime3_Column] = value; }
        }

         [DataMember]
        public string CustServiceCloseTime4_
        {
             get
            {
                try
                {
                    return this[CustServiceCloseTime4_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceCloseTime4_Column] = value; }
        }

         [DataMember]
        public string CustServiceCloseTime5_
        {
             get
            {
                try
                {
                    return this[CustServiceCloseTime5_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceCloseTime5_Column] = value; }
        }

         [DataMember]
        public string CustServiceCloseTime6_
        {
             get
            {
                try
                {
                    return this[CustServiceCloseTime6_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceCloseTime6_Column] = value; }
        }

         [DataMember]
        public string CustServiceCloseTime7_
        {
             get
            {
                try
                {
                    return this[CustServiceCloseTime7_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceCloseTime7_Column] = value; }
        }


        [DataMember]
        public string CustServiceOpenTime
        {
             get
            {
                try
                {
                    return this[CustServiceOpenTimeColumn].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceOpenTimeColumn] = value; }
        }

         [DataMember]
        public string CustServiceOpenTime2_
        {
             get
            {
                try
                {
                    return this[CustServiceOpenTime2_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceOpenTime2_Column] = value; }
        }

         [DataMember]
        public string CustServiceOpenTime3_
        {
             get
            {
                try
                {
                    return this[CustServiceOpenTime3_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceOpenTime3_Column] = value; }
        }

         [DataMember]
        public string CustServiceOpenTime4_
        {
             get
            {
                try
                {
                    return this[CustServiceOpenTime4_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceOpenTime4_Column] = value; }
        }

         [DataMember]
        public string CustServiceOpenTime5_
        {
             get
            {
                try
                {
                    return this[CustServiceOpenTime5_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceOpenTime5_Column] = value; }
        }

         [DataMember]
        public string CustServiceOpenTime6_
        {
             get
            {
                try
                {
                    return this[CustServiceOpenTime6_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceOpenTime6_Column] = value; }
        }

         [DataMember]
        public string CustServiceOpenTime7_
        {
             get
            {
                try
                {
                    return this[CustServiceOpenTime7_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[CustServiceOpenTime7_Column] = value; }
        }

         [DataMember]
        public string FulfillerEmail
        {
             get
            {
                try
                {
                    return this[FulfillerEmailColumn].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[FulfillerEmailColumn] = value; }
        }

         [DataMember]
        public string FulfillerPhoneNumber
        {
             get
            {
                try
                {
                    return this[FulfillerPhoneNumberColumn].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[FulfillerPhoneNumberColumn] = value; }
        }

        [DataMember]
        public int LicenseeAccount
        {
            get { return Convert.ToInt32(this[LicenseeAccountColumn]); }
            set { this[LicenseeAccountColumn] = value; }
        }

          [DataMember]
        public string StoreServiceCloseTime
        {
             get
            {
                try
                {
                    return this[StoreServiceCloseTimeColumn].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceCloseTimeColumn] = value; }
        }

         [DataMember]
        public string StoreServiceCloseTime2_
        {
             get
            {
                try
                {
                    return this[StoreServiceCloseTime2_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceCloseTime2_Column] = value; }
        }

         [DataMember]
        public string StoreServiceCloseTime3_
        {
             get
            {
                try
                {
                    return this[StoreServiceCloseTime3_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceCloseTime3_Column] = value; }
        }

         [DataMember]
        public string StoreServiceCloseTime4_
        {
             get
            {
                try
                {
                    return this[StoreServiceCloseTime4_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceCloseTime4_Column] = value; }
        }

         [DataMember]
        public string StoreServiceCloseTime5_
        {
             get
            {
                try
                {
                    return this[StoreServiceCloseTime5_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceCloseTime5_Column] = value; }
        }

         [DataMember]
        public string StoreServiceCloseTime6_
        {
             get
            {
                try
                {
                    return this[StoreServiceCloseTime6_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceCloseTime6_Column] = value; }
        }

         [DataMember]
        public string StoreServiceCloseTime7_
        {
             get
            {
                try
                {
                    return this[StoreServiceCloseTime7_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceCloseTime7_Column] = value; }
        }


        [DataMember]
        public string StoreServiceOpenTime
        {
             get
            {
                try
                {
                    return this[StoreServiceOpenTimeColumn].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceOpenTimeColumn] = value; }
        }

         [DataMember]
        public string StoreServiceOpenTime2_
        {
             get
            {
                try
                {
                    return this[StoreServiceOpenTime2_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceOpenTime2_Column] = value; }
        }

         [DataMember]
        public string StoreServiceOpenTime3_
        {
             get
            {
                try
                {
                    return this[StoreServiceOpenTime3_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceOpenTime3_Column] = value; }
        }

         [DataMember]
        public string StoreServiceOpenTime4_
        {
             get
            {
                try
                {
                    return this[StoreServiceOpenTime4_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceOpenTime4_Column] = value; }
        }

         [DataMember]
        public string StoreServiceOpenTime5_
        {
             get
            {
                try
                {
                    return this[StoreServiceOpenTime5_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceOpenTime5_Column] = value; }
        }

         [DataMember]
        public string StoreServiceOpenTime6_
        {
             get
            {
                try
                {
                    return this[StoreServiceOpenTime6_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceOpenTime6_Column] = value; }
        }

         [DataMember]
        public string StoreServiceOpenTime7_
        {
             get
            {
                try
                {
                    return this[StoreServiceOpenTime7_Column].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[StoreServiceOpenTime7_Column] = value; }
        }

        [DataMember]
        public long RecId
        {
            get
            {
                try
                {
                    return Convert.ToInt64(this[RecIdColumn]);
                }
                catch (Exception)
                {
                    return 0;
                }
            }
            set { this[RecIdColumn] = value; }
   
        }

        [DataMember]
        public int DoNotUpdateFulfillerInfo
        {
            get { return Convert.ToInt32(this[DoNotUpdateFullfillerInfoColumn]); }
            set { this[DoNotUpdateFullfillerInfoColumn] = value; }
        }


        [DataMember]
        public string AccountNumber
        {
            get
            {
                try
                {
                    return this[AccountNumberColumn].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[AccountNumberColumn] = value; }
        }

        [DataMember]
        public string InvoiceAccount
        {
            get
            {
                try
                {
                    return this[InvoiceAccountColumn].ToString();
                }
                catch (Exception)
                {

                    return string.Empty;
                }

            }
            set { this[InvoiceAccountColumn] = value; }
        }
    }
}
