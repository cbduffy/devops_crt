﻿using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.Models
{
    public class AFMConfirmationNumberMaskData : CommerceEntity
    {



        private const string ChannelRecIdColumnName = "RetailOnlineChannelTable";
        private const string MaskColumnName = "Mask";
        private const string TransactionReceiptIdName = "RetailReceiptTransType";
        private const string OperatingUnitName = "OMOperatingUnitNumber";


        public AFMConfirmationNumberMaskData()
            : base("ConfirmationNumberMaskData")
        {
        }

        [DataMember]
        public long ChannelRecordId
        {
            get
            {
                return (long)(this["RetailOnlineChannelTable"] ?? (object)0);
            }
            set
            {
                this["RetailOnlineChannelTable"] = (object)value;
            }

        }

        [DataMember]
        public string Mask
        {
            get
            {
                return (string)(this["Mask"] ?? (object)string.Empty);
            }
            set
            {
                this["Mask"] = (object)value;
            }

        }


        [DataMember]
        public int RetailReceiptTransType
        {
            get
            {
                return (int)(this["RetailReceiptTransType"] ?? (object)0);
            }
            set
            {
                this["RetailReceiptTransType"] = (object)value;
            }

        }

        [DataMember]
        public string OMOperatingUnit
        {
            get
            {
                return (string)(this["OMOperatingUnitNumber"] ?? (object)string.Empty);
            }
            set
            {
                this["OMOperatingUnitNumber"] = (object)value;
            }

        }

    }
}
