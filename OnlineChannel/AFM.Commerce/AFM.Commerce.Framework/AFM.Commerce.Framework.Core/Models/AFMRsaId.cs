﻿using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Runtime.Serialization;

namespace AFM.Commerce.Framework.Core.Models
{
    //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
    /// <summary>
    /// This class represents commerce entity containing RSA information of record.
    /// </summary>
    [Serializable]
    [DataContract]
    public class AFMRsaId : CommerceEntity
    {
        private const string RecordIdColumnName = "RECID";
        private const string RsaIdColumnName = "RSAID";

        public AFMRsaId()
            : base("AFMRsaId")
        {
        }

        [DataMember]
        public long RecId
        {
            get
            {
                return (long)(this[RecordIdColumnName] ?? (object)0);
            }
            set
            {
                this[RecordIdColumnName] = (object)value;
            }

        }

        [DataMember]
        public string RsaId
        {
            get
            {
                return (string)(this[RsaIdColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[RsaIdColumnName] = (object)value;
            }

        }
    }
    //NE - RxL
}
