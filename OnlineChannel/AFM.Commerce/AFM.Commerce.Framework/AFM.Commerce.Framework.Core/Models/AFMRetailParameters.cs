﻿using System;
using System.Runtime.Serialization;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.Models
{
    /// <summary>
    /// Stores all the Retail Parameters information.
    /// </summary>
    /// 
    [Serializable]
    [DataContract]
    public class AFMRetailParameters : CommerceEntity
    {
        private const string InforCodeDescriptionColumnName = "DESCRIPTION";
        private const string InforCodeIdColumnName = "INFOCODEID";


        public AFMRetailParameters()
            : base("AFMRetailParameters")
        {
        }

        [DataMember]
        public string InforCodeDescription
        {
            get
            {
                return (string)(this[InforCodeDescriptionColumnName] ?? string.Empty);
            }
            set
            {
                this[InforCodeDescriptionColumnName] = (object)value;
            }

        }

        [DataMember]
        public string InforCodeId
        {
            get
            {
                return (string)(this[InforCodeIdColumnName] ?? (object)string.Empty);
            }
            set
            {
                this[InforCodeIdColumnName] = (object)value;
            }

        }
    }
}