﻿using AFM.Commerce.Framework.Core.Models;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
    /// <summary>
    /// This class represents the repsonse recieved from GetRsaIdServiceRequest of AFMCartDataService of CRT.
    /// </summary>
    public class GetRsaIdServiceResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
        public GetRsaIdServiceResponse(List<AFMRsaId> rsaIds)
        {
            this.RsaIds = rsaIds;
        }

        /// <summary>
        /// List of RSA IDs fetched using AFMCartDataService.
        /// </summary>
        [DataMember]
        public List<AFMRsaId> RsaIds { get; set; }
    }
    //NE - RxL
}
