﻿using System.Runtime.Serialization;
using AFM.Commerce.Framework.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using System.Collections.Generic;


namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    public class AFMFulfillerInformationServiceResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
        public AFMFulfillerInformationServiceResponse(List<AFMFulFillerInformation> fulfillerInformation)
        {
            this.FulfillerInformation = fulfillerInformation;

        }
        [DataMember]
        public List<AFMFulFillerInformation> FulfillerInformation { get; set; }
    }
}
