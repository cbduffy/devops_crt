﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Framework.Core.Models;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    public sealed class DeliveryChargesAndModeResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShippingDetailsResponse"/> class.
        /// </summary>
        /// <param name="shippingDetails">The Shipping Details.</param>
        public DeliveryChargesAndModeResponse(List<AFMShippingDetails> shippingDetails)
        {
            this.ShippingDetails = shippingDetails;
        }

        /// <summary>
        /// Gets all the shipping Details.
        /// </summary>
        public List<AFMShippingDetails> ShippingDetails { get; set; }
    }
}

