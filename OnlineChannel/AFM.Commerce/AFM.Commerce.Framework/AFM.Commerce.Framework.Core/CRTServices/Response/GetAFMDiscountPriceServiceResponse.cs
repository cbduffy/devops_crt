﻿namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    public sealed class GetAFMDiscountPriceServiceResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
        public GetAFMDiscountPriceServiceResponse(SalesTransaction trans)
        {
            this.SalesTransactionData = trans;
        }

        /// <summary>
        /// Gets the response transaction
        /// </summary>
        public SalesTransaction SalesTransactionData { get; set; }
    }
}
