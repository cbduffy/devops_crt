﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/27/2014*/


namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    public sealed class UpdateCartDataResponse : Response
    {
        public UpdateCartDataResponse(bool result)
        {
            this.Result = result;
        }

        /// <summary>
        /// Gets all the response Details.
        /// </summary>
        public bool Result { get; private set; }
    }
}
