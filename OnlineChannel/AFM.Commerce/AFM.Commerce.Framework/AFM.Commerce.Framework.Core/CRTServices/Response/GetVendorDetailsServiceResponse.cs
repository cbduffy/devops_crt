﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/27/2014*/

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Models;
    using Microsoft.Dynamics.Commerce.Runtime.Services;

    /// <summary>
    /// Represents the response object for the <see cref="GetVendorDetailsServiceRequest"/> class.
    /// </summary>
    public sealed class GetVendorDetailsServiceResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="GetPromotionsServiceResponse"/> class.
        /// </summary>
        /// <param name="vendor details">The Vendor Details.</param>
        public GetVendorDetailsServiceResponse(IEnumerable<AFMVendorDetails> vendorDetails)
        {
            this.VendorDetails = vendorDetails.AsReadOnly();
        }

        public GetVendorDetailsServiceResponse(IEnumerable<AFMItemFlags> itemFlagDetails)
        {
            this.ProductFlagDetail = itemFlagDetails.AsReadOnly();
        }

        /// <summary>
        /// Gets all the vendor Details.
        /// </summary>
        public ReadOnlyCollection<AFMVendorDetails> VendorDetails { get; private set; }

        public ReadOnlyCollection<AFMItemFlags> ProductFlagDetail { get; private set; } 
   
    }
}
