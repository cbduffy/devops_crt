﻿/*NS Developed by muthait for AFM_TFS_40686 dated 07/26/2014*/



namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Models;
    using Microsoft.Dynamics.Commerce.Runtime.Services;

    /// <summary>
    /// Represents the response object for the <see cref="GetVendorDetailsServiceRequest"/> class.
    /// </summary>
    public sealed class GetPrdtNAPResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="GetPrdtNAPResponse"/> class.
        /// </summary>
        /// <param name="vendor details">The Vendor Details.</param>
        public GetPrdtNAPResponse(List<AFMPrdtNAP> prdtNAPList)
        {
            this.PrdtNAPList = prdtNAPList;
        }

        /// <summary>
        /// Gets all the vendor Details.
        /// </summary>
        public List<AFMPrdtNAP> PrdtNAPList { get; private set; }
   
    }
}
