﻿namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    using AFM.Commerce.Framework.Core.Models;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    public class GetAFMAffiliationServiceResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
        public GetAFMAffiliationServiceResponse()
            : base()
        {
        }

        public GetAFMAffiliationServiceResponse(AfmVendorAffiliation afmAffiliation)
        {
            this.AfmAffiliation = afmAffiliation;
        }

        public GetAFMAffiliationServiceResponse(IEnumerable<AfmVendorAffiliation> affiliations)
        {
            this.AfmAffiliations = affiliations.AsReadOnly();
        }

        [DataMember]
        public AfmVendorAffiliation AfmAffiliation { get; set; }

        [DataMember]
        public ReadOnlyCollection<AfmVendorAffiliation> AfmAffiliations { get; set; }

        [DataMember]
        public long AfmDefaultAffiliationId { get; set; }

    }
}
