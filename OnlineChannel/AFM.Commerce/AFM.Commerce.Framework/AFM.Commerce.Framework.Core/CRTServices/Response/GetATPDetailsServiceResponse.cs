﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Entities;
using Microsoft.Dynamics.Commerce.Runtime.Services;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    public sealed class GetATPDetailsServiceResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="GetATPDetailsServiceResponse"/> class.
        /// </summary>
        /// <param name="itemGroupings">The Shipping Details.</param>
        public GetATPDetailsServiceResponse(List<AFMATPItemGroupingResponse> itemGroupings)
         {
             this.ItemGroupings = itemGroupings;
         }
        /// <summary>
        /// Gets or sets item Groupings
        /// </summary>
        public List<AFMATPItemGroupingResponse> ItemGroupings
        {
            get;
            set;
        }
    }
}
