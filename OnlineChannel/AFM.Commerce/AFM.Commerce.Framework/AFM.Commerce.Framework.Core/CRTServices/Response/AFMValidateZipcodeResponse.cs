﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Framework.Core.Models;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    public sealed class AFMValidateZipcodeResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AFMGetAddressStateResponse"/> class.
        /// </summary>
        /// <param name="shippingDetails">The AFMGetAddressStateResponse.</param>
        public AFMValidateZipcodeResponse(bool isValidZipcode)
        {
            this.IsValidZipcode = isValidZipcode;
        }

        /// <summary>
        /// Gets all the AddressStateList.
        /// </summary>
        public bool IsValidZipcode { get; set; }
    }
}
