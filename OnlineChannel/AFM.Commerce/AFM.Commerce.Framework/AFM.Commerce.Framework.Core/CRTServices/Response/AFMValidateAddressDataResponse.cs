﻿using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    [DataContract]
    public sealed class AFMValidateAddressDataResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
        public AFMValidateAddressDataResponse()
        {

        }
        //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
        public AFMValidateAddressDataResponse(bool isAddressValid, string errorMessage, AddressValidationDll.DataModels.Address suggestedAddress, bool isServiceVerified)
        {
            this.IsAddressValid = isAddressValid;
            this.ErrorMessage = errorMessage;
            this.addressValidationSuggestedAddress = suggestedAddress;
            this.IsServiceVerified = isServiceVerified;
           
        }
        //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore

        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public bool IsAddressValid { get; set; }       

        //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
        [DataMember]
        public AddressValidationDll.DataModels.Address addressValidationSuggestedAddress { get; set; }
        //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore

        //CR 306 - Address validation off - CS Developed by spriya Dated 09/14/2015
        [DataMember]
        public bool IsServiceVerified { get; set; }

        //CR 306 - Address validation off - CE Developed by spriya Dated 09/14/2015
    }
}
