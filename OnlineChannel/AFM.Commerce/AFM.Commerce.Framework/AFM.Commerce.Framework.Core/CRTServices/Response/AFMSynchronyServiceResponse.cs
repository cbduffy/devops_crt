﻿using AFM.Commerce.Framework.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    //NS by spriya : DMND0010113 - Synchrony Financing online
    public class AFMSynchronyServiceResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {

        public AFMSynchronyServiceResponse(List<AFMSynchronyOptions> synchronyOptions)
        {
            this.synchronyOptions = synchronyOptions;
        }
        [DataMember]
        public List<AFMSynchronyOptions> synchronyOptions { get; set; }
    }
    //NE by spriya : DMND0010113 - Synchrony Financing online
}
