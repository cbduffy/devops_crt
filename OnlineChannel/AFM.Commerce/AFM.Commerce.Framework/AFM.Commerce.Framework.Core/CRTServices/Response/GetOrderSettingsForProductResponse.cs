﻿using System.Collections.Generic;
using AFM.Commerce.Framework.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime.Messages;
using Microsoft.Dynamics.Commerce.Runtime.Services;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    public sealed class GetOrderSettingsForProductResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
        /// <summary>
        /// Gets the order settings for the product.
        /// </summary>
        public List<AFMProductOrderSettings> ProductOrderSettingsList { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GetOrderSettingsForProductResponse"/> class.
        /// </summary>
        /// <param name="productOrderSettingsList"></param>
        public GetOrderSettingsForProductResponse(List<AFMProductOrderSettings> productOrderSettingsList)
        {
            this.ProductOrderSettingsList = productOrderSettingsList;
        }
    }
}
