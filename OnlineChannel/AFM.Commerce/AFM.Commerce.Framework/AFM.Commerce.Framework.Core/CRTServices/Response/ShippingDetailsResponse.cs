﻿/*NS Developed by ysrini for AFM_TFS_40686 dated 06/29/2014*/

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    using AFM.Commerce.Framework.Core.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public sealed class ShippingDetailsResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShippingDetailsResponse"/> class.
        /// </summary>
        /// <param name="shippingDetails">The Shipping Details.</param>
        public ShippingDetailsResponse(AFMShippingDetails shippingDetails)
        {
            this.ShippingDetails = shippingDetails;
        }

        /// <summary>
        /// Gets all the shipping Details.
        /// </summary>
        public AFMShippingDetails ShippingDetails { get; set; }
    }
}
