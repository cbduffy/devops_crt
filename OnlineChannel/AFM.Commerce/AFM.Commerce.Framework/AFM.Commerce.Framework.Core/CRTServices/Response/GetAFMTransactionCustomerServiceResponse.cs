﻿using Microsoft.Dynamics.Commerce.Runtime.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    public class GetAFMTransactionCustomerServiceResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {

        public GetAFMTransactionCustomerServiceResponse(bool result)
        {
            this.Result = result;
        }

        /// <summary>
        /// Gets all the response Details.
        /// </summary>
        public bool Result { get; private set; }
    }

    }

