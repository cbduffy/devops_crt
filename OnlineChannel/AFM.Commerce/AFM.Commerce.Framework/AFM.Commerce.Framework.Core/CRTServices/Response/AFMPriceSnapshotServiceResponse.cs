﻿using System.Runtime.Serialization;
using AFM.Commerce.Framework.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using System.Collections.Generic;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    public class AFMPriceSnapshotServiceResponse
    {
        public AFMPriceSnapshotServiceResponse(List<AFMItemPriceSnapshot> itemPriceSnapshot)
        {
            this.ItemPriceSnapshot = itemPriceSnapshot;

        }
        [DataMember]
        public List<AFMItemPriceSnapshot> ItemPriceSnapshot { get; set; }
    }
}
