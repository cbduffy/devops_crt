﻿/*NS by muthait dated 02Jan2015*/
namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    public sealed class CloneCartDataResponse : Response
    {
        public CloneCartDataResponse(string cartId)
        {
            this.CartID = cartId;
        }

        /// <summary>
        /// Gets all the response Details.
        /// </summary>
        public string CartID { get; private set; }
    }
}

