﻿
using System.Runtime.Serialization;
using AFM.Commerce.Framework.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime.Services;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    public class AFMProductDetailsServiceResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {
        public AFMProductDetailsServiceResponse(AFMProductDetails prodDetails)
        {
            this.ProductDetails = prodDetails;

        }
        [DataMember]
        public AFMProductDetails ProductDetails { get; set; }


    }
}
