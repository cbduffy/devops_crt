﻿using AFM.Commerce.Framework.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.CRTServices.Response
{
    public class GetAFMRetailParametersServiceResponse : Microsoft.Dynamics.Commerce.Runtime.Messages.Response
    {

        public GetAFMRetailParametersServiceResponse(AFMRetailParameters infoCodeId)
        {
            this.InfoCodeId = infoCodeId;
        }

        /// <summary>
        /// Gets all the response Details.
        /// </summary>
        public AFMRetailParameters InfoCodeId { get; private set; }
    }

    }

