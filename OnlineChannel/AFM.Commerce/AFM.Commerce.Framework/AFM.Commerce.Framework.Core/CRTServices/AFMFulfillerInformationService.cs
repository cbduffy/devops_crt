﻿using System;
using System.Globalization;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.DataManagers;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using System.Collections.Generic;

namespace AFM.Commerce.Framework.Core.CRTServices
{
    public class AFMFulfillerInformationService : IRequestHandler
    {

        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(AFMFulfillerInformationServiceRequest)
                };
            }
        }


        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestedType = request.GetType();

            if (requestedType == typeof(AFMFulfillerInformationServiceRequest))
            {
                return (AFMFulfillerInformationServiceResponse)GetAllFulfillerInformation((AFMFulfillerInformationServiceRequest)request);
            }
            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));

        }
        /// <summary>
        /// This method calls method from Data manager class
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AFMFulfillerInformationServiceResponse GetAllFulfillerInformation(AFMFulfillerInformationServiceRequest request)
        {
            var dataManager = new AFMFulfillerInformationDataManager(request.RequestContext);
            return new AFMFulfillerInformationServiceResponse(dataManager.GetAllFulfillerInformation());
        }


    }
}
