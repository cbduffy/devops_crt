﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/30/2014*/
namespace AFM.Commerce.Framework.Core.CRTServices
{

    using System;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.DataManagers;
    using AFM.Commerce.Entities;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using System.Collections.Generic;

    /// <summary>
    /// Service class responsible for serving Transaction Lines requests.
    /// </summary>
    public class AFMTransLineService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(SetVendorIdServiceRequest)
                };
            }
        }

        /// <summary>
        /// Executes the request and returns response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            Type requestedType = request.GetType();

            if (requestedType == typeof(SetVendorIdServiceRequest))
            {
                return (SetVendorIdServiceResponse) UpdateAFMSalesTransLines((SetVendorIdServiceRequest)request);
            }

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }
       
        public SetVendorIdServiceResponse UpdateAFMSalesTransLines(SetVendorIdServiceRequest request)
        {
            var dataManager = new AFMTransLineDataManager(request.RequestContext);
            AFMSalesTransItems afmSalesTransItems = new AFMSalesTransItems();
            afmSalesTransItems.OrderNumber = request.OrderNumber;
            afmSalesTransItems.LineNumber = request.LineNumber;
            afmSalesTransItems.VendorId = request.VendorId;
            afmSalesTransItems.KitItemId = request.KitItemId;
            afmSalesTransItems.KitProductId = request.KitProductId;
            afmSalesTransItems.KitItemProductDetails = request.KitItemProductDetails;
            afmSalesTransItems.KitSequenceNumber = request.KitSequenceNumber;
            afmSalesTransItems.KitItemQuantity = request.KitItemQuantity;
            afmSalesTransItems.BeginDateRange = request.BeginDateRange;
            afmSalesTransItems.BestDate = request.BestDate;
            afmSalesTransItems.EndDateRange = request.EndDateRange;
            afmSalesTransItems.EarliestDeliveryDate = request.EarliestDeliveryDate;
            afmSalesTransItems.LatestDeliveryDate = request.LatestDeliveryDate;
            afmSalesTransItems.IsAvailable = request.IsAvailable;
            afmSalesTransItems.Message = request.Message;
            afmSalesTransItems.LineConfirmationID = request.LineConfirmationID;
            afmSalesTransItems.AFMItemType = request.AFMItemType;
            // HXM To resolve 67447 Color and Size not populating
            afmSalesTransItems.Color = request.Color;
            afmSalesTransItems.Size = request.Size;
            afmSalesTransItems.ShippingMode = request.ShippingMode;

            //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
            afmSalesTransItems.BeginDateRangeMOD = request.BeginDateRangeMOD;
            afmSalesTransItems.BestDateMOD = request.BestDateMOD;
            afmSalesTransItems.EndDateRangeMOD = request.EndDateRangeMOD;
            afmSalesTransItems.EarliestDeliveryDateMOD = request.EarliestDeliveryDateMOD;
            afmSalesTransItems.LatestDeliveryDateMOD = request.LatestDeliveryDateMOD;
            afmSalesTransItems.MessageMOD = request.MessageMOD;
            //NE by RxL

            bool resultMessage = dataManager.UpdateAFMSalesTransLines(afmSalesTransItems);
            return new SetVendorIdServiceResponse(resultMessage);
        }

    }
}
