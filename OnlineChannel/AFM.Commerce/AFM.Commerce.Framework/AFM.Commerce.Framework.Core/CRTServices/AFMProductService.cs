﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/30/2014*/
/*CE Added methods for CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box */

namespace AFM.Commerce.Framework.Core.CRTServices
{
    using System;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.DataManagers;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using System.Collections.Generic;

    /// <summary>
    /// Service class responsible for serving Product requests.
    /// </summary>
    public class AFMProductService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetPrdtNAPRequest),
                    typeof(GetOrderSettingsForProductRequest)
                };
            }
        }


        /// <summary>
        /// Executes the request and returns response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            Type reqType = request.GetType();
            if (reqType == typeof(GetPrdtNAPRequest))
            {
                return (GetPrdtNAPResponse) GetPrdtNAPListData((GetPrdtNAPRequest) request);
            }
            if (reqType == typeof(GetOrderSettingsForProductRequest))
            {
                return (GetOrderSettingsForProductResponse) GetOrderSettingsData((GetOrderSettingsForProductRequest)request);
            }

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }

        public GetPrdtNAPResponse GetPrdtNAPListData(GetPrdtNAPRequest request)
        {
            var dataManager = new AFMProductDataManager(request.RequestContext);
            var PrdtNAPList = dataManager.GetPrdtNAPList(request.ProductIds,DateTime.Now);
            return new GetPrdtNAPResponse(PrdtNAPList);
        }


        public GetOrderSettingsForProductResponse GetOrderSettingsData(GetOrderSettingsForProductRequest request)
        {
            var dataManager = new AFMProductDataManager(request.RequestContext);
            var productOrderSettingsList = dataManager.GetOrderSettingsForProduct(request.ItemIds);
            return new GetOrderSettingsForProductResponse(productOrderSettingsList);
        }
    }
}
