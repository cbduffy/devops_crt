﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/30/2014*/

namespace AFM.Commerce.Framework.Core.CRTServices
{
    using System;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.DataManagers;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using System.Collections.Generic;
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.Models;

    /// <summary>
    /// Service class responsible for serving Vendor Details requests.
    /// </summary>
    public class AFMVendorDetailsService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetVendorDetailsServiceRequest),
                    typeof(GetAFIVendorDetailsServiceRequest),
                    typeof(GetZipCodeVendorDetailsServiceRequest),
                    typeof(GetItemEcomDetailServiceRequest)
                };
            }
        }
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestedType = request.GetType();
            if (requestedType == typeof(GetVendorDetailsServiceRequest))
            {
                var serviceRequest = (GetVendorDetailsServiceRequest)request;

                AFMItemFlags itemFlagInfo = GetEcommOwnedStatus(serviceRequest);
                serviceRequest.isEcomOwned = Convert.ToBoolean(itemFlagInfo.IsEcommOwned);
                serviceRequest.IsSDSO = Convert.ToBoolean(itemFlagInfo.IsSDSO);
                if (Convert.ToBoolean(itemFlagInfo.IsEcommOwned))
                {
                    return GetVendorDetailsforEComOwnedItem(serviceRequest);
                }
                else
                {
                    return GetVendorDetailsbyZipCode(serviceRequest);
                }
            }
            else if (requestedType == typeof(GetAFIVendorDetailsServiceRequest))
            {
                var serviceRequest = (GetAFIVendorDetailsServiceRequest)request;
                return GetVendorDetailsforEComOwnedItem(serviceRequest);
            }
            else if (requestedType == typeof(GetZipCodeVendorDetailsServiceRequest))
            {
                var serviceRequest = (GetZipCodeVendorDetailsServiceRequest)request;
                return GetVendorDetailsbyZipCode(serviceRequest);
            }
            else if (requestedType == typeof(GetItemEcomDetailServiceRequest))
            {
                var serviceRequest = (GetItemEcomDetailServiceRequest)request;
                return GetEcomItemDetail(serviceRequest);
            }

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }

        public AFMItemFlags GetEcommOwnedStatus(GetVendorDetailsServiceRequest request)
        {
            var dataManager = new AFMVendorDetailsDataManager(request.RequestContext);
            AFMItemFlags itemFlagInfo = dataManager.GetEcommOwnedStatus(request.ItemId);
            return itemFlagInfo;
        }

        public GetVendorDetailsServiceResponse GetVendorDetailsbyZipCode(GetVendorDetailsServiceRequest request)
        {
            var dataManager = new AFMVendorDetailsDataManager(request.RequestContext);
            var vendorDetails = dataManager.GetVendorDetailsbyZipCode(request.ZipCode, request.ItemId);
            return new GetVendorDetailsServiceResponse(vendorDetails);
        }

        public GetVendorDetailsServiceResponse GetVendorDetailsbyZipCode(GetZipCodeVendorDetailsServiceRequest request)
        {
            var dataManager = new AFMVendorDetailsDataManager(request.RequestContext);
            var vendorDetails = dataManager.GetVendorDetailsbyZipCode(request.ZipCode, "");
            return new GetVendorDetailsServiceResponse(vendorDetails);
        }

        public GetVendorDetailsServiceResponse GetVendorDetailsforEComOwnedItem(GetAFIVendorDetailsServiceRequest request)
        {
            var dataManager = new AFMVendorDetailsDataManager(request.RequestContext);
            var vendorDetails = dataManager.GetVendorDetailsforEComOwnedItem();
            return new GetVendorDetailsServiceResponse(vendorDetails);
        }

        public GetVendorDetailsServiceResponse GetVendorDetailsforEComOwnedItem(GetVendorDetailsServiceRequest request)
        {
            var dataManager = new AFMVendorDetailsDataManager(request.RequestContext);
            var vendorDetails = dataManager.GetVendorDetailsforEComOwnedItem();
            return new GetVendorDetailsServiceResponse(vendorDetails);
        }

        public GetVendorDetailsServiceResponse GetEcomItemDetail(GetItemEcomDetailServiceRequest request)
        {
            var dataManager = new AFMVendorDetailsDataManager(request.RequestContext);
            var itemFlagInfo = dataManager.GetEomOwnedItemDetail(request.ProductIds.ToList());

            return new GetVendorDetailsServiceResponse(itemFlagInfo.AsEnumerable());
        }
    }
}
