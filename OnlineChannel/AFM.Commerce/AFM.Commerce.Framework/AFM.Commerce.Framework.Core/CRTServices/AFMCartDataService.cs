﻿using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.DataManagers;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using System;
using System.Collections.Generic;

namespace AFM.Commerce.Framework.Core.CRTServices
{
    //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
    /// <summary>
    /// This class handles the Requests corresponding  to card data and returns the response
    /// </summary>
    public class AFMCartDataService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this service handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetRsaIdServiceRequest),
                    typeof(AFMSynchronyServiceRequest)
                };
            }
        }

        /// <summary>
        /// Execute the method corrpsonding to request type and return the response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestedType = request.GetType();

            if (requestedType == typeof(GetRsaIdServiceRequest))
            {
                return (GetRsaIdServiceResponse)GetRsaIds((GetRsaIdServiceRequest)request);
            }
            if (requestedType == typeof(AFMSynchronyServiceRequest))
            {
                return (AFMSynchronyServiceResponse)GetSynchronyOptions((AFMSynchronyServiceRequest)request);
            }
            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }

        /// <summary>
        /// This method calls method calls Data Manger to fetch the RSA Ids and return the list in response object
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetRsaIdServiceResponse GetRsaIds(GetRsaIdServiceRequest request)
        {
            var dataManager = new AFMCartDataManager(request.RequestContext);
            return new GetRsaIdServiceResponse(dataManager.GetRsaIds(request.RsaId));
        }
        //NE - RxL
        //NS by spriya : DMND0010113 - Synchrony Financing online
        public AFMSynchronyServiceResponse GetSynchronyOptions(AFMSynchronyServiceRequest request)
        {
            var dataManager = new AFMCartDataManager(request.RequestContext);
            return new AFMSynchronyServiceResponse(dataManager.GetSynchronyOptions());
        }
        //NE by spriya : DMND0010113 - Synchrony Financing online
    }
    
}
