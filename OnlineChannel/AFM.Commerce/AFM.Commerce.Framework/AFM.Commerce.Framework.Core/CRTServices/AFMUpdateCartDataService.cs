﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/30/2014*/
namespace AFM.Commerce.Framework.Core.CRTServices
{

    using System;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.DataManagers;
    using AFM.Commerce.Entities;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using System.Collections.Generic;

    /// <summary>
    /// Service class responsible for serving Transaction Lines requests.
    /// </summary>
    public class AFMUpdateCartDataService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(UpdateCartDataRequest),
                    typeof(CloneCartDataRequest)
                };
            }
        }

        /// <summary>
        /// Executes the request and returns response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            Type requestedType = request.GetType();

            if (requestedType == typeof(UpdateCartDataRequest))
            {
                return (UpdateCartDataResponse)UpdateCartData((UpdateCartDataRequest)request);
            }
            if (requestedType == typeof(CloneCartDataRequest))
            {
                return (CloneCartDataResponse)UpdateCartData((CloneCartDataRequest)request);
            }
            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }

        public UpdateCartDataResponse UpdateCartData(UpdateCartDataRequest request)
        {
            var dataManager = new AFMCartDataManager(request.RequestContext);
            bool resultMessage = dataManager.UpdateSalesTransaction(request.IsDelete, request.CartId);
            return new UpdateCartDataResponse(resultMessage);
        }

        public CloneCartDataResponse UpdateCartData(CloneCartDataRequest request)
        {
            var dataManager = new AFMCartDataManager(request.RequestContext);
            var cartID = dataManager.CloneSalesTransaction(request.NewCartId, request.SourceCartId);
            return new CloneCartDataResponse(cartID);
        }

    }
}
