﻿namespace AFM.Commerce.Framework.Core.CRTServices
{

    using System;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Service class responsible for serving discounts requests.
    /// </summary>
    public class AFMGetDiscountPriceService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetAFMDiscountPriceServiceRequest)
                };
            }
        }
       // Executes the request and returns response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type reqType = request.GetType();
            if (reqType == typeof(GetAFMDiscountPriceServiceRequest))
            {
                return GetDiscountPrice((GetAFMDiscountPriceServiceRequest)request);
            }

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }


        public static GetAFMDiscountPriceServiceResponse GetDiscountPrice(GetAFMDiscountPriceServiceRequest request)
        {
            SalesTransaction salesTransactionData = request.SalesTransactionData;
            PricingService pricingService = new PricingService();
            request.RequestContext.SetSalesTransaction(salesTransactionData);
            //CalculateDiscountsServiceRequest requestData = new CalculateDiscountsServiceRequest(request.Context, salesTransactionData, DiscountCalculationMode.CalculateOffer);
            GetIndependentPriceDiscountServiceRequest requestData = new GetIndependentPriceDiscountServiceRequest(salesTransactionData);
            GetPriceServiceResponse responseData = request.requestContext.Runtime.Execute <GetPriceServiceResponse>(requestData, request.requestContext);
            //CalculateDiscount(request.Context, salesTransactionData, DiscountCalculationMode.CalculateOffer);
            return new GetAFMDiscountPriceServiceResponse(responseData.Transaction);
        }
      
    }
}
