﻿using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.DataManagers;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.CRTServices
{

   public class AFMTransactionCustomerService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetAFMTransactionCustomerServiceRequest)
                };
            }
        }

        /// <summary>
        /// Executes the request and returns response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
           if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            Type requestedType = request.GetType();

            if (requestedType == typeof(GetAFMTransactionCustomerServiceRequest))
            {
                return UpdateTransactionCustomer((GetAFMTransactionCustomerServiceRequest) request);
            }

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }

        public GetAFMTransactionCustomerServiceResponse UpdateTransactionCustomer(GetAFMTransactionCustomerServiceRequest request)
        {
            var transactionCustomerManager = new AFMTransactionCustomerDataManager(request.RequestContext);
            bool resultMessage = transactionCustomerManager.UpdateAFMTransactionCustomerID(request.cctTransactionID, request.sctTransactionID, request.customerId);
            return new GetAFMTransactionCustomerServiceResponse(resultMessage);
        }



    }
}
