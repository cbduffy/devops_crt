﻿using System;
using System.Globalization;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.DataManagers;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using System.Collections.Generic;


namespace AFM.Commerce.Framework.Core.CRTServices
{
    public class AFMPriceSnapshotService 
    {

        
        /// <summary>
        /// This method calls method from Data manager class
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AFMPriceSnapshotServiceResponse GetPriceSnapshot(AFMPriceSnapshotServiceRequest request)
        {
            var dataManager = new AFMPriceSnapshotDataManager();
            if (request.AffiliationId>0)
            return new AFMPriceSnapshotServiceResponse(dataManager.GetPriceSnapshot(request.ItemIds,request.AffiliationId));
            else
            return new AFMPriceSnapshotServiceResponse(dataManager.GetPriceSnapshot(request.ItemIds));
        }
    }
}
