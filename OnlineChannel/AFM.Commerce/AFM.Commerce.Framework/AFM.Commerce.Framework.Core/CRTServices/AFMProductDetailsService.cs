﻿using System;
using System.Globalization;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.DataManagers;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using System.Collections.Generic;

namespace AFM.Commerce.Framework.Core.CRTServices
{
    /// <summary>
    /// This is service class which gets called by the workflow and connects to data manager
    /// </summary>
    public class AFMProductDetailsService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(AFMProductDetailsServiceRequest)
                };
            }
        }
        // Executes the request and returns response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type reqType = request.GetType();
            if (reqType == typeof(AFMProductDetailsServiceRequest))
            {
                return (AFMProductDetailsServiceResponse) GetProductDetails((AFMProductDetailsServiceRequest)request);
            }

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }
        /// <summary>
        /// This method calls method from Data manager class
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public AFMProductDetailsServiceResponse GetProductDetails(AFMProductDetailsServiceRequest request)
        {
            var dataManager = new AFMProductDetailsDataManager(request.RequestContext);
            return new AFMProductDetailsServiceResponse(dataManager.SetProductDetails(request.ProductIds, DateTime.Today));
        }
    }
}
