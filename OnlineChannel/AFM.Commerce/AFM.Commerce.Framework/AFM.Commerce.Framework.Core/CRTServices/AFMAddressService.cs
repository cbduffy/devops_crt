﻿using System;
using System.Globalization;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.DataManagers;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using System.Collections.Generic;
using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Avalara.Ax.CRT;
using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System.Collections.ObjectModel;
using AFM.Commerce.Framework.Core.Models;
using AddressValidationDll.Service.Implementations;
using AddressValidationDll.DataModels;
using AFM.Commerce.Runtime.Services;

namespace AFM.Commerce.Framework.Core.CRTServices
{
    public class AFMAddressService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(AFMValidateAddressDataRequest),
                    typeof(AFMGetAddressStateRequest),
                    typeof(AFMValidateZipcodeRequest)
                };
            }
        }

        // Executes the request and returns response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type reqType = request.GetType();
            if (reqType == typeof(AFMGetAddressStateRequest))
            {
                return (AFMGetAddressStateResponse)GetAddressState((AFMGetAddressStateRequest)request);
            }
            if (reqType == typeof(AFMValidateZipcodeRequest))
            {
                return (AFMValidateZipcodeResponse)ValidateZipcode((AFMValidateZipcodeRequest)request);
            }
            //NS Developed by spriya
            if (reqType == typeof(AFMValidateAddressDataRequest))
            {
                return (AFMValidateAddressDataResponse)ValidateAddress((AFMValidateAddressDataRequest)request);
            }
            //NE Developed by spriya

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }


        //CS Developed by spriya
        private AFMValidateAddressDataResponse ValidateAddress(AFMValidateAddressDataRequest request)
        {                  
            //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
            ThrowIf.Null(request.AddressValidationAddress, CustomerConstants.Address); 
            if (string.IsNullOrWhiteSpace(request.AddressValidationAddress.Country))
            {
                return null;
            }            
            if(request.AddressValidationAddress.Type == AddressType.Invoice.ToString())            
            {
                var dataManager = new AFMAddressDataManager(request.RequestContext);               
                AFMZipCodeMasterData zipCodeMasterData = new AFMZipCodeMasterData(request.AddressValidationAddress.City, request.AddressValidationAddress.State, request.AddressValidationAddress.Zip.Substring(0, 5),
                CustomerConstants.ThreeLetterCountryCode, (AddressType)Enum.Parse(typeof(AddressType), CustomerConstants.Invoice, true));                 

                AFMAddressValidationData validatedAddressData = dataManager.GetAddressValidationData(zipCodeMasterData);
                if (validatedAddressData.ServiceByPassFlag)
                {
                    return new AFMValidateAddressDataResponse(validatedAddressData.IsZipCodeMasterValid, CustomerConstants.ZipCodeMaster, null, false);
                }
            }           
            AddressValidationService addressValidationServiceRequest = new AddressValidationService();
            AddressValidationResponse response = addressValidationServiceRequest.ValidateAddress(request.AddressValidationAddress);
            bool isValid = response.IsValid;
            string errorMsg = response.ErrorMsg;

            if (response.SuggestedAddress != null)
            {                
                if (!string.IsNullOrEmpty(response.SuggestedAddress.Type) && response.SuggestedAddress.Type.ToLower().Contains("p") && request.AddressValidationAddress.Type == AddressType.Delivery.ToString())
                {
                    isValid = false;
                    errorMsg = CustomerConstants.POError;
                }
            }            
             if (isValid)
            {
                if (response.SuggestedAddress != null && !string.IsNullOrEmpty(response.SuggestedAddress.Zip)
                    && request.AddressValidationAddress.Zip.ToLower() != response.SuggestedAddress.Zip.ToLower())
                {
                    if (request.AddressValidationAddress.Zip.ToLower().Split('-')[0] == response.SuggestedAddress.Zip.ToLower().Split('-')[0])
                    {
                        response.SuggestedAddress.Zip = response.SuggestedAddress.Zip.ToLower().Split('-')[0];
                    }
                }
            }            

            return new AFMValidateAddressDataResponse(isValid, errorMsg, response.SuggestedAddress, true);

            //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore           
        }            

        private AFMGetAddressStateResponse GetAddressState(AFMGetAddressStateRequest request)
        {
            var dataManager = new AFMAddressDataManager(request.RequestContext);
            return new AFMGetAddressStateResponse(dataManager.GetAddressState(request.CountryRegionId));
        }

        private AFMValidateZipcodeResponse ValidateZipcode(AFMValidateZipcodeRequest request)
        {
            var dataManager = new AFMAddressDataManager(request.RequestContext);
            return new AFMValidateZipcodeResponse(dataManager.ValidateZipCode(request.ZipCode, request.CountryRegionId));
        }
    }
}
