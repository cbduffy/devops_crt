﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/30/2014*/
namespace AFM.Commerce.Framework.Core.CRTServices
{

    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.DataManagers;
    using AFM.Commerce.Entities;

    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    //using Microsoft.Dynamics.Commerce.Runtime.Services.ShippingAdapters.FedEx;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Service class responsible for serving Transaction Lines requests.
    /// </summary>
    public class AFMTransHeaderService : IRequestHandler
    {

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(SetTransHeaderServiceRequest)
                };
            }
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="Response"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// </exception>
        /// <exception cref="NotSupportedException">
        /// </exception>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            Type requestedType = request.GetType();

            if (requestedType == typeof(SetTransHeaderServiceRequest))
            {
                var serviceRequest = (SetTransHeaderServiceRequest)request;
                return (SetTransHeaderServiceResponse)this.UpdateAFMSalesTransHeader(serviceRequest);
            }
            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }

        /// <summary>
        /// The update afm sales trans header.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="SetTransHeaderServiceResponse"/>.
        /// </returns>
        public SetTransHeaderServiceResponse UpdateAFMSalesTransHeader(SetTransHeaderServiceRequest request)
        {
            var dataManager = new AFMTransHeaderDataManager(request.RequestContext);
            AFMSalesTransHeader afmSalesTransHeader = request.transHeader;

            bool resultMessage = dataManager.UpdateSalesTransHeader(afmSalesTransHeader);
            return new SetTransHeaderServiceResponse(resultMessage);
        }

    }
}
