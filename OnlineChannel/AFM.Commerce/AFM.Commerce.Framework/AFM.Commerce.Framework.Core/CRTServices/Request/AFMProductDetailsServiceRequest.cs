﻿
using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Services;

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    public class AFMProductDetailsServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        public AFMProductDetailsServiceRequest()
        {
        }


        [DataMember]
        public List<long> ProductIds { get; set; }
    }
}
