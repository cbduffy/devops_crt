﻿using System.Runtime.Serialization;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using System;
using System.Collections.Generic;

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    [DataContract]
    public class GetAFMRetailParametersServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {

        public GetAFMRetailParametersServiceRequest()
        {
        }

    }
}
