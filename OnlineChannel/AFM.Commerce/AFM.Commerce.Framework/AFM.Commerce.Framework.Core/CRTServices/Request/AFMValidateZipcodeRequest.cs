﻿

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    
    
    [DataContract]
    public class AFMValidateZipcodeRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        [DataMember]
        public string CountryRegionId
        {
            get;
            set;
        }

        [DataMember]
        public string ZipCode
        {
            get;
            set;
        }
    }
}
