﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/28/2014*/

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;

    /// <summary>
    /// Get the request to retrieve all the promotions for a {Channel, Catalog} pair.
    /// </summary>
    [DataContract]
    public sealed class GetVendorDetailsServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GetVendorDetailsServiceRequest"/> class.
        /// </summary>
        /// <param name="context">The context of the request.</param>
        public GetVendorDetailsServiceRequest()
        {
        }

        /// <summary>
        /// Gets or sets the ZipCode identifier.
        /// </summary>
        [DataMember]
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the ItemId identifier.
        /// </summary>
        [DataMember]
        public string ItemId { get; set; }

        /// <summary>
        /// Gets or sets the isEcomOwned identifier.
        /// </summary>
        [DataMember]
        public bool isEcomOwned { get; set; }

        /// <summary>
        /// Gets or sets the isEcomOwned identifier.
        /// </summary>
        [DataMember]
        public string OperatingUnitNumber { get; set; }

        /// <summary>
        /// Gets or sets the isSDSO identifier.
        /// </summary>
        [DataMember]
        public bool IsSDSO { get; set; }
    }
}
