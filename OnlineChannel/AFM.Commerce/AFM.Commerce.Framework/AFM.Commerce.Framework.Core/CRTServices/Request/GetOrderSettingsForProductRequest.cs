﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.Services;

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    public class GetOrderSettingsForProductRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        public GetOrderSettingsForProductRequest()
        {
        }

        [DataMember]
        public List<ProductLookupClause> ItemIds { get; set; }

    }
}
