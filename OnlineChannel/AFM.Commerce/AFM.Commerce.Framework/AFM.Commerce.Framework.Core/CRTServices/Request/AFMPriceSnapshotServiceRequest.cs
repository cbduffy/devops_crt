﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Services;


namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    public class AFMPriceSnapshotServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        public AFMPriceSnapshotServiceRequest()
        {
        }


        [DataMember]
        public List<string> ItemIds { get; set; }

        [DataMember]
        public long AffiliationId { get; set; }
    }
}
