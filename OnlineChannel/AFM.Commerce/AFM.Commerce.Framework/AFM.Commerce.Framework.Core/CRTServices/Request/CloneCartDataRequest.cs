﻿/*NS by muthait dated 02JAn2015*/
namespace AFM.Commerce.Framework.Core.CRTServices.Request
{

    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using System;

    [DataContract]
    public sealed class CloneCartDataRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetVendorIdServiceRequest"/> class.
        /// </summary>
        /// <param name="context">The context of the request.</param>
        public CloneCartDataRequest()
        {
        }

        /// <summary>
        /// Gets or sets the CartId
        /// </summary>
        [DataMember]
        public string NewCartId { get; set; }

        /// <summary>
        /// Gets or sets the Is Delete property to update deleted time column in db for the cart
        /// </summary>
        [DataMember]
        public string SourceCartId { get; set; }


    }
}
