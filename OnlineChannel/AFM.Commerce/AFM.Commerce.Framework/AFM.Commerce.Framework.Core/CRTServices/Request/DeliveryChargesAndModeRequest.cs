﻿using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Framework.Core.Models;

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    [DataContract]
    public sealed class DeliveryChargesAndModeRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        public DeliveryChargesAndModeRequest()
        {

        }

        /// <summary>
        /// Gets or sets the ZipCode identifier.
        /// </summary>
        [DataMember]
        public List<AFMShippingDetails> CartItems { get; set; }
    }
}

