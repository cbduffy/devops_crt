﻿using AFM.Commerce.Entities;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    public class SetTransHeaderServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SetTransHeaderServiceRequest"/> class.
        /// </summary>
        public SetTransHeaderServiceRequest()
        {
        }

        /// <summary>
        /// Gets or sets the trans header.
        /// </summary>
        public AFMSalesTransHeader transHeader { get; set; }
    }
}
