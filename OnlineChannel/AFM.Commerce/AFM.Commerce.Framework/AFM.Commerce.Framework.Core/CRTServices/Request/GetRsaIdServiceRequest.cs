﻿
namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID    
    /// <summary>
    /// This class represents service request class to fetch RSA Ids using AFMCartDataService of CRT.
    /// </summary>
    public class GetRsaIdServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        public string RsaId { get; set; }

        public GetRsaIdServiceRequest(string rsaId)
        {
            RsaId = rsaId;
        }
    }
    //NE - RxL
}
