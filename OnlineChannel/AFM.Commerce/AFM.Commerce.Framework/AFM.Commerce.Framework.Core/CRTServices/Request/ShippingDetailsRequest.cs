﻿/*NS Developed by ysrini for AFM_TFS_40686 dated 06/27/2014*/

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;
    [DataContract]
    public sealed class ShippingDetailsRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        public ShippingDetailsRequest()
        {

        }

        /// <summary>
        /// Gets or sets the ZipCode identifier.
        /// </summary>
        [DataMember]
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the ItemId identifier.
        /// </summary>
        [DataMember]
        public string ItemId { get; set; }

        /// <summary>
        /// Gets or sets the isEcomOwned identifier.
        /// </summary>
        [DataMember]
        public string OperatingUnitNumber { get; set; } 
    }
}
