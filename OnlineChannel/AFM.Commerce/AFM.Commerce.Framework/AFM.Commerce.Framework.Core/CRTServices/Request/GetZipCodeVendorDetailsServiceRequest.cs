﻿
using System.Runtime.Serialization;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.Services;

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    /// <summary>
    /// Get the request to retrieve all the promotions for a {Channel, Catalog} pair.
    /// </summary>
    [DataContract]
    public sealed class GetZipCodeVendorDetailsServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GetZipCodeVendorDetailsServiceRequest"/> class.
        /// </summary>
        /// <param name="context">The context of the request.</param>
        public GetZipCodeVendorDetailsServiceRequest(string zipcode)
        {
            this.ZipCode = zipcode;
        }

        /// <summary>
        /// Gets or sets the ZipCode identifier.
        /// </summary>
        [DataMember]
        public string ZipCode { get; set; }
    }
}
