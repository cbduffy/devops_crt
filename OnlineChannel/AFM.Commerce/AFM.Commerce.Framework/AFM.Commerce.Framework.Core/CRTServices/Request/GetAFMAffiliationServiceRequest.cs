﻿namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    public class GetAFMAffiliationServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        /// <summary>
        /// Request can be made for these three calls
        /// </summary>
        public enum AffiliationRequestType
        {
            GetDefaultAffiliationId,
            GetAffiliationIdByZipCode,
            GetAffiliationIdByProduct,
            GetAffiliationIdByProductIds,
            GetAllAffiliations
        }

      

        public AffiliationRequestType RequestType { get; set; }
        /// <summary>
        /// Is the request by ZipCode or ProductCode
        /// </summary>
        public bool IsRequestByZipCode { get; set; }

        /// <summary>
        /// Gets or sets the channel identifier.
        /// </summary>
        [DataMember]
        public string OMOperatingUnitNumber { get; set; }

        /// <summary>
        /// Gets or sets the channel identifier.
        /// </summary>
        [DataMember]
        public long ChannelId { get; set; }

        /// <summary>
        /// Gets or sets the ZipCode identifier.
        /// </summary>
        [DataMember]
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the ProductCode identifier.
        /// </summary>
        [DataMember]
        public string ProductId { get; set; }

        /// <summary>
        /// Gets or sets the ProductCode identifier.
        /// </summary>
        [DataMember]
        public List<string> ProductIds { get; set; }
    }
}
