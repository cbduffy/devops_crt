﻿using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    [DataContract]
    public sealed class AFMValidateAddressDataRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {        
        //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
        public AFMValidateAddressDataRequest(AddressValidationDll.DataModels.Address address)
        {
            this.AddressValidationAddress = address;
        }

        [DataMember]
        public AddressValidationDll.DataModels.Address AddressValidationAddress { get; set; }

        //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
    }
}
