﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/28/2014*/

using System;
using System.Collections.Generic;

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;

    /// <summary>
    /// Get the request to retrieve all the promotions for a {Channel, Catalog} pair.
    /// </summary>
    [DataContract]
    public sealed class GetPrdtNAPRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GetPrdtNAPRequest"/> class.
        /// </summary>
        /// <param name="context">The context of the request.</param>
        public GetPrdtNAPRequest()
        {
        }

        /// <summary>
        /// Gets or sets the ProductIds List identifier.
        /// </summary>
        [DataMember]
        public List<long> ProductIds { get; set; }

        /// <summary>
        /// Gets or sets the ProductIds List identifier.
        /// </summary>
        [DataMember]
        public DateTime CurrentDate { get; set; }

    }
}
