﻿using System.Runtime.Serialization;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using System;
using System.Collections.Generic;

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    [DataContract]
    public class GetAFMTransactionCustomerServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {

        public GetAFMTransactionCustomerServiceRequest()
        {
        }

        [DataMember]
        public string cctTransactionID { get; set; }


        [DataMember]
        public string sctTransactionID { get; set; }

        [DataMember]
        public string customerId { get; set; }

    }
}
