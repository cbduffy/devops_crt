﻿namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

     [DataContract]
    public sealed class AFMGetAddressStateRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        public AFMGetAddressStateRequest(string CountryRegionID)
        {
            this.CountryRegionId = CountryRegionID;
        }

        [DataMember]
        public string CountryRegionId
        {
            get;
            set;
        }
    }
}
