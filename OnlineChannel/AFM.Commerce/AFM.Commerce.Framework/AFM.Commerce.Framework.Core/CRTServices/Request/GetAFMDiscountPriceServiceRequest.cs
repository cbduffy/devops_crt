﻿namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    using System.Runtime.Serialization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    public class GetAFMDiscountPriceServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GetAFMDiscountPriceServiceRequest"/> class.
        /// </summary>
        /// <param name="context">The context of the request.</param>
        public GetAFMDiscountPriceServiceRequest()
        {
        }

        /// <summary>
        /// Gets or sets the SalesTransaction
        /// </summary>
        [DataMember]
        public SalesTransaction SalesTransactionData { get; set; }

        [DataMember]
        public RequestContext requestContext { get; set; }
    }
}
