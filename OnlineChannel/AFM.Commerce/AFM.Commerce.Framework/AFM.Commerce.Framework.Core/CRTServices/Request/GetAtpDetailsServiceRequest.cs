﻿using AFM.Commerce.Entities;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.CRTServices.Request
{
    public class GetAtpDetailsServiceRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
         /// <summary>
        /// Initializes a new instance of the <see cref="GetAtpDetailsServiceRequest"/> class.
        /// </summary>
        /// <param name="context">The context of the request.</param>
        public GetAtpDetailsServiceRequest()
        {
        }
        /// <summary>
        /// Gets or sets item Groupings
        /// </summary>
        public List<AFMATPItemGroupingRequest> ItemGroupings
        {
            get;
            set;
        }
    }
}
