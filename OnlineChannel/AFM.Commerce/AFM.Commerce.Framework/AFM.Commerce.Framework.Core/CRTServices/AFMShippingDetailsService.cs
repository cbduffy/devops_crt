﻿/*NS Developed by ysrini for AFM_TFS_40686 dated 06/29/2014*/

namespace AFM.Commerce.Framework.Core.CRTServices
{
    using System;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.DataManagers;
    using AFM.Commerce.Framework.Core.Models;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using System.Collections.Generic;

    public class AFMShippingDetailsService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(ShippingDetailsRequest),
                    typeof(DeliveryChargesAndModeRequest)
                };
            }
        }

        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestedType = request.GetType();

            if (requestedType == typeof(ShippingDetailsRequest))
            {
                return GetShippingDetailsByItemId((ShippingDetailsRequest)request);
            }

            if (requestedType == typeof(DeliveryChargesAndModeRequest))
            {
                return GetShippingDetailsForCartItems((DeliveryChargesAndModeRequest)request);
            }

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }

        public ShippingDetailsResponse GetShippingDetailsByItemId(ShippingDetailsRequest request)
        {
            var dataManager = new AFMShippingDetailsManager(request.RequestContext);
            //var itemShippingInfo = dataManager.GetShippingDetailsByItemId(request.ItemId);
            return new ShippingDetailsResponse(new AFMShippingDetails());
        }

        public DeliveryChargesAndModeResponse GetShippingDetailsForCartItems(DeliveryChargesAndModeRequest request)
        {
            var dataManager = new AFMShippingDetailsManager(request.RequestContext);
            var itemShippingInfo = dataManager.GetShippingDetailsForCartItems(request.CartItems);
            return new DeliveryChargesAndModeResponse(itemShippingInfo);
        }

    }
}
