﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Entities;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using Microsoft.Dynamics.Commerce.Runtime;
using AFM.Commerce.Framework.Core.AtpService;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;

namespace AFM.Commerce.Framework.Core.CRTServices
{
    public class AFMATPService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetAtpDetailsServiceRequest)
                };
            }
        }
        /// <summary>
        /// Executes the request and returns response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            Type requestedType = request.GetType();

            if (requestedType == typeof(GetAtpDetailsServiceRequest))
            {
                return (GetATPDetailsServiceResponse) GetAtpForEcommerce((GetAtpDetailsServiceRequest)request);
            }

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }

        /// <summary>
        /// Fetch's ATP for ecommerce 
        /// </summary>
        /// <param name="request">request object</param>
        /// <returns>response object</returns>
        public  GetATPDetailsServiceResponse GetAtpForEcommerce(GetAtpDetailsServiceRequest request)
        {
            GetATPDetailsServiceResponse responseEntity = new GetATPDetailsServiceResponse(null);
            AtpRequest atpForEcommerceRequest = new AtpRequest();
            MapRequestEntityToServiceRequest(request, atpForEcommerceRequest);

            AtpResponse requestResponse = null;
            try
            {
                using (AtpWebServiceClient atpClient = new AtpWebServiceClient())
                {
                    requestResponse = atpClient.GetAtpForEcommerce(atpForEcommerceRequest);
                    MapServiceResponseToResponseEntity(requestResponse, responseEntity);
                }
            }
            catch
            {
                //TODO:Replace hardcoded string with resource string
                //responseEntity.ATPError = "Something went wrong. Try again.";
            }

            return responseEntity;

        }

        /// <summary>
        /// maps GetAtpDetailsServiceRequest to ATPServiceRequest
        /// </summary>
        /// <param name="request">GetAtpDetailsServiceRequest</param>
        /// <param name="serviceRequest">AtpRequest</param>
        public static void MapRequestEntityToServiceRequest(GetAtpDetailsServiceRequest request, AtpRequest serviceRequest)
        {
            List<ItemGroupingRequest> serviceItemGroupings = new List<ItemGroupingRequest>();

            foreach (var itemGroup in request.ItemGroupings)
            {

                List<RequestItem> serviceItems = new List<RequestItem>();
                foreach (var item in itemGroup.Items)
                {
                    RequestItem reqItem = new RequestItem();

                    reqItem.ItemId = item.ItemId;
                    reqItem.Quantity = item.Quantity;
                    reqItem.ThirdPartyItem = item.ThirdPartyItem;

                    serviceItems.Add(reqItem);
                }

                if(string.IsNullOrEmpty(itemGroup.CustomerZip)){
                    ItemGroupingRequest serviceItemGroup = new ItemGroupingRequest();
                    serviceItemGroup.AccountNumber = itemGroup.AccountNumber;
                    serviceItemGroup.ShipTo = itemGroup.ShipTo;
                    serviceItemGroup.Items = serviceItems.ToArray();
                    serviceItemGroupings.Add(serviceItemGroup);
                }
                else
                {
                    DirectShipItemGrouping serviceDirectShipItemGroup = new DirectShipItemGrouping();
                    serviceDirectShipItemGroup.AccountNumber = itemGroup.AccountNumber;
                    serviceDirectShipItemGroup.ShipTo = itemGroup.ShipTo;
                    serviceDirectShipItemGroup.CustomerZip = itemGroup.CustomerZip;
                    serviceDirectShipItemGroup.Items = serviceItems.ToArray();
                    serviceItemGroupings.Add(serviceDirectShipItemGroup);
                }
            }

            serviceRequest.ItemGroupings = serviceItemGroupings.ToArray();
        }

        /// <summary>
        /// Maps ATP service response to GetATPDetailsServiceResponse
        /// </summary>
        /// <param name="requestResponse">ATP response</param>
        /// <param name="responseEntity">GetATPDetailsServiceResponse</param>
        public static void MapServiceResponseToResponseEntity(AtpResponse requestResponse, GetATPDetailsServiceResponse responseEntity)
        {
            List<AFMATPItemGroupingResponse> respItemGroupings = new List<AFMATPItemGroupingResponse>();

            foreach (var itemGroup in requestResponse.ItemGroupings)
            {
                AFMATPItemGroupingResponse respItemGroup = new AFMATPItemGroupingResponse();
                respItemGroup.AccountNumber = itemGroup.AccountNumber;
                respItemGroup.ShipTo = itemGroup.ShipTo;
                var firstItem = itemGroup.Items.ElementAt(0);
                DateTime earliestDate = firstItem.BestDate.AddHours((firstItem.BeginDateRange - firstItem.BestDate).TotalHours);
                DateTime latestDate = firstItem.BestDate.AddHours((firstItem.EndDateRange - firstItem.BestDate).TotalHours);
                List<AFMATPResponseItem> respItems = new List<AFMATPResponseItem>();

                foreach (var item in itemGroup.Items)
                {
                    AFMATPResponseItem respItem = new AFMATPResponseItem();
                    respItem.BeginDateRange = item.BeginDateRange;
                    respItem.BestDate = item.BestDate;
                    respItem.EndDateRange = item.EndDateRange;
                    respItem.EarliestDeliveryDate = item.BestDate.AddHours((item.BeginDateRange - item.BestDate).TotalHours);
                    if (earliestDate != null && earliestDate.CompareTo(respItem.EarliestDeliveryDate) > 0)
                        earliestDate = respItem.EarliestDeliveryDate;
                    respItem.LatestDeliveryDate = item.BestDate.AddHours((item.EndDateRange - item.BestDate).TotalHours);
                    if (latestDate != null && latestDate.CompareTo(respItem.LatestDeliveryDate) < 0)
                        latestDate = respItem.LatestDeliveryDate;
                    respItem.IsAvailable = item.IsAvailable;
                    respItem.ItemId = item.ItemId;
                    respItem.Message = item.Message;
                    respItem.Quantity = item.Quantity;

                    respItems.Add(respItem);
                }
                foreach (var responseItem in respItems)
                {
                    responseItem.EarliestDeliveryDate = earliestDate;
                    responseItem.LatestDeliveryDate = latestDate;
                }
                respItemGroup.Items = respItems;

                respItemGroupings.Add(respItemGroup);
            }

            responseEntity.ItemGroupings = respItemGroupings;
        }
    }
}
