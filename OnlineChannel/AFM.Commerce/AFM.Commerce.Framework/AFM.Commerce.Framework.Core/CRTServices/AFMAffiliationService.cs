﻿

namespace AFM.Commerce.Framework.Core.CRTServices
{
    using System;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using AFM.Commerce.Framework.Core.CRTServices.Request;
    using AFM.Commerce.Framework.Core.CRTServices.Response;
    using AFM.Commerce.Framework.Core.DataManagers;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using System.Collections.Generic;

    /// <summary>
    /// Service class responsible for serving promotion requests
    /// </summary>
    public class AFMAffiliationService : IRequestHandler
    {
         /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetAFMAffiliationServiceRequest)
                };
            }
        }
       // Executes the request and returns response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Microsoft.Dynamics.Commerce.Runtime.Messages.Response Execute(Microsoft.Dynamics.Commerce.Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type reqType = request.GetType();
            var req = (GetAFMAffiliationServiceRequest) request;
            if (reqType == typeof(GetAFMAffiliationServiceRequest))
            {

                switch (req.RequestType)
                {
                    case GetAFMAffiliationServiceRequest.AffiliationRequestType.GetDefaultAffiliationId:
                        return GetDefaultAffiliationID((GetAFMAffiliationServiceRequest)request);
                    case GetAFMAffiliationServiceRequest.AffiliationRequestType.GetAffiliationIdByZipCode:
                        return GetAffiliationByZipCode((GetAFMAffiliationServiceRequest)request);
                    case GetAFMAffiliationServiceRequest.AffiliationRequestType.GetAffiliationIdByProduct:
                        return GetAffiliationByProductCode((GetAFMAffiliationServiceRequest)request);
                    case GetAFMAffiliationServiceRequest.AffiliationRequestType.GetAffiliationIdByProductIds:
                        return GetAffiliationByProductIds((GetAFMAffiliationServiceRequest)request);
                    case GetAFMAffiliationServiceRequest.AffiliationRequestType.GetAllAffiliations:
                        return GetAllAffiliations((GetAFMAffiliationServiceRequest)request);
                    default:
                        break;
                }
            }
            
            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));

        }

        internal static GetAFMAffiliationServiceResponse GetAffiliationByZipCode(GetAFMAffiliationServiceRequest request)
        {
            string zipCode = request.ZipCode;
            long channelId = request.ChannelId;
           


            var dataManager = new AFMAffiliationDataManager(request.RequestContext);
            var affiliationData = dataManager.GetAffiliationByZipCode(zipCode, channelId);

            return new GetAFMAffiliationServiceResponse(affiliationData);
            }

        internal static GetAFMAffiliationServiceResponse GetAffiliationByProductCode(GetAFMAffiliationServiceRequest request)
        {
            string productId = request.ProductId;
            long channelId = request.ChannelId;

            var dataManager = new AFMAffiliationDataManager(request.RequestContext);
            var affiliationData = dataManager.GetAffiliationsByProduct(productId, channelId);

            return new GetAFMAffiliationServiceResponse(affiliationData);
        }


        internal static GetAFMAffiliationServiceResponse GetAffiliationByProductIds(GetAFMAffiliationServiceRequest request)
        {
            //string productId = request.ProductId;
            long channelId = request.ChannelId;

            var dataManager = new AFMAffiliationDataManager(request.RequestContext);
            var affiliationData = dataManager.GetAffiliationsByProductIds(request.ProductIds, channelId);

            return new GetAFMAffiliationServiceResponse(affiliationData);
        }

        internal static GetAFMAffiliationServiceResponse GetDefaultAffiliationID(GetAFMAffiliationServiceRequest request)
        {
            var dataManager = new AFMAffiliationDataManager(request.RequestContext);
            long channelId = request.ChannelId;
            var affiliationData = dataManager.GetDefaultAffiliationIdForChannel(channelId);

            var response = new GetAFMAffiliationServiceResponse();
            return new GetAFMAffiliationServiceResponse(affiliationData);

        }

        internal static GetAFMAffiliationServiceResponse GetAllAffiliations(GetAFMAffiliationServiceRequest request)
        {
            var dataManager = new AFMAffiliationDataManager(request.RequestContext);
            long channelId = request.ChannelId;
            var affiliationData = dataManager.GetAllAffiliationsForChannel(channelId);

            var response = new GetAFMAffiliationServiceResponse();
            return new GetAFMAffiliationServiceResponse(affiliationData);

        }

    }
}
