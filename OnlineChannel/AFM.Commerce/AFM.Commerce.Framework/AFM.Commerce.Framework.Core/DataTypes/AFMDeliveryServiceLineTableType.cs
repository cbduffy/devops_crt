﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Framework.Core.Models;

namespace AFM.Commerce.Runtime.Services.DataTypes
{
    public sealed class AFMDeliveryServiceLineTableType : TableType
    {
        private const string AFMDeliveryServiceLineTableTypeName = "AFMDELIVERYSALESLINE";

        public AFMDeliveryServiceLineTableType(IEnumerable<AFMShippingDetails> afmshippingDetail)
            : base("AFMDELIVERYSALESLINE")
        {
            this.CreateTableSchema();
            foreach (AFMShippingDetails shippinginfo in afmshippingDetail)
                this.DataTable.Rows.Add(this.GetDataRow(shippinginfo));
        }

        protected override void CreateTableSchema()
        {
            this.DataTable.Columns.Add(new DataColumn("LineId", typeof(string)));           
            this.DataTable.Columns.Add(new DataColumn("ProductId", typeof(long)));
            this.DataTable.Columns.Add(new DataColumn("ItemId", typeof(string)));
            this.DataTable.Columns.Add(new DataColumn("Quantity", typeof(int)));
            this.DataTable.Columns.Add(new DataColumn("ZipCode", typeof(string)));
            this.DataTable.Columns.Add(new DataColumn("TotalValue", typeof(decimal)));            
            this.DataTable.Columns.Add(new DataColumn("AFMShippingMode", typeof(string)));
            this.DataTable.Columns.Add(new DataColumn("AFMShippingCost", typeof(decimal)));
        }

        private DataRow GetDataRow(AFMShippingDetails shippingDetail)
        {
            DataRow dataRow = this.DataTable.NewRow();
            dataRow["LineId"] = shippingDetail.LineId;            
            dataRow["ProductId"] = shippingDetail.ProductId;
            dataRow["ItemId"] = shippingDetail.ItemId;
            dataRow["Quantity"] = shippingDetail.Quantity;
            dataRow["ZipCode"] = shippingDetail.ZipCode;
            dataRow["TotalValue"] = shippingDetail.TotalValue;           
            dataRow["AFMShippingMode"] = shippingDetail.ShippingMode;
            dataRow["AFMShippingCost"] = shippingDetail.ShippingCost;
            return dataRow;
        }

    }
}
