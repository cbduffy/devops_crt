﻿/*
 * NS Developed for CR - 137 Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box
 */

using System.Collections.Generic;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using AFM.Commerce.Runtime.Services.DataTypes;
using AFM.Commerce.Runtime.Services.Common;

namespace AFM.Commerce.Framework.Core.DataTypes
{
    internal sealed class ItemIdSearchTableType : JoinableTableType 
    {
        private const string TableTypeName = "ItemIdSearchTableType";
        private const string ItemIdColumnName = "ITEMID";
        private const string InventDimIdColumnName = "INVENTDIMID";
        public override string TableTypeJoinColumn
        {
            get
            {
                return "ITEMID";
            }
        }
        public ItemIdSearchTableType(IEnumerable<ProductLookupClause> itemLookups)
            : this(itemLookups, "ITEMID")
        {
        }
        public ItemIdSearchTableType(IEnumerable<ProductLookupClause> itemLookups, string joinColumn)
            : base("ItemIdSearchTableType")
        {
            ThrowIf.Null<IEnumerable<ProductLookupClause>>(itemLookups, "itemLookups");
            ThrowIf.NullOrWhiteSpace(joinColumn, "joinColumn");
            base.JoinColumn = joinColumn;
            this.CreateTableSchema();
            foreach (ProductLookupClause current in itemLookups)
            {
                base.DataTable.Rows.Add(new object[]
				{
					current.ItemId,
					current.InventDimensionId ?? string.Empty
				});
            }
        }
        protected override void CreateTableSchema()
        {
            base.DataTable.Columns.Add("ITEMID", typeof(string));
            base.DataTable.Columns.Add("INVENTDIMID", typeof(string));
        }
    }
}
