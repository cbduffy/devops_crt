﻿
namespace AFM.Commerce.Runtime.Services
{
    /// <summary>
    /// This class contains constant strings used across the customization.
    /// </summary>
    public static class CustomerConstants
    {
        //Transaction service constants
        public const string UpdateCustomerInformationTransactionServiceMethod = "afmUpdateCustomerInformation";

        //Database constants
        public const string CreateUpdateElectronicAddressStoredProcedure = "CREATEUPDATEELECTRONICADDRESS";
        public const string GetElectronicAddressesStoredProcedure = "AFMGETELECTRONICADDRESSES";
        public const string UpdateAddressInfoStoredProcedure = "AFMUPDATEADDRESSINFO";

        //Customer level constants
        public const string SourceInfo = "SOURCEINFO";
        public const string IsMarketingOptin = "ISMARKETINGOPTIN";
        public const string IsGuestCheckout = "ISGUESTCHECKOUT";
        public const string IsOlderThan13 = "ISOLDERTHAN13";
        public const string AddressFirstName = "ADDRESSFIRSTNAME";
        public const string AddressLastName = "ADDRESSLASTNAME";
        public const string AddressName = "NAME";

        //NS developed by v-hapat for CR 72099 
        public const string IsMarketingOptinUniqueId = "ISMARKETINGOPTINUNIQUEID";
        public const string IsMarketingOptinVersionId = "ISMARKETINGOPTINVERSIONID";
        public const string IsMarketingOptinDate = "ISMARKETINGOPTINDATE";
        //NS developed by v-hapat for CR 72099 

        //Address level constants
        public const string LogisticElectronicLocationRecordId = "LOGISTICELECTRONICLOCATIONRECORDID";

        //Electronic Address level contastant
        public const string Email = "EMAIL";
        public const string EmailRecordId = "EMAILRECORDID";
        public const string EmailDescription = "EMAILDESC";

        public const string Email1 = "EMAIL1";
        public const string Email1Description = "EMAIL1DESC";
        public const string Email1RecordId = "EMAIL1RECORDID";

        public const string Email2 = "EMAIL2";
        public const string Email2Description = "EMAIL2DESC";
        public const string Email2RecordId = "EMAIL2RECORDID";

        public const string Phone = "PHONE";
        public const string PhoneRecordId = "PHONERECORDID";
        public const string PhoneDescription = "PHONEDESC";

        public const string Phone1RecordId = "PHONE1RECORDID";
        public const string Phone1 = "PHONE1";
        public const string Phone1Description = "PHONE1DESC";
        public const string Phone1Extension = "PHONE1EXT";

        public const string Phone2RecordId = "PHONE2RECORDID";
        public const string Phone2 = "PHONE2";
        public const string Phone2Description = "PHONE2DESC";
        public const string Phone2Extension = "PHONE2EXT";

        public const string Phone3RecordId = "PHONE3RECORDID";
        public const string Phone3 = "PHONE3";
        public const string Phone3Description = "PHONE3DESC";
        public const string Phone3Extension = "PHONE3EXT";

        //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
        public const string Address = "address";
        public const string ThreeLetterCountryCode = "USA";
        public const string TwoLetterCountryCode = "US";
        public const string Invoice = "Invoice";
        public const string ZipCodeMaster = "Zip code master";
        public const string POError = "POERROR";
        //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
    }
}
