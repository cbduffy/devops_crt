﻿
using System;
using System.Collections;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace AFM.Commerce.Runtime.Services.Common
{
    /// <summary>
    /// This is copy of Microsoft.Dynamics.Commerce.Runtime.ThrowIf clas. This class has methods to throw in case errors in case of invalid parameters
    /// </summary>
    public static class ThrowIf
    {
        [DebuggerStepThrough]
        public static void Null<T>([ThrowIf.ValidatedNotNull] T arg, string parameterName) where T : class
        {
            if ((object)arg == null)
                throw new ArgumentNullException(parameterName);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "FxCop false positive.  This code is called.")]
        [DebuggerStepThrough]
        public static void NullOrWhiteSpace([ThrowIf.ValidatedNotNull] string arg, string parameterName)
        {
            if (string.IsNullOrWhiteSpace(arg))
                throw new ArgumentNullException(parameterName);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "FxCop false positive.  This code is called.")]
        [DebuggerStepThrough]
        public static void NullOrEmpty([ThrowIf.ValidatedNotNull] ICollection arg, string parameterName)
        {
            if (arg == null)
                throw new ArgumentNullException(parameterName);
            if (arg.Count == 0)
                throw new ArgumentException("The specified collection cannot be empty.", parameterName);
        }

        [DebuggerStepThrough]
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This can be called by external classes.")]
        public static void EmptyGuid(Guid guid, string parameterName)
        {
            if (guid == Guid.Empty)
                throw new ArgumentOutOfRangeException(parameterName, "Guid should not be empty.");
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This can be called by external classes.")]
        [DebuggerStepThrough]
        public static void Undefined(Enum enumeration, string parameterName)
        {
            if (!Enum.IsDefined(enumeration.GetType(), (object)enumeration))
                throw new ArgumentOutOfRangeException(parameterName, "Enum value should exists in a specified enumeration.");
        }

        [AttributeUsage(AttributeTargets.Parameter)]
        private sealed class ValidatedNotNullAttribute : Attribute
        {
        }
    }
}

