﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Runtime.Services.Common
{
    public enum ReceiptTransactionTypeExtension
    {
        [EnumMember]
        OrderConfirmation = 1,
    }
}
