﻿using AFM.Commerce.Entities;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.Workflows.Request;
using AFM.Commerce.Framework.Core.Workflows.Response;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
using Microsoft.Dynamics.Commerce.Runtime.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AFM.Commerce.Framework.Core.ClientManager
{
    public class AFMAddressValidationManager
    {
        private readonly CommerceRuntime _runtime;

        /// <summary>
        /// Initializes runtime property<see cref="AFMAddressValidationManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        public AFMAddressValidationManager(CommerceRuntime runtime)
        {
            this._runtime = runtime;
        }

        /// <summary>
        /// Creates a new instance of the clientManager with commerce runtime<see cref="AFMConfirmationNumberManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        /// <returns>
        /// A new instance of the Client manager with commerce runtime<see cref="AFMProductDetailsClientManager"/> class.
        /// </returns>
        public static AFMAddressValidationManager Create(CommerceRuntime runtime)
        {
            return new AFMAddressValidationManager(runtime);
        }

        /// <summary>
        /// This function calls the workflow to retrieve product details
        /// </summary>
        /// <param name="productIdsList"></param>
        /// <returns>Response with product details</returns>

        //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
        public AFMAddressData ValidateAddress(AddressValidationDll.DataModels.Address address)
        {
            var request = new AFMValidateAddressDataRequest(address);
            var response = this._runtime.Execute<AFMValidateAddressDataResponse>
                   (request, this._runtime.CreateRequestContext(request),true);
            return new AFMAddressData()
            {
                ErrorMessage = response.ErrorMessage,
                IsAddressValid = response.IsAddressValid,
                addressValidationsuggestedAddress = response.addressValidationSuggestedAddress,
                IsServiceVerified = response.IsServiceVerified
            };
        }
        //NE- DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
    }
}
