﻿using AFM.Commerce.Framework.Core.CRTServices;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.ClientManager
{
    public class AFMTransactionCustomerClientManager
    {

        private readonly CommerceRuntime _runtime;

        /// <summary>
        /// Initializes runtime property<see cref="AFMTransactionCustomerClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        public AFMTransactionCustomerClientManager(CommerceRuntime runtime)
        {
            this._runtime = runtime;
        }

        /// <summary>
        /// Creates a new instance of the clientManager with commerce runtime<see cref="AFMTransactionCustomerClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        /// <returns>
        /// A new instance of the Client manager with commerce runtime<see cref="AFMTransactionCustomerClientManager"/> class.
        /// </returns>
        public static AFMTransactionCustomerClientManager Create(CommerceRuntime runtime)
        {
            return new AFMTransactionCustomerClientManager(runtime);
        }

     public bool UpdateTransactionCustomer(string cct, string sct, string customerId)
        {
            AFMTransactionCustomerService transactionCustomer = new AFMTransactionCustomerService();
            GetAFMTransactionCustomerServiceRequest transactionCustomerServiceRequest = new GetAFMTransactionCustomerServiceRequest()
            {
                cctTransactionID = cct,
                sctTransactionID = sct,
                customerId = customerId
            };
            //return transactionCustomer.UpdateTransactionCustomer(transactionCustomerServiceRequest).Result;
            return this._runtime.Execute<GetAFMTransactionCustomerServiceResponse>(transactionCustomerServiceRequest, this._runtime.CreateRequestContext(transactionCustomerServiceRequest),true).Result;

        }
    }
}
