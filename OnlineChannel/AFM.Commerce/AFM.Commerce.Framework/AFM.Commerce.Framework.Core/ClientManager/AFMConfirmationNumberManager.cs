﻿using AFM.Commerce.Framework.Core.Workflows.Request;
using AFM.Commerce.Framework.Core.Workflows.Response;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.ClientManager
{
    public class AFMConfirmationNumberManager
    {
        private readonly CommerceRuntime _runtime;

        /// <summary>
        /// Initializes runtime property<see cref="AFMProductDetailsClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        public AFMConfirmationNumberManager(CommerceRuntime runtime)
        {
            this._runtime = runtime;
        }

        /// <summary>
        /// Creates a new instance of the clientManager with commerce runtime<see cref="AFMConfirmationNumberManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        /// <returns>
        /// A new instance of the Client manager with commerce runtime<see cref="AFMProductDetailsClientManager"/> class.
        /// </returns>
        public static AFMConfirmationNumberManager Create(CommerceRuntime runtime)
        {
            return new AFMConfirmationNumberManager(runtime);
        }

        /// <summary>
        /// This function calls the workflow to retrieve product details
        /// </summary>
        /// <param name="productIdsList"></param>
        /// <returns>Response with product details</returns>
        public string GetConfirmationSequenceNumber()
        {
            var request = new GetAFMConfirmationNumberRequest();
            var response = this._runtime.Execute<GetAFMConfirmationNumberResponse>
                    (request, null);
            return response.ConfirmationNumber;
        }

    }
}
