﻿using AFM.Commerce.Framework.Core.CRTServices;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Framework.Core.ClientManager
{
    public class AFMRetailParametersClientManager
    {

        private readonly CommerceRuntime _runtime;

        /// <summary>
        /// Initializes runtime property<see cref="AFMRetailParametersClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        public AFMRetailParametersClientManager(CommerceRuntime runtime)
        {
            this._runtime = runtime;
        }

        /// <summary>
        /// Creates a new instance of the clientManager with commerce runtime<see cref="AFMTransactionCustomerClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        /// <returns>
        /// A new instance of the Client manager with commerce runtime<see cref="AFMTransactionCustomerClientManager"/> class.
        /// </returns>
        public static AFMRetailParametersClientManager Create(CommerceRuntime runtime)
        {
            return new AFMRetailParametersClientManager(runtime);
        }

        public AFMRetailParameters getRetailParameters()
        {
            AFMRetailParametersService retailParameters = new AFMRetailParametersService();
            GetAFMRetailParametersServiceRequest retailParametersServiceRequest = new GetAFMRetailParametersServiceRequest()
            ;
            var reqcontext = this._runtime.CreateRequestContext(retailParametersServiceRequest);
            Microsoft.Dynamics.Commerce.Runtime.Client.ChannelManager chanalnmanger= Microsoft.Dynamics.Commerce.Runtime.Client.ChannelManager.Create(this._runtime);
            reqcontext.SetChannelConfiguration(chanalnmanger.GetChannelConfiguration());
            return this._runtime.Execute<GetAFMRetailParametersServiceResponse>(retailParametersServiceRequest, reqcontext, true).InfoCodeId;

        }
    }
}
