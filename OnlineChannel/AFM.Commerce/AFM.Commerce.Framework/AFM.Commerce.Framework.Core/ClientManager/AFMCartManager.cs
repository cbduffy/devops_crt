﻿/*NS Developed by muthait dated 22Oct2014 */
namespace AFM.Commerce.Framework.Core.ClientManager
{
    using AFM.Commerce.Entities;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Messages;
using System.Collections.Generic;

    public class AFMCartManager
    {
        private readonly CommerceRuntime _runtime;

        public AFMCartManager(CommerceRuntime runtime)
        {
            this._runtime = runtime;
        }

        public static AFMCartManager Create(CommerceRuntime runtime)
        {
            return new AFMCartManager(runtime);
        }

        public bool UpdateCartData(bool isDelete, string cartId)
        {
            var req = new UpdateCartDataRequest()
            {
                CartId = cartId,
                IsDelete = isDelete
            };
            return this._runtime.Execute<UpdateCartDataResponse>(req, this._runtime.CreateRequestContext(req),true).Result;
        }

        /*ND Developed by muthait dated 02Jan2015*/
        public string CloneCartData(string newCartId,string sourceCartId)
        {
            var req = new CloneCartDataRequest()
            {
                NewCartId = newCartId,
                SourceCartId = sourceCartId
            };
            return this._runtime.Execute<CloneCartDataResponse>(req, this._runtime.CreateRequestContext(req), true).CartID;
        }


        //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID        
        /// <summary>
        /// Get the RSA ID record of given rsa from AFMCartDataService using GetRsaIdServiceRequest.
        /// If rsaId is empty then get the list of RSA Ids from AFMCartDataService using GetRsaIdServiceRequest
        /// </summary>
        /// <returns>List of Rsa Ids</returns>
        public List<AFMRsaId> GetRsaIds(string rsaId)
        {
            var req = new GetRsaIdServiceRequest(rsaId);
            var response = this._runtime.Execute<GetRsaIdServiceResponse>(req, this._runtime.CreateRequestContext(req));
            return response.RsaIds;
        }
        //NE - RxL

        //NS by spriya : DMND0010113 - Synchrony Financing online
        /// <summary>
        /// Get Synchrony options using thee AFMSynchronyServiceRequest
        /// </summary>
        /// <returns>List of SynchronyOptions</returns>
        public List<AFM.Commerce.Framework.Core.Models.AFMSynchronyOptions> GetSynchronyOptions()
        {
            var req = new AFMSynchronyServiceRequest();
            var response = this._runtime.Execute<AFMSynchronyServiceResponse>(req, this._runtime.CreateRequestContext(req));
            return response.synchronyOptions;
        }
        //NE by spriya : DMND0010113 - Synchrony Financing online
    }
}