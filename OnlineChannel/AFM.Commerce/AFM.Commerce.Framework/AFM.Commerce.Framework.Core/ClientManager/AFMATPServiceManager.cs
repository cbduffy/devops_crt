﻿
using AFM.Commerce.Entities;

namespace AFM.Commerce.Framework.Core.ClientManager
{
    using AFM.Commerce.Framework.Core.Workflows.Request;
    using AFM.Commerce.Framework.Core.Workflows.Response;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using System.Collections.Generic;

    public class AFMATPServiceManager
    {
        private readonly CommerceRuntime runtime;

         /// <summary>
        /// Initializes a new instance of the <see cref="AFMATPServiceManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        public AFMATPServiceManager(CommerceRuntime runtime)
        {
            this.runtime = runtime;
        }

        /// <summary>
        /// Creates a new instance of the <see cref="AFMATPServiceManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        /// <returns>
        /// A new instance of the <see cref="AFMATPServiceManager"/> class.
        /// </returns>
        public static AFMATPServiceManager Create(CommerceRuntime runtime)
        {
            return new AFMATPServiceManager(runtime);
        }

        public List<AFMATPItemGroupingResponse> GetAtpForEcommerce(AFMATPRequest request)
        {
            var req = new ATPRequest()
            {
                ItemGroupings = request.ItemGroupings
            };

            return this.runtime.Execute<ATPResponse>(req, null).ItemGroupings;
        }


    }
}
