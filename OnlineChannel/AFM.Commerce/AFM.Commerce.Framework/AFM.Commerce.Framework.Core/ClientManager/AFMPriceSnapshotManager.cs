﻿
using System.Collections.Generic;
using AFM.Commerce.Framework.Core.Models;
using AFM.Commerce.Framework.Core.Workflows.Request;
using AFM.Commerce.Framework.Core.Workflows.Response;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Messages;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.CRTServices;

namespace AFM.Commerce.Framework.Core.ClientManager
{
    public class AFMPriceSnapshotManager
    {
         private readonly CommerceRuntime _runtime;

        /// <summary>
        /// Initializes runtime property<see cref="AFMProductDetailsClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
         public AFMPriceSnapshotManager(CommerceRuntime runtime)
        {
            this._runtime = runtime;
        }

        /// <summary>
        /// Creates a new instance of the clientManager with commerce runtime<see cref="AFMProductDetailsClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        /// <returns>
        /// A new instance of the Client manager with commerce runtime<see cref="AFMProductDetailsClientManager"/> class.
        /// </returns>
         public static AFMPriceSnapshotManager Create(CommerceRuntime runtime)
        {
            return new AFMPriceSnapshotManager(runtime);
        }

     /// <summary>
     /// This function calls the workflow to retrieve product details
     /// </summary>
     /// <param name="productIdsList"></param>
     /// <returns>Response with product details</returns>
        public List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> ItemIds,long affiliationId)
        {
            
            if (ItemIds != null && ItemIds.Count > 0)
            {
                var req = new AFMPriceSnapshotServiceRequest() {
                        ItemIds = ItemIds,
                        AffiliationId=affiliationId
                    };
                var response = new AFMPriceSnapshotService().GetPriceSnapshot(req);

            return response.ItemPriceSnapshot;
            }

            return null;
        }

        /// <summary>
        /// Overloaded method to get prices for all affiliations if only item ids passed
        /// </summary>
        /// <param name="ItemIds"></param>
        /// <returns></returns>
        public List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> ItemIds)
        {

            if (ItemIds != null && ItemIds.Count > 0)
            {
                var req = new AFMPriceSnapshotServiceRequest()
                {
                    ItemIds = ItemIds
                };
                var response = new AFMPriceSnapshotService().GetPriceSnapshot(req);

                return response.ItemPriceSnapshot;
            }

            return null;
        }
    }
}
