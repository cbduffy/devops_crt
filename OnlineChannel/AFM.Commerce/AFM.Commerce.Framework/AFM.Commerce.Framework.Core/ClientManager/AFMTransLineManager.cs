﻿namespace AFM.Commerce.Framework.Core.ClientManager
{
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.Workflows.Request;
    using AFM.Commerce.Framework.Core.Workflows.Response;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    public class AFMTransLineManager
    {
        private readonly CommerceRuntime _runtime;

        public AFMTransLineManager(CommerceRuntime runtime)
        {
            this._runtime = runtime;
        }

        public static AFMTransLineManager Create(CommerceRuntime runtime)
        {
            return new AFMTransLineManager(runtime);
        }

        public bool UpdateAFMSalesTransLines(AFMSalesTransItems afmSalesTransItems)
        {
            var req = new GetAFMTransLineRequest()
            {
                VendorId = afmSalesTransItems.VendorId,
                LineNumber = afmSalesTransItems.LineNumber,
                OrderNumber = afmSalesTransItems.OrderNumber,
                KitItemId = afmSalesTransItems.KitItemId,
                KitSequenceNumber = afmSalesTransItems.KitSequenceNumber,
                KitProductId = afmSalesTransItems.KitProductId,
                KitItemProductDetails = afmSalesTransItems.KitItemProductDetails,
                KitItemQuantity = afmSalesTransItems.KitItemQuantity,
                BeginDateRange = afmSalesTransItems.BeginDateRange,
                BestDate = afmSalesTransItems.BestDate,
                EndDateRange = afmSalesTransItems.EndDateRange,
                //CS Developed by spriya Bug 77476
                LatestDeliveryDate = afmSalesTransItems.LatestDeliveryDate,
                EarliestDeliveryDate = afmSalesTransItems.EarliestDeliveryDate,
                //CE Devloped by spriya
                IsAvailable = afmSalesTransItems.IsAvailable,
                Message = afmSalesTransItems.Message,
                LineConfirmationID = afmSalesTransItems.LineConfirmationID,
                AFMItemType = afmSalesTransItems.AFMItemType,
                // HXM To resolve 67447 Color and Size not populating
                Color = afmSalesTransItems.Color,
                Size = afmSalesTransItems.Size,
                ShippingMode = afmSalesTransItems.ShippingMode,
                
                //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
                BeginDateRangeMOD = afmSalesTransItems.BeginDateRangeMOD,
                BestDateMOD = afmSalesTransItems.BestDateMOD,
                EndDateRangeMOD = afmSalesTransItems.EndDateRangeMOD,
                LatestDeliveryDateMOD = afmSalesTransItems.LatestDeliveryDateMOD,
                EarliestDeliveryDateMOD = afmSalesTransItems.EarliestDeliveryDateMOD,
                MessageMOD = afmSalesTransItems.MessageMOD
                //NE by RxL
            };
            req.Context = new RequestContext(this._runtime);
            return this._runtime.Execute<GetAFMTransLineResponse>(req, null).VendorUpdateResult;
        }
    }
}