﻿
using System.Collections.Generic;
using AFM.Commerce.Framework.Core.Models;
using AFM.Commerce.Framework.Core.Workflows.Request;
using AFM.Commerce.Framework.Core.Workflows.Response;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Messages;


namespace AFM.Commerce.Framework.Core.ClientManager
{
    /// <summary>
    /// This is a client manager class which calls the workflow request handler
    /// </summary>
    public class AFMProductDetailsClientManager
    {

        private readonly CommerceRuntime _runtime;

        /// <summary>
        /// Initializes runtime property<see cref="AFMProductDetailsClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        public AFMProductDetailsClientManager(CommerceRuntime runtime)
        {
            this._runtime = runtime;
        }

        /// <summary>
        /// Creates a new instance of the clientManager with commerce runtime<see cref="AFMProductDetailsClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        /// <returns>
        /// A new instance of the Client manager with commerce runtime<see cref="AFMProductDetailsClientManager"/> class.
        /// </returns>
        public static AFMProductDetailsClientManager Create(CommerceRuntime runtime)
        {
            return new AFMProductDetailsClientManager(runtime);
        }

     /// <summary>
     /// This function calls the workflow to retrieve product details
     /// </summary>
     /// <param name="productIdsList"></param>
     /// <returns>Response with product details</returns>
        public AFMProductDetails GetListingExtension(List<long> productIdsList)
        {
            
            if (productIdsList != null && productIdsList.Count > 0)
            {
                var req = new GetAFMProductDetailsRequest() {
                        ProductIdsList = productIdsList 
                    };
                var response = (GetAFMProductDetailsResponse) this._runtime.Execute<GetAFMProductDetailsResponse>(req, null);

            return response.ProductDetails;
            }

            return null;
        }

    }
}
