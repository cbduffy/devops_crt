﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using AFM.Commerce.Framework.Core.Workflows.Request;
using AFM.Commerce.Framework.Core.Workflows.Response;

namespace AFM.Commerce.Framework.Core.ClientManager 
{
    public class AFMPromotionsUtilityManager
    {
        private readonly CommerceRuntime _runtime;

        /// <summary>
        /// Initializes a new instance of the <see cref="PromotionsManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        public AFMPromotionsUtilityManager(CommerceRuntime runtime)
        {
            this._runtime = runtime;
        }

        /// <summary>
        /// Creates a new instance of the <see cref="PromotionsManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        /// <returns>
        /// A new instance of the <see cref="PromotionsManager"/> class.
        /// </returns>
        public static AFMPromotionsUtilityManager Create(CommerceRuntime runtime)
        {
            return new AFMPromotionsUtilityManager(runtime);
        }

        /// <summary>
        /// Gets the promotions for the specified catalog.
        /// </summary>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="tr">The input transaction object</param>
        /// <param name="affiliationids">The affiliations to be assigned</param>
        /// <returns>
        /// The collection of promotion details.
        /// </returns>
        public SalesTransaction GetDiscountedProducts(long catalogId, SalesTransaction tr, List<AffiliationLoyaltyTier> affiliationids)
        {
            if (affiliationids!=null && affiliationids.Count > 0)
            {
                foreach (AffiliationLoyaltyTier a in affiliationids)
                {
                    if (a.AffiliationId > 0)
                    {
                        SalesAffiliationLoyaltyTier sa = new SalesAffiliationLoyaltyTier();
                        sa.AffiliationId = a.AffiliationId;
                        sa.AffiliationType = a.AffiliationType;
                        sa.LoyaltyTierId = a.LoyaltyTierId;
                        sa.CustomerId = a.CustomerId;
                        tr.AffiliationLoyaltyTierLines.Add(sa);
                    }
                }
            }
            GetAFMDiscountPriceRequest request = new GetAFMDiscountPriceRequest() { SalesTransactionData = tr};
            var response = this._runtime.Execute <GetAFMDiscountPriceResponse>(request, null);
            return response.SalesTransactionData;
        }
    }
}
