﻿using System.Collections.Generic;
using AFM.Commerce.Framework.Core.Models;
using AFM.Commerce.Framework.Core.Workflows.Request;
using AFM.Commerce.Framework.Core.Workflows.Response;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Messages;
using AFM.Commerce.Framework.Core.CRTServices.Request;
using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.CRTServices;




namespace AFM.Commerce.Framework.Core.ClientManager
{
    public class AFMFulfillerInformationManager
    {
         private readonly CommerceRuntime _runtime;

        /// <summary>
        /// Initializes runtime property<see cref="AFMProductDetailsClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
         public AFMFulfillerInformationManager(CommerceRuntime runtime)
        {
            this._runtime = runtime;
        }

        /// <summary>
        /// Creates a new instance of the clientManager with commerce runtime<see cref="AFMProductDetailsClientManager"/> class.
        /// </summary>
        /// <param name="runtime">The runtime.</param>
        /// <returns>
        /// A new instance of the Client manager with commerce runtime<see cref="AFMProductDetailsClientManager"/> class.
        /// </returns>
         public static AFMFulfillerInformationManager Create(CommerceRuntime runtime)
        {
            return new AFMFulfillerInformationManager(runtime);
        }

     /// <summary>
     /// This function calls the workflow to retrieve product details
     /// </summary>
     /// <param name="productIdsList"></param>
     /// <returns>Response with product details</returns>
         public List<AFMFulFillerInformation> GetAllFulfillerInformation()
        {

            var req = new AFMFulfillerInformationServiceRequest();
            var response = this._runtime.Execute<AFMFulfillerInformationServiceResponse>(req, this._runtime.CreateRequestContext(req));
            return response.FulfillerInformation;
            }
        }


    }
