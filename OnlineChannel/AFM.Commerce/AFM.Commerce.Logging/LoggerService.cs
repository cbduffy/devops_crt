﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace AFM.Commerce.Logging
{
    public class LoggerService : ILogger
    {
        private Logger mLogger;
        private EventViwerLogger mEventViwerLogger;


        public LoggerService()
        {
            mLogger = Logger.Instance;
            if (ConfigurationManager.AppSettings["IsLogRequired"].ToLower() == "on")
            {
                string eventLogName = (ConfigurationManager.AppSettings["EventLogName"] != null ? ConfigurationManager.AppSettings["EventLogName"] : "Application");
                string eventSourceName = (ConfigurationManager.AppSettings["EventLogSource"] != null ? ConfigurationManager.AppSettings["EventLogSource"] : "Application Error");
                if (!EventLog.Exists(eventLogName))
                {
                    if (!EventLog.SourceExists(eventSourceName))
                        EventLog.CreateEventSource(eventSourceName, "Application");
                    else
                        eventLogName = EventLog.LogNameFromSourceName(eventSourceName, ".");
                }
                else
                {
                    if (!EventLog.SourceExists(eventSourceName))
                        EventLog.CreateEventSource(eventSourceName, "Application");
                    else
                        eventLogName = EventLog.LogNameFromSourceName(eventSourceName, ".");
                }               
                mEventViwerLogger = new EventViwerLogger(eventLogName, eventSourceName);
                mLogger.RegisterObserver(mEventViwerLogger);
            }
        }
        public void ProcessLogMessage(string message)
        {
            mLogger.AddLogMessage(message + "\r\n");
        }

        public void ProcessLogMessage(Exception ex)
        {
            mLogger.AddLogMessage(ex);
        }
    }
}
