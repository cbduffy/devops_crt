﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Configuration;

namespace AFM.Commerce.Logging
{
    class EventViwerLogger : ILogger
    {
        #region Data
        private string mEventsourceName;
        private string mEventLogName;

        public string EventsourceName
        {
            get { return mEventsourceName; }
        }

        public string EventLogName
        {
            get { return mEventLogName; }
        }

        #endregion

        #region Constructor
        public EventViwerLogger(string EventLogName, string EventsourceName)
        {
            mEventLogName = EventLogName;
            mEventsourceName = EventsourceName;
        }
        #endregion

        #region ILogger Members

        public void ProcessLogMessage(string logMessage)
        {
            try
            {
                EventLog evntLog = new EventLog(mEventLogName);
                evntLog.Source = mEventsourceName;
                evntLog.WriteEntry(logMessage, EventLogEntryType.Error);
            }
            catch (Exception ex)
            {
                Console.Write(ex);
            }
        }
        #endregion
    }
}
