﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Logging
{
    public static class LoggerUtilities
    {
        public static LoggerService logService = new LoggerService();

        public static void ProcessLogMessage(string message)
        {
            logService.ProcessLogMessage(message + "\r\n");
        }

        public static void  ProcessLogMessage(Exception ex)
        {
            logService.ProcessLogMessage(ex);
        }
    }
}
