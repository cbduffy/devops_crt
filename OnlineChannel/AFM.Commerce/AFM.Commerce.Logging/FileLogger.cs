﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Logging
{
    class FileLogger : ILogger
    {
        #region Data
        private string mFileName;

        public string FileName
        {
            get { return mFileName; }
        }
        #endregion

        #region Constructor
        public FileLogger(string fileName)
        {
            mFileName = fileName;
        }
        #endregion

        #region ILogger Members

        public void ProcessLogMessage(string logMessage)
        {
            using (StreamWriter mLogFile = new StreamWriter(mFileName, true))
            {
                mLogFile.WriteLine(logMessage);
            }
        }
        #endregion
    }
}
