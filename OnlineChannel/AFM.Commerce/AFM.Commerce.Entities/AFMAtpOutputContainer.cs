﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    public class AFMAtpOutputContainer
    {
        public string ErrorMsg 
        {
            get;
            set;
        }
        public List<AFMATPItemGroupingResponse> ItemGroupings
        {
            get;
            set;
        }
    }
}
