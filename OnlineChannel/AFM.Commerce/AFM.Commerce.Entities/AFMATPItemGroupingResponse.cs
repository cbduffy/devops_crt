﻿/*NS Developed by ysrini for AFM_TFS_40686 dated 06/26/2014*/
namespace AFM.Commerce.Entities
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    public class AFMATPItemGroupingResponse
    {
        /// <summary>
        /// Gets or sets accountNumber
        /// </summary>
        [DataMember]
        public string AccountNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets items
        /// </summary>
        [DataMember]
        public List<AFMATPResponseItem> Items
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets shipTo
        /// </summary>
        [DataMember]
        public string ShipTo
        {
            get;
            set;
        }
    }
}
