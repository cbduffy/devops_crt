﻿namespace AFM.Commerce.Entities
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    /// <summary>
    /// Viewmodel for Atp request
    /// </summary>
    [DataContract]
    public class AFMATPRequest : Microsoft.Dynamics.Commerce.Runtime.Messages.Request
    {
        /// <summary>
        /// Gets or sets item Groupings
        /// </summary>
        [DataMember]
        public List<AFMATPItemGroupingRequest> ItemGroupings
        {
            get;
            set;
        }
    }
}
