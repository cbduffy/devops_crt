﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/26/2014*/

namespace AFM.Commerce.Entities
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Viewmodel for atp Item grouping request
    /// </summary>
    public class AFMATPItemGroupingRequest
    {
        /// <summary>
        /// Gets or sets accountNumber
        /// </summary>
        [DataMember]
        public string AccountNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets items
        /// </summary>
        [DataMember]
        public List<AFMATPRequestItem> Items
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets shipTo
        /// </summary>
        [DataMember]
        public string ShipTo
        {
            get;
            set;
        }


        /// <summary>
        /// Gets or sets Customer ZipCode
        /// </summary>
        [DataMember]
        public string CustomerZip
        {
            get;
            set;
        }
    }
}
