﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    public class AFMItemInfo
    {
        public string ItemId 
        {
            get;
            set;
        }
        public decimal Quantity 
        {
            get;
            set;
        }
        public bool IsSDSO 
        {
            get;
            set; 
        }
    }
}
