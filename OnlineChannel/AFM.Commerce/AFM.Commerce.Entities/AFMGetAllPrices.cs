﻿

namespace AFM.Commerce.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Collections.ObjectModel;
    public class AFMGetAllPrices
    {
        public ReadOnlyCollection<AFMAllAffiliationPrices> AFMAllAffiliationPrices { get; set; }

        public string Result { get; set; }
    }
}
