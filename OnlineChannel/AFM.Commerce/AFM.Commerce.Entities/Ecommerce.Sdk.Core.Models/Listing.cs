/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the data model for a listing.
    /// </summary>
    [DataContract]
    public partial class Listing
    {
        /// <summary>
        /// The listing identifier.
        /// </summary>
        [DataMember]
        public long ListingId { get; set; }

        /// <summary>
        /// The quantity.
        /// </summary>
        [DataMember]
        public int Quantity { get; set; }

        /// <summary>
        /// Indicates whether this listing is a kit.
        /// </summary>
        [DataMember]
        public bool IsKitVariant { get; set; }

        /// <summary>
        /// Indicates the numerical value of the gift card in the channel's currency.
        /// </summary>
        [DataMember]
        public decimal GiftCardAmount { get; set; }

        /// <summary>
        /// The product details.
        /// </summary>
        [DataMember]
        public string ProductDetails { get; set; }

        /// <summary>
        /// Gets or sets the details of the kit components if the present listing is that of a kit.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Need to be able to read and write to it for serialization/deserialization."), DataMember]
        public Collection<KitComponentInfo> KitComponentDetails { get; set; }

        /// <summary>
        /// Gets or sets the comment for this item.
        /// </summary>
        [DataMember]
        public string Comment { get; set; }      
    }
}