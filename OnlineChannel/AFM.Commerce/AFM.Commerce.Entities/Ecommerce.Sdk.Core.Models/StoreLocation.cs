/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a store location
    /// </summary>
    [DataContract]
    public class StoreLocation : StoreAddress
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StoreProductAvailability"/> class.
        /// </summary>
        public StoreLocation()
        {

        }

        /// <summary>
        /// Gets the channel id.
        /// </summary>
        [DataMember]
        public long ChannelId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the location latitude.
        /// </summary>
        [DataMember]
        public decimal Latitude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the location longitude.
        /// </summary>
        [DataMember]
        public decimal Longitude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the distance to the location.
        /// </summary>
        [DataMember]
        public string Distance
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the inventory location id.
        /// </summary>
        [DataMember]
        public string InventoryLocationId
        {
            get;
            set;
        }
    }
}
