﻿using Entities = AFM.Commerce.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    public partial class Listing
    {
        //NS Developed by  ysrini for "0230-FDD Catalogue availability based on ATP" dated 8/18/2014 
        /// <summary>
        /// The item Id.
        /// </summary>
        [DataMember]
        public string ItemId { get; set; }
        //NE Developed by  ysrini for "0230-FDD Catalogue availability based on ATP" dated 8/18/2014 

        //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
        [DataMember]
        public string AFMKitItemId { get; set; }

        [DataMember]
        public int AFMKitSequenceNumber { get; set; }

        [DataMember]
        public long AFMKitProductId { get; set; }

        [DataMember]
        public string AFMKitItemProductDetails { get; set; }

        [DataMember]
        public int AFMKitItemQuantity { get; set; }
        //NE - RxL   
    
        // NS by muthait dated 01Nov@014
        [DataMember]
        public int AFMItemType
        {
            get { return afmItemType; } 
            set 
            {
                if (afmItemType != value)
                    afmItemType = value;
            } 
        }

        private int afmItemType = (int)Entities.AFMItemType.Default;
        // NE by muthait dated 01NOV2014

        [DataMember]
        public int AFMMultipleQty { get; set; }

        [DataMember]
        public int AFMLowestQty { get; set; }

        [DataMember]
        public int AFMQuantityPerBox { get; set; }

        [DataMember]
        public Entities.AFMATPResponseItem AFMCartLineATP
        {
            get;
            set;
        }
    }
}
