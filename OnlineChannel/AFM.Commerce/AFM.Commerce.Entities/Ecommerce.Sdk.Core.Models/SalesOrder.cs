/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a completed sales order and its details.
    /// </summary>
    [Serializable]
    [DataContract]
    public partial class SalesOrder : Transaction
    {
        /// <summary>
        /// Instantiates a new instance of <see cref="SalesOrder"/> class.
        /// </summary>
        public SalesOrder()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SalesOrder"/> class with the 
        /// specified sales order items.
        /// </summary>
        /// <param name="items">A collection of sales order items.</param>
        public SalesOrder(Collection<TransactionItem> items)
            : base()
        {
            this.Items = (items != null ? items : new Collection<TransactionItem>());
        }

        /// <summary>
        /// Gets or sets the order id.
        /// </summary>
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the channel id.
        /// </summary>
        [DataMember]
        public long ChannelId { get; set; }

        /// <summary>
        /// Gets or sets the sales id.
        /// </summary>
        [DataMember]
        public string SalesId { get; set; }

        /// <summary>
        /// Gets or sets the order status.
        /// </summary>
        [DataMember]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the payment status.
        /// </summary>
        [DataMember]
        public int PaymentStatus { get; set; }

        /// <summary>
        /// Gets the items ordered.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly"), DataMember]
        public Collection<TransactionItem> Items { get; set; }

        /// <summary>
        /// Gets or sets the order placed date.
        /// </summary>
        [DataMember]
        public string OrderPlacedDate { get; set; }

        /// <summary>
        /// Gets or sets the requested delivery date.
        /// </summary>
        [DataMember]
        public string RequestedDeliveryDate { get; set; }

        /// <summary>
        /// Gets or sets the confirmation identifier.
        /// </summary>
        [DataMember]
        public string ConfirmationId { get; set; }
    }
}