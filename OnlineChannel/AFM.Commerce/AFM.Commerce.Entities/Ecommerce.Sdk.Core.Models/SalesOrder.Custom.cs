﻿using System.Runtime.Serialization;

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    public partial class SalesOrder
    {
        [DataMember]
        public string AFMBillingZipCode { get; set; }

        [DataMember]
        public string ReceiptEmail { get; set; }
  }
}
