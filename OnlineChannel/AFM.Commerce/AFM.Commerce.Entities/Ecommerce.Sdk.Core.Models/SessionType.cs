/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    /// <summary>
    /// Used to indicate the type of the web session.
    /// </summary>
    public enum SessionType
    {
        /// <summary>
        /// Session of an anonymous user.
        /// </summary>
        Anonymous,

        /// <summary>
        /// Session of a signed in user.
        /// </summary>
        SignedIn,

        /// <summary>
        /// Session could belong to either a signed-in or an anonymous user.
        /// </summary>
        All
    }
}