﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Describes the entry type of the loyalty reward point line.
    /// </summary>
    /// <remarks>
    /// Maps to RetailLoyaltyRewardPointEntryType base enum in AX.
    /// </remarks>
    [DataContract]
    public enum LoyaltyRewardPointEntryType
    {
        /// <summary>
        /// The default entry type. Should not be used.
        /// </summary>
        [EnumMemberAttribute]
        None = 0,

        /// <summary>
        /// Earn reward points.
        /// </summary>
        [EnumMemberAttribute]
        Earn = 1,

        /// <summary>
        /// Redeem reward points.
        /// </summary>
        [EnumMemberAttribute]
        Redeem = 2,
    }
}