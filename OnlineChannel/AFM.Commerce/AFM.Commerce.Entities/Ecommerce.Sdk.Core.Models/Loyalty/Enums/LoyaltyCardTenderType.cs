﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Describes the tender type of the loyalty card.
    /// </summary>
    /// <remarks>
    /// Maps to RetailLoyaltyTenderTypeBase base enum in AX.
    /// </remarks>
    [DataContract]
    public enum LoyaltyCardTenderType
    {
        /// <summary>
        /// The defult value indicating the card can only redeem reward points that are earned on itself.
        /// </summary>
        [EnumMemberAttribute]
        AsCardTender = 0,

        /// <summary>
        /// The card can redeem reward points that are earned on any cards of the same customer.
        /// </summary>
        [EnumMemberAttribute]
        AsContactTender = 1,

        /// <summary>
        /// The card can only earn reward points but not redeem.
        /// </summary>
        [EnumMemberAttribute]
        NoTender = 2,

        /// <summary>
        /// The card is blocked.
        /// </summary>
        [EnumMemberAttribute]
        Blocked = 3
    }
}