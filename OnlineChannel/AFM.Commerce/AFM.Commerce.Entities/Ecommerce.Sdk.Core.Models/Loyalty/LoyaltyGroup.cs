﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Runtime.Serialization;

    /// <summary>
    /// Presents a loyalty group which may or may not contain a list of loyalty tiers.
    /// </summary>
    [DataContract]
    [DebuggerDisplay("RecId = {RecordId}, NameColumn = {NameColumn}")]
    public class LoyaltyGroup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoyaltyGroup"/> class.
        /// </summary>
        public LoyaltyGroup()
        {
            this.LoyaltyTiers = new Collection<LoyaltyTier>();
            this.LoyaltyCardTiers = new Collection<LoyaltyCardTier>();
        }

        /// <summary>
        /// Gets the record identifier.
        /// </summary>
        [DataMember]
        public long RecordId { get; set; }

        /// <summary>
        /// Gets the loyalty group name.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets the loyalty group description.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the loyalty tiers.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Data transfer object.")]
        [DataMember]
        public IList<LoyaltyTier> LoyaltyTiers { get; set; }

        /// <summary>
        /// Gets or sets the loyalty card tiers.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Data transfer object.")]
        [DataMember]
        public IList<LoyaltyCardTier> LoyaltyCardTiers { get; set; }
    }
}