﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Diagnostics;
    using System.Runtime.Serialization;

    /// <summary>
    /// Presents a loyalty tier.
    /// </summary>
    /// <remarks>
    /// A loyalty tier belongs to one loyalty group. One loyalty group can have zero or more loyalty tiers (e.g. Silver Tier, Gold Tier).
    /// </remarks>
    [DataContract]
    [DebuggerDisplay("RecordId = {RecordId}, TierId = {TierId}")]
    public class LoyaltyTier
    {
        /// <summary>
        /// Gets the record identifier.
        /// </summary>
        [DataMember]
        public long RecordId { get; set; }

        /// <summary>
        /// Gets the tier description.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets the readable identifier of the loyalty tier.
        /// </summary>
        [DataMember]
        public string TierId { get; set; }

        /// <summary>
        /// Gets the tier level. Bigger number means higher level.
        /// </summary>
        [DataMember]
        public decimal TierLevel { get; set; }
    }
}