﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Diagnostics;
    using System.Runtime.Serialization;

    /// <summary>
    /// Presents a loyalty card.
    /// </summary>
    [DataContract]
    [DebuggerDisplay("RecId = {RecordId}, RewardPointId = {RewardPointId}")]
    public class LoyaltyRewardPoint
    {
        /// <summary>
        /// Gets the record identifier.
        /// </summary>
        [IgnoreDataMember]
        public long RecordId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets expiration time unit of the reward point.
        /// </summary>
        [IgnoreDataMember]
        public DayMonthYear ExpirationTimeUnit { get; set; }

        /// <summary>
        /// Gets expiration time value of the reward point.
        /// </summary>
        [IgnoreDataMember]
        public int ExpirationTimeValue { get; set; }

        /// <summary>
        /// Gets a value indicating whether the reward point is redeemable.
        /// </summary>
        [DataMember]
        public bool IsRedeemable { get; set; }

        /// <summary>
        /// Gets the redeem ranking.
        /// </summary>
        [IgnoreDataMember]
        public int RedeemRanking { get; set; }

        /// <summary>
        /// Gets the currency of the reward point if the reward point is amount type.
        /// </summary>
        [DataMember]
        public string RewardPointCurrency { get; set; }

        /// <summary>
        /// Gets or sets the readable identifier of the reward point.
        /// </summary>
        [DataMember]
        public string RewardPointId { get; set; }

        /// <summary>
        /// Gets the reward point unit type.
        /// </summary>
        [IgnoreDataMember]
        public LoyaltyRewardPointType RewardPointType { get; set; }

        /// <summary>
        /// Gets or sets the issued points.
        /// </summary>
        [DataMember]
        public decimal IssuedPoints { get; set; }

        /// <summary>
        /// Gets or sets the used points.
        /// </summary>
        [DataMember]
        public decimal UsedPoints { get; set; }

        /// <summary>
        /// Gets or sets the expired points.
        /// </summary>
        [DataMember]
        public decimal ExpiredPoints { get; set; }

        /// <summary>
        /// Gets or sets the active points.
        /// </summary>
        [DataMember]
        public decimal ActivePoints { get; set; }
    }
}