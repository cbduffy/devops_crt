﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.Diagnostics;
    using System.Runtime.Serialization;

    /// <summary>
    /// Presents a loyalty card.
    /// </summary>
    [DataContract]
    [DebuggerDisplay("RecId = {RecordId}, CardNumber = {CardNumber}")]
    public class LoyaltyCard
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoyaltyCard"/> class.
        /// </summary>
        public LoyaltyCard()
        {
            this.LoyaltyGroups = new Collection<LoyaltyGroup>();
            this.RewardPoints = new Collection<LoyaltyRewardPoint>();
        }

        /// <summary>
        /// Gets the record identifier.
        /// </summary>
        [IgnoreDataMember]
        public long RecordId { get; set; }

        /// <summary>
        /// Gets or sets the card number.
        /// </summary>
        [DataMember]
        [Key]
        public string CardNumber { get; set; }

        /// <summary>
        /// Gets or sets the card tender type.
        /// </summary>
        [DataMember]
        public LoyaltyCardTenderType CardTenderType { get; set; }

        /// <summary>
        /// Gets or sets the customer account number of loyalty card.
        /// </summary>
        [DataMember]
        public string CustomerAccount { get; set; }

        /// <summary>
        /// Gets the record identifier of the party of the card owner.
        /// </summary>
        [IgnoreDataMember]
        public long PartyRecordId { get; set; }

        /// <summary>
        /// Gets or sets the loyalty groups that the loyalty card belongs to.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Data transfer object.")]
        [DataMember]
        public IList<LoyaltyGroup> LoyaltyGroups { get; set; }

        /// <summary>
        /// Gets or sets the reward points on the loyalty card.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Data transfer object.")]
        [DataMember]
        public IList<LoyaltyRewardPoint> RewardPoints { get; set; }
    }
}