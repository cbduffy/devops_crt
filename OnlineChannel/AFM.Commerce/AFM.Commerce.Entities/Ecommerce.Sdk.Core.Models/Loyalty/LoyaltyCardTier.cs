﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Diagnostics;
    using System.Runtime.Serialization;

    /// <summary>
    /// Presents a loyalty card tier. A loyalty card may belong to a loyalty group (i.e. the default loyalty tier), or a specific loyalty tier.
    /// </summary>
    [DataContract]
    [DebuggerDisplay("RecordId = {RecordId}, ValidFrom = {ValidFrom}")]
    public class LoyaltyCardTier
    {
        /// <summary>
        /// Gets the record identifier.
        /// </summary>
        [DataMember]
        public long RecordId { get; set; }

        /// <summary>
        /// Gets the record identifier of the loyalty tier.
        /// </summary>
        [DataMember]
        public long LoyaltyTierRecordId { get; set; }

        /// <summary>
        /// Gets the readable identifier of the loyalty tier.
        /// </summary>
        [DataMember]
        public string TierId { get; set; }

        /// <summary>
        /// Gets the start date of the valid period.
        /// </summary>
        [DataMember]
        public DateTime ValidFrom { get; set; }

        /// <summary>
        /// Gets the start date of the valid period in a numeric string.
        /// </summary>
        [DataMember]
        public string ValidFromFriendly { get; set; }

        /// <summary>
        /// Gets the end date of the valid period.
        /// </summary>
        [DataMember]
        public DateTime ValidTo { get; set; }

        /// <summary>
        /// Gets the start date of the valid period in a numeric string.
        /// </summary>
        [DataMember]
        public string ValidToFriendly { get; set; }
    }
}