﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Presents a loyalty card.
    /// </summary>
    [DataContract]
    public class LoyaltyCardTransaction
    {
        /// <summary>
        /// Gets the identifier of the transaction.
        /// </summary>
        [DataMember]
        public string TransactionId { get; set; }

        /// <summary>
        /// Gets the amount or the quantity of the reward point.
        /// </summary>
        [DataMember]
        public decimal RewardPointAmountQuantity { get; set; }

        /// <summary>
        /// Gets the entry type of the reward point, e.g. Earn, Redeem.
        /// </summary>
        [DataMember]
        public LoyaltyRewardPointEntryType EntryType { get; set; }

        /// <summary>
        /// Gets the entry type of the reward point, e.g. Earn, Redeem.
        /// </summary>
        [DataMember]
        public string EntryTypeFriendly { get; set; }

        /// <summary>
        /// Gets the expiration date of the earned points.
        /// </summary>
        [DataMember]
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets the expiration date of the earned points.
        /// </summary>
        [DataMember]
        public string ExpirationDateFriendly { get; set; }

        /// <summary>
        /// Gets the entry date time of the transaction.
        /// </summary>
        [DataMember]
        public DateTime EntryDateTime { get; set; }

        /// <summary>
        /// Gets the entry date time of the transaction.
        /// </summary>
        [DataMember]
        public string EntryDate { get; set; }

        /// <summary>
        /// Gets the entry date time of the transaction.
        /// </summary>
        [DataMember]
        public string EntryTime { get; set; }

        /// <summary>
        /// Gets the channel name where the transaction occurred.
        /// </summary>
        [DataMember]
        public string ChannelName { get; set; }
    }
}