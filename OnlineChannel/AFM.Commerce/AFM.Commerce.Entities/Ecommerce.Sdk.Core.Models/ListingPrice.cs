/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Container for the price of an item.
    /// </summary>
    [Serializable]
    [DataContract]
    public class ListingPrice : IExtensibleDataObject
    {
        [NonSerialized]
        private ExtensionDataObject extensionData;

        /// <summary>
        /// Gets or sets the formatted currency string for the base sales price for the item on the listing.
        /// </summary>
        [DataMember]
        public string BasePriceWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the fthe base sales price for the item on the listing.
        /// </summary>
        [DataMember]
        public decimal BasePrice { get; set; }

        /// <summary>
        /// Gets or sets the formatted currency string for the trade agreement price for the listing.
        /// </summary>
        /// <remarks>
        /// If no trade agreements found, this is the BasePrice.
        /// </remarks>
        [DataMember]
        public string TradeAgreementPriceWithCurrency { get; set; }

        // <summary>
        /// Gets or sets the trade agreement price for the listing.
        /// </summary>
        /// <remarks>
        /// If no trade agreements found, this is the BasePrice.
        /// </remarks>
        [DataMember]
        public decimal TradeAgreementPrice { get; set; }

        /// <summary>
        /// Gets or sets the formatted currency string for the adjusted price for the listing.
        /// </summary>
        /// <remarks>
        /// This is the best of the TradeAgreementPrice and retail price adjustments.
        /// </remarks>
        [DataMember]
        public string AdjustedPriceWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the adjusted price for the listing.
        /// </summary>
        /// <remarks>
        /// This is the best of the TradeAgreementPrice and retail price adjustments.
        /// </remarks>
        [DataMember]
        public decimal AdjustedPrice { get; set; }

        /// <summary>
        /// Gets or sets the formatted currency string for the discounted price for the listing.
        /// </summary>        
        [DataMember]
        public string DiscountedPriceWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the discounted price for the listing.
        /// </summary>
        [DataMember]
        public decimal DiscountedPrice { get; set; }

        /// <summary>
        /// Gets or sets the listing id.
        /// </summary>
        [DataMember]
        public long ListingId { get; set; }

        /// <summary>
        /// Gets or sets the item identifier that this listing price applies to.
        /// </summary>
        [DataMember]
        public string ItemId { get; set; }

        /// <summary>
        /// Gets or sets the optional dimension identifier (for variant) that this listing price applies to.
        /// </summary>
        [DataMember]
        public string InventoryDimensionId { get; set; }

        /// <summary>
        /// Gets or sets the currency code.
        /// </summary>
        [DataMember]
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Gets or sets the date that the provided price is applicable to.
        /// </summary>
        [DataMember]
        public DateTime ActiveDate { get; set; }

        /// <summary>
        /// Gets or sets the unit of measure.
        /// </summary>
        [DataMember]
        public string UnitOfMeasure { get; set; }

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionData; }
            set { this.extensionData = value; }
        }
    }
}