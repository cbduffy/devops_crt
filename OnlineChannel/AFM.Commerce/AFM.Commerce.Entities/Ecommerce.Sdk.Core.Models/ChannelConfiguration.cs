/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the channel configuration.
    /// </summary> 
    [Serializable]
    [DataContract]
    public class ChannelConfiguration
    {
        /// <summary>
        /// Gets the record identifier for the channel.
        /// </summary>
        [DataMember]
        [Key]
        public long RecordId { get; set; }

        /// <summary>
        /// Gets the invent location.
        /// </summary>
        [DataMember]
        public string InventLocation { get; set; }

        /// <summary>
        /// Gets the currency.
        /// </summary>
        [DataMember]
        public string Currency { get; set; }

        /// <summary>
        /// Gets the company accounting currency.
        /// </summary>
        [DataMember]
        public string CompanyCurrency { get; set; }

        /// <summary>
        /// Gets a value indicating whether prices include sales tax.
        /// </summary>
        [DataMember]
        public bool PriceIncludesSalesTax { get; set; }

        /// <summary>
        /// Gets the country/region identifier of the channel.
        /// </summary>
        [DataMember]
        public string CountryRegionId { get; set; }

        /// <summary>
        /// Gets the default language identifier.
        /// </summary>
        [DataMember]
        public string DefaultLanguageId { get; set; }

        /// <summary>
        /// Gets the delivery mode code for pick up in store.
        /// </summary>
        [DataMember]
        public string PickupDeliveryModeCode { get; set; }

        /// <summary>
        /// Gets Bing maps api key required for pick up in store scenarios.
        /// </summary>
        [DataMember]
        public string BingMapsApiKey { get; set; }

        /// <summary>
        /// Gets the time zone identifier.
        /// </summary>
        [DataMember]
        public string TimeZoneInfoId { get; set; }

        /// <summary>
        /// Gets the Email delivery mode code.
        /// </summary>
        [DataMember]
        public string EmailDeliveryModeCode { get; set; }

        /// <summary>
        /// Gets the gift card item id.
        /// </summary>
        [DataMember]
        public string GiftCardItemId { get; set; }

        /// <summary>
        /// Gets/sets the template for the currency string.
        /// </summary>
        /// <value>
        /// The currency string template.
        /// </value>
        [DataMember]
        public string CurrencyStringTemplate { get; set; }
    }
}