/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// The transaction view model representation.
    /// </summary>
    [Serializable]
    [DataContract]
    public partial class Transaction : IExtensibleDataObject
    {
        [NonSerialized]
        private ExtensionDataObject extensionData;

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionData; }
            set { this.extensionData = value; }
        }

        /// <summary>
        /// Instantiates a new instance of <see cref="Transaction"/> class.
        /// </summary>
        public Transaction()
        {
            this.DiscountCodes = new Collection<string>();
        }

        /// <summary>
        /// Gets or sets the Discount Codes.
        /// </summary>
        [DataMember]
        public Collection<string> DiscountCodes { get; private set; }

        /// <summary>
        /// Gets or sets the Loyalty Card Number.
        /// </summary>
        [DataMember]
        public string LoyaltyCardId { get; set; }

        /// <summary>
        /// Gets or sets the cart sub total.
        /// </summary>
        [DataMember]
        public string SubtotalWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the cart's currency formatted total discount amount.
        /// </summary>
        [DataMember]
        public string DiscountAmountWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the numerical value of the cart's total discount amount.
        /// </summary>
        [DataMember]
        public decimal DiscountAmount { get; set; }

        /// <summary>
        /// Gets or sets the cart charges total.
        /// </summary>
        [DataMember]
        public string ChargeAmountWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the cart sales tax total.
        /// </summary>
        [DataMember]
        public string TaxAmountWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the currency formatted cart grand total.
        /// </summary>
        [DataMember]
        public string TotalAmountWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the numerical value of the cart grand total.
        /// </summary>
        [DataMember]
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Gets or sets the delivery mode identifier.
        /// </summary>
        [DataMember]
        public string DeliveryModeId { get; set; }

        /// <summary>
        /// Gets or sets the delivery mode description.
        /// </summary>
        [DataMember]
        public string DeliveryModeDescription { get; set; }

        /// <summary>
        /// Gets or sets the shipping address.
        /// </summary>
        [DataMember]
        public Address ShippingAddress { get; set; }
    }
}