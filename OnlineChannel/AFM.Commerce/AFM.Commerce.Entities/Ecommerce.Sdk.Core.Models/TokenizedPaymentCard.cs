/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The tokenized payment view model representation.
    /// </summary>
    [Serializable]
    [DataContract]
    public class TokenizedPaymentCard : IExtensibleDataObject
    {
        [NonSerialized]
        private ExtensionDataObject extensionData;

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenizedPaymentCard"/> class.
        /// </summary>
        public TokenizedPaymentCard()
        {
            this.PaymentAddress = new Address();
        }

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionData; }
            set { this.extensionData = value; }
        }

        /// <summary>
        /// Gets the payment address.
        /// </summary>
        [DataMember]
        public Address PaymentAddress { get; set; }

        /// <summary>
        /// Gets or sets the card type.
        /// </summary>
        [DataMember]
        public string CardType { get; set; }

        /// <summary>
        /// Gets or sets the card expiration month.
        /// </summary>
        [DataMember]
        public int ExpirationMonth { get; set; }

        /// <summary>
        /// Gets or sets the card expiration year.
        /// </summary>
        [DataMember]
        public int ExpirationYear { get; set; }

        /// <summary>
        /// Gets or sets the name on the card.
        /// </summary>
        [DataMember]
        public string NameOnCard { get; set; }

        /// <summary>
        /// Gets or sets the payment card token.
        /// </summary>
        [DataMember]
        public string CardToken { get; set; }

        /// <summary>
        /// Gets or sets the unique payment card identifier.
        /// </summary>
        [DataMember]
        public string UniqueCardId { get; set; }

        /// <summary>
        /// Gets or sets the masked card number.
        /// </summary>
        [DataMember]
        public string MaskedCardNumber { get; set; }

        //CS by spriya : DMND0010113 - Synchrony Financing online
        /// <summary>
        /// Gets or sets the promo desc on the card.
        /// </summary>
        [DataMember]
        public string SynchronyPromoDesc { get; set; }

        /// <summary>
        /// Gets or sets the promo desc on the card.
        /// </summary>
        [DataMember]
        public string FinancingOptionId { get; set; }
        //CE by spriya : DMND0010113 - Synchrony Financing online
    }
}