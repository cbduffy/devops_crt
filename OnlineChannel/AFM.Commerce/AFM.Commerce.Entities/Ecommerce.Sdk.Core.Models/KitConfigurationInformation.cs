/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Information for a specific kit configuration.
    /// </summary>
    [Serializable]
    [DataContract]
    public class KitConfigurationInformation : IExtensibleDataObject
    {
        [NonSerialized]
        private ExtensionDataObject extensionData;

        /// <summary>
        /// Initializes a new instance of the <see cref="KitConfigurationInformation"/> object.
        /// </summary>
        public KitConfigurationInformation()
            : base()
        {
        }

        /// <summary>
        /// Indicates the product identifier of a specific kit configuration.
        /// </summary>
        [DataMember]
        public long ProductIdentifier { get; set; }

        /// <summary>
        /// Indicates configuration identifier of a specific kit configuration.
        /// </summary>
        [DataMember]
        public string ConfigurationIdentifier { get; set; }

        /// <summary>
        /// Indicates the title of a specific kit configuration.
        /// </summary>
        [DataMember]
        public string Title { get; set; }

        /// <summary>
        /// Indicates the customer item number of a specific kit configuration.
        /// </summary>
        [DataMember]
        public string CustomerItemNumber { get; set; }

        /// <summary>
        /// Indicates the base price of a specific kit configuration.
        /// </summary>
        [DataMember]
        public string BasePriceWithCurrency { get; set; }

        /// <summary>
        /// Indicates the base price of a specific kit configuration.
        /// </summary>
        [DataMember]
        public decimal BasePrice { get; set; }

        /// <summary>
        /// Indicates the adjusted price of a specific kit configuration.
        /// </summary>
        [DataMember]
        public string AdjustedPriceWithCurrency { get; set; }

        /// <summary>
        /// Indicates the adjusted price of a specific kit configuration.
        /// </summary>
        [DataMember]
        public decimal AdjustedPrice { get; set; }

        /// <summary>
        /// Gets or sets the formatted currency string for the discounted price for a specific kit configuration.
        /// </summary>
        [DataMember]
        public string DiscountedPriceWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the discounted price for a specific kit configuration.
        /// </summary>
        [DataMember]
        public decimal DiscountedPrice { get; set; }

        /// <summary>
        /// Indiciates the number of units available for a specific kit configuration.
        /// </summary>
        [DataMember]
        public decimal AvailabilityCount { get; set; }

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionData; }
            set { this.extensionData = value; }
        }
    }
}