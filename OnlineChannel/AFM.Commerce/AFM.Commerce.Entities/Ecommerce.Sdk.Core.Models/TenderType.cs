/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;    
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Represents the tender types.
    /// </summary>
    [DataContract]
    public enum TenderType
    {
        /// <summary>
        /// Credit card tender type operation id.
        /// </summary>
        [EnumMember]
        PayCard = (int)CrtDataModel.RetailOperation.PayCard,

        /// <summary>
        /// Gift card tender type operation id.
        /// </summary>
        [EnumMember]
        PayGiftCard = (int)CrtDataModel.RetailOperation.PayGiftCertificate,

        /// <summary>
        /// Loyalty card tender type operation id.
        /// </summary>
        [EnumMember]
        PayLoyaltyCard = (int)CrtDataModel.RetailOperation.PayLoyalty,
    }
}