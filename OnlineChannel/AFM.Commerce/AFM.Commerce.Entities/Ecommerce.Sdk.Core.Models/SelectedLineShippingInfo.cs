/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the shipping address that is being set for a sales line.
    /// </summary>
    [Serializable]
    [DataContract]
    public class SelectedLineShippingInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SelectedLineShippingInfo"/> class.
        /// </summary>
        public SelectedLineShippingInfo()
        {
        }

        /// <summary>
        /// Gets or sets the item id.
        /// </summary>
        [DataMember]
        public string LineId { get; set; }

        /// <summary>
        /// Gets or sets the item quantity.
        /// </summary>
        [DataMember]
        public Address ShipToAddress { get; set; }
    }
}