/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.Runtime.Serialization;

    /// <summary>
    /// Wish list view model representation.
    /// </summary>
    [DataContract]
    [Serializable]
    public class WishList
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WishList"/> class.
        /// </summary>
        public WishList()
        {
            this.WishListLines = new Collection<WishListLine>();
        }

        /// <summary>
        /// Gets or sets the wishlist identifier.
        /// </summary>
        [DataMember]
        [Key]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name associated with the wishlist.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the wishlist line customer.
        /// </summary>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the wish list is favorite.
        /// </summary>
        [DataMember]
        public bool? IsFavorite { get; set; }

        /// <summary>
        /// Gets or sets the wish list lines.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Changing wish lists by adding, removing, and updating wish list lines is supported.")]
        [DataMember]
        public Collection<WishListLine> WishListLines { get; set; }
    }
}