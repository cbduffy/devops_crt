/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The payment view model representation.
    /// </summary>
    [Serializable]
    [DataContract]
    public class TenderDataLine : IExtensibleDataObject
    {
        [NonSerialized]
        private ExtensionDataObject extensionData;

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionData; }
            set { this.extensionData = value; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TenderDataLine"/> class.
        /// </summary>
        public TenderDataLine()
        {
        }

        /// <summary>
        /// Gets or sets the numerical value of the amount to be paid.
        /// </summary>
        [DataMember]
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets the gift card identifier.
        /// </summary>
        [DataMember]
        public string GiftCardId { get; set; }

        /// <summary>
        /// Gets or sets the loyalty card identifier.
        /// </summary>
        [DataMember]
        public string LoyaltyCardId { get; set; }

        /// <summary>
        /// Gets or sets the card type.
        /// </summary>
        /// <value>
        /// The payment card.
        /// </value>
        [DataMember]
        public Payment PaymentCard { get; set; }

        /// <summary>
        /// Gets or sets the tokenized payment card.
        /// </summary>
        /// <value>
        /// The tokenized payment card.
        /// </value>
        [DataMember]
        public TokenizedPaymentCard TokenizedPaymentCard { get; set; }

    }
}