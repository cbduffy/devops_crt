/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the delivery preference types available for the provided sales line identifier.
    /// </summary>
    [DataContract]
    public class LineDeliveryPreference
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LineDeliveryPreference"/> class.
        /// </summary>
        public LineDeliveryPreference()
        {
        }

        /// <summary>
        /// Gets the sales line identifier for which the delivery preferences are computed.
        /// </summary>
        /// <value>
        /// The sales line identifier.
        /// </value>
        [DataMember]
        public string LineId { get; set; }

        /// <summary>
        /// Gets the collection of delivery preferences available for this sales line.
        /// </summary>
        /// <value>
        /// The delivery preferences.
        /// </value>
        [DataMember]
        public IEnumerable<DeliveryPreferenceType> DeliveryPreferenceTypes { get; set; }
    }
}