/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;
    using CrtDataModel = Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Defines the available delivery preferences.
    /// </summary>
    [Serializable]
    [DataContract]
    public enum DeliveryPreferenceType
    {
        /// <summary>
        /// Represents the case when the delivery preference type has not been set.
        /// </summary>
        [EnumMember]
        None = (int) CrtDataModel.DeliveryPreferenceType.None,

        /// <summary>
        /// Represents the case when the item can be shipped to an address.
        /// </summary>
        [EnumMember]
        ShipToAddress = (int) CrtDataModel.DeliveryPreferenceType.ShipToAddress,

        /// <summary>
        /// Represents the case when the item can be electronically delivered over e-mail.
        /// </summary>
        [EnumMember]
        PickupFromStore = (int)CrtDataModel.DeliveryPreferenceType.PickupFromStore,

        /// <summary>
        /// Represents the case when the item can be picked up from a store.
        /// </summary>
        [EnumMember]
        ElectronicDelivery = (int)CrtDataModel.DeliveryPreferenceType.ElectronicDelivery,

        /// <summary>
        /// Represents the case when the items can be delivered individually. Applicable only to cart header level delivery preferences.
        /// </summary>
        [EnumMember]
        DeliverItemsIndividually = (int)CrtDataModel.DeliveryPreferenceType.DeliverItemsIndividually
    }
}