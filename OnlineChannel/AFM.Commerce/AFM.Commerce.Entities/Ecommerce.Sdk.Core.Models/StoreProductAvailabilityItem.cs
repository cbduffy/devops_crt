/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An item available in a store.
    /// </summary>
    [DataContract]
    public class StoreProductAvailabilityItem
    {
        /// <summary>
        /// Gets or sets the record identifier.
        /// </summary>
        /// <value>
        /// The record identifier.
        /// </value>
        [DataMember]
        public long RecordId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the item ID.
        /// </summary>
        /// <value>
        /// The item ID.
        /// </value>
        [DataMember]
        public string ItemId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the variant inventory dimension ID.
        /// </summary>
        /// <value>
        /// The variant inventory dimension ID.
        /// </value>
        [DataMember]
        public string VariantInventoryDimensionId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the warehouse inventory dimension ID.
        /// </summary>
        /// <value>
        /// The warehouse inventory dimension ID.
        /// </value>
        [DataMember]
        public string WarehouseInventoryDimensionId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the inventory location ID.
        /// </summary>
        /// <value>
        /// The inventory location ID.
        /// </value>
        [DataMember]
        public string InventoryLocationId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the available quantity.
        /// </summary>
        /// <value>
        /// The available quantity.
        /// </value>
        [DataMember]
        public decimal AvailableQuantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the product details.
        /// </summary>
        [DataMember]
        public string ProductDetails
        {
            get;
            set;
        }
    }
}