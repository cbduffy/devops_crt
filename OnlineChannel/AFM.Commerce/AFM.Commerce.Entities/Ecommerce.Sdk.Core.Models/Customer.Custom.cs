﻿using System.Runtime.Serialization;
using System;
namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    public sealed partial class Customer 
    {
        [DataMember]
        public string SourceInfo { get; set; }

        [DataMember]
        public bool IsMarketingOptIn { get; set; }

        [DataMember]
        public bool IsGuestCheckout { get; set; }

        [DataMember]
        public bool IsOlderThan13 { get; set; }

        //NS developed by v-hapat for CR 72099 
        [DataMember]
        public string IsMarketingOptInUniqueId { get; set; }
        [DataMember]
        public string IsMarketingOptInVersionId { get; set; }
        [DataMember]
        public DateTime IsMarketingOptInDate { get; set; }
        //NS developed by v-hapat for CR 72099 
    }
}
