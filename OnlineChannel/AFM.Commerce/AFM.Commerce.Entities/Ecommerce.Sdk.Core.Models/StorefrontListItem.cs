/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Encapsulates information for a storefront list item.
    /// </summary>
    [DataContract]
    public class StorefrontListItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StorefrontListItem"/> class.
        /// </summary>
        public StorefrontListItem()
        {
        }

        /// <summary>
        /// Gets or sets the item number.
        /// </summary>
        [DataMember]
        public string ItemNumber { get; set; }

        /// <summary>
        /// Gets or sets the customer item number.
        /// </summary>
        [DataMember]
        public string CustomerItemNumber { get; set; }

        /// <summary>
        /// Gets or sets the size.
        /// </summary>
        [DataMember]
        public string Size { get; set; }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        [DataMember]
        public string Color { get; set; }

        /// <summary>
        /// Gets or sets the style.
        /// </summary>
        [DataMember]
        public string Style { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        [DataMember]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        [DataMember]
        public string Image { get; set; }

        /// <summary>
        /// Gets or sets the group number.
        /// </summary>
        [DataMember]
        public string GroupNumber { get; set; }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        [DataMember]
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the review rating.
        /// </summary>
        [DataMember]
        public string ReviewRating { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the specification.
        /// </summary>
        [DataMember]
        public string Specification { get; set; }

        /// <summary>
        /// Gets or sets the identifiers of the kit masters that the item belongs to.
        /// </summary>
        [DataMember]
        public string ParentKitId { get; set; }

        /// <summary>
        /// Gets or sets the identifiers of the kit masters that contain the item as a default component.
        /// </summary>
        [DataMember]
        public string KitDefaultComponent { get; set; }

        /// <summary>
        /// Gets or sets key value pairs of line identifiers and parent kit identifiers.
        /// </summary>
        [DataMember]
        public string KitComponentIndex { get; set; }

        /// <summary>
        /// Gets or sets key value pairs that indicate miscellaneous component properties such as UoM, price, etc.
        /// </summary>
        [DataMember]
        public string KitComponentProperties { get; set; }
    }
}