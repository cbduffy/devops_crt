﻿using System.Runtime.Serialization;

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    public partial class Address
    {
        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Street2 { get; set; }

        [DataMember]
        public string Email2 { get; set; }

        [DataMember]
        public string Phone2 { get; set; }

        [DataMember]
        public string Phone3 { get; set; }

        [DataMember]
        public string Phone1Extension { get; set; }

        [DataMember]
        public string Phone2Extension { get; set; }

        [DataMember]
        public string Phone3Extension { get; set; }
    }
}
