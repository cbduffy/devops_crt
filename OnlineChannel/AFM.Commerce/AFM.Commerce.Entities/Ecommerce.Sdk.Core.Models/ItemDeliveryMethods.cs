/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// A single delivery method's view model representation. (FEDEX, UPS, etc).
    /// </summary>
    [Serializable]
    [DataContract]
    public class LineDeliveryOption : IExtensibleDataObject
    {
        [NonSerialized]
        private ExtensionDataObject extensionData;

        /// <summary>
        /// Initializes a new instance of the <see cref="LineDeliveryOption"/> object.
        /// </summary>
        public LineDeliveryOption()
            : this(null, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LineDeliveryOption"/> object.
        /// </summary>
        /// <param name="lineId">The line identifier.</param>
        public LineDeliveryOption(string lineId)
            : this(lineId, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LineDeliveryOption"/> object.
        /// </summary>
        /// <param name="lineId">The line identifier.</param>
        /// <param name="deliveryOptions">The read only collection of delivery options.</param>
        public LineDeliveryOption(string lineId, Collection<DeliveryOption> deliveryOptions)
        {
            this.LineId = lineId ?? string.Empty;
            this.DeliveryOptions = deliveryOptions ?? new Collection<DeliveryOption>();
        }

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionData; }
            set { this.extensionData = value; }
        }

        /// <summary>
        /// Gets or sets the line identifier.
        /// </summary>
        [DataMember]
        public string LineId { get; private set; }

        /// <summary>
        /// Gets the delivery options.
        /// </summary>
        [DataMember]
        public Collection<DeliveryOption> DeliveryOptions { get; private set; }
    }
}