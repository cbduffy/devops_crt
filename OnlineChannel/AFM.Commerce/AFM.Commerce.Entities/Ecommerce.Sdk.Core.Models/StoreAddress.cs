/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The store address view model representation.
    /// </summary>
    [Serializable]
    [DataContract]
    public class StoreAddress : Address
    {
        /// <summary>
        /// Gets or sets the store Id.
        /// </summary>
        [DataMember]
        public string StoreId { get; set; }

        /// <summary>
        /// Gets or sets the store name.
        /// </summary>
        [DataMember]
        public string StoreName { get; set; }

        /// <summary>
        /// Gets or sets the location record id.
        /// </summary>
        [DataMember]
        public long PostalAddressId { get; set; }
    }
}
