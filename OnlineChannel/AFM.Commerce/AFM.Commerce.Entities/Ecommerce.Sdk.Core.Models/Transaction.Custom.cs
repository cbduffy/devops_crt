﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using AFM.Commerce.Entities;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;
    public partial class Transaction
    {

        /* ND start by muthait*/
        /// <summary>
        /// Gets or sets the cart sub total.
        /// </summary>
        [DataMember]
        public string Subtotal { get; set; }

        
        /// <summary>
        /// Gets or sets the cart sub total.
        /// </summary>
        [DataMember]
        public string EstShipping { get; set; }

        /// <summary>
        /// Gets or sets the cart sub total.
        /// </summary>
        [DataMember]
        public string EstHomeDelivery { get; set; }


        /// <summary>
        /// Gets or sets the cart EstShippingDiscount.
        /// </summary>
        [DataMember]
        public string EstShippingDiscount { get; set; }

        /// <summary>
        /// Gets or sets the cart EstHomeDeliveryDiscount.
        /// </summary>
        [DataMember]
        public string EstHomeDeliveryDiscount { get; set; }
        /* ND end by muthait */
        //TO DO spriya merged for upgrade
        //NS by muthait dated 21Sep2014
        /// <summary>
        /// Gets or sets the avalara tax error.
        /// </summary>
        [DataMember]
        public string AvalaraTaxError { get; set; }

        /// <summary>
        /// Gets or sets the ATP error.
        /// </summary>
        [DataMember]
        public string ATPError { get; set; }

        /// <summary>
        /// Gets or sets the ATP error.
        /// </summary>
        [DataMember]
        public string ChargeTypeError { get; set; }

        // NS CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey
        /// <summary>
        /// Versions of text used for survey and terms and conditions
        /// </summary>
        [DataMember]
        public string SurveyTermsAndConditionsVersions { get; set; }

        /// Is the customer accepting the terms and conditions
        /// </summary>
        [DataMember]
        public bool IsAcceptTermsAndConditions;
        // NE CR - 117 - New Sales Order fields needed for storing versioning of the acceptance of Terms and OK to Survey

        // NS by muthait dated 02NOV2014
        /// Is the customer accepting the terms and conditions
        /// </summary>
        [DataMember]
        public int CartCount;
        // NE by muthait dated 02NOV2014

        //Rakesh
        [DataMember]
        public Collection<TenderDataLine> TenderLines { get; set; }

        /// <summary>
        /// Gets or sets the cart CustomerId.
        /// </summary>
        [DataMember]
        public string CustomerId { get; set; }


        /// <summary>
        /// Gets or sets the billing address.
        /// </summary>
        [DataMember]
        public Address BillingAddress { get; set; }

        //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
        /// <summary>
        /// Gets or sets the cart RSA ID.
        /// </summary>
        [DataMember]
        public string RsaId { get; set; }
        //NE - RxL

        //NS by spriya : DMND0010113 - Synchrony Financing online
        [DataMember]
        public string AfmFinancingOptionId{get;set;}

        [DataMember]
        public bool RemoveCartDiscount { get; set; }

        [DataMember]
        public string AFMCardName { get; set; }

        [DataMember]
        public string AFMPromoDesc { get; set; }
        //NE by spriya : DMND0010113 - Synchrony Financing online
    }
}
