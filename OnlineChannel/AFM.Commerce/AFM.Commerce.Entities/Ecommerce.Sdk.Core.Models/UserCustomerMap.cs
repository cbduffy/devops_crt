/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Globalization;

    /// <summary>
    /// A user customer mapping.
    /// </summary>
    public class UserCustomerMap
    {
        /// <summary>
        /// Gets or sets the User id of the mapping.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the customer id of the mapping.
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the one-time-use token of the mapping.
        /// </summary>
        public string SingleUseToken { get; set; }

        /// <summary>
        /// Gets or sets the datetime of when an pending email was sent.
        /// </summary>
        public DateTime? EmailSentDateTime { get; set; }

        /// <summary>
        /// Gets or sets the datetime when the mapping was verified and set to active.
        /// </summary>
        public DateTime? CustomerVerifiedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the status of the mapping.
        /// </summary>
        public UserCustomerMapStatus Status { get; set; }

        /// <summary>
        /// Additional data that needs to be stored. Used during pending state.
        /// </summary>
        public string XmlData { get; set; }

        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "UserId: '{0}', CustomerId: '{1}', Status: '{2}'", this.UserId, this.CustomerId, this.Status);
        }
    }
}
