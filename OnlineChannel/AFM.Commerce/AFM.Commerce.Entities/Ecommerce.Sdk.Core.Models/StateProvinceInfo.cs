/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The state/province info view model representation.
    /// </summary>
    [Serializable]
    [DataContract]
    public class StateProvinceInfo
    {
        /// <summary>
        /// Gets or sets the country region id.
        /// </summary>
        [DataMember]
        public string CountryRegionId { get; set; }

        /// <summary>
        /// Gets or sets the state or province id.
        /// </summary>
        [DataMember]
        public string StateId { get; set; }

        /// <summary>
        /// Gets or sets the state or province name.
        /// </summary>
        [DataMember]
        public string StateName { get; set; }
    }
}