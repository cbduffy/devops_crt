/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the selected delivery options for an order.
    /// </summary>
    [Serializable]
    [DataContract]
    public class SelectedDeliveryOption
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SelectedDeliveryOption"/> class.
        /// </summary>
        public SelectedDeliveryOption()
        {
            // Objects needs to be always present for the client side to 
            // bind to.
            this.CustomAddress = new Address();
            this.StoreAddress = new StoreProductAvailability();
        }

        /// <summary>
        /// Gets or sets the selected delivery method.
        /// </summary>
        [DataMember]
        public string DeliveryModeId { get; set; }

        /// <summary>
        /// Gets or sets the selected shipping option.
        /// </summary>
        [DataMember]
        public string DeliveryPreferenceId { get; set; }

        /// <summary>
        ///  Gets or sets the custom address
        /// </summary>
        /// <remarks>This is supposed to be set with the equivalent of a home delivery.</remarks>
        [DataMember]
        public Address CustomAddress { get; set; }

        /// <summary>
        /// Gets or sets the store address.
        /// </summary>
        /// <remarks>This is supposed to be set with in-store pickup.</remarks>
        [DataMember]
        public StoreProductAvailability StoreAddress { get; set; }

        /// <summary>
        /// Gets or sets the electronic delivery email address.
        /// </summary>
        /// <remarks>This is supposed to be set with email delivery.</remarks>
        [DataMember]
        public string ElectronicDeliveryEmail { get; set; }

        /// <summary>
        /// Gets or sets the electronic delivery email content.
        /// </summary>
        /// <remarks>This is supposed to be set with email delivery.</remarks>
        [DataMember]
        public string ElectronicDeliveryEmailContent { get; set; }
    }
}
