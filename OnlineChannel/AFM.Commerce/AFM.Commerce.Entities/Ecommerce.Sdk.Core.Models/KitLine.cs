/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Encapsulates information for a kit line.
    /// </summary>
    [DataContract]
    public class KitLine
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KitLine"/> class.
        /// </summary>
        public KitLine()
        {
        }

        /// <summary>
        /// Gets or sets the line identifier.
        /// </summary>
        [DataMember]
        public long LineId { get; set; }

        /// <summary>
        /// Gets or sets the product identifier.
        /// </summary>
        [DataMember]
        public long ProductId { get; set; }
    }
}