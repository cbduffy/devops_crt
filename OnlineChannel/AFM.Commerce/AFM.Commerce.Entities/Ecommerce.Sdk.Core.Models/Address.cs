/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Configuration;
    using System.Runtime.Serialization;

    /// <summary>
    /// Customer Address Type. These should stay in sync w/the DM enum and AX.
    /// </summary>
    [DataContract]
    public enum AddressType : int
    {
        /// <summary>
        /// Address type not specified.
        /// </summary>
        [EnumMember]
        None = 0,

        //NS Developed by spriya
        /// <summary>
        /// AX/Invoice
        /// </summary>
        [EnumMember]
        Invoice = 1,


        /// <summary>
        /// AX/Delivery 
        /// </summary>
        [EnumMember]
        Delivery = 2,

        /// <summary>
        /// AX/Payment 
        /// </summary>
        [EnumMember]
        Payment = 5,
    }

    /// <summary>
    /// The address view model representation.
    /// </summary>
    [Serializable]
    [DataContract]
    public partial class Address : IExtensibleDataObject
    {
        /// <summary>
        /// Represents name of configuration app settings key which stores Country Name.
        /// </summary>
        private const string TargetCountryCodePropertyName = "StoreFront_CountryCode";

        [NonSerialized]
        private ExtensionDataObject extensionData;

        /// <summary>
        /// Initializes a new instance of the <see cref="Address"/> class.
        /// </summary>
        public Address()
        {
            this.Country = ConfigurationManager.AppSettings[TargetCountryCodePropertyName];
        }

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionData; }
            set { this.extensionData = value; }
        }

        /// <summary>
        /// Gets or sets the Country.
        /// </summary>
        [DataMember]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the State.
        /// </summary>
        [DataMember]
        public string State { get; set; }

        /// <summary>
        /// Gets or sets County.
        /// </summary>
        [DataMember]
        public string County { get; set; }

        /// <summary>
        /// Gets or sets the City.
        /// </summary>
        [DataMember]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the DistrictName.
        /// </summary>
        [DataMember]
        public string DistrictName { get; set; }

        /// <summary>
        /// Gets or sets the Attention To.
        /// </summary>
        [DataMember]
        public string AttentionTo { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Street.
        /// </summary>
        [DataMember]
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the StreetNumber.
        /// </summary>
        [DataMember]
        public string StreetNumber { get; set; }

        /// <summary>
        /// Gets or sets the ZipCode.
        /// </summary>
        [DataMember]
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets the Phone.
        /// </summary>
        [DataMember]
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the Email.
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the Email content.
        /// </summary>
        [DataMember]
        public string EmailContent { get; set; }

        /// <summary>
        /// Gets or sets the RecordId.
        /// </summary>
        [DataMember]
        public long RecordId { get; set; }

        /// <summary>
        /// Gets or sets the Deactivate indicator.
        /// </summary>
        [DataMember]
        public bool Deactivate { get; set; }

        /// <summary>
        /// Gets or sets the Deactivate indicator.
        /// </summary>
        [DataMember]
        public bool IsPrimary { get; set; }

        /// <summary>
        /// Gets or sets the AddressType for this Address.
        /// </summary>
        [DataMember]
        public AddressType AddressType { get; set; }

        /// <summary>
        /// Gets or sets the adddress friendly name.
        /// </summary>
        [DataMember]
        public string AddressFriendlyName { get; set; }
    }
}

