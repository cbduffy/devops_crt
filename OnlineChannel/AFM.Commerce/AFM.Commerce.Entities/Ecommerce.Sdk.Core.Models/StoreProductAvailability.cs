/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a store product availability.
    /// </summary>
    [DataContract]
    public sealed class StoreProductAvailability : StoreLocation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StoreProductAvailability"/> class.
        /// </summary>
        public StoreProductAvailability()
        {
            this.ProductAvailabilities = new Collection<StoreProductAvailabilityItem>();
        }

        /// <summary>
        /// Gets the item quantity
        /// </summary>
        [DataMember]
        public Collection<StoreProductAvailabilityItem> ProductAvailabilities
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the flag allowing store selection.
        /// </summary>
        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "value", Justification = "Necessary for serialization.")]
        [DataMember]
        public string SelectDisabled
        {
            get
            {
                // Allow select only if all items available.
                return (this.ProductAvailabilities == null || this.ProductAvailabilities.Any(item => item.AvailableQuantity < 1)) ? "disabled" : "";
            }

            set { }
        }
    }
}
