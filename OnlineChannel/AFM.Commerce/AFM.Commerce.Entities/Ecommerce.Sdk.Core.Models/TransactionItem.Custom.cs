﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Entities;

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    //NS by spriya : DMND0010113 - Synchrony Financing online
    //Enum for custom card types as Retail SDK supports limited enum types
    [DataContract]
    public enum PaymentMethodType : int
    {
        [EnumMember]
        CreditCard = 1,

        [EnumMember]
        Synchrony = 2
    }
    //NS by spriya : DMND0010113 - Synchrony Financing online
    public partial class TransactionItem
    {
        //NS Developed by muthait for AFM_TFS_40686 dated 06/24/2014

        /// <summary>
        /// Gets or sets the Vendor ID.
        /// </summary>
        [DataMember]
        public string AFMVendorId { get; set; }

        /// <summary>
        /// Gets or sets the Vendor ID.
        /// </summary>
        [DataMember]
        public bool IsEcommOwned { get; set; }

        /// <summary>
        /// Gets or sets the Vendor Name.
        /// </summary>
        [DataMember]
        public string AFMVendorName { get; set; }

        /// <summary>
        /// Gets or sets item ShippingMode
        /// </summary>
        [DataMember]
        public string AFMShippingMode
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets item ShippingCost
        /// </summary>
        [DataMember]
        public decimal AFMShippingCost
        {
            get;
            set;
        }
        //NS Developed by muthait for AFM_TFS_40686 dated 08/31/2014

        /// <summary>
        /// Gets or sets item Groupings
        /// </summary>
        [DataMember]
        public string PriceAfterDiscount
        {
            get;
            set;
        }
        //NE Developed by muthait for AFM_TFS_40686 dated 08/31/2014

        //NS Developed by Srinivas for AFM_TFS_40686 dated 06/25/2014
        /// <summary>
        /// Gets or sets item Groupings
        /// </summary>
        [DataMember]
        public AFMATPResponseItem AFMCartLineATP
        {
            get;
            set;
        }

        //NS - RxL - FDD0238-ECS-Manage Shopping Cart v3 0
        [DataMember]
        public string AFMKitItemId { get; set; }

        [DataMember]
        public long AFMKitProductId { get; set; }

        [DataMember]
        public int AFMKitSequenceNumber { get; set; }

        [DataMember]
        public string AFMKitItemProductDetails { get; set; }

        [DataMember]
        public int AFMKitItemQuantity { get; set; }

        [DataMember]
        public string AFMUnit { get; set; }

        [DataMember]
        public string LineOrderNumber { get; set; }

        //NE - RxL

        //NE Developed by Srinivas for AFM_TFS_40686 dated 06/25/2014

        //NS CR - 137 - Addendum Design addition to Manage Shopping Cart FDD - Multiple Qty of Chair in a box

        /// <summary>
        /// Gets or sets item multiple qty
        /// </summary>
        [DataMember]
        public decimal AFMMultipleQty
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets item minimum order qty
        /// </summary>
        [DataMember]
        public decimal AFMLowestQty
        {
            get;
            set;
        }

        /// <summary>
        /// Get or Sets the quantity options for multiple qty set items
        /// </summary>
        [DataMember]
        public Collection<decimal> AFMQtyOptions { get; set; }

        [DataMember]
        public decimal AFMQtyPerBox { get; set; }

        [DataMember]
        public string AFMLineConfirmationNumber { get; set; }

        [DataMember]
        public int AFMDirectShip { get; set; }

        [DataMember]
        public int AFMSupplierDirectShip { get; set; }

        [DataMember]
        public int AFMItemType { get; set; }

        //NS by RxL - FDD0258-ECS-Review Customer Profile History Wish List V3 Phased-signed
        [DataMember]
        public DateTime AFMScheduledDeliveryDate { get; set; }

        [DataMember]
        public DateTime AFMActualDeliveryDate { get; set; }

        [DataMember]
        public string AFMCarrier { get; set; }

        [DataMember]
        public string AFMTrackingNumber { get; set; }

        [DataMember]
        public DateTime AFMShippingDate { get; set; }

        [DataMember]
        public string AFMModelId { get; set; }
        //NE by RxL    

        [DataMember]
        public string AFMColor { get; set; }

        [DataMember]
        public string AFMSize { get; set; }

        //NS by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
        [DataMember]
        public decimal AFMNetAmount { get; set; }

        [DataMember]
        public decimal AFMTaxAmount { get; set; }

        [DataMember]
        public int AFMLineConfirmationGroup { get; set; }
        //NS by RxL
        //NS by spriya : DMND0010113 - Synchrony Financing online
        //[DataMember]
        public string AfmFinancingOptionId { get; set; }

        public bool RemoveCartDiscount { get; set; }

        public string CardName { get; set; }

        public string PromoDesc { get; set; }
        //NE by spriya : DMND0010113 - Synchrony Financing online


    }
}
