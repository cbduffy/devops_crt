/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the delivery preferences available based on items in the cart.
    /// </summary>
    [Serializable]
    [DataContract]
    public class CartDeliveryPreferences
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CartDeliveryPreferences"/> class.
        /// </summary>
        public CartDeliveryPreferences()
        {
        }

        /// <summary>
        /// Gets the collection of delivery preferences that have been computed for the all the items in the cart combined.
        /// </summary>
        /// <value>
        /// The header delivery preferences.
        /// </value>
        [DataMember]
        public IEnumerable<DeliveryPreferenceType> HeaderDeliveryPreferenceTypes { get; set; }

        /// <summary>
        /// Gets the collection of delivery preferences that have been computed at line level.
        /// </summary>
        /// <value>
        /// The line delivery preferences.
        /// </value>
        [DataMember]
        public IEnumerable<LineDeliveryPreference> LineDeliveryPreferences { get; set; }
    }
}