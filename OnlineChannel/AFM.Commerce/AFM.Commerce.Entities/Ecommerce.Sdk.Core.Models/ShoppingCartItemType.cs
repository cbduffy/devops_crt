/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the transaction item type.
    /// </summary>
    [DataContract]
    public enum TransactionItemType
    {
        /// <summary>
        /// None (indicates that the transaction item is neither a kit nor a kit component).
        /// </summary>
        [EnumMember]
        None,

        /// <summary>
        /// Indicates that the transaction item is a kit.
        /// </summary>
        [EnumMember]
        Kit,

        /// <summary>
        /// Indicates that the transaction item is a kit component.
        /// </summary>
        [EnumMember]
        KitComponent
    }
}
