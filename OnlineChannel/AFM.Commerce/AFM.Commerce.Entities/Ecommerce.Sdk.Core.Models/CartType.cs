/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the shopping cart type.
    /// </summary>
    [DataContract]
    public enum CartType
    {
        /// <summary>
        /// None (Cart Type not defined).
        /// </summary>
        [EnumMember]
        None,

        /// <summary>
        /// Shopping cart.
        /// </summary>
        [EnumMember]
        Shopping,

        /// <summary>
        /// Checkout cart - The temporary copy of the shopping cart 
        /// created during the checkout process.
        /// </summary>
        [EnumMember]
        Checkout
    }
}