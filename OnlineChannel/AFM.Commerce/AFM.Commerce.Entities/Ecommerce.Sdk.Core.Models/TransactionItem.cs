/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using AFM.Commerce.Entities;
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// The transaction item view model representation.
    /// </summary>
    [Serializable]
    [DataContract]
    public partial class TransactionItem : IExtensibleDataObject
    {
        [NonSerialized]
        private ExtensionDataObject extensionData;

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionData; }
            set { this.extensionData = value; }
        }

        /// <summary>
        /// Instantiates a new instance of <see cref="TransactionItem"/> class.
        /// </summary>
        public TransactionItem()
        {
            this.PromotionLines = new Collection<string>();
        }

        /// <summary>
        /// Gets or sets the line id.
        /// </summary>
        [DataMember]
        public string LineId { get; set; }

        /// <summary>
        /// Gets or sets the product details.
        /// </summary>
        [DataMember]
        public string ProductDetails { get; set; }

        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        [DataMember]
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the number of the product.
        /// </summary>
        [DataMember]
        public string ProductNumber { get; set; }

        /// <summary>
        /// Gets or sets the product description.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the product url.
        /// </summary>
        [DataMember]
        public string ProductUrl { get; set; }

        /// <summary>
        /// Gets or sets the product image info.
        /// </summary>
        [DataMember]
        public ImageInfo Image { get; set; }

        /// <summary>
        /// Gets or sets the image data.
        /// </summary>
        /// <remarks>To be deprecated.</remarks>
        [DataMember]
        public string ImageData { get; set; }

        /// <summary>
        /// Gets or sets the product color.
        /// </summary>
        [DataMember]
        public string Color { get; set; }

        /// <summary>
        /// Gets or sets the product size.
        /// </summary>
        [DataMember]
        public string Size { get; set; }

        /// <summary>
        /// Gets or sets the product style.
        /// </summary>
        [DataMember]
        public string Style { get; set; }

        /// <summary>
        /// Gets or sets the product configuration.
        /// </summary>
        [DataMember]
        public string Configuration { get; set; }

        /// <summary>
        /// Gets or sets the comment for this item.
        /// </summary>
        [DataMember]
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the product id.
        /// </summary>
        [DataMember]
        public long ProductId { get; set; }

        /// <summary>
        /// Gets or sets the Item id.
        /// </summary>
        [DataMember]
        public string ItemId { get; set; }

        /// <summary>
        /// Gets or sets the VariantDimension id.
        /// </summary>
        [DataMember]
        public string VariantInventoryDimensionId { get; set; }

        /// <summary>
        /// Gets or sets the quantity.
        /// </summary>
        [DataMember]
        public int Quantity { get; set; }

        /// <summary>
        /// Gets or sets the formatted currency string for the price.
        /// </summary>
        [DataMember]
        public string PriceWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the formatted currency string for the tax value.
        /// </summary>
        [DataMember]
        public string TaxAmountWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the names of offers that have been applied.
        /// </summary>
        [DataMember]
        public string OfferNames { get; set; }

        /// <summary>
        /// Gets or sets the numerical value of the discount.
        /// </summary>
        [DataMember]
        public decimal DiscountAmount { get; set; }

        /// <summary>
        /// Gets or sets the formatted currency string for the discount value.
        /// </summary>
        [DataMember]
        public string DiscountAmountWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the formatted currency string for the net amount of the item.
        /// </summary>
        [DataMember]
        public string NetAmountWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the delivery mode identifier.
        /// </summary>
        [DataMember]
        public string DeliveryModeId { get; set; }

        /// <summary>
        /// Gets or sets the delivery mode description.
        /// </summary>
        [DataMember]
        public string DeliveryModeDescription { get; set; }

        /// <summary>
        /// Gets or sets the electronic delivery email.
        /// </summary>
        [DataMember]
        public string ElectronicDeliveryEmail { get; set; }

        /// <summary>
        /// Gets or sets the shipping address.
        /// </summary>
        [DataMember]
        public Address ShippingAddress { get; set; }

        /// <summary>
        /// Gets a collection of all promotions belonging to the shopping cart line.
        /// </summary>
        [DataMember]
        public Collection<string> PromotionLines { get; private set; }

        /// <summary>
        /// Gets or sets the value indicating whether the item is a kit, kit component or neither.
        /// </summary>
        /// <remarks>
        /// If the item is a kit, then the kit component collection cannot be empty.
        /// The price of a kit component specifies the delta price from the kit price.
        /// </remarks>
        [DataMember]
        public TransactionItemType ItemType { get; set; }

        /// <summary>
        /// Gets or sets kit components.
        /// </summary>
        [DataMember]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Justification = "Adding, removing, updating kit component is supported.")]
        public Collection<TransactionItem> KitComponents { get; set; }
    }
}