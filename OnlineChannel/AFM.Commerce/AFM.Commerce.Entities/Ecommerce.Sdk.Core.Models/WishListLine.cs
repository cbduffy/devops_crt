/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a wish list line.
    /// </summary>
    [DataContract]
    [Serializable]
    public class WishListLine
    {
        /// <summary>
        /// Gets or sets the wish list identifier.
        /// </summary>
        [DataMember]
        public string WishListId { get; set; }

        /// <summary>
        /// Gets or sets the wish list line identifier.
        /// </summary>
        [DataMember]
        public string LineId { get; set; }

        /// <summary>
        /// Gets or sets the wish list line customer.
        /// </summary>
        [DataMember]
        public string CustomerId { get; set; }

        /// <summary>
        /// Gets or sets the wish list line product identifier.
        /// </summary>
        [DataMember]
        public long ProductId { get; set; }

        /// <summary>
        /// Gets or sets the wish list line product number.
        /// </summary>
        [DataMember]
        public string ProductNumber { get; set; }

        /// <summary>
        /// Gets or sets the wish list line quantity.
        /// </summary>
        [DataMember]
        public decimal Quantity { get; set; }

        /// <summary>
        /// Gets or sets the formatted currency string indicating the current price of the product.
        /// </summary>
        [DataMember]
        public string PriceWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the formatted currency string indicating the total price of the wish list line.
        /// </summary>
        [DataMember]
        public string TotalWithCurrency { get; set; }

        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the product description.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the product url.
        /// </summary>
        [DataMember]
        public string ProductUrl { get; set; }

        /// <summary>
        /// Gets or sets the product image info.
        /// </summary>
        [DataMember]
        public ImageInfo Image { get; set; }

        /// <summary>
        /// Gets or sets the product color.
        /// </summary>
        [DataMember]
        public string Color { get; set; }

        /// <summary>
        /// Gets or sets the product size.
        /// </summary>
        [DataMember]
        public string Size { get; set; }

        /// <summary>
        /// Gets or sets the product style.
        /// </summary>
        [DataMember]
        public string Style { get; set; }

        /// <summary>
        /// Gets or sets the product configuration.
        /// </summary>
        [DataMember]
        public string Configuration { get; set; }
    }
}