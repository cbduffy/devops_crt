/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// The shopping cart view model representation.
    /// </summary>
    [Serializable]
    [DataContract]
    public class ShoppingCart : Transaction
    {
        /// <summary>
        /// Instantiates a new instance of <see cref="ShoppingCart"/> class.
        /// </summary>
        public ShoppingCart()
            : base()
        {
            this.Items = new Collection<TransactionItem>();
            this.PromotionLines = new Collection<string>();
        }

        /// <summary>
        /// Gets or sets the cart id.
        /// </summary>
        [DataMember]
        public string CartId { get; set; }

        /// <summary>
        /// Gets or sets the cart name.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the cart items.
        /// </summary>
        [DataMember]
        public Collection<TransactionItem> Items { get; private set; }

        /// <summary>
        /// Gets or sets the last modified date and time of the shopping cart
        /// </summary>
        [DataMember]
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets the type of the shopping cart (shopping cart, checkout cart, wishList)
        /// </summary>
        [DataMember]
        public CartType CartType { get; set; }

        /// <summary>
        /// Gets a collection of all promotions belonging to the shopping cart.
        /// </summary>
        [DataMember]
        public Collection<string> PromotionLines { get; private set; }

        /// <summary>
        /// Gets or sets the Order rnumber.
        /// </summary>
        [DataMember]
        public string SalesOrderNumber { get; set; }
    }
}
