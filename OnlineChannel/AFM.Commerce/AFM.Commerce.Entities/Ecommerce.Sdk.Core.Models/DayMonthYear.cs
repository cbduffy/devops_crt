/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    /// <summary>
    /// Represents the time unit.
    /// </summary>
    /// <remarks>
    /// Maps to the RetailDayMonthYear enum in AX.
    /// </remarks>
    public enum DayMonthYear
    {
        /// <summary>
        /// The time is measured in days.
        /// </summary>
        Day = 0,

        /// <summary>
        /// The time is measured in months.
        /// </summary>
        Month = 1,

        /// <summary>
        /// The time is measured in years.
        /// </summary>
        Year = 2
    }
}