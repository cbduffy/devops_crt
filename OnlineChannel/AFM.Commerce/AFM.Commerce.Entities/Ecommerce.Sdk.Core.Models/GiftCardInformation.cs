/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Gift Card view model class.
    /// </summary>
    [Serializable]
    [DataContract]
    public class GiftCardInformation : IExtensibleDataObject
    {
        [NonSerialized]
        private ExtensionDataObject extensionData;

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionData; }
            set { this.extensionData = value; }
        }

        /// <summary>
        /// Gets the gift card Id.
        /// </summary>
        [DataMember]
        public string GiftCardId { get; set; }

        /// <summary>
        /// Gets the gift card currency.
        /// </summary>
        [DataMember]
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Gets/sets numerical value of the gift card's balance.
        /// </summary>
        [DataMember]
        public decimal Balance { get; set; }

        /// <summary>
        /// Gets/sets the currency formatted string of the gift card's balance.
        /// </summary>
        [DataMember]
        public string BalanceWithCurrency { get; set; }

        /// <summary>
        /// Gets the gift card availability information.
        /// </summary>
        [DataMember]
        public bool IsInfoAvailable { get; set; }
    }
}
