/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the level of information contained in shopping cart entity in storefront.
    /// </summary>
    [DataContract]
    public enum ShoppingCartDataLevel
    {
        /// <summary>
        /// The default level which indicates only minimal information is obtained.
        /// </summary>
        /// <remarks>
        /// This data level is mainly used for mini shopping cart where product details and pricing details will suffice.
        /// </remarks>
        [EnumMember]
        Minimal,

        /// <summary>
        /// This data level indicates that cart contains additional information such as shipping options, discount codes, loyalty details.
        /// </summary>
        /// <remarks>
        /// This is used everywhere except mini shopping cart and the shopping cart display pages.
        /// </remarks>
        [EnumMember]
        Extended,

        /// <summary>
        /// This level indicates that cart contains all information associated with it.
        /// </summary>
        /// <remarks>
        /// This includes the kit component level details and cart promotions. This is used in the shopping cart display in the 'View shopping cart', 'checkout review', 'order details' pages.
        /// </remarks>
        [EnumMember]
        All
    }
}
