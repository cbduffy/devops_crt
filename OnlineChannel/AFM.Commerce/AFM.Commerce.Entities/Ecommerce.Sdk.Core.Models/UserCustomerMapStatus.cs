/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    /// <summary>
    /// Enumeration that defines the status of a user customer mapping.
    /// </summary>
    public enum UserCustomerMapStatus
    {
        /// <summary>
        /// Unknown status.
        /// </summary>
        None = -1,

        /// <summary>
        /// Pending status, not active yet.
        /// </summary>
        Pending = 0,

        /// <summary>
        /// Active status, can be used.
        /// </summary>
        Active = 1,
    }
}