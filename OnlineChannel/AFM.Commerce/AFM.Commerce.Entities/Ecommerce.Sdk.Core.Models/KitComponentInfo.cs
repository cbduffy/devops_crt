/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents the view model for a kit component.
    /// </summary>
    [DataContract]
    public class KitComponentInfo
    {
        /// <summary>
        /// Gets the kit line identifier.
        /// </summary>
        [DataMember]
        public long KitLineIdentifier { get; set; }

        /// <summary>
        /// Gets the productId of the product used in a kit as a component or substitute.
        /// </summary>
        [DataMember]
        public long KitLineProductId { get; set; }

        /// <summary>
        /// Gets the Quantity of the product used in the kit as a component or substitute.
        /// </summary>
        [DataMember]
        public decimal Quantity { get; set; }

        /// <summary>
        /// Gets the unit for the product used in a kit as a component or substitute.
        /// </summary>
        [DataMember]
        public string Unit { get; set; }

        /// <summary>
        /// Gets a value indicating whether the current kit line product is used as the default component of the kit.
        /// </summary>
        [DataMember]
        public bool IsDefaultComponent { get; set; }

        /// <summary>
        /// Gets the Kit's ProductMasterId.
        /// </summary>
        [DataMember]
        public long KitProductMasterId { get; set; }

        /// <summary>
        /// Gets the component's name.
        /// </summary>
        [DataMember]
        public string Title { get; set; }
    }
}