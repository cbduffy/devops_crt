﻿
/*NS Developed by  spriya for AFM_TFS_46529 dated 6/19/2014 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;


namespace AFM.Commerce.Entities
{
    public class AFMDeliveryOptions
    {
        public Collection<DeliveryOption> DeliveryMethods { get; set; }

        public bool AddressVerficationResult { get; set; }
    }
}
