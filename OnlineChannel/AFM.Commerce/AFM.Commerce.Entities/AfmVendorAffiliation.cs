﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    public class AFMVendorAffiliation
    {
        /// <summary>
        /// Gets or sets the AffiliationID.
        /// </summary>
        public long AffiliationId { get; set; }

        /// <summary>
        /// Gets or sets the VendorId.
        /// </summary>
        public string VendorId { get; set; }

        /// <summary>
        /// Gets or sets the InvoiceVendorId.
        /// </summary>
        public string InvoiceVendorId { get; set; }

        /// <summary>
        /// Gets or sets the ShipToId.
        /// </summary>
        public string ShipToId { get; set; }
    }
}
