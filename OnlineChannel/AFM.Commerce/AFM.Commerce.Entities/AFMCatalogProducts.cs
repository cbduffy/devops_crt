﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.Entities
{
    public class AFMCatalogProducts
    {
        public List<Product> products { get; set; }
        public long CatalogId { get; set; }
    }
}
