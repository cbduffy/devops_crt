﻿using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    public class AFMCatalogProductPrices
    {
        public long catalogId { get; set; }
        public ReadOnlyCollection<ProductPrice> productPricesList { get; set; }
    }
}
