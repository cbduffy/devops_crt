﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    /// <summary>
    /// Stores all the discounts related details for a {Channel, Catalog} pair.
    /// </summary>
    public sealed class AFMChannelPromotionsSummarizedData
    {
        /// <summary>
        /// Gets or sets the product name to display.
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the product offer details to display.
        /// </summary>
        public string ProductOfferDescription { get; set; }

        /// <summary>
        /// Gets or sets the description of the offer associated with the product.
        /// </summary>
        public string ProductPromotionStatement { get; set; }

        /// <summary>
        /// Gets or sets the link to the image of the product.
        /// </summary>
        public string ProductImageLink { get; set; }

        /// <summary>
        /// Gets or sets the discount name to display.
        /// </summary>
        public string DiscountName { get; set; }

        /// <summary>
        /// Gets or sets the discount description to display.
        /// </summary>
        public string DiscountDescription { get; set; }

        /// <summary>
        /// Gets or sets the discount disclaimers to display.
        /// </summary>
        public string Disclaimer { get; set; }

        /// <summary>
        /// Gets or sets the offer identifier for the product.
        /// </summary>
        public string OfferId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether discount codes are required.
        /// </summary>
        public bool IsDiscountCodeRequired { get; set; }

        /// <summary>
        /// Gets or sets the discount codes associated for the product.
        /// </summary>
        public string DiscountCodes { get; set; }
    }
}