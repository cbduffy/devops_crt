﻿//NS Developed by muthait dated 30Oct2014

namespace AFM.Commerce.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;
    
    /// <summary>
    /// Service Item Type. Helps to identify the current cart line is of which service item type
    /// </summary>
    [DataContract]
    public enum ServiceItemType : int
        {   /// <summary>
            /// None - default product item
            /// </summary>
            [EnumMember]
            None = 0,

            /// <summary>
            /// Delivery Service Item
            /// </summary>
            [EnumMember]
            DeliveryServiceItem = 1,
    
            /// <summary>
            /// Service Item
            /// </summary>
            [EnumMember]
            Delivery = 2
    }
}
