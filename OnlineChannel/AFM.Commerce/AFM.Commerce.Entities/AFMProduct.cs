﻿using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    public class AFMProduct
    {
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal AdjustedPrice { get; set; }
        public decimal BasePrice { get; set; }
        public string ItemId { get; set; }
        public RichMediaLocations Image { get; set; }
        public long PrimaryCategoryId { get; set; }
        public decimal Price { get; set; }
        public bool ISNap { get; set; }
        public ICollection<ProductPropertyTranslation> ProductProperties { get; set; }
    }
}
