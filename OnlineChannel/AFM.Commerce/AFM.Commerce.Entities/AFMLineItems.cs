﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    public class AFMLineItems
    {
       
        [DataMember]
        public string cartItemId { get; set; }

        [DataMember]
        public string lineOrderNumber {get; set;}
    }
}
