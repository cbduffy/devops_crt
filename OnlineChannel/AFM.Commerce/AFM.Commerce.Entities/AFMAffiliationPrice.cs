﻿using System.Runtime.Serialization;

namespace AFM.Commerce.Entities
{
    public sealed class AfmAffiliationPrice
    {
        /// <summary>
        /// Gets or sets the AffiliationID.
        /// </summary>
        [DataMember]
        public long AffiliationId { get; set; }

        /// <summary>
        /// Gets or sets the item identifier that this price applies to.
        /// </summary>
        [DataMember]
        public string ItemId { get; set; }

        /// <summary>
        /// Gets or sets the regular price
        /// </summary>
        [DataMember]
        public decimal RegularPrice { get; set;}


        /// <summary>
        /// Gets or sets the Sale price
        /// </summary>
        [DataMember]
        public decimal SalePrice { get; set; }

    }
}
