﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    public class AFMUserPageMode
    {
        //Get & Set the PageMode of the current control mode
        public PageModeOptions PageMode { get; set; }
    }
    
    public enum PageModeOptions
    {
        CartView = 0,
        ShippingView = 1,
        ShippingViewCompleted = 2,
        BillingView = 3,
        BillingViewcompleted = 4,
        PaymentView = 5,
        PaymentViewCompleted = 6,
        ConfirmView = 7,
        ConfirmViewCompleted = 8,
        ThankYou = 9
    }
}
