﻿using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    [DataContract]
    public class AFMOrderConfirmationResponse 
    {
        [DataMember]
        public ShoppingCart ShoppingCart { get; set; }

        [DataMember]
        public Address ShippingAddress { get; set; }

        [DataMember]
        public Address BillingAddress{get; set;}



    }
}
