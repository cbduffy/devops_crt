﻿/*NS Developed by ysrini for AFM_TFS_40686 dated 06/26/2014*/

namespace AFM.Commerce.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Viewmodel for ATP request item
    /// </summary>
    [Serializable]
    [DataContract]
    public class AFMATPRequestItem
    {
        [NonSerialized]
        private ExtensionDataObject _extensionData;

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this._extensionData; }
            set { this._extensionData = value; }
        }

        /// <summary>
        /// Gets or sets itemId
        /// </summary>
        [DataMember]
        public string ItemId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets quantity
        /// </summary>
        [DataMember]
        public int Quantity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets thirdPartyItem
        /// </summary>
        [DataMember]
        public bool ThirdPartyItem
        {
            get;
            set;
        }
    }
}
