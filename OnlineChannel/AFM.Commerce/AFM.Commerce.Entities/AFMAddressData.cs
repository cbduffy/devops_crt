﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    /// <summary>
    /// Custom Entity to hold suggested address returned from Avalara
    /// </summary>
    public class AFMAddressData
    {
        [DataMember]
        public string ErrorMessage { get; set; }

        [DataMember]
        public bool IsAddressValid { get; set; }

        [DataMember]
        public Microsoft.Dynamics.Commerce.Runtime.DataModel.Address suggestedAddress { get; set; }

        //NS Developed by Kishore for Shipping Address Validation
        [DataMember]
        public  AddressValidationDll.DataModels.Address addressValidationsuggestedAddress { get; set; }

        [DataMember]
        public string Street2 { get; set; }
        //NS Developed by Kishore for Shipping Address Validation

        //CR 306 - Address validation off - CS Developed by spriya Dated 09/14/2015
        [DataMember]
        public bool IsServiceVerified { get; set; }

        //CR 306 - Address validation off - CE Developed by spriya Dated 09/14/2015
    }
}
