﻿using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    //Store the Basic User Cart Ingo in cookie to reuse on page redirection
    [Serializable]
    [DataContract]
    public class AFMUserCartData
    {
        //Get & Set the Shipping Address in cookie to reuse on page redirection
        [DataMember]
        public Address ShippingAddress { get; set; }

        //Get & set the Payment Address in cookie to reuse on page redirection
        [DataMember]
        public Address PaymentAddress { get; set; }

        [DataMember]
        public Boolean IsOkToText { get; set; }

        [DataMember]
        public List<AFMLineItems> LineItems { get; set; }

        [DataMember]
        public string OrderNumber { get; set; }

        [DataMember]
        public Boolean IsOptinDeals { get; set; }
        //NS by spriya : DMND0010113 - Synchrony Financing online
        [DataMember]
        public bool IsMultiAuth { get; set; }

        [DataMember]
        public string SynchronyTnC { get; set; }

        [DataMember]
        public string FinanceOptionId { get; set; }

        [DataMember]
        public bool RemoveCartDiscount { get; set; }

        [DataMember]
        public decimal MinimumPurchaseAmount { get; set; }

        [DataMember]
        public string PromoDesc1 { get; set; }

        [DataMember]
        public string PromoDesc2 { get; set; }

        [DataMember]
        public string PromoDesc3 { get; set; }

        [DataMember]
        public string CardName { get; set; }

        [DataMember]
        public string PaymTermId { get; set; }

        [DataMember]
        public string PaymMode { get; set; }

        [DataMember]
        public string LearnMore { get; set; }

        [DataMember]
        public string CardType { get; set; }

        [DataMember]
        public string PaymentFee { get; set; }

        [DataMember]
        public string ThirdPartyTermCode { get; set; }
        //NE by spriya : DMND0010113 - Synchrony Financing online
    }

}
