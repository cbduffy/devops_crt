﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    public class AFMSalesTransItems
    {
        public string OrderNumber
        {
            get;
            set;
        }
        public string LineNumber
        {
            get;
            set;
        }
        public string VendorId
        {
            get;
            set;
        }
        public string KitItemId
        {
            get;
            set;
        }
        public long KitProductId
        {
            get;
            set;
        }
        public int KitSequenceNumber
        {
            get;
            set;
        }

        public string KitItemProductDetails
        {
            get;
            set;
        }

        public int KitItemQuantity
        {
            get;
            set;
        }

        public DateTime BeginDateRange
        {
            get;
            set;
        }
        public DateTime BestDate
        {
            get;
            set;
        }
        public DateTime EndDateRange
        {
            get;
            set;
        }

        public DateTime LatestDeliveryDate
        {
            get;
            set;
        }

        public DateTime EarliestDeliveryDate
        {
            get;
            set;
        }

        public bool IsAvailable
        {
            get;
            set;
        }
        public string Message
        {
            get;
            set;
        }
        public string LineConfirmationID 
        {
            get; 
            set;
        }
        public int AFMItemType
        {
            get;
            set;
        }
        // HXM To resolve 67447 Color and Size not populating
        public string Color
        {
            get;
            set;
        }
        public string Size
        {
            get;
            set;
        }
        public string ShippingMode
        {
            get;
            set;
        }

        //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
        public DateTime BeginDateRangeMOD
        {
            get;
            set;
        }
        public DateTime BestDateMOD
        {
            get;
            set;
        }
        public DateTime EndDateRangeMOD
        {
            get;
            set;
        }

        public DateTime LatestDeliveryDateMOD
        {
            get;
            set;
        }

        public DateTime EarliestDeliveryDateMOD
        {
            get;
            set;
        }

        public string MessageMOD
        {
            get;
            set;
        }
        //NE by RxL
    }
}
