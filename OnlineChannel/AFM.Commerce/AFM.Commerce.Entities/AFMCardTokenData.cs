﻿/*NS Developed by muthait for AFM_TFS_47629 dated 06/29/2014*/

using System.Runtime.Serialization;
namespace AFM.Commerce.Entities
{
    [DataContract]
    public class AFMCardTokenData
    {
        /// <summary>
        /// Gets or sets the CardToken identifier.
        /// </summary>
        [DataMember]
        public string CardToken { get; set; }

        /// <summary>
        /// Gets or sets the CardType identifier.
        /// </summary>
        [DataMember]
        public string CardType { get; set; }

        /// <summary>
        /// Gets or sets the ExpirationMonth identifier.
        /// </summary>
        [DataMember]
        public string ExpirationMonth { get; set; }

        /// <summary>
        /// Gets or sets the ExpirationYear identifier.
        /// </summary>
        [DataMember]
        public string ExpirationYear { get; set; }

        /// <summary>
        /// Gets or sets the FullName identifier.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Last4Digits identifier.
        /// </summary>
        [DataMember]
        public string Last4Digits { get; set; }

        /// <summary>
        /// Gets or sets the UniqueCardId identifier.
        /// </summary>
        [DataMember]
        public string UniqueCardId { get; set; }

        // CS by muthait dated 26Dec2014
        /// <summary>
        /// Gets or sets the Error if any from payment connector.
        /// </summary>
        [DataMember]
        public string Error { get; set; }
        //NS by spriya : DMND0010113 - Synchrony Financing online
        //New payment response properties for synchrony financing
        [DataMember]
        public string SynchronyPromoDesc { get; set; }

        [DataMember]
        public string FinancingOptionId { get; set; }

        [DataMember]
        public bool IsMultiAuth { get; set; }

        [DataMember]
        public string SynchronyTnC { get; set; }

        [DataMember]
        public decimal SynchronyMinPurchase { get; set; }

        [DataMember]
        public bool RemoveCartDiscount { get; set; }
        //NE by spriya : DMND0010113 - Synchrony Financing online
    }
}
