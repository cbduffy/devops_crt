﻿/*NS Developed by muthait for AFM_TFS_40686 dated 06/26/2014*/
namespace AFM.Commerce.Entities
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Viewmodel for ATP response item
    /// </summary>
    [Serializable]
    [DataContract]
    public class AFMATPResponseItem
    {
        [NonSerialized]
        private ExtensionDataObject _extensionData;

        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this._extensionData; }
            set { this._extensionData = value; }
        }

        /// <summary>
        /// Gets or sets beginDateRnage
        /// </summary>
        [DataMember]
        public DateTime BeginDateRange
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets bestDate
        /// </summary>
        [DataMember]
        public DateTime BestDate
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets endDateRange
        /// </summary>
        [DataMember]
        public DateTime EndDateRange
        {
            get;
            set;
        }
        /// <summary>
        ///  This date is sum of BestDate  added to EndDateRange by mode of delivery
        /// </summary>
        [DataMember]
        public DateTime LatestDeliveryDate
        {
            get;
            set;
        }

        /// <summary>
        /// This date is sum of BestDate added to BeginDateRange by mode of delivery
        /// </summary>
        [DataMember]
        public DateTime EarliestDeliveryDate
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets isAvailable
        /// </summary>
        [DataMember]
        public bool IsAvailable
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets itemId
        /// </summary>
        [DataMember]
        public string ItemId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets message
        /// </summary>
        [DataMember]
        public string Message
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets quantity
        /// </summary>
        [DataMember]
        public int Quantity
        {
            get;
            set;
        }

        private string shippingSpan;

        /// <summary>
        /// Gets or sets ShippingSpan
        /// </summary>
        [DataMember]
        public string ShippingSpan
        {
            get
            {
                //var startweekval = Math.Truncate((this.BestDate - this.BeginDateRange).TotalDays / 7);
                //var endweekval = Math.Truncate((this.EndDateRange - this.BeginDateRange).TotalDays / 7);
                shippingSpan = string.IsNullOrEmpty(this.MessageMOD) ? this.Message : this.MessageMOD;
                return shippingSpan;
            }
            set
            {
                if (shippingSpan != value)
                    shippingSpan = value;
            }
        }

        //NS by RxL: 77476:ECS - ATP Date related fields by Mode of Delivery
        [DataMember]
        public DateTime BeginDateRangeMOD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets bestDate
        /// </summary>
        [DataMember]
        public DateTime BestDateMOD
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets endDateRange
        /// </summary>
        [DataMember]
        public DateTime EndDateRangeMOD
        {
            get;
            set;
        }
        /// <summary>
        ///  This date is sum of BestDate  added to EndDateRange by mode of delivery
        /// </summary>
        [DataMember]
        public DateTime LatestDeliveryDateMOD
        {
            get;
            set;
        }

        /// <summary>
        /// This date is sum of BestDate added to BeginDateRange by mode of delivery
        /// </summary>
        [DataMember]
        public DateTime EarliestDeliveryDateMOD
        {
            get;
            set;
        }
       
        /// <summary>
        /// Gets or sets message
        /// </summary>
        [DataMember]
        public string MessageMOD
        {
            get;
            set;
        }
        //NE by RxL
    }
}
