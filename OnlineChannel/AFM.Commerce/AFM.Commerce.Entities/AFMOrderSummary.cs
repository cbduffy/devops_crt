﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    [Serializable]
    [DataContract]
    public class AFMOrderSummary
    {
         [DataMember]
        public string SubtotalWithCurrency { get; set; }
         [DataMember]
        public string EstShipping { get; set; }
         [DataMember]
        public string EstHomeDelivery{ get; set; }
         [DataMember]
        public string TaxAmountWithCurrency { get; set; }
         [DataMember]
        public string TotalAmountWithCurrency { get; set; }
    }
}
