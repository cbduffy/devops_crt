﻿/*NS Developed by ysrini for AFM_TFS_40686 dated 06/26/2014*/
namespace AFM.Commerce.Entities
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Viewmodel for Atp response
    /// </summary>
    [DataContract]
    public class AFMATPResponse
    {
        /// <summary>
        /// Gets or sets item Groupings
        /// </summary>
        [DataMember]
        public List<AFMATPItemGroupingResponse> ItemGroupings
        {
            get;
            set;
        }
    }
}
