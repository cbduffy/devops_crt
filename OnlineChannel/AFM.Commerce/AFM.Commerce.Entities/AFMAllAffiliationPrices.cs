﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using System;
    using System.Runtime.Serialization;
    /// <summary>
    /// Container for the price of an item.
    /// </summary>
    [Serializable]
    [DataContract]
    public class AFMAllAffiliationPrices
    {
        [NonSerialized]
        private ExtensionDataObject extensionData;

        public long AffiliationId { get; set; }

        /// <summary>
        /// Gets or sets the adjusted price for the listing.
        /// </summary>
        /// <remarks>
        /// This is the best of the TradeAgreementPrice and retail price adjustments.
        /// </remarks>
        [DataMember]
        public string BestPrice { get; set; }

        /// <summary>
        /// Gets or sets the adjusted price for the item.
        /// </summary>
        /// <remarks>
        /// This is the item id.
        /// </remarks>
        public string ItemId { get; set; }
        /// <summary>
        /// Gets or sets the structure that contains extra data.
        /// </summary>
        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/> that contains data that is not recognized as belonging to the data contract.</returns>
        public ExtensionDataObject ExtensionData
        {
            get { return this.extensionData; }
            set { this.extensionData = value; }
        }
    }
}
