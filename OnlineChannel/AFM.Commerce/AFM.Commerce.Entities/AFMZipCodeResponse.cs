﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    [DataContract]
    public class AFMZipCodeResponse
    {   
        [DataMember]
        public string AFMZipCode { get; set; }
    }
}
