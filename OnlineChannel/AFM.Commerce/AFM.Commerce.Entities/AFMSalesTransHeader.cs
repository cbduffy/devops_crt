﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    [Serializable]
    [DataContract]
    public class AFMSalesTransHeader
    {
        /// <summary>
        /// The order number
        /// </summary>
        public string OrderNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Is the customer ok for receiving text
        /// </summary>
        public bool IsOkToText
        {
            get;
            set;
        }


        /// <summary>
        /// Unique identifier for the Ok to Text Survey
        /// </summary>
        [DataMember]
        public string OkToTextUniqueId
        {
            get;
            set;
        }

        /// <summary>
        /// Version identifier for the Ok to Text Survey
        /// </summary>
        [DataMember]
        public string OkToTextVersionId
        {
            get;
            set;
        }

        /// <summary>
        /// Is the customer accepting the terms and conditions
        /// </summary>
        [DataMember]
        public bool IsAcceptTermsAndConditions
        {
            get;
            set;
        }

        /// <summary>
        /// Unique identifier for the terms and conditions
        /// </summary>
        [DataMember]
        public string TermsAndConditionsUniqueId
        {
            get;
            set;
        }

        /// <summary>
        /// Version identifier for the terms and conditions
        /// </summary>
        [DataMember]
        public string TermsAndConditionsVersionId
        {
            get;
            set;
        }

        //NS developed by v-hapat for CR 72099 
        [DataMember]
        public string IsOptinDealsUniqueId { get; set; }

        [DataMember]
        public string IsOptinDealsVersionId { get; set; }

        //NE developed by v-hapat for CR 72099 

        //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
        /// <summary>
        /// Gets or sets the record id of RSA ID associated with cart.
        /// </summary>
        [DataMember]
        public long RsaRecId { get; set; }
        //NE - RxL

        //NS by spriya : DMND0010113 - Synchrony Financing online
        [DataMember]
        public string FinancingOptionId { get; set; }

        [DataMember]
        public string SynchronyTnC { get; set; }

        [DataMember]
        public bool IsMultiAuthAllowed { get; set; }

        [DataMember]
        public string PromoDesc1 { get; set; }
        
        [DataMember]
        public string PromoDesc2 { get; set; }

        [DataMember]
        public string PromoDesc3{ get; set; }

        [DataMember]
        public bool RemoveCartDiscount{ get; set; }

        [DataMember]
        public Decimal MinimumPurchaseAmount { get; set; }

        [DataMember]
        public string PaymTermId { get; set; }

        [DataMember]
        public string PaymMode { get; set; }

        [DataMember]
        public string LearnMore { get; set; }

        [DataMember]
        public string CardType { get; set; }

        [DataMember]
        public string PaymentFee { get; set; }

        [DataMember]
        public string ThirdPartyTermCode { get; set; }
        //NE by spriya : DMND0010113 - Synchrony Financing online
    }
}
