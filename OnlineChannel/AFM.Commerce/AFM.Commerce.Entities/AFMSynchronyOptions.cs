﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    [Serializable]
    [DataContract]
    //NS by spriya : DMND0010113 - Synchrony Financing online
    //This class contains properties for synchrony options for synchrony service
    public class AFMSynchronyOptions
    {
        [DataMember]
        public string FinancingOptionId { get; set; }

        [DataMember]
        public string CardType { get; set; }

        [DataMember]
        public bool DefaultOrderTotal { get; set; }

        [DataMember]
        public int Sortorder { get; set; }

        [DataMember]
        public string PromoDesc1 { get; set; }

        [DataMember]
        public string PromoDesc2 { get; set; }

        [DataMember]
        public string PromoDesc3 { get; set; }

        [DataMember]
        public string LearnMore { get; set; }

        [DataMember]
        public bool ShowLearnMore { get; set; }

        [DataMember]
        public string ThirdPartyTermCode { get; set; }

        [DataMember]
        public bool RemoveDiscountPopUp { get; set; }

        [DataMember]
        public DateTime EffectiveDate { get; set; }

        [DataMember]
        public DateTime ExpirationDate { get; set; }

        [DataMember]
        public bool Status { get; set; }

        [DataMember]
        public bool RemoveCartDiscount { get; set; }

        [DataMember]
        public Decimal MinimumPurchaseAmount { get; set; }

        [DataMember]
        public string PaymentFee { get; set; }

        [DataMember]
        public Decimal EstimatedPercentage { get; set; }

        [DataMember]
        public string PaymTermId { get; set; }

        [DataMember]
        public string PaymMode { get; set; }

        [DataMember]
        public Decimal SubTotal { get; set; }

        [DataMember]
        public Decimal EstimatedTotal { get; set; }

        [DataMember]
        public bool ShowOption { get; set; }

        [DataMember]
        public string CardName { get; set; }
    }
}
