﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AFMLineConfirmationItems.cs" company="Microsoft">
//   
// </copyright>
// <summary>
//   The afm line confirmation items.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Runtime.Serialization;

namespace AFM.Commerce.Entities
{
    /// <summary>
    /// The afm line confirmation items.
    /// </summary>
    [DataContract]
    public class AFMLineConfirmationItems
    {
        /// <summary>
        /// Gets or sets the item id.
        /// </summary>
        [DataMember]
        public string ItemId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is e comm owned.
        /// </summary>
        [DataMember]
        public bool IsECommOwned { get; set; }

        /// <summary>
        /// Gets or sets the vendor id.
        /// </summary>
        [DataMember]
        public string VendorId { get; set; }

        /// <summary>
        /// Gets or sets the direct ship.
        /// </summary>
        [DataMember]
        public int DirectShip { get; set; }

        /// <summary>
        /// Gets or sets the supplier direct ship.
        /// </summary>
        [DataMember]
        public int SupplierDirectShip { get; set; }

        /// <summary>
        /// Gets or sets the line confirmation number.
        /// </summary>
        [DataMember]
        public int LineConfirmationNumber { get; set; }

    }
}
