﻿// ND by muthait dated 30Oct2014
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.Entities
{
    /// <summary>
    ///AFM Item Type. Helps us identify the type of CartLine
    /// </summary>
    [DataContract]
    public enum AFMItemType : int
    {
        /// <summary>
        /// Default Item type
        /// </summary>
        [EnumMember]
        Default = 0,

        /// <summary>
        /// Delivery Service Item Type
        /// </summary>
        [EnumMember]
        DeliveryServiceItem = 1,

        /// <summary>
        /// Service Item Type
        /// </summary>
        [EnumMember]
        ServiceItem = 2,

    }
}
