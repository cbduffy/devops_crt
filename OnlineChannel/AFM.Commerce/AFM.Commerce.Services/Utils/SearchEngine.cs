﻿using System.Web.Configuration;

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Web;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using AFM.Commerce.Framework.Extensions.Utils;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

    /// <summary>
    /// Represents the search engine for the application.
    /// </summary>
    public class SearchEngine : ISearchEngine
    {
        /// <summary>
        /// Gets web application root url value.
        /// </summary>
        private static Uri applicationRootPathValue;

        /// <summary>
        /// Gets image location value.
        /// </summary>
        private static string imagesLocationValue;

        /// <summary>
        /// Gets the name of the search property that contains the item's description.
        /// </summary>
        public string DescriptionSearchPropertyName
        {
            get { return CommonConstants.DescriptionSearchPropertyName; }
        }

        /// <summary>
        /// Gets the name of the search property that contains the item's path.
        /// </summary>
        public string PathSearchPropertyName
        {
            get { return CommonConstants.PathSearchPropertyName; }
        }

        /// <summary>
        /// Gets the name of the search property that contains the item's image.
        /// </summary>
        public string ImageSearchPropertyName
        {
            get { return CommonConstants.ImageSearchPropertyName; }
        }

        /// <summary>
        /// Gets the name of the search property that contains the item's title.
        /// </summary>
        public string TitleSearchPropertyName
        {
            get { return CommonConstants.TitleSearchPropertyName; }
        }

        /// <summary>
        /// Gets the name of the search property that contains the item's color.
        /// </summary>
        public string ColorSearchPropertyName
        {
            get { return CommonConstants.ColorSearchPropertyName; }
        }

        /// <summary>
        /// Gets the name of the search property that contains the item's size.
        /// </summary>
        public string SizeSearchPropertyName
        {
            get { return CommonConstants.SizeSearchPropertyName; }
        }

        /// <summary>
        /// Gets the name of the search property that contains the item's style.
        /// </summary>
        public string StyleSearchPropertyName
        {
            get { return CommonConstants.StyleSearchPropertyName; }
        }

        /// <summary>
        /// Gets the name of the search property that contains the item's configuration.
        /// </summary>
        public string ConfigurationSearchPropertyName
        {
            get { return CommonConstants.ConfigurationSearchPropertyName; }
        }

        /// <summary>
        /// Gets the application root path.
        /// </summary>
        private static Uri ApplicationRootPath
        {
            get
            {
                if (SearchEngine.applicationRootPathValue == null)
                {
                    HttpRequest request = HttpContext.Current.Request;
                    SearchEngine.applicationRootPathValue = new Uri(request.Url.Scheme + "://" + request.Url.Authority + request.ApplicationPath);
                }

                return SearchEngine.applicationRootPathValue;
            }
        }

        /// <summary>
        /// Gets image location.
        /// </summary>
        private static string ImagesLocation
        {
            get
            {
                if (SearchEngine.imagesLocationValue == null)
                {
                    SearchEngine.imagesLocationValue = WebConfigurationManager.AppSettings[CommonConstants.ImagesLocationKeyName];

                    bool isAbsolute = Uri.IsWellFormedUriString(SearchEngine.imagesLocationValue, UriKind.Absolute);
                    if (!isAbsolute)
                    {
                        SearchEngine.imagesLocationValue = SearchEngine.ApplicationRootPath + SearchEngine.imagesLocationValue;
                    }
                }

                return SearchEngine.imagesLocationValue;
            }
        }

        /// <summary>
        /// Gets the requested properties for the given listing ids.
        /// </summary>
        /// <param name="isResultMandatory">if set to <c>true</c> then the method will fail if no satisfactory results are found.
        /// if set to <c>false</c> then the method will return a null if no satisfactory results are found.</param>
        /// <param name="listingRecordIds">AX Listing's record IDs.</param>
        /// <param name="propertyNames">The properties of the given listing ids that needs to be returned.</param>
        /// <returns>
        /// listingRecordId and properties of given listing as key value pairs. For instance: (12345, Path, /Sites/RetailPublishingPortal/12345/12345).
        /// </returns>
        public IDictionary<long, IDictionary<string, object>> GetListingsProperties(bool isResultMandatory, IEnumerable<long> listingRecordIds, params string[] propertyNames)
        {
            if (propertyNames == null || propertyNames.Length == 0)
            {
                throw new ArgumentNullException("propertyNames");
            }

            if (listingRecordIds == null)
            {
                throw new ArgumentNullException("listingRecordIds");
            }

            IDictionary<long, IDictionary<string, object>> properties = new Dictionary<long, IDictionary<string, object>>();
            ReadOnlyCollection<Product> products = DemoController.GetProductsByIds(listingRecordIds.ToList());

            foreach (long listingRecordId in listingRecordIds)
            {
                IDictionary<string, object> listingProperties = new Dictionary<string, object>();

                // Check if the listing record id is a standalone product
                Product product = products.Where(p => p.RecordId == listingRecordId).FirstOrDefault();
                ProductVariant variant = null;

                if (product == null)
                {
                    // Check if the listing record id is a variant of one of the products.
                    foreach (Product currentProduct in products)
                    {
                        if (currentProduct.IsMasterProduct)
                        {
                            variant = currentProduct.CompositionInformation.VariantInformation.Variants.Where(v => v.DistinctProductVariantId == listingRecordId).SingleOrDefault();
                            if (variant != null)
                            {
                                product = currentProduct;
                                break;
                            }
                        }
                    }
                }

                if (product != null)
                {
                    listingProperties = this.GetPropertiesMap(product, variant, listingRecordId, propertyNames);
                }

                if (isResultMandatory && !listingProperties.Any())
                {
                    string message = string.Format("Unable to get the requested property details for the given listing {0}, Please try again later", listingRecordId);
                    throw new Exception(message);
                }

                if (!properties.ContainsKey(listingRecordId))
                {
                    properties.Add(listingRecordId, listingProperties);
                }
            }

            return properties;
        }

        /// <summary>
        /// Gets the requested properties for the given listing id.
        /// </summary>
        /// <param name="isResultMandatory">if set to <c>true</c> then the method will fail if no satisfactory results are found.
        /// if set to <c>false</c> then the method will return a null if no satisfactory results are found.</param>
        /// <param name="listingRecordId">AX Listing's record ID.</param>
        /// <param name="propertyNames">The properties of the given listing id that needs to be returned.</param>
        /// <returns> Properties of given listing as key value pairs. For instance: (Path, /Sites/RetailPublishingPortal/12345/67890). </returns>
        public IDictionary<string, object> GetListingProperties(bool isResultMandatory, long listingRecordId, params string[] propertyNames)
        {
            if (propertyNames == null || propertyNames.Length == 0)
            {
                throw new ArgumentNullException("propertyNames");
            }

            var products = DemoController.GetProductsByIds(new List<long> { listingRecordId });
            Product product = null;
            if (products != null && products.Count() > 0)
                product = products.Single();
            IDictionary<string, object> listingProperties = new Dictionary<string, object>();

            if (product != null && product.CompositionInformation != null && product.CompositionInformation.VariantInformation != null                
                && product.CompositionInformation.VariantInformation.Variants != null)
            {
                //TODO: Check with PG if this is going to fix in next release
                if (product.CompositionInformation.VariantInformation.Variants.Where(v => Convert.ToInt64(v.DistinctProductVariantId) == listingRecordId).Count() > 0)
                {
                    ProductVariant variant = product.CompositionInformation.VariantInformation.Variants.Where(v => Convert.ToInt64(v.DistinctProductVariantId) == listingRecordId).Single();
                    listingProperties = this.GetPropertiesMap(product, variant, listingRecordId, propertyNames);
                }
            }

            if (isResultMandatory && !listingProperties.Any())
            {
                string message = string.Format("Unable to get the requested property details for the given listing {0}, Please try again later", listingRecordId);
                throw new Exception(message);
            }

            return listingProperties;
        }

        /// <summary>
        /// Gets the information of the kit variant that has the given products as its components.
        /// Only variants of the given kit master product are looked up for a match.
        /// </summary>
        /// <param name="kitMasterProductId">Product identifier of the kit master product.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <param name="kitLines">The different kit lines that define a unique configuration of the kit.</param>
        /// <returns>Returns title, customer item number and product identifier of the kit variant.</returns>
        public Tuple<string, string, long, string> GetKitVariantProductInfo(long kitMasterProductId, long catalogId, IEnumerable<Commerce.Runtime.DataModel.KitLineProductProperty> kitLines)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the applicable variants of a kit component.
        /// </summary>
        /// <param name="kitComponentProductId">Product identifier of the kit component.</param>
        /// <param name="kitComponentParentId">Product identifier of the kit component master.</param>
        /// <param name="parentKitId">Product identifier of the kit master product.</param>
        /// <param name="kitComponentLineId">The kit component line identifier.</param>
        /// <param name="catalogId">The catalog identifier.</param>
        /// <returns>Returns a collection of kit component variants.</returns>
        public IEnumerable<StorefrontListItem> GetKitComponentVariants(long kitComponentProductId, long kitComponentParentId, long parentKitId, long kitComponentLineId, long catalogId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the product image data and converts it into rich media location view model entity.
        /// </summary>
        /// <param name="imageSearchValue">The product image data.</param>
        /// <returns>The product image data in rich media location view model entity.</returns>
        public ImageInfo UpdateProductImageData(RichMediaLocations imageSearchValue)
        {
            if (imageSearchValue == null)
            {
                throw new ArgumentNullException("imageSearchValue");
            }

            ImageInfo image = new ImageInfo();
            image.AltText = imageSearchValue.Items[0].AltText;
            image.Url = SearchEngine.ImagesLocation + imageSearchValue.Items[0].Url;

            return image;
        }

        /// <summary>
        /// Gets the listing properties given the product and product variant (if any) entities.
        /// </summary>
        /// <param name="product">The product entity.</param>
        /// <param name="variant">The product variant entity.</param>
        /// <param name="listingRecordId">The listing record id.</param>
        /// <param name="propertyNames">The property names.</param>
        /// <returns>Property key value pairs.</returns>
        private IDictionary<string, object> GetPropertiesMap(Product product, ProductVariant variant, long listingRecordId, params string[] propertyNames)
        {
            IDictionary<string, object> listingProperties = new Dictionary<string, object>();
            foreach (string propertyName in propertyNames)
            {
                if (propertyName.Equals(CommonConstants.PathSearchPropertyName))
                {
                    listingProperties[CommonConstants.PathSearchPropertyName] = string.Empty;
                }
                else if (propertyName.Equals(CommonConstants.TitleSearchPropertyName))
                {
                    listingProperties[CommonConstants.TitleSearchPropertyName] = product.ProductName;
                }
                else if (propertyName.Equals(CommonConstants.DescriptionSearchPropertyName))
                {
                    listingProperties[CommonConstants.DescriptionSearchPropertyName] = product.Description;
                }
                else if (propertyName.Equals(CommonConstants.ImageSearchPropertyName))
                {
                    ImageInfo image = new ImageInfo();
                    if (listingRecordId == product.RecordId)
                    {
                        //CS by muthait dated 24 Oct2014
                        if (product.Image != null)
                        {
                            image.Url = SearchEngine.ImagesLocation + product.Image.Items[0].Url;
                            image.AltText = product.Image.Items[0].AltText;
                        }
                    }
                    else
                    {
                        ProductPropertyDictionary indexedProperties = product.GetIndexedProperties(variant.DistinctProductVariantId, CrtUtilities.GetChannelDefaultCulture().Name);
                        ProductProperty imageProperty = null;
                        if (indexedProperties.TryGetValue(CommonConstants.ImageSearchPropertyName, out imageProperty))
                        {
                            if (imageProperty.Value != null)
                            {
                                RichMediaLocations imageSearchValue = imageProperty.Value as RichMediaLocations;
                                image.Url = SearchEngine.ImagesLocation + imageSearchValue.Items[0].Url;
                                image.AltText = imageSearchValue.Items[0].AltText;
                            }
                        }
                    }

                    listingProperties[CommonConstants.ImageSearchPropertyName] = image;
                }
                else if (propertyName.Equals(CommonConstants.ConfigurationSearchPropertyName))
                {
                    listingProperties[CommonConstants.ConfigurationSearchPropertyName] = (variant == null) ? variant.Configuration : string.Empty;
                }
                else if (propertyName.Equals(CommonConstants.ColorSearchPropertyName))
                {
                    listingProperties[CommonConstants.ColorSearchPropertyName] = (variant == null) ? variant.Color : string.Empty;
                }
                else if (propertyName.Equals(CommonConstants.SizeSearchPropertyName))
                {
                    listingProperties[CommonConstants.SizeSearchPropertyName] = (variant == null) ? variant.Size : string.Empty;
                }
                else if (propertyName.Equals(CommonConstants.StyleSearchPropertyName))
                {
                    listingProperties[CommonConstants.StyleSearchPropertyName] = (variant == null) ? variant.Style : string.Empty;
                }
            }

            return listingProperties;
        }
    }
}
