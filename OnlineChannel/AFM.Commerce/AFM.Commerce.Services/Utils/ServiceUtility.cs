﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Framework.Extensions.Utils;

namespace Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core
{
    using System;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;

    public class ServiceUtility : ServiceUtilityBase
    {
        public override string GetCulture()
        {
            return AFMDataUtilities.GetCultureIdFromCookie();
        }

        public override string GetCurrentCustomerId(bool isMandatory)
        {
            return AFMDataUtilities.GetCustomerIdFromCookie();
        }

        public override string GetUserName()
        {
            //TODO: Implement
            return Guid.NewGuid().ToString();
        }

        public override void ClearCheckoutCartIdInPersistenceStorage()
        {
            //throw new NotImplementedException();
            AFMDataUtilities.ClearCheckoutCartIdResponseCookies(SessionType.All);

        }

        public override void SetCartIdInPersistenceStorage(SessionType sessionType, CartType cartType, string cartId)
        {
            AFMDataUtilities.SetCartIdInResponseCookie(sessionType, cartType, cartId);
            AFMDataUtilities.ClearOrderIdFromCookie();
        }

        public override SessionType GetSessionType()
        {
            return AFMDataUtilities.GetSessionType();
        }

        public override string GetShoppingCartIdFromPersistenceStorage(SessionType sessionType)
        {
            return AFMDataUtilities.GetShoppingCartIdFromCookie(sessionType);
        }

        public override string GetCheckoutCartIdFromPersistenceStorage(SessionType sessionType)
        {
            return AFMDataUtilities.GetCheckoutCartIdFromCookie(sessionType);
        }

        public override void ClearShoppingCartIdInPersistenceStorage()
        {
            AFMDataUtilities.ClearShoppingCartIdResponseCookies(SessionType.All);
        }

        public override bool IsAssociated()
        {
            throw new NotImplementedException();
        }

        public override string GetUserIdentifier()
        {
            throw new NotImplementedException();
        }

        public override void AddCustomClaims(string customerId)
        {
            throw new NotImplementedException();
        }

        public override string GetUserEmail()
        {
            throw new NotImplementedException();
        }
    }
}
