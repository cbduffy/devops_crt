﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Collections.ObjectModel;
using AFM.Commerce.Entities;
using AFM.Commerce.Framework.Extensions.Utils;
using AFM.Commerce.Framework.Extensions.Controllers;
using AFM.Commerce.Framework.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;

namespace AFM.Commerce.Services
{
    public class AFMPricingService : ServiceBase<AFMPricingController>, IAFMPricingService
    {

        private static readonly ISearchEngine searchEngine = new SearchEngine();
        private static readonly IProductValidator productValidator = new ProductValidator();
        private static readonly ServiceUtilityBase serviceProxy = new ServiceUtility();

        /// <summary>
        /// Get affiliation by zipcode
        /// </summary>
        /// <returns>
        /// The vendor affiliation details based on the zipcode passed
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual AFMVendorAffiliation GetAffiliationByZipCode(string zipCode)
        {
            if (string.IsNullOrWhiteSpace(zipCode))
            {
                throw new ArgumentNullException("zipCode");
            }
            var afmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(zipCode);
            return afmVendorAffiliation;
        }

        /// <summary>
        /// Gets Price by affiliation
        /// </summary>
        /// <returns>
        /// The affilaition price based on affiliation id for all itemids passed as parameter
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual ReadOnlyCollection<AfmAffiliationPrice> GetPriceByAffiliation(long affiliationId, List<string> itemIds)
        {
            var afmGetPriceByAffiliation = Controller.GetPriceByAffiliation(affiliationId, itemIds);
            return afmGetPriceByAffiliation;
        }

        /// <summary>
        /// Gets All Price for items
        /// </summary>
        /// <returns>
        /// Get prices based on affiliation for all zipcodes for the item ids passed as parameter
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual AFMGetAllPrices GetAllPrices(List<string> itemIds)
        {
            var getAllPrices = new AFMGetAllPrices
            {
                AFMAllAffiliationPrices = Controller.GetAllPrices(itemIds)
            };
            if (getAllPrices.AFMAllAffiliationPrices != null && getAllPrices.AFMAllAffiliationPrices.Count > 0)
                getAllPrices.Result = "Success";
            else
                getAllPrices.Result = "No Results Found";
            return getAllPrices;
        }

        ///// <summary>
        ///// Gets Price Snapshot Data
        ///// </summary>
        ///// <returns>
        ///// Get prices based on affiliation for all zipcodes for the item ids passed as parameter
        ///// </returns>
        //public virtual List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> itemIds)
        //{
        //    var getPriceSnapshot = Controller.GetPriceSnapshot(itemIds, 0);
        //    return getPriceSnapshot;
        //}

        /// <summary>
        /// Gets Price Snapshot Data
        /// </summary>
        /// <returns>
        /// Get prices based on affiliation for all zipcodes for the item ids passed as parameter
        /// </returns>
        public virtual List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> itemIds,long affiliationId)
        {
            var getPriceSnapshot = Controller.GetPriceSnapshot(itemIds, affiliationId);
            return getPriceSnapshot;
        }


        public virtual List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> itemIds)
        {
            var getPriceSnapshot = Controller.GetPriceSnapshot(itemIds);
            return getPriceSnapshot;
        }
        /// <summary>
        /// Get all fulfiller information
        /// </summary>
        /// <returns></returns>
        public virtual List<AFMFulFillerInformation> GetAllFulfillerInformation()
        {
            var getAllFulfillerInformation = Controller.GetAllFulfillerInformation();
            return getAllFulfillerInformation;
        }
        /// <summary>
        /// Gets Demo implementation of ISearchEngine.
        /// </summary>
        protected override ISearchEngine SearchEngine
        {
            get { return searchEngine; }
        }

        /// <summary>
        /// Gets Demo implementation of IProductValidator.
        /// </summary>
        protected override IProductValidator ProductValidator
        {
            get { return productValidator; }
        }

        /// <summary>
        /// Gets Demo implementation of ServiceUtilityBase.
        /// </summary>
        protected override ServiceUtilityBase ServiceUtility
        {
            get { return serviceProxy; }
        }

        /// <summary>
        /// Gets Demo implementation of IConfiguration.
        /// </summary>
        protected override IConfiguration Configuration
        {
            get { return new SiteConfiguration(); }
        }

        /// <summary>
        /// Executes the given service action and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="action">Service action.</param>
        protected override void ExecuteAfterRequestValidation(ServiceResponse response, System.Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            AFMServiceResponse afmServiceResponse = null;
            if (response != null)
            {
                afmServiceResponse = new AFMServiceResponse();
                afmServiceResponse.CanRetry = response.CanRetry;
                foreach (var error in response.Errors)
                {
                    AFM.Commerce.Entities.ResponseError responseError = new AFM.Commerce.Entities.ResponseError();
                    responseError.ErrorCode = error.ErrorCode;
                    responseError.ErrorMessage = error.ErrorMessage;
                    responseError.ExtendedErrorMessage = error.ExtendedErrorMessage;
                    responseError.RedirectUrl = error.RedirectUrl;
                    responseError.ShowDebugInfo = error.ShowDebugInfo;
                    responseError.Source = error.Source;
                    responseError.StackTrace = error.StackTrace;
                    afmServiceResponse.Errors.Add(responseError);

                }
                afmServiceResponse.RedirectUrl = response.RedirectUrl;
            }
            AFMDataUtilities.ExecuteAndHandleExceptions(afmServiceResponse, action);
        }

        /// <summary>
        /// Executes handler and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void Execute(ServiceResponse response, Action action)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if user is authenticated and assosiated then calls <see cref="ExecuteAfterPostRequestValidation"/> with new execution handler.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void ExecuteForAuthorizedUser(ServiceResponse response, Action<string> action)
        {
            throw new NotImplementedException();
        }


    }


}
