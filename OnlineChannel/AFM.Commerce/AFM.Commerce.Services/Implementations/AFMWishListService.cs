﻿using AFM.Commerce.Entities;
using AFM.Commerce.Framework.Extensions.Controllers;
using AFM.Commerce.Framework.Extensions.Utils;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Storefront;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;


namespace AFM.Commerce.Services
{
    public class AFMWishListService : ServiceBase<AFMWishListController>, IAFMWishListService
    {

        private static readonly ISearchEngine searchEngine = new SearchEngine();
        private static readonly IProductValidator productValidator = new ProductValidator();
        private static readonly ServiceUtilityBase serviceProxy = new ServiceUtility();


        /// <summary>
        /// Gets the wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        public virtual WishListResponse GetWishList(string wishListId)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentNullException("wishListId");
            }

            WishListResponse response = new WishListResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                response.WishList = Controller.GetWishList(wishListId, customerId, this.SearchEngine);
            });

            return response;
        }

        /// <summary>
        /// Gets the wish lists corresponding to customer.
        /// </summary>
        /// <returns>
        /// The service response containing the wish lists.
        /// </returns>
        public virtual WishListCollectionResponse GetWishListsForCurrentCustomer()
        {
            WishListCollectionResponse response = new WishListCollectionResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                response.WishLists = Controller.GetWishListsForCustomer(customerId, this.SearchEngine);
            });

            return response;
        }

        /// <summary>
        /// Deletes the wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        public virtual NullResponse DeleteWishList(string wishListId)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentException("wishListId");
            }
            NullResponse response = new NullResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                Controller.DeleteWishList(wishListId,customerId);
            });

            return response;
        }

        /// <summary>
        /// Creates the wish list.
        /// </summary>
        /// <param name="wishListName">The wish list name.</param>       
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        public virtual WishListResponse CreateWishList(string wishListName)
        {
            if (string.IsNullOrWhiteSpace(wishListName))
            {
                throw new ArgumentException("wishListName");
            }
            WishListResponse response = new WishListResponse();
            WishList wishList = new WishList();
            wishList.Name = wishListName;

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                response.WishList = Controller.CreateWishList(wishList, customerId);
            });

            return response;
        }

        /// <summary>
        /// Adds items to wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="listings">The items to add.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        public virtual WishListResponse AddItemsToWishList(string wishListId, IEnumerable<Listing> listings)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentException("wishListId");
            }
            if (listings == null)
            {
                throw new ArgumentNullException("listings");
            }
            WishListResponse response = new WishListResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                response.WishList = Controller.AddItemsToWishList(wishListId, customerId, listings);
            });

            return response;
        }

        /// <summary>
        /// Adds items from wish list to Cart.
        /// </summary>
        /// <param name="shoppingCartId">Cart Id</param>
        /// <param name="customerId">Logged in Customer</param>
        /// <param name="listings">item listings</param>
        /// <param name="dataLevel">Shopping cart datalevel</param>
        /// <param name="productValidator">product validator class</param>
        /// <param name="searchEngine">Search engine</param>
        /// <param name="wishListId">selected wishlist id</param>
        /// <returns>The service response</returns>
        public virtual ShoppingCart AddItemToCartFromWhishList(string shoppingCartId, string customerId,
            Collection<ViewModel.Listing> listings, ShoppingCartDataLevel dataLevel, IProductValidator productValidator,
            ISearchEngine searchEngine, string wishListId)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentException("wishListId");
            }
            if (string.IsNullOrWhiteSpace(customerId))
            {
                throw new ArgumentException("customerId");
            }
           
            ShoppingCart response = new ShoppingCart();

            response = Controller.AddItemToCartFromWhishList(shoppingCartId, customerId, listings, dataLevel, productValidator,
            searchEngine, wishListId);

            if (response != null)
            {
                ServiceUtility.SetCartIdInPersistenceStorage(SessionType.SignedIn, CartType.Shopping, response.CartId);
            }

            return response;
        }

        /// <summary>
        /// Updates items on wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="listings">The items to update.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        public virtual WishListResponse UpdateItemsOnWishList(string wishListId, IEnumerable<Listing> listings)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentNullException("wishListId");
            }

            if (listings == null)
            {
                throw new ArgumentNullException("listings");
            }
            WishListResponse response = new WishListResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                response.WishList = Controller.UpdateItemsOnWishList(wishListId, customerId, listings);
            });

            return response;
        }

        /// <summary>
        /// Updates the wish list properties.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="wishListName">The wish list name.</param>
        /// <param name="isFavorite">The favorite attribute of wish list.</param>
        /// <returns>
        /// The service response containing the wish list.
        /// </returns>
        public virtual WishListResponse UpdateWishListProperties(string wishListId, string wishListName, bool? isFavorite)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentNullException("wishListId");
            }
            if (string.IsNullOrWhiteSpace(wishListName))
            {
                throw new ArgumentNullException("wishListName");
            }
            WishListResponse response = new WishListResponse();

            string customerId = ServiceUtility.GetCurrentCustomerId(true);

            WishList wishList = new WishList();
            wishList.Id = wishListId;
            wishList.Name = wishListName;
            wishList.IsFavorite = isFavorite;
            wishList.CustomerId = customerId;

            ExecuteAfterRequestValidation(response, () =>
            {
                response.WishList = Controller.UpdateWishListProperties(wishList,customerId);
            });

            return response;
        }

        /// <summary>
        /// Remove item from wish list.
        /// </summary>
        /// <param name="wishListId">The wish list Id.</param>
        /// <param name="listingId">The item to remove from the wish list.</param>
        public virtual NullResponse RemoveItemFromWishList(string wishListId, string listingId)
        {
            if (string.IsNullOrWhiteSpace(wishListId))
            {
                throw new ArgumentNullException("wishListId");
            }

            if (listingId == null)
            {
                throw new ArgumentNullException("listings");
            }
            NullResponse response = new NullResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string customerId = ServiceUtility.GetCurrentCustomerId(true);
                Controller.RemoveItemFromWishList(wishListId, customerId, listingId);
            });

            return response;
        }


        /// <summary>
        /// Gets Demo implementation of ISearchEngine.
        /// </summary>
        protected override ISearchEngine SearchEngine
        {
            get { return searchEngine; }
        }

        /// <summary>
        /// Gets Demo implementation of IProductValidator.
        /// </summary>
        protected override IProductValidator ProductValidator
        {
            get { return productValidator; }
        }

        /// <summary>
        /// Gets Demo implementation of ServiceUtilityBase.
        /// </summary>
        protected override ServiceUtilityBase ServiceUtility
        {
            get { return serviceProxy; }
        }

        /// <summary>
        /// Gets Demo implementation of IConfiguration.
        /// </summary>
        protected override IConfiguration Configuration
        {
            get { return new SiteConfiguration(); }
        }

        /// <summary>
        /// Executes the given service action and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="action">Service action.</param>
        protected override void ExecuteAfterRequestValidation(ServiceResponse response, System.Action action)
        {
            if (action == null)
            {
                throw new ArgumentNullException("action");
            }

            AFMServiceResponse afmServiceResponse = null;
            if (response != null)
            {
                afmServiceResponse = new AFMServiceResponse();
                afmServiceResponse.CanRetry = response.CanRetry;
                foreach (var error in response.Errors)
                {
                    AFM.Commerce.Entities.ResponseError responseError = new AFM.Commerce.Entities.ResponseError();
                    responseError.ErrorCode = error.ErrorCode;
                    responseError.ErrorMessage = error.ErrorMessage;
                    responseError.ExtendedErrorMessage = error.ExtendedErrorMessage;
                    responseError.RedirectUrl = error.RedirectUrl;
                    responseError.ShowDebugInfo = error.ShowDebugInfo;
                    responseError.Source = error.Source;
                    responseError.StackTrace = error.StackTrace;
                    afmServiceResponse.Errors.Add(responseError);

                }
                afmServiceResponse.RedirectUrl = response.RedirectUrl;
            }
            AFMDataUtilities.ExecuteAndHandleExceptions(afmServiceResponse, action);
        }

        /// <summary>
        /// Executes handler and handles exceptions.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void Execute(ServiceResponse response, Action action)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks if user is authenticated and assosiated then calls <see cref="ExecuteAfterPostRequestValidation"/> with new execution handler.
        /// </summary>
        /// <param name="response">Service response.</param>
        /// <param name="executionHandler">Execution handler.</param>
        protected override void ExecuteForAuthorizedUser(ServiceResponse response, Action<string> action)
        {
            throw new NotImplementedException();
        }
  
    }
}
