﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AFMExtendedService.cs" company="Microsoft">
// NS by muthait
// </copyright>
// <summary>
//   The afm extended service base.   
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AFM.Commerce.Services
{
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.ClientManager;
    using AFM.Commerce.Framework.Core.Models;
    using AFM.Commerce.Framework.Extensions;
    using AFM.Commerce.Framework.Extensions.Controllers;
    using AFM.Commerce.Framework.Extensions.Utils;
    using Microsoft.Dynamics.Commerce.Runtime.Client;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.ServiceModel.Web;
    using System.Web;
    using System.Web.Configuration;

    // [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]

    /// <summary>
    /// The afm extended service base.
    /// </summary>
    public abstract class AFMExtendedServiceBase : ServiceBase<AFMShoppingCartController>, IAFMExtendedService
    {
        /// <summary>
        /// The set zip code.
        /// </summary>
        /// <param name="zipCode">
        /// The zip code.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual ShoppingCartResponse SetZipCode(string zipCode, bool isCheckoutSession)
        {
            if (string.IsNullOrWhiteSpace(zipCode))
            {
                throw new ArgumentNullException("zipCode");
            }
            ShoppingCartResponse response = new ShoppingCartResponse();
            ExecuteAfterRequestValidation(response, () =>
               {
                   //NS  developed by v-hapat on dated 11/19/2014  set Affiliation based on Zipcode
                   var tiers = new List<long>();
                   string customerId = ServiceUtility.GetCurrentCustomerId(false);
                   if (string.IsNullOrEmpty(customerId))
                       customerId = AFMDataUtilities.GetCustomerIdTempFromcookie();
                   string cartId;
                   if (isCheckoutSession)
                       cartId = ServiceUtility.GetCheckoutCartId();
                   else
                       cartId = ServiceUtility.GetShoppingCartId();
                   AFMDataUtilities.SetAFMZipCodeInCookie(zipCode);
                   if ( !string.IsNullOrEmpty( cartId) )
                   {                       
                       //Add for Estimating shipping call when you update ZipCode for Shoppinf Cart                  
                       OrderManager manager = OrderManager.Create(CrtUtilities.GetCommerceRuntime());
                       Cart cart = manager.GetCart(cartId, CalculationModes.None);

                       this.Controller.UpdateZipCodeAffiliationtoCart(ref cart);

                       //AFMShoppingCartController objShoppingCartController = new AFMShoppingCartController();

                       //Update when ZipCode is Changed Update ATP Related data Cart to use Next ATP call
                       ICollection<CartLine> cartLinetoUpdate = cart.CartLines;
                       this.Controller.UpdateATPDataCartLine(ref cartLinetoUpdate);
                       cart = manager.UpdateCartLines(cart.Id, cart.CustomerId, cartLinetoUpdate,CalculationModes.None);
                       //Update when ZipCode is Changed

                       ShoppingCart shoppingCart = this.Controller.GetShoppingCart(cartId, customerId, ShoppingCartDataLevel.All, this.ProductValidator, this.SearchEngine, isCheckoutSession);

                       //objShoppingCartController.AddEstShippingCostToCart(shoppingCart, cart, AFMDataUtilities.GetCustomerIdFromCookie(), isCheckoutSession);

                       if (cart[CartConstant.ChargeTypeError] != null && !string.IsNullOrEmpty(cart[CartConstant.ChargeTypeError].ToString()))
                       {
                           shoppingCart.ChargeTypeError = cart[CartConstant.ChargeTypeError].ToString();
                       }
                       var ServiceItem = shoppingCart.Items.Where(p => p.AFMItemType == Convert.ToInt32(AFMItemType.DeliveryServiceItem)).ToList();
                       if (ServiceItem != null || ServiceItem.Count > 0)
                           shoppingCart.ChargeTypeError = string.Empty;

                       response.ShoppingCart = shoppingCart;
                       //CS Merged with upgrade
                       if (!string.IsNullOrEmpty(response.ShoppingCart.AvalaraTaxError))
                       {
                           response.AddError(Resource.AvalaraTaxNotFound, response.ShoppingCart.AvalaraTaxError, response.ShoppingCart.TaxAmountWithCurrency);
                       }

                       if (!string.IsNullOrEmpty(response.ShoppingCart.ChargeTypeError))
                       {
                           response.AddError("Error: ", response.ShoppingCart.ChargeTypeError, string.Empty);
                       }

                       if (!string.IsNullOrEmpty(response.ShoppingCart.ATPError))
                       {
                           response.AddError("Error: ", response.ShoppingCart.ATPError, string.Empty);
                       }
                       //CE Merged with upgrade
                   }

                   //Add for Estimating shipping call when you update ZipCode
                   //NE  developed by v-hapat on dated 11/19/2014  set Affiliation based on Zipcode
               });

            return response;
        }

        /// <summary>
        /// The get zip code.
        /// </summary>
        /// <returns>
        /// The <see cref="AFMZipCodeResponse"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual AFMZipCodeResponse GetZipCode()
        {
            string AFMZipCode = AFMDataUtilities.GetAFMZipCodeFromCookie();
            AFMZipCodeResponse response = new AFMZipCodeResponse();
            response.AFMZipCode = AFMZipCode;
            return response;
        }

        /// <summary>
        /// The get Culture id.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual string GetCultureId()
        {
            string cultureId = AFMDataUtilities.GetCultureIdFromCookie();
            return cultureId;

        }

        /// <summary>
        /// The send OrderSummary to payment gateway page
        /// </summary>
        /// <param name="orderSummary">
        /// The orderSummary.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual string SendOrderSummary(AFMOrderSummary orderSummary)
        {
            return AFMDataUtilities.SendOrderSummaryInCookie(orderSummary);
        }

        /// <summary>
        /// The get affiliation by zip code.
        /// </summary>
        /// <param name="zipCode">
        /// The zip code.
        /// </param>
        /// <returns>
        /// The <see cref="AFMVendorAffiliation"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual AFMVendorAffiliation GetAffiliationByZipCode(string zipCode)
        {
            if (string.IsNullOrWhiteSpace(zipCode))
            {
                throw new ArgumentNullException("zipCode");
            }
            var afmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(zipCode);
            return afmVendorAffiliation;
        }

        /// <summary>
        /// The get prdtswith nap info.
        /// </summary>
        /// <param name="productIds">
        /// The product ids.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual List<AFMPrdtNAP> GetPrdtswithNAPInfo(List<long> productIds)
        {
            var getPrdtNAPList = AFMProductsController.GetPrdtNAPListData(productIds);
            return getPrdtNAPList;
        }

        /// <summary>
        /// The get price by affiliation.
        /// </summary>
        /// <param name="affiliationId">
        /// The affiliation id.
        /// </param>
        /// <param name="products">
        /// The products.
        /// </param>
        /// <returns>
        /// The <see cref="ReadOnlyCollection"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual ReadOnlyCollection<AfmAffiliationPrice> GetPriceByAffiliation(long affiliationId, List<Product> products)
        {
            var prdtList = products;
            var itemIds = prdtList.Select(prdt => prdt.ItemId).ToList();
            var afmPricingController = new AFMPricingController();
            var afmGetPriceByAffiliation = afmPricingController.GetPriceByAffiliation(affiliationId, itemIds, products.AsReadOnly());
            return afmGetPriceByAffiliation;
        }

        /// <summary>
        /// The create order.
        /// </summary>
        /// <param name="tenderDataLine">
        /// The tender data line.
        /// </param>
        /// <param name="emailAddress">
        /// The email address.
        /// </param>
        /// <param name="cardToken">
        /// The card token.
        /// </param>
        /// <returns>
        /// The <see cref="CreateSalesOrderResponse"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual CreateSalesOrderResponse CreateOrder(IEnumerable<TenderDataLine> tenderDataLine, AFMSalesTransHeader headerValues, string emailAddress, string cardToken)
        {
            if (string.IsNullOrWhiteSpace(emailAddress))
            {
                throw new ArgumentNullException("emailAddress");
            }
            if (tenderDataLine == null)
            {
                throw new ArgumentNullException("tenderDataLine");
            }
            var response = new CreateSalesOrderResponse();
            ExecuteAfterRequestValidation(response, () =>
            {
                string checkoutCartId = ServiceUtility.GetCheckoutCartId();
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                string customerIdTemp = AFMDataUtilities.GetCustomerIdTempFromcookie();
                string userName = ServiceUtility.GetUserName();
                if (string.IsNullOrEmpty(customerId))
                    customerId = AFMDataUtilities.GetCustomerIdFromCookie();
                if (string.IsNullOrEmpty(customerId) && !string.IsNullOrEmpty(customerIdTemp))
                    customerId = customerIdTemp;
                //CS Below Check Out Cart Objec is not used and find unwated Call 
                //ShoppingCart checkoutCart = new AFMShoppingCartController().GetShoppingCart(checkoutCartId, customerId,
                //ShoppingCartDataLevel.Minimal, this.ProductValidator, this.SearchEngine, true);
                //IEnumerable<string> lineIdsToRemoveOnOrderCreation = checkoutCart.Items.Select(i => i.LineId);
                //CE Below Check Out Cart Objec is not used and find unwated Call
                AFMCheckoutController afmCheckoutController = new AFMCheckoutController();
                afmCheckoutController.SetConfiguration(new SiteConfiguration());
                var orderNumber = afmCheckoutController.CreateOrder(checkoutCartId, customerId, cardToken,
                    tenderDataLine, headerValues, emailAddress, userName, this.ProductValidator, this.SearchEngine);

                if (orderNumber.Contains("Error:"))
                {
                    var ResponseErrorMessage = new Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services.ResponseError(orderNumber.Remove(0, 6).ToString());
                    response.Errors.Add(ResponseErrorMessage);
                }
                else
                {
                    response.OrderNumber = orderNumber;
                    response.AFMUserCartData = AFMDataUtilities.GetUserCartInfo();
                    if (response.AFMUserCartData != null)
                    {
                        response.AFMUserCartData.PaymentAddress = null;
                        if (response.AFMUserCartData.LineItems != null)
                            response.AFMUserCartData.LineItems = null;
                    }
                }

                // Since the order has been successfully created by this point,
                // we should swallow all exceptions after this to prevent the call from failing.
                // We do not want the user to unintentionally create duplicate orders.
                //try
                //{
                //Now removing the items purchased by the customer which were in the 'Checkout' cart from the 'Shopping' cart
                // At the same time, retain any items that were added to the 'Shopping' cart in a different session.
                //CS Below fetch cart ID is not used and find unwated Call
                //string shoppingCartId = ServiceUtility.GetShoppingCartId();
                //if (shoppingCartId == null)
                //{
                //    shoppingCartId = AFMDataUtilities.GetAnonymousShoppingCartId(CookieConstants.AnonymousShoppingCartToken);
                //    customerId = string.Empty;
                //}
                //CE Below fetch cart ID is not used and find unwated Call
                //Controller.RemoveItems(shoppingCartId, customerId,
                //    lineIdsToRemoveOnOrderCreation.ToList(), ShoppingCartDataLevel.Minimal,
                //    this.ProductValidator, this.SearchEngine);
                //}
                //catch (Exception ex)
                //{
                //    NetTracer.Error(ex, "Failed to remove items from shopping cart after order creation.");
                //    response.AddError("NeedToDetermineErrorCode", "Failed to remove items from shopping cart.", ex);
                //}

                /*  try
                  {
                      // Clear the checkout cart id from the cookie.
                     ServiceUtility.ClearCheckoutCartIdInPersistenceStorage();
                      // We will leave the shopping cart cookie as is, in case the user wants to resume their browsing session after checking out.
                  }
                  catch (Exception ex)
                  {
                      NetTracer.Error(ex, "Failed to delete cookies after the end of order creation.");
                      response.AddError("NeedToDetermineErrorCode",
                          "Failed to delete cookies after the end of order creation.", ex);
                  }*/

            });

            return response;
        }

        /// <summary>
        /// Starts the checkout process by creating a secure cart and returning it.
        /// </summary>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>A new cart with a random cart id that should be used during the secure checkout process.</returns>
        public virtual ShoppingCartResponse CommenceCheckout(ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();

            ExecuteAfterRequestValidation(response, () =>
            {
                string shoppingCartId = ServiceUtility.GetShoppingCartId();
                string customerId = ServiceUtility.GetCurrentCustomerId(false);
                string previousCheckoutCartId = ServiceUtility.GetCheckoutCartId();
                SessionType sessionType = ServiceUtility.GetSessionType();
                var shoppingCartcontroller = new AFMShoppingCartController();

                // Shopping cart would be null if the user lands on the checkout page immediately after signing in.
                // In this case we need to claim the anonymous shopping cart and assign it to the signed in user,
                // because there is no explicit get shopping cart call, which implicitly does the claiming, in the checkout page.
                if (String.IsNullOrWhiteSpace(shoppingCartId) && sessionType == SessionType.SignedIn)
                {
                    ShoppingCart claimedShoppingCart = shoppingCartcontroller.GetShoppingCart(shoppingCartId, customerId, ShoppingCartDataLevel.Minimal, this.ProductValidator, this.SearchEngine);
                    shoppingCartId = claimedShoppingCart.CartId;
                    ServiceUtility.SetCartIdInPersistenceStorage(sessionType, Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.CartType.Shopping, shoppingCartId);
                }

                response.ShoppingCart = shoppingCartcontroller.AFMCommenceCheckout(shoppingCartId, customerId, previousCheckoutCartId, this.ProductValidator, this.SearchEngine, dataLevel);

                if (response.ShoppingCart == null)
                {
                    string message = string.Format("Unable to create a checkout cart from shopping cart id: {0}", shoppingCartId);
                    throw new InvalidOperationException(message);
                }

                // Update the checkout cart id cookie.
                ServiceUtility.SetCartIdInPersistenceStorage(sessionType, Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.CartType.Checkout, response.ShoppingCart.CartId);
            });

            return response;
        }

        /// <summary>
        /// The set shippingto line items.
        /// </summary>
        /// <param name="shippingOptions">
        /// The shipping options.
        /// </param>
        /// <param name="dataLevel">
        /// The data level.
        /// </param>
        /// <returns>
        /// The <see cref="ShoppingCartResponse"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual ShoppingCartResponse SetShippingtoLineItems(SelectedLineDeliveryOption shippingOptions, ShoppingCartDataLevel dataLevel)
        {
            if (shippingOptions == null)
            {
                throw new ArgumentNullException("shippingOptions");
            }
            var shoppingCartResponse = new ShoppingCartResponse();

            //NS developed by v-hapat on date 20/11/2014 Set Affilication before you set Delivery Detail
            var tiers = new List<long>();
            var cartId = AFMDataUtilities.GetCartIdFromRequestCookie(ServiceUtility.GetSessionType(), ServiceUtility.GetCurrentCartType(true));
            var userCartInfo = AFMDataUtilities.GetUserCartInfo();
            //Bug 88866 : CS by spriya Dated 06/05/11
            if (userCartInfo != null && userCartInfo.ShippingAddress != null && !string.IsNullOrEmpty(userCartInfo.ShippingAddress.ZipCode)
                 && userCartInfo.ShippingAddress.ZipCode.Length > 5)
                userCartInfo.ShippingAddress.ZipCode = userCartInfo.ShippingAddress.ZipCode.Substring(0, 5);
            //Bug 88866 : CE by spriya Dated 06/05/11
            AFMVendorAffiliation afmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(userCartInfo.ShippingAddress.ZipCode);
            if (afmVendorAffiliation != null && afmVendorAffiliation.AffiliationId > 0)
            {
                tiers.Add(afmVendorAffiliation.AffiliationId);
                this.Controller.SetCartAffiliationLoyaltyTiers(cartId, tiers);
            }
            //NS developed by v-hapat on date 20/11/2014 Set Affilication before you set Delivery Detail

            ExecuteAfterRequestValidation(
                shoppingCartResponse,
                () =>
                {
                    var customerId = ServiceUtility.GetCurrentCustomerId(false);
                    if (string.IsNullOrEmpty(customerId))
                        customerId = AFMDataUtilities.GetCustomerIdTempFromcookie();
                    var checkoutCartId = ServiceUtility.GetCheckoutCartId();

                    var afmcheckoutController = new AFMCheckoutController();
                    afmcheckoutController.SetConfiguration(new SiteConfiguration());
                    shoppingCartResponse.ShoppingCart = afmcheckoutController.SetShippingOptionPerItem(checkoutCartId, customerId, shippingOptions, this.ProductValidator, this.SearchEngine, dataLevel);
                });

            return shoppingCartResponse;
        }

        /// <summary>
        /// The get card token data.
        /// </summary>
        /// <param name="tranid">
        /// The tranid.
        /// </param>
        /// <returns>
        /// The <see cref="AFMCardTokenData"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual AFMCardTokenData GetCardTokenData(string tranid)
        {
            return AFMPaymentController.GetCardToken(tranid);
        }

        /// <summary>
        /// The set user cart info.
        /// </summary>
        /// <param name="userCartData">
        /// The user cart data.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual string SetUserCartInfo(AFMUserCartData userCartData)
        {
            if (userCartData == null)
            {
                throw new ArgumentNullException("userCartData");
            }
            return AFMDataUtilities.SetUserCartInfo(userCartData);
        }

        /// <summary>
        /// The get user cart info.
        /// </summary>
        /// <returns>
        /// The <see cref="AFMUserCartData"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual AFMUserCartData GetUserCartInfo()
        {
            return AFMDataUtilities.GetUserCartInfo();
        }

        /// <summary>
        /// The set user page mode.
        /// </summary>
        /// <param name="userPageMode">
        /// The user page mode.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual string SetUserPageMode(AFMUserPageMode userPageMode)
        {
            return AFMDataUtilities.SetUserPageMode(userPageMode);
        }

        /// <summary>
        /// The get user page mode.
        /// </summary>
        /// <returns>
        /// The <see cref="AFMUserPageMode"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual AFMUserPageMode GetUserPageMode()
        {
            return AFMDataUtilities.GetUserPageMode();
        }

        /// <summary>
        /// The set validareAddress.
        /// </summary>
        /// <param name="address">
        /// The address
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual AFMAddressData ValidateAddress(Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Address address)
        {
            return AFMCheckoutController.ValidateAddress(address);
        }

        /// <summary>
        /// The clear all cookies.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public virtual string ClearAllCookies()
        {
            try
            {
                AFMDataUtilities.ClearUserPageModeFromCookie();
                AFMDataUtilities.ClearUserCartInfoFromCookie();
                AFMDataUtilities.ClearCustomerIdTempFromCookie();
                var cartId = AFMDataUtilities.GetShoppingCartId();
                var cartManager = AFMCartManager.Create(CrtUtilities.GetCommerceRuntime());
                cartManager.UpdateCartData(false, cartId);
                if (AFMDataUtilities.GetSessionType() == SessionType.SignedIn)
                {
                    AFMDataUtilities.ClearShoppingCartIdResponseCookies(SessionType.SignedIn);
                    AFMDataUtilities.ClearCheckoutCartIdResponseCookies(SessionType.SignedIn);
                }
                else
                {
                    AFMDataUtilities.ClearShoppingCartIdResponseCookies(SessionType.Anonymous);
                    AFMDataUtilities.ClearCheckoutCartIdResponseCookies(SessionType.Anonymous);
                }

               /*
                * //NS: By Spriya: Bug#61496: ECS - Subtotal, Home Delivery and Sales tax charges are $0.00 under Order Summary on Payment screen 
                //AFMDataUtilities.ClearPaymentOrderSummaryFromCookie();
                //NE by Spriya
                */
                return "Success";
            }
            catch 
            {
                return "failure";
            }
        }

        /// <summary>
        /// The get addressStateList.
        /// </summary>
        /// <param name="tranid">
        /// The countryRegionId.
        /// </param>
        /// <returns>
        /// The <see cref="countryRegionId"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual List<AFMAddressState> GetAFMAddressState(string countryRegionId)
        {
            return AFMCheckoutController.GetAddressList(countryRegionId);
        }

        /// <summary>
        /// ValidateZipcode.
        /// </summary>
        /// <param name="zipCode">
        /// The zipCode.
        /// </param>
        /// <param name="countryRegionId">
        /// The countryRegionId.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual bool ValidateZipcode(string zipCode, string countryRegionId)
        {
            return AFMCheckoutController.ValidateZipcode(zipCode, countryRegionId);
        }


        /// NS by muthait dated 13Nov2014
        /// <summary>
        /// Gets the active shopping cart associated with the current customer.
        /// </summary>
        /// <param name="customerId">Gets the customerId of the current customer</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The service response containing the shopping cart.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual ShoppingCartResponse GetActiveShoppingCart(string customerId, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();

            Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.CartType cartType = this.ServiceUtility.GetCurrentCartType(false);
            SessionType sessionType = this.ServiceUtility.GetSessionType();

            ExecuteAfterRequestValidation(response, () =>
            {
                response.ShoppingCart = new AFMShoppingCartController().GetActiveShoppingCart(customerId, dataLevel, this.ProductValidator, this.SearchEngine);
                string responseCartId = string.Empty;
                if (response.ShoppingCart != null)
                {
                    responseCartId = response.ShoppingCart.CartId;
                    ServiceUtility.SetCartIdInPersistenceStorage(sessionType, cartType, responseCartId);
                }
                else
                {
                    string message = string.Format("No Active Shopping Cart available for Current Customer");
                    throw new InvalidOperationException(message);
                }

            });
            if (!string.IsNullOrEmpty(response.ShoppingCart.AvalaraTaxError))
            {
                response.AddError(Resource.AvalaraTaxNotFound, response.ShoppingCart.AvalaraTaxError, response.ShoppingCart.TaxAmountWithCurrency);
            }

            if (!string.IsNullOrEmpty(response.ShoppingCart.ATPError))
            {
                response.AddError("Error: ", response.ShoppingCart.ATPError, string.Empty);
            }

            return response;
        }

        /// <summary>
        /// Moves the items between carts.
        /// </summary>
        /// <param name="sourceShoppingCartId">The source shopping cart identifier.</param>
        /// <param name="destinationShoppingCartId">The destination shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">The data level.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// sourceShoppingCartId
        /// or
        /// destinationShoppingCartId
        /// </exception>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual ShoppingCartResponse MoveItemsBetweenCarts(string sourceShoppingCartId, string destinationShoppingCartId, string customerId, ShoppingCartDataLevel dataLevel)
        {
            ShoppingCartResponse response = new ShoppingCartResponse();


            ExecuteAfterRequestValidation(response, () =>
            {
                AFMShoppingCartController afmShoppingCartController = new AFMShoppingCartController();
                afmShoppingCartController.SetConfiguration(new SiteConfiguration());
                response.ShoppingCart = afmShoppingCartController.MoveItemsBetweenCarts(sourceShoppingCartId, destinationShoppingCartId, customerId, dataLevel, this.ProductValidator, this.SearchEngine);
                string responseCartId = string.Empty;
                if (response.ShoppingCart != null)
                {
                    responseCartId = response.ShoppingCart.CartId;
                }
                else
                {
                    string message = string.Format("No Active Shopping Cart available for Current Customer");
                    throw new InvalidOperationException(message);
                }

            });
            if (!string.IsNullOrEmpty(response.ShoppingCart.AvalaraTaxError))
            {
                response.AddError(Resource.AvalaraTaxNotFound, response.ShoppingCart.AvalaraTaxError, response.ShoppingCart.TaxAmountWithCurrency);
            }

            if (!string.IsNullOrEmpty(response.ShoppingCart.ATPError))
            {
                response.AddError("Error: ", response.ShoppingCart.ATPError, string.Empty);
            }

            return response;
        }
        /// NE by muthait dated 13Nov2014


        /// <summary>
        /// ValidatePromocode.
        /// </summary>
        /// <param name="promoCode">
        /// The promoCode.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual bool ValidatePromocode(string promoCode)
        {
            AFMShoppingCartController objshoppingCartcontroller = new AFMShoppingCartController();
            return objshoppingCartcontroller.IsDicountCodeValid(promoCode);
        }

        /// <summary>
        /// CheckAtpItemAvailability.
        /// </summary>
        /// <param name="itemId">
        /// The itemId.
        /// </param>
        /// <param name="ZipCode">
        /// The ZipCode.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual IEnumerable<Listing> CheckAtpItemAvailability(string itemId, string zipCode, int Qty)
        {
            var productList = AFMProductsController.GetProductsByItemIds(new List<string> { itemId });
            var listing = new List<Listing>();
            dynamic dimensionData = new System.Dynamic.ExpandoObject();
            dynamic productListing = new System.Dynamic.ExpandoObject();
            foreach (var product in productList)
            {
                dimensionData.Color = "Black";
                dimensionData.Size = "12 inch";

                productListing.Name = product.ProductName;
                productListing.Description = product.Description;
                productListing.DimensionValues = dimensionData;
                productListing.ImageUrl = "/images/Sofa.png";
                productListing.ImageAlt = "";

                var productDetails = Convert.ToString(productListing);

                var listingtemp = new Listing { ListingId = product.RecordId, ItemId = product.ProductNumber, Quantity = Qty, ProductDetails = productDetails };
                listing.Add(listingtemp);
            }
            string customerId = AFMDataUtilities.GetCustomerIdFromCookie();
            var ATPoutput = this.Controller.CheckAtpItemAvailability(customerId, listing.AsEnumerable(), zipCode);
            if (ATPoutput != null)
            {
                if (!ATPoutput.ItemGroupings[0].Items[0].IsAvailable)
                    listing.Clear();
            }
            else
            {
                listing.Clear();
            }
            return listing.AsEnumerable();
        }


        //CS Developed by spriya
        /// <summary>
        /// Gets the Shipping addresses.
        /// </summary>
        /// <returns>An address collection response of delivery addresses.</returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual AddressCollectionResponse GetShippingAddresses(bool IsShipping)
        {
            AddressCollectionResponse response = new AddressCollectionResponse();

            Collection<Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Address> addresses = null;
            ExecuteForAuthorizedUser(response, (string accountNumber) =>
            {
                if (!string.IsNullOrEmpty(accountNumber))
                    addresses = new AFMCustomerController().GetShippingAddresses(accountNumber, IsShipping);
            });

            if (addresses == null || !addresses.Any())
            {
                string customer = AFMDataUtilities.GetCustomerIdFromCookie();
                if (!string.IsNullOrEmpty(customer))
                    addresses = new AFMCustomerController().GetShippingAddresses(customer, IsShipping);
            }

            response.Addresses = addresses;

            return response;
        }


        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual AFMOrderConfirmationResponse GetOrderConfirmationDetails()
        {
            return this.Controller.GetOrderConfirmationDetails(this.SearchEngine);
        }

        /// <summary>
        /// Redirect to Payment Gateway
        /// </summary>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual string RedirectToPayment()
        {
            var cartId = AFMDataUtilities.GetCheckoutCartId();
            // NS Changed by AxL for synchrony FDD on 14/12/2015
            //var tranId = HttpUtility.UrlPathEncode(AFM.Commerce.Framework.Extensions.Utils.SecurityManager.HashData(cartId));
            var tranId = HttpUtility.UrlEncode(AFM.Commerce.Framework.Extensions.Utils.SecurityManager.EncryptCartID(cartId));
            // NE Changed by AxL for synchrony FDD on 14/12/2015

             //NS: By Spriya: Bug#61496: ECS - Subtotal, Home Delivery and Sales tax charges are $0.00 under Order Summary on Payment screen 
            var jsonData = AFMDataUtilities.GetPaymentOrderSummaryFromCookie();            
            var Base64Data = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(jsonData));
            var OSData = AFM.Commerce.Framework.Extensions.Utils.SecurityManager.ToRestFriendlyFromBase64(Base64Data);
            var HashedData = AFM.Commerce.Framework.Extensions.Utils.SecurityManager.CalculateHashedPassword(OSData);

            return WebConfigurationManager.AppSettings["PaymentGatewayUrl"] + "?tranid=" + tranId + "&os=" + OSData + "." + HashedData;
            //NE: By Spriya: Bug#61496
            
        }

        //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID
        /// <summary>
        /// This method validates if given RSA ID is valid by calling IsValidRSAId method of AFMShoppingCartController class
        /// </summary>
        /// <param name="rsaId"></param>
        /// <returns></returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual bool ValidateRsaId(string rsaId,  bool isCheckoutSession)
        {
            if (string.IsNullOrEmpty(rsaId))
                return false;

            string cartId;
            if (isCheckoutSession)
                cartId = ServiceUtility.GetCheckoutCartId();
            else
                cartId = ServiceUtility.GetShoppingCartId();

            AFMShoppingCartController objshoppingCartcontroller = new AFMShoppingCartController();
            return objshoppingCartcontroller.IsValidRsaId(cartId, rsaId);
        }

        /// <summary>
        /// This method removes the RSA ID associated with the cart by calling RemoveRsaIdFromCart method of AFMShoppingCartController class
        /// </summary>
        /// <param name="rsaId"></param>
        /// <returns></returns>
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        public virtual bool RemoveRsaIdFromCart(bool isCheckoutSession)
        {
            string cartId;
            if (isCheckoutSession)
                cartId = ServiceUtility.GetCheckoutCartId();
            else
                cartId = ServiceUtility.GetShoppingCartId();
            AFMShoppingCartController objshoppingCartcontroller = new AFMShoppingCartController();
            objshoppingCartcontroller.RemoveRsaIdFromCart(cartId);
            return true;
        }
        //NE - RxL
    }
}
