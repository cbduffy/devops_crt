﻿using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;

namespace AFM.Commerce.Services
{
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.Models;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ServiceModel;


    /// <summary>
    /// The checkout service interface.
    /// </summary>
    [ServiceContract]
    public interface IAFMExtendedService
    {
        /// <summary>
        /// Sets the AFM
        /// to Cookie
        /// </summary>
        /// <returns>The service sets AFMZipCode to Cookie container of AFMZipcode</returns>
        [OperationContract]
        ShoppingCartResponse SetZipCode(string zipCode, bool isCheckoutSession);

        /// <summary>
        /// Gets the AFMZipCode from Cookie
        /// </summary>
        /// <returns>The service gets AFMZipCode from Cookie container of AFMZipcode</returns>
        [OperationContract]
        AFMZipCodeResponse GetZipCode();

          /// <summary>
        /// Gets the Culture Id from Cookie
        /// </summary>
        /// <returns>The service gets Culture Id from Cookie container of cuid</returns>
        [OperationContract]
        string GetCultureId();

        /// <summary>
        /// The send OrderSummary to payment gateway page
        /// </summary>
        /// <param name="orderSummary">
        /// The orderSummary.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string SendOrderSummary(AFMOrderSummary orderSummary);

        /// <summary>
        /// Gets the AffilitationDetails for ZipCode
        /// </summary>
        /// <returns>The service gets AffilitationDetails using ZipCode</returns>
        [OperationContract]
        AFMVendorAffiliation GetAffiliationByZipCode(string zipCode);

        /// <summary>
        /// Gets the List of Product with NAP data based on effective date 
        /// </summary>
        /// <returns>The service gets ReadOnlyCollection of ListingPrice</returns>
        [OperationContract]
        List<AFMPrdtNAP> GetPrdtswithNAPInfo(List<long> ProductIds);

        /// <summary>
        /// Gets Price by affiliation
        /// </summary>
        /// <param name="affiliationId">Affiliation ID</param>
        /// <param name="products">List of products</param>
        /// <returns>
        /// Gets Prices for products by affiliation
        /// </returns>
        ReadOnlyCollection<AfmAffiliationPrice> GetPriceByAffiliation(long affiliationId, List<Product> products);

        /// <summary>
        /// Processes a sales order.
        /// </summary>
        /// <param name="tenderDataLine">Tender lines data containing payments for the order.</param>
        /// <param name="emailAddress">The email address.</param>
        /// <param name="cardToken">The card Token</param>
        /// <returns>
        /// The updated shopping cart item collection.
        /// </returns>
        [OperationContract]
        CreateSalesOrderResponse CreateOrder(IEnumerable<TenderDataLine> tenderDataLine, AFMSalesTransHeader headerValues, string emailAddress, string cardToken);

        /// <summary>
        /// Starts the checkout process by creating a secure cart and returning it.
        /// </summary>
        /// <param name="dataLevel">The shopping cart data level.</param>
        /// <returns>A new cart with a random cart id that should be used during the secure checkout process.</returns>
        [OperationContract]
        ShoppingCartResponse CommenceCheckout(ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Get cart Token data from payment connector
        /// </summary>
        /// <param name="tranId">tranId of the current cart object</param>
        /// <returns>
        /// The cart token data with additional responses like last 4 digits, expiry date, etc.
        /// </returns>
        [OperationContract]
        AFMCardTokenData GetCardTokenData(string tranid);

        /// <summary>
        /// Sets the User Cart Info
        /// to Cookie
        /// </summary>
        /// <returns>The service sets User Cart Info to Cookie container of UsercartInfo</returns>
        [OperationContract]
        string SetUserCartInfo(AFMUserCartData userCartData);

        /// <summary>
        /// Gets the UsercartInfo from Cookie
        /// </summary>
        /// <returns>The service gets User Cart Info from Cookie container of UsercartInfo</returns>
        [OperationContract]
        AFMUserCartData GetUserCartInfo();


        /// <summary>
        /// Sets the User Cart Info
        /// to Cookie
        /// </summary>
        /// <returns>The service sets User Cart Info to Cookie container of UsercartInfo</returns>
        [OperationContract]
        string SetUserPageMode(AFMUserPageMode userPageMode);

        /// <summary>
        /// Gets the UsercartInfo from Cookie
        /// </summary>
        /// <returns>The service gets User Cart Info from Cookie container of UsercartInfo</returns>
        [OperationContract]
        AFMUserPageMode GetUserPageMode();

        /// <summary>
        /// Clears all the cookie value
        /// </summary>
        /// <returns>Clears All Cookie Values</returns>
        [OperationContract]
        string ClearAllCookies();

        /// <summary>
        /// The set shippingto line items.
        /// </summary>
        /// <param name="shippingOptions">
        /// The shipping options.
        /// </param>
        /// <param name="dataLevel">
        /// The data level.
        /// </param>
        /// <returns>
        /// The <see cref="ShoppingCartResponse"/>.
        /// </returns>
        [OperationContract]
        ShoppingCartResponse SetShippingtoLineItems(SelectedLineDeliveryOption shippingOptions, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// validateAddress
        /// </summary>
        /// <param name="address"></param>
        /// <returns>bool</returns>
        /// Changed returned type to AFMAddressData to return suggested address along with object
        [OperationContract]
        AFMAddressData ValidateAddress(Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Address address);

        /// <summary>
        /// GetAFMAddressState
        /// </summary>
        /// <param name="countryRegionId"></param>
        /// <returns>AFMAddressState</returns>
        [OperationContract]
        List<AFMAddressState> GetAFMAddressState(string countryRegionId);

        /// NS by muthait dated 13Nov2014
        /// <summary>
        /// Gets the active shopping cart associated with the current customer.
        /// </summary>
        /// <param name="customerId">Gets the customerId of the current customer</param>
        /// <param name="dataLevel">Represents the data level of the cart entity that is required by the caller.</param>
        /// <returns>The service response containing the shopping cart.</returns>
        [OperationContract]
        ShoppingCartResponse GetActiveShoppingCart(string customerId, ShoppingCartDataLevel dataLevel);

        /// <summary>
        /// Moves the items between carts.
        /// </summary>
        /// <param name="sourceShoppingCartId">The source shopping cart identifier.</param>
        /// <param name="destinationShoppingCartId">The destination shopping cart identifier.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="dataLevel">The data level.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// sourceShoppingCartId
        /// or
        /// destinationShoppingCartId
        /// </exception>
        ShoppingCartResponse MoveItemsBetweenCarts(string sourceShoppingCartId, string destinationShoppingCartId, string customerId, ShoppingCartDataLevel dataLevel);
        /// NE by muthait dated 13Nov2014

        /// <summary>
        /// validateZipcode
        /// </summary>
        /// <param name="zipCode"></param>
        /// <param name="countryRegionId"></param>
        /// <returns>bool</returns>
        [OperationContract]
        bool ValidateZipcode(string zipCode, string countryRegionId);

        /// <summary>
        /// ValidatePromocode.
        /// </summary>
        /// <param name="promoCode">
        /// The promoCode.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [OperationContract]
        bool ValidatePromocode(string promoCode);

         /// <summary>
        /// CheckAtpItemAvailability.
        /// </summary>
        /// <param name="itemId">
        /// The itemId.
        /// </param>
        /// <param name="ZipCode">
        /// The ZipCode.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [OperationContract]
        IEnumerable<Listing> CheckAtpItemAvailability(string itemId, string zipCode,int Qty);

        /// <summary>
        /// Gets the Shipping addresses.
        /// </summary>
        /// <returns>An address collection response of delivery addresses.</returns>
        [OperationContract]
        AddressCollectionResponse GetShippingAddresses(bool IsShipping);

        /// <summary>
        /// Gets the order detail.
        /// </summary>
        /// <returns>An address collection response of delivery addresses.</returns>
        [OperationContract]
        AFMOrderConfirmationResponse GetOrderConfirmationDetails();

        /// <summary>
        /// Redirect to Payment Gateway
        /// </summary>
        [OperationContract]
        string RedirectToPayment();

        //NS - RxL - FDD0318 - DMND0023660 - RSA Tracking ID   
        /// <summary>
        /// Validates if given rsa id is valid or not
        /// </summary>
        /// <param name="rsaId">RSA ID</param>
        /// <returns> true if valid else false</returns>
        [OperationContract]
        bool ValidateRsaId(string rsaId,  bool isCheckoutSession);

        /// <summary>
        /// removes rsa id associated with given cart it
        /// </summary>
        /// <param name="shoppingCartId"></param>
        [OperationContract]
        bool RemoveRsaIdFromCart(bool isCheckoutSession);
        //NE - RxL    
    }
}
