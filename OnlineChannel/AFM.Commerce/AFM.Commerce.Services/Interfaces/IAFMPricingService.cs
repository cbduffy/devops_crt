﻿
namespace AFM.Commerce.Services
{
    using AFM.Commerce.Entities;
    using AFM.Commerce.Framework.Core.Models;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ServiceModel;


    /// <summary>
    /// The checkout service interface.
    /// </summary>
    [ServiceContract]
    public interface IAFMPricingService
    {
        /// <summary>
        /// Gets the AffilitationDetails for ZipCode
        /// </summary>
        /// <returns>The service gets AffilitationDetails using ZipCode</returns>
        [OperationContract]
        AFMVendorAffiliation GetAffiliationByZipCode(string zipCode);

        /// <summary>
        /// Gets the AFMGetPriceByAffiliation 
        /// </summary>
        /// <returns>The service gets ReadOnlyCollection of ListingPrice</returns>
        [OperationContract]
        ReadOnlyCollection<AfmAffiliationPrice> GetPriceByAffiliation(long affiliationId, List<string> itemIds);

        /// <summary>
        /// Gets the GetAllPrices 
        /// </summary>
        /// <returns>The service gets ReadOnlyCollection of ListingPrice</returns>
        [OperationContract]
        AFMGetAllPrices GetAllPrices(List<string> itemIds);



        ///// <summary>
        ///// Gets the GetPriceSnapshot 
        ///// </summary>
        ///// <returns>The service gets List of ListingPrice</returns>
        //[OperationContract]
        //List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> itemIds);

        /// <summary>
        /// Gets the GetPriceSnapshot 
        /// </summary>
        /// <returns>The service gets List of ListingPrice</returns>
        [OperationContract]
        List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> itemIds,long affilication);


        /// <summary>
        /// Gets the GetPriceSnapshot 
        /// </summary>
        /// <returns>The service gets List of ListingPrice</returns>
        [OperationContract]
        List<AFMItemPriceSnapshot> GetPriceSnapshot(List<string> itemIds);

        [OperationContract]
        List<AFMFulFillerInformation> GetAllFulfillerInformation();

    }
}
