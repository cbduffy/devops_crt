﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AFM.Commerce.UnitTests.ServiceReference1 {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SynchronyRequest", Namespace="http://schemas.datacontract.org/2004/07/SynchronyService")]
    [System.SerializableAttribute()]
    public partial class SynchronyRequest : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool syncOptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string transactionidField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool syncOption {
            get {
                return this.syncOptionField;
            }
            set {
                if ((this.syncOptionField.Equals(value) != true)) {
                    this.syncOptionField = value;
                    this.RaisePropertyChanged("syncOption");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string transactionid {
            get {
                return this.transactionidField;
            }
            set {
                if ((object.ReferenceEquals(this.transactionidField, value) != true)) {
                    this.transactionidField = value;
                    this.RaisePropertyChanged("transactionid");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SynchronyOptions", Namespace="http://schemas.datacontract.org/2004/07/SynchronyService")]
    [System.SerializableAttribute()]
    public partial class SynchronyOptions : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CardTypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool DefaultOrderTotalField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal EstimatedPercentageField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal EstimatedTotalField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string FinancingOptionIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string LearnMoreField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal MinimumPurchaseAmountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PaymModeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PaymTermIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal PaymentFeeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PromoDesc1Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PromoDesc2Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PromoDesc3Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool RemoveCartDiscountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool RemoveDiscountPopUpField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool ShowLearnMoreField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool ShowOptionField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int SortorderField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private bool StatusField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private decimal SubTotalField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ThirdPartyTermCodeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string CardType {
            get {
                return this.CardTypeField;
            }
            set {
                if ((object.ReferenceEquals(this.CardTypeField, value) != true)) {
                    this.CardTypeField = value;
                    this.RaisePropertyChanged("CardType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool DefaultOrderTotal {
            get {
                return this.DefaultOrderTotalField;
            }
            set {
                if ((this.DefaultOrderTotalField.Equals(value) != true)) {
                    this.DefaultOrderTotalField = value;
                    this.RaisePropertyChanged("DefaultOrderTotal");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal EstimatedPercentage {
            get {
                return this.EstimatedPercentageField;
            }
            set {
                if ((this.EstimatedPercentageField.Equals(value) != true)) {
                    this.EstimatedPercentageField = value;
                    this.RaisePropertyChanged("EstimatedPercentage");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal EstimatedTotal {
            get {
                return this.EstimatedTotalField;
            }
            set {
                if ((this.EstimatedTotalField.Equals(value) != true)) {
                    this.EstimatedTotalField = value;
                    this.RaisePropertyChanged("EstimatedTotal");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FinancingOptionId {
            get {
                return this.FinancingOptionIdField;
            }
            set {
                if ((object.ReferenceEquals(this.FinancingOptionIdField, value) != true)) {
                    this.FinancingOptionIdField = value;
                    this.RaisePropertyChanged("FinancingOptionId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string LearnMore {
            get {
                return this.LearnMoreField;
            }
            set {
                if ((object.ReferenceEquals(this.LearnMoreField, value) != true)) {
                    this.LearnMoreField = value;
                    this.RaisePropertyChanged("LearnMore");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal MinimumPurchaseAmount {
            get {
                return this.MinimumPurchaseAmountField;
            }
            set {
                if ((this.MinimumPurchaseAmountField.Equals(value) != true)) {
                    this.MinimumPurchaseAmountField = value;
                    this.RaisePropertyChanged("MinimumPurchaseAmount");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PaymMode {
            get {
                return this.PaymModeField;
            }
            set {
                if ((object.ReferenceEquals(this.PaymModeField, value) != true)) {
                    this.PaymModeField = value;
                    this.RaisePropertyChanged("PaymMode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PaymTermId {
            get {
                return this.PaymTermIdField;
            }
            set {
                if ((object.ReferenceEquals(this.PaymTermIdField, value) != true)) {
                    this.PaymTermIdField = value;
                    this.RaisePropertyChanged("PaymTermId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal PaymentFee {
            get {
                return this.PaymentFeeField;
            }
            set {
                if ((this.PaymentFeeField.Equals(value) != true)) {
                    this.PaymentFeeField = value;
                    this.RaisePropertyChanged("PaymentFee");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PromoDesc1 {
            get {
                return this.PromoDesc1Field;
            }
            set {
                if ((object.ReferenceEquals(this.PromoDesc1Field, value) != true)) {
                    this.PromoDesc1Field = value;
                    this.RaisePropertyChanged("PromoDesc1");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PromoDesc2 {
            get {
                return this.PromoDesc2Field;
            }
            set {
                if ((object.ReferenceEquals(this.PromoDesc2Field, value) != true)) {
                    this.PromoDesc2Field = value;
                    this.RaisePropertyChanged("PromoDesc2");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PromoDesc3 {
            get {
                return this.PromoDesc3Field;
            }
            set {
                if ((object.ReferenceEquals(this.PromoDesc3Field, value) != true)) {
                    this.PromoDesc3Field = value;
                    this.RaisePropertyChanged("PromoDesc3");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool RemoveCartDiscount {
            get {
                return this.RemoveCartDiscountField;
            }
            set {
                if ((this.RemoveCartDiscountField.Equals(value) != true)) {
                    this.RemoveCartDiscountField = value;
                    this.RaisePropertyChanged("RemoveCartDiscount");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool RemoveDiscountPopUp {
            get {
                return this.RemoveDiscountPopUpField;
            }
            set {
                if ((this.RemoveDiscountPopUpField.Equals(value) != true)) {
                    this.RemoveDiscountPopUpField = value;
                    this.RaisePropertyChanged("RemoveDiscountPopUp");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ShowLearnMore {
            get {
                return this.ShowLearnMoreField;
            }
            set {
                if ((this.ShowLearnMoreField.Equals(value) != true)) {
                    this.ShowLearnMoreField = value;
                    this.RaisePropertyChanged("ShowLearnMore");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool ShowOption {
            get {
                return this.ShowOptionField;
            }
            set {
                if ((this.ShowOptionField.Equals(value) != true)) {
                    this.ShowOptionField = value;
                    this.RaisePropertyChanged("ShowOption");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Sortorder {
            get {
                return this.SortorderField;
            }
            set {
                if ((this.SortorderField.Equals(value) != true)) {
                    this.SortorderField = value;
                    this.RaisePropertyChanged("Sortorder");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Status {
            get {
                return this.StatusField;
            }
            set {
                if ((this.StatusField.Equals(value) != true)) {
                    this.StatusField = value;
                    this.RaisePropertyChanged("Status");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal SubTotal {
            get {
                return this.SubTotalField;
            }
            set {
                if ((this.SubTotalField.Equals(value) != true)) {
                    this.SubTotalField = value;
                    this.RaisePropertyChanged("SubTotal");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ThirdPartyTermCode {
            get {
                return this.ThirdPartyTermCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.ThirdPartyTermCodeField, value) != true)) {
                    this.ThirdPartyTermCodeField = value;
                    this.RaisePropertyChanged("ThirdPartyTermCode");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.SynchronyServiceInterface")]
    public interface SynchronyServiceInterface {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SynchronyServiceInterface/GetSynchronyOptionsJson", ReplyAction="http://tempuri.org/SynchronyServiceInterface/GetSynchronyOptionsJsonResponse")]
        AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[] GetSynchronyOptionsJson(AFM.Commerce.UnitTests.ServiceReference1.SynchronyRequest req);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SynchronyServiceInterface/GetSynchronyOptionsJson", ReplyAction="http://tempuri.org/SynchronyServiceInterface/GetSynchronyOptionsJsonResponse")]
        System.Threading.Tasks.Task<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[]> GetSynchronyOptionsJsonAsync(AFM.Commerce.UnitTests.ServiceReference1.SynchronyRequest req);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SynchronyServiceInterface/GetSynchronyOptionsTestJson", ReplyAction="http://tempuri.org/SynchronyServiceInterface/GetSynchronyOptionsTestJsonResponse")]
        AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[] GetSynchronyOptionsTestJson();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SynchronyServiceInterface/GetSynchronyOptionsTestJson", ReplyAction="http://tempuri.org/SynchronyServiceInterface/GetSynchronyOptionsTestJsonResponse")]
        System.Threading.Tasks.Task<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[]> GetSynchronyOptionsTestJsonAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SynchronyServiceInterface/GetResults", ReplyAction="http://tempuri.org/SynchronyServiceInterface/GetResultsResponse")]
        AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[] GetResults(string transactionId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/SynchronyServiceInterface/GetResults", ReplyAction="http://tempuri.org/SynchronyServiceInterface/GetResultsResponse")]
        System.Threading.Tasks.Task<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[]> GetResultsAsync(string transactionId);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface SynchronyServiceInterfaceChannel : AFM.Commerce.UnitTests.ServiceReference1.SynchronyServiceInterface, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SynchronyServiceInterfaceClient : System.ServiceModel.ClientBase<AFM.Commerce.UnitTests.ServiceReference1.SynchronyServiceInterface>, AFM.Commerce.UnitTests.ServiceReference1.SynchronyServiceInterface {
        
        public SynchronyServiceInterfaceClient() {
        }
        
        public SynchronyServiceInterfaceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SynchronyServiceInterfaceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SynchronyServiceInterfaceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SynchronyServiceInterfaceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[] GetSynchronyOptionsJson(AFM.Commerce.UnitTests.ServiceReference1.SynchronyRequest req) {
            return base.Channel.GetSynchronyOptionsJson(req);
        }
        
        public System.Threading.Tasks.Task<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[]> GetSynchronyOptionsJsonAsync(AFM.Commerce.UnitTests.ServiceReference1.SynchronyRequest req) {
            return base.Channel.GetSynchronyOptionsJsonAsync(req);
        }
        
        public AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[] GetSynchronyOptionsTestJson() {
            return base.Channel.GetSynchronyOptionsTestJson();
        }
        
        public System.Threading.Tasks.Task<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[]> GetSynchronyOptionsTestJsonAsync() {
            return base.Channel.GetSynchronyOptionsTestJsonAsync();
        }
        
        public AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[] GetResults(string transactionId) {
            return base.Channel.GetResults(transactionId);
        }
        
        public System.Threading.Tasks.Task<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions[]> GetResultsAsync(string transactionId) {
            return base.Channel.GetResultsAsync(transactionId);
        }
    }
}
