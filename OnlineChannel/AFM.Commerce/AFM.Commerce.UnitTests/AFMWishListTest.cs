﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AFM.Commerce.Framework.Extensions.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;

namespace AFM.Commerce.UnitTests
{
    [TestClass]
    public class AFMWishListTest
    {
        private string stringWishListId = "5637152076";
        private String MainCustomreId = "0000000835";
        [TestMethod]
        public void CreateWishList()
        {
            //Arrange
            var CustomreId = "0000000835";
            var wishList = new WishList() { CustomerId = CustomreId, Name = "TestWishList", IsFavorite = false };

            //Act
            wishList = new WishListController().CreateWishList(wishList, CustomreId);
            //Assert
        }

        [TestMethod]
        public void AdditemToWishList()
        {
            //Arrange

            //Act
            var wishListId = new WishListController().AddItemsToWishList(this.stringWishListId, MainCustomreId, new List<Listing> {(new Listing
                {
                        ListingId = 5637145377,
                        IsKitVariant = false,
                        GiftCardAmount = 0,
                        Quantity = 1,
                        ProductDetails = "TestChair"
                    }) });
            //Assert

        }

        [TestMethod]
        public void getWishList()
        {
            var searchEngine = new SearchEngine();
            var WishList = new AFMWishListController().GetWishListsForCustomer(this.MainCustomreId, searchEngine);
        }
    }
}
