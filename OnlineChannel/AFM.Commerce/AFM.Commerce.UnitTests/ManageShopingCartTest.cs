﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AFM.Commerce.Framework.Extensions.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Services;

namespace AFM.Commerce.UnitTests
{
    /// <summary>
    /// Createing unit test method for Manage Shoping Cart Funcation 
    /// </summary>
    [TestClass]
    public class ManageShopingCartTest
    {
        AFMShoppingCartController objAFMShoppingCartController = new AFMShoppingCartController();
        ShoppingCartController objShoppingCartController = new ShoppingCartController();

        [TestInitialize]
        public void InitTest()
        {
            Utils.TestHelper.CustomerId = "0000001003";// Need to change as per environment
            //Utils.TestHelper.PromotionCode = "RTRN15";
            Utils.TestHelper.PromotionCode = "DISC05";// Need to change as per environment
            Utils.TestHelper.CreateCheckOutDataXmlFile();
            Utils.TestHelper.CreateCurrentHttpContext();
            Utils.TestHelper.AddZipCodetoCookie("10104"); // Need to change as per environment
            Utils.TestHelper.CreateShoppingCart();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            Utils.TestHelper.SearchEngine = new Utils.AFMSearchEngine();

        }

        [TestCleanup]
        public void TestCleanUp()
        {
            Utils.TestHelper.RemoveCheckOutDataxmlFile();
            Utils.TestHelper.RemoveShoppingCart();
        }

        #region Add Item Method
        [TestMethod]
        public void AddItemShopingCartwihtoutCustomerIdWithtowItemExpectingTwoItem()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, string.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal, Utils.TestHelper.ProductValidator, null, false);

            //Assert                                   
            Assert.IsTrue((objShpoingCart.Items.Count == itemListing.Count), "Numberof Item mismatch");
            var expectedShoppingCart = Utils.TestHelper.CreateShoppingCartFromXml();
            foreach (var cartLine in objShpoingCart.Items)
            {
                var cartLineExpected = expectedShoppingCart.Items.FirstOrDefault(cartitem => cartitem.ItemId == cartLine.ItemId);
                if (cartLineExpected != null)
                {
                    if (cartLine.AFMCartLineATP != null)
                    {
                        Assert.IsTrue(cartLine.AFMCartLineATP.IsAvailable == cartLineExpected.AFMCartLineATP.IsAvailable, "AFMCartLineATP.IsAvailable not matched");
                        Assert.IsTrue(cartLine.AFMCartLineATP.Message == cartLineExpected.AFMCartLineATP.Message, "AFMCartLineATP.Message not matched");
                        Assert.IsTrue(cartLine.AFMCartLineATP.ShippingSpan == cartLineExpected.AFMCartLineATP.ShippingSpan, "AFMCartLineATP.Message not matched");
                    }
                    Assert.IsTrue(cartLine.AFMShippingCost == cartLineExpected.AFMShippingCost, "AFMShippingCost not matched");
                    Assert.IsTrue(cartLine.AFMShippingMode == cartLineExpected.AFMShippingMode, "AFMShippingMode not matched");
                    Assert.IsTrue(cartLine.AFMVendorName == cartLineExpected.AFMVendorName, "AFMVendorName not matched");
                    Assert.IsTrue(cartLine.IsEcommOwned == cartLineExpected.IsEcommOwned, "IsEcommOwned not matched");
                    Assert.IsTrue(cartLine.AFMVendorId == cartLineExpected.AFMVendorId, "AFMVendorId not matched");
                }
            }
            //Clean 
            Utils.TestHelper.RemoveShoppingCart(objShpoingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void AddItemShopingCartWihtCustomerIdExpectingCustomerDetail()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            //Act
            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert
            // TODO: Add validation with test result and exptected Result for AddItemShopingCart                        
            Assert.IsTrue((objShpoingCart.ShippingAddress != null), "Customer Shipping detail is expected");
            Utils.TestHelper.RemoveShoppingCart(objShpoingCart.CartId, Utils.TestHelper.CustomerId);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddShoppingCartWithITemListingNullExpectionNullReferenceExpection()
        {
            //Arrange            
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            Utils.TestHelper.CustomerId = "1003";

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, Utils.TestHelper.CustomerId, null,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert            
            //Clean 
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void AddItemShopingCartNullProductValidatorExpectingNullReferacneException()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, string.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                null, null, false);

            //Assert   
            //Clean 
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void AddItemShopingCartNullCustomerIdNoExceptionExpected()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, null, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert            
            //Clean 
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }


        [TestMethod]
        //[ExpectedException(typeof(DataValidationException))]
        [ExpectedException(typeof(NullReferenceException))]
        // This test case has been modified to check for invalid customer Id
        // Since the customer ID is invalid, due to inavailabilitty of customer NullRefernce exception 
        // will be thrown
        public void AddItemShopingCartInvalidCustomerIdNullArgumentException()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            var CustId50Chars = Utils.TestHelper.CustomerId.PadLeft(250, '0');

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, CustId50Chars, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert         
            /*
             * It should returnn invalid customer with DataValidation Exception it will retrun 
             * Null Referance Exception
             */
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void AddItemShopingCartNullShopingCartIdExceptionExpected()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            //Utils.TestHelper.ProductValidator = null;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(null, String.Empty, itemListing,
                  Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                  Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert       

            //Clean 
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void AddItemShopingCartNullSearchEngineNoExceptionExpected()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert          

            //Clean 
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        //[ExpectedException(typeof(DataValidationException))]
        public void AddItemShopingCartInvalidShoppingCartIdExcepationExcepted()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            var shoppingCartId = "12345678998745632112365215421542154214515421";

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(shoppingCartId, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert   

            //Clean
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void AddItemShopingCartAddItemExistingShoppingCartLineShouldbeAddedSameCart()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            var shoppinCartOld = Utils.TestHelper.ShoppingCart;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(Utils.TestHelper.ShoppingCartId, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert            
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            Assert.IsTrue(shoppingCart.CartId == Utils.TestHelper.ShoppingCartId, "Item Shoul be added to same shoppingCart ID");
            //Assert.IsTrue(shoppingCart.Items.Count == (Utils.TestHelper.ShoppingCart.Items.Count * 2), "Item should not merger if same Item Added to Cart");
            foreach (var tempItem in shoppinCartOld.Items)
            {
                var tmpExpItem = shoppingCart.Items.Where(tmp => tmp.ProductId == tempItem.ProductId);
                if (tmpExpItem != null)
                {
                    Assert.IsTrue(tmpExpItem.Count() > 0, "Item No Added to Cart");
                }
                else
                {
                    Assert.IsTrue(false, "Item " + tempItem + " not in exp Result");
                }
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void AddItemShopingCartShoppingCartDataLevelMinmalExpectKitLineShouldNotHaveKitComp()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            itemListing.Insert(0,(new Listing
            {
                ItemId = "APK-W271-CFP",
                ListingId = 5637150502,
                IsKitVariant = true,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Corner TV Stand with Fireplace"
            }));

            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert            
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == 5637150406);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.KitComponents == null, "Kit Detail should not added");
            }
            else
            {
                Assert.IsTrue(false, "Kit Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void AddItemShopingCartShoppingCartDataLevelAllExpectKitItemShouldHaveKitComp()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            itemListing.Insert(0,(new Listing
            {
                ItemId = "APK-W271-CFP",
                ListingId = 5637150502,
                IsKitVariant = true,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Corner TV Stand with Fireplace"
            }));
            
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act 
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert            
            //Kit Product Shoul be Add in Cart
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == 5637150406);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.KitComponents == null, "Kit Detail is not added");
            }
            else
            {
                Assert.IsTrue(false, "Kit Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        //[ExpectedException(typeof(NullReferenceException))]
        // No longer valid
        public void AddItemShopingCartShoppingCartDataLevelAllKitItemSearchEngineNullExcpctedNullReferance()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            itemListing.Insert(0,(new Listing
            {
                ItemId = "APK-W271-CFP",
                ListingId = 5637150502,
                IsKitVariant = true,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Corner TV Stand with Fireplace"
            }));

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert            
            //Kit Product Shoul be Add in Cart
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == 5637150406);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.KitComponents == null, "Kit Detail is not added");
            }
            else
            {
                Assert.IsTrue(false, "Kit Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void AddItemstoCustomerCart()
        {
            var itemListing = Utils.TestHelper.ItemListing();
            itemListing.Insert(0,(new Listing
            {
                ItemId = "APK-W271-CFP",
                ListingId = 5637150502,
                IsKitVariant = true,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Corner TV Stand with Fireplace"
            }));

            //var shoppinCartResponse = new ShoppingCartServiceBase().AddItems(false, itemListing, ShoppingCartDataLevel.Minimal);

        }

        #endregion

        #region SetCartAffiliationLoyaltyTiers
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetCartAffiliationLoyaltyTiersNullCartIdNullReferanceExpected()
        {
            //Arrange
            var affilList = Utils.TestHelper.GetLoyaltyAffilcationTierIds();

            //Act
            objShoppingCartController.SetCartAffiliationLoyaltyTiers(null, affilList.AsEnumerable());

            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetCartAffiliationLoyaltyTiersNullAffilListNullReferanceExpected()
        {
            //Arrange                       

            //Act
            objShoppingCartController.SetCartAffiliationLoyaltyTiers(Utils.TestHelper.ShoppingCart.CartId, null);

            //Assert            
        }

        [TestMethod]
        public void SetCartAffiliationLoyaltyTiersValidInputs()
        {
            //Arrange                        
            var affiList = Utils.TestHelper.GetLoyaltyAffilcationTierIds();

            //Act
            objShoppingCartController.SetCartAffiliationLoyaltyTiers(Utils.TestHelper.ShoppingCart.CartId, affiList.AsEnumerable());

            //Assert
            Microsoft.Dynamics.Commerce.Runtime.DataModel.Cart objCart = Utils.TestHelper.GetCartFromCartId(Utils.TestHelper.ShoppingCart.CartId);
            Assert.IsTrue(objCart.AffiliationLines.Count != 0, "No AffiliationLoyaltyTier set on Cart");
            foreach (var objaff in affiList)
            {
                if (objCart.AffiliationLines.FirstOrDefault(aff => aff.AffiliationId == objaff) == null)
                {
                    Assert.IsTrue(false, "AffiliationLoyaltyTier " + objaff + " not set in cart");
                }
            }
        }
        #endregion

        #region GetCartAffiliationLoyaltyTiers
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetCartAffiliationLoyaltyTiersNullCartIdNullReferenceExpected()
        {
            //Arrange

            //Act
            objShoppingCartController.GetCartAffiliationLoyaltyTiers(null);

            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetCartAffiliationLoyaltyTiersEmptyStringCartIdNullReferenceExpected()
        {
            //Arrange

            //Act
            objShoppingCartController.GetCartAffiliationLoyaltyTiers(string.Empty);

            //Assert

        }

        [TestMethod]
        public void GetCartAffiliationLoyaltyTiersCreateCartIdExpectedLoyaltyTiers()
        {
            //Arrange            
            var affiList = Utils.TestHelper.GetLoyaltyAffilcationTierIds();
            objShoppingCartController.SetCartAffiliationLoyaltyTiers(Utils.TestHelper.ShoppingCart.CartId, affiList.AsEnumerable());

            //Act
            var affiliationList = objShoppingCartController.GetCartAffiliationLoyaltyTiers(Utils.TestHelper.ShoppingCartId);

            //Assert
            Assert.IsTrue(affiliationList != null, "Null should not Affiliation");
            var enumerable = affiliationList as IList<long> ?? affiliationList.ToList();
            Assert.IsTrue(enumerable.Count() != 0, "Affiliation count should not be Zero");
            foreach (var aff in enumerable)
            {
                Assert.IsTrue(affiList.Contains(aff), "Affiliaction id " + aff + " not in expeced result");
            }
        }

        #endregion

        #region UpdateItems
        [TestMethod]
        public void UpdateItemsShopingCartValidInputsExpectedValidOutput()
        {
            //Arrange
            var objShoppingCart = Utils.TestHelper.CreateShoppingCartHavingKitItem();
            var tmpCartTime = objShoppingCart.Items[0];
            tmpCartTime.Quantity = 10;
            var itemListing = new List<TransactionItem> { tmpCartTime };
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            //Act
            var objShpoingCart = objAFMShoppingCartController.UpdateItems(objShoppingCart.CartId,
                Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert
            foreach (var shoppingCartItem in itemListing)
            {
                var tempItem = objShpoingCart.Items.FirstOrDefault(tempitem => tempitem.LineId == shoppingCartItem.LineId);
                if (tempItem != null)
                {
                    Assert.IsTrue(shoppingCartItem.Quantity == tempItem.Quantity, "Line Qutinty Mis matched for " + tempItem.Description);
                }
                else
                {
                    Assert.IsTrue(false, shoppingCartItem.Description + " Not found in Cart");
                }
            }
        }

        [TestMethod]
        //[ExpectedException(typeof(NullReferenceException))]
        // No longer valid
        public void UpdateItemsShopingCartKitItemSearchEngineShoppingCartDataLevelAllNullException()
        {
            //Arrange
            var objShoppingCart = Utils.TestHelper.CreateShoppingCartHavingKitItem();
            var tmpCartTime = objShoppingCart.Items[0];
            tmpCartTime.Quantity = 10;
            var itemListing = new List<TransactionItem> { tmpCartTime };
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            //Act
            var objShpoingCart = objAFMShoppingCartController.UpdateItems(objShoppingCart.CartId,
                Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert
        }

        [TestMethod]
        public void UpdateItemsShopingCartKitItemSearchEngineShoppingCartDataLevelMinimalNoException()
        {
            //Arrange
            var objShoppingCart = Utils.TestHelper.CreateShoppingCartHavingKitItem();
            var tmpCartTime = objShoppingCart.Items[0];
            tmpCartTime.Quantity = 10;
            var itemListing = new List<TransactionItem> { tmpCartTime };
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            //Act
            var objShpoingCart = objAFMShoppingCartController.UpdateItems(objShoppingCart.CartId,
                Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void UpdateItemsInvalidShopingCartidExpectedNullReferenceException()
        {
            //Arrange
            var tmpCartTime = Utils.TestHelper.ShoppingCart.Items[0];
            tmpCartTime.Quantity = 10;
            new List<TransactionItem> { tmpCartTime };
            var itemListing = Utils.TestHelper.GetShopingCartItems();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            //Act
            var objShpoingCart = objAFMShoppingCartController.UpdateItems("sdfasdfkjfghertkjhdkf", Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert
            foreach (var shoppingCartItem in Utils.TestHelper.GetShopingCartItems())
            {
                var tempItem = objShpoingCart.Items.FirstOrDefault(tempitem => tempitem.LineId == shoppingCartItem.LineId);
                if (tempItem != null)
                {
                    Assert.IsTrue(shoppingCartItem.Quantity == tempItem.Quantity, "Line Qutinty Mis matched for " + tempItem.Description);
                }
                else
                {
                    Assert.IsTrue(false, shoppingCartItem.Description + " Not found in Cart");
                }
            }
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UpdateItemsCartWithITemListingNullExpectionNullReferenceExpection()
        {
            //Arrange            
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            Utils.TestHelper.CustomerId = "1003";

            //Act
            objAFMShoppingCartController.UpdateItems(Utils.TestHelper.ShoppingCart.CartId, Utils.TestHelper.CustomerId, null,
                 Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                 Utils.TestHelper.ProductValidator, null, false);

            //Assert            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UpdateItemsShopingCartNullProductValidatorExpectingNullReferacneException()
        {
            //Arrange
            var itemListing = Utils.TestHelper.GetShopingCartItems();
            Utils.TestHelper.ProductValidator = null;

            //Act
            objAFMShoppingCartController.UpdateItems(string.Empty, string.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert            
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void UpdateItemsShopingCartNullCustomerIdNullReferenceException()
        {
            //Arrange
            var itemListing = Utils.TestHelper.GetShopingCartItems();

            //Act
            objAFMShoppingCartController.UpdateItems(Utils.TestHelper.ShoppingCart.CartId, null, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void UpdateItemsShopingCartNullShopingCartIdNullArgumentExceptionExpected()
        {
            //Arrange
            var itemListing = Utils.TestHelper.GetShopingCartItems();
            Utils.TestHelper.ProductValidator = null;

            //Act
            objAFMShoppingCartController.UpdateItems(null, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert            
        }

        [TestMethod]
        public void UpdateItemsShopingCartNullSearchEngineNoExceptionExpected()
        {
            //Arrange
            var itemListing = Utils.TestHelper.GetShopingCartItems();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            objAFMShoppingCartController.UpdateItems(Utils.TestHelper.ShoppingCart.CartId, Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert            
        }

        #endregion

        #region RemoveItems
        [TestMethod]
        public void RemoveItemsShopingCartValidInputsExpectedValidOutput()
        {
            //Arrange
            var itemListing = Utils.TestHelper.GetShopingCartItemLineIds();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            //Act
            var objShpoingCart = objAFMShoppingCartController.RemoveItems(Utils.TestHelper.ShoppingCartId, Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal, Utils.TestHelper.ProductValidator, null, false);

            //Assert
            foreach (var shoppingCartItem in Utils.TestHelper.GetShopingCartItemLineIds())
            {
                var tempItem = objShpoingCart.Items.FirstOrDefault(tempitem => tempitem.LineId == shoppingCartItem);
                if (tempItem != null)
                {
                    Assert.IsTrue(false, "Line Qutinty Mis matched for " + tempItem.Description);
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void RemoveItemsShopingCartInValidShoppingCartNullReferenceException()
        {
            //Arrange
            var itemListing = Utils.TestHelper.GetShopingCartItemLineIds();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            //Act
            var objShpoingCart = objAFMShoppingCartController.RemoveItems("245asdfas5df42asdf5", Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal, Utils.TestHelper.ProductValidator, null, false);

            //Assert            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RemoveItemsCartWithITemListingNullExpectionNullReferenceExpection()
        {
            //Arrange            
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            Utils.TestHelper.CustomerId = "1003";

            //Act
            objAFMShoppingCartController.RemoveItems(Utils.TestHelper.ShoppingCartId, Utils.TestHelper.CustomerId, null,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert            
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void RemoveItemsShopingCartNullProductValidatorExpectingNullReferacneException()
        {
            //Arrange
            var itemListing = Utils.TestHelper.GetShopingCartItemLineIds();

            //Act
            objAFMShoppingCartController.RemoveItems(Utils.TestHelper.ShoppingCartId, Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                null, Utils.TestHelper.SearchEngine, false);

            //Assert            
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void RemoveItemsShopingCartNullCustomerIdNullReferenceException()
        {
            //Arrange
            var itemListing = Utils.TestHelper.GetShopingCartItemLineIds();

            //Act
            objAFMShoppingCartController.RemoveItems(Utils.TestHelper.ShoppingCartId, null, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert            
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RemoveItemsShopingCartNullShopingCartIdNullArgumentExceptionExpected()
        {
            //Arrange
            var itemListing = Utils.TestHelper.GetShopingCartItemLineIds();
            Utils.TestHelper.ProductValidator = null;

            //Act
            objAFMShoppingCartController.RemoveItems(null, Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert            
        }

        [TestMethod]
        public void RemoveItemsShopingCartNullSearchEngineNoExceptionExpected()
        {
            //Arrange
            var itemListing = Utils.TestHelper.GetShopingCartItemLineIds();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            objAFMShoppingCartController.RemoveItems(Utils.TestHelper.ShoppingCart.CartId, Utils.TestHelper.CustomerId, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.Minimal,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert            
        }
        #endregion

        #region AddEstShippingCostToCart
        [TestMethod]
        public void AddEstShippingCostToValidInputs()
        {

            //Arrange
            var cart = Utils.TestHelper.GetCartFromCartId(Utils.TestHelper.ShoppingCartId);

            //Act
            //objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, cart,
            //    Utils.TestHelper.CustomerId, false, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, cart,
                Utils.TestHelper.CustomerId, false);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(CartValidationException))]
        public void AddEstShippingCostToInValidInvalidCustomerIdDataValidationException()
        {

            //Arrange
            var cart = Utils.TestHelper.GetCartFromCartId(Utils.TestHelper.ShoppingCartId);

            //Act
            //objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, cart,
            //    "asdfasdfasersdf", false, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, cart,
    "asdfasdfasersdf", false);
            //Assert
        }

        [TestMethod]
        //[ExpectedException(typeof(DataValidationException))]
        public void AddEstShippingCostToInValidInvalidShoppingCartIdsDataValidationException()
        {

            //Arrange
            var cart = Utils.TestHelper.GetCartFromCartId(Utils.TestHelper.ShoppingCartId);

            //Act
            //objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, cart,
            //    Utils.TestHelper.CustomerId, false, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, cart,
               Utils.TestHelper.CustomerId, false);

            //Assert
        }

        [TestMethod]
        //[ExpectedException(typeof(NullReferenceException))]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddEstShippingCostToCartNullShoppingCartExpectedNullReferanceException()
        {
            //Arrange
            var cart = Utils.TestHelper.GetCartFromCartId(Utils.TestHelper.ShoppingCartId);

            //Act
            //objAFMShoppingCartController.AddEstShippingCostToCart(null, cart,
            //    string.Empty, false, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            objAFMShoppingCartController.AddEstShippingCostToCart(null, cart,
                string.Empty, false);

            //Assert
        }

        [TestMethod]
        //[ExpectedException(typeof(NullReferenceException))]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddEstShippingCostToCartNullCartExpectedNullReferanceException()
        {
            //Arrange            

            //Act
            //objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, null,
            //    string.Empty, false, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, null,
                string.Empty, false);

            //Assert
        }

        [TestMethod]
        public void AddEstShippingCostToNullCustomerIsnoExpectionExpected()
        {

            //Arrange
            var cart = Utils.TestHelper.GetCartFromCartId(Utils.TestHelper.ShoppingCartId);

            //Act
            //objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, cart,
            //    string.Empty, false, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, cart,
                string.Empty, false);

            //Assert
        }

        [TestMethod]
        public void AddEstShippingCostToNullShoppingCartIdExpectedNoException()
        {
            //Arrange
            var cart = Utils.TestHelper.GetCartFromCartId(Utils.TestHelper.ShoppingCartId);

            //Act
            //objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, cart,
            //    string.Empty, true, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            objAFMShoppingCartController.AddEstShippingCostToCart(Utils.TestHelper.ShoppingCart, cart,
                string.Empty, true);

            //Assert
        }

        #endregion

        #region GetShoppingCarts
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetShoppingCartsNullCustomerIdExpectedNullreferance()
        {
            //Arrange
            var productValidator = Utils.TestHelper.ProductValidator;


            //Act
            objAFMShoppingCartController.GetShoppingCarts(
                null, Utils.TestHelper.ShoppingCartDataLevel,
                productValidator, Utils.TestHelper.SearchEngine);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetShoppingCartsNullProductValidatorExpectedNullReferanceException()
        {
            //Arrange

            //Act
            objAFMShoppingCartController.GetShoppingCarts(
                                    Utils.TestHelper.CustomerId, Utils.TestHelper.ShoppingCartDataLevel,
                                    null, Utils.TestHelper.SearchEngine);
            //Assert
        }

        [TestMethod]
        public void GetShoppingCartsNullSearchEngineNoExceptionExpected()
        {
            //Arrange

            //Act
            objAFMShoppingCartController.GetShoppingCarts(
                                    Utils.TestHelper.CustomerId, Utils.TestHelper.ShoppingCartDataLevel,
                                    Utils.TestHelper.ProductValidator, null);
            //Assert
        }

        [TestMethod]
        //[ExpectedException(typeof(NullReferenceException))]
        // No longer valid
        public void GetShoppingCartsCustomerIdLength50xpectedNullreferance()
        {
            //Arrange
            var productValidator = Utils.TestHelper.ProductValidator;

            //Act
            objAFMShoppingCartController.GetShoppingCarts(
                Utils.TestHelper.CustomerId.PadLeft(50, '0'), Utils.TestHelper.ShoppingCartDataLevel,
                productValidator, Utils.TestHelper.SearchEngine);

            //Assert
        }


        [TestMethod]
        public void GetShoppingCartsAllparamater()
        {
            //Arrange

            //Act
            var objShoppingCarts = objAFMShoppingCartController.GetShoppingCarts(
                                    Utils.TestHelper.CustomerId, Utils.TestHelper.ShoppingCartDataLevel,
                                    Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            //Assert
            Assert.IsTrue(objShoppingCarts != null, "No Shoping Cart Return");
        }
        #endregion

        #region GetShoppingCart

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetShoppingCartNullshopingCartIdExpectedNullReferanceException()
        {
            //Arrange

            //Act
            var shoppcart = objAFMShoppingCartController.GetShoppingCart(null, Utils.TestHelper.CustomerId,
                Utils.TestHelper.ShoppingCartDataLevel,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert
        }

        [TestMethod]
        public void GetShoppingCartForCustomerIdExpectedNoExpection()
        {
            //Arrange

            //Act
            var shoppcart = objAFMShoppingCartController.GetShoppingCart(Utils.TestHelper.ShoppingCartId,
                Utils.TestHelper.CustomerId,
                Utils.TestHelper.ShoppingCartDataLevel,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert            
        }

        [TestMethod]
        [ExpectedException(typeof(DataValidationException))]
        public void GetShoppingCartInvalidcustomerIdExpectedNoExpection()
        {
            //Arrange

            //Act
            var shoppcart = objAFMShoppingCartController.GetShoppingCart("sfase0sdf14sdf54er0sd",
                Utils.TestHelper.CustomerId,
                Utils.TestHelper.ShoppingCartDataLevel,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert            
        }

        [TestMethod]
        [ExpectedException(typeof(DataValidationException))]
        public void GetShoppingCartInvalidShoppingCartIdExpectedNoExpection()
        {
            //Arrange

            //Act
            var shoppcart = objAFMShoppingCartController.GetShoppingCart(Utils.TestHelper.ShoppingCartId,
                "124sfd4sfdsdf45",
                Utils.TestHelper.ShoppingCartDataLevel,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //Assert            
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetShoppingCartNullProductvalidatorNullReferenceExpection()
        {
            //Arrange

            //Act
            var shoppcart = objAFMShoppingCartController.GetShoppingCart(Utils.TestHelper.ShoppingCartId,
                Utils.TestHelper.CustomerId,
                Utils.TestHelper.ShoppingCartDataLevel,
                null, Utils.TestHelper.SearchEngine, false);

            //Assert
            Assert.IsTrue(shoppcart.CartId == Utils.TestHelper.ShoppingCartId, "Shopping Cart Id not matched");
        }

        [TestMethod]
        public void GetShoppingCartvalidShoopingCartIdExpectedShoppingCart()
        {
            //Arrange

            //Act
            var shoppcart = objAFMShoppingCartController.GetShoppingCart(Utils.TestHelper.ShoppingCartId,
                Utils.TestHelper.CustomerId,
                Utils.TestHelper.ShoppingCartDataLevel,
                Utils.TestHelper.ProductValidator, null, false);

            //Assert
            Assert.IsTrue(shoppcart.CartId == Utils.TestHelper.ShoppingCartId, "Shopping Cart Id not matched");

        }

        #endregion

        #region GetAllPromotionsForShoppingCart
        [TestMethod]
        public void GetAllPromotionsForShoppingCart()
        {
            //Arrange

            //Act
            var objShoppingCart = objAFMShoppingCartController.GetAllPromotionsForShoppingCart(
                Utils.TestHelper.ShoppingCartId, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, true);

            //Assert 
            Assert.IsNotNull(objShoppingCart, "Shopping Cart Should not be null");
            foreach (var tempItem in objShoppingCart.Items)
            {
                if (tempItem.DiscountAmount != null && tempItem.DiscountAmount > 0)
                {
                    Assert.IsTrue(tempItem.PromotionLines.Count == 0, "After Apply Discount Cart Item Must Have Promoline Should");
                }
            }
        }

        [TestMethod]
        //[ExpectedException(typeof(NullReferenceException))]
        // No longer valid
        public void GetAllPromotionsForShoppingCartKitItemDataLevelAllNullSearchEngineNullReferanceExcepted()
        {
            //Arrange
            var objShoppingCartKit = Utils.TestHelper.CreateShoppingCartHavingKitItem();

            //Act
            var objShoppingCart = objAFMShoppingCartController.GetAllPromotionsForShoppingCart(
                objShoppingCartKit.CartId, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator,
                null, true);

            //Assert             
        }

        [TestMethod]
        public void GetAllPromotionsForShoppingCartKitItemDataLevelMinimalNullSearchEngineNullReferanceExcepted()
        {
            //Arrange
            var objShoppingCartKit = Utils.TestHelper.CreateShoppingCartHavingKitItem();

            //Act
            var objShoppingCart = objAFMShoppingCartController.GetAllPromotionsForShoppingCart(
                objShoppingCartKit.CartId, ShoppingCartDataLevel.Minimal, Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, true);

            //Assert             
            Assert.IsNotNull(objShoppingCart, "Shopping Cart Should not be null");
            foreach (var tempItem in objShoppingCart.Items)
            {
                if (tempItem.DiscountAmount != null && tempItem.DiscountAmount > 0)
                {
                    Assert.IsTrue(tempItem.PromotionLines.Count != 0, "After Apply Discount Cart Item Must Have Promoline Should");
                }
            }
        }

        [TestMethod]
        public void GetAllPromotionsForShoppingCartShoppingDataLeavelAll()
        {
            //Arrange
            var itemListing = Utils.TestHelper.ItemListing();
            itemListing.Insert(0,(new Listing
            {
                ItemId = "APK-W271-CFP",
                ListingId = 5637150502,
                IsKitVariant = true,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Corner TV Stand with Fireplace"
            }));

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);


            //Act
            var objShoppingCart = objAFMShoppingCartController.GetAllPromotionsForShoppingCart(shoppingCart.CartId,
                ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, true);

            //Assert 
            Assert.IsNotNull(objShoppingCart, "Shopping Cart Should not be null");
            foreach (var tempItem in objShoppingCart.Items)
            {
                if (tempItem.DiscountAmount != null && tempItem.DiscountAmount > 0)
                {
                    Assert.IsTrue(tempItem.PromotionLines.Count == 0, "After Apply Discount Cart Item Must Have Promoline Should");
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetAllPromotionsForShoppingCartNullShoppingCartIdNullReferanceExcepted()
        {
            //Arrange

            //Act
            var objShoppingCart = objAFMShoppingCartController.GetAllPromotionsForShoppingCart(null, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, true);

            //Assert 
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetAllPromotionsForShoppingCartNullproductValidatorNoExceptionExcepted()
        {
            //Arrange

            //Act
            var objShoppingCart = objAFMShoppingCartController.GetAllPromotionsForShoppingCart(Utils.TestHelper.ShoppingCartId, ShoppingCartDataLevel.All, null,
                Utils.TestHelper.SearchEngine, true);

            //Assert 
        }

        [TestMethod]
        public void GetAllPromotionsForShoppingCartNullSearchengineNoExceptionExcepted()
        {
            //Arrange

            //Act
            var objShoppingCart = objAFMShoppingCartController.GetAllPromotionsForShoppingCart(Utils.TestHelper.ShoppingCartId, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, true);

            //Assert 
        }

        [TestMethod]
        [ExpectedException(typeof(DataValidationException))]
        public void GetAllPromotionsForShoppingCartInvalidShoppingCartIdDataValidationException()
        {
            //Arrange

            //Act
            var objShoppingCart = objAFMShoppingCartController.GetAllPromotionsForShoppingCart("012451154sd1f25as4fd12", ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, true);

            //Assert 
        }
        #endregion

        #region CommenceCheckout
        [TestMethod]
        public void CommenceCheckout()
        {
            //Arrange

            //Act
            var shoppingCart = objAFMShoppingCartController.CommenceCheckout(Utils.TestHelper.ShoppingCartId,
                Utils.TestHelper.CustomerId, "", Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, ShoppingCartDataLevel.All);

            //Assert
            Assert.IsNotNull(shoppingCart, "Shopping Cart Should No be Null");
        }

        [TestMethod]
        //[ExpectedException(typeof(NullReferenceException))]
        // No longer valid
        public void CommenceCheckoutKitItemSearchEngineNullShoppingCartDataLevelAllNullException()
        {
            //Arrange            
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;
            var itemListing = Utils.TestHelper.ItemListing();
            itemListing.Insert(0,(new Listing
            {
                ItemId = "APK-W271-CFP",
                ListingId = 5637150502,
                IsKitVariant = true,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Corner TV Stand with Fireplace"
            }));


            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, Utils.TestHelper.CustomerId,
                itemListing, ShoppingCartDataLevel.All, productValidator, searchEngine, true);
            var shoppingCartIdToBeClaimed = objShpoingCart.CartId;

            //Act            
            var shoppingCart = objAFMShoppingCartController.CommenceCheckout(objShpoingCart.CartId,
               Utils.TestHelper.CustomerId, "", Utils.TestHelper.ProductValidator,
               null, ShoppingCartDataLevel.All);

            //Assert           
        }

        [TestMethod]
        //[ExpectedException(typeof(NullReferenceException))]
        // No longer valid 
        public void CommenceCheckoutKitItemSearchEngineNullShoppingCartDataLevelMinimalNoException()
        {
            //Arrange            
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;
            var itemListing = Utils.TestHelper.ItemListing();
            itemListing.Insert(0,(new Listing
            {
                ItemId = "APK-W271-CFP",
                ListingId = 5637150502,
                IsKitVariant = true,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Corner TV Stand with Fireplace"
            }));


            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, Utils.TestHelper.CustomerId,
                itemListing, ShoppingCartDataLevel.Minimal, productValidator, searchEngine, true);
            var shoppingCartIdToBeClaimed = objShpoingCart.CartId;

            //Act            
            var shoppingCart = objAFMShoppingCartController.CommenceCheckout(objShpoingCart.CartId,
               Utils.TestHelper.CustomerId, "", Utils.TestHelper.ProductValidator,
               null, ShoppingCartDataLevel.All);

            //Assert           
        }

        [TestMethod]
        public void CommenceCheckoutPreviousShoppingCartIdExceptedPreShoppingCartShouldbeDeleted()
        {
            //Arrange
            var shoppingCartPrevious = objAFMShoppingCartController.CommenceCheckout(Utils.TestHelper.ShoppingCartId,
                Utils.TestHelper.CustomerId, "", Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, ShoppingCartDataLevel.All);

            //Act
            var shoppingCart = objAFMShoppingCartController.CommenceCheckout(Utils.TestHelper.ShoppingCartId,
                Utils.TestHelper.CustomerId, shoppingCartPrevious.CartId, Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, ShoppingCartDataLevel.All);

            //Assert
            Assert.IsNotNull(shoppingCart, "Shopping Cart Should No be Null");
            Assert.IsNull(Utils.TestHelper.GetCartFromCartId(shoppingCartPrevious.CartId), "Previous Shopping Cart no Removed");
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void CommenceCheckoutInvalidShoppingCartIdExceptedNullException()
        {
            //Arrange

            //Act
            var shoppingCart = objAFMShoppingCartController.CommenceCheckout("0212012102asdfa5sd4fa1sdf21asdf0",
                Utils.TestHelper.CustomerId, "", Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, ShoppingCartDataLevel.All);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void CommenceCheckoutInvalidCustomerIDIdExceptedNullReferenceException()
        {
            //Arrange

            //Act
            var shoppingCart = objAFMShoppingCartController.CommenceCheckout(Utils.TestHelper.ShoppingCartId,
                "asdfasfaserdf", "", Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, ShoppingCartDataLevel.All);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CommenceCheckoutNullShoppingCartIdExceptedNullArgumentException()
        {
            //Arrange

            //Act
            var shoppingCart = objAFMShoppingCartController.CommenceCheckout(null,
                Utils.TestHelper.CustomerId, "", Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, ShoppingCartDataLevel.All);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void CommenceCheckoutNullCustomerIsIdExceptedNullReferenceException()
        {
            //Arrange

            //Act
            var shoppingCart = objAFMShoppingCartController.CommenceCheckout(Utils.TestHelper.ShoppingCartId,
               null, "", Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, ShoppingCartDataLevel.All);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void CommenceCheckoutNullProductValidationExceptedNullArgumentException()
        {
            //Arrange

            //Act
            var shoppingCart = objAFMShoppingCartController.CommenceCheckout(Utils.TestHelper.ShoppingCartId,
                Utils.TestHelper.CustomerId, "", null,
                Utils.TestHelper.SearchEngine, ShoppingCartDataLevel.All);

            //Assert
        }

        [TestMethod]
        public void CommenceCheckoutNullSearchEngineExceptedNoException()
        {
            //Arrange

            //Act
            var shoppingCart = objAFMShoppingCartController.CommenceCheckout(Utils.TestHelper.ShoppingCartId,
                Utils.TestHelper.CustomerId, "", Utils.TestHelper.ProductValidator,
                null, ShoppingCartDataLevel.All);

            //Assert
        }
        #endregion

        #region AddOrRemovePromotionCode
        [TestMethod]
        public void AddOrRemovePromotionCodeAddPromotionCode()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode(shoppingCartId, customerId, promotionCode, true, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
            Assert.IsNotNull(shoppingCart, "Shopping Cart Shoul Not be Null");
            Assert.IsNotNull(shoppingCart.DiscountCodes, "Discount Codes Should not be Null");
            Assert.IsNotNull(shoppingCart.DiscountCodes.FirstOrDefault(tmp => tmp == promotionCode), "Discount Code " + promotionCode + " is not added to Cart");
        }

        [TestMethod]
        public void AddOrRemovePromotionCodeRemovePromotionCode()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode(shoppingCartId, customerId, promotionCode, true, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Act
            shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode(shoppingCartId, customerId, promotionCode, false, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
            Assert.IsNotNull(shoppingCart, "Shopping Cart Shoul Not be Null");
            if (shoppingCart.DiscountCodes != null)
                Assert.IsNull(shoppingCart.DiscountCodes.FirstOrDefault(tmp => tmp == promotionCode), "Discount Code " + promotionCode + " is nor removed to Cart");

        }

        [TestMethod]
        //[ExpectedException(typeof(DataValidationException))]
        public void AddOrRemovePromotionCodeRemovePromotionCodeNotExistsInCart()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode(shoppingCartId, customerId, promotionCode, false, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
            Assert.IsNotNull(shoppingCart, "Shopping Cart Shoul Not be Null");
            if (shoppingCart.DiscountCodes != null)
                Assert.IsNull(shoppingCart.DiscountCodes.FirstOrDefault(tmp => tmp == promotionCode), "Discount Code " + promotionCode + " is nor removed to Cart");

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddOrRemovePromotionCodeNullShoppingCartIdNullReferanceException()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode(null, customerId, promotionCode, true, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void AddOrRemovePromotionCodeNullCustomerIdNullReferanceException()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode(shoppingCartId, null, promotionCode, true, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(CartValidationException))]
        public void AddOrRemovePromotionCodeInvalidShoppingCartIdNullReferanceException()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode("sfsdfaer4sdf2145er1sdfer", customerId, promotionCode, true, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void AddOrRemovePromotionCodeInvalidCustomerIdNullReferanceException()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode(shoppingCartId, "asdfaserewr21", promotionCode, true, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(CartValidationException))]
        public void AddOrRemovePromotionCodeInvalidCustomerIdInvalidShoppingCartIdNullReferanceException()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode("sdfasrewr1s2df2s1df54er21wer5", "sdf12s1df4esrsr5se", promotionCode, true, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddOrRemovePromotionCodeNullPromotionCodeNullReferanceException()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode(shoppingCartId, customerId, null, true, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
        }

        [TestMethod]
        public void AddOrRemovePromotionCodeNullProductValidatiorNoException()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode(shoppingCartId, customerId, promotionCode, true, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
        }

        [TestMethod]
        public void AddOrRemovePromotionCodeNullSearchEngineNoException()
        {
            //Arrange
            var shoppingCartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var promotionCode = Utils.TestHelper.PromotionCode; //ST100008
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act
            var shoppingCart = objAFMShoppingCartController.AddOrRemovePromotionCode(shoppingCartId, customerId, promotionCode, true, ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
        }
        //AddOrRemovePromotionCode(string shoppingCartId, string customerId, string promotionCode, bool isAdd, ShoppingCartDataLevel dataLevel, IProductValidator productValidator, ISearchEngine searchEngine)
        #endregion

        #region ClaimAnonymousCart
        [TestMethod]
        public void ClaimAnonymousCart()
        {
            //Arrange            
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;
            var itemListing = Utils.TestHelper.ItemListing();

            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
               ShoppingCartDataLevel.Minimal, productValidator, searchEngine, true);
            var shoppingCartIdToBeClaimed = objShpoingCart.CartId;

            //Act            
            var shoppingCart = objAFMShoppingCartController.ClaimAnonymousCart(shoppingCartIdToBeClaimed, customerId, ShoppingCartDataLevel.All, productValidator, searchEngine,false);

            //Assert
            Assert.IsNotNull(shoppingCart, "Shopping Cart Should not be null");
        }

  

        [TestMethod]
        public void ClaimAnonymousCartKitItemSearchEngineShoppingCartDataLevelMinimalNoException()
        {
            //Arrange            
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;
            var itemListing = Utils.TestHelper.ItemListing();
            itemListing.Add((new Listing
            {
                ListingId = 52565441658,
                IsKitVariant = true,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Camera kit"
            }));


            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
               ShoppingCartDataLevel.Minimal, productValidator, searchEngine, false);
            var shoppingCartIdToBeClaimed = objShpoingCart.CartId;

            //Act            
            var shoppingCart = objAFMShoppingCartController.ClaimAnonymousCart(shoppingCartIdToBeClaimed, customerId,
                ShoppingCartDataLevel.Minimal, productValidator, null,false);

            //Assert
            Assert.IsNotNull(shoppingCart, "Shopping Cart Should not be null");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ClaimAnonymousCartNullshoppingCartIdToBeClaimedNullExceptionExcepted()
        {
            //Arrange
            var shoppingCartIdToBeClaimed = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            //Act            
            var shoppingCart = objAFMShoppingCartController.ClaimAnonymousCart(null, customerId, ShoppingCartDataLevel.All, productValidator, searchEngine,false);
        }

        [TestMethod]
        public void ClaimAnonymousCartInvalidshoppingCartIdToBeClaimedNullShoppingCartExcepted()
        {
            //Arrange           
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            var itemListing = Utils.TestHelper.ItemListing();

            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
               ShoppingCartDataLevel.Minimal, productValidator, null, false);
            var shoppingCartIdToBeClaimed = objShpoingCart.CartId;


            //Act            
            var shoppingCart = objAFMShoppingCartController.ClaimAnonymousCart("sadfaserwe54rsdf54er2", customerId, ShoppingCartDataLevel.All, productValidator, searchEngine,false);

            //Assert
            Assert.IsNull(shoppingCart, "Shopping Cart Shoul be Null");
        }

        [TestMethod]
        public void ClaimAnonymousCartNullCustomerIdShoppingCartExcepted()
        {
            //Arrange
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;
            var itemListing = Utils.TestHelper.ItemListing();

            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
               ShoppingCartDataLevel.Minimal, productValidator, null, false);
            var shoppingCartIdToBeClaimed = objShpoingCart.CartId;

            //Act            
            var shoppingCart = objAFMShoppingCartController.ClaimAnonymousCart(shoppingCartIdToBeClaimed, null, ShoppingCartDataLevel.All, productValidator, searchEngine,false);

            //Assert
            Assert.IsNotNull(shoppingCart, "Shopping Cart Shoul be Null");
        }

        [TestMethod]
        [ExpectedException(typeof(CartValidationException))]
        public void ClaimAnonymousCartInvalidCustomerIdCartValidationExceptionExcepted()
        {
            //Arrange
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            var itemListing = Utils.TestHelper.ItemListing();

            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
               ShoppingCartDataLevel.Minimal, productValidator, null, false);
            var shoppingCartIdToBeClaimed = objShpoingCart.CartId;

            //Act            
            var shoppingCart = objAFMShoppingCartController.ClaimAnonymousCart(shoppingCartIdToBeClaimed, "sdfse4re1d4s1sew4", ShoppingCartDataLevel.All, productValidator, searchEngine);

            //Assert
        }

        [TestMethod]
        public void ClaimAnonymousCartInvalidshoppingCartIdToBeClaimedInvalidCustomerIdNullExceptionExcepted()
        {
            //Arrange            
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            var itemListing = Utils.TestHelper.ItemListing();

            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
               ShoppingCartDataLevel.Minimal, productValidator, null, false);
            var shoppingCartIdToBeClaimed = objShpoingCart.CartId;


            //Act            
            var shoppingCart = objAFMShoppingCartController.ClaimAnonymousCart("asdfsdresdfdskfkerjkjksdf", "asdfwser1", ShoppingCartDataLevel.All, productValidator, searchEngine,false);

            //Assert
            Assert.IsNull(shoppingCart, "Shopping Cart Shoul be Null");
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void ClaimAnonymousCartNullProductValidatorNullReferenceExceptionExcepted()
        {
            //Arrange

            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;

            var itemListing = Utils.TestHelper.ItemListing();

            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
               ShoppingCartDataLevel.Minimal, productValidator, null, false);
            var shoppingCartIdToBeClaimed = objShpoingCart.CartId;

            //Act            
            var shoppingCart = objAFMShoppingCartController.ClaimAnonymousCart(shoppingCartIdToBeClaimed, customerId, ShoppingCartDataLevel.All, null, searchEngine,false);

            //Assert
        }

        [TestMethod]
        public void ClaimAnonymousCartNullSearchEngineNoExceptionExcepted()
        {
            //Arrange            
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var searchEngine = Utils.TestHelper.SearchEngine;
            var itemListing = Utils.TestHelper.ItemListing();

            var objShpoingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
               ShoppingCartDataLevel.Minimal, productValidator, null, false);
            var shoppingCartIdToBeClaimed = objShpoingCart.CartId;

            //Act            
            var shoppingCart = objAFMShoppingCartController.ClaimAnonymousCart(shoppingCartIdToBeClaimed, customerId, ShoppingCartDataLevel.All, productValidator, null,false);

            //Assert
        }
        #endregion

        #region AFMSetShippingModeAndCostToItem
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AFMSetShippingModeAndCostToItem()
        {
            //Arrange

            //Act
            objAFMShoppingCartController.AFMSetShippingModeAndCostToItem(Utils.TestHelper.ShoppingCart.Items[0]);

            //Assert
            Assert.IsNotNull(Utils.TestHelper.ShoppingCart.Items[0].AFMShippingCost, "AFMShippingCost Should Not be Null");
            Assert.IsNotNull(Utils.TestHelper.ShoppingCart.Items[0].AFMShippingMode, "AFMShippingMode Should Not be Null");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AFMSetShippingModeAndCostToItemNullCartItemNullReferanceExcepted()
        {
            //Arrange

            //Act
            objAFMShoppingCartController.AFMSetShippingModeAndCostToItem(null);

            //Assert
        }
        #endregion

        #region
        public void MergeCartTest()
        {
            var customerId = "0000000525";
            var Controller = new AFMShoppingCartController();
            var shoppingCart = Controller.GetShoppingCart("3Bwmxs8HncAZXJM11CeBYB3I8w1Roosghjmd+LHe24c=", null, ShoppingCartDataLevel.Minimal, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            var shoppingCartId = shoppingCart.CartId;
            var ActiveCart = Controller.GetActiveShoppingCart("0000000525", ShoppingCartDataLevel.Minimal, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            var ActiveCartId = ActiveCart.CartId;

            var mergeCart = Controller.MoveItemsBetweenCarts(shoppingCartId, ActiveCartId, customerId, ShoppingCartDataLevel.Minimal, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);
            shoppingCart = Controller.GetShoppingCart(mergeCart.CartId, customerId, ShoppingCartDataLevel.Minimal, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);

            Assert.IsNotNull(shoppingCart.Items, "Success");
        }
        #endregion
    }
}
