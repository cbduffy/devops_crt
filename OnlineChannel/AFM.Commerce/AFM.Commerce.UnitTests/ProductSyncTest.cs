﻿using System;
using System.Linq;
using System.Data;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ECommerce.Publishing;

namespace AFM.Commerce.UnitTests
{
    [TestClass]
    public class ProductSyncTest
    {
        [TestMethod]
        public void AfmGetProductCustomEntitiesNoProductListNullValueExpected()
        {
            //Arrange            
            var objAfmSiteCoreCatalogPublisher = new AFMSiteCoreCatalogPublisher();

            //Act            
            var productDetail = objAfmSiteCoreCatalogPublisher.AFMGetProductCustomEntities(new List<long>());

            //Assert
            Assert.IsTrue(productDetail == null, "No Product Assign Null Value");

        }


        [TestMethod]
        public void AfmGetProductCustomEntitiesAftergerChangedProducts()
        {
            //Arrange
            var productDetailExpected = Utils.TestHelper.GetProductSyncProductDetail();
            var objAfmSiteCoreCatalogPublisher = new AFMSiteCoreCatalogPublisher();
            PrivateObject objPrivateobject = new PrivateObject(objAfmSiteCoreCatalogPublisher);
            var productIds = Utils.TestHelper.ProductsRecIds();
            //objPrivateobject.SetField("_productRecordIdsList", productIds);

            //Act            
            var productDetail = objAfmSiteCoreCatalogPublisher.AFMGetProductCustomEntities(productIds);

            //Assert
            Assert.IsTrue(productDetail != null, "Product Detail Shuld not be Null Object");
            Assert.IsTrue(productDetail.AlsoKnownAs.Count() == productDetailExpected.AlsoKnownAs.Count, "AlsoKnownAs Rows Not Matched");
            Assert.IsTrue(productDetail.ItemCovers.Count() == productDetailExpected.ItemCovers.Count, "ItemCovers Rows Not Matched");
            Assert.IsTrue(productDetail.ProductCountryMap.Count() == productDetailExpected.ProductCountryMap.Count, "ProductCountryMap Rows Not Matched");
            Assert.IsTrue(productDetail.UpCrossSell.Count() == productDetailExpected.UpCrossSell.Count, "UpCrossSell Rows Not Matched");
            Assert.IsTrue(productDetail.ECommOwnedData.Count() == productDetailExpected.ECommOwnedData.Count, "ECommOwnedData Rows Not Matched");

            foreach (var tempobj in productDetail.AlsoKnownAs)
            {
                var objExp = productDetailExpected.AlsoKnownAs.FirstOrDefault(alsoknownas => alsoknownas.ProductRecordId == tempobj.ProductRecordId);
                if (objExp == null)
                    Assert.IsTrue(false, tempobj.ProductRecordId + " Not found in Expected Result AlsoKnownAs");
            }

            foreach (var tempobj in productDetail.ItemCovers)
            {
                var objExp = productDetailExpected.ItemCovers.FirstOrDefault(alsoknownas => alsoknownas.ProductRecordId == tempobj.ProductRecordId);
                if (objExp == null)
                    Assert.IsTrue(false, tempobj.ProductRecordId + " Not found in Expected Result ItemCovers");
            }

            foreach (var tempobj in productDetail.ProductCountryMap)
            {
                var objExp = productDetailExpected.ProductCountryMap.FirstOrDefault(alsoknownas => alsoknownas.ProductRecordId == tempobj.ProductRecordId);
                if (objExp == null)
                    Assert.IsTrue(false, tempobj.ProductRecordId + " Not found in Expected Result ProductCountryMap");
            }

            foreach (var tempobj in productDetail.UpCrossSell)
            {
                var objExp = productDetailExpected.UpCrossSell.FirstOrDefault(alsoknownas => alsoknownas.ProductRecordId == tempobj.ProductRecordId);
                if (objExp == null)
                    Assert.IsTrue(false, tempobj.ProductRecordId + " Not found in Expected Result UpCrossSell");
            }
            foreach (var tempobj in productDetail.ECommOwnedData)
            {
                var objExp = productDetailExpected.ECommOwnedData.FirstOrDefault(alsoknownas => alsoknownas.ProductRecordId == tempobj.ProductRecordId);
                if (objExp == null)
                    Assert.IsTrue(false, tempobj.ProductRecordId + " Not found in Expected Result ECommOwnedData");
            }
        }
    }
}
