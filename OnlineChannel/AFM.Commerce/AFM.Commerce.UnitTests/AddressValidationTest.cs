﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AFM.Commerce.Framework.Extensions.Controllers;
using AFM.Commerce.Entities;
using Microsoft.Dynamics.Commerce.Runtime.Client;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Address = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Address;
using AddressType = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.AddressType;

namespace AFM.Commerce.UnitTests
{
    [TestClass]
    public class AddressValidationTest
    {
        AFMCheckoutController objCheckoutController = new AFMCheckoutController();

        Address objShippingAddress;
        Address objShippingAddressWithFirstNameEmpty;
        Address objShippingAddressWithLastNameEmpty;
        Address objShippingAddressWithInvalidAddressStreets;
        Address objShippingAddressWithPOBoxAddress;
        Address objShippingaddressWithPartialCorrectAddress;

        Address objBillingAddress;
        Address objBillingAddressWithFirstNameEmpty;
        Address objBillingAddressWithLastNameEmpty;
        Address objBillingAddressWithInvalidAddressStreets;
        Address objBillingAddressWithPOBoxAddress;
        Address objBillingAddressWithPartialCorrectAddress;

        [TestInitialize]
        public void InitTest()
        {
           ChannelManager channelManager = ChannelManager.Create(CrtUtilities.GetCommerceRuntime());
           channelManager.GetDefaultChannelId();
           Utils.TestHelper.Zipcode = "10104";
           Utils.TestHelper.CountryRegioncId = "USA";


           objShippingAddress = Utils.TestHelper.ShippingAddressInit("Kishore", "Kommi", "NEW YORK", "NY", "1290 AVENUE OF AMERICAS", "", "10104");
           objShippingAddressWithFirstNameEmpty = Utils.TestHelper.ShippingAddressInit("", "Kommi", "NEW YORK", "NY", "1290 AVENUE OF AMERICAS", "", "10104");
           objShippingAddressWithLastNameEmpty = Utils.TestHelper.ShippingAddressInit("Kishore", "", "NEW YORK", "NY", "1290 AVENUE OF AMERICAS", "", "10104");
           objShippingAddressWithInvalidAddressStreets = Utils.TestHelper.ShippingAddressInit("Kishore", "Kommi", "NEW YORK", "NY", "test street 1", "test street 2", "10104");
           objShippingAddressWithPOBoxAddress = Utils.TestHelper.ShippingAddressInit("Kishore", "Kommi", "SAN FRANSACISCO", "CA", "PO BOX 880004", "", "10104");
           objShippingaddressWithPartialCorrectAddress = Utils.TestHelper.ShippingAddressInit("Kishore", "Kommi", "NEW YORK", "NY", "1290 avenue of Americas", "", "10104");

           objBillingAddress = Utils.TestHelper.BillingAddressInit("Kishore", "Kommi", "NEW YORK", "NY", "1290 AVENUE OF AMERICAS", "", "10104");
           objBillingAddressWithFirstNameEmpty = Utils.TestHelper.BillingAddressInit("", "Kommi", "NEW YORK", "NY", "1290 AVENUE OF AMERICAS", "", "10104");
           objBillingAddressWithLastNameEmpty = Utils.TestHelper.BillingAddressInit("Kishore", "", "NEW YORK", "NY", "1290 AVENUE OF AMERICAS", "", "10104");
           objBillingAddressWithInvalidAddressStreets = Utils.TestHelper.BillingAddressInit("Kishore", "Kommi", "NEW YORK", "NY", "test street 1", "test street 2", "10104");
           objBillingAddressWithPOBoxAddress = Utils.TestHelper.BillingAddressInit("Kishore", "Kommi", "SAN FRANSACISCO", "CA", "PO BOX 880004", "", "10104");
           objBillingAddressWithPartialCorrectAddress = Utils.TestHelper.BillingAddressInit("Kishore", "Kommi", "NEW YORK", "NY", "1290 avenue of Americas", "", "10104");
        }


        [TestMethod]
        public void ValidateShippingAddress()
        {
            //Arrange            

            //Act            
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objShippingAddress);

            //Assert
            Assert.IsTrue((afmAddressData != null), "Address is null");

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ValidateShippingAddressWithFirstNameEmpty()
        {
            //Arrange                        

            //Act            
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objShippingAddressWithFirstNameEmpty);
            //Assert
            /*
             * It should return Data Validation Exception
             * ArgumentException
             */
            Assert.IsTrue((afmAddressData != null), "FirstName is null");

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ValidateShippingAddressWithLastNameEmpty()
        {
            //Arrange                        

            //Act            
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objShippingAddressWithLastNameEmpty);
            //Assert
            /*
             * It should return Data Validation Exception
             * ArgumentException
             */
            Assert.IsTrue((afmAddressData != null), "LastName is null");

        }

        [TestMethod]
        public void ValidateShippingAddressWithInvalidAddressStreets()
        {
            //Arrange                        

            //Act            
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objShippingAddressWithInvalidAddressStreets);
            
            //Assert
            Assert.IsTrue((!afmAddressData.IsAddressValid), "Address not valid");
        }

        [TestMethod]
        public void ValidateShippingAddressWithPOBoxAddress()
        {
            //Arrange                        

            //Act       
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objShippingAddressWithPOBoxAddress);

            //Assert
            /*
             * It should return ErrorMessage  as this is a PO BOX type address             
             */
            Assert.IsTrue((!afmAddressData.IsAddressValid), "Address not valid");
            Assert.IsTrue((afmAddressData.ErrorMessage != ""), afmAddressData.ErrorMessage);

        }

        [TestMethod]
        public void ValidateShippingAddressWithPartialCorrectAddress()
        {
            //Arrange                        

            //Act            
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objShippingaddressWithPartialCorrectAddress);

            //Assert
            /*
             * It should return suggested address
             * ArgumentException
             */
            Assert.IsTrue((afmAddressData.addressValidationsuggestedAddress != null), "Input address cannot be validated");

        }

        //Billing Address

        [TestMethod]
        public void ValidateBillingAddress()
        {
            //Arrange                        

            //Act            
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objBillingAddress);

            //Assert
            Assert.IsTrue((afmAddressData != null), "Address is null");

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ValidateBillingAddressWithFirstNameEmpty()
        {
            //Arrange                        

            //Act            
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objBillingAddressWithFirstNameEmpty);

            //Assert
            /*
             * It should return Data Validation Exception
             * ArgumentException
             */
            Assert.IsTrue((afmAddressData != null), "FirstName is null");

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ValidateBillingAddressWithLastNameEmpty()
        {
            //Arrange                        

            //Act            
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objBillingAddressWithLastNameEmpty);

            //Assert
            /*
             * It should return Data Validation Exception
             * ArgumentException
             */
            Assert.IsTrue((afmAddressData != null), "Lastname is null");

        }

        [TestMethod]
        public void ValidateBillingAddressWithInvalidAddressStreets()
        {
            //Arrange                        

            //Act             
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objBillingAddressWithInvalidAddressStreets);

            //Assert            
            Assert.IsTrue((afmAddressData.IsAddressValid), "Address not valid");
        }

        [TestMethod]
        public void ValidateBillingAddressWithPOBoxAddress()
        {
            //Arrange                        

            //Act            
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objBillingAddressWithPOBoxAddress);

            //Assert           
            Assert.IsTrue((!afmAddressData.IsAddressValid), "Address not valid");

        }

        [TestMethod]
        public void ValidateBillingAddressWithPartialCorrectAddress()
        {
            //Arrange                        

            //Act            
            AFMAddressData afmAddressData = AFMCheckoutController.ValidateAddress(objBillingAddressWithPartialCorrectAddress);

            //Assert            
            Assert.IsTrue((afmAddressData.IsAddressValid), "Address not valid");

        }

        [TestMethod]
        public void ValidateZipCode()
        {
            //Arrange 
            
            //Act            
            bool isvalidZip = AFMCheckoutController.ValidateZipcode(Utils.TestHelper.Zipcode, Utils.TestHelper.CountryRegioncId);

            //Assert            
            Assert.IsTrue((isvalidZip), "Zipcode Code not valid");

        }

        //public Address ShippingAddress(string firstName, string lastName, string city, string state, string streetLine1, string streetLine2, string Zipcode)
        //{
        //    Address shippingaddress = Utils.TestHelper.AddressData;

        //    shippingaddress.FirstName = firstName;
        //    shippingaddress.LastName = lastName;
        //    shippingaddress.City = city;
        //    shippingaddress.Country = "US";
        //    shippingaddress.State = state;
        //    shippingaddress.Street = streetLine1;
        //    shippingaddress.Street2 = streetLine2;
        //    shippingaddress.ZipCode = Zipcode;

        //    return shippingaddress;
        //}

        //public Address BillingAddress(string firstName, string lastName, string city, string state, string streetLine1, string streetLine2, string Zipcode)
        //{
        //    Address billingaddress = Utils.TestHelper.AddressData;

        //    billingaddress.FirstName = firstName;
        //    billingaddress.LastName = lastName;
        //    billingaddress.City = city;
        //    billingaddress.Country = "USA";
        //    billingaddress.AddressType = AddressType.Invoice;
        //    billingaddress.State = state;
        //    billingaddress.Street = streetLine1;
        //    billingaddress.Street2 = streetLine2;
        //    billingaddress.ZipCode = Zipcode;

        //    return billingaddress;
        //}
    }
}
