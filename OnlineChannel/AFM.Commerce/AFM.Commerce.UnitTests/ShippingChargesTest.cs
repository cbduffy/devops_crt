﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AFM.Commerce.Framework.Extensions.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;

#region Test Config Require
/*
 *  //--Charger Qty Range 0 to 5 -- 10 5 to 10 50 
 *  For Run this unit test below config required
22565423184 -- 1119 -- SDSO & SOD -- Fabrikam Laptop16 M6000
22565423163 -- 1112 -- DS & SOD -- WWI Laptop8.9 E0089
22565423182 -- 1118 -- NO ASSIGN MENT & SOD -- Proseware Laptop8.9 E089
22565423160 -- 1111 -- NO SOD -- WWI Laptop15.4W M0156

10 -- DS
11 -- HD
21 -- CC
 * 
 * 
 * //--Charger Qty Range 0 to 1000 -- 10 and 1001 to 2000 50 
 */
#endregion

namespace AFM.Commerce.UnitTests
{
    [TestClass]
    public class ShippingChargesTest
    {
        AFMShoppingCartController objAFMShoppingCartController = new AFMShoppingCartController();
        [TestInitialize]
        public void InitTest()
        {
            Utils.TestHelper.CustomerId = "0000000632";
            Utils.TestHelper.CreateCurrentHttpContext();
            Utils.TestHelper.AddZipCodetoCookie("10104");
            Utils.TestHelper.CreateCheckOutDataXmlFile();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            Utils.TestHelper.SearchEngine = new Utils.AFMSearchEngine();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            Utils.TestHelper.RemoveCheckOutDataxmlFile();
        }

        //[TestMethod]
        //public void ShippingChargesAddItemDSSODExceptedDeliveryMethodDS()
        //{
        //    //Arrange
        //    var itemListing = new List<Listing>{(new Listing
        //    {
        //        ListingId = 22565423163,
        //        IsKitVariant = false,
        //        GiftCardAmount = 0,
        //        Quantity = 1,
        //        ProductDetails = "WWI Laptop8.9 E0089"
        //    })};

        //    Utils.TestHelper.ProductValidator = new ProductValidator();

        //    //Act
        //    var shoppingCart = AFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
        //        Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
        //        Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);

        //    //Assert            
        //    //Kit Product Shoul be Add in Cart
        //    Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
        //    var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
        //    if (objKitItem != null)
        //    {
        //        Assert.IsTrue(objKitItem.AFMShippingMode == "DS", "Shipping Mode Should be DS");
        //    }
        //    else
        //    {
        //        Assert.IsTrue(false, "Item is not added");
        //    }
        //    Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        //}

        //--- check DS wiht SOD
        [TestMethod]
        public void ShippingChargesAddItemDSTrueSODFalseExceptedDeliveryMethodHD()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw",
               
                
            })};

            int DS = 1;
            int SDSO = 0;

            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 0;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

             shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert            
            //Kit Product Shoul be Add in Cart
             Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
             var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode == "Home Delivery", "Shipping Mode Should be HD and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemSODTrueExceptedDeliveryMethodCC()
        {
            var itemListing = new List<Listing>{(new Listing
            {
                 ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            //Update Item Deliver Detail
            int DS = 0;
            int SDSO = 0;
            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 1;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
               Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);


            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode == "Carrier", "Shipping Mode Should be CC and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemDSFalseSODTrueExceptedDeliveryMethodCC()
        {
            var itemListing = new List<Listing>{(new Listing
            {
                 ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            //Update Item Deliver Detail
            int DS = 0;
            int SDSO = 0;
            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 1;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);


            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);


            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode == "Carrier", "Shipping Mode Should be CC and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemDSTrueSDSOFalseSODFalseExceptedDeliveryMethodHD()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                 ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            //Update Item Deliver Detail
            int DS = 1;
            int SDSO = 0;
            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 0;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);


            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode == "Home Delivery", "Shipping Mode Should be HD and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        //--- check SDSO wiht SOD
        [TestMethod]
        public void ShippingChargesAddItemDSTrueSODFalseSDSOFalseExceptedDeliveryMethodHD()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                 ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            int DS = 1;
            int SDSO = 0;

            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 0;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);


            //Assert            
            //Kit Product Shoul be Add in Cart
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode.Trim() == "Home Delivery", "Shipping Mode Should be HD and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemDSFalseSDSOFalseSODTrueExceptedDeliveryMethodCC()
        {
            var itemListing = new List<Listing>{(new Listing
            {
                 ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            //Update Item Deliver Detail
            int DS = 0;
            int SDSO = 0;
            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 1;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);


            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode.Trim() == "Carrier", "Shipping Mode Should be CC and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemSDSOFalseSODTrueExceptedDeliveryMethodCC()
        {
            var itemListing = new List<Listing>{(new Listing
            {
                 ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};


            //Update Item Deliver Detail
            int DS = 0;
            int SDSO = 0;
            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 1;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);


            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode == "Carrier", "Shipping Mode Should be CC and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemSDSOFalseSODFalseExceptedDeliveryMethodHD()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                 ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            //Update Item Deliver Detail
            int DS = 1;
            int SDSO = 0;
            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 0;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode == "Home Delivery", "Shipping Mode Should be HD and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        //--- check SDSO  and DS with SOD True

        [TestMethod]
        public void ShippingChargesAddItemDSFalseSODTrueSDSOFalseExceptedDeliveryMethodCC()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            int DS = 0;
            int SDSO = 0;

            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 1;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert            
            //Kit Product Shoul be Add in Cart
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode.Trim() == "Carrier", "Shipping Mode Should be CC and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemSDSOTrueSODTrueDSFalseExceptedDeliveryMethodCC()
        {
            var itemListing = new List<Listing>{(new Listing
            {
                ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            //Update Item Deliver Detail
            int DS = 0;
            int SDSO = 0;
            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 1;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode.Trim() == "Carrier", "Shipping Mode Should be CC and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemSODTrueDSFalseSDSOFalseExceptedDeliveryMethodCC()
        {
            var itemListing = new List<Listing>{(new Listing
            {
                ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            //Update Item Deliver Detail
            int DS = 0;
            int SDSO = 0;
            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 1;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode == "Carrier", "Shipping Mode Should be CC and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemSDSOFalseDSFalseSODTrueExceptedDeliveryMethodCC()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            //Update Item Deliver Detail
            int DS = 0;
            int SDSO = 0;
            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 1;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            var objKitItem = shoppingCart.Items.FirstOrDefault(tmp => tmp.ProductId == itemListing[0].ListingId);
            if (objKitItem != null)
            {
                Assert.IsTrue(objKitItem.AFMShippingMode == "Carrier", "Shipping Mode Should be CC and You get " + objKitItem.AFMShippingMode);
            }
            else
            {
                Assert.IsTrue(false, "Item is not added");
            }
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        //--Zip code
        [TestMethod]
        public void ShippingChargesAddItemMultiPleItemZipcodeExceptedChargeAmount()
        {
            //Arrange
            var itemListing = new List<Listing>{                    
                    new Listing  {
                        ItemId="A1000029T",
                        ListingId = 5637146279,
                        IsKitVariant = false,
                        GiftCardAmount = 0,
                        Quantity = 1,
                        ProductDetails = "Throw"
                    },
                    new Listing  {
                        ItemId="A1000030T",
                        ListingId = 5637146281,
                        IsKitVariant = false,
                        GiftCardAmount = 0,
                        Quantity = 1,
                        ProductDetails = "Throw"
                    }
            };


            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            //objAFMShoppingCartController.AddEstShippingCostToCart(shoppingCart, Utils.TestHelper.GetCartFromCartId(shoppingCart.CartId),
             //   Utils.TestHelper.CustomerId, true, ShoppingCartDataLevel.All, Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine);

            objAFMShoppingCartController.AddEstShippingCostToCart(shoppingCart, Utils.TestHelper.GetCartFromCartId(shoppingCart.CartId),
                Utils.TestHelper.CustomerId, true);


            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            Assert.IsTrue(shoppingCart.EstShipping != "0", "ShoppingCart Estimated shipping Should be greater then zero");
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        //--Charger Qty Range 0 to 5 -- 10 5 to 10 50 
        [TestMethod]
        public void ShippingChargesAddItemDSTrueSODTrueItemQtyInRangeChargeAmount()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            decimal chargeAmount = 10;
            //int fromQty = 0;
            //int toQty = 5;
            //string SQLQuery = "Update [ax].[AFMATPGRADUATEDPIECEDLVCHARGE] set AMOUNT=" +
            //    chargeAmount + " ,FROMQTY=" + fromQty + " , TOQTY=" + toQty + "  where  CHARGECODE='FS02'";
            //Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
             Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            Assert.IsTrue(shoppingCart.EstShipping != ""  , "ShoppingCart Estimated shipping Should be greater then zero");
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemDSTrueSODTrueItemQtySeconRangeChargeAmountMax()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                ItemId = "1040021",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};
            decimal chargeAmount = 50;
            //int fromQty = 5;
            //int toQty = 10;
            //string SQLQuery = "Update [ax].[AFMATPGRADUATEDPIECEDLVCHARGE] set AMOUNT=" +
            //    chargeAmount + " ,FROMQTY=" + fromQty + " , TOQTY=" + toQty + "  where  CHARGECODE='FS02'";
            //Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
             Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemDSTrueSODTrueItemQtyGreaterToQtyChargeAmountMax()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            decimal chargeAmount = 50;
            //int fromQty = 5;
            //int toQty = 10;
            //string SQLQuery = "Update [ax].[AFMATPGRADUATEDPIECEDLVCHARGE] set AMOUNT=" +
            //    chargeAmount + " ,FROMQTY=" + fromQty + " , TOQTY=" + toQty + "  where  CHARGECODE='FS02'";
            //Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
             Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            Assert.IsTrue(shoppingCart.EstShipping != "", "ShoppingCart Estimated shipping should be zero but you get " + shoppingCart.EstShipping);
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        ////--CC Charger Fixced Rated
        [TestMethod]
        public void ShippingChargesAddItemFixcedRatedChargeAmount()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                ItemId = "A1000029T",
                ListingId = 5637146279,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};
            decimal chargeAmount = 50;
            //int fromQty = 0;
            //int toQty = 5;
            //string SQLQuery = "Update [ax].[AFMATPGRADUATEDPIECEDLVCHARGE] set AMOUNT=" +
            //    chargeAmount + " ,FROMQTY=" + fromQty + " , TOQTY=" + toQty + "  where  CHARGECODE='FS02'";
            //Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
             Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            Assert.IsTrue(shoppingCart.EstShipping == "$0.00", "ShoppingCart Estimated shipping Should be " + chargeAmount + " you get" + shoppingCart.EstShipping);
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        //--Charger Qty Range 0 to 1000 -- 10 and 1001 to 2000 50 
        [TestMethod]
        public void ShippingChargesAddItemItemAmountFirstChargeAmountMax()
        {
            var itemListing = new List<Listing>{(new Listing
            {
                ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            int DS = 1;
            int SDSO = 0;

            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 22565423159;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 0;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 22565423159;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            decimal chargeAmount = 10;
            //int fromQty = 5;
            //int toQty = 10;
            //string SQLQuery = "Update [ax].[AFMATPGRADUATEDPIECEDLVCHARGE] set AMOUNT=" +
            //    chargeAmount + " ,FROMQTY=" + fromQty + " , TOQTY=" + toQty + "  where  CHARGECODE='FS02'";
            //Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
             Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            //Assert.IsTrue((shoppingCart.EstShipping + shoppingCart.EstHomeDelivery) == chargeAmount, "ShoppingCart Estimated shipping Should be " + chargeAmount + " you get" + shoppingCart.EstShipping);
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemItemAmountSeconRangeChargeAmountMax()
        {
            var itemListing = new List<Listing>{(new Listing
            {
                 ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            int DS = 1;
            int SDSO = 0;

            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 0;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            decimal chargeAmount = 50;
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            //Assert.IsTrue((shoppingCart.EstShipping + shoppingCart.EstHomeDelivery) == chargeAmount, "ShoppingCart Estimated shipping Should be " + chargeAmount + " you get" + (shoppingCart.EstShipping + shoppingCart.EstHomeDelivery));
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }

        [TestMethod]
        public void ShippingChargesAddItemItemamountGreaterToQtyChargeAmountMax()
        {
            //Arrange
            var itemListing = new List<Listing>{(new Listing
            {
                 ItemId = "A1000030T",
                ListingId = 5637146281,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            })};

            int DS = 1;
            int SDSO = 0;

            string SQLQuery = "UPDATE AX.AFMECORESPRODUCT SET DIRECTSHIPONLY=" + SDSO + " , EXPRESSDELIVERY=" + DS + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);

            int SOD = 0;
            SQLQuery = "UPDATE AX.AFMECOMMPRODUCTOMOPERATINGUNIT SET ISECOMOWNED=" + SOD + " where ECORESPRODUCT=" + 5637146281;
            Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);
            decimal chargeAmount = 50;
            //int fromQty = 5;
            //int toQty = 10;
            //string SQLQuery = "Update [ax].[AFMATPGRADUATEDPIECEDLVCHARGE] set AMOUNT=" +
            //    chargeAmount + " ,FROMQTY=" + fromQty + " , TOQTY=" + toQty + "  where  CHARGECODE='FS02'";
            //Utils.TransactionHelper.ExecuteNonQuery(SQLQuery);
            Utils.TestHelper.ProductValidator = new AFMProductValidator();

            //Act
            var shoppingCart = objAFMShoppingCartController.AddItems(string.Empty, String.Empty, itemListing,
                Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.ShoppingCartDataLevel.All,
                Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false);

            shoppingCart = objAFMShoppingCartController.GetShoppingCart(shoppingCart.CartId, String.Empty, ShoppingCartDataLevel.All,
              Utils.TestHelper.ProductValidator, Utils.TestHelper.SearchEngine, false, true);

            //Assert                        
            Assert.IsTrue(shoppingCart != null, "Null Value Retrun in ShoppingCart");
            //Assert.IsTrue((shoppingCart.EstShipping + shoppingCart.EstHomeDelivery) == chargeAmount, "ShoppingCart Estimated shipping should be " + chargeAmount + " but you get " + (shoppingCart.EstShipping + shoppingCart.EstHomeDelivery));
            Utils.TestHelper.RemoveShoppingCart(shoppingCart.CartId, string.Empty);
        }
    }
}
