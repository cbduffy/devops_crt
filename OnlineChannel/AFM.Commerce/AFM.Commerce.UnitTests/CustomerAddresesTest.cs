﻿
using System;
using AFM.Commerce.Framework.Core.ClientManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using System.Collections.ObjectModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using AFM.Commerce.Framework.Extensions.Controllers;
using AFM.Commerce.UnitTests.Utils;
using System.Collections.Generic;
using customModel= Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

namespace AFM.Commerce.UnitTests
{
    [TestClass]
    public class CustomerAddresesTest
    {
        [TestMethod]
        public void CustomerAddresesNotEmpty()
        {
            CustomerController objCustomerController = new CustomerController();

            //Arrange            
            Collection<customModel.Address> customerAddresses = objCustomerController.GetAddresses("0000000632");


            //Assert
              Assert.IsTrue((customerAddresses!=null), "Address not null");
            Assert.IsTrue((customerAddresses.Count>0), "Received addresses");

        }

        [TestMethod]
        public void UpdateAddresses()
        {
            AFMCustomerController customerController = new AFMCustomerController();
            customModel.Customer customer = customerController.GetCustomer("0000000632");
            List<customModel.Address> addresses = new List<customModel.Address>();
            TestHelper.AddressData.AddressType = customModel.AddressType.Invoice;
            addresses.Add(TestHelper.AddressData);
            customModel.Customer updatedCustomer = customerController.UpdateAddresses("0000000632", addresses);
           

            //Assert
            Assert.IsTrue((updatedCustomer != null), "Updated customer not null");

        }
        }
    }

