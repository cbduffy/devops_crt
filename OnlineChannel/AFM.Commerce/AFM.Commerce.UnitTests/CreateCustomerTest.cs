﻿
using System;
using AFM.Commerce.Framework.Core.ClientManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using System.Collections.ObjectModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using AFM.Commerce.Framework.Extensions.Controllers;

namespace AFM.Commerce.UnitTests
{
    [TestClass]
    public class CreateCustomerTest
    {
        [TestMethod]
        public void CreateCustomerNotNull()
        {

            //Arrange            
            Customer customer = new Customer();
            customer.Email = "email@ashleydomain.com";
            customer.FirstName = "test";
            customer.IsGuestCheckout = true;
            customer.IsMarketingOptIn = false;
            customer.IsMarketingOptInDate = Convert.ToDateTime("1/1/1900");
            customer.IsOlderThan13 = true;
            customer.LastName = "ashleycustomer";
            customer.Phone = "986589657";
            customer.PhoneExt = "124";
            //customer.Email = "abc@wer.com";
            customer.SourceInfo = "Times Of India Newspaper";

            Customer newCustomer = new AFMCustomerController().CreateCustomer(customer, "email@ashleydomain.com");

            string accountNumber = string.Empty;

            if(newCustomer!=null)
               accountNumber = newCustomer.AccountNumber;
            //Assert
            Assert.IsTrue((newCustomer != null), "Customer not null");
            Assert.IsTrue((!string.IsNullOrEmpty(accountNumber)), "Customer account number created");

        }

        [TestMethod]
        public void ValidateZipCode()
        {
            bool isValid = AFMCheckoutController.ValidateZipcode("10104", "USA");
        }
    }
}