﻿using System;
using AFM.Commerce.Framework.Core.ClientManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;

namespace AFM.Commerce.UnitTests
{
    [TestClass]
    public class ConfirmationNumberTest
    {
        [TestMethod]
        public void OrderConfirmationNumberNotEmpty()
        {

            //Arrange            
            AFMConfirmationNumberManager afmConfirmationNumber = AFMConfirmationNumberManager.Create(CrtUtilities.GetCommerceRuntime());  

            //Act            
             string confirmationNumber=afmConfirmationNumber.GetConfirmationSequenceNumber();
             Console.WriteLine(confirmationNumber);

            //Assert
              Assert.IsTrue(!string.IsNullOrEmpty(confirmationNumber), "No confirmation number generated");

        }
        }
    }

