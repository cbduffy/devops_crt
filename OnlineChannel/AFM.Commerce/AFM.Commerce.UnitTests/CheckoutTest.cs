﻿using System;
using System.Reflection;
using System.Globalization;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Microsoft.Dynamics.Commerce.Runtime;
using AFM.Commerce.Framework.Extensions.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using System.Web;
using AFM.Commerce.Framework.Extensions.Utils;

namespace AFM.Commerce.UnitTests
{
    [TestClass]
    public class CheckoutTest
    {
        private AFMCheckoutController objCheckOutcontroller = new AFMCheckoutController();
        private AFMShoppingCartController objAFMShoppingCartController = new AFMShoppingCartController();
        CustomerController objCustomerController = new CustomerController();

        [TestInitialize]
        public void InitTest()
        {

            Utils.TestHelper.UserName = "HARDIK PATEL";
            Utils.TestHelper.CustomerId = "0000001003";            
            Utils.TestHelper.CreateCheckOutDataXmlFile();
            Utils.TestHelper.CreateCurrentHttpContext();
            Utils.TestHelper.AddZipCodetoCookie("10104");
            Utils.TestHelper.CreateShoppingCart();                
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
            Utils.TestHelper.ShippingAddressInit(
                "FirstName",
                "LastName",
                "New York",
                "NY",
                "1290 Avenue of Americas",
                "",
                "10104"
                );
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            Utils.TestHelper.RemoveCheckOutDataxmlFile();
            Utils.TestHelper.RemoveShoppingCart(Utils.TestHelper.ShoppingCartId, string.Empty);
        }

        # region GetDeliveryMethod
        [TestMethod]
        public void GetDeliveryMethodOrderShippingOptionIsPickupFromStore()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            selectedOrderShippingOptions.DeliveryPreferenceId = "2";
            //selectedOrderShippingOptions.DeliveryMethodId = "10";
            //selectedOrderShippingOptions.DeliveryMethodText = "Truck";
            var objAllDeliveryMethodExpected = Utils.TestHelper.GetAllDeliveryMethodsExpected();
            //Act
            var objGetDeliveryMethods = objCheckOutcontroller.GetDeliveryMethods(shoppingcartId, customerId, selectedOrderShippingOptions);
            //Assert
            Assert.IsTrue(objGetDeliveryMethods != null, "Delivery method shoul not be Null");
            if (objGetDeliveryMethods != null)
            {
                foreach (var objAllDelivery in objGetDeliveryMethods.DeliveryMethods)
                {
                    var objAllDeliveryExpected =
                        objAllDeliveryMethodExpected.FirstOrDefault(objDelivery => objDelivery.Id == objAllDelivery.Id);
                    if (objAllDeliveryExpected != null)
                    {
                        Assert.IsTrue(objAllDelivery.Id == objAllDeliveryExpected.Id, "Delivery Method not matched");
                    }
                    else
                    {
                        Assert.IsTrue(false, "Delivery method " + objAllDelivery.Id + " Not Found in expected result");

                    }
                }
            }

        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetDeliveryMethodOrderShippingOptionIsShipItemsIndividuallyDeliveryMethodNullNullReferanceException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            selectedOrderShippingOptions.DeliveryPreferenceId = "1";
            //selectedOrderShippingOptions.DeliveryMethodId = "10";
            //selectedOrderShippingOptions.DeliveryMethodText = "Truck";
            var objAllDeliveryMethodExpected = Utils.TestHelper.GetAllDeliveryMethodsExpected();
            //Act
            var objGetDeliveryMethods = objCheckOutcontroller.GetDeliveryMethods(shoppingcartId, customerId, selectedOrderShippingOptions);
            //Assert          

        }

        [TestMethod]
        public void GetDeliveryMethodOrderShippingOptionIsShipItemsIndividuallyDeliveryMethodValue()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            selectedOrderShippingOptions.DeliveryPreferenceId = "2";
            selectedOrderShippingOptions.DeliveryModeId = "10";
            //selectedOrderShippingOptions.DeliveryMethodText = "Truck";
            var objAllDeliveryMethodExpected = Utils.TestHelper.GetAllDeliveryMethodsExpected();
            //Act
            var objGetDeliveryMethods = objCheckOutcontroller.GetDeliveryMethods(shoppingcartId, customerId, selectedOrderShippingOptions);
            //Assert
            Assert.IsTrue(objGetDeliveryMethods != null, "Delivery method shoul not be Null");
            if (objGetDeliveryMethods != null)
            {
                foreach (var objAllDelivery in objGetDeliveryMethods.DeliveryMethods)
                {
                    var objAllDeliveryExpected =
                        objAllDeliveryMethodExpected.FirstOrDefault(objDelivery => objDelivery.Id == objAllDelivery.Id);
                    if (objAllDeliveryExpected != null)
                    {
                        Assert.IsTrue(objAllDelivery.Id == objAllDeliveryExpected.Id, "Delivery Method not matched");
                    }
                    else
                    {
                        Assert.IsTrue(false, "Delivery method " + objAllDelivery.Id + " Not Found in expected result");

                    }
                }
            }

        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetDeliveryMethodOrderShippingOptionIsShipToNewAddressNullCustomAddressNullException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            selectedOrderShippingOptions.DeliveryPreferenceId = "3";
            selectedOrderShippingOptions.DeliveryModeId = "10";
            //selectedOrderShippingOptions.DeliveryMethodText = "Truck";
            var objAllDeliveryMethodExpected = Utils.TestHelper.GetAllDeliveryMethodsExpected();
            //Act
            var objGetDeliveryMethods = objCheckOutcontroller.GetDeliveryMethods(shoppingcartId, customerId, selectedOrderShippingOptions);
            //Assert            
        }

        [TestMethod]
        public void GetDeliveryMethodOrderShippingOptionIsShipToNewAddressCustomAddress()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            selectedOrderShippingOptions.DeliveryPreferenceId = "2";
            selectedOrderShippingOptions.DeliveryModeId = "10";
            //selectedOrderShippingOptions.DeliveryMethodText = "Truck";
            var objAllDeliveryMethodExpected = Utils.TestHelper.GetAllDeliveryMethodsExpected();
            //Act
            var objGetDeliveryMethods = objCheckOutcontroller.GetDeliveryMethods(shoppingcartId, customerId, selectedOrderShippingOptions);
            //Assert
            Assert.IsTrue(objGetDeliveryMethods.DeliveryMethods != null, "Delivery method shoul not be Null");
            if (objGetDeliveryMethods.DeliveryMethods != null)
            {
                foreach (var objAllDelivery in objGetDeliveryMethods.DeliveryMethods)
                {
                    var objAllDeliveryExpected =
                        objAllDeliveryMethodExpected.FirstOrDefault(objDelivery => objDelivery.Id == objAllDelivery.Id);
                    if (objAllDeliveryExpected != null)
                    {
                        Assert.IsTrue(objAllDelivery.Id == objAllDeliveryExpected.Id, "Delivery Method not matched");
                    }
                    else
                    {
                        Assert.IsTrue(false, "Delivery method " + objAllDelivery.Id + " Not Found in expected result");

                    }
                }
            }

        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetDeliveryMethodShoppingCartInvalidShoppingCartIDNullExceptionExcepted()
        {
            //Arrange
            var shoppingCart = "adbcde";
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingCart;
            var shippingAddres = objCustomerController.GetAddresses(Utils.TestHelper.CustomerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            //Act
            objCheckOutcontroller.GetDeliveryMethods(shoppingCart, string.Empty, selectedOrderShippingOptions);

            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetDeliveryMethodShoppingCartInvalidCustomerIDNullExceptionExcepted()
        {
            //Arrange
            var shoppingCart = Utils.TestHelper.ShoppingCartId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingCart;
            var shippingAddres = objCustomerController.GetAddresses(Utils.TestHelper.CustomerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            //Act
            objCheckOutcontroller.GetDeliveryMethods(shoppingCart, "15000150", selectedOrderShippingOptions);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetDeliveryMethodShoppingCart50CustomerIDNullExceptionExcepted()
        {
            //Arrange
            var shoppingCart = Utils.TestHelper.ShoppingCartId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingCart;
            var shippingAddres = objCustomerController.GetAddresses(Utils.TestHelper.CustomerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            //Act
            objCheckOutcontroller.GetDeliveryMethods(shoppingCart, Utils.TestHelper.CustomerId.PadRight(50, '0'), selectedOrderShippingOptions);

            //Assert
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetDeliveryMethodNullShoppingCartExpectedNullReferenceException()
        {
            //Arrange 
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();

            //Act
            objCheckOutcontroller.GetDeliveryMethods(null, customerId, selectedOrderShippingOptions);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void GetDeliveryMethodNullCustomerIdExpectedNullReferenceException()
        {
            //Arrange 
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //Act
            objCheckOutcontroller.GetDeliveryMethods(shoppingcartId, null, selectedOrderShippingOptions);
            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetDeliveryMethodNullSelectedOrderShippingOptionsExpectedNullReferenceException()
        {
            //Arrange 
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            //Act
            objCheckOutcontroller.GetDeliveryMethods(shoppingcartId, customerId, null);
            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetDeliveryMethodNullAllParameterExpectedNullReferenceException()
        {
            //Arrange 

            //Act
            objCheckOutcontroller.GetDeliveryMethods(null, null, null);
            //Assert
        }
        #endregion

        # region SetShippingOption

        [TestMethod]
        public void SetShippingOptionAllParameterShippingOptionTypePickupFromStore()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;

            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode,
                StoreId = "HOUSTON"
            };
            selectedOrderShippingOptions.DeliveryPreferenceId = "2";
            selectedOrderShippingOptions.DeliveryModeId = "10";
            //selectedOrderShippingOptions.DeliveryMethodText = "Truck";

            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
            Assert.IsTrue(objSetShippingOption != null, "Shipping Option should not be Null");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetShippingOptionAllParameterShippingOptionTypePickupFromStoreNullStoreAddressNullException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;

            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.DeliveryPreferenceId = "2";
            //selectedOrderShippingOptions.DeliveryMethodId = "10";
            //selectedOrderShippingOptions.DeliveryMethodText = "Truck";

            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
            Assert.IsTrue(objSetShippingOption != null, "Shipping Option should not be Null");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetShippingOptionAllParameterShippingOptionTypePickupFromStoreNullStoreAddressStoreIdNullExceptionExcepted()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;

            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.DeliveryPreferenceId = "2";
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
            Assert.IsTrue(objSetShippingOption != null, "Shipping Option should not be Null");
        }


        [TestMethod]
        public void SetShippingOptionAllParameterShippingOptionTypeEmailDelivery()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };
            selectedOrderShippingOptions.DeliveryPreferenceId = "4";

            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;

            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
        }


        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void SetShippingOptionAllParameterShippingOptionTypeShipItemsIndividuallyNullCustomAddress()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            // selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };
            selectedOrderShippingOptions.DeliveryPreferenceId = "1";
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;


            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void SetShippingOptionAllParameterShippingOptionTypeShipItemsIndividuallyNullDeliveryMethods()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };
            selectedOrderShippingOptions.DeliveryPreferenceId = "1";
            //selectedOrderShippingOptions.DeliveryMethodId = "10";
            //selectedOrderShippingOptions.DeliveryMethodText = "Truck";
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;


            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
        }

        [TestMethod]
        public void SetShippingOptionAllParameterShippingOptionTypeShipItemsIndividuallyDeliveryMethods()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };
            selectedOrderShippingOptions.DeliveryPreferenceId = "1";
            selectedOrderShippingOptions.DeliveryModeId = "10";
            //selectedOrderShippingOptions.DeliveryMethodText = "Truck";
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = Utils.TestHelper.ShoppingCartDataLevel;


            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
            Assert.IsTrue(objSetShippingOption != null, "ShippingOption Should Not be Null");
        }


        [TestMethod]
        public void SetShippingOptionAllParameterShippingOptionTypeUndefined()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;

            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };
            selectedOrderShippingOptions.DeliveryPreferenceId = "0";
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;


            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
        }


        [TestMethod]
        public void SetShippingOptionAllParameterShoppingCartDataLevelTypeAll()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;
            //Act
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
               selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);
            //Assert

        }

        [TestMethod]
        public void SetShippingOptionAllParameterShoppingCartDataLevelTypeMinimal()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;
            //Act
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
               selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);
            //Assert

        }

        [TestMethod]
        public void SetShippingOptionAllParameterShoppingCartDataLevelTypeExtended()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;

            //Act
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
               selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert

        }


        [TestMethod]
        [ExpectedException(typeof(DataValidationException))]
        public void SetShippingOptionInvalidShoppingCartIdNullReferanceExcepted()
        {
            //Arrange
            var shoppingcartId = "00052421sadfasdfsersdf";
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;

            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(customerId, shoppingcartId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void SetShippingOptionInvalidCustomerIdNullReferanceExcepted()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = "asdfasdfwerwersd";
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;

            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(customerId, shoppingcartId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void SetShippingOption50CustomerIdArgumentOutOfRangeException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId.PadRight(50, '0');
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;

            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            //selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode
            };

            //Act    
            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(customerId, shoppingcartId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetShippingOptionsNullShoppingCartIdExpectedNullReferanceException()
        {
            //Arrange
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;

            //Act
            objCheckOutcontroller.SetOrderDeliveryOption(null, customerId,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetShippingOptionsNullCustomerIdExpectedNullReferanceException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;
            //Act
            objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, null,
                selectedOrderShippingOptions, productValidator, Utils.TestHelper.SearchEngine, dataLevel);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetShippingOptionsNullShoppingSelectedShippingOptionsExpectedNullReferanceException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var productValidator = Utils.TestHelper.ProductValidator;
            var dataLevel = ShoppingCartDataLevel.Minimal;
            //Act
            objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                null, productValidator, Utils.TestHelper.SearchEngine, dataLevel);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetShippingOptionsNullShoppingProductValidatorExpectedNullReferanceException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            var dataLevel = ShoppingCartDataLevel.Minimal;
            //Act
            objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, null, Utils.TestHelper.SearchEngine, dataLevel);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void SetShippingOptionsKitItemShoppingCartDataLevelAllSearchengineNullExpectedNullReferenceException()
        {
            //Arrange
            var objShoppingCart = Utils.TestHelper.CreateShoppingCartHavingKitItem();
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            var dataLevel = ShoppingCartDataLevel.All;
            //Act
            objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, dataLevel);
            //Assert
        }

        [TestMethod]
        public void SetShippingOptionsKitItemShoppingCartDataLevelMinimalSearchEngineNullExpectedNoException()
        {
            //Arrange
            var objShoppingCart = Utils.TestHelper.CreateShoppingCartHavingKitItem();
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            var dataLevel = ShoppingCartDataLevel.Minimal;
            //Act
            objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, Utils.TestHelper.ProductValidator,
                Utils.TestHelper.SearchEngine, dataLevel);
            //Assert
        }

        #endregion

        #region CreateOrder
        [TestMethod]
        public void CreateOrderAllParameter()
        {
            //Arrange
            var cutInfo = CultureInfo.InvariantCulture;
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = Utils.TestHelper.AddressData.Email;
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();

            //Update Shipping Address
            var selectedOrderShippingOptions = Utils.TestHelper.CreateShippingOption();
            
            var dataLevel = Utils.TestHelper.ShoppingCartDataLevel;
            
            //var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
            //    selectedOrderShippingOptions, productValidator, null, dataLevel);

            //Act
            objAFMShoppingCartController.CommenceCheckout(shoppingcartId, customerId, "", productValidator, Utils.TestHelper.SearchEngine,
                ShoppingCartDataLevel.Minimal);

            var objSetCreateOrder = objCheckOutcontroller.CreateOrder(shoppingcartId, customerId, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(DataValidationException))]
        public void CreateOrderWithoutShippindAddressDataValidationExceptionExcepted()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = Utils.TestHelper.EmailAddress;
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();

            //Act
            var ShoppingCart = objAFMShoppingCartController.CommenceCheckout(shoppingcartId, customerId, "", productValidator, Utils.TestHelper.SearchEngine,
               ShoppingCartDataLevel.Minimal);

            var objSetCreateOrder = objCheckOutcontroller.CreateOrder(ShoppingCart.CartId, customerId, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert

            Assert.IsNotNull(objSetCreateOrder, "Null Value Return");
        }

        [TestMethod]
        [ExpectedException(typeof(DataValidationException))]
        public void CreateOrderWithoutTenderLineHaveingSamePaymentDetailDataValidationExceptionExcepted()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = Utils.TestHelper.EmailAddress;
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();
            tenderDataLines.Add(tenderDataLines[0]);

            var selectedOrderShippingOptions = new SelectedDeliveryOption();
            // selectedOrderShippingOptions.CartId = shoppingcartId;
            var shippingAddres = objCustomerController.GetAddresses(customerId)[0];
            selectedOrderShippingOptions.CustomAddress = shippingAddres;
            selectedOrderShippingOptions.StoreAddress = new StoreProductAvailability()
            {
                AddressFriendlyName = shippingAddres.AddressFriendlyName,
                AttentionTo = shippingAddres.AttentionTo,
                AddressType = shippingAddres.AddressType,
                ChannelId = Utils.TestHelper.GetDefaultChannelId(),
                City = shippingAddres.City,
                Country = shippingAddres.Country,
                County = shippingAddres.County,
                Deactivate = shippingAddres.Deactivate,
                Distance = "",
                DistrictName = shippingAddres.DistrictName,
                Email = shippingAddres.Email,
                Email2 = shippingAddres.Email2,
                EmailContent = shippingAddres.EmailContent,
                ExtensionData = shippingAddres.ExtensionData,
                FirstName = shippingAddres.FirstName,
                RecordId = shippingAddres.RecordId,
                LastName = shippingAddres.LastName,
                State = shippingAddres.State,
                Street = shippingAddres.Street,
                StreetNumber = shippingAddres.StreetNumber,
                ZipCode = shippingAddres.ZipCode,
                StoreId = "Houston"
            };
            selectedOrderShippingOptions.DeliveryPreferenceId = "4";
            selectedOrderShippingOptions.DeliveryModeId = "10";
            //selectedOrderShippingOptions.DeliveryMethodText = "Truck";

            var dataLevel = Utils.TestHelper.ShoppingCartDataLevel;

            var objSetShippingOption = objCheckOutcontroller.SetOrderDeliveryOption(shoppingcartId, customerId,
                selectedOrderShippingOptions, productValidator, null, dataLevel);

            //Act
            var ShoppingCart = objAFMShoppingCartController .CommenceCheckout(shoppingcartId, customerId, "", productValidator, Utils.TestHelper.SearchEngine,
              ShoppingCartDataLevel.Minimal);

            var objSetCreateOrder = objCheckOutcontroller.CreateOrder(ShoppingCart.CartId, customerId, cardToken, tenderDataLines,new Entities.AFMSalesTransHeader() , emailAddress, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(DataValidationException))]
        public void CreateOrderInvalidShoppingCartIdInValidCustomerExceptedNullException()
        {
            //Arrange
            var shoppingcartId = "asdfasdfsadfasdfsd";
            var customerId = "121542154214124";
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = Utils.TestHelper.EmailAddress;
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();

            //Act
            var objSetCreateOrder = objCheckOutcontroller.CreateOrder(shoppingcartId, customerId, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(DataValidationException))]
        public void CreateOrderInvalidShoppingCartIdValidCustomerExceptedNullException()
        {
            //Arrange
            var shoppingcartId = "asdfasdfsadfasdfsd";
            var customerId = Utils.TestHelper.CustomerId;
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = Utils.TestHelper.EmailAddress;
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();

            //Act
            var objSetCreateOrder = objCheckOutcontroller.CreateOrder(shoppingcartId, customerId, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(DataValidationException))]
        public void CreateOrderInvalidCustomerIdValidShoppingCartExceptionNullException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = "124as5df4";
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = Utils.TestHelper.EmailAddress;
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();

            //Act
            var objSetCreateOrder = objCheckOutcontroller.CreateOrder(shoppingcartId, customerId, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(DataValidationException))]
        public void CreateOrderInvalidEmailAddressDataValidationException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = "asdfsadf";
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();

            //Act
            var objSetCreateOrder = objCheckOutcontroller.CreateOrder(shoppingcartId, customerId, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateOrderNullShoppingCartIdExpectedNullReferanceException()
        {
            //Arrange
            var customerId = Utils.TestHelper.CustomerId;
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = Utils.TestHelper.EmailAddress;
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();
            //Act
            objCheckOutcontroller.CreateOrder(null, customerId, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateOrderNullCustomerIdExpectedNullReferanceException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = Utils.TestHelper.EmailAddress;
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();
            //Act
            objCheckOutcontroller.CreateOrder(shoppingcartId, null, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateOrderNullCardTokenExpectedNullReferanceException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var emailAddress = Utils.TestHelper.EmailAddress;
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();
            //Act
            objCheckOutcontroller.CreateOrder(shoppingcartId, customerId, null, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateOrderNullEmailAddressExpectedNullReferanceException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var cardToken = Utils.TestHelper.CardToken;
            var userName = Utils.TestHelper.UserName;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();
            //Act
            objCheckOutcontroller.CreateOrder(shoppingcartId, customerId, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), null, userName, productValidator, Utils.TestHelper.SearchEngine);
            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void CreateOrderNullUserNameExpectedNullReferanceException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = Utils.TestHelper.EmailAddress;
            var productValidator = Utils.TestHelper.ProductValidator;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();
            //Act
            objCheckOutcontroller.CreateOrder(shoppingcartId, customerId, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, null, productValidator, Utils.TestHelper.SearchEngine);
            //Assert

        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void CreateOrderNullProductValidatorExpectedNullReferanceException()
        {
            //Arrange
            var shoppingcartId = Utils.TestHelper.ShoppingCartId;
            var customerId = Utils.TestHelper.CustomerId;
            var cardToken = Utils.TestHelper.CardToken;
            var emailAddress = Utils.TestHelper.EmailAddress;
            var userName = Utils.TestHelper.UserName;
            var tenderDataLines = Utils.TestHelper.AllTenderdataLines();
            //Act
            objCheckOutcontroller.CreateOrder(shoppingcartId, customerId, cardToken, tenderDataLines, new Entities.AFMSalesTransHeader(), emailAddress, userName, null, Utils.TestHelper.SearchEngine);
            //Assert

        }

        #endregion

        #region GetAllDeliveryMethod

        //[TestMethod]
        //public void GetAllDeliveryMethod()
        //{
        //    //Arrange
        //    var objAllDeliveryMethodExpected = Utils.TestHelper.GetAllDeliveryMethodsExpected();
        //    //Act
        //    var objGetAllDeliveryMethod = objCheckOutcontroller.GetDeliveryMethods();
        //    //Assert
        //    foreach (var objAllDelivery in objGetAllDeliveryMethod.DeliveryMethods)
        //    {
        //        var objAllDeliveryExpected =
        //            objAllDeliveryMethodExpected.FirstOrDefault(objDelivery => objDelivery.Id == objAllDelivery.Id);
        //        if (objAllDeliveryExpected != null)
        //        {
        //            Assert.IsTrue(objAllDelivery.Id == objAllDeliveryExpected.Id, "Delivery Method not matched");
        //        }
        //        else
        //        {
        //            Assert.IsTrue(false, "Delivery method " + objAllDelivery.Id + " Not Found in expected result");

        //        }
        //    }
        //}

        #endregion
    }
}
