﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AFM.Commerce.UnitTests
{
    using AFM.Commerce.Framework.Extensions.Controllers;
    using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Utils;
    using System.Collections.Generic;

    [TestClass]

    #region SubmitOrder Unit Tests

    public class SubmitOrderUnitTests
    {
        AFMCheckoutController objAFMCheckoutController = new AFMCheckoutController();
        [TestMethod]
        //Test Method for verifiying SubmitOrder from CRT end. currently written for TestConnector. Follow instructions to swap for Ashley Connector.
        public void SubmitOrder()
        {
            TestHelper.CreateCheckOutDataXmlFile();
            var checkOutCart = Utils.TestHelper.CreateCheckOutShoppingCart();
            var shippingOption = TestHelper.CreateShippingOption();
            checkOutCart = objAFMCheckoutController.SetOrderDeliveryOption(checkOutCart.CartId, "", shippingOption,
                TestHelper.ProductValidator, TestHelper.SearchEngine, ShoppingCartDataLevel.All);
            var tlines = new List<TenderDataLine> { TestHelper.CreateTenderDataLine };
            //For Ashley Connector Only, set debug point here and then create a transaction or use a avaialble transaction id here to use Connector result.
            //var cardtokenData = PaymentController.GetCardToken("");
            // var tlines = new List<TenderDataLine> { TestHelper.UpdateTenderDataLine(cardtokenData) };
            var ordernumber = objAFMCheckoutController.CreateOrder(checkOutCart.CartId, "", TestHelper.cardToken, tlines.AsReadOnly(),
                new Entities.AFMSalesTransHeader(), TestHelper.AddressData.Email, "", TestHelper.ProductValidator, TestHelper.SearchEngine);
            Assert.IsNotNull(ordernumber);
        }

        [TestMethod]
        public void GetCardToken()
        {
            var response = AFMPaymentController.GetCardToken(TestHelper.GetTranid());
            Assert.IsNotNull(response);
        }
    }

    # endregion
}
