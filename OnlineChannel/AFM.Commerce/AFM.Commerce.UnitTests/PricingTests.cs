﻿using System;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using AFM.Commerce.Framework.Extensions.Controllers;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;

namespace AFM.Commerce.UnitTests
{
    [TestClass]
    public class PricingTests
    {
        AFMPricingController objAFMPricingController = new AFMPricingController();
        [TestInitialize]
        public void InitTest()
        {
            Utils.TestHelper.CatalogId = 5637145337;
        }

        #region GetAllPrices
        [TestMethod]
        public void GetAllPricesValidItemExpectdAllpriceforAllproducts()
        {
            //Arrange
            List<string> productIds = new List<string>() { "A1000030T" }; //Utils.TestHelper.ProductsIds();

            //Act
            var getAllPrices = objAFMPricingController.GetAllPrices(productIds);

            //Assert
            var expectedResult = Utils.TestHelper.AfmAllAffiliationPrices();

            if (getAllPrices.Count != expectedResult.Count)
                Assert.IsTrue(false, "Number of Affilaction not match with expected result.");
            for (int i = 0; i < getAllPrices.Count; i++)
            {
                var temp = expectedResult.AsEnumerable().FirstOrDefault(frm => frm.ItemId == getAllPrices[i].ItemId);
                if (temp != null)
                {
                    Assert.IsTrue(temp.AffiliationId == getAllPrices[i].AffiliationId, "AffiliationId not Matched " + getAllPrices[i].ItemId);
                    Assert.IsTrue(temp.BestPrice == getAllPrices[i].BestPrice, "Price not Matched " + getAllPrices[i].ItemId);
                }
                else
                    Assert.IsTrue(false, "Item " + getAllPrices[i].ItemId + " is not in excepted Results");
            }
        }

        [TestMethod]
        public void GetPriceSnapshotValidItemExpectedSnapshotforAllproducts()
        {
            //Arrange
            List<string> productIds = new List<string>() { "1102" }; //Utils.TestHelper.ProductsIds();

            //Act
            var getAllPrices = objAFMPricingController.GetPriceSnapshot(productIds, 5637145328);
            var getAllAffPrices = objAFMPricingController.GetPriceSnapshot(productIds);

            //Assert
            var expectedResult = Utils.TestHelper.AfmAllAffiliationPrices();

            //if (getAllPrices.Count != expectedResult.Count)
               // Assert.IsTrue(false, "Number of Affilaction not match with expected result.");
            for (int i = 0; i < getAllPrices.Count; i++)
            {
                var temp = expectedResult.AsEnumerable().FirstOrDefault(frm => frm.ItemId == getAllPrices[i].ItemId);
                if (temp != null)
                {
                    Assert.IsTrue(temp.AffiliationId == getAllPrices[i].AffiliationId, "AffiliationId not Matched " + getAllPrices[i].ItemId);
                    Assert.IsTrue(System.Convert.ToDecimal(temp.BestPrice) == getAllPrices[i].BestPrice, "Price not Matched " + getAllPrices[i].ItemId);
                }
                else
                    Assert.IsTrue(false, "Item " + getAllPrices[i].ItemId + " is not in excepted Results");
            }
        }

        [TestMethod]
        public void GetNotNullFulfillerInformation()
        {

            var getAllFulfillerInformation = objAFMPricingController.GetAllFulfillerInformation();
        }

        [TestMethod]
        public void GetAllPricesNapItemNoPriceGroupAttachedtoStoreNoDiscountApply()
        {
            //Arrange
            List<string> productIds = new List<string>() { "A1000030T" }; //Utils.TestHelper.ProductsIds();            
            var expPrice = Utils.TestHelper.getItemBasePrice(productIds[0]);
            Utils.TestHelper.RemovePriceGroupChannelId();

            //Act
            var getAllPrices = objAFMPricingController.GetAllPrices(productIds);

            //Assert
            Utils.TestHelper.RestorePriceGroupChannelId();
            Assert.IsNotNull(getAllPrices, "Price Should Not Be Null");
            Assert.IsTrue(getAllPrices.Count != 0, "Price Should Not Be Null");
            if (getAllPrices.Count != 0)
                Assert.IsTrue(Convert.ToDecimal(getAllPrices[0].BestPrice) == expPrice, "Prices No Matched Wiht Base Price");

        }

        [TestMethod]
        public void GetAllPricesNapItemPriceGroupAttachedtoStoreDiscountApply()
        {
            //Arrange            
            List<string> productIds = new List<string>() { "A1000030T" }; //Utils.TestHelper.ProductsIds();            
            var BasePrice = Utils.TestHelper.getItemBasePrice(productIds[0]);

            //Act
            var getAllPrices = objAFMPricingController.GetAllPrices(productIds);

            //Assert            
            Assert.IsNotNull(getAllPrices, "Price Should Not Be Null");
            Assert.IsTrue(getAllPrices.Count != 0, "Price Should Not Be Null");
            if (getAllPrices.Count != 0)
                Assert.IsTrue(Convert.ToDecimal(getAllPrices[0].BestPrice) <= BasePrice, Convert.ToDecimal(getAllPrices[0].BestPrice) + " Prices No Matched Wiht Base Price" + BasePrice);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetAllPricesNullProductsIdsArgumentNullExpected()
        {
            //Arrange            

            //Act
            objAFMPricingController.GetAllPrices(null);

            //Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetAllPricesInvalidProductIdsExpectedArgumentNullException()
        {
            //Arrange            
            List<string> productIds = Utils.TestHelper.InvalidProductsIds();

            //Act
            var getAllPrices = objAFMPricingController.GetAllPrices(productIds);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void GetAllPricesProductIdsLenghtMorethen50CharExceptionExpected()
        {
            //Arrange            
            List<string> productIds = new List<string> { Utils.TestHelper.InvalidProductsIds()[0].PadRight(55, '0') };

            //Act
            var getAllPrices = objAFMPricingController.GetAllPrices(productIds);

            //Assert
            //Assert.IsTrue(getAllPrices.Count == 0, "Return Wrong price");
        }

        #endregion

        #region GetDiscountedProducts
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetDiscountedProductsNullProductListExpectingNullReferanceException()
        {
            //Arrange
            long channelId = Utils.TestHelper.GetDefaultChannelId();

            //Act
            objAFMPricingController.GetDiscountedProducts(channelId, Utils.TestHelper.CatalogId, null,
                Utils.TestHelper.GetLoyaltyAffilcationTier());
            //Assert
        }

        [TestMethod]
        public void GetDiscountedProductsNullAffiliationLoyaltyTierNoException()
        {
            //Arrange
            long channelId = Utils.TestHelper.GetDefaultChannelId();

            //Act
            objAFMPricingController.GetDiscountedProducts(channelId, Utils.TestHelper.CatalogId, Utils.TestHelper.GetProductCollaction(),
                null);
            //Assert
        }


        [TestMethod]
        public void GetDiscountedProductsValidItemNoAffiliationLoyaltyTier()
        {
            //Arrange
            long channelId = Utils.TestHelper.GetDefaultChannelId();

            //Act
            var productList = objAFMPricingController.GetDiscountedProducts(channelId, Utils.TestHelper.CatalogId, Utils.TestHelper.GetProductCollaction(new List<string> { "A1000030T" }),
                new List<AffiliationLoyaltyTier>());

            //Assert
            Assert.IsNotNull(productList, "Product List Should not be Null");
        }

        [TestMethod]
        public void GetDiscountedProductsValidItemAffiliationLoyaltyTierDefaultAfflicaton()
        {
            //Arrange
            long channelId = Utils.TestHelper.GetDefaultChannelId();
            var affliationList = Utils.TestHelper.GetLoyaltyAffilcationTier();

            //Act
            var productList = objAFMPricingController.GetDiscountedProducts(channelId, Utils.TestHelper.CatalogId,
                Utils.TestHelper.GetProductCollaction(new List<string> { "A1000030T" }),
                affliationList);

            //Assert
            Assert.IsNotNull(productList, "Product List Should not be Null");
            Assert.IsTrue(productList[0].AdjustedPrice != productList[0].BasePrice);
        }

        #endregion

        #region GetAffiliationByZipCode
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetAffiliationByZipCodeNullZipcodeExpectedDefaultAffiliation()
        {
            //Arrange

            //Act
            var objAfmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(null);

            //Assert
            Assert.IsTrue(objAfmVendorAffiliation.AffiliationId != null, "Default affiliation not configured");
        }

        [TestMethod]
        public void GetAffiliationByZipCodeValidZipcodeAffiliationExpected()
        {
            //Arrange
            Utils.TestHelper.Zipcode = "10104";
            var objAffilicationExpected = Utils.TestHelper.GetAffiliactionByZipCodeStoreProc(Utils.TestHelper.Zipcode);

            //Act
            var objAfmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(Utils.TestHelper.Zipcode);

            //Assert
            Assert.IsTrue(objAfmVendorAffiliation != null, "Return Null Affiliaction");
            Assert.IsTrue(objAfmVendorAffiliation.AffiliationId == objAffilicationExpected.AffiliationId, "AffiliationId Not matched");
            Assert.IsTrue(objAfmVendorAffiliation.InvoiceVendorId == objAffilicationExpected.InvoiceVendorId, "InvoiceVendorId Not matched");
            Assert.IsTrue(objAfmVendorAffiliation.ShipToId == objAffilicationExpected.ShipToId, "ShipToId Not matched");
            Assert.IsTrue(objAfmVendorAffiliation.VendorId == objAffilicationExpected.VendorId, "VendorId Not matched");
        }

        [TestMethod]
        public void GetAffiliationByZipCodeInValidZipcodeDefalutAffiliationExpected()
        {
            //Arrange
            Utils.TestHelper.Zipcode = "1010400";
            //var objAffilicationExpected = Utils.TestHelper.GetDefalutAffiliactionStoreProc();
            var objAffilicationExpected = Utils.TestHelper.GetAffiliactionByZipCodeStoreProc(Utils.TestHelper.Zipcode);

            //Act
            var objAfmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(Utils.TestHelper.Zipcode);

            //Assert
            Assert.IsTrue(objAfmVendorAffiliation != null, "Return Null Affiliaction");
            Assert.IsTrue(objAfmVendorAffiliation.AffiliationId == objAffilicationExpected.AffiliationId, "AffiliationId Not matched");
            Assert.IsTrue(objAfmVendorAffiliation.InvoiceVendorId == objAffilicationExpected.InvoiceVendorId, "InvoiceVendorId Not matched");
            Assert.IsTrue(objAfmVendorAffiliation.ShipToId == objAffilicationExpected.ShipToId, "ShipToId Not matched");
            Assert.IsTrue(objAfmVendorAffiliation.VendorId == objAffilicationExpected.VendorId, "VendorId Not matched");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void GetAffiliationByZipCodeValidZipcodeLenghtMorethan50LengthExcepation()
        {
            //Arrange
            Utils.TestHelper.Zipcode = "10104";
            var ZipCodeWith250Char = Utils.TestHelper.Zipcode.PadLeft(50, '0');

            //Act
            var objAfmVendorAffiliation = AFMAffiliationController.GetAffiliationByZipCode(ZipCodeWith250Char);

            //Assert           
        }

        [TestMethod]
        public void GetAffiliationByZipCodeOnSecondZipcode()
        {
            //string cookieName = AFM.Commerce.UnitTests.Utils.CommonConstants.AFMZipCode;
            //Arrange
            Utils.TestHelper.Zipcode = "10104";

            var objAffilicationExpectedOnSecondZipcode = Utils.TestHelper.GetAffiliactionByZipCodeStoreProc(Utils.TestHelper.Zipcode);

            //Act
            var objAfmVendorAffiliatiOnSecondZipCode = AFMAffiliationController.GetAffiliationByZipCode(Utils.TestHelper.Zipcode);

            //Assert
            Assert.IsTrue(objAfmVendorAffiliatiOnSecondZipCode.AffiliationId == objAffilicationExpectedOnSecondZipcode.AffiliationId, "AffiliationId  different for zipcode");
        }

        #endregion

        #region GetAffiliationsByProduct
        [TestMethod]
        public void GetAffiliationsByProductValidProductID()
        {
            //Arrange            
            var objAffilicationExpected = Utils.TestHelper.GetAffiliactionsByProductIdsStoreProc(Utils.TestHelper.ProductsRecIds()[0]);

            //Act
            var objAfmVendorAffiliations = AFMAffiliationController.GetAffiliationsByProduct(Utils.TestHelper.ProductsRecIds()[0].ToString()).AfmAffiliations;

            //Assert
            Assert.IsTrue(objAfmVendorAffiliations != null, "Return Null Affiliaction");
            Assert.IsTrue(objAfmVendorAffiliations.Count == objAffilicationExpected.Count, "Affilication Count not matched");
            foreach (var objtemp in objAfmVendorAffiliations)
            {
                var objtempExpected = objAffilicationExpected.FirstOrDefault(tmp => tmp.AffiliationId == objtemp.AffiliationId);
                if (objtempExpected != null)
                {
                    Assert.IsTrue(objtemp.AffiliationId == objtempExpected.AffiliationId, "AffiliationId Not matched");
                    Assert.IsTrue(objtemp.InvoiceVendorId == objtempExpected.InvoiceVendorId, "InvoiceVendorId Not matched");
                    Assert.IsTrue(objtemp.ShipToId == objtempExpected.ShipToId, "ShipToId Not matched");
                    Assert.IsTrue(objtemp.VendorId == objtempExpected.VendorId, "VendorId Not matched");
                }
                else
                    Assert.IsTrue(false, "AffiliationId " + objtemp.AffiliationId + "not found in expected Result");
            }

        }

        [TestMethod]
        public void GetAffiliationsByProductValidInvalidProductIDNullAffilicationsExpected()
        {
            //Arrange                        

            //Act
            var objAfmVendorAffiliations = AFMAffiliationController.GetAffiliationsByProduct("0000000000000").AfmAffiliations;

            //Assert
            Assert.IsTrue(objAfmVendorAffiliations.Count == 0, "Affiliaction Shuould for invalid productids");
        }

        #endregion

        #region GetPriceByAffiliation
        [TestMethod]
        public void GetPriceByAffiliationZeroAffilicationIsNoAffilicationPriceExpected()
        {
            //Arrange

            //Act
            var objAffiliactionPrice = objAFMPricingController.GetPriceByAffiliation(0, Utils.TestHelper.ProductsIds());

            //Assert
            Assert.IsTrue(objAffiliactionPrice != null, "Has Pricing Values");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetPriceByAffiliationNullProductIsExpectedNullReferanceException()
        {
            //Arrange

            //Act
            var objAffiliactionPrice = objAFMPricingController.GetPriceByAffiliation(Utils.TestHelper.GetDefalutAffiliactionStoreProc().AffiliationId, null);

            //Assert           
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetPriceByAffiliationInvalidProductIdsExpectedZeroAffilicationPriceCount()
        {
            //Arrange

            //Act
            var objAffiliactionPrice = objAFMPricingController.GetPriceByAffiliation(Utils.TestHelper.GetDefalutAffiliactionStoreProc().AffiliationId, Utils.TestHelper.InvalidProductsIds());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetPriceByAffiliationProductIds50LengthExpectedArgumentNullException()
        {
            //Arrange
            var productIds = new List<string>(){ 
                Utils.TestHelper.InvalidProductsIds()[0].PadRight(50,'0')
            };

            //Act
            var objAffiliactionPrice = objAFMPricingController.GetPriceByAffiliation(Utils.TestHelper.GetDefalutAffiliactionStoreProc().AffiliationId, productIds);

            //Assert            
        }

        [TestMethod]
        public void GetPriceByAffiliationPassAllParamter()
        {
            //Arrange
            var objAffilicationPriceExpcted = Utils.TestHelper.CreateAfmAffiliationPriceFromXml();

            //Act
            var objAffiliactionPrice = objAFMPricingController.GetPriceByAffiliation(
                Utils.TestHelper.GetDefalutAffiliactionStoreProc().AffiliationId,
                Utils.TestHelper.ProductsIds(), Utils.TestHelper.GetProductCollaction());

            //var bojxml = Utils.TestHelper.CreateXml(objAffiliactionPrice);
            //Assert  
            Assert.IsTrue(objAffiliactionPrice != null, "Affilication price is null");
            Assert.IsTrue(objAffilicationPriceExpcted.Count == objAffiliactionPrice.Count, "Row not Matched");
            foreach (var tempres in objAffiliactionPrice)
            {
                var tempobj = objAffilicationPriceExpcted.FirstOrDefault(temp => temp.ItemId == tempres.ItemId);
                if (tempobj != null)
                {
                    Assert.IsTrue(tempres.AffiliationId == tempobj.AffiliationId, "AffiliationId Not matched for" + tempobj.ItemId);
                    Assert.IsTrue(tempres.RegularPrice == tempobj.RegularPrice, "RegularPrice Not matched for" + tempobj.ItemId);
                    Assert.IsTrue(tempres.SalePrice == tempobj.SalePrice, "SalePrice Not matched for" + tempobj.ItemId);
                }
                else
                {
                    Assert.IsTrue(false, "Item " + tempres.ItemId + " Price not found in Expected Result");
                }
            }
        }

        [TestMethod]
        public void GetPriceByAffiliationPassAllParamterWithoutDefaultAfflitiationId()
        {
            //Arrange
            Utils.TestHelper.Zipcode = "10104";
            var objAffilicationPriceExpcted = Utils.TestHelper.CreateAfmAffiliationPriceFromXml();
            var objGetAffiliationByZipCode = AFMAffiliationController.GetAffiliationByZipCode(Utils.TestHelper.Zipcode);

            //Act
            var objAffiliactionPrice = objAFMPricingController.GetPriceByAffiliation(objGetAffiliationByZipCode.AffiliationId
                , Utils.TestHelper.ProductsIds(), Utils.TestHelper.GetProductCollaction());

            var bojxml = Utils.TestHelper.CreateXml(objAffiliactionPrice);
            //Assert  
            Assert.IsTrue(objAffiliactionPrice != null, "Affilication price is null");
            Assert.IsTrue(objAffilicationPriceExpcted.Count == objAffiliactionPrice.Count, "Row not Matched");
            foreach (var tempres in objAffiliactionPrice)
            {
                var tempobj = objAffilicationPriceExpcted.FirstOrDefault(temp => temp.ItemId == tempres.ItemId);
                if (tempobj != null)
                {
                    Assert.IsTrue(tempres.AffiliationId == tempobj.AffiliationId, "AffiliationId Not matched for" + tempobj.ItemId);
                    Assert.IsTrue(tempres.RegularPrice == tempobj.RegularPrice, "RegularPrice Not matched for" + tempobj.ItemId);
                    Assert.IsTrue(tempres.SalePrice == tempobj.SalePrice, "SalePrice Not matched for" + tempobj.ItemId);
                }
                else
                {
                    Assert.IsTrue(false, "Item " + tempres.ItemId + " Price not found in Expected Result");
                }
            }
        }

        [TestMethod]
        public void GetPriceByAffiliationPassAllParamterWithSecondAfflitionId()
        {
            //Arrange
            Utils.TestHelper.Zipcode = "10104";
            var objAffilicationPriceExpcted = Utils.TestHelper.CreateAfmAffiliationPriceDetailFromXml();
            var objGetAffiliationByZipCode = AFMAffiliationController.GetAffiliationByZipCode(Utils.TestHelper.Zipcode);

            //Act
            var objAffiliactionPrice = objAFMPricingController.GetPriceByAffiliation(objGetAffiliationByZipCode.AffiliationId
                , Utils.TestHelper.ProductsIds(), Utils.TestHelper.GetProductCollaction());

            var bojxml = Utils.TestHelper.CreateXml(objAffiliactionPrice);
            //Assert  
            Assert.IsTrue(objAffiliactionPrice != null, "Affilication price is null");
            Assert.IsTrue(objAffilicationPriceExpcted.Count == objAffiliactionPrice.Count, "Row not Matched");
            foreach (var tempres in objAffiliactionPrice)
            {
                var tempobj = objAffilicationPriceExpcted.FirstOrDefault(temp => temp.ItemId == tempres.ItemId);
                if (tempobj != null)
                {
                    Assert.IsTrue(tempres.AffiliationId == tempobj.AffiliationId, "AffiliationId Not matched for" + tempobj.ItemId);
                    Assert.IsTrue(tempres.RegularPrice == tempobj.RegularPrice, "RegularPrice Not matched for" + tempobj.ItemId);
                    Assert.IsTrue(tempres.SalePrice == tempobj.SalePrice, "SalePrice Not matched for" + tempobj.ItemId);
                }
                else
                {
                    Assert.IsTrue(false, "Item " + tempres.ItemId + " Price not found in Expected Result");
                }
            }
        }

        #endregion
    }
}
