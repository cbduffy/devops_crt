﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace AFM.Commerce.UnitTests.Utils
{
    /// <summary>
    /// Centralized location for managing all of the constants.
    /// </summary>
    public static class CommonConstants
    {
        /// <summary>
        /// Represents the name of the search property that contains the item's title.
        /// </summary>
        public static readonly string TitleSearchPropertyName = "Title";

        /// <summary>
        /// Represents the name of the search property that contains the item's configuration.
        /// </summary>
        public static readonly string ConfigurationSearchPropertyName = "RetConfiguration";

        /// <summary>
        /// Represents the name of the search property that contains the item's size.
        /// </summary>
        public static readonly string SizeSearchPropertyName = "Size";

        /// <summary>
        /// Represents the name of the search property that contains the item's color.
        /// </summary>
        public static readonly string ColorSearchPropertyName = "Color";

        /// <summary>
        /// Represents the name of the search property that contains the item's style.
        /// </summary>
        public static readonly string StyleSearchPropertyName = "Style";

        /// <summary>
        /// Represents the name of the search property that contains the item's image.
        /// </summary>
        public static readonly string ImageSearchPropertyName = "Image";

        /// <summary>
        /// Represents the name of the search property that contains the item's path.
        /// </summary>
        public static readonly string PathSearchPropertyName = "Path";

        /// <summary>
        /// Represents the name of the search property that contains the item's description.
        /// </summary>
        public static readonly string DescriptionSearchPropertyName = "Description";
    }
}