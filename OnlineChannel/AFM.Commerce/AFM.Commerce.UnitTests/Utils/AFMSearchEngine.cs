﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;

namespace AFM.Commerce.UnitTests.Utils
{
    public class AFMSearchEngine : ISearchEngine
    {
        public IDictionary<string, object> GetListingProperties(bool isResultMandatory, long listingRecordId, params string[] propertyNames)
        {
            if (propertyNames == null || propertyNames.Length == 0)
            {
                throw new ArgumentNullException("propertyNames");
            }

            Product product = DemoController.GetProductsByIds(new List<long> { listingRecordId }).Single();
            IDictionary<string, object> listingProperties = new Dictionary<string, object>();

            if (product != null)
            {
                foreach (string propertyName in propertyNames)
                {
                    ProductVariant variant = null;
                    if (propertyName.Equals(CommonConstants.PathSearchPropertyName))
                    {
                        listingProperties[CommonConstants.PathSearchPropertyName] = string.Empty;
                    }
                    else if (propertyName.Equals(CommonConstants.TitleSearchPropertyName))
                    {
                        listingProperties[CommonConstants.TitleSearchPropertyName] = product.ProductName;
                    }
                    else if (propertyName.Equals(CommonConstants.DescriptionSearchPropertyName))
                    {
                        listingProperties[CommonConstants.DescriptionSearchPropertyName] = product.Description;
                    }
                    else if (propertyName.Equals(CommonConstants.ImageSearchPropertyName))
                    {
                        listingProperties[CommonConstants.ImageSearchPropertyName] = string.Empty;
                    }
                    else if (propertyName.Equals(CommonConstants.ConfigurationSearchPropertyName))
                    {
                        variant = GetVariant(product, variant, listingRecordId);
                        listingProperties[CommonConstants.ConfigurationSearchPropertyName] = (variant == null) ? variant.Configuration : string.Empty;
                    }
                    else if (propertyName.Equals(CommonConstants.ColorSearchPropertyName))
                    {
                        variant = GetVariant(product, variant, listingRecordId);
                        listingProperties[CommonConstants.ColorSearchPropertyName] = (variant == null) ? variant.Color : string.Empty;
                    }
                    else if (propertyName.Equals(CommonConstants.SizeSearchPropertyName))
                    {
                        variant = GetVariant(product, variant, listingRecordId);
                        listingProperties[CommonConstants.SizeSearchPropertyName] = (variant == null) ? variant.Size : string.Empty;
                    }
                    else if (propertyName.Equals(CommonConstants.StyleSearchPropertyName))
                    {
                        variant = GetVariant(product, variant, listingRecordId);
                        listingProperties[CommonConstants.StyleSearchPropertyName] = (variant == null) ? variant.Style : string.Empty;
                    }
                }
            }

            if (isResultMandatory && !listingProperties.Any())
            {
                string message = string.Format("Unable to get the requested property details for the given listing {0}, Please try again later", listingRecordId);
                throw new Exception(message);
            }

            return listingProperties;
        }

        public Tuple<string, string, long, string> GetKitVariantProductInfo(long kitMasterProductId, long catalogId, IEnumerable<KitLineProductProperty> kitLines)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<StorefrontListItem> GetKitComponentVariants(long kitComponentProductId, long kitComponentParentId, long parentKitId, long kitComponentLineId, long catalogId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's description.
        /// </summary>
        public string DescriptionSearchPropertyName
        {
            get { return CommonConstants.DescriptionSearchPropertyName; }
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's path.
        /// </summary>
        public string PathSearchPropertyName
        {
            get { return CommonConstants.PathSearchPropertyName; }
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's image.
        /// </summary>
        public string ImageSearchPropertyName
        {
            get { return CommonConstants.ImageSearchPropertyName; }
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's title.
        /// </summary>
        public string TitleSearchPropertyName
        {
            get { return CommonConstants.TitleSearchPropertyName; }
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's color.
        /// </summary>
        public string ColorSearchPropertyName
        {
            get { return CommonConstants.ColorSearchPropertyName; }
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's size.
        /// </summary>
        public string SizeSearchPropertyName
        {
            get { return CommonConstants.SizeSearchPropertyName; }
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's style.
        /// </summary>
        public string StyleSearchPropertyName
        {
            get { return CommonConstants.StyleSearchPropertyName; }
        }

        /// <summary>
        /// Represents the name of the search property that contains the item's configuration.
        /// </summary>
        public string ConfigurationSearchPropertyName
        {
            get { return CommonConstants.ConfigurationSearchPropertyName; }
        }

        /// <summary>
        /// Gets the product variant given the product and listing id if the variant object is null.
        /// </summary>
        /// <param name="product">The product.</param>
        /// <param name="variant">The variant.</param>
        /// <param name="listingRecordId">The listing id.</param>
        /// <returns>The product variant.</returns>
        private static ProductVariant GetVariant(Product product, ProductVariant variant, long listingRecordId)
        {
            return variant ?? product.CompositionInformation.VariantInformation.Variants.Where(v => Convert.ToInt64(v.VariantId) == listingRecordId).Single();
        }

        public IDictionary<long, IDictionary<string, object>> GetListingsProperties(bool isResultMandatory, IEnumerable<long> listingRecordIds, params string[] propertyNames)
        {
            throw new NotImplementedException();
        }

        public ImageInfo UpdateProductImageData(RichMediaLocations imageSearchValue)
        {
            throw new NotImplementedException();
        }
    }
}
