﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using AFM.Commerce.Framework.Extensions.Controllers;
using AFM.Commerce.Entities;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Client;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Controllers;
using AFM.Commerce.Framework.Core.Models;
using Address = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Address;
using AddressType = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.AddressType;
using Listing = Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.Listing;
using System.Web;
using AFM.Commerce.Framework.Extensions.Utils;
using System.Web.Hosting;
using System.Web.SessionState;
using System.Reflection;



namespace AFM.Commerce.UnitTests.Utils
{
    public static class TestHelper
    {
        private static readonly string CheckOutFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("Common\\DemoData", "CheckoutData.xml"));
        private static readonly string FilePathItemPrice = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("Data", "ItemPrice.xml"));
        private static readonly string FilePathShoppingCart = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("Data", "ShoppingCart.xml"));
        private static readonly string FilePathProdcutAffilicationPrice = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("Data", "ItemPriceAffiliaction.xml"));
        private static readonly string FilePathProdcutAffilicationPriceDetail = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Path.Combine("Data", "ItemPriceAffiliactionDetail.xml"));

        public const string cardToken = "45";

        private static AFMShoppingCartController objafmshoppingCartController = new AFMShoppingCartController();
        private static ShoppingCartController objShoppingCartController = new ShoppingCartController();

        public static CommerceRuntime CommerceRuntime
        {
            get { return CrtUtilities.GetCommerceRuntime(); }
        }

        public static AFMProductValidator ProductValidator
        {
            get;
            set;
        }
        public static AFMSearchEngine SearchEngine
        {
            get;
            set;
        }

        public static string CustomerId
        {
            get;
            set;
        }

        public static string DefaultCustomerId
        {
            get { return ConfigurationManager.AppSettings["DefaultCustomerId"]; }
        }

        public static string ShoppingCartId
        {
            get { return ShoppingCart.CartId; }
        }

        public static ShoppingCart ShoppingCart { get; set; }

        public static long CatalogId
        {
            get;
            set;
        }

        public static string Zipcode { get; set; }

        public static string CountryRegioncId { get; set; }

        public static string PromotionCode
        {
            get;
            set;
        }

        public static ShoppingCartDataLevel ShoppingCartDataLevel
        {
            get { return ShoppingCartDataLevel.All; }
        }

        public static long GetDefaultChannelId()
        {
            var channelManager = ChannelManager.Create(CommerceRuntime);
            long channelRecId = channelManager.GetDefaultChannelId();
            return channelRecId;
        }

        public static ShoppingCart CreateShoppingCart()
        {
            var itemListing = ItemListing();
            ProductValidator = new AFMProductValidator();

            var objShpoingCart = objafmshoppingCartController.AddItems(string.Empty, CustomerId, itemListing,
                ShoppingCartDataLevel.Minimal, ProductValidator, null, false);

            ShoppingCart = objShpoingCart;

            return objShpoingCart;
        }

        public static ShoppingCart CreateShoppingCartHavingKitItem()
        {
            var itemListing = ItemListing();
            ProductValidator = new AFMProductValidator();
            itemListing.Add((new Listing
            {
                ItemId = "A1000029T",
                ListingId = 5637145328,
                IsKitVariant = false,
                GiftCardAmount = 0,
                Quantity = 1,
                ProductDetails = "Throw"
            }));

            var objShpoingCart = objafmshoppingCartController.AddItems(string.Empty, CustomerId, itemListing,
                ShoppingCartDataLevel.Minimal, ProductValidator, SearchEngine, false);

            return objShpoingCart;
        }

        public static void RemoveShoppingCart()
        {
            var shoppingCartIds = new List<string> { ShoppingCart.CartId };
            //objShoppingCartController.DeleteShoppingCarts(shoppingCartIds, CustomerId);
        }

        public static void RemoveShoppingCart(String Cartid, String CustomerId)
        {
            var shoppingCartIds = new List<string> { Cartid };
            //objShoppingCartController.DeleteShoppingCarts(shoppingCartIds, CustomerId);
        }

        public static Address ShippingAddress
        {
            get
            {
                return new Address
                {
                    AddressFriendlyName = "",
                    AddressType = AddressType.Delivery,
                    AttentionTo = "",
                    City = "",
                    Country = "",
                    County = "",
                    Deactivate = false,
                    DistrictName = "",
                    Email = "",
                    EmailContent = "",
                    IsPrimary = true,
                    Name = "",
                    Phone = "",
                    RecordId = 1212121,
                    State = "",
                    Street = "",
                    Street2 = "",
                    StreetNumber = "",
                    ZipCode = ""
                };
            }
        }

        public static List<Listing> ItemListing()
        {
            return new List<Listing>
            {
                //(new Listing
                //{
                //        ItemId = "1040021",
                //        ListingId = 5637145327,
                //        IsKitVariant = false,
                //        GiftCardAmount = 0,
                //        Quantity = 1,
                //        ProductDetails = "Chair"
                //}),
                (new Listing
                {
                        ItemId = "1040021",
                        ListingId = 5637145327,
                        IsKitVariant = false,
                        GiftCardAmount = 0,
                        Quantity = 1,
                        ProductDetails = "Chair"
                    })
                //    ,
                //(new Listing
                //{
                //        ItemId = "A1000029T",
                //        ListingId = 5637145328,
                //        IsKitVariant = false,
                //        GiftCardAmount = 0,
                //        Quantity = 1,
                //        ProductDetails = "Throw"
                //})                
                ////,
            };
        }

        public static List<TransactionItem> GetShopingCartItems()
        {
            var shoppingCartItem = ShoppingCart.Items[0];
            shoppingCartItem.Quantity = 10;
            return new List<TransactionItem> { shoppingCartItem };
        }

        public static List<string> GetShopingCartItemLineIds()
        {
            var shoppingCartItem = ShoppingCart.Items[0];
            return new List<string> { shoppingCartItem.LineId };
        }

        public static void CreateCheckOutDataXmlFile()
        {
            const string checkoutXmlFileData = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                                               "<Checkout>" +
                                               "<Address>" +
                                               "<Name>Jane Doe</Name>" +
                                               "<Street>1290 Avenue of Americas</Street>" +
                                               "<City>New York</City>" +
                                               "<State>NY</State>" +
                                               "<Zipcode>10104</Zipcode>" +
                                               "</Address>" +
                                               "<Email>JaneDoe@demo.com</Email>" +
                                               "<Payment>" +
                                               "<CardNumber>4159282222222221</CardNumber>" +
                                               "<CCID>210</CCID>" +
                                               "</Payment>" +
                                               "</Checkout>";
            const string dirName = "Common\\DemoData";
            if (!Directory.Exists(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dirName)))
            {
                Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, dirName));
            }
            var objCheckOutFileStream = File.Create(CheckOutFilePath);
            var objByteArr = new UTF8Encoding(true).GetBytes(checkoutXmlFileData);
            objCheckOutFileStream.Write(objByteArr, 0, objByteArr.Length);
            objCheckOutFileStream.Close();
        }

        public static void RemoveCheckOutDataxmlFile()
        {
            if (File.Exists(CheckOutFilePath))
                File.Delete(CheckOutFilePath);
        }

        public static List<string> ProductsIds()
        {
            return new List<string> { "A1000030T", "APK-W271-CFP" };
        }

        public static List<long> ProductsRecIds()
        {
            return new List<long> { 5637150502, 5637146281 };
        }

        public static List<string> InvalidProductsIds()
        {
            return new List<string> { "11060000", "00001102", "000001103" };
        }


        public static List<AFMAllAffiliationPrices> AfmAllAffiliationPrices()
        {
            var serializer = new XmlSerializer(typeof(List<AFMAllAffiliationPrices>));

            List<AFMAllAffiliationPrices> tmpAfmAllAffiliationPricesList;
            using (FileStream stream = File.OpenRead(FilePathItemPrice))
            {
                tmpAfmAllAffiliationPricesList = (List<AFMAllAffiliationPrices>)serializer.Deserialize(stream);
            }
            return tmpAfmAllAffiliationPricesList;
        }

        public static ReadOnlyCollection<Product> GetProductCollaction()
        {
            var prodManager = ProductManager.Create(CommerceRuntime);
            var channelManager = ChannelManager.Create(CommerceRuntime);
            var plc = new List<ProductLookupClause>();
            foreach (var productId in ProductsIds())
            {
                plc.Add(new ProductLookupClause { ItemId = productId });
            }
            var criteria = new ProductSearchCriteria(channelManager.GetDefaultChannelId())
            {
                DataLevel = CommerceEntityDataLevel.Complete,
                ItemIds = plc
            };
            var products = prodManager.SearchProducts(criteria, new QueryResultSettings()).Results;
            return products;
        }

        public static ReadOnlyCollection<Product> GetProductCollaction(List<string> ProductIds)
        {
            var prodManager = ProductManager.Create(CommerceRuntime);
            var channelManager = ChannelManager.Create(CommerceRuntime);
            var plc = new List<ProductLookupClause>();
            foreach (var productId in ProductIds)
            {
                plc.Add(new ProductLookupClause { ItemId = productId });
            }
            var criteria = new ProductSearchCriteria(channelManager.GetDefaultChannelId())
            {
                DataLevel = CommerceEntityDataLevel.Complete,
                ItemIds = plc
            };
            var products = prodManager.SearchProducts(criteria, new QueryResultSettings()).Results;
            return products;
        }

        public static List<AffiliationLoyaltyTier> GetLoyaltyAffilcationTier()
        {
            var tmpAffiliationLoyaltyTierList = new List<AffiliationLoyaltyTier>() { 
                new AffiliationLoyaltyTier { 
                    AffiliationId = GetDefalutAffiliactionStoreProc().AffiliationId, 
                    AffiliationType = RetailAffiliationType.General 
                } 
            };
            return tmpAffiliationLoyaltyTierList;
        }

        public static List<long> GetLoyaltyAffilcationTierIds()
        {
            var tmpAffiliationLoyaltyTierList = new List<long>
            { 
                5637145326
            };
            return tmpAffiliationLoyaltyTierList;
        }

        public static Cart GetCartFromCartId(string shoppingCartId)
        {
            OrderManager manager = OrderManager.Create(CommerceRuntime);
            var cart = manager.GetCart(shoppingCartId, CalculationModes.None);
            return cart;
        }

        public static ShoppingCart CreateShoppingCartFromXml()
        {
            var fs = new FileStream(FilePathShoppingCart, FileMode.Open);

            System.Xml.XmlDictionaryReader reader =
                 System.Xml.XmlDictionaryReader.CreateTextReader(fs, new System.Xml.XmlDictionaryReaderQuotas());
            var ser = new DataContractSerializer(typeof(ShoppingCart));

            // Deserialize the data and read it from the instance.
            var deserializedShoppingCart =
                (ShoppingCart)ser.ReadObject(reader, true);
            reader.Close();
            fs.Close();
            return deserializedShoppingCart;
        }

        public static ReadOnlyCollection<AfmAffiliationPrice> CreateAfmAffiliationPriceFromXml()
        {
            var fs = new FileStream(FilePathProdcutAffilicationPrice, FileMode.Open);

            System.Xml.XmlDictionaryReader reader =
                 System.Xml.XmlDictionaryReader.CreateTextReader(fs, new System.Xml.XmlDictionaryReaderQuotas());
            var ser = new DataContractSerializer(typeof(ReadOnlyCollection<AfmAffiliationPrice>));

            // Deserialize the data and read it from the instance.
            var deserializedShoppingCart =
                (ReadOnlyCollection<AfmAffiliationPrice>)ser.ReadObject(reader, true);
            reader.Close();
            fs.Close();
            return deserializedShoppingCart;
        }

        public static string CreateXml(Object yourClassObject)
        {
            var xmlDoc = new System.Xml.XmlDocument();
            // Initializes a new instance of the XmlDocument class.          
            var xmlSerializer = new DataContractSerializer(yourClassObject.GetType());
            // Creates a stream whose backing store is memory. 
            using (var xmlStream = new MemoryStream())
            {
                xmlSerializer.WriteObject(xmlStream, yourClassObject);
                xmlStream.Position = 0;
                //Loads the XML document from the specified string.
                xmlDoc.Load(xmlStream);
                return xmlDoc.InnerXml;
            }
        }

        public static AFMProductDetails GetProductSyncProductDetail()
        {
            var productRecIdList = ProductsRecIds();
            DataTable dtProduct = new DataTable();
            dtProduct.Columns.Add("RecId");
            foreach (var item in productRecIdList)
            {
                var dr = dtProduct.NewRow();
                dr["Recid"] = item;
                dtProduct.Rows.Add(dr);
            }
            var coll = new List<SqlParameter>();
            var parm = new SqlParameter();
            parm.ParameterName = "@tvp_ProductIds";
            parm.SqlDbType = SqlDbType.Structured;
            parm.Value = dtProduct;
            coll.Add(parm);
            parm = new SqlParameter();
            parm.ParameterName = "@dt_ChannelDate";
            parm.SqlDbType = SqlDbType.Date;
            parm.Value = DateTime.Now;
            coll.Add(parm);
            DataSet ds = TransactionHelper.FillDataSetStoreProcedure("crt.AFMGETPRODUCTDETAILS", coll);
            var objItemCovers = ds.Tables[0].AsEnumerable().Select(row =>
                    new AFMItemCovers
                    {
                        ProductNumber = row.Field<string>("DISPLAYPRODUCTNUMBER"),
                        ProductRecordId = row.Field<long>("PRODUCTID")
                    }).ToList();
            var objProductCountryMap = ds.Tables[1].AsEnumerable().Select(row =>
                    new AFMProductCountryMap
                    {
                        ProductNumber = row.Field<string>("PRODUCTNUMBER"),
                        ProductRecordId = row.Field<long>("PRODUCTID")
                    }).ToList();
            var objUpCrossSell = ds.Tables[2].AsEnumerable().Select(row =>
                    new AFMUpCrossSell
                    {
                        ProductNumber = row.Field<string>("PRODUCTNUMBER"),
                        ProductRecordId = row.Field<long>("PRODUCTID")
                    }).ToList();
            var objAlsoKnownAs = ds.Tables[3].AsEnumerable().Select(row =>
                    new AFMAlsoKnownAs
                    {
                        ProductNumber = row.Field<string>("PRODUCTNUMBER"),
                        ProductRecordId = row.Field<long>("PRODUCTID")
                    }).ToList();
            var objECommOwnedData = ds.Tables[4].AsEnumerable().Select(row =>
                    new AFMECommOwnedData
                    {
                        ProductNumber = row.Field<string>("PRODUCTNUMBER"),
                        ProductRecordId = row.Field<long>("PRODUCTID")
                    }).ToList();
            return new AFMProductDetails
            {
                AlsoKnownAs = objAlsoKnownAs,
                ItemCovers = objItemCovers,
                ECommOwnedData = objECommOwnedData,
                ProductCountryMap = objProductCountryMap,
                UpCrossSell = objUpCrossSell
            };
        }

        public static AFMVendorAffiliation GetAffiliactionByZipCodeStoreProc(String ZipCode)
        {
            var coll = new List<SqlParameter>();
            var parm = new SqlParameter();
            parm.ParameterName = "@ChannelId";
            parm.SqlDbType = SqlDbType.NVarChar;
            parm.Value = GetDefaultChannelId();
            coll.Add(parm);
            parm = new SqlParameter();
            parm.ParameterName = "@ZipCode";
            parm.SqlDbType = SqlDbType.NVarChar;
            parm.Value = ZipCode;
            coll.Add(parm);
            var ds = TransactionHelper.FillDataTableStoreProcedure("crt.GETAFFILIATIONIDBYZIPCODE", coll);
            if (ds.Rows.Count != 0)
                return new AFMVendorAffiliation
                {
                    AffiliationId = Convert.ToInt64(ds.Rows[0]["AffiliationId"]),
                    InvoiceVendorId = Convert.ToString(ds.Rows[0]["InvoiceVendorId"]),
                    ShipToId = Convert.ToString(ds.Rows[0]["ShipToId"]),
                    VendorId = Convert.ToString(ds.Rows[0]["VendorId"]),
                };
            else
                return null;
        }

        public static AFMVendorAffiliation GetDefalutAffiliactionStoreProc()
        {
            var coll = new List<SqlParameter>();
            var parm = new SqlParameter();
            parm.ParameterName = "@ChannelId";
            parm.SqlDbType = SqlDbType.NVarChar;
            parm.Value = GetDefaultChannelId();
            coll.Add(parm);
            var ds = TransactionHelper.FillDataTableStoreProcedure("crt.GETDEFAULTAFFILIATIONIDBYCHANNEL", coll);
            if (ds.Rows.Count != 0)
                return new AFMVendorAffiliation
                {
                    AffiliationId = Convert.ToInt64(ds.Rows[0]["AffiliationId"]),
                    InvoiceVendorId = Convert.ToString(ds.Rows[0]["InvoiceVendorId"]),
                    ShipToId = Convert.ToString(ds.Rows[0]["ShipToId"]),
                    VendorId = Convert.ToString(ds.Rows[0]["VendorId"]),
                };
            else
                return null;
        }

        public static List<AFMVendorAffiliation> GetAffiliactionsByProductIdsStoreProc(long _ProductRecid)
        {
            var coll = new List<SqlParameter>();
            var parm = new SqlParameter();
            parm.ParameterName = "@ChannelId";
            parm.SqlDbType = SqlDbType.NVarChar;
            parm.Value = GetDefaultChannelId();
            coll.Add(parm);
            parm = new SqlParameter();
            parm.ParameterName = "@ProductCode";
            parm.SqlDbType = SqlDbType.NVarChar;
            parm.Value = _ProductRecid;
            coll.Add(parm);
            var ds = TransactionHelper.FillDataTableStoreProcedure("crt.GETAFFILIATIONSBYPRODUCTCODE", coll);
            var objAffiliactionList = ds.AsEnumerable().Select(row =>
                   new AFMVendorAffiliation
                   {
                       AffiliationId = row.Field<long>("AffiliationId"),
                       InvoiceVendorId = row.Field<string>("InvoiceVendorId"),
                       ShipToId = row.Field<string>("ShipToId"),
                       VendorId = row.Field<string>("VendorId")
                   }).ToList();

            return objAffiliactionList;
        }
        
        public static AFMATPRequest GetAFMATPRequest()
        {
            return new Entities.AFMATPRequest
            {
                ItemGroupings
                    = new List<Entities.AFMATPItemGroupingRequest>() { 
                        new AFMATPItemGroupingRequest(){ AccountNumber="4444400",
                            Items=new List<Entities.AFMATPRequestItem>{
                                new  AFMATPRequestItem{ ItemId="A1000029T",Quantity=1,ThirdPartyItem=false}
                               
                            }
                        }
                    }
            };
        }

        public static AFMATPResponse GetAFMATPResponse()
        {
            return new Entities.AFMATPResponse
            {
                ItemGroupings
                    = new List<Entities.AFMATPItemGroupingResponse>() { 
                        new AFMATPItemGroupingResponse(){ AccountNumber="4444400",
                            Items=new List<Entities.AFMATPResponseItem>{
                                new  AFMATPResponseItem{ ItemId="A1000029T",Quantity=1,IsAvailable=true,Message="Usually delivers in 1 to 2 weeks",ShippingSpan="Usually delivers in 1 to 2 weeks"}
                               
                            }
                        }
                    }
            };
        }

        public static string CardToken { get; set; }

        public static string EmailAddress
        {
            get { return "sam@gmail.com"; }
        }

        public static string UserName { get; set; }

        public static List<TenderDataLine> AllTenderdataLines()
        {
            return new List<TenderDataLine>
            {
                (new TenderDataLine
                {
                    Amount = ShoppingCart.TotalAmount,
                    PaymentCard = new Payment
                    {
                        CCID = "434",
                        CardNumber = "4259282222222221",
                        CardType = "Visa",
                        ExpirationMonth = 9,
                        ExpirationYear = 2018,
                        PaymentAddress = new Address(){
                                        Email = "Jon.Dale@test.com",
                                        Country =  "USA",
                                        City = "New York",
                                        Name = "Jon Dale",
                                        ZipCode = "10104",
                                        State = "WA",
                                        Street = "1290, Avenue of americas",
                                    }
                    }

                })
            };
        }

        public static List<Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.DeliveryOption> GetAllDeliveryMethodsExpected()
        {
            var listAllDeliveryMethodObj = new List<Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.DeliveryOption>();
            var ds = TransactionHelper.FillDataSetSelectSqlQuery("SELECT * FROM [crt].[DELIVERYMODESVIEW]", null);
            listAllDeliveryMethodObj = ds.Tables[0].AsEnumerable().Select(row =>
                    new Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models.DeliveryOption
                    {
                        Description = row.Field<string>("TXT"),
                        Id = row.Field<string>("CODE")

                    }).ToList();
            return listAllDeliveryMethodObj;

        }

        //Payment connector TestCase 
        public static ShoppingCart CheckoutCart
        {
            get;
            set;
        }

        public static ShoppingCart CreateCheckOutShoppingCart()
        {
            var itemListing = ItemListing();
            ProductValidator = new AFMProductValidator();

            ShoppingCart = objafmshoppingCartController.AddItems(string.Empty, string.Empty, itemListing,
                ShoppingCartDataLevel.Minimal, ProductValidator, null, true);
            CheckoutCart = objafmshoppingCartController.CommenceCheckout(ShoppingCart.CartId, "", "", ProductValidator, SearchEngine, ShoppingCartDataLevel.Minimal);
            return CheckoutCart;
        }

        public static SelectedDeliveryOption CreateShippingOption()
        {
            return new SelectedDeliveryOption
            {
                DeliveryModeId = "HD",
                DeliveryPreferenceId = "1",
                CustomAddress = AddressData,
                StoreAddress = new StoreProductAvailability() { 
                    StoreId = "HOUSTON",
                }
            };

        }

        private static TenderDataLine _createTenderDataLine;
        public static TenderDataLine CreateTenderDataLine
        {
            get
            {
                _createTenderDataLine = new TenderDataLine
                {
                    Amount = CheckoutCart.TotalAmount,
                    PaymentCard = new Payment
                    {
                        CCID = "434",
                        CardNumber = "4259282222222221",
                        CardType = "Visa",
                        ExpirationMonth = 9,
                        ExpirationYear = 2018,
                        PaymentAddress = AddressData
                    }
                };
                return _createTenderDataLine;
            }
            set
            {
                if (_createTenderDataLine != null && _createTenderDataLine != value)
                    _createTenderDataLine = value;
            }
        }

        public static string GetTranid()
        {
            //return "HCFHTP6A1D068CCMu8jiaORGwAJ52toIUEAQUh7tz0E=";
            return "n+tvdxtPW9N0eAc67Ry/YDSn0fZ5Kr2ZZ3w2jayc2zFKpqaru4GiBRmVV/Yq4d7x";
        }

        public static TenderDataLine UpdateTenderDataLine(AFMCardTokenData cardTokenData)
        {
            CreateTenderDataLine.PaymentCard.CardNumber = "425928222222" + cardTokenData.Last4Digits;
            CreateTenderDataLine.PaymentCard.ExpirationMonth = Convert.ToInt32(cardTokenData.ExpirationMonth);
            CreateTenderDataLine.PaymentCard.ExpirationYear = Convert.ToInt32(cardTokenData.ExpirationYear);
            CreateTenderDataLine.PaymentCard.NameOnCard = cardTokenData.Name;

            return CreateTenderDataLine;
        }

        public static Address AddressData
        {
            get
            {
                return new Address()
                {
                    Email = "test@test.com",
                    Country = "USA",
                    City = "Chicago",
                    FirstName ="Jon",
                    LastName ="Dale",
                    Name = "Jon Dale",
                    ZipCode = "60610",
                    State = "IL",
                    Street = "1241 N Dearborn St",
                    Street2 = "",
                    Phone = "1234567890",
                    AddressType = AddressType.Delivery
                };
            }
        }

        public static decimal getItemBasePrice(string ItemId)
        {
            var ItemPrice = 0m;
            var SqlSelectQuery = "select PRICE from ax.INVENTTABLEMODULE where ITEMID='" + ItemId + "' and MODULETYPE=2";
            var dt = TransactionHelper.FillDataTableSelectSqlQuery(SqlSelectQuery);
            if (dt.Rows.Count != 0)
                ItemPrice = Convert.ToDecimal(dt.Rows[0][0]);

            return ItemPrice;
        }

        public static void RemovePriceGroupChannelId()
        {
            var SqlSelectQuery = "update [ax].[RETAILCHANNELPRICEGROUP] set RETAILCHANNEL=123456789 where RETAILCHANNEL=" + GetDefaultChannelId();
            TransactionHelper.ExecuteNonQuery(SqlSelectQuery);
        }

        public static void RestorePriceGroupChannelId()
        {
            var SqlSelectQuery = "update [ax].[RETAILCHANNELPRICEGROUP] set RETAILCHANNEL=" + GetDefaultChannelId() + " where RETAILCHANNEL=123456789 ";
            TransactionHelper.ExecuteNonQuery(SqlSelectQuery);
        }

        public static ReadOnlyCollection<AfmAffiliationPrice> CreateAfmAffiliationPriceDetailFromXml()
        {
            var fs = new FileStream(FilePathProdcutAffilicationPriceDetail, FileMode.Open);

            System.Xml.XmlDictionaryReader reader =
                 System.Xml.XmlDictionaryReader.CreateTextReader(fs, new System.Xml.XmlDictionaryReaderQuotas());
            var ser = new DataContractSerializer(typeof(ReadOnlyCollection<AfmAffiliationPrice>));

            // Deserialize the data and read it from the instance.
            var deserializedShoppingCart =
                (ReadOnlyCollection<AfmAffiliationPrice>)ser.ReadObject(reader, true);
            reader.Close();
            fs.Close();
            return deserializedShoppingCart;
        }

        public static void AddZipCodetoCookie(string ZipCode)
        {
            AFMDataUtilities.SetAFMZipCodeInCookie(ZipCode);
        }

        public static void CreateCurrentHttpContext()
        {
            var httpRequest = new HttpRequest("", "http://tempuri.org", "");
            var stringWriter = new StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponce);

            var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(),
                                                                 new HttpStaticObjectsCollection(), 10, true,
                                                                 HttpCookieMode.AutoDetect,
                                                                 SessionStateMode.InProc, false);

            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                                                     BindingFlags.NonPublic | BindingFlags.Instance,
                                                     null, CallingConventions.Standard,
                                                     new[] { typeof(HttpSessionStateContainer) },
                                                     null)
                                                .Invoke(new object[] { sessionContainer });

            HttpContext.Current = httpContext;
        }

        public static Address ShippingAddressInit(string firstName, string lastName, string city, string state, string streetLine1, string streetLine2, string Zipcode)
        {
            Address shippingaddress = Utils.TestHelper.AddressData;

            shippingaddress.FirstName = firstName;
            shippingaddress.LastName = lastName;
            shippingaddress.City = city;
            shippingaddress.Country = "US";
            shippingaddress.State = state;
            shippingaddress.Street = streetLine1;
            shippingaddress.Street2 = streetLine2;
            shippingaddress.ZipCode = Zipcode;

            return shippingaddress;
        }

        public static Address BillingAddressInit(string firstName, string lastName, string city, string state, string streetLine1, string streetLine2, string Zipcode)
        {
            Address billingaddress = Utils.TestHelper.AddressData;

            billingaddress.FirstName = firstName;
            billingaddress.LastName = lastName;
            billingaddress.City = city;
            billingaddress.Country = "USA";
            billingaddress.AddressType = AddressType.Invoice;
            billingaddress.State = state;
            billingaddress.Street = streetLine1;
            billingaddress.Street2 = streetLine2;
            billingaddress.ZipCode = Zipcode;

            return billingaddress;
        }

    }
}
