﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using Transaction = System.Transactions;
using System.Configuration;
using System.Data.SqlClient;

namespace AFM.Commerce.UnitTests.Utils
{
    /// <summary>
    /// Transaction helper class contain transaction scop related features 
    /// </summary>
    public static class TransactionHelper
    {
        private const string ConnectionStringName = "CommerceRuntimeConnectionString";
        /// <summary>
        /// return transaction scop
        /// </summary>
        /// <returns>TransactionScope</returns>
        public static Transaction.TransactionScope GetTransactionScop()
        {
            return (new Transaction.TransactionScope(Transaction.TransactionScopeOption.Required,
                new Transaction.TransactionOptions { IsolationLevel = Transaction.IsolationLevel.ReadCommitted }));
        }

        public static DataSet FillDataSetStoreProcedure(String storeProcName)
        {
            return FillDataSetStoreProcedure(storeProcName, null);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public static DataSet FillDataSetStoreProcedure(String storeProcName, List<SqlParameter> parmList)
        {
            var connString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
            var retDataSet = new DataSet();
            using (var conn = new SqlConnection(connString))
            {
                conn.Open();
                using (IDbTransaction tran = conn.BeginTransaction())
                {
                    try
                    {
                        // transactional code...
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = storeProcName;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Transaction = tran as SqlTransaction;
                            if (parmList != null)
                            {
                                cmd.Parameters.AddRange(parmList.ToArray());
                            }
                            var da = new SqlDataAdapter(cmd);
                            da.Fill(retDataSet);
                        }
                        tran.Commit();
                    }
                    catch (Exception)
                    {
                        tran.Rollback();
                        throw;
                    }
                }
            }

            return retDataSet;
        }


        public static DataTable FillDataTableStoreProcedure(String storeProcName)
        {
            return FillDataTableStoreProcedure(storeProcName, null);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public static DataTable FillDataTableStoreProcedure(String storeProcName, List<SqlParameter> parmList)
        {
            var connString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
            var retDataSet = new DataTable();
            using (var conn = new SqlConnection(connString))
            {
                conn.Open();
                using (IDbTransaction tran = conn.BeginTransaction())
                {
                    try
                    {
                        // transactional code...
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = storeProcName;
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Transaction = tran as SqlTransaction;
                            if (parmList != null)
                            {
                                cmd.Parameters.AddRange(parmList.ToArray());
                            }
                            var da = new SqlDataAdapter(cmd);
                            da.Fill(retDataSet);
                        }
                        tran.Commit();
                    }
                    catch (Exception)
                    {
                        tran.Rollback();
                        throw;
                    }
                }
            }

            return retDataSet;
        }


        public static DataSet FillDataSetSelectSqlQuery(String query)
        {
            return FillDataSetSelectSqlQuery(query, null);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public static DataSet FillDataSetSelectSqlQuery(String query, List<SqlParameter> parmList)
        {
            var connString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
            var retDataSet = new DataSet();
            using (var conn = new SqlConnection(connString))
            {
                conn.Open();
                using (IDbTransaction tran = conn.BeginTransaction())
                {
                    try
                    {
                        // transactional code...
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = query;
                            cmd.CommandType = CommandType.Text;
                            cmd.Transaction = tran as SqlTransaction;
                            if (parmList != null)
                            {
                                cmd.Parameters.AddRange(parmList.ToArray());
                            }
                            var da = new SqlDataAdapter(cmd);
                            da.Fill(retDataSet);
                        }
                        tran.Commit();
                    }
                    catch (Exception)
                    {
                        tran.Rollback();
                        throw;
                    }
                }
            }

            return retDataSet;
        }


        public static DataTable FillDataTableSelectSqlQuery(String storeProcName)
        {
            return FillDataTableSelectSqlQuery(storeProcName, null);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public static DataTable FillDataTableSelectSqlQuery(String storeProcName, List<SqlParameter> parmList)
        {
            var connString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
            var retDataSet = new DataTable();
            using (var conn = new SqlConnection(connString))
            {
                conn.Open();
                using (IDbTransaction tran = conn.BeginTransaction())
                {
                    try
                    {
                        // transactional code...
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = storeProcName;
                            cmd.CommandType = CommandType.Text;
                            cmd.Transaction = tran as SqlTransaction;
                            if (parmList != null)
                            {
                                cmd.Parameters.AddRange(parmList.ToArray());
                            }
                            var da = new SqlDataAdapter(cmd);
                            da.Fill(retDataSet);
                        }
                        tran.Commit();
                    }
                    catch (Exception)
                    {
                        tran.Rollback();
                        throw;
                    }
                }
            }

            return retDataSet;
        }


        public static void ExecuteNonQuery(String storeProcName)
        {
            ExecuteNonQuery(storeProcName, null);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public static void ExecuteNonQuery(String storeProcName, List<SqlParameter> parmList)
        {
            var connString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;
            using (var conn = new SqlConnection(connString))
            {
                conn.Open();
                using (IDbTransaction tran = conn.BeginTransaction())
                {
                    try
                    {
                        // transactional code...
                        using (SqlCommand cmd = conn.CreateCommand())
                        {
                            cmd.CommandText = storeProcName;
                            cmd.CommandType = CommandType.Text;
                            cmd.Transaction = tran as SqlTransaction;
                            if (parmList != null)
                            {
                                cmd.Parameters.AddRange(parmList.ToArray());
                            }
                            cmd.ExecuteNonQuery();
                        }
                        tran.Commit();
                    }
                    catch (Exception)
                    {
                        tran.Rollback();
                        throw;
                    }
                }
            }
        }
    }
}
