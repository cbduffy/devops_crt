﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AFM.Commerce.UnitTests.ServiceReference1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Security;

namespace AFM.Commerce.UnitTests
{
    [TestClass]
    public class SynchronyServiceTest
    {
        SynchronyRequest req = new SynchronyRequest();

        [TestInitialize]
        public void InitTest()
        {
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            
            /*
             * Replace this transactionid from the env. where ECS website is running
             */
            req.transactionid = "a0uWsWm5ERdmOiTxiRevE2fsZ2nzssK6ZaP86fxYCuatYiwuGknwvvX9b7hqM85Y"; // Guest user
            //req.transactionid = "afFcsJmxSW7blUDHhv3Fl8lYKAt3C+iVU7P4UGY3pJkgtFTcO0nklzApi9y69jL3"; //Signedin user
            req.syncOption = true;

        }

        [TestMethod]
        public void GetSynchronyOptions()
        {
            //Arrange
            ServiceReference1.SynchronyServiceInterfaceClient serv = new ServiceReference1.SynchronyServiceInterfaceClient();            
            serv.ClientCredentials.UserName.UserName = "Test";
            serv.ClientCredentials.UserName.Password = "Test1";

            //Act
            List<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions> list1 = serv.GetSynchronyOptionsJson(req).OfType<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions>().ToList<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions>();
           
            //Assert
            Assert.IsTrue((list1.Count > 0), "Invalid transactionId");

            
        }
        [TestMethod]
        [ExpectedException(typeof(MessageSecurityException))]
        public void GetSynchronyOptionsWithInvalidUser()
        {
            //Arrange
            ServiceReference1.SynchronyServiceInterfaceClient serv = new ServiceReference1.SynchronyServiceInterfaceClient();
            //user name and password is compared with the username and password in service reference web.config
            serv.ClientCredentials.UserName.UserName = "Test1";
            serv.ClientCredentials.UserName.Password = "Test2";

            //Act

            /*
             * Replace this transactionid from the env. where ECS website is running
             */
            List<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions> list1 = serv.GetSynchronyOptionsJson(req).OfType<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions>().ToList<AFM.Commerce.UnitTests.ServiceReference1.SynchronyOptions>();

            //Assert
            Assert.IsTrue((list1.Count > 0), "Invalid User");


        }

    }
}
