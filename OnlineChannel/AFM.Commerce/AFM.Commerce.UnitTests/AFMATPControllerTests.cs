﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Framework.Extensions.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AFM.Commerce.Entities;

namespace AFM.Commerce.UnitTests
{
    [TestClass()]
    public class AFMATPControllerTests
    {
        [TestMethod()]
        public void GetAtpForEcommerceTestValidRequestExpectedValidOutput()
        {
            //Arrange
            var objAtpRequest = Utils.TestHelper.GetAFMATPRequest();
            var objAtpResponseExpected = Utils.TestHelper.GetAFMATPResponse();
            //Act

            var objAtpResopnse = AFMATPController.GetAtpForEcommerce(objAtpRequest);

            //Assert
            Assert.IsTrue(objAtpResopnse != null, "Resonse Should not be Null");
            Assert.IsTrue(objAtpResopnse.ItemGroupings != null, "Resonse Should not be Null");
            foreach (var tempitemGrouping in objAtpResopnse.ItemGroupings)
            {
                var tmpExpected = objAtpResponseExpected.ItemGroupings.FirstOrDefault(tmp => tmp.AccountNumber == tempitemGrouping.AccountNumber);
                if (tmpExpected == null)
                {
                    Assert.IsTrue(false, "Item group Not Found " + tempitemGrouping.AccountNumber);
                }
                else
                {
                    foreach (var tmpATPItem in tempitemGrouping.Items)
                    {
                        var tmpExpectedResItem = tmpExpected.Items.FirstOrDefault(tmp =>
                            tmp.ItemId == tmpATPItem.ItemId);
                        if (tmpExpectedResItem == null)
                            Assert.IsTrue(false, "Item is not found in Expected Resonse" + tmpATPItem.ItemId);
                        else
                        {
                            Assert.IsTrue(tmpATPItem.IsAvailable == tmpExpectedResItem.IsAvailable, "IsAvailable Not Mathced for " + tmpATPItem.ItemId);
                            Assert.IsTrue(tmpATPItem.Message.ToUpper() == tmpExpectedResItem.Message.ToUpper(), "Message Not Mathced for " + tmpATPItem.ItemId);
                            Assert.IsTrue(tmpATPItem.ShippingSpan.ToUpper() == tmpExpectedResItem.ShippingSpan.ToUpper(), "ShippingSpan Not Mathced for " + tmpATPItem.ItemId);
                            //Assert.IsTrue(tmpATPItem.Quantity == tmpExpectedResItem.Quantity, "Quantity Not Mathced for " + tmpATPItem.ItemId);
                        }
                    }
                }

            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetAtpForEcommerceTestNullRequestExceptedNullReferanceException()
        {
            //Arrange
            
            //Act
            var objAtpResopnse = AFMATPController.GetAtpForEcommerce(null);

            //Assert
            
        }

        [TestMethod()]
        public void GetAtpForEcommerceAsyncTest()
        {
            //Assert.Fail();
            Assert.IsTrue(true, "No Code Writen");
        }
    }
}
