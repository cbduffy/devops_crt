﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Web;
using System.IO;
using System.Threading.Tasks;
using AFM.Commerce.Framework.Extensions.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;


namespace AFM.Commerce.UnitTests
{
    [TestClass()]
    public class AFMProductsControllerTests
    {
        [TestInitialize]
        public void InitTest()
        {
            Utils.TestHelper.CustomerId = "1003";
            Utils.TestHelper.CreateCheckOutDataXmlFile();
            Utils.TestHelper.ProductValidator = new AFMProductValidator();
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            Utils.TestHelper.RemoveCheckOutDataxmlFile();            
        }

        [TestMethod()]
        public void GetProductsTest()
        {
            //Arrange

            //Act
            var products = AFMProductsController.GetProducts();

            //Assert
            var xEleMain = new XElement("ItemPrices",
               from emp in products
               select new XElement("ItemPrices",
                              new XAttribute("ItemId", emp.ItemId),
                              new XElement("BasePrice", emp.BasePrice),
                              new XElement("Price", emp.Price),
                              new XElement("AdjustedPrice", emp.AdjustedPrice)
                          ));
            xEleMain.Save("c:\\ItemPriceListAssortedContest.xml");
            
        }

        [TestMethod]
        public void GetProductTestBasedOnZipCodeFromCookies()
        {
            HttpContext.Current = new HttpContext(
                new HttpRequest("", "http://tempuri.org", ""),
                new HttpResponse(new StringWriter())
                );

            System.Web.HttpContext.Current.Request.Cookies.Add(new HttpCookie("AFMZipCode", "00346"));
            //Act
            var products = AFMProductsController.GetProducts();

            //Assert

        }

        #region GetPrdtswithNapInfo
        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        // Exception will be thrown as request context is not available.
        public void GetPrdtswithNapInfoWithProductList()
        {
            //Arrange
            var productIds = Utils.TestHelper.ProductsRecIds();

            //Act
            var prodwihtNapInfo = AFMProductsController.GetPrdtNAPListData(productIds);

            //Assert
            Assert.IsTrue(prodwihtNapInfo.Count() == productIds.Count, "List of product not match");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetPrdtswithNapInfoWithNullProductListNullReferanceExpected()
        {
            //Arrange

            //Act
            AFMProductsController.GetPrdtNAPListData(null);

            //Assert            
        }

        #endregion
    }
}
