﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SynchronyService
{
    /// <summary>
    /// This class defines properties related to Synchrony Financing 
    /// </summary>
    [DataContract]
    public class SynchronyOptions
    {
        public SynchronyOptions()
        {
         this.FinancingOptionId = "Sync12mnth" ;

         this.CardType = "Synchrony";

         this.DefaultOrderTotal = true;

         this.Sortorder = 1;

         this.PromoDesc1 = "PromoDesc1";

         this.PromoDesc2 = "PromoDesc2";

         this.PromoDesc3 = "PromoDesc3";

         this.LearnMore ="Learn More";

         this.ShowLearnMore = true;

         this.ThirdPartyTermCode = "thirdPartyTermCode";

         this.RemoveDiscountPopUp = true;

         this.MinimumPurchaseAmount = 50.0M;

        this.SubTotal = 123.0M;

        this.EstimatedTotal = 154.0M;

        this.ShowOption = true;

        this.PaymTermId = "PaymTermId";
        this.PaymMode = "PaymMode";
        }
        /// <summary>
        /// Constructor for initializing synchrony options
        /// </summary>
        /// <param name="FinancingOptionId"></param>
        /// <param name="CardType"></param>
        /// <param name="DefaultOrderTotal"></param>
        /// <param name="Sortorder"></param>
        /// <param name="PromoDesc1"></param>
        /// <param name="PromoDesc2"></param>
        /// <param name="PromoDesc3"></param>
        /// <param name="LearnMore"></param>
        /// <param name="ShowLearnMore"></param>
        /// <param name="ThirdpartyTermCode"></param>
        /// <param name="RemoveDiscountPopUp"></param>
        /// <param name="Subtotal"></param>
        /// <param name="EstimatedTotal"></param>
        /// <param name="ShowOption"></param>
        /// <param name="PaymTermId"></param>
        /// <param name="PaymMode"></param>
        /// <param name="MinimumPurchaseAmount"></param>
        /// <param name="Status"></param>
        public SynchronyOptions(string FinancingOptionId, string CardType, bool DefaultOrderTotal, int Sortorder,string PromoDesc1,string PromoDesc2,
            string PromoDesc3,string LearnMore,bool ShowLearnMore,string ThirdpartyTermCode,bool RemoveDiscountPopUp,Decimal Subtotal,Decimal EstimatedTotal,
            bool ShowOption, string PaymTermId, string PaymMode,Decimal MinimumPurchaseAmount, bool Status,bool RemoveCartDiscount)
        {
            this.FinancingOptionId = FinancingOptionId;

            this.CardType = CardType;

            this.DefaultOrderTotal = DefaultOrderTotal;

            this.Sortorder = Sortorder;

            this.PromoDesc1 = PromoDesc1;

            this.PromoDesc2 = PromoDesc2;

            this.PromoDesc3 = PromoDesc3;

            this.LearnMore = LearnMore;

            this.ShowLearnMore = ShowLearnMore;

            this.ThirdPartyTermCode = ThirdpartyTermCode;

            this.RemoveDiscountPopUp = RemoveDiscountPopUp;

            this.SubTotal = Subtotal;

            this.EstimatedTotal = EstimatedTotal;
            this.MinimumPurchaseAmount = MinimumPurchaseAmount;

            this.ShowOption = ShowOption;
            this.PaymTermId = PaymTermId;
            this.PaymMode = PaymMode;
            this.Status = Status;
            this.RemoveCartDiscount = RemoveCartDiscount;
        }

        [DataMember]
        string FinancingOptionId;

        [DataMember]
        string CardType;

        [DataMember]
        bool DefaultOrderTotal;

        [DataMember]
        int Sortorder;

        [DataMember]
        string PromoDesc1;

        [DataMember]
        string PromoDesc2;

        [DataMember]
        string PromoDesc3;

        [DataMember]
        string LearnMore;

        [DataMember]
        bool ShowLearnMore;

        [DataMember]
        string ThirdPartyTermCode;

        [DataMember]
        bool RemoveDiscountPopUp;

        [DataMember]
        Decimal SubTotal;

        [DataMember]
        Decimal EstimatedTotal;

        [DataMember]
        bool ShowOption;


        [DataMember]
        public bool Status;

        [DataMember]
        public bool RemoveCartDiscount;

        [DataMember]
        public Decimal MinimumPurchaseAmount;

        [DataMember]
        public Decimal PaymentFee;

        [DataMember]
        public Decimal EstimatedPercentage;

        [DataMember]
        public string PaymTermId;

        [DataMember]
        public string PaymMode;
    }
}