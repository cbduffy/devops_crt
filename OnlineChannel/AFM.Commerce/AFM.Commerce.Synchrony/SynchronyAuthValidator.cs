﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;
using System.Security.Principal;

namespace SynchronyService
{
    public class SynchronyAuthValidator : UserNamePasswordValidator
    {
        public override void Validate(string userName, string password)
        {
            string user = ConfigurationManager.AppSettings["SynchronyUser"];
            string pwd = ConfigurationManager.AppSettings["SynchronyPwd"];
         
            if (userName != null && password != null && userName == user && password == pwd)
            return; 
            throw new SecurityTokenException("Unknown Username or Password"); 
        }
    }
}