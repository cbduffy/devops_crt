﻿using AFM.Commerce.Entities;
using AFM.Commerce.Framework.Extensions.Controllers;
using AFM.Commerce.Framework.Extensions.Mappers;
using AFM.Commerce.Framework.Extensions.Utils;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.Client;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core;
using Microsoft.Dynamics.Retail.Ecommerce.Sdk.Core.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.Web;

namespace SynchronyService
{
    public abstract class SynchronyServiceBase : SynchronyServiceInterface
    {
        public virtual List<SynchronyOptions> GetSynchronyOptionsJson(SynchronyRequest req)
        {
            List<SynchronyOptions> list = new List<SynchronyOptions>();
            string tranid = null;
            try
            {
                IProductValidator productValidator = new ProductValidator();
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                OrderManager manager = OrderManager.Create(runtime);
                tranid = req.transactionid;
                string cartId = AFM.Commerce.Framework.Extensions.Utils.SecurityManager.DecryptCartID(tranid);
                //CS by spriya - Bug:108339 Synchrony option for profile user not coming correctly
                Cart cart = manager.GetCart(cartId,CalculationModes.None);
                //CE by spriya - Bug:108339 Synchrony option for profile user not coming correctly
                decimal shippingCharges = 0.0M;
                foreach (var item in cart.CartLines)
                {
                    if (item.LineData[CartConstant.AFMItemType] != null && (int)item.LineData[CartConstant.AFMItemType] == (int)AFMItemType.DeliveryServiceItem)
                    {
                        shippingCharges += item.ExtendedPrice;
                    }
                }
                var subTotal = cart.SubtotalAmountWithoutTax - shippingCharges;
                List<AFM.Commerce.Entities.AFMSynchronyOptions> synchronyOptionList = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(AFMDataUtilities.GetSynchronyOptions(), cart.TotalAmount, subTotal, cart.DiscountAmount);
                SynchronyOptions syncOptionsEntity = null;
                if (req.syncOption)
                    foreach (AFM.Commerce.Entities.AFMSynchronyOptions opt in synchronyOptionList)
                    {
                        if (!opt.Status)
                            continue;
                        syncOptionsEntity = new SynchronyOptions(opt.FinancingOptionId, opt.CardType, opt.DefaultOrderTotal, opt.Sortorder, opt.PromoDesc1,
                            opt.PromoDesc2, opt.PromoDesc3, opt.LearnMore, opt.ShowLearnMore, opt.ThirdPartyTermCode, opt.RemoveDiscountPopUp, opt.SubTotal, opt.EstimatedTotal, opt.ShowOption,
                            opt.PaymTermId, opt.PaymMode, opt.MinimumPurchaseAmount, opt.Status,opt.RemoveCartDiscount);

                        list.Add(syncOptionsEntity);
                    }
                else
                {
                    foreach (AFM.Commerce.Entities.AFMSynchronyOptions opt in synchronyOptionList)
                    {
                        syncOptionsEntity = new SynchronyOptions(opt.FinancingOptionId, opt.CardType, opt.DefaultOrderTotal, opt.Sortorder, opt.PromoDesc1,
                             opt.PromoDesc2, opt.PromoDesc3, opt.LearnMore, opt.ShowLearnMore, opt.ThirdPartyTermCode, opt.RemoveDiscountPopUp, opt.SubTotal, opt.EstimatedTotal, opt.ShowOption,
                             opt.PaymTermId, opt.PaymMode, opt.MinimumPurchaseAmount, opt.Status,opt.RemoveCartDiscount);

                        list.Add(syncOptionsEntity);
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                AFM.Commerce.Logging.LoggerUtilities.ProcessLogMessage(
                       " transId : " + tranid + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                return list;
            }
        }

        public virtual List<SynchronyOptions> GetSynchronyOptionsTestJson()
        {
            List<SynchronyOptions> list = new List<SynchronyOptions>();
            SynchronyOptions syncOptions1 = new SynchronyOptions();
            list.Add(syncOptions1);
            list.Add(syncOptions1);
            return list;
        }

        //Use only for testing with exact cart id from crt.salestransactin table in channel database which is not encrypted for all options
        public virtual List<SynchronyOptions> GetResults(string TransactionID)
        {
            List<SynchronyOptions> list = new List<SynchronyOptions>();
            try
            {
                IProductValidator productValidator = new ProductValidator();
                CommerceRuntime runtime = CrtUtilities.GetCommerceRuntime();
                OrderManager manager = OrderManager.Create(runtime);
                Cart cart = manager.GetCart(TransactionID,CalculationModes.None);
                decimal shippingCharges = 0.0M;
                foreach (var item in cart.CartLines)
                {
                    if (item.LineData[CartConstant.AFMItemType] != null && (int)item.LineData[CartConstant.AFMItemType] == (int)AFMItemType.DeliveryServiceItem)
                    {
                        shippingCharges += item.ExtendedPrice;
                    }
                }
                var subTotal = cart.SubtotalAmountWithoutTax - shippingCharges;
                List<AFM.Commerce.Entities.AFMSynchronyOptions> synchronyOptionList = new AFMShoppingCartMapper(new SiteConfiguration()).ConvertToViewModel(AFMDataUtilities.GetSynchronyOptions(), cart.TotalAmount, subTotal, cart.DiscountAmount);
                SynchronyOptions syncOptionsEntity = null;
                
                foreach (AFM.Commerce.Entities.AFMSynchronyOptions opt in synchronyOptionList)
                {
                    syncOptionsEntity = new SynchronyOptions(opt.FinancingOptionId, opt.CardType, opt.DefaultOrderTotal, opt.Sortorder, opt.PromoDesc1,
                        opt.PromoDesc2, opt.PromoDesc3, opt.LearnMore, opt.ShowLearnMore, opt.ThirdPartyTermCode, opt.RemoveDiscountPopUp, opt.SubTotal, opt.EstimatedTotal, opt.ShowOption,
                        opt.PaymTermId, opt.PaymMode, opt.MinimumPurchaseAmount, opt.Status, opt.RemoveCartDiscount);

                    list.Add(syncOptionsEntity);
                }

                return list;
            }
            catch (Exception ex)
            {
                AFM.Commerce.Logging.LoggerUtilities.ProcessLogMessage(
                       " transId : " + TransactionID + Environment.NewLine +
                       ex.Message + Environment.NewLine +
                       ex.StackTrace);
                return list;
            }
        }
    }   
}