﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SynchronyService
{
    [ServiceContract]
    public interface SynchronyServiceInterface
    {

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        List<SynchronyOptions> GetSynchronyOptionsJson(SynchronyRequest req);

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        List<SynchronyOptions> GetSynchronyOptionsTestJson();

        [OperationContract]
        [WebInvoke(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, Method = "POST")]
        List<SynchronyOptions> GetResults(string transactionId);

    }


    [DataContract]
    public class SynchronyRequest
    {
        [DataMember]
        public string transactionid { get; set; }

        [DataMember]
        public bool syncOption { get; set; }
    }


}
