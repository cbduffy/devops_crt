﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles the request for issuing a loyalty card.
    /// </summary>
    public sealed class IssueLoyaltyCardRequestHandler : WorkflowRequestHandler<IssueLoyaltyCardRequest, IssueLoyaltyCardResponse>
    {
        /// <summary>
        /// Executes the workflow to issue a loyalty card.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override IssueLoyaltyCardResponse Process(IssueLoyaltyCardRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.LoyaltyIssueCard);

            // If the request contains a customer account number, find the party recid of the customer.
            long partyRecordId = 0;
            if (!string.IsNullOrWhiteSpace(request.CustomerAccountNumber))
            {
                var settings = new QueryResultSettings(new PagingInfo(PagingInfo.MaximumPageSize, 0));

                var getCustomerServiceRequest = new GetCustomersServiceRequest(settings, request.CustomerAccountNumber);
                var getCustomerServiceResponse = this.Context.Execute<GetCustomersServiceResponse>(getCustomerServiceRequest);
                var customer = getCustomerServiceResponse.Customers.FirstOrDefault();

                if (customer == null || customer.DirectoryPartyRecordId == 0)
                {
                    throw new DataValidationException(LoyaltyWorkflowErrors.CustomerOrDirectoryPartyNotFound);
                }

                partyRecordId = customer.DirectoryPartyRecordId;
            }

            var issueLoyaltyCardRequest = new IssueLoyaltyCardServiceRequest(
                request.LoyaltyCardNumber, 
                LoyaltyCardTenderType.AsCardTender,
                request.CustomerAccountNumber,
                partyRecordId,
                this.Context.GetPrincipal().ChannelId);

            // Issue the loyalty card in HQ
            var serviceResponse = this.Context.Execute<IssueLoyaltyCardServiceResponse>(issueLoyaltyCardRequest);
            return new IssueLoyaltyCardResponse(serviceResponse.LoyaltyCard);
        }
    }
}