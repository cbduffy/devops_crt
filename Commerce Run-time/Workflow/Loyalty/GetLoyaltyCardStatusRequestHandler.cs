﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles the request for getting the status of a loyalty card including card tiers and reward points status.
    /// </summary>
    public sealed class GetLoyaltyCardStatusRequestHandler : WorkflowRequestHandler<GetLoyaltyCardStatusRequest, GetLoyaltyCardStatusResponse>
    {
        /// <summary>
        /// Executes the workflow to get the status of a loyalty card including card tiers and reward points status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetLoyaltyCardStatusResponse Process(GetLoyaltyCardStatusRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.NullOrWhiteSpace(request.LoyaltyCardNumber, "request.LoyaltyCardNumber");

            // Call service
            var loyaltyServiceRequest = new GetLoyaltyCardStatusServiceRequest(request.LoyaltyCardNumber);
            loyaltyServiceRequest.RetrieveFutureLoyaltyCardTiers = true;
            loyaltyServiceRequest.RetrieveRewardPointStatus = true;

            var loyaltyServiceResponse = this.Context.Execute<GetLoyaltyCardStatusServiceResponse>(loyaltyServiceRequest);

            return new GetLoyaltyCardStatusResponse(loyaltyServiceResponse.LoyaltyCard);
        }
    }
}