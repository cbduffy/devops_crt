﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles the request for getting the loyalty card transactions.
    /// </summary>
    public sealed class GetLoyaltyCardTransactionsRequestHandler : WorkflowRequestHandler<GetLoyaltyCardTransactionsRequest, GetLoyaltyCardTransactionsResponse>
    {
        /// <summary>
        /// Executes the workflow to get the loyalty card transactions.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetLoyaltyCardTransactionsResponse Process(GetLoyaltyCardTransactionsRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.NullOrWhiteSpace(request.LoyaltyCardNumber, "request.LoyaltyCardNumber");
            ThrowIf.NullOrWhiteSpace(request.RewardPointId, "request.RewardPointId");
            ThrowIf.Null(request.QueryResultSettings, "request.QueryResultSettings");
            ThrowIf.Null(request.QueryResultSettings.Paging, "request.QueryResultSettings.Paging");

            var serviceRequest = new GetLoyaltyCardTransactionsServiceRequest(
                request.LoyaltyCardNumber,
                request.RewardPointId,
                request.QueryResultSettings);

            GetLoyaltyCardTransactionsServiceResponse serviceResponse = request.RequestContext.Runtime.Execute<GetLoyaltyCardTransactionsServiceResponse>(serviceRequest, this.Context);

            return new GetLoyaltyCardTransactionsResponse(serviceResponse.LoyaltyCardTransactions);
        }
    }
}