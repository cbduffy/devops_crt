﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to create a shift record.
    /// </summary>
    public sealed class ResumeShiftRequestHandler : WorkflowRequestHandler<ResumeShiftRequest, ResumeShiftResponse>
    {
        private const string ManagerPrivilegies = "MANAGERPRIVILEGES";

        /// <summary>
        /// Executes the resume shift workflow.
        /// </summary>
        /// <param name="request">The new shift request.</param>
        /// <returns>The resume shift response.</returns>
        protected override ResumeShiftResponse Process(ResumeShiftRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.ShiftTerminalId, "request.ShiftTerminalId");

            if (this.Context.GetTerminal() != null)
            {
                request.TerminalId = this.Context.GetTerminal().TerminalId;
            }

            EmployeePermissions employeePermission = EmployeePermissionHelper.GetEmployeePermissions(this.Context, this.Context.GetPrincipal().UserId);
            bool includeSharedShifts = employeePermission.HasManagerPrivileges || employeePermission.AllowManageSharedShift;

            Shift shift = ShiftDataDataServiceHelper.GetShift(this.Context, this.Context.GetPrincipal().ChannelId, request.ShiftTerminalId, request.ShiftId, includeSharedShifts);

            this.ValidateCanResumeShift(request, shift);

            shift.CashDrawer = request.CashDrawer;

            if (!string.Equals(shift.StaffId, this.Context.GetPrincipal().UserId, StringComparison.OrdinalIgnoreCase))
            {
                shift.CurrentStaffId = this.Context.GetPrincipal().UserId;
            }

            shift.CurrentTerminalId = request.TerminalId;

            shift.Status = ShiftStatus.Open;
            shift.StatusDateTime = this.Context.GetNowInChannelTimeZone();

            UpdateShiftStagingTableDataServiceRequest dataServiceRequest = new UpdateShiftStagingTableDataServiceRequest(shift);
            request.RequestContext.Runtime.Execute<NullResponse>(dataServiceRequest, this.Context);

            return new ResumeShiftResponse(shift);
        }

        /// <summary>
        /// Validates whether the shift can be resumed.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="shift">The shift.</param>
        private void ValidateCanResumeShift(ResumeShiftRequest request, Shift shift)
        {
            if (shift == null)
            {
                throw new DataValidationException(DataValidationErrors.ObjectNotFound, "There is no shift with the given identifier.");
            }

            switch (shift.Status)
            {
                case ShiftStatus.Open:
                    // we cannot open a shift opened on a different terminal
                    if (!string.Equals(shift.TerminalId, request.TerminalId, StringComparison.OrdinalIgnoreCase) &&
                        !string.Equals(shift.CurrentTerminalId, request.TerminalId, StringComparison.OrdinalIgnoreCase))
                    {
                        throw new DataValidationException(DataValidationErrors.ShiftAlreadyOpenOnDifferentTerminal, "The shift is open on a different terminal.");
                    }

                    break;
                case ShiftStatus.Closed:
                case ShiftStatus.BlindClosed:
                    throw new DataValidationException(DataValidationErrors.InvalidRequest, "The shift was previously closed or blind closed.");
                case ShiftStatus.Suspended:
                    if (!this.Context.GetPrincipal().IsInRole(ResumeShiftRequestHandler.ManagerPrivilegies))
                    {
                        if (shift.IsShared)
                        {
                            EmployeePermissions employeePermission = EmployeePermissionHelper.GetEmployeePermissions(this.Context, this.Context.GetPrincipal().UserId);
                            if (!employeePermission.AllowManageSharedShift)
                            {
                                throw new DataValidationException(DataValidationErrors.EmployeeNotAllowedManageSharedShift, "Employee not allowed to manage shared shift.");
                            }
                        }

                        // if the user is not a manager, the user can only resume a shift that she/he owns or have permissions to
                        if (!string.Equals(shift.StaffId, this.Context.GetPrincipal().UserId, StringComparison.OrdinalIgnoreCase)
                            && !string.Equals(shift.CurrentStaffId, this.Context.GetPrincipal().UserId, StringComparison.OrdinalIgnoreCase))
                        {
                            throw new DataValidationException(DataValidationErrors.InvalidRequest, "The user does not have permission to resume shift.");
                        }
                    }

                    break;
                case ShiftStatus.None:
                default:
                    break;
            }

            IList<Shift> openShiftsAtTerminal = ShiftDataDataServiceHelper.GetAllOpenedShiftsOnTerminal(request.RequestContext, request.RequestContext.GetPrincipal().ChannelId, request.TerminalId, false);

            if (openShiftsAtTerminal.Any())
            {
                if (shift.Status == ShiftStatus.Open && !openShiftsAtTerminal.Where(s => s.ShiftId == shift.ShiftId).Any())
                {
                    // The shift does not exist on the current terminal.
                    throw new DataValidationException(DataValidationErrors.ShiftDoesNotExist, "The shift does not exist on the current terminal.");
                }
                else if (shift.Status == ShiftStatus.Suspended
                    && openShiftsAtTerminal.Where(s => string.Equals(request.CashDrawer, s.CashDrawer, StringComparison.OrdinalIgnoreCase)).Any())
                {
                    // There is a shift already open on the this cash drawer.
                    throw new DataValidationException(DataValidationErrors.CashDrawerHasAnOpenShift, "There is an open shift on the current cash drawer.");
                }
            }
        }
    }
}
