﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to create a shift record.
    /// </summary>
    public sealed class UseExistingShiftRequestHandler : WorkflowRequestHandler<UseShiftRequest, UseShiftResponse>
    {
        private const string ManagerPrivilegies = "MANAGERPRIVILEGES";

        /// <summary>
        /// Executes the resume shift workflow.
        /// </summary>
        /// <param name="request">The new shift request.</param>
        /// <returns>The resume shift response.</returns>
        protected override UseShiftResponse Process(UseShiftRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.ShiftTerminalId, "request.ShiftTerminalId");

            ShiftDataManager shiftDataManager = new ShiftDataManager(this.Context);

            GetCurrentTerminalIdDataRequest dataRequest = new GetCurrentTerminalIdDataRequest();
            request.TerminalId = this.Context.Execute<SingleEntityDataServiceResponse<string>>(dataRequest).Entity;
            Shift shift = shiftDataManager.GetShift(request.ShiftTerminalId, request.ShiftId);

            shift.CurrentStaffId = this.Context.GetPrincipal().UserId;

            this.ValidateCanUseShift(request, shift);

            shift.StatusDateTime = this.Context.GetNowInChannelTimeZone();

            UpdateShiftStagingTableDataServiceRequest dataServiceRequest = new UpdateShiftStagingTableDataServiceRequest(shift);
            request.RequestContext.Runtime.Execute<NullResponse>(dataServiceRequest, this.Context);

            return new UseShiftResponse(shift);
        }

        /// <summary>
        /// Validates whether the shift can be resumed.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="shift">The shift.</param>
        private void ValidateCanUseShift(UseShiftRequest request, Shift shift)
        {
            if (!this.Context.GetPrincipal().IsInRole(ManagerPrivilegies))
            {
                EmployeePermissions employeePermission = EmployeePermissionHelper.GetEmployeePermissions(this.Context, this.Context.GetPrincipal().UserId);

                if (employeePermission != null && employeePermission.AllowMultipleShiftLogOn == false)
                {
                    throw new DataValidationException(SecurityErrors.UseExistingShiftPermissionDenied, string.Format(CultureInfo.CurrentUICulture, "Permission denied to use the existing shift: {0}", this.Context.GetPrincipal().UserId));
                }

                if (shift.IsShared && !employeePermission.AllowUseSharedShift && !employeePermission.AllowManageSharedShift)
                {
                    throw new DataValidationException(DataValidationErrors.EmployeeNotAllowedManageSharedShift, "Employee not allowed to manage shared shift.");
                }
            }

            if (shift == null)
            {
                throw new DataValidationException(DataValidationErrors.ObjectNotFound, "There is no shift with the given identifier.");
            }
            else if (shift.Status == ShiftStatus.Closed || shift.Status == ShiftStatus.BlindClosed)
            {
                throw new DataValidationException(DataValidationErrors.InvalidRequest, "The shift was previously closed or blind closed.");
            }
            else if (shift.Status == ShiftStatus.Open)
            {
                // we cannot open a shift opened on a different terminal
                if (!string.Equals(shift.TerminalId, request.TerminalId, StringComparison.OrdinalIgnoreCase))
                {
                    throw new DataValidationException(DataValidationErrors.ShiftAlreadyOpenOnDifferentTerminal, "The shift is open on a different terminal.");
                }
            }
        }
    }
}
