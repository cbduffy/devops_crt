﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Save all incoming non-sale transaction from store operations.
    /// </summary>
    public class SaveStoreOperationRequestHandler : WorkflowRequestHandler<SaveStoreOperationRequest, SaveStoreOperationResponse>
    {
        /// <summary>
        /// Executes the workflow to save non-sale transactions from store operation.
        /// </summary>
        /// <param name="request">Instance of <see cref="SaveStoreOperationRequest"/>.</param>
        /// <returns>Instance of <see cref="SaveStoreOperationResponse"/>.</returns>
        protected override SaveStoreOperationResponse Process(SaveStoreOperationRequest request)
        {
            ThrowIf.Null(request, "request");

            SaveStoreOperationResponse response = null;

            if (request.NonSalesTenderOperation != null)
            {
                if (request.NonSalesTenderOperation.NonSalesTenderType == NonSalesTenderType.OpenDrawer)
                {
                    this.Context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.OpenDrawer);
                }

                NonSalesTransaction tenderOperation = request.NonSalesTenderOperation;

                var serviceRequest = new SaveNonSaleTenderServiceRequest()
                {
                    TransactionId = request.NonSalesTenderOperation.Id, 
                    Amount = tenderOperation.Amount, 
                    Description = tenderOperation.Description, 
                    Currency = tenderOperation.ForeignCurrency, 
                    NonSalesTenderType = tenderOperation.NonSalesTenderType, 
                    ShiftId = tenderOperation.ShiftId, 
                    ShiftTerminalId = tenderOperation.ShiftTerminalId
                };

                var serviceResponse = this.Context.Execute<SaveNonSaleTenderServiceResponse>(serviceRequest);
                response = new SaveStoreOperationResponse(serviceResponse.NonSalesTenderOperation);
            }
            else if (request.TenderDropAndDeclareOperation != null)
            {
                DropAndDeclareTransaction dropAndDeclareOperation = request.TenderDropAndDeclareOperation;

                var serviceRequest = new SaveDropAndDeclareServiceRequest()
                {
                    TransactionId = request.TenderDropAndDeclareOperation.Id,
                    TenderDetails = dropAndDeclareOperation.TenderDetails, 
                    TenderDropAndDeclareType = dropAndDeclareOperation.TenderDropAndDeclareType, 
                    ShiftId = dropAndDeclareOperation.ShiftId, 
                    ShiftTerminalId = dropAndDeclareOperation.ShiftTerminalId
                };

                var serviceResponse = this.Context.Execute<SaveDropAndDeclareServiceResponse>(serviceRequest);
                response = new SaveStoreOperationResponse(serviceResponse.TenderDropAndDeclareOperation);
            }

            return response;
        }
    }
}
