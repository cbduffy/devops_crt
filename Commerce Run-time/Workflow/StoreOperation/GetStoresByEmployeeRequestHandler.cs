﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Services = Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow to process the employee time clock registration.
    /// </summary>
    public class GetStoresByEmployeeRequestHandler : WorkflowRequestHandler<GetStoresByEmployeeRequest, GetStoresByEmployeeResponse>
    {
        /// <summary>
        /// Executes the workflow to get Stores By Employee.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetStoresByEmployeeRequest"/>.</param>
        /// <returns>Instance of <see cref="GetStoresByEmployeeResponse"/>.</returns>
        protected override GetStoresByEmployeeResponse Process(GetStoresByEmployeeRequest request)
        {
            GetStoresByEmployeeResponse response = null;

            ThrowIf.Null(request, "request");

            GetEmployeeStoresFromAddressBookDataRequest dataRequest = new GetEmployeeStoresFromAddressBookDataRequest(this.Context.GetPrincipal().UserId, request.QueryResultSettings);
            var stores = this.Context.Execute<EntityDataServiceResponse<OrgUnit>>(dataRequest).EntityCollection;

            response = new GetStoresByEmployeeResponse(stores);

            return response;
         }
    }
}