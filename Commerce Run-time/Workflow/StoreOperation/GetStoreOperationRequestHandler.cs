﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    
    /// <summary>
    /// Save all incoming non-sale transaction from store operations.
    /// </summary>
    public class GetStoreOperationRequestHandler : WorkflowRequestHandler<GetStoreOperationRequest, GetStoreOperationResponse>
    {
        /// <summary>
        /// Executes the workflow to get aggregated value of drawer entry operation.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetStoreOperationRequest"/>.</param>
        /// <returns>Instance of <see cref="GetStoreOperationResponse"/>.</returns>
        protected override GetStoreOperationResponse Process(GetStoreOperationRequest request)
        {
            ThrowIf.Null(request, "request");

            var serviceRequest = new GetNonSaleTenderServiceRequest() { NonSalesTenderType = request.NonSalesTenderType, ShiftId = request.ShiftId, TransactionId = string.Empty };
            var serviceResponse = this.Context.Execute<GetNonSaleTenderServiceResponse>(serviceRequest);
            return new GetStoreOperationResponse(serviceResponse.NonSalesTenderOperation);
        }
    }
}
