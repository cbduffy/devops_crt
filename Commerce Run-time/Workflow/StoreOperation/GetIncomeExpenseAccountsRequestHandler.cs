﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Workflow to handle the income or expense accounts.
    /// </summary>
    public class GetIncomeExpenseAccountsRequestHandler : WorkflowRequestHandler<GetIncomeExpenseAccountsRequest, GetIncomeExpenseAccountsResponse>
    {
        /// <summary>
        /// Executes the workflow to get the income or expense accounts.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetIncomeExpenseAccountsRequest"/>.</param>
        /// <returns>Instance of <see cref="GetIncomeExpenseAccountsResponse"/>.</returns>
        protected override GetIncomeExpenseAccountsResponse Process(GetIncomeExpenseAccountsRequest request)
        {
            ThrowIf.Null(request, "request");

            if (request.IncomeExpenseAccountType == IncomeExpenseAccountType.None)
            {
                throw new DataValidationException(DataValidationErrors.UnknownRequest, "The income or expense account type is not set");
            }

            var channelDataManager = new ChannelDataManager(this.Context);

            // Validate whether cashier / manager has permissions to income (or) expense accounts.
            if (request.IncomeExpenseAccountType == IncomeExpenseAccountType.Income)
            {
                this.Context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.IncomeAccounts);
            }
            else if (request.IncomeExpenseAccountType == IncomeExpenseAccountType.Expense)
            {
                this.Context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.ExpenseAccounts);
            }

            var incomeExpenseAccounts = channelDataManager.GetIncomeExpenseAccounts(request.IncomeExpenseAccountType, new QueryResultSettings());

            return new GetIncomeExpenseAccountsResponse(incomeExpenseAccounts);
        }
    }
}
