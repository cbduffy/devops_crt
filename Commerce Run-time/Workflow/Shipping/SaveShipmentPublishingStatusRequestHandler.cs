﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    
    /// <summary>
    /// Handler for saving shipments publishing status.
    /// </summary>
    public sealed class SaveShipmentPublishingStatusRequestHandler : WorkflowRequestHandler<SaveShipmentPublishingStatusRequest, NullResponse>
    {
        /// <summary>
        /// Executes the workflow to save shipment publishing status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override NullResponse Process(SaveShipmentPublishingStatusRequest request)
        {
            ThrowIf.Null(request, "request");

            var dataServiceRequest = new CreateOrUpdateShipmentStatusDataRequest(request.ShipmentPublishingStatusCollection);
            request.RequestContext.Runtime.Execute<NullResponse>(dataServiceRequest, this.Context);
            
            return new NullResponse();
        }
    }
}