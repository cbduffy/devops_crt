﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow.Composition;

    /// <summary>
    /// Handles workflow for GetLineDeliveryOptions.
    /// </summary>
    public sealed class GetLineDeliveryOptionsRequestHandler : 
        WorkflowRequestHandler<GetLineDeliveryOptionsRequest, GetLineDeliveryOptionsResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch delivery options for given product and address.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetLineDeliveryOptionsRequest"/>.</param>
        /// <returns>Instance of <see cref="GetLineDeliveryOptionsResponse"/>.</returns>
        protected override GetLineDeliveryOptionsResponse Process(GetLineDeliveryOptionsRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.SalesLines, "request.SalesLines");

            var dataServiceRequest = new GetLineDeliveryOptionsDataRequest(request.SalesLines);
            var dataServiceResponse = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<SalesLineDeliveryOption>>(dataServiceRequest, this.Context);

            return new GetLineDeliveryOptionsResponse(dataServiceResponse.EntityCollection);
        }
    }
}