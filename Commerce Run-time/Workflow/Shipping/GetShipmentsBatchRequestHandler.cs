﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handler for reading shipments.
    /// </summary>
    public sealed class GetShipmentsBatchRequestHandler : WorkflowRequestHandler<GetShipmentsBatchRequest, GetShipmentsBatchResponse>
    {
        /// <summary>
        /// Executes the workflow to read a shipment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetShipmentsBatchResponse Process(GetShipmentsBatchRequest request)
        {
            ThrowIf.Null(request, "request");
            
            EntityDataServiceResponse<Shipment> dataServiceResponse;

            if (request.ShipmentIdCollection != null &&
                request.ShipmentIdCollection.Any())
            {
                var dataServiceRequest = new EntityDataServiceRequest<IEnumerable<string>, Shipment>(request.ShipmentIdCollection);
                dataServiceResponse = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<Shipment>>(dataServiceRequest, this.Context);
            }
            else
            {
                throw new ArgumentException("Either ShipmentIdCollection or QueryResultSettings must be provided in the request.", "request");
            }
            ////else if (request.QueryResultSettings != null)
            ////{
            ////    shipments = shippingDataManager.GetShipments(request.QueryResultSettings);
            ////}

            return new GetShipmentsBatchResponse(dataServiceResponse.EntityCollection);
        }
    }
}