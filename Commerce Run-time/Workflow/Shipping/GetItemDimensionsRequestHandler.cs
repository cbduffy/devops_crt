﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles workflow for GetItemDimensions.
    /// </summary>
    public sealed class GetItemDimensionsRequestHandler : 
        WorkflowRequestHandler<GetItemDimensionsRequest, GetItemDimensionsResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch delivery options for given product and address.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetItemDimensionsRequest"/>.</param>
        /// <returns>Instance of <see cref="GetItemDimensionsResponse"/>.</returns>
        protected override GetItemDimensionsResponse Process(GetItemDimensionsRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.ItemIds, "request.ItemIds");

            var dataServiceRequest = new GetItemDimensionsDataRequest(request.ItemIds, new QueryResultSettings(request.Columns));
            var dataServiceResponse = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<ItemDimensions>>(dataServiceRequest, this.Context);

            return new GetItemDimensionsResponse(dataServiceResponse.EntityCollection);
        }
    }
}