﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handler for reading shipments.
    /// </summary>
    public sealed class GetShipmentsRequestHandler : WorkflowRequestHandler<GetShipmentsRequest, GetShipmentsResponse>
    {
        /// <summary>
        /// Executes the workflow to read a shipment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetShipmentsResponse Process(GetShipmentsRequest request)
        {
            ThrowIf.Null(request, "request");

            var serviceRequest = new GetShipmentsServiceRequest(request.SalesId, request.ShipmentId, request.GetTrackingInfo);
            var serviceResponse = this.Context.Execute<GetShipmentsServiceResponse>(serviceRequest);
            return new GetShipmentsResponse(serviceResponse.Shipments);
        }
    }
}