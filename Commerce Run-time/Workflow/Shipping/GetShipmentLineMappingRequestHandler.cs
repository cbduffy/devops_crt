﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    
    /// <summary>
    /// Handler for reading shipments.
    /// </summary>
    public sealed class GetShipmentLineMappingRequestHandler : WorkflowRequestHandler<GetShipmentLineMappingRequest, GetShipmentLineMappingResponse>
    {
        /// <summary>
        /// Executes the workflow to get shipment line mappings.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetShipmentLineMappingResponse Process(GetShipmentLineMappingRequest request)
        {
            ThrowIf.Null(request, "request");

            var dataServiceRequest = new EntityDataServiceRequest<IEnumerable<string>, ShipmentLineMapping>(request.SalesIdCollection);
            var dataServiceResponse = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<ShipmentLineMapping>>(dataServiceRequest, this.Context);

            return new GetShipmentLineMappingResponse(dataServiceResponse.EntityCollection);
        }
    }
}