﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handles workflow for GetWarehouseDetails.
    /// </summary>
    public sealed class GetWarehouseDetailsRequestHandler : 
        WorkflowRequestHandler<GetWarehouseDetailsRequest, GetWarehouseDetailsResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch delivery options for given product and address.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetWarehouseDetailsRequest"/>.</param>
        /// <returns>Instance of <see cref="GetWarehouseDetailsResponse"/>.</returns>
        protected override GetWarehouseDetailsResponse Process(GetWarehouseDetailsRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.WarehouseIds, "request.WarehouseIds");
           
            var dataServiceRequest = new GetWarehouseDetailsDataRequest(request.WarehouseIds, new QueryResultSettings(request.Columns));
            var dataServiceResponse = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<WarehouseDetails>>(dataServiceRequest, this.Context);
            
            return new GetWarehouseDetailsResponse(dataServiceResponse.EntityCollection);
        }
    }
}