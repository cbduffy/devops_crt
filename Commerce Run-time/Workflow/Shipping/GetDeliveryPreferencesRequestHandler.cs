﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles workflow for getting the delivery preference types applicable to a cart.
    /// </summary>
    public sealed class GetDeliveryPreferencesRequestHandler :
        WorkflowRequestHandler<GetDeliveryPreferencesRequest, GetDeliveryPreferencesResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch delivery preferences for the given cart identifier.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetLineDeliveryOptionsRequest"/>.</param>
        /// <returns>Instance of <see cref="GetLineDeliveryOptionsResponse"/>.</returns>
        protected override GetDeliveryPreferencesResponse Process(GetDeliveryPreferencesRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.CartId, "request.CartId");

            var serviceRequest = new GetDeliveryPreferencesServiceRequest(this.Context, request.CartId);
            var serviceResponse = request.RequestContext.Execute<GetDeliveryPreferencesServiceResponse>(serviceRequest);
            GetDeliveryPreferencesResponse response = new GetDeliveryPreferencesResponse(serviceResponse.CartDeliveryPreferences);

            return response;
        }
    }
}