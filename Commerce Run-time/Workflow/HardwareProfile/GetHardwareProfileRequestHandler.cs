﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Request handler to get the hardware profiles.
    /// </summary>
    public sealed class GetHardwareProfileRequestHandler :
        WorkflowRequestHandler<GetHardwareProfileRequest, GetHardwareProfileResponse>
    {
        /// <summary>
        /// Gets the hardware profiles from the hardware profiles service.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.ConfigurationException">Required Service missing.</exception>
        protected override GetHardwareProfileResponse Process(GetHardwareProfileRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.ProfileId, "request.ProfileId");

            var hardwareProfileDataManager = new HardwareProfileDataManager(this.Context);

            HardwareProfile hardwareProfile = hardwareProfileDataManager.GetHardwareProfile(request.ProfileId);

            if (hardwareProfile == null)
            {
                throw new DataValidationException(DataValidationErrors.ObjectNotFound, "There is no hardware profile with the given profile identifier.");
            }

            GetCardPaymentPropertiesServiceRequest propertyRequest = new GetCardPaymentPropertiesServiceRequest(hardwareProfile.EftMerchantPropertyXML, true, "Desktop");
            GetCardPaymentPropertiesServiceResponse propertyResponse = this.Context.Execute<GetCardPaymentPropertiesServiceResponse>(propertyRequest);
            hardwareProfile.EftMerchantPropertyXML = propertyResponse.PortablePaymentPropertyXmlBlob;

            return new GetHardwareProfileResponse(hardwareProfile);
        }
    }
}
