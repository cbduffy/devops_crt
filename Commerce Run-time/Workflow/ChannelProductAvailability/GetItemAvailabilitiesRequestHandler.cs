﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow required to get item availabilities.
    /// </summary>
    public sealed class GetItemAvailabilitiesRequestHandler : WorkflowRequestHandler<GetItemAvailabilitiesRequest, GetItemAvailabilitiesResponse>
    {
        /// <summary>
        /// Executes the workflow for getting item availabilities.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetItemAvailabilitiesResponse Process(GetItemAvailabilitiesRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.ItemQuantities, "request.ItemQuantities");

            // Get item availabilities.
            var itemQuantitiesRequest = new GetItemAvailabilitiesByItemQuantitiesServiceRequest(
                request.QueryResultSettings, 
                request.ItemQuantities, 
                request.CustomerAccountNumber, 
                request.MaxWarehousesPerItem);

            var itemQuantitiesResponse = this.Context.Execute<GetItemAvailabilitiesByItemQuantitiesServiceResponse>(itemQuantitiesRequest);

            return new GetItemAvailabilitiesResponse(itemQuantitiesResponse.ItemAvailabilities);
        }
    }
}