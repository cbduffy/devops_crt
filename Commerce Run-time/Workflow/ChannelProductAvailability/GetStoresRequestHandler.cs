﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Services = Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    ///  Encapsulates the workflow required to get list of stores.
    /// </summary>
    public sealed class GetStoresRequestHandler : WorkflowRequestHandler<GetStoreLocationsRequest, GetStoreLocationsResponse>
    {
        /// <summary>
        /// Executes the workflow for a get stores.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetStoreLocationsResponse Process(GetStoreLocationsRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.SearchArea, "request.SearchArea");

            ChannelDataManager manager = new ChannelDataManager(this.Context);
            long channelId = manager.GetCurrentChannelId();

            var serviceRequest = new Services.GetStoresServiceRequest(
                request.QueryResultSettings,
                channelId,
                request.SearchArea);

            var serviceResponse = this.Context.Execute<Services.GetStoresServiceResponse>(serviceRequest);

            return new GetStoreLocationsResponse(serviceResponse.Stores, serviceResponse.TotalNumberOfRecords);
        }
    }
}