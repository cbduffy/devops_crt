﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Abstract base class for all request handlers for workflow.
    /// </summary>
    /// <typeparam name="TRequest">The type of the request.</typeparam>
    /// <typeparam name="TResponse">The type of the response.</typeparam>
    public abstract class WorkflowRequestHandler<TRequest, TResponse> : IRequestHandler
        where TRequest : Request
        where TResponse : Response
    {
        /// <summary>
        /// Gets list of supported request types.
        /// </summary>
        public virtual IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new Type[] { typeof(TRequest) };
            }
        }

        /// <summary>
        /// Gets the context associated with the current request.
        /// </summary>
        protected RequestContext Context { get; private set; }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            var context = request.RequestContext;
            if (context == null)
            {
                throw new ArgumentNullException("request", "request.RequestContext");
            }

            Guid instrumentationId = Guid.NewGuid();
            string requestName = context.Request.GetType().FullName;
            RetailLogger.Log.CrtWorkflowExecuteWorkflowHanderStarted(requestName, instrumentationId);
            bool isSuccess = false;

            try
            {
                this.Context = context;
                TRequest typedRequest = request as TRequest;
                if (typedRequest == null)
                {
                    throw new DataValidationException(DataValidationErrors.UnknownRequestResponsePair, "The specified request type is not supported by handler type {0}.", this.GetType().ToString());
                }

                TResponse response = this.Process(typedRequest);

                response.Notifications.AddRange(this.Context.Notifications);

                this.Context.Response = response;
                isSuccess = true;

                return response;
            }
            finally
            {
                RetailLogger.Log.CrtWorkflowExecuteWorkflowHanderFinished(requestName, instrumentationId, Convert.ToInt32(isSuccess));
            }
        }

        /// <summary>
        /// Execute method to be overridden by each derived class.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected abstract TResponse Process(TRequest request);
    }
}