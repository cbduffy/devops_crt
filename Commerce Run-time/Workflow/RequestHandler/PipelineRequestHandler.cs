﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow.Composition
{
    using System;
    using System.Linq;
    using DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// The request handler that initiates the request pipeline.
    /// </summary>
    /// <remarks>
    /// Initial implementation of the pipeline assuming that this does not vary much per request. If it does, we may need to make each of the steps as individual handlers.
    /// </remarks>
    public class PipelineRequestHandler : IRequestPipeline
    {
        /// <summary>
        /// Populates the request context.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>The populated request context.</returns>
        public static RequestContext PopulateRequestContext(RequestContext context)
        {
            ThrowIf.Null(context, "context");

            if (context.IsInitialized)
            {
                return context;
            }

            if (context.GetPrincipal() != null && !context.GetPrincipal().IsChannelAgnostic)
            {
                var channelDataManager = new ChannelDataManager(context);
                context.SetChannelConfiguration(channelDataManager.GetChannelConfiguration(context.GetPrincipal().ChannelId));

                if (context.GetChannelConfiguration().ChannelType == RetailChannelType.RetailStore)
                {
                    PopulateContextWithOrgUnit(context);
                }

                if (!context.GetPrincipal().IsTerminalAgnostic)
                {
                    PopulateContextWithTerminal(context);
                }

                // Use the channel's default language if no language was set on the request.
                if (string.IsNullOrWhiteSpace(context.LanguageId))
                {
                    context.LanguageId = context.GetChannelConfiguration().DefaultLanguageId;
                }
            }

            if (context.GetPrincipal() != null && context.GetTerminal() != null && context.GetPrincipal().ShiftId == 0)
            {
                PopulateContextWithShiftInformation(context);
            }

            context.SetInitialized();
            return context;
        }

        /// <summary>
        /// Executes each step in the process including the pre and post steps and the underlying handler.
        /// </summary>
        /// <param name="handler">The request handler to execute the request.</param>
        /// <param name="request">The request.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(IRequestHandler handler, Request request)
        {
            if (handler == null)
            {
                throw new ArgumentException("handler");
            }

            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            CommercePrincipal principal = request.RequestContext.GetPrincipal();
            if (request.NeedChannelIdFromPrincipal && principal.IsChannelAgnostic)
            {
                throw new UserAuthorizationException(
                    SecurityErrors.InvalidChannel,
                    "Invalid channel id {0} in current principal. Request type is {1}.",
                    principal.ChannelId,
                    request.GetType().ToString());
            }

            RequestContext retailContext = PopulateRequestContext(request.RequestContext);

            try
            {
                this.OnBeginRequest(retailContext);

                this.OnAuthorizeRequest(retailContext);

                this.OnPreExecuteRequest(retailContext);

                // invoke the actual handler to execute the business process from the request.
                NetTracer.Verbose("Invoking Request Handler {0}.", handler.GetType().ToString());
                var response = handler.Execute(request);

                this.OnPostExecuteRequest(retailContext);

                this.OnEndRequest(retailContext);

                NetTracer.Verbose("Request completed {0}", handler.GetType().ToString());
                return response;
            }
            catch (CommerceRuntimeException ex)
            {
                NetTracer.Error(ex, "CommerceRuntimeException was thrown.");
                throw;
            }
            catch (Exception ex)
            {
                NetTracer.Error(ex, "Non-CommerceRuntimeException was thrown.");
                throw;
            }
        }

        /// <summary>
        /// Invoked when a request is submitted to the handler.
        /// </summary>
        /// <param name="context">The request context.</param>
        protected virtual void OnBeginRequest(RequestContext context)
        {
            // trace the incoming request
            if (context != null && context.Request != null)
            {
                CommercePrincipal principal = context.GetPrincipal();
                if (principal != null && !string.IsNullOrWhiteSpace(principal.DeviceId))
                {
                    CrtMonitoringTracer.LogMposDiscoveryInformation(principal.DeviceId, principal.ChannelId, principal.TerminalId);
                }
            }
        }

        /// <summary>
        /// Invoked to authorize the request.
        /// </summary>
        /// <param name="context">The request context.</param>
        protected virtual void OnAuthorizeRequest(RequestContext context)
        {
            // Initialize the retail operation permission to call into auth service.
            if (context != null)
            {
                context.SetCheckAccessRetailOperationAction(retailOperation =>
                    {
                        var serviceRequest = new CheckAccessServiceRequest(context.GetPrincipal(), retailOperation);
                        context.Execute<Response>(serviceRequest);
                    });

                context.SetCheckAccessIsManagerAction(() =>
                    {
                        var serviceRequest = new CheckAccessIsManagerServiceRequest(context.GetPrincipal());
                        context.Execute<Response>(serviceRequest);
                    });

                context.SetCheckAccessIsShiftOpenAction(() =>
                {
                    var serviceRequest = new CheckAccessHasShiftServiceRequest(context.GetPrincipal());
                    context.Execute<Response>(serviceRequest);
                });
            }
        }

        /// <summary>
        /// Invoked before the execute method is called on the handlers.
        /// </summary>
        /// <param name="context">The request context.</param>
        protected virtual void OnPreExecuteRequest(RequestContext context)
        {
        }

        /// <summary>
        /// Invoked after the handler for the request has returned.
        /// </summary>
        /// <param name="context">The request context.</param>
        protected virtual void OnPostExecuteRequest(RequestContext context)
        {
        }

        /// <summary>
        /// Invoked as the last call before the response is returned to the caller.
        /// </summary>
        /// <param name="context">The request context.</param>
        protected virtual void OnEndRequest(RequestContext context)
        {
        }

        /// <summary>
        /// Gets Original Staff Id during Elevated Retail Operations (not a Manager's Staff id).
        /// </summary>
        /// <param name="context">
        /// Current request context.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>Cashier Staff Id, even during elevated operations.
        /// </returns>
        private static string GetOriginalStaffId(RequestContext context)
        {
            return string.IsNullOrWhiteSpace(context.GetPrincipal().OriginalUserId) ? context.GetPrincipal().UserId : context.GetPrincipal().OriginalUserId;
        }

        /// <summary>
        /// Gets the store by identifier and sets it on the context.
        /// </summary>
        /// <param name="context">The context.</param>
        private static void PopulateContextWithOrgUnit(RequestContext context)
        {
            GetStoreDataServiceRequest request = new GetStoreDataServiceRequest(context.GetPrincipal().ChannelId);
            OrgUnit orgUnit = context.Runtime.Execute<SingleEntityDataServiceResponse<OrgUnit>>(request, context, skipRequestPipeline: true).Entity;
            context.SetOrgUnit(orgUnit);
        }

        /// <summary>
        /// Gets the terminal by identifier and sets it on the context.
        /// </summary>
        /// <param name="context">The context.</param>
        private static void PopulateContextWithTerminal(RequestContext context)
        {
            TerminalDataManager manager = new TerminalDataManager(context);
            Terminal terminal = manager.GetTerminalByRecordId(context.GetPrincipal().TerminalId, new ColumnSet());

            if (terminal == null)
            {
                throw new DataValidationException(DataValidationErrors.TerminalNotFound);
            }

            context.SetTerminal(terminal);
        }

        /// <summary>
        /// Gets the current shift and sets it on the context.
        /// </summary>
        /// <param name="context">The context.</param>
        private static void PopulateContextWithShiftInformation(RequestContext context)
        {
            ShiftDataQueryCriteria criteria = new ShiftDataQueryCriteria();
            criteria.ChannelId = context.GetPrincipal().ChannelId;
            criteria.TerminalId = context.GetTerminal().TerminalId;
            criteria.StaffId = GetOriginalStaffId(context);
            criteria.Status = (int)ShiftStatus.Open;
            criteria.SearchByStaffId = true;
            criteria.SearchByCurrentStaffId = true;
            criteria.SearchByTerminalId = true;
            criteria.SearchByCurrentTerminalId = true;

            GetEmployeePermissionsDataRequest permissionsDataRequest = new GetEmployeePermissionsDataRequest(criteria.StaffId, new ColumnSet());
            EmployeePermissions employeePermissions = context.Runtime.Execute<SingleEntityDataServiceResponse<EmployeePermissions>>(
                permissionsDataRequest, context, skipRequestPipeline: true).Entity;
            if (employeePermissions != null)
            {
                criteria.IncludeSharedShifts = employeePermissions.HasManagerPrivileges
                    || employeePermissions.AllowManageSharedShift
                    || employeePermissions.AllowUseSharedShift
                    || employeePermissions.AllowMultipleShiftLogOn;
            }

            GetShiftDataDataRequest dataServiceRequest = new GetShiftDataDataRequest(criteria, new QueryResultSettings());
            Shift shift = context.Runtime.Execute<EntityDataServiceResponse<Shift>>(dataServiceRequest, context, skipRequestPipeline: true).EntityCollection.FirstOrDefault();

            if (shift != null)
            {
                // TerminalId is the identifier of the terminal which creates the shift
                context.GetPrincipal().ShiftId = shift.ShiftId;
                context.GetPrincipal().ShiftTerminalId = shift.TerminalId;
            }
        }
    }
}