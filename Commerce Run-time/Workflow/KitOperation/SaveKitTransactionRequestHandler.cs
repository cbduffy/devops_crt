﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Save all incoming kit (disassembly) operation transactions.
    /// </summary>
    public class SaveKitTransactionRequestHandler : WorkflowRequestHandler<SaveKitTransactionRequest, SaveKitTransactionResponse>
    {
        /// <summary>
        /// Executes the workflow to save  transactions from kit (disassembly) operation.
        /// </summary>
        /// <param name="request">Instance of <see cref="SaveKitTransactionRequest"/>.</param>
        /// <returns>Instance of <see cref="SaveKitTransactionResponse"/>.</returns>
        protected override SaveKitTransactionResponse Process(SaveKitTransactionRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.KitDisassembly);

            SaveKitTransactionResponse response = null;

            KitTransactionDataManager transactionDataManager = new KitTransactionDataManager(this.Context);
            KitTransaction transaction = this.GetKitTransaction(request);
            transactionDataManager.Save(transaction);
            response = new SaveKitTransactionResponse(transaction);

            return response;
        }

        /// <summary>
        /// Gets transaction object of the kit operation from the request and sets context related information.
        /// </summary>
        /// <param name="request">The request object.</param>
        /// <returns>Returns the kit transaction object.</returns>
        private KitTransaction GetKitTransaction(SaveKitTransactionRequest request)
        {
            var kitTransaction = new KitTransaction();

            kitTransaction.Id = request.KitTransaction.Id;
            kitTransaction.ShiftId = request.KitTransaction.ShiftId;
            kitTransaction.StoreId = this.Context.GetOrgUnit().OrgUnitNumber;
            kitTransaction.StaffId = this.Context.GetPrincipal().UserId;
            kitTransaction.TerminalId = this.Context.GetTerminal().TerminalId;
            kitTransaction.TransactionType = request.KitTransaction.TransactionType;

            foreach (var kitTransLine in request.KitTransaction.KitTransactionLines)
            {
                var transLine = new KitTransactionLine();
                transLine.Quantity = kitTransLine.Quantity;
                transLine.ItemId = kitTransLine.ItemId;
                transLine.InventoryDimensionId = kitTransLine.InventoryDimensionId;

                kitTransaction.KitTransactionLines.Add(transLine);
            }

            return kitTransaction;
        }
    }
}