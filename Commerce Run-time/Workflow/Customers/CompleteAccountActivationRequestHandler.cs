﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow to mark the account activation request as finished.
    /// </summary>
    public sealed class CompleteAccountActivationRequestHandler : WorkflowRequestHandler<CompleteAccountActivationRequest, NullResponse>
    {
        /// <summary>
        /// Executes the workflow to mark the account activation request as finished.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override NullResponse Process(CompleteAccountActivationRequest request)
        {
            ThrowIf.Null(request, "request");

            if (string.IsNullOrWhiteSpace(request.EmailAddress))
            {
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "E-mail address can't be empty.");
            }

            if (string.IsNullOrWhiteSpace(request.ActivationToken))
            {
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "Activation token can't be empty.");
            }

            // Validate the request
            var serviceRequest = new SaveCustomerAccountActivationServiceRequest(request.EmailAddress, request.ActivationToken, 1);
            return this.Context.Execute<NullResponse>(serviceRequest);
        }
    }
}