﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Get customer balance request handler.
    /// </summary>
    public sealed class GetCustomerBalanceRequestHandler : WorkflowRequestHandler<GetCustomerBalanceRequest, GetCustomerBalanceResponse>
    {
        /// <summary>
        /// Executes the workflow to retrieve customer balances.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetCustomerBalanceResponse Process(GetCustomerBalanceRequest request)
        {
            ThrowIf.Null(request, "request");

            var serviceRequest = new GetCustomerBalanceServiceRequest(
                request.CustomerAccountNumber,
                request.InvoiceAccountNumber,
                request.SearchLocation);

            var serviceResponse = this.Context.Execute<GetCustomerBalanceServiceResponse>(serviceRequest);

            return new GetCustomerBalanceResponse(serviceResponse.Balance);
        }
    }
}
