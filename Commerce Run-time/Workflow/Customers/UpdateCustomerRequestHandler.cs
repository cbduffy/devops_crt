﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// UpdateCustomerRequest class.
    /// </summary>
    public sealed class UpdateCustomerRequestHandler : WorkflowRequestHandler<UpdateCustomerRequest, UpdateCustomerResponse>
    {
        /// <summary>
        /// Executes the workflow to retrieve customer loyalty information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override UpdateCustomerResponse Process(UpdateCustomerRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.CustomerEdit);

            // Check for permissions if a new shipping address is being added.
            foreach (Address address in request.UpdatedCustomer.Addresses)
            {
                // If recordId is 0 that means that a new customer address is being added.
                if (address.RecordId == 0)
                {
                    this.Context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.ShippingAddressAdd);
                }
            }

            CustomerHelper.ValidateAddresses(this.Context, request.UpdatedCustomer.Addresses);

            var saveCustomerServiceRequest = new SaveCustomerServiceRequest(request.UpdatedCustomer);
            var saveCustomerServiceResponse = this.Context.Execute<SaveCustomerServiceResponse>(saveCustomerServiceRequest);

            return new UpdateCustomerResponse(saveCustomerServiceResponse.UpdatedCustomer);
        }
    }
}