﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// GetCustomersRequest class.
    /// </summary>
    public sealed class GetCustomersRequestHandler : WorkflowRequestHandler<GetCustomersRequest, GetCustomersResponse>
    {
        /// <summary>
        /// Executes the workflow to retrieve customer information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetCustomersResponse Process(GetCustomersRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.CustomerSearch);

            var serviceRequest = new GetCustomersServiceRequest(
                request.QueryResultSettings, 
                request.RecordId, 
                request.CustomerAccountNumber,
                request.PartyRecordId,
                request.CustomerPartyNumber);

            var serviceResponse = this.Context.Execute<GetCustomersServiceResponse>(serviceRequest);
            return new GetCustomersResponse(serviceResponse.Customers);
        }
    }
}