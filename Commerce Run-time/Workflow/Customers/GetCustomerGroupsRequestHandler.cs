﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve the customer groups.
    /// </summary>
    public class GetCustomerGroupsRequestHandler : WorkflowRequestHandler<GetCustomerGroupsRequest, GetCustomerGroupsResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving list of customer groups.
        /// </summary>
        /// <param name="request">The customer group request.</param>
        /// <returns>The customer group response.</returns>
        protected override GetCustomerGroupsResponse Process(GetCustomerGroupsRequest request)
        {
            ThrowIf.Null(request, "request");

            var serviceRequest = new GetCustomerGroupsServiceRequest(request.QueryResultSettings);
            var serviceResponse = this.Context.Execute<GetCustomerGroupsServiceResponse>(serviceRequest);
            return new GetCustomerGroupsResponse(serviceResponse.CustomerGroups);
        }
    }
}
