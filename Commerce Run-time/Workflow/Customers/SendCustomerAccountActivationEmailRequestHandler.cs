﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow to send an e-mail to a specified user.
    /// </summary>
    public sealed class SendCustomerAccountActivationEmailRequestHandler : WorkflowRequestHandler<SendCustomerAccountActivationEmailRequest, NullResponse>
    {
        /// <summary>
        /// Executes the workflow to send an e-mail to the specified customer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override NullResponse Process(SendCustomerAccountActivationEmailRequest request)
        {
            ThrowIf.Null(request, "request");

            if (string.IsNullOrWhiteSpace(request.EmailAddress))
            {
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "An email address is required.");
            }

            // Validate the customer
            GlobalCustomer customer = this.GetCustomer(request.EmailAddress);
            if (customer == null)
            {
                throw new DataValidationException(DataValidationErrors.CustomerNotFound, "No customer found using the specified email address. The email address is '{0}'.", request.EmailAddress);
            }

            // Get activation token
            string activationToken = request.EmailTemplateProperties.Single(x => x.Name.Equals("ActivationToken")).Value;

            // Send email to customer
            this.SendCustomerEmail(customer.Email, request.EmailTemplateId, request.EmailTemplateProperties);

            // Save the request to channel DB
            var serviceRequest = new SaveCustomerAccountActivationServiceRequest(request.EmailAddress, activationToken, 0);
            this.Context.Execute<Response>(serviceRequest);

            return new NullResponse();
        }

        private GlobalCustomer GetCustomer(string emailAddress)
        {
            CustomerSearchCriteria criteria = new CustomerSearchCriteria { Keyword = emailAddress };
            var searchCustomerRequest = new CustomersSearchServiceRequest(criteria, new QueryResultSettings());
            var searchCustomerResponse = this.Context.Execute<CustomersSearchServiceResponse>(searchCustomerRequest);
            var matchedSearchResults = searchCustomerResponse.Customers.Where(c => emailAddress.Equals(c.Email, System.StringComparison.OrdinalIgnoreCase));

            GlobalCustomer foundCustomer = matchedSearchResults.FirstOrDefault();

            return foundCustomer;
        }

        private void SendCustomerEmail(string email, string templateId, ICollection<NameValuePair> properties)
        {
            var serviceRequest = new SendEmailServiceRequest(
                email,
                properties,
                this.Context.GetChannelConfiguration().DefaultLanguageId,
                xmlData: null,
                emailId: templateId);

            this.Context.Execute<NullResponse>(serviceRequest);
        }
    }
}
