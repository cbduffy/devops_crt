﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to get all available employees for a store.
    /// </summary>
    public class GetAllStoreEmployeesRequestHandler : WorkflowRequestHandler<GetAllStoreEmployeesRequest, GetAllStoreEmployeesResponse>
    {
        /// <summary>
        /// Executes the workflow to get available stores.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetAllStoreEmployeesResponse Process(GetAllStoreEmployeesRequest request)
        {
            ThrowIf.Null(request, "request");

            EntityDataServiceRequest<Employee> dataRequest = new EntityDataServiceRequest<Employee>
            {
                QueryResultSettings = request.QueryResultSettings
            };
            ReadOnlyCollection<Employee> employees = this.Context.Execute<EntityDataServiceResponse<Employee>>(dataRequest).EntityCollection;

            return new GetAllStoreEmployeesResponse(employees);
        }
    }
}