﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve affiliations.
    /// </summary>
    public sealed class GetAffiliationsRequestHandler : WorkflowRequestHandler<GetAffiliationsRequest, GetAffiliationsResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving affiliations.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetAffiliationsResponse Process(GetAffiliationsRequest request)
        {
            ThrowIf.Null(request, "request");

            GetAffiliationsDataRequest getAffiliationsDataRequest = new GetAffiliationsDataRequest(request.QueryResultSettings);
            var affiliations = this.Context.Execute<EntityDataServiceResponse<Affiliation>>(getAffiliationsDataRequest).EntityCollection;

            var response = new GetAffiliationsResponse(affiliations);
            return response;
        }
    }
}