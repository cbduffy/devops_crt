﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve localized strings.
    /// </summary>
    public class GetLocalizedStringsRequestHandler : WorkflowRequestHandler<GetLocalizedStringsRequest, GetLocalizedStringsResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving localized strings, optionally by language and text identifiers.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetLocalizedStringsResponse Process(GetLocalizedStringsRequest request)
        {
            ThrowIf.Null(request, "request");

            ChannelDataManager manager = new ChannelDataManager(this.Context);

            return new GetLocalizedStringsResponse(manager.GetLocalizedStrings(request.LanguageId, request.TextId, request.QueryResultSettings));
        }
    }
}