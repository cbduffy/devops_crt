﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to create a shift record.
    /// </summary>
    public sealed class CreateShiftRequestHandler : WorkflowRequestHandler<CreateShiftRequest, CreateShiftResponse>
    {
        private const string ManagerPrivilegies = "MANAGERPRIVILEGES";

        /// <summary>
        /// Executes the create shift staging workflow.
        /// </summary>
        /// <param name="request">The new Shift request.</param>
        /// <returns>The new Shift response.</returns>
        protected override CreateShiftResponse Process(CreateShiftRequest request)
        {
            ThrowIf.Null(request, "request");

            GetCurrentTerminalIdDataRequest dataRequest = new GetCurrentTerminalIdDataRequest();
            request.TerminalId = this.Context.Execute<SingleEntityDataServiceResponse<string>>(dataRequest).Entity;
            this.ValidateCanOpenShift(request);
            
            Shift shift = this.CreateNewShift(request);

            var createShiftRequest = new CreateShiftDataServiceRequest(shift);
            request.RequestContext.Runtime.Execute<NullResponse>(createShiftRequest, this.Context);

            return new CreateShiftResponse(shift);
        }

        /// <summary>
        /// Validates whether employee can open shifts.
        /// </summary>
        /// <param name="request">The create shift request.</param>
        private void ValidateCanOpenShift(CreateShiftRequest request)
        {
            if (!this.Context.GetPrincipal().IsInRole(ManagerPrivilegies))
            {
                if (request.IsShared)
                {
                    EmployeePermissions employeePermission = EmployeePermissionHelper.GetEmployeePermissions(this.Context, this.Context.GetPrincipal().UserId);
                    if (!employeePermission.AllowManageSharedShift)
                    {
                        throw new DataValidationException(DataValidationErrors.EmployeeNotAllowedManageSharedShift, "Employee not allowed to manage shared shift.");
                    }
                }
            }

            IList<Shift> shifts = ShiftDataDataServiceHelper.GetAllOpenedShiftsOnTerminal(this.Context, this.Context.GetPrincipal().ChannelId, request.TerminalId, true);

            if (shifts.Any())
            {
                // Validate if any shift is open for the cash drawer specified in request.
                var openShift = shifts.Where(shift => string.Equals(shift.CashDrawer, request.CashDrawer, StringComparison.OrdinalIgnoreCase));

                if (openShift.Any())
                {
                    throw new DataValidationException(DataValidationErrors.CashDrawerHasAnOpenShift, "There is an open shift on the current cash drawer.");
                }
            }
        }

        /// <summary>
        /// Creates a new shift given the request.
        /// </summary>
        /// <param name="request">The create shift request.</param>
        /// <returns>The created shift.</returns>
        private Shift CreateNewShift(CreateShiftRequest request)
        {
            Shift shift = new Shift();
            shift.StartDateTime = this.Context.GetNowInChannelTimeZone();
            shift.StatusDateTime = this.Context.GetNowInChannelTimeZone();
            shift.StaffId = this.Context.GetPrincipal().UserId;
            shift.StoreRecordId = this.Context.GetPrincipal().ChannelId;
            shift.Status = ShiftStatus.Open;
            shift.CashDrawer = request.CashDrawer;
            shift.IsShared = request.IsShared;

            // Get the store identifier
            shift.StoreId = this.Context.GetOrgUnit().OrgUnitNumber;

            // Get terminal identifier
            GetCurrentTerminalIdDataRequest dataRequest = new GetCurrentTerminalIdDataRequest();
            shift.TerminalId = shift.CurrentTerminalId = this.Context.Execute<SingleEntityDataServiceResponse<string>>(dataRequest).Entity;
         
            if (request.ShiftId.HasValue && request.ShiftId.Value > 0)
            {
                shift.ShiftId = request.ShiftId.Value;
            }
            else
            {
                // Get the next number sequence value
                var numberSequenceRequest = new NumberSequenceDataServiceRequest(NumberSequenceSeedType.BatchId, shift.StoreId, shift.TerminalId);
                var response = request.RequestContext.Runtime.Execute<NumberSequenceDataServiceResponse>(numberSequenceRequest, request.RequestContext);

                shift.ShiftId = response.NumberSequence;
            }

            return shift;
        }
    }
}