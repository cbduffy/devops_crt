﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve currencies.
    /// </summary>
    public class GetUnitsOfMeasureRequestHandler : WorkflowRequestHandler<GetUnitsOfMeasureRequest, GetUnitsOfMeasureResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving list of units of measure.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetUnitsOfMeasureResponse Process(GetUnitsOfMeasureRequest request)
        {
            ThrowIf.Null(request, "request");

            var manager = new UnitOfMeasureConversionDataManager(this.Context);

            GetUnitsOfMeasureResponse response = null;

            if (string.IsNullOrEmpty(request.UnitId))
            {
                var units = manager.GetUnitsOfMeasure(request.QueryResultSettings);
                response = new GetUnitsOfMeasureResponse(units);
            }
            else
            {
                var unit = manager.GetUnitOfMeasure(request.UnitId);
                response = new GetUnitsOfMeasureResponse(new List<UnitOfMeasure> { unit });
            }
            
            return response;
        }
    }
}
