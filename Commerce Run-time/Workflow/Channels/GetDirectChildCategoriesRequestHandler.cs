﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handles workflow for GetDirectChildCategoriesRequest.
    /// </summary>
    public sealed class GetDirectChildCategoriesRequestHandler : WorkflowRequestHandler<GetDirectChildCategoriesRequest, GetChannelCategoryHierarchyResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving items.
        /// </summary>
        /// <param name="request">The request context.</param>
        /// <returns>
        /// A response encapsulating the collection of channels.
        /// </returns>
        protected override GetChannelCategoryHierarchyResponse Process(GetDirectChildCategoriesRequest request)
        {
            ThrowIf.Null(request, "request");

            ChannelDataManager manager = new ChannelDataManager(this.Context);
            IEnumerable<Category> directChildCategories = manager.GetDirectChildCategories(request.ChannelId, request.ParentCategoryId, request.QueryResultSettings);
            var response = new GetChannelCategoryHierarchyResponse(request.ChannelId, null, directChildCategories);

            return response;
        }
    }
}