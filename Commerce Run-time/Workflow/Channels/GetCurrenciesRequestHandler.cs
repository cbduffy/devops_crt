﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve currencies.
    /// </summary>
    public class GetCurrenciesRequestHandler : WorkflowRequestHandler<GetCurrenciesRequest, GetCurrenciesResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving list of currencies.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetCurrenciesResponse Process(GetCurrenciesRequest request)
        {
            ThrowIf.Null(request, "request");

            var manager = new ChannelDataManager(this.Context);
            var currencies = manager.GetCurrencies(request.QueryResultSettings);

            var response = new GetCurrenciesResponse(currencies);

            return response;
        }
    }
}
