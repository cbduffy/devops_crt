﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handler for the GetChannelProfileByChannelIdRequest.
    /// </summary>
    public class GetChannelProfileByChannelIdRequestHandler : WorkflowRequestHandler<GetChannelProfileByChannelIdRequest, GetChannelProfileByChannelIdResponse>
    {
        /// <summary>
        /// Executes the workflow to retrieve the channel's profile..
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetChannelProfileByChannelIdResponse Process(GetChannelProfileByChannelIdRequest request)
        {
            ThrowIf.Null(request, "request");

            ChannelDataManager manager = new ChannelDataManager(this.Context);
            ChannelProfile profile = manager.GetChannelProfileByChannelId(request.ChannelId, new ColumnSet());
            return new GetChannelProfileByChannelIdResponse(profile);
        }
    }
}