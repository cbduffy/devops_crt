﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow.Composition;

    /// <summary>
    /// Encapsulates the workflow required to retrieve channel cash declaration setup.
    /// </summary>
    public sealed class GetChannelCashDeclarationRequestHandler : WorkflowRequestHandler<GetChannelCashDeclarationRequest, GetChannelCashDeclarationResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving cash declaration setup by channel identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetChannelCashDeclarationResponse Process(GetChannelCashDeclarationRequest request)
        {
            ThrowIf.Null(request, "request");

            ChannelDataManager manager = new ChannelDataManager(this.Context);
            ReadOnlyCollection<CashDeclaration> attributes = manager.GetChannelCashDeclaration(request.ChannelId, request.QueryResultSettings);

            var response = new GetChannelCashDeclarationResponse(attributes);
            return response;
        }
    }
}