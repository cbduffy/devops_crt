﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to get available stores for device.
    /// </summary>
    public sealed class GetAvailableShiftsRequestHandler :
        WorkflowRequestHandler<GetAvailableShiftsRequest, GetAvailableShiftsResponse>
    {
        private const string ManagerPrivilegies = "MANAGERPRIVILEGES";

        /// <summary>
        /// Executes the workflow to get available Shifts.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetAvailableShiftsResponse Process(GetAvailableShiftsRequest request)
        {
            ThrowIf.Null(request, "request");

            ShiftDataManager shiftManager = new ShiftDataManager(this.Context);

            IEnumerable<Shift> shifts = null;
            EmployeePermissions employeePermission = EmployeePermissionHelper.GetEmployeePermissions(this.Context, this.Context.GetPrincipal().UserId);
            bool includeSharedShifts = employeePermission.AllowManageSharedShift;
            bool isManager = this.Context.GetPrincipal().IsInRole(GetAvailableShiftsRequestHandler.ManagerPrivilegies);

            switch (request.Status)
            {
                case ShiftStatus.Suspended:
                    if (isManager)
                    {
                        shifts = ShiftDataDataServiceHelper.GetAllStoreShiftsWithStatus(this.Context, this.Context.GetPrincipal().ChannelId, ShiftStatus.Suspended, request.QueryResultSettings, true);
                    }
                    else
                    {
                        shifts = ShiftDataDataServiceHelper.GetShiftsForStaffWithStatus(this.Context, this.Context.GetPrincipal().ChannelId, this.Context.GetPrincipal().UserId, ShiftStatus.Suspended, includeSharedShifts);
                    }

                    break;
                case ShiftStatus.Open:
                    GetCurrentTerminalIdDataRequest dataRequest = new GetCurrentTerminalIdDataRequest();
                    string terminalId = this.Context.Execute<SingleEntityDataServiceResponse<string>>(dataRequest).Entity;

                    includeSharedShifts |= employeePermission.AllowUseSharedShift;

                    if (isManager || employeePermission.AllowMultipleShiftLogOn)
                    {
                        shifts = ShiftDataDataServiceHelper.GetAllOpenedShiftsOnTerminal(this.Context, this.Context.GetPrincipal().ChannelId, terminalId, includeSharedShifts || isManager);
                    }
                    else
                    {
                        shifts = ShiftDataDataServiceHelper.GetOpenedShiftsOnTerminalForStaff(this.Context, this.Context.GetPrincipal().ChannelId, this.Context.GetPrincipal().UserId, terminalId, includeSharedShifts);
                    }

                    break;
                case ShiftStatus.BlindClosed:
                    this.Context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.ShowBlindClosedShifts);

                    if (isManager)
                    {
                        shifts = ShiftDataDataServiceHelper.GetAllStoreShiftsWithStatus(this.Context, this.Context.GetPrincipal().ChannelId, ShiftStatus.BlindClosed, request.QueryResultSettings, true);
                    }
                    else
                    {
                        shifts = ShiftDataDataServiceHelper.GetShiftsForStaffWithStatus(this.Context, this.Context.GetPrincipal().ChannelId, this.Context.GetPrincipal().UserId, ShiftStatus.BlindClosed, includeSharedShifts);
                    }

                    break;
                default:
                    shifts = new List<Shift>();
                    break;
            }

            return new GetAvailableShiftsResponse(shifts);
        }
    }
}