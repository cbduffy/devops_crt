﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve the default channel.
    /// </summary>
    public sealed class GetDefaultChannelRequestHandler : WorkflowRequestHandler<GetDefaultChannelRequest, GetDefaultChannelResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving info on the default channel.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetDefaultChannelResponse Process(GetDefaultChannelRequest request)
        {
            ThrowIf.Null(request, "request");

            var resolver = new StorageResolver(this.Context);
            long defaultChannelId = resolver.GetDefaultChannelId();
            var response = new GetDefaultChannelResponse(defaultChannelId);

            return response;
        }
    }
}