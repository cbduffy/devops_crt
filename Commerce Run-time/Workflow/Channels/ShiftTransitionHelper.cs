﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Helper class for shift status transition.
    /// </summary>
    internal class ShiftTransitionHelper
    {
        private const string ManagerPrivilegies = "MANAGERPRIVILEGES";

        private RequestContext context;
        private ChangeShiftStatusRequest request;
        private IDictionary<KeyValuePair<ShiftStatus, ShiftStatus>, Action<Shift>> transitions;
        private DeviceConfiguration deviceConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="ShiftTransitionHelper"/> class.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="request">The request.</param>
        internal ShiftTransitionHelper(RequestContext context, ChangeShiftStatusRequest request)
        {
            this.context = context;
            this.request = request;
            this.InitShiftTransitions();
        }

        /// <summary>
        /// Transits the shift object status and trigger associated operations.
        /// </summary>
        /// <param name="shift">The shift object.</param>
        public void TransitShiftStatus(Shift shift)
        {
            ThrowIf.Null(shift, "shift");

            ShiftStatus toStatus = this.request.ToStatus;
            Action<Shift> nextAction;
            if (!this.transitions.TryGetValue(new KeyValuePair<ShiftStatus, ShiftStatus>(shift.Status, toStatus), out nextAction))
            {
                throw new DataValidationException(
                    DataValidationErrors.InvalidStatus,
                    "Invalid status change request from {0} to {1}.",
                    shift.Status,
                    toStatus);
            }

            // Validates if the shift status transition is allowable
            this.ValidateCanChangeStatus(shift);

            // Validates if the close shift request is allowable
            this.ValidateCloseShift(shift);

            ThrowIf.Null(nextAction, "nextAction");
            nextAction.Invoke(shift);
        }

        /// <summary>
        /// Validates if the starting amounts and tender declaration of a close shift have been set.
        /// </summary>
        /// <param name="shift">The shift object to be verified.</param>
        private void ValidateStartingAmountsAndTenderDeclarationForClose(Shift shift)
        {
            var validationResults = new Collection<DataValidationFailure>();

            // Reuse the calculated starting amount value for a shift to be closed
            if (this.deviceConfiguration.RequireAmountDeclaration && shift.StartingAmountTotal == 0)
            {
                validationResults.Add(new DataValidationFailure(DataValidationErrors.ShiftStartingAmountNotEntered, "Starting amounts have not been entered."));
            }

            // Reuse the calculated declared tender value for a shift to be closed
            if (this.deviceConfiguration.RequireAmountDeclaration && shift.DeclareTenderAmountTotal == 0)
            {
                validationResults.Add(new DataValidationFailure(DataValidationErrors.ShiftTenderDeclarationAmountNotEntered, "A tender declaration has not been performed."));
            }

            if (validationResults.Any())
            {
                throw new DataValidationException(DataValidationErrors.ShiftValidationError, validationResults, "Could not close shift due to data validation failures.");
            }
        }

        /// <summary>
        /// Validates whether a status transition is possible on the specified shift.
        /// </summary>
        /// <param name="shift">The shift.</param>
        private void ValidateCanChangeStatus(Shift shift)
        {
            if (shift.Status == ShiftStatus.Open)
            {
                if (!string.Equals(shift.TerminalId, this.request.TerminalId, StringComparison.OrdinalIgnoreCase)
                    && !string.Equals(shift.CurrentTerminalId, this.request.TerminalId, StringComparison.OrdinalIgnoreCase))
                {
                    throw new DataValidationException(DataValidationErrors.ShiftAlreadyOpenOnDifferentTerminal, "The shift is open on a different terminal.");
                }
            }
            else if (!this.context.GetPrincipal().IsInRole(ShiftTransitionHelper.ManagerPrivilegies))
            {
                if (shift.IsShared)
                {
                    EmployeePermissions employeePermission = EmployeePermissionHelper.GetEmployeePermissions(this.context, this.context.GetPrincipal().UserId);
                    if (!employeePermission.AllowManageSharedShift)
                    {
                        throw new DataValidationException(DataValidationErrors.EmployeeNotAllowedManageSharedShift, "Employee not allowed to manage shared shift.");
                    }
                }

                if (!string.Equals(shift.StaffId, this.context.GetPrincipal().UserId, StringComparison.OrdinalIgnoreCase))
                {
                    throw new DataValidationException(DataValidationErrors.InvalidRequest, "The user does not have permission to change shift status.");
                }
            }
        }

        /// <summary>
        /// Validates the close or blind close shift request.
        /// </summary>
        /// <param name="shift">The shift object to be verified.</param>
        private void ValidateCloseShift(Shift shift)
        {
            bool noCloseOrBlindCloseShift = (this.request.ToStatus != ShiftStatus.Closed) && (this.request.ToStatus != ShiftStatus.BlindClosed);
            if (noCloseOrBlindCloseShift)
            {
                return;
            }

            bool canForceClose = this.deviceConfiguration.RequireAmountDeclaration && this.request.CanForceClose;
            if (canForceClose)
            {
                return;
            }

            if (this.request.ToStatus == ShiftStatus.Closed)
            {
                this.ValidateStartingAmountsAndTenderDeclarationForClose(shift);
            }
            else if (this.request.ToStatus == ShiftStatus.BlindClosed)
            {
                this.ValidateStartingsAmountsForBlindClose(shift);
            }
        }

        /// <summary>
        /// Validates if the starting amounts of a blind close shift have been set.
        /// </summary>
        /// <param name="shift">The shift object to be verified.</param>
        private void ValidateStartingsAmountsForBlindClose(Shift shift)
        {
            // Retrieve the non-sale tender transaction info
            var serviceRequest = new GetNonSaleTenderServiceRequest() { NonSalesTenderType = NonSalesTenderType.StartingAmount, ShiftId = shift.ShiftId.ToString(System.Globalization.CultureInfo.InvariantCulture), TransactionId = string.Empty };
            var response = this.context.Execute<GetNonSaleTenderServiceResponse>(serviceRequest);

            // Check if the starting amounts has been set
            var transactions = response.NonSalesTenderOperation as IList<NonSalesTransaction> ?? null;
            var hasStartAmount = false;

            if (transactions != null)
            {
                foreach (var transaction in transactions)
                {
                    if (transaction.Amount != 0)
                    {
                        hasStartAmount = true;
                        break;
                    }
                }
            }

            if (this.deviceConfiguration.RequireAmountDeclaration && !hasStartAmount)
            {
                throw new DataValidationException(DataValidationErrors.ShiftValidationError, "Starting amounts have not been entered. ");
            }
        }

        /// <summary>
        /// Blind closes the shift.
        /// </summary>
        /// <param name="shift">The shift.</param>
        private void BlindCloseShift(Shift shift)
        {
            this.CloseShift(shift, true);
        }

        /// <summary>
        /// Closes the shift.
        /// </summary>
        /// <param name="shift">The shift.</param>
        private void CloseShift(Shift shift)
        {
            this.CloseShift(shift, false);
        }

        /// <summary>
        /// Suspends the shift.
        /// </summary>
        /// <param name="shift">The shift.</param>
        private void SuspendShift(Shift shift)
        {
            // we have to clear current staff identifier on the shift in order to prevent multiple shifts open
            // with same staff identifier on different drawers
            shift.CashDrawer = null;
            shift.CurrentStaffId = null;
            shift.Status = ShiftStatus.Suspended;
        }

        /// <summary>
        /// Closes the shift.
        /// </summary>
        /// <param name="shift">The shift.</param>
        /// <param name="isBlindClose">If <c>true</c> the shift will be closed with a Closed status, otherwise with BlindClosed status.</param>
        private void CloseShift(Shift shift, bool isBlindClose)
        {
            shift.Status = isBlindClose ? ShiftStatus.BlindClosed : ShiftStatus.Closed;
            shift.CloseDateTime = this.context.GetNowInChannelTimeZone();
            shift.ClosedAtTerminalId = shift.CurrentTerminalId = this.context.GetTerminal().TerminalId;
        }

        /// <summary>
        /// Initializes the allowed shift status transitions.
        /// </summary>
        private void InitShiftTransitions()
        {
            this.transitions = new Dictionary<KeyValuePair<ShiftStatus, ShiftStatus>, Action<Shift>>();

            this.transitions.Add(new KeyValuePair<ShiftStatus, ShiftStatus>(ShiftStatus.Open, ShiftStatus.Closed), this.CloseShift);
            this.transitions.Add(new KeyValuePair<ShiftStatus, ShiftStatus>(ShiftStatus.Open, ShiftStatus.BlindClosed), this.BlindCloseShift);
            this.transitions.Add(new KeyValuePair<ShiftStatus, ShiftStatus>(ShiftStatus.Open, ShiftStatus.Suspended), this.SuspendShift);
            this.transitions.Add(new KeyValuePair<ShiftStatus, ShiftStatus>(ShiftStatus.BlindClosed, ShiftStatus.Closed), this.CloseShift);

            var dataRequest = new GetDeviceConfigurationDataServiceRequest(this.context.GetPrincipal().ChannelId, this.context.GetTerminal().TerminalId);
            this.deviceConfiguration = this.context.Runtime.Execute<SingleEntityDataServiceResponse<DeviceConfiguration>>(dataRequest, this.context).Entity;
            if (this.deviceConfiguration == null)
            {
                throw new DataValidationException(DataValidationErrors.ObjectNotFound, "There is no device configuration for the current channel and terminal");
            }
        }
    }
}