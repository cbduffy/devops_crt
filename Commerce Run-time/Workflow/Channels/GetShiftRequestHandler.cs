﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to get a single Shift object.
    /// </summary>
    public sealed class GetShiftRequestHandler : WorkflowRequestHandler<GetShiftRequest, GetShiftResponse>
    {
        /// <summary>
        /// Executes the workflow to get a Shift.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetShiftResponse Process(GetShiftRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.ShiftTerminalId, "request.ShiftTerminalId");

            var manager = new ShiftDataManager(this.Context);
            Shift shift = manager.GetShift(request.ShiftTerminalId, request.ShiftId);

            if (shift == null)
            {
                throw new DataValidationException(DataValidationErrors.ObjectNotFound, "There is no shift with the given identifier and terminal identifier.");
            }

            return new GetShiftResponse(shift);
        }
    }
}
