﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to update the status of a shift record.
    /// </summary>
    public sealed class ChangeShiftStatusRequestHandler : WorkflowRequestHandler<ChangeShiftStatusRequest, ChangeShiftStatusResponse>
    {
        /// <summary>
        /// Executes the create shift staging workflow.
        /// </summary>
        /// <param name="request">The new Shift request.</param>
        /// <returns>The new Shift response.</returns>
        protected override ChangeShiftStatusResponse Process(ChangeShiftStatusRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.ShiftTerminalId, "request.ShiftTerminalId");

            if (request.ToStatus == ShiftStatus.Closed)
            {
                this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.CloseShift);
            }
            else if (request.ToStatus == ShiftStatus.BlindClosed)
            {
                this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.BlindCloseShift);
            }
            else if (request.ToStatus == ShiftStatus.Suspended)
            {
                this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.SuspendShift);
            }

            GetCurrentTerminalIdDataRequest dataRequest = new GetCurrentTerminalIdDataRequest();
            request.TerminalId = this.Context.Execute<SingleEntityDataServiceResponse<string>>(dataRequest).Entity;

            var shiftDataManager = new ShiftDataManager(Context);
            Shift shift = shiftDataManager.GetShift(request.ShiftTerminalId, request.ShiftId);

            // Trigger batch calculation for the shift to be closed
            if (request.ToStatus == ShiftStatus.Closed)
            {
                ShiftCalculator.Calculate(this.Context, shift, request.ShiftTerminalId, request.ShiftId);
            }

            // Validate the if the change of shift status can be performed
            new ShiftTransitionHelper(this.Context, request).TransitShiftStatus(shift);

            shift.StatusDateTime = this.Context.GetNowInChannelTimeZone().DateTime;

            UpdateShiftStagingTableDataServiceRequest dataServiceRequest = new UpdateShiftStagingTableDataServiceRequest(shift);
            request.RequestContext.Runtime.Execute<NullResponse>(dataServiceRequest, this.Context);

            this.SaveTransactionLog(shift, request.TransactionId);

            if (request.ToStatus == ShiftStatus.Closed)
            {
                this.PurgeSalesTransactionData(request.RequestContext);
            }

            return new ChangeShiftStatusResponse(shift);
        }

        private static TransactionType? GetTransactionType(Shift shift)
        {
            switch (shift.Status)
            {
                case ShiftStatus.BlindClosed:
                    return TransactionType.BlindCloseShift;
                case ShiftStatus.Closed:
                    return TransactionType.CloseShift;
                case ShiftStatus.Suspended:
                    return TransactionType.SuspendShift;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Simple, non-critical maintenance task assigned to the shift closing event.
        /// </summary>
        /// <param name="requestContext">The request context.</param>
        private void PurgeSalesTransactionData(RequestContext requestContext)
        {
            if (requestContext == null || requestContext.Runtime == null)
            {
                return;
            }

            // Get retention period in days from device configuration
            Terminal terminal = requestContext.GetTerminal();
            GetDeviceConfigurationDataServiceRequest deviceConfigurationRequest = new GetDeviceConfigurationDataServiceRequest(terminal.ChannelId, terminal.TerminalId);
            DeviceConfiguration deviceConfiguration = requestContext.Runtime.Execute<SingleEntityDataServiceResponse<DeviceConfiguration>>(deviceConfigurationRequest, requestContext).Entity;

            // Purge transactions
            PurgeSalesTransactionsDataRequest dataServiceRequest = new PurgeSalesTransactionsDataRequest(terminal.ChannelId, terminal.TerminalId, deviceConfiguration.RetentionDays);
            requestContext.Runtime.Execute<NullResponse>(dataServiceRequest, this.Context);
        }

        private void SaveTransactionLog(Shift shift, string transactionId)
        {
            TransactionType? transactionType = GetTransactionType(shift);

            if (transactionType == null)
            {
                return; // Do nothing
            }

            TransactionLog transactionLog = new TransactionLog();
            transactionLog.TransactionType = transactionType.Value;
            transactionLog.StaffId = shift.StaffId;
            transactionLog.Id = transactionId;

            // Need to persist the current terminal id here since the Shift could be floated.
            transactionLog.TerminalId = shift.CurrentTerminalId;
            transactionLog.StoreId = shift.StoreId;

            TransactionLogDataManager transDataManager = new TransactionLogDataManager(this.Context);
            transDataManager.Save(transactionLog);
        }
    }
}
