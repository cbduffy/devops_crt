﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow.Composition;

    /// <summary>
    /// Encapsulates the workflow required to retrieve channel tender types.
    /// </summary>
    public sealed class GetChannelTenderTypesRequestHandler : WorkflowRequestHandler<GetChannelTenderTypesRequest, GetChannelTenderTypesResponse>
    {
        private const int COUNTINGREQUIRED = 1;

        /// <summary>
        /// Executes the workflow associated with retrieving tender types by channel identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetChannelTenderTypesResponse Process(GetChannelTenderTypesRequest request)
        {
            ThrowIf.Null(request, "request");

            GetChannelTenderTypesResponse response = null;

            ChannelDataManager manager = new ChannelDataManager(this.Context);
            if (request.IsCountingRequired)
            {
                var attributes = manager.GetChannelTenderTypes(request.ChannelId, COUNTINGREQUIRED, request.QueryResultSettings);
                response = new GetChannelTenderTypesResponse(attributes);
            }
            else
            {
                var attributes = manager.GetChannelTenderTypes(request.ChannelId, request.QueryResultSettings);
                response = new GetChannelTenderTypesResponse(attributes);
            }

            return response;
        }
    }
}