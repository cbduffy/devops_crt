﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;

    /// <summary>
    /// Encapsulates the workflow required to retrieve channel configuration.
    /// </summary>
    public sealed class GetChannelConfigurationRequestHandler : WorkflowRequestHandler<GetChannelConfigurationRequest, GetChannelConfigurationResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving channel configuration by channel identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetChannelConfigurationResponse Process(GetChannelConfigurationRequest request)
        {
            ThrowIf.Null(request, "request");

            var configuration = this.Context.GetChannelConfiguration();
            if (configuration == null)
            {
                GetChannelConfigurationDataServiceRequest dataServiceRequest = new GetChannelConfigurationDataServiceRequest();
                var response = request.RequestContext.Runtime.Execute<SingleEntityDataServiceResponse<ChannelConfiguration>>(dataServiceRequest, this.Context);
                configuration = response.Entity;
            }

            return new GetChannelConfigurationResponse(configuration);
        }
    }
}