﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve sales tax groups associated with AX company.
    /// </summary>
    public class GetSalesTaxGroupsRequestHandler : WorkflowRequestHandler<GetSalesTaxGroupsRequest, GetSalesTaxGroupsResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving list of sales tax groups.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        /// The response.
        /// </returns>
        protected override GetSalesTaxGroupsResponse Process(GetSalesTaxGroupsRequest request)
        {
            ThrowIf.Null(request, "request");

            GetSalesTaxGroupsDataRequest dataRequest = new GetSalesTaxGroupsDataRequest(request.QueryResultSettings);
            ReadOnlyCollection<SalesTaxGroup> salesTaxGroups = this.Context.Execute<EntityDataServiceResponse<SalesTaxGroup>>(dataRequest).EntityCollection;

            return new GetSalesTaxGroupsResponse(salesTaxGroups);
        }
    }
}
