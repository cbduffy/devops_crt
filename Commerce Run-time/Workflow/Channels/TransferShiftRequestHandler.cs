﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to save a shift record.
    /// </summary>
    public sealed class TransferShiftRequestHandler : WorkflowRequestHandler<TransferShiftRequest, TransferShiftResponse>
    {
        /// <summary>
        /// Executes the save shift workflow.
        /// </summary>
        /// <param name="request">The new Shift request.</param>
        /// <returns>The new Shift response.</returns>
        protected override TransferShiftResponse Process(TransferShiftRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.Shift, "request.Shift");

            Shift shift = request.Shift;

            if (string.IsNullOrWhiteSpace(shift.TerminalId))
            {
                throw new DataValidationException(DataValidationErrors.InvalidRequest, "The shift terminal identifier is invalid.");
            }

            shift.StoreRecordId = this.Context.GetPrincipal().ChannelId;

            if (shift.ShiftId <= 0)
            {
                // Get the next number sequence value
                var numberSequenceRequest = new NumberSequenceDataServiceRequest(NumberSequenceSeedType.BatchId, shift.StoreId, shift.TerminalId);
                var response = request.RequestContext.Runtime.Execute<NumberSequenceDataServiceResponse>(numberSequenceRequest, request.RequestContext);

                shift.ShiftId = response.NumberSequence;
            }

            // Save the Shift into DB
            var createShiftRequest = new CreateShiftDataServiceRequest(shift);
            request.RequestContext.Runtime.Execute<NullResponse>(createShiftRequest, this.Context);

            return new TransferShiftResponse(shift);
        }
    }
}
