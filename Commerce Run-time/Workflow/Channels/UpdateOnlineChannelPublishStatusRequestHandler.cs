﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow required to update the channel publish status.
    /// </summary>
    public sealed class UpdateOnlineChannelPublishStatusRequestHandler : WorkflowRequestHandler<UpdateOnlineChannelPublishStatusRequest, UpdateOnlineChannelPublishStatusResponse>
    {
        /// <summary>
        /// Executes the workflow associated with updating publish status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override UpdateOnlineChannelPublishStatusResponse Process(UpdateOnlineChannelPublishStatusRequest request)
        {
            ThrowIf.Null(request, "request");

            var updateChannelPublishStatusRequest = new UpdateChannelPublishStatusServiceRequest(
                request.ChannelId,
                request.PublishStatus,
                request.PublishStatusMessage);

            this.Context.Execute<NullResponse>(updateChannelPublishStatusRequest);

            return new UpdateOnlineChannelPublishStatusResponse();
        }
    }
}