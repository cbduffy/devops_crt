﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to upload a shift record.
    /// </summary>
    public sealed class DeleteShiftRequestHandler : WorkflowRequestHandler<DeleteShiftRequest, NullResponse>
    {
        /// <summary>
        /// Executes the delete shift workflow.
        /// </summary>
        /// <param name="request">The delete Shift request.</param>
        /// <returns>The null response object.</returns>
        protected override NullResponse Process(DeleteShiftRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.Shift, "request.Shift");

            if (string.IsNullOrEmpty(request.Shift.TerminalId))
            {
                throw new DataValidationException(DataValidationErrors.InvalidRequest, "The shift terminal identifier is invalid.");
            }

            if (request.Shift.Status == ShiftStatus.Closed || request.Shift.Status == ShiftStatus.BlindClosed)
            {
                throw new DataValidationException(DataValidationErrors.InvalidRequest, "The closed shift cannot be deleted.");
            }

            var deleteShiftRequest = new DeleteShiftDataServiceRequest(request.Shift);
            request.RequestContext.Runtime.Execute<NullResponse>(deleteShiftRequest, this.Context);

            return new NullResponse();
        }
    }
}
