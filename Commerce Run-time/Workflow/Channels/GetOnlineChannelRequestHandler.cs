﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve the online channel.
    /// </summary>
    public sealed class GetOnlineChannelRequestHandler : WorkflowRequestHandler<GetOnlineChannelRequest, GetOnlineChannelResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving online channel by channel identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetOnlineChannelResponse Process(GetOnlineChannelRequest request)
        {
            ThrowIf.Null(request, "request");

            ChannelDataManager manager = new ChannelDataManager(this.Context);
            OnlineChannel channel = manager.GetOnlineChannelById(request.ChannelId, new ColumnSet());
            if (channel == null)
            {
                string message = string.Format(CultureInfo.InvariantCulture, "Cannot load channel for ID '{0}'.", request.ChannelId);
                throw new DataValidationException(DataValidationErrors.ValueOutOfRange, message);
            }

            var settings = new QueryResultSettings(new PagingInfo(PagingInfo.MaximumPageSize, 0));

            channel.ChannelProfile = manager.GetChannelProfileByChannelId(request.ChannelId, new ColumnSet());
            channel.ChannelProperties = manager.GetChannelPropertiesByChannelId(request.ChannelId, settings);
            channel.ChannelLanguages = manager.GetChannelLanguagesByChannelId(request.ChannelId, settings);

            var response = new GetOnlineChannelResponse(channel);
            return response;
        }
    }
}