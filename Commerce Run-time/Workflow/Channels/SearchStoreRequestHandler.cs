﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Search the store information for the given search criteria.
    /// </summary>
    public class SearchStoreRequestHandler : WorkflowRequestHandler<SearchStoreRequest, SearchStoreResponse>
    {
        /// <summary>
        /// Executes the workflow to retrieve store information using the specified criteria.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override SearchStoreResponse Process(SearchStoreRequest request)
        {
            ThrowIf.Null(request, "request");

            if (request.QueryCriteria == null)
            {
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "Criteria must be specified");
            }

            ChannelDataManager manager = new ChannelDataManager(this.Context);

            return new SearchStoreResponse(manager.SearchOrgUnit(request.QueryCriteria, request.QueryResultSettings));
        }
    }
}
