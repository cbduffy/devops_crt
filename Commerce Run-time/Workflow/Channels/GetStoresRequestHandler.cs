﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve stores.
    /// </summary>
    public sealed class GetStoreRequestHandler : WorkflowRequestHandler<GetStoresRequest, GetStoresResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving stores.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetStoresResponse Process(GetStoresRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.QueryResultSettings, "queryResultSettings");

            ChannelDataManager manager = new ChannelDataManager(this.Context);

            PagedResult<OrgUnit> result;

            if (request.GetAllStores)
            {
                result = new PagedResult<OrgUnit>(manager.GetStores(request.QueryResultSettings), false);
            }
            else
            {
                result = manager.GetStoreByStoreNumber(request.StoreNumbers, request.QueryResultSettings);
            }

            var response = new GetStoresResponse(result);
            return response;
        }
    }
}