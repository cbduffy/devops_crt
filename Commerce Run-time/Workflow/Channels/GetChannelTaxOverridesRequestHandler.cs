﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow.Composition;

    /// <summary>
    /// Encapsulates the workflow required to retrieve channel tax overrides.
    /// </summary>
    public sealed class GetChannelTaxOverridesRequestHandler : WorkflowRequestHandler<GetTaxOverridesRequest, GetTaxOverridesResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving tax overrides by channel identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetTaxOverridesResponse Process(GetTaxOverridesRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.QueryResultSettings, "request.QueryResultSettings");

            GetTaxOverridesDataRequest dataServiceRequest = new GetTaxOverridesDataRequest(request.RequestContext.GetPrincipal().ChannelId, request.OverrideBy);
            dataServiceRequest.QueryResultSettings = new QueryResultSettings(new ColumnSet());
            EntityDataServiceResponse<TaxOverride> response = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<TaxOverride>>(dataServiceRequest, request.RequestContext);
            
            return new GetTaxOverridesResponse(response.EntityCollection);
        }
    }
}