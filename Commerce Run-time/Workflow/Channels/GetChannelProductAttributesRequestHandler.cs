﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve the channel product attributes.
    /// </summary>
    public sealed class GetChannelProductAttributesRequestHandler : WorkflowRequestHandler<GetChannelProductAttributesRequest, GetChannelProductAttributesResponse>
    {
        /// <summary>
        /// Executes the workflow associated with retrieving product attributes by channel identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetChannelProductAttributesResponse Process(GetChannelProductAttributesRequest request)
        {
            ThrowIf.Null(request, "request");

            ChannelDataManager manager = new ChannelDataManager(this.Context);
            var attributes = manager.GetChannelProductAttributesByChannelId(request.ChannelId, request.QueryResultSettings);

            var response = new GetChannelProductAttributesResponse(attributes);
            return response;
        }
    }
}