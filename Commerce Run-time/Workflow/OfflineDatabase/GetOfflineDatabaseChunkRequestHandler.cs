﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to get the offline database metadata.
    /// </summary>
    public sealed class GetOfflineDatabaseChunkRequestHandler : WorkflowRequestHandler<GetOfflineDatabaseChunkRequest, GetOfflineDatabaseChunkResponse>
    {
        /// <summary>
        /// Executes the get offline database chunk workflow.
        /// </summary>
        /// <param name="request">The new <see cref="GetOfflineDatabaseChunkRequest"/> request.</param>
        /// <returns>The new <see cref="GetOfflineDatabaseChunkResponse"/> response.</returns>
        protected override GetOfflineDatabaseChunkResponse Process(GetOfflineDatabaseChunkRequest request)
        {
            ThrowIf.Null(request, "request");

            var dataManager = new OfflineDatabaseProvisionDataManager(Context);

            OfflineDatabaseChunk chunk;
            var found = dataManager.TryGetOfflineDatabaseChunk(request.RecordId, out chunk);

            if (!found)
            {
                throw new DataValidationException(DataValidationErrors.OfflineDatabaseChunkFileNotFound, "There is no provisioned database with the given database type for the client.");
            }

            return new GetOfflineDatabaseChunkResponse(chunk);
        }
    }
}