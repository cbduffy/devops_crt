﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to get the offline database.
    /// </summary>
    public sealed class GetLatestOfflineDatabaseChunksRequestHandler : WorkflowRequestHandler<GetLatestOfflineDatabaseChunksRequest, GetLatestOfflineDatabaseChunksResponse>
    {
        /// <summary>
        /// Executes the get offline database workflow.
        /// </summary>
        /// <param name="request">The new <see cref="GetLatestOfflineDatabaseChunksRequest"/> request.</param>
        /// <returns>The new <see cref="GetLatestOfflineDatabaseChunksResponse"/> response.</returns>
        protected override GetLatestOfflineDatabaseChunksResponse Process(GetLatestOfflineDatabaseChunksRequest request)
        {
            ThrowIf.Null(request, "request");

            var dataManager = new OfflineDatabaseProvisionDataManager(Context);
            var chunks = dataManager.GetLatestOfflineDatabaseChunks(request.DatabaseType, Context.GetPrincipal().ChannelId);

            return new GetLatestOfflineDatabaseChunksResponse(chunks);
        }
    }
}
