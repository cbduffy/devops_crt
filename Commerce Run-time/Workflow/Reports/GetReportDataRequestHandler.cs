﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetReportDataRequestHandler.cs" company="Microsoft Corporation">
//   Copyright (C) Microsoft Corporation.  All rights reserved.
//  THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
//  OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
//  THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
//  NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
// </copyright>
// <summary>
//  Encapsulates the workflow required to get datatable for reports data.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow required to get data table for reports data.
    /// </summary>
    public sealed class GetReportDataRequestHandler : WorkflowRequestHandler<GetReportDataRequest, GetReportDataResponse>
    {       
        /// <summary>
        /// Execute method to be overridden by each derived class.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetReportDataResponse Process(GetReportDataRequest request)
        {           
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.ReportId, "request.ReportID");
            ThrowIf.Null(request.QueryResultSettings, "request.QueryResultSettings");

            this.Context.GetCheckAccessRetailOperationAction()(DataModel.RetailOperation.ViewReport);

            // Get report data based on parameters from BI service.
            GetReportDataServiceRequest serviceRequest = new GetReportDataServiceRequest(request.ReportId, request.Locale, request.ReportParameters);
            GetReportDataServiceResponse serviceResponse = this.Context.Execute<GetReportDataServiceResponse>(serviceRequest);

            // Return data after localizing the output table's columns.
            ThrowIf.Null(serviceResponse.ReportData, "Report output");
            return new GetReportDataResponse(serviceResponse.ReportData);
        }
    }
}