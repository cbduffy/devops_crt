﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GetSupportedReportsRequestHandler.cs" company="Microsoft Corporation">
//   Copyright (C) Microsoft Corporation.  All rights reserved.
//  THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
//  OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
//  THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
//  NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
// </copyright>
// <summary>
//   Encapsulates the workflow required to get supported reports.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to get supported reports.
    /// </summary>
    public sealed class GetSupportedReportsRequestHandler : WorkflowRequestHandler<GetSupportedReportsRequest, GetSupportedReportsResponse>
    {       
        /// <summary>
        /// Execute method to be overridden by each derived class.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>        
        protected override GetSupportedReportsResponse Process(GetSupportedReportsRequest request)
        {            
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.ViewReport);

            // Get supported reports data from data service direct, since there is no business logic.
            var dataRequest = new GetSupportedReportsDataRequest(request.Locale, request.QueryResultSettings);
            ReportDataSet dataResponse = request.RequestContext.Runtime.Execute<SingleEntityDataServiceResponse<ReportDataSet>>(dataRequest, request.RequestContext).Entity;

            return new GetSupportedReportsResponse(dataResponse);
        }
    }
}