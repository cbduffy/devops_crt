﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// The request handler for GetReceiptRequest class.
    /// </summary>
    public sealed class GetReceiptRequestHandler : WorkflowRequestHandler<GetReceiptRequest, GetReceiptResponse>
    {
        private const string Esc = "&#x1B;";
        private const string LogoMessage = "<L>";
        private const string Barcode = "<B>";
        private const string SingleSpace = "|1C";
        private const string DoubleSpace = "|2C";
        private readonly string receiptEmailTemplate = "EmailRecpt";
        private readonly string receiptEmailTemplateParameter = "message";

        /// <summary>
        /// Processes the GetReceiptRequest to return the set of receipts. The request should not be null.
        /// </summary>
        /// <param name="request">The request parameter.</param>
        /// <returns>The GetReceiptResponse.</returns>
        protected override GetReceiptResponse Process(GetReceiptRequest request)
        {
            ThrowIf.Null(request, "request");

            if (string.IsNullOrWhiteSpace(request.TransactionId) && string.IsNullOrWhiteSpace(request.SalesId))
            {
                throw new DataValidationException(DataValidationErrors.ObjectNotFound, "Transaction identifier must be provided.");
            }

            SalesOrder salesOrder;
            string key;
            ReadOnlyCollection<TenderLine> tenderLines;
           
            key = request.TransactionId;
            
            if (!request.QueryBySalesId)
            {
                salesOrder = this.GetTransactionByTransactionId(key, request.IsRemoteTransaction);
            }
            else
            {
                salesOrder = this.GetTransactionBySalesId(key);
            }

            tenderLines = CartWorkflowHelper.GetTenderLinesForSalesOrder(this.Context, request);

            if (salesOrder == null)
            {
                throw new DataValidationException(
                    DataValidationErrors.ObjectNotFound,
                    "Unable to get the sales order created. ID: {0}",
                            key);
            }

            List<ReceiptType> receiptTypes = CartWorkflowHelper.GetReceiptTypes(salesOrder, this.Context, request).ToList<ReceiptType>();

            var getReceiptServiceRequest = new GetReceiptServiceRequest(
                salesOrder,
                receiptTypes,
                tenderLines,
                false,
                request.IsPreview);

            if (request.ReceiptType == ReceiptType.SafeDrop
                || request.ReceiptType == ReceiptType.BankDrop
                || request.ReceiptType == ReceiptType.TenderDeclaration)
            {
                getReceiptServiceRequest.DropAndDeclareTransaction = this.GetDropAndDeclareTransaction(request.TransactionId);
            }
            else if (request.ReceiptType == ReceiptType.FloatEntry
                || request.ReceiptType == ReceiptType.StartingAmount
                || request.ReceiptType == ReceiptType.RemoveTender)
            {
                switch (request.ReceiptType)
                {
                    case ReceiptType.FloatEntry:
                        getReceiptServiceRequest.NonSalesTenderTransaction = this.GetNonSaleTransaction(request.TransactionId, NonSalesTenderType.FloatEntry, request.ShiftId);
                        break;
                    case ReceiptType.StartingAmount:
                        getReceiptServiceRequest.NonSalesTenderTransaction = this.GetNonSaleTransaction(request.TransactionId, NonSalesTenderType.StartingAmount, request.ShiftId);
                        break;
                    case ReceiptType.RemoveTender:
                        getReceiptServiceRequest.NonSalesTenderTransaction = this.GetNonSaleTransaction(request.TransactionId, NonSalesTenderType.RemoveTender, request.ShiftId);
                        break;
                }
            }
            
            if (!string.IsNullOrEmpty(salesOrder.ReceiptEmail))
            {
                try
                {
                    this.SendReceiptMail(salesOrder, tenderLines);
                }
                catch (CommunicationException ex)
                {
                    NetTracer.Error(ex, ex.Message);
                }
            }

            var getReceiptServiceResponse = this.Context.Execute<GetReceiptServiceResponse>(getReceiptServiceRequest);
            return new GetReceiptResponse(getReceiptServiceResponse.Receipts);
        }

        /// <summary>
        /// Make the transaction service call to email the receipt.
        /// </summary>
        /// <param name="salesOrder">The sales order.</param>
        /// <param name="tenderLines">The tender lines.</param>
        private void SendReceiptMail(SalesOrder salesOrder, ReadOnlyCollection<TenderLine> tenderLines)
        {
            List<ReceiptType> receiptTypes = new List<ReceiptType>();
            receiptTypes.Add(ReceiptType.SalesReceipt);

            var emailReceipt = new GetEmailReceiptServiceRequest(
                salesOrder,
                receiptTypes,
                tenderLines,
                false);

            var emailResponse = this.Context.Execute<GetEmailReceiptServiceResponse>(emailReceipt);

            if (emailResponse.Receipts.Count == 0 || emailResponse.Receipts == null)
            {
                return;
            }

            string emailMessage = emailResponse.Receipts[0].Header + emailResponse.Receipts[0].Body + emailResponse.Receipts[0].Footer;
            emailMessage = emailMessage.Replace(Esc, string.Empty);
            emailMessage = emailMessage.Replace(LogoMessage, string.Empty);
            emailMessage = emailMessage.Replace(Barcode, string.Empty);
            emailMessage = emailMessage.Replace(SingleSpace, " ");
            emailMessage = emailMessage.Replace(DoubleSpace, "  ");

            string language = string.Empty;

            if (!string.IsNullOrEmpty(salesOrder.CustomerId))
            {
                var getCustomerDataRequest = new GetCustomerDataRequest(salesOrder.CustomerId);
                SingleEntityDataServiceResponse<Customer> getCustomerDataResponse = this.Context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerDataRequest, this.Context);
                Customer customer = getCustomerDataResponse.Entity;

                if (customer == null)
                {
                    language = this.Context.GetChannelConfiguration().DefaultLanguageId;
                }
                else
                {
                    language = customer.Language;
                }
            }

            if (string.IsNullOrEmpty(language))
            {
                language = CultureInfo.CurrentUICulture.ToString();
            }

            NameValuePair mapping = new NameValuePair();
            mapping.Name = this.receiptEmailTemplateParameter;
            mapping.Value = emailMessage;

            Collection<NameValuePair> mappings = new Collection<NameValuePair>();
            mappings.Add(mapping);

            var emailServiceRequest = new SendEmailServiceRequest(
                salesOrder.ReceiptEmail,
                mappings,
                language,
                string.Empty,
                this.receiptEmailTemplate);

            this.Context.Execute<NullResponse>(emailServiceRequest);
        }

        /// <summary>
        /// Get the non sale tender transaction for receipt printing.
        /// </summary>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <param name="tenderType">The non sale tender type.</param>
        /// <param name="shiftId">The identifier of the shift associated with the receipt.</param>
        /// <returns>The non sale tender transaction.</returns>
        private NonSalesTransaction GetNonSaleTransaction(string transactionId, NonSalesTenderType tenderType, long shiftId)
        {
            var serviceRequest = new GetNonSaleTenderServiceRequest { NonSalesTenderType = tenderType, TransactionId = transactionId, ShiftId = shiftId.ToString() };
            var serviceResponse = this.Context.Execute<GetNonSaleTenderServiceResponse>(serviceRequest);
            return serviceResponse.NonSalesTenderOperation.SingleOrDefault();
        }

        /// <summary>
        /// Get the drop and declare tender transaction.
        /// </summary>
        /// <param name="transactionId">The transaction identifier.</param>
        /// <returns>The drop and declare tender transaction.</returns>
        private DropAndDeclareTransaction GetDropAndDeclareTransaction(string transactionId)
        {
            TenderDropAndDeclareDataManager dataManager = new TenderDropAndDeclareDataManager(this.Context);
            DropAndDeclareTransaction transaction = dataManager.GetTransaction(transactionId, new QueryResultSettings());
            ReadOnlyCollection<TenderDetail> tenderDetails = dataManager.GetTransactionTenders(transactionId, new QueryResultSettings());

            transaction.TenderDetails = tenderDetails;
            return transaction;
        }

        /// <summary>
        /// Returns Sales Order by sales id and terminal id. Used to get remote orders from AX which does not have transaction id.
        /// </summary>
        /// <param name="salesId">The sales id parameter.</param>
        /// <returns>The  SalesOrder.</returns>
        private SalesOrder GetTransactionBySalesId(string salesId)
        {
            // Recall the customer order
            var serviceRequest = new RecallCustomerOrderServiceRequest(
                salesId,
                false);

            var serviceResponse = this.Context.Execute<RecallCustomerOrderServiceResponse>(serviceRequest);
            SalesOrder salesOrder = serviceResponse.SalesOrder;

            // Set transaction to context
            this.Context.SetSalesTransaction(salesOrder);

            // Channel and terminal don't come from ax
            salesOrder.ChannelId = this.Context.GetPrincipal().ChannelId;
            salesOrder.TerminalId = this.Context.GetTerminal().TerminalId;

            // Perform order calculations (deposit, amount due, etc)
            CartWorkflowHelper.Calculate(this.Context, requestedMode: null);

            return salesOrder;
        }

        /// <summary>
        /// Returns Sales Order by transaction id and terminal id. Used to get local orders.
        /// </summary>
        /// <param name="transactionId">The transaction id parameter.</param>
        /// <param name="isRemoteTransaction">Client sends if this is local or remote transaction.</param>
        /// <returns>The SalesOrder.</returns>
        private SalesOrder GetTransactionByTransactionId(string transactionId, bool isRemoteTransaction)
        {
            // Based on order type we decide the Search location to be efficient
            DataModel.SearchLocation searchLocationType = isRemoteTransaction ? SearchLocation.All : SearchLocation.Local;
            var criteria = new SalesOrderSearchCriteria
            {
                TransactionIds = new[] { transactionId },
                SearchLocationType = searchLocationType,
                IncludeDetails = true,
                SearchType = OrderSearchType.SalesTransaction
            };

            // Get the order. If order sales id is provided then it should be remote search mode.
            var getOrdersServiceRequest = new GetOrdersServiceRequest(criteria, new QueryResultSettings());
            var getOrdersServiceResponse = this.Context.Execute<GetOrdersServiceResponse>(getOrdersServiceRequest);

            if (getOrdersServiceResponse.Orders == null || !getOrdersServiceResponse.Orders.Any())
            {
                throw new DataValidationException(
                    DataValidationErrors.ObjectNotFound,
                    "Unable to get the sales order created. ID: {0}",
                    transactionId);
            }

            if (getOrdersServiceResponse.Orders.HasMultiple())
            {
                throw new DataValidationException(
                    DataValidationErrors.DuplicateObject,
                    "Multiple orders found with ID: {0}",
                    transactionId);
            }

            return getOrdersServiceResponse.Orders.Single();
        }
    }
}
