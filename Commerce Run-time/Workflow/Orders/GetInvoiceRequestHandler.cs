﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles workflow for recall customer order into a cart.
    /// </summary>
    public sealed class GetInvoiceRequestHandler : WorkflowRequestHandler<GetInvoiceRequest, GetInvoiceResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch the invoices.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetInvoiceRequest"/>.</param>
        /// <returns>Instance of <see cref="GetInvoiceResponse"/>.</returns>
        protected override GetInvoiceResponse Process(GetInvoiceRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(DataModel.RetailOperation.SalesInvoice);
            
            // Get the invoices
            var serviceRequest = new GetInvoiceServiceRequest(
                request.CustomerAccountNumber,
                request.SalesId,
                request.InvoiceId);

            var serviceResponse = this.Context.Execute<GetInvoiceServiceResponse>(serviceRequest);

            return new GetInvoiceResponse(serviceResponse.Invoices);
        }
    }
}