﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles workflow to get gift card.
    /// </summary>
    public sealed class GetGiftCardRequestHandler : WorkflowRequestHandler<GetGiftCardRequest, GetGiftCardResponse>
    {
        /// <summary>
        /// Executes the workflow to get gift card.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetGiftCardRequest"/>.</param>
        /// <returns>Instance of <see cref="GetGiftCardResponse"/>.</returns>
        protected override GetGiftCardResponse Process(GetGiftCardRequest request)
        {
            ThrowIf.Null(request, "request");

            // Get the invoices
            var serviceRequest = new GetGiftCardServiceRequest(
                request.Id);

            GetGiftCardServiceResponse serviceResponse = this.Context.Execute<GetGiftCardServiceResponse>(serviceRequest);

            return new GetGiftCardResponse(serviceResponse.GiftCard);
        }
    }
}