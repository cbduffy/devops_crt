﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CartWorkflowHelper.cs" company="Microsoft Corporation">
// THIS CODE IS MADE AVAILABLE AS IS. MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
// OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
// THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
// NO TECHNICAL SUPPORT IS PROVIDED. YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
// </copyright>
// <summary>
// Helper class for shopping cart related workflows.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Helper class for shopping cart related workflows.
    /// </summary>
    [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "In progress of refactoring.")]
    public static class CartWorkflowHelper
    {
        /// <summary>
        /// Loads a returned sales transaction. 
        /// </summary>
        /// <param name="context">The request context. It must contains sales transaction which has at least one sales lines for return.</param>
        /// <param name="cart">The cart object.</param>
        /// <returns>The loaded returned sales transaction.</returns>
        public static SalesTransaction LoadSalesTransactionForReturn(RequestContext context, Cart cart)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");
            ThrowIf.Null(cart, "cart");

            SalesTransaction originalTransaction = context.GetSalesTransaction();

            string transactionIdToLoad = ValidateAndGetReturnTransactionId(cart, originalTransaction);

            if (transactionIdToLoad == null)
            {
                return null;
            }

            context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.ReturnTransaction);

            SalesOrderSearchCriteria criteria = new SalesOrderSearchCriteria()
            {
                TransactionIds = new[] { transactionIdToLoad },
                IncludeDetails = true,
                SearchLocationType = SearchLocation.All,
                SalesTransactionTypes = new[] { SalesTransactionType.Sales },
                SearchType = OrderSearchType.SalesTransaction
            };
            GetOrdersServiceRequest request = new GetOrdersServiceRequest(criteria, new QueryResultSettings());

            GetOrdersServiceResponse response = context.Execute<GetOrdersServiceResponse>(request);
            if (!response.Orders.Any())
            {
                throw new DataValidationException(DataValidationErrors.ObjectNotFound, "Return transaction with id '{0}' not found.", transactionIdToLoad);
            }

            return response.Orders[0];
        }

        /// <summary>
        /// Writes an entry into the audit table.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="source">The log source.</param>
        /// <param name="logEntry">The log entry.</param>
        public static void LogAuditEntry(RequestContext context, string source, string logEntry)
        {
            var auditLogServiceRequest = new InsertAuditLogServiceRequest(source, logEntry, AuditLogTraceLevel.Trace, unchecked((int)context.RequestTimer.ElapsedMilliseconds));
            context.Execute<NullResponse>(auditLogServiceRequest);
        }

        /// <summary>
        /// Loads the sales transactions given the customer identifier.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>The loaded sales transaction.</returns>
        public static IEnumerable<SalesTransaction> LoadSalesTransactionsByCustomer(RequestContext context, string customerId)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(customerId, "customerId");

            NetTracer.Information("CartWorkflowHelper.LoadSalesTransactions(): CustomerId = {0}", customerId);

            // Try to load the transaction
            SalesTransactionDataManager dataManager = new SalesTransactionDataManager(context);
            return dataManager.GetTransactionsByCustomer(customerId, new ColumnSet());
        }

        /// <summary>
        /// Loads the sales transactions given the customer identifier.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="cartId">Cart identifier.</param>
        /// <returns>The loaded sales transaction.</returns>
        public static SalesTransaction LoadSalesTransaction(RequestContext context, string cartId)
        {
            ThrowIf.Null(context, "context");

            NetTracer.Information("CartWorkflowHelper.LoadSalesTransactionById(): cartId = {0}", cartId);

            // Try to load the transaction
            SalesTransactionDataManager dataManager = new SalesTransactionDataManager(context);
            return dataManager.GetTransactionById(cartId, new ColumnSet());
        }

        /// <summary>
        /// Loads the active sales transaction given the customer identifier.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="customerId">The customer identifier.</param>
        /// <returns>The active sales transaction.</returns>
        public static IEnumerable<SalesTransaction> LoadActiveSalesTransaction(RequestContext context, string customerId)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.NullOrWhiteSpace(customerId, "customerId");

            NetTracer.Information("CartWorkflowHelper.LoadActiveSalesTransaction(): CustomerId = {0}", customerId);

            // Try to load the transaction
            SalesTransactionDataManager dataManager = new SalesTransactionDataManager(context);
            ParameterSet parameters = new ParameterSet();
            parameters["CHANNELID"] = context.GetPrincipal().ChannelId;
            parameters["CUSTOMERID"] = customerId;
            parameters["TYPE"] = (int)CartType.Shopping;

            PagingInfo paging = new PagingInfo(1, 0);

            var sorting = new SortingInfo("MODIFIEDDATETIME", true);
            return dataManager.GetTransactions(parameters, new QueryResultSettings(paging, sorting));
        }

        /// <summary>
        /// Load all the suspended transactions.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>The loaded sales transaction.</returns>
        public static IEnumerable<SalesTransaction> LoadSuspendedSalesTransactions(RequestContext context)
        {
            ThrowIf.Null(context, "context");

            // NetTracer.Information("CartWorkflowHelper.LoadSuspendedSalesTransactions()");

            // Try to load the transaction
            SalesTransactionDataManager dataManager = new SalesTransactionDataManager(context);
            ParameterSet parameters = new ParameterSet();
            parameters["CHANNELID"] = context.GetPrincipal().ChannelId;
            parameters["ISSUSPENDED"] = "1";

            var sorting = new SortingInfo("CREATEDDATETIME", true);
            return dataManager.GetTransactions(parameters, new QueryResultSettings(sorting));
        }

        /// <summary>
        /// Gets the details of the transactions that are to be present on the receipt.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// /// <param name="request">The GetReceipt request.</param>
        /// <returns>Returns the sales order or the object that contains all the information of the receipt.</returns>
        public static ReadOnlyCollection<TenderLine> GetTenderLinesForSalesOrder(RequestContext context, GetReceiptRequest request)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(request, "request");
            SalesOrderDataManager dataManager = new SalesOrderDataManager(context);

            ReadOnlyCollection<TenderLine> tenderLines;
            QueryResultSettings settings = new QueryResultSettings(new PagingInfo(0, 0));

            tenderLines = dataManager.GetTenderLinesForSalesOrder(request.TransactionId, settings);

            // Get the receipt details by using context.request.
            // dataManager.GetOrderByReceiptId("<receiptId>", "<storeNumber>", "<terminalId>", new QueryResultSettings());
            return tenderLines;
        }

        /// <summary>
        /// Gets the types of receipts that are to be printed for a transaction.
        /// </summary>
        /// <param name="salesOrder">The sales order.</param>
        /// <param name="context">The request context.</param>
        /// <param name="request">The request parameter.</param>
        /// <returns>Returns the receipt types are will be printed.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Refactoring pending")]
        public static ReadOnlyCollection<ReceiptType> GetReceiptTypes(SalesTransaction salesOrder, RequestContext context, GetReceiptRequest request)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(request, "request");

            HashSet<ReceiptType> receiptTypes = new HashSet<ReceiptType>();

            // If the request is to print packing slip only
            switch (request.ReceiptType)
            {
                case ReceiptType.PackingSlip:
                case ReceiptType.BankDrop:
                case ReceiptType.SafeDrop:
                case ReceiptType.TenderDeclaration:
                case ReceiptType.StartingAmount:
                case ReceiptType.FloatEntry:
                case ReceiptType.RemoveTender:
                    receiptTypes.Add(request.ReceiptType);
                    return receiptTypes.AsReadOnly();
            }

            if (salesOrder != null)
            {
                // If the sales order is still null then check if it is a quotation.
                if (salesOrder.TransactionType == SalesTransactionType.CustomerOrder)
                {
                    if (salesOrder.CustomerOrderType == CustomerOrderType.SalesOrder && !(salesOrder.CustomerOrderType == CustomerOrderType.Quote))
                    {
                        receiptTypes.Add(ReceiptType.SalesOrderReceipt);

                        if (salesOrder.CustomerOrderMode == CustomerOrderMode.Pickup)
                        {
                            if (salesOrder.SalesLines.Where(line => line.DeliveryMode == context.GetChannelConfiguration().PickupDeliveryModeCode).Any())
                            {
                                receiptTypes.Add(ReceiptType.PickupReceipt);
                            }
                        }
                    }
                    else if (salesOrder.CustomerOrderType == CustomerOrderType.Quote)
                    {
                        receiptTypes.Add(ReceiptType.QuotationReceipt);
                    }
                }
                else if (salesOrder.TransactionType == SalesTransactionType.Sales || salesOrder.TransactionType == SalesTransactionType.IncomeExpense)
                {
                    receiptTypes.Add(ReceiptType.SalesReceipt);
                }

                QueryResultSettings settings = new QueryResultSettings(new PagingInfo(0, 0));
                string[] columnSet = new string[] { RetailTransactionPaymentSchema.CardOrAccountColumn, RetailTransactionPaymentSchema.TenderTypeColumn };
                settings.ColumnSet.Add(columnSet);

                foreach (SalesLine line in salesOrder.SalesLines)
                {
                    if (!line.IsVoided)
                    {
                        // For reason codes CRT still uses the comment to print whereas the client does not. If a new parameter is added to
                        // the receipt template to print the info code then we should remove the code below.
                        string comment = string.Empty;

                        foreach (ReasonCodeLine reasonCodeLine in line.ReasonCodeLines)
                        {
                            comment = comment + reasonCodeLine.Information + Environment.NewLine;
                        }

                        line.Comment = comment + line.Comment;

                        if (line.IsGiftCardLine)
                        {
                            receiptTypes.Add(ReceiptType.GiftCertificate);
                        }
                        else if (line.IsCustomerAccountDeposit)
                        {
                            receiptTypes.Add(ReceiptType.CustomerAccountDeposit);
                        }
                        else if (line.IsReturnByReceipt || line.Quantity < 0)
                        {
                            if (IsLabelPrintingRequired(salesOrder, line, context))
                            {
                                receiptTypes.Add(ReceiptType.ReturnLabel);
                            }
                        }
                    }
                }

                SalesOrderDataManager salesOrderDataManager = new SalesOrderDataManager(context);
                ChannelDataManager channelDataManager = new ChannelDataManager(context);

                ReadOnlyCollection<TenderType> tenderTypes = channelDataManager.GetChannelTenderTypes(context.GetPrincipal().ChannelId, new QueryResultSettings());

                foreach (TenderLine tenderLine in salesOrder.TenderLines)
                {
                    TenderType tenderType = tenderTypes.Where(type => type.TenderTypeId == tenderLine.TenderTypeId).SingleOrDefault();
                    RetailOperation operation = (RetailOperation)tenderType.OperationId;

                    switch (operation)
                    {
                        case RetailOperation.PayCard:
                            {
                                if (tenderLine.Amount >= 0)
                                {
                                    receiptTypes.Add(ReceiptType.CardReceiptForShop);
                                    receiptTypes.Add(ReceiptType.CardReceiptForCustomer);
                                }
                                else
                                {
                                    receiptTypes.Add(ReceiptType.CardReceiptForShopReturn);
                                    receiptTypes.Add(ReceiptType.CardReceiptForCustomerReturn);
                                }
                            }

                            break;
                        case RetailOperation.PayCustomerAccount:
                            {
                                if (tenderLine.Amount >= 0)
                                {
                                    receiptTypes.Add(ReceiptType.CustomerAccountReceiptForShop);
                                    receiptTypes.Add(ReceiptType.CustomerAccountReceiptForCustomer);
                                }
                                else
                                {
                                    receiptTypes.Add(ReceiptType.CustomerAccountReceiptForShopReturn);
                                    receiptTypes.Add(ReceiptType.CustomerAccountReceiptForCustomerReturn);
                                }
                            }

                            break;
                        case RetailOperation.PayCreditMemo:
                            receiptTypes.Add(ReceiptType.CreditMemo);
                            break;
                        case RetailOperation.PayCash:
                            // No special receipt needs to be added for cash payment.
                            break;
                        default:
                            receiptTypes.Add(ReceiptType.Unknown);
                            break;
                    }
                }
            }

            return receiptTypes.AsReadOnly();
        }

        /// <summary>
        /// Creates a sales transaction with given transaction id and customer id.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="transactionId">The transaction id.</param>
        /// <param name="customerId">The customer id.</param>
        /// <returns>The newly created sales transaction.</returns>
        public static SalesTransaction CreateSalesTransaction(RequestContext context, string transactionId, string customerId)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.NullOrWhiteSpace(transactionId, "transactionId");

            NetTracer.Information("CartWorkflowHelper.CreateSalesTransaction(): TransactionId = {0}, CustomerId = {1}", transactionId, customerId);

            SalesTransactionDataManager dataManager = new SalesTransactionDataManager(context);
            SalesTransaction transaction = dataManager.GetTransactionById(transactionId, new ColumnSet());
            if (transaction != null)
            {
                throw new CartValidationException(DataValidationErrors.DuplicateObject, transactionId);
            }

            // Gets the channel configuration
            ChannelConfiguration channelConfiguration = context.GetChannelConfiguration();

            // Gets the current store
            ChannelDataManager channelDataManager = new ChannelDataManager(context);

            // Creates the transaction
            transaction = new SalesTransaction();
            transaction.Id = transactionId;

            transaction.ShiftId = context.GetPrincipal().ShiftId;
            transaction.ShiftTerminalId = context.GetPrincipal().ShiftTerminalId;
            transaction.CustomerId = customerId;

            GetCurrentTerminalIdDataRequest dataRequest = new GetCurrentTerminalIdDataRequest();
            transaction.TerminalId = context.Execute<SingleEntityDataServiceResponse<string>>(dataRequest).Entity;

            transaction.IsTaxIncludedInPrice = channelConfiguration.PriceIncludesSalesTax;
            transaction.InventoryLocationId = channelConfiguration.InventLocation;

            transaction.BeginDateTime = context.GetNowInChannelTimeZone();

            if (context.GetChannelConfiguration().ChannelType == RetailChannelType.RetailStore)
            {
                if (context.GetOrgUnit() != null)
                {
                    transaction.StoreId = context.GetOrgUnit().OrgUnitNumber;
                }
            }

            return transaction;
        }

        /// <summary>
        /// Saves the context's sales transaction to the DB.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="transaction">Transaction to save.</param>
        public static void SaveSalesTransaction(RequestContext context, SalesTransaction transaction)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(transaction, "transaction");

            NetTracer.Information("CartWorkflowHelper.SaveSalesTransaction(): TransactionId = {0}, CustomerId = {1}", transaction.Id, transaction.CustomerId);

            try
            {
                context.Execute<NullResponse>(new SaveCartDataRequest(new[] { transaction }));
            }
            catch (StorageException e)
            {
                // if there is a concurrency exception, surface this to the the caller as a validation issue that can be retried
                if (e.ErrorResourceId == StorageErrors.ObjectVersionMismatchError)
                {
                    throw new CartValidationException(OrderWorkflowErrors.InvalidCartVersion, transaction.Id, "Cart version mismatch", e);
                }

                throw;
            }
        }

        /// <summary>
        /// Transfer the context's sales transaction to the DB.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="transaction">Transaction to save.</param>
        public static void TransferSalesTransaction(RequestContext context, SalesTransaction transaction)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(transaction, "transaction");

            NetTracer.Information("CartWorkflowHelper.TransferSalesTransaction(): TransactionId = {0}, CustomerId = {1}", transaction.Id, transaction.CustomerId);

            // Transfer sales transaction has to be seamless. 
            // Row version check need to be ignored since data gets tranfered from different storage.
            context.Execute<NullResponse>(new SaveCartDataRequest(new[] { transaction }, ignoreRowVersionCheck: true));
        }

        /// <summary>
        /// Validates the update cart request.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="salesTransaction">The sales transaction.</param>
        /// <param name="returnedSalesTransaction">The returned sales transaction.</param>
        /// <param name="newCart">The cart with updates/ new cart from the client.</param>
        /// <param name="isGiftCardOperation">True if request is a result of gift card operation.</param>
        /// <exception cref="CartValidationException">Invalid cart.</exception>
        public static void ValidateUpdateCartRequest(RequestContext context, SalesTransaction salesTransaction, SalesTransaction returnedSalesTransaction, Cart newCart, bool isGiftCardOperation)
        {
            ThrowIf.Null(salesTransaction, "salesTransaction");
            ThrowIf.Null(newCart, "newCart");

            NetTracer.Information("CartWorkflowHelper.ValidateUpdateCartRequest(): TransactionId = {0}, CustomerId = {1}", newCart.Id, newCart.CustomerId);

            // If cart type is None it means type has not been changhed so we check cart type on existing cart.
            var cartType = newCart.CartType == CartType.None ? salesTransaction.CartType : newCart.CartType;

            CartLineValidationResults validationResults = new CartLineValidationResults();
            HashSet<string> lineIdSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            // Validate readonly properties
            foreach (DataValidationFailure failure in ReadOnlyAttribute.CheckReadOnlyProperties(newCart, salesTransaction))
            {
                validationResults.AddLineResult(0, failure);
            }

            // Validates conflicts in cart lines
            CartWorkflowHelper.ValidateConflicts(newCart.CartLines, lineIdSet, validationResults);

            // Set to true if it is a return transaction by receipt.
            bool isCashAndCarryReturnByReceipt = cartType != CartType.CustomerOrder &&
                (((returnedSalesTransaction != null) && returnedSalesTransaction.IsReturnByReceipt) || newCart.CartLines.Any(l => !string.IsNullOrWhiteSpace(l.ReturnTransactionId)));

            if (isCashAndCarryReturnByReceipt)
            {
                // Validate conflicts in cart lines for return
                CartWorkflowHelper.ValidateReturnConflicts(returnedSalesTransaction, newCart.CartLines, validationResults);
            }

            // Validate all the lines.
            Dictionary<string, SalesLine> salesLineByLineId = salesTransaction.SalesLines
                .ToDictionary(sl => sl.LineId, sl => sl);

            Dictionary<decimal, SalesLine> returnSalesLineByLineNumber = null;
            if (returnedSalesTransaction != null)
            {
                // Validate all the lines.
                returnSalesLineByLineNumber = returnedSalesTransaction.SalesLines
                    .ToDictionary(sl => sl.LineNumber, sl => sl);
            }

            CartWorkflowHelper.ValidateSalesLineOperations(context, newCart, salesTransaction, salesLineByLineId, returnSalesLineByLineNumber, isGiftCardOperation, validationResults);
            CartWorkflowHelper.ValidateCartLineUnitOfMeasureAndQuantity(context, newCart, salesTransaction, validationResults);

            CartWorkflowHelper.ValidateCustomerAccount(context, newCart, salesTransaction);
            AccountDepositHelper.ValidateCustomerAccountDepositTransaction(context, newCart, validationResults);

            if (cartType == CartType.IncomeExpense)
            {
                ValidateIncomeExpenseTransaction(context, newCart, validationResults);
            }

            // Loyalty validations
            CartWorkflowHelper.ValidateLoyaltyCard(context, newCart, salesTransaction, validationResults);

            // Customer order validations
            CustomerOrderWorkflowHelper.CustomerOrderCartValidations(context, newCart, salesTransaction, returnedSalesTransaction, validationResults);

            // Invoice lines validations
            CartWorkflowHelper.ValidateTransactionWithInvoiceLines(context, newCart, salesTransaction, validationResults);

            if (validationResults.HasErrors)
            {
                foreach (var validationResult in validationResults.ValidationResults)
                {
                    foreach (var validationFailure in validationResult.Value)
                    {
                        NetTracer.Information("Cart line validation failure. Line id: {0}. Details: {1}", validationResult.Key, validationFailure.ToString());
                    }
                }

                throw new CartValidationException(DataValidationErrors.AggregateValidationError, newCart.Id, validationResults, "Validation failures occurred when verifying cart for update.");
            }
        }

        /// <summary>
        /// Validates the save cart lines request.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="request">The request.</param>
        public static void ValidateSaveCartLinesRequest(RequestContext context, SaveCartLinesRequest request)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(request, "request");

            ValidateUpdateCartRequest(context, context.GetSalesTransaction(), context.GetReturnedSalesTransaction(), request.Cart, isGiftCardOperation: false);
        }

        /// <summary>
        /// Performs the save cart request on the context's sales transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="request">The save cart request.</param>
        [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Nothing obvious to refactor.")]
        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Nothing obvious to refactor.")]
        public static void PerformSaveCartOperations(RequestContext context, SaveCartRequest request)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.Cart, "request.Cart");

            NetTracer.Information("CartWorkflowHelper.PerformSaveCartOperations(): TransactionId = {0}, CustomerId = {1}", request.Cart.Id, request.Cart.CustomerId);

            // Look at all the lines
            Dictionary<string, SalesLine> salesLineByLineId = context.GetSalesTransaction().SalesLines
                .ToDictionary(sl => sl.LineId, sl => sl);

            // Add or Update transaction level reason code lines.
            ReasonCodesWorkflowHelper.AddOrUpdateReasonCodeLinesOnTransaction(context.GetSalesTransaction(), request.Cart);

            // Add or update affiliations when add or change customer from the Cart if the customer contains affiliations.
            CartWorkflowHelper.AddOrUpdateAffiliationLinesFromCustomer(context, context.GetSalesTransaction(), request.Cart);

            // Set OriginalPrice value
            foreach (CartLine cartLine in request.Cart.CartLines)
            {
                SalesLine salesLine = null;
                if (salesLineByLineId.TryGetValue(cartLine.LineId, out salesLine)
                    && cartLine.IsPriceOverridden
                    && !salesLine.IsPriceOverridden
                    && cartLine.LineData.OriginalPrice == null)
                {
                    cartLine.OriginalPrice = salesLine.Price;
                }
            }

            CartWorkflowHelper.CheckChangeUnitOfMeasureOperation(context, request.Cart, salesLineByLineId.Values);

            // Update address information on the request
            CustomerOrderWorkflowHelper.FillAddressInformation(context, context.GetSalesTransaction(), request.Cart, salesLineByLineId);

            // copy properties from cart to sales transaction but ignore nulls (this enables delta cart updates)
            context.GetSalesTransaction().CopyPropertiesFrom(request.Cart);

            OrderWorkflowHelper.FillTransactionWithContextData(context, context.GetSalesTransaction());

            context.GetSalesTransaction().AttributeValues.Clear();
            foreach (AttributeValueBase attributeValueBase in request.Cart.AttributeValues)
            {
                if (attributeValueBase != null)
                {
                    context.GetSalesTransaction().AttributeValues.Add(attributeValueBase);
                }
            }

            // check for reason codes on total discount
            if (context.GetSalesTransaction().TotalManualDiscountAmount != 0 || context.GetSalesTransaction().TotalManualDiscountPercentage != 0)
            {
                ReasonCodesWorkflowHelper.CalculateRequiredReasonCodesOnTransaction(context, ReasonCodeSourceType.TotalDiscount);
            }

            // Perform save operations for lines
            CartWorkflowHelper.PerformSaveCartLineOperations(context, request, salesLineByLineId);

            // Save the tax override codes on the transaction and lines.
            CartWorkflowHelper.UpdateTaxOverrideCodes(context, context.GetSalesTransaction(), request.Cart);

            // Perform save income (or) expense lines
            context.GetSalesTransaction().IncomeExpenseLines.Clear();
            context.GetSalesTransaction().IncomeExpenseLines.AddRange(request.Cart.IncomeExpenseLines);

            UpdateIncomeExpenseTransactionFields(context, context.GetSalesTransaction(), request.Cart);

            // Set default transaction data for account deposit cart
            UpdateAccountDepositTransactionFields(context, context.GetSalesTransaction(), request.Cart);

            // Set default data on all sales lines
            SetDefaultDataOnSalesLines(context, salesLineByLineId.Values);

            // Refresh the sales lines
            context.GetSalesTransaction().SalesLines.Clear();
            context.GetSalesTransaction().SalesLines.AddRange(salesLineByLineId.Values);

            // Gets the inventory site identifier for the sales transaction inventory location identifier.
            CartWorkflowHelper.GetSalesTransactionWarehouseInformation(context);

            // Calculate required reason codes on transaction level.
            ReasonCodesWorkflowHelper.CalculateRequiredReasonCodesOnTransaction(context, ReasonCodeSourceType.None);

            // Update customer order related fields
            CustomerOrderWorkflowHelper.UpdateCustomerOrderFieldsOnSave(context, context.GetSalesTransaction(), request.Cart);

            // Refresh transaction level affiliation lines.
            CartWorkflowHelper.AddOrUpdateAffiliationLines(context, request.Cart.AffiliationLines);

            // Refresh loyalty groups and tiers on the transaction.
            CartWorkflowHelper.RefreshSalesLoyaltyTierLines(context);
        }

        /// <summary>
        /// Sets the default data on sales lines.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="salesLines">The sales lines.</param>
        public static void SetDefaultDataOnSalesLines(RequestContext context, IEnumerable<SalesLine> salesLines)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(salesLines, "salesLines");

            IEnumerable<Item> items = GetItemsForSalesLines(context, salesLines);
            Dictionary<string, Item> itemsByItemId = items.ToDictionary(i => i.ItemId, i => i, StringComparer.OrdinalIgnoreCase);

            var itemUnitConversions = new HashSet<ItemUnitConversion>();
            foreach (SalesLine salesLine in salesLines)
            {
                if (!string.IsNullOrWhiteSpace(salesLine.SalesOrderUnitOfMeasure))
                {
                    var conversion = new ItemUnitConversion
                    {
                        FromUnitOfMeasure = salesLine.SalesOrderUnitOfMeasure,
                        ToUnitOfMeasure = salesLine.OriginalSalesOrderUnitOfMeasure,
                        ItemId = salesLine.ItemId
                    };
                    itemUnitConversions.Add(conversion);
                }
            }

            var unitOfMeasureConversionMap = new Dictionary<ItemUnitConversion, UnitOfMeasureConversion>();

            if (itemUnitConversions.Any())
            {
                unitOfMeasureConversionMap = CartWorkflowHelper.GetUnitOfMeasureConversions(context, itemUnitConversions).ToDictionary(key =>
                    new ItemUnitConversion
                    {
                        FromUnitOfMeasure = key.FromUnitOfMeasureId,
                        ToUnitOfMeasure = key.ToUnitOfMeasureId,
                        ItemId = key.ItemId
                    });
            }

            foreach (SalesLine salesLine in salesLines)
            {
                // Keep the current field values for non-product cart lines, such as gift card, invoice line, account deposit, etc.
                if (salesLine.IsProductLine)
                {
                    Item cartItem = null;
                    if (itemsByItemId.TryGetValue(salesLine.ItemId, out cartItem))
                    {
                        SetSalesLineDefaultsFromItemData(salesLine, cartItem);
                    }
                    else
                    {
                        NetTracer.Warning("Item information for item id {0} was not found.", salesLine.ItemId);
                    }

                    // Set Unit of Measure conversion if the convert to unit of measure specified.
                    if (!string.IsNullOrWhiteSpace(salesLine.SalesOrderUnitOfMeasure))
                    {
                        UnitOfMeasureConversion unitOfMeasureConversion;
                        var itemUnitConversion = new ItemUnitConversion
                        {
                            FromUnitOfMeasure = salesLine.SalesOrderUnitOfMeasure,
                            ToUnitOfMeasure = salesLine.OriginalSalesOrderUnitOfMeasure,
                            ItemId = salesLine.ItemId
                        };

                        // Set unit of measure conversion value and set default if it cannot find.
                        if (unitOfMeasureConversionMap.TryGetValue(itemUnitConversion, out unitOfMeasureConversion))
                        {
                            salesLine.UnitOfMeasureConversion = unitOfMeasureConversion;
                        }
                        else
                        {
                            salesLine.UnitOfMeasureConversion = UnitOfMeasureConversion.CreateDefaultUnitOfMeasureConversion();
                        }
                    }

                    switch (context.GetSalesTransaction().CartType)
                    {
                        case CartType.Checkout:
                        case CartType.Shopping:
                        case CartType.IncomeExpense:
                            SetSalesLineDefaultsFromOrderHeader(salesLine, context.GetSalesTransaction());
                            break;

                        case CartType.CustomerOrder:
                            break;

                        case CartType.None:
                        default:
                            throw new NotSupportedException(string.Format("Cart type {0} is not supported.", context.GetSalesTransaction().CartType));
                    }
                }
            }
        }

        /// <summary>
        /// Updates income expense transaction fields.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="salesTransaction">Set sales transaction.</param>
        /// <param name="cart">Set cart received on the request.</param>
        public static void UpdateIncomeExpenseTransactionFields(RequestContext context, SalesTransaction salesTransaction, Cart cart)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(cart, "cart");
            ThrowIf.Null(salesTransaction, "salesTransaction");

            // only make changes if it's a income expense cart.
            if (salesTransaction.CartType != CartType.IncomeExpense)
            {
                return;
            }

            salesTransaction.TransactionType = SalesTransactionType.IncomeExpense;
        }

        /// <summary>
        /// Updates Account Deposit Transaction fields.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="salesTransaction">Set sales transaction.</param>
        /// <param name="cart">Set cart received on the request.</param>
        public static void UpdateAccountDepositTransactionFields(RequestContext context, SalesTransaction salesTransaction, Cart cart)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(cart, "cart");
            ThrowIf.Null(salesTransaction, "salesTransaction");

            // only make changes if it's an account deposit cart.
            if (salesTransaction.CartType != CartType.AccountDeposit)
            {
                return;
            }

            salesTransaction.TransactionType = SalesTransactionType.CustomerAccountDeposit;
        }

        /// <summary>
        /// Converts from a sales transaction to a cart.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="salesTransaction">The sales transaction to convert from.</param>
        /// <returns>The populated cart.</returns>
        public static Cart ConvertToCart(RequestContext context, SalesTransaction salesTransaction)
        {
            if (salesTransaction == null)
            {
                return null;
            }

            Cart cart = new Cart();
            cart.CopyPropertiesFrom(salesTransaction);
            cart.CartStatus = GetCartStatus(salesTransaction);

            cart.AttributeValues.AddRange(salesTransaction.AttributeValues);
            cart.DiscountCodes.AddRange(salesTransaction.DiscountCodes);
            cart.ChargeLines.AddRange(salesTransaction.ChargeLines);
            cart.TaxOverrideCode = salesTransaction.TaxOverrideCode;

            if (context.GetChannelConfiguration().DisplayTaxPerTaxComponent)
            {
                InsertTaxDetailIntoCart(context, cart, salesTransaction);
            }

            foreach (SalesLine salesLine in salesTransaction.SalesLines)
            {
                CartLine cartLine = new CartLine()
                {
                    LineId = salesLine.LineId,
                    ProductId = salesLine.ProductId,
                    ItemId = salesLine.ItemId,
                    InventoryDimensionId = salesLine.InventoryDimensionId,
                    TaxOverrideCode = salesLine.TaxOverrideCode
                };

                cartLine.LineData.CopyPropertiesFrom(salesLine);

                cartLine.LineData.DiscountLines.Clear();
                cartLine.LineData.DiscountLines.AddRange(salesLine.DiscountLines);

                cartLine.LineData.ReasonCodeLines.Clear();
                cartLine.LineData.ReasonCodeLines.AddRange(salesLine.ReasonCodeLines);

                cartLine.LineData.ChargeLines.Clear();
                cartLine.LineData.ChargeLines.AddRange(salesLine.ChargeLines);

                cartLine.LineData.LineIdsLinkedProductMap.Clear();
                cartLine.LineData.LineIdsLinkedProductMap.AddRange(salesLine.LineIdsLinkedProductMap);

                cartLine.LineData.LinkedParentLineId = salesLine.LinkedParentLineId;
                cartLine.TaxRatePercent = salesLine.TaxRatePercent;
                cart.CartLines.Add(cartLine);
            }

            cart.IncomeExpenseLines.AddRange(salesTransaction.IncomeExpenseLines);
            cart.TenderLines.AddRange(salesTransaction.TenderLines);
            cart.ReasonCodeLines.AddRange(salesTransaction.ReasonCodeLines);

            CopyAffiliationLoyaltyTierToCart(salesTransaction, cart);

            return cart;
        }

        /// <summary>
        /// Converts from a cart to a sales transaction.
        /// </summary>
        /// <param name="cart">The cart to convert from.</param>
        /// <returns>The populated sales transaction.</returns>
        public static SalesTransaction ConvertToSalesTransaction(Cart cart)
        {
            if (cart == null)
            {
                return null;
            }

            SalesTransaction salesTransaction = new SalesTransaction();
            salesTransaction.CopyPropertiesFrom(cart);

            salesTransaction.AttributeValues.AddRange(cart.AttributeValues);
            salesTransaction.DiscountCodes.AddRange(cart.DiscountCodes);
            salesTransaction.ChargeLines.AddRange(cart.ChargeLines);
            salesTransaction.TaxOverrideCode = cart.TaxOverrideCode;

            foreach (CartLine cartLine in cart.CartLines)
            {
                SalesLine salesLine = new SalesLine()
                {
                    LineId = cartLine.LineId,
                    ProductId = cartLine.ProductId,
                    ItemId = cartLine.ItemId,
                    InventoryDimensionId = cartLine.InventoryDimensionId,
                    TaxOverrideCode = cartLine.TaxOverrideCode
                };

                salesLine.CopyPropertiesFrom(cartLine.LineData);

                salesLine.DiscountLines.Clear();
                salesLine.DiscountLines.AddRange(cartLine.LineData.DiscountLines);

                salesLine.ReasonCodeLines.Clear();
                salesLine.ReasonCodeLines.AddRange(cartLine.LineData.ReasonCodeLines);

                salesLine.ChargeLines.Clear();
                salesLine.ChargeLines.AddRange(cartLine.LineData.ChargeLines);

                salesLine.LineIdsLinkedProductMap.Clear();
                salesLine.LineIdsLinkedProductMap.AddRange(cartLine.LineData.LineIdsLinkedProductMap);

                salesLine.LinkedParentLineId = cartLine.LineData.LinkedParentLineId;
                salesLine.TaxRatePercent = cartLine.LineData.TaxRatePercent;

                salesTransaction.SalesLines.Add(salesLine);
            }

            salesTransaction.IncomeExpenseLines.AddRange(cart.IncomeExpenseLines);
            salesTransaction.TenderLines.AddRange(cart.TenderLines);
            salesTransaction.ReasonCodeLines.AddRange(cart.ReasonCodeLines);

            CopyAffiliationLoyaltyTierToSalesTransaction(cart, salesTransaction);

            return salesTransaction;
        }

        /// <summary>
        /// Converts CartTenderLine to TenderLine.
        /// </summary>
        /// <param name="cartTenderLine">The cart tender line.</param>
        /// <returns>The tender line.</returns>
        public static TenderLine ConvertToTenderLine(CartTenderLine cartTenderLine)
        {
            ThrowIf.Null(cartTenderLine, "cartTenderLine");

            TenderLine tenderLine = new TenderLine
            {
                TenderLineId = cartTenderLine.TenderLineId
            };

            tenderLine.CopyPropertiesFrom(cartTenderLine);

            // CopyPropertiesFrom() copied all properties including those not available on destination class (TenderLine does not have PaymentCard property).
            // In order to avoid persisting payment card information as part of serialized TenderLine setting it to null.
            tenderLine[CartTenderLine.PaymentCardColumn] = null;

            if (cartTenderLine.ReasonCodeLines != null &&
                cartTenderLine.ReasonCodeLines.Any())
            {
                tenderLine.ReasonCodeLines.Clear();

                // Retrieves existing reason code lines here for only update so that 
                // the new reason code lines can be added through AddOrUpdateReasonCodeLinesOnTenderLine()
                IEnumerable<ReasonCodeLine> existingReasonCodeLines = cartTenderLine.ReasonCodeLines.Where(l => !string.IsNullOrWhiteSpace(l.LineId));
                tenderLine.ReasonCodeLines.AddRange(existingReasonCodeLines);
            }

            return tenderLine;
        }

        /// <summary>
        /// Calculates the various totals depending on the specified calculation mode.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="requestedMode">The calculation mode.</param>
        public static void Calculate(RequestContext context, CalculationModes? requestedMode)
        {
            CartWorkflowHelper.Calculate(context, requestedMode, false);
        }

        /// <summary>
        /// Calculates the various totals depending on the specified calculation mode.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="requestedMode">The calculation mode.</param>
        /// <param name="restoreItemPrices">The boolean indicating if prices should be restored ignoring any changes.</param>
        public static void Calculate(RequestContext context, CalculationModes? requestedMode, bool restoreItemPrices)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "SalesTransaction");

            CalculationModes mode = requestedMode ?? GetCalculationModes(context);

            if (mode == CalculationModes.None)
            {
                // No calculations are required.
                return;
            }

            if (mode.HasFlag(CalculationModes.Prices) || mode.HasFlag(CalculationModes.Discounts))
            {
                CartWorkflowHelper.AssociateCatalogsToSalesLines(context);
            }

            if (mode.HasFlag(CalculationModes.Prices))
            {
                CalculatePrices(context, restoreItemPrices);
            }

            if (mode.HasFlag(CalculationModes.Discounts))
            {
                CalculateDiscounts(context);
            }

            if (mode.HasFlag(CalculationModes.Charges))
            {
                CalculateCharges(context);
            }

            if (mode.HasFlag(CalculationModes.Taxes))
            {
                CalculateTaxes(context);
            }

            if (mode.HasFlag(CalculationModes.Totals))
            {
                CalculateTotals(context);
                RoundTotals(context);
            }

            if (mode.HasFlag(CalculationModes.Deposit))
            {
                CustomerOrderWorkflowHelper.CalculateDeposit(context, context.GetSalesTransaction());
            }

            if (mode.HasFlag(CalculationModes.AmountDue))
            {
                CalculateAmountsPaidAndDue(context, context.GetSalesTransaction());
            }
        }

        /// <summary>
        /// Gets the listings in cart lines.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="cartLines">The cart lines.</param>
        /// <returns>A dictionary of listing id to listings mapping.</returns>
        public static Dictionary<long, Product> GetProductsInCartLines(RequestContext context, IEnumerable<CartLine> cartLines)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(cartLines, "cartLines");

            // Get referenced productIds for CartLines to be created
            var productSearchCriteria = new ProductSearchCriteria(context.GetPrincipal().ChannelId);

            // Minimal level is required to retrieve product rules.
            productSearchCriteria.DataLevel = CommerceEntityDataLevel.Minimal;
            productSearchCriteria.Ids = cartLines.Where(cl => cl.LineData.IsProductLine).Select(cl => cl.LineData.ProductId).Distinct().ToList();

            // We'll restrict the search here to ids only. Bail if there aren't any.
            if (productSearchCriteria.Ids.Count < 1)
            {
                return new Dictionary<long, Product>(0);
            }

            var paging = new PagingInfo(productSearchCriteria.Ids.Count, 0);
            var settings = new QueryResultSettings(paging);
            var results = context.Runtime.Execute<ProductSearchServiceResponse>(
                    new ProductSearchServiceRequest(productSearchCriteria, settings), context).ProductSearchResult;

            var lookupIdMap = results.ProductIdLookupMap;
            var denormalizedProducts = new Dictionary<long, Product>(lookupIdMap.Count);

            var productMap = new Dictionary<long, Product>(results.Results.Count);
            foreach (var product in results.Results)
            {
                productMap[product.RecordId] = product;
            }

            long lookupId;
            Dictionary<long, string> missingProductIdItemIdMap = new Dictionary<long, string>();
            foreach (var originalProductId in productSearchCriteria.Ids)
            {
                if (!lookupIdMap.TryGetValue(originalProductId, out lookupId))
                {
                    // Searching for first match of product because all variants will be covered later when downloading product information.
                    string itemId = cartLines.First(l => l.ProductId == originalProductId).ItemId;
                    if (!missingProductIdItemIdMap.ContainsKey(originalProductId))
                    {
                        missingProductIdItemIdMap.Add(originalProductId, itemId);
                    }

                    continue;
                }

                Product product = null;
                if (!productMap.TryGetValue(lookupId, out product))
                {
                    continue;
                }

                denormalizedProducts.Add(originalProductId, product);
            }

            // If there were any product identifiers that were not found, we will try to fetch them from AX.
            if (missingProductIdItemIdMap.Any())
            {
                var remoteProducts = CartWorkflowHelper.DownloadAndRetrieveRemoteProducts(context, missingProductIdItemIdMap);
                foreach (Product remoteProduct in remoteProducts)
                {
                    denormalizedProducts.Add(remoteProduct.RecordId, remoteProduct);
                }
            }

            return denormalizedProducts;
        }

        /// <summary>
        /// The a sales line from the sales transaction by the line number.
        /// </summary>
        /// <param name="salesTransaction">The sales transaction.</param>
        /// <param name="lineNumber">The line number.</param>
        /// <returns>The sales line.</returns>
        public static SalesLine GetSalesLineByNumber(SalesTransaction salesTransaction, decimal lineNumber)
        {
            ThrowIf.Null(salesTransaction, "salesTransaction");

            return salesTransaction.SalesLines
                .Where(sl => sl.LineNumber == lineNumber)
                .Select(sl => sl).FirstOrDefault();
        }

        /// <summary>
        /// Sets the sales line based on returned sales line.
        /// </summary>
        /// <param name="salesLine">The sales line.</param>
        /// <param name="returnedSalesLine">The returned sales line.</param>
        /// <param name="returnTransactionId">The return transaction id.</param>
        /// <param name="quantity">The quantity.</param>
        public static void SetSalesLineBasedOnReturnedSalesLine(SalesLine salesLine, SalesLine returnedSalesLine, string returnTransactionId, decimal quantity)
        {
            ThrowIf.Null(salesLine, "salesLine");
            ThrowIf.Null(returnedSalesLine, "returnedSalesLine");

            decimal originalLineNumber = salesLine.LineNumber;
            string originalStore = salesLine.Store;
            string originalTerminalId = salesLine.TerminalId;
            string originalInventoryLocationId = salesLine.InventoryLocationId;
            salesLine.CopyPropertiesFrom(returnedSalesLine);

            // add a line identifier if none
            if (string.IsNullOrWhiteSpace(salesLine.LineId))
            {
                AssignUniqueLineId(salesLine);
            }

            // Restore original sales line values
            salesLine.LineNumber = originalLineNumber;
            salesLine.Store = originalStore;
            salesLine.TerminalId = originalTerminalId;
            salesLine.InventoryLocationId = originalInventoryLocationId;

            // Mark the item that it's an item that was returned with a receipt.  That means that the item line is not eligible
            // for price changes & overrides, discounts, qty changes above the allowed return qty, etc....
            salesLine.IsReturnByReceipt = true;

            // Set return fields
            salesLine.ReturnTransactionId = returnTransactionId;
            salesLine.ReturnLineNumber = returnedSalesLine.LineNumber;
            salesLine.ReturnChannelId = returnedSalesLine.ReturnChannelId;
            salesLine.ReturnStore = returnedSalesLine.Store;
            salesLine.ReturnTerminalId = returnedSalesLine.TerminalId;

            // Copy quantity from the new cart line
            salesLine.Quantity = quantity;

            // Reconstruct discount lines
            // Discount - Handling, copy from returned sales line
            if (returnedSalesLine.DiscountLines.Count > 0)
            {
                // Copy the same discount lines
                foreach (DiscountLine returnDisLine in returnedSalesLine.DiscountLines)
                {
                    DiscountLine salesLineDiscount = returnDisLine.Clone<DiscountLine>();
                    salesLine.DiscountLines.Add(salesLineDiscount);
                }
            }
        }

        /// <summary>
        /// Sets the default properties on the sales line from the order header.
        /// </summary>
        /// <param name="salesLine">The sales line from the sales transaction.</param>
        /// <param name="salesTransaction">The sales transaction.</param>
        public static void SetSalesLineDefaultsFromOrderHeader(SalesLine salesLine, SalesTransaction salesTransaction)
        {
            ThrowIf.Null(salesLine, "salesLine");
            ThrowIf.Null(salesTransaction, "salesTransaction");

            if (string.IsNullOrWhiteSpace(salesLine.DeliveryMode))
            {
                salesLine.DeliveryMode = salesTransaction.DeliveryMode;
            }

            if (salesLine.ShippingAddress == null && salesTransaction.ShippingAddress != null)
            {
                salesLine.ShippingAddress = new Address();
                salesLine.ShippingAddress.CopyPropertiesFrom(salesTransaction.ShippingAddress);
            }

            if (string.IsNullOrWhiteSpace(salesLine.InventoryLocationId))
            {
                salesLine.InventoryLocationId = salesTransaction.InventoryLocationId;
            }

            if (string.IsNullOrEmpty(salesLine.Store))
            {
                salesLine.Store = salesTransaction.StoreId;
            }
        }

        /// <summary>
        /// Sets the missing sales tax group identifier.
        /// </summary>
        /// <param name="context">The context.</param>
        public static void SetSalesTaxGroup(RequestContext context)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "salesTransaction");

            SalesTransaction salesTransaction = context.GetSalesTransaction();

            // On customer order returns, we don't change tax groups as they come populated from the headquarters
            // Tax groups have been set during order creation and could be from a different store than the one the return is being made
            if (salesTransaction.CartType == CartType.CustomerOrder
                && salesTransaction.CustomerOrderMode == CustomerOrderMode.Return)
            {
                return;
            }

            SetSalesTaxGroupOnNonReturn(context);
        }

        /// <summary>
        /// Gets all the periodic discounts on the context's sales transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        public static void GetAllPeriodicDiscounts(RequestContext context)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");

            NetTracer.Information("CartWorkflowHelper.GetAllPeriodicDiscounts(): TransactionId = {0}, CustomerId = {1}", context.GetSalesTransaction().Id, context.GetSalesTransaction().CustomerId);

            GetAllPeriodicDiscountsServiceRequest request = new GetAllPeriodicDiscountsServiceRequest(context.GetSalesTransaction());
            GetPriceServiceResponse response = context.Execute<GetPriceServiceResponse>(request);

            context.SetSalesTransaction(response.Transaction);
        }

        /// <summary>
        /// Add or updates tender lines on the cart.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="updatedTenderLine">The tender line to be updated or added.</param>
        public static void AddOrUpdateTenderLine(RequestContext context, TenderLineBase updatedTenderLine)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(updatedTenderLine, "updatedTenderLine");

            TenderLine tenderLineOnCart;
            SalesTransaction salesTransaction = context.GetSalesTransaction();

            // Keeps track of the tender line being created or updated. 
            Dictionary<string, TenderLine> tenderLineByLineId = salesTransaction.TenderLines.ToDictionary(sl => sl.TenderLineId, sl => sl);

            if (updatedTenderLine.TenderLineId == null || !tenderLineByLineId.ContainsKey(updatedTenderLine.TenderLineId))
            {
                // new payment
                if (updatedTenderLine.Status == TenderLineStatus.Voided)
                {
                    throw new PaymentException(PaymentErrors.InvalidPaymentRequest, string.Format("Voided payment cannot be added.Transaction Id : {0}, Tender Line Id : {1}", salesTransaction.Id, updatedTenderLine.TenderLineId));
                }

                AccountDepositHelper.ValidateCustomerAccountDepositPaymentRestrictions(context, salesTransaction, updatedTenderLine);

                if (updatedTenderLine is TenderLine && ((TenderLine)updatedTenderLine).IsPreProcessed)
                {
                    tenderLineOnCart = updatedTenderLine as TenderLine;
                }
                else
                {
                    tenderLineOnCart = CartWorkflowHelper.ConvertToTenderLine((CartTenderLine)updatedTenderLine);
                }

                if (tenderLineOnCart.Status != TenderLineStatus.Historical)
                {
                    // Authorize the tender line and add it to cart.
                    AuthorizePaymentServiceRequest authorizeRequest;
                    if (tenderLineOnCart.IsPreProcessed)
                    {
                        authorizeRequest = new AuthorizePaymentServiceRequest(tenderLineOnCart, paymentCard: null, skipLimitValidation: false);
                    }
                    else
                    {
                        authorizeRequest = new AuthorizePaymentServiceRequest(tenderLineOnCart, ((CartTenderLine)updatedTenderLine).PaymentCard);
                    }

                    IRequestHandler paymentManagerHandler = context.Runtime.GetRequestHandler(authorizeRequest.GetType(), ServiceTypes.PaymentManagerService);
                    AuthorizePaymentServiceResponse authorizeResponse = context.Execute<AuthorizePaymentServiceResponse>(authorizeRequest, paymentManagerHandler);

                    // Add the tender line to cart
                    if (authorizeResponse.TenderLine == null)
                    {
                        throw new PaymentException(PaymentErrors.UnableToAuthorizePayment, "Payment service did not return tender line.");
                    }

                    tenderLineOnCart = authorizeResponse.TenderLine;
                }

                tenderLineOnCart.TenderLineId = Guid.NewGuid().ToString("N");
                tenderLineByLineId.Add(tenderLineOnCart.TenderLineId, tenderLineOnCart);
            }
            else
            {
                // existing tender line
                tenderLineOnCart = tenderLineByLineId[updatedTenderLine.TenderLineId];

                // If the pre-prrocessed tender line already exists, but the status has changed to Committed, remove the existing tender line from the cart, and add the new one.
                if (updatedTenderLine.Status == TenderLineStatus.Committed && tenderLineOnCart.Status == TenderLineStatus.PendingCommit)
                {
                    if (tenderLineOnCart.IsPreProcessed)
                    {
                        var captureRequest = new CapturePaymentServiceRequest((TenderLine)updatedTenderLine);
                        IRequestHandler paymentManagerHandler = context.Runtime.GetRequestHandler(captureRequest.GetType(), ServiceTypes.PaymentManagerService);
                        CapturePaymentServiceResponse response = context.Execute<CapturePaymentServiceResponse>(captureRequest, paymentManagerHandler);
                        tenderLineOnCart.Status = response.TenderLine.Status;
                        tenderLineOnCart.Authorization = response.TenderLine.Authorization;
                        tenderLineOnCart.IsVoidable = response.TenderLine.IsVoidable;
                    }
                    else
                    {
                        throw new PaymentException(PaymentErrors.InvalidPaymentRequest, "Tender line can be captured explicitly only if it is pre-processed.");
                    }
                }

                CartLineValidationResults validationResults = new CartLineValidationResults();

                // Validate readonly properties
                foreach (DataValidationFailure failure in ReadOnlyAttribute.CheckReadOnlyProperties(updatedTenderLine, tenderLineOnCart))
                {
                    validationResults.AddLineResult(0, failure);
                }

                if (validationResults.HasErrors)
                {
                    foreach (var validationResult in validationResults.ValidationResults)
                    {
                        foreach (var validationFailure in validationResult.Value)
                        {
                            NetTracer.Information("Tender line validation failure. Line id: {0}. Details: {1}", validationResult.Key, validationFailure.ToString());
                        }
                    }

                    throw new CartValidationException(DataValidationErrors.AggregateValidationError, salesTransaction.Id, validationResults, "Validation failures occurred when verifying tender line for update.");
                }
            }

            // Update the tender line with signature.
            tenderLineOnCart.SignatureData = updatedTenderLine.SignatureData;

            // Add or update reason code lines on tender line.
            ReasonCodesWorkflowHelper.AddOrUpdateReasonCodeLinesOnTenderLine(tenderLineOnCart, updatedTenderLine, salesTransaction.Id);

            // Calculate the required reason codes on the tender line.
            ReasonCodesWorkflowHelper.CalculateRequiredReasonCodesOnTenderLine(context, tenderLineOnCart, ReasonCodeSourceType.None);

            // Update the tender lines.
            salesTransaction.TenderLines.Clear();
            salesTransaction.TenderLines.AddRange(tenderLineByLineId.Values);

            // Update amount due
            CartWorkflowHelper.Calculate(context, CalculationModes.AmountDue);
        }

        /// <summary>
        /// Voids tender lines on the cart.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="tenderLineBase">The tender line to be updated or added.</param>
        public static void VoidTenderLine(RequestContext context, TenderLineBase tenderLineBase)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(tenderLineBase, "tenderLineBase");

            TenderLine tenderLineOnCart;
            SalesTransaction salesTransaction = context.GetSalesTransaction();

            // Keeps track of the tender line being created or updated. 
            Dictionary<string, TenderLine> tenderLineByLineId = salesTransaction.TenderLines.ToDictionary(sl => sl.TenderLineId, sl => sl);

            // existing tender line
            tenderLineOnCart = tenderLineByLineId[tenderLineBase.TenderLineId];
            if (tenderLineOnCart.Status == TenderLineStatus.Voided)
            {
                throw new PaymentException(PaymentErrors.PaymentAlreadyVoided, "The payment has been voided already.");
            }

            var voidPaymentServiceRequest = new VoidPaymentServiceRequest(tenderLineOnCart);
            IRequestHandler paymentManagerService = context.Runtime.GetRequestHandler(voidPaymentServiceRequest.GetType(), ServiceTypes.PaymentManagerService);

            var response = context.Execute<VoidPaymentServiceResponse>(voidPaymentServiceRequest, paymentManagerService);

            tenderLineByLineId[response.TenderLine.TenderLineId] = response.TenderLine;
            tenderLineOnCart = response.TenderLine;

            // Calculate the required reason codes on the tender line for voiding payments.
            ReasonCodesWorkflowHelper.CalculateRequiredReasonCodesOnTenderLine(context, tenderLineOnCart, ReasonCodeSourceType.VoidPayment);

            // Update the tender lines.
            salesTransaction.TenderLines.Clear();
            salesTransaction.TenderLines.AddRange(tenderLineByLineId.Values);

            // Update amount due
            CartWorkflowHelper.Calculate(context, CalculationModes.AmountDue);
        }

        /// <summary>
        /// Sets the loyalty card in the cart from the customer.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="cart">The cart with updates or new cart from the client.</param>
        public static void SetLoyaltyCardFromCustomer(RequestContext context, Cart cart)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(cart, "cart");

            if (!string.IsNullOrWhiteSpace(cart.CustomerId) && string.IsNullOrWhiteSpace(cart.LoyaltyCardId))
            {
                // Find the loyalty cards by customer number
                var request = new GetCustomerLoyaltyCardsDataRequest(cart.CustomerId);
                var response = context.Runtime.Execute<EntityDataServiceResponse<LoyaltyCard>>(request, context);

                if (response != null && response.EntityCollection != null && response.EntityCollection.Count > 0)
                {
                    List<LoyaltyCard> activeLoyaltyCards = (from c in response.EntityCollection
                                                            where c.CardTenderType != LoyaltyCardTenderType.Blocked
                                                            select c).ToList();

                    // Only when the customer has a single active loyalty card, we add the card to the transaction.
                    if (activeLoyaltyCards.Count == 1)
                    {
                        cart.LoyaltyCardId = activeLoyaltyCards.Single().CardNumber;
                    }
                }
            }
        }

        /// <summary>
        /// Add cart lines to the given cart.
        /// </summary>
        /// <param name="cart">Cart object.</param>
        /// <param name="cartLines">Cart lines to be added to cart.</param>
        public static void AddCartLines(Cart cart, IEnumerable<CartLine> cartLines)
        {
            ThrowIf.Null(cart, "cart");
            ThrowIf.Null(cartLines, "cartLines");

            foreach (var cartLine in cartLines)
            {
                cart.CartLines.Add(cartLine);
            }
        }

        /// <summary>
        /// Creates the cart line.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="barcode">Barcode value.</param>
        /// <param name="itemId">Item identifier.</param>
        /// <param name="variantId">Variant identifier.</param>
        /// <param name="unitOfMeasureSymbol">Unit of measure.</param>
        /// <param name="quantity">Cart line quantity value.</param>
        /// <param name="barcodePrice">Barcode price value.</param>
        /// <returns>Returns the cart line object.</returns>
        public static CartLine CreateCartLine(RequestContext context, string barcode, string itemId, string variantId, string unitOfMeasureSymbol, decimal quantity, decimal barcodePrice)
        {
            ThrowIf.NullOrWhiteSpace(itemId, "itemId");

            ItemDataManager itemDataManager = new ItemDataManager(context);

            string inventDimId = string.Empty;
            long productVariantId = 0;

            if (!string.IsNullOrWhiteSpace(variantId))
            {
                ProductVariant variant = itemDataManager.GetVariantByVariantId(variantId, new ColumnSet());
                inventDimId = variant.InventoryDimensionId;
                productVariantId = variant.DistinctProductVariantId;
            }

            var cartLine = new CartLine
            {
                Barcode = barcode,
                LineId = string.Empty,
                UnitOfMeasureSymbol = unitOfMeasureSymbol,
                LineData = CreateCartLineData(context, itemId, barcode, 0, inventDimId, unitOfMeasureSymbol, quantity, barcodePrice),
                ProductId = productVariantId
            };

            return cartLine;
        }

        /// <summary>
        /// Creates the cart line data.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="itemId">Item identifier.</param>
        /// <param name="barcode">Barcode value.</param>
        /// <param name="productId">Product identifier.</param>
        /// <param name="inventDimId">Invent dimension identifier.</param>
        /// <param name="unitOfMeasureSymbol">Unit of measure.</param>
        /// <param name="quantity">Cart line quantity value.</param>
        /// <param name="barcodePrice">Barcode price value.</param>
        /// <returns>Returns the cart line data.</returns>
        public static CartLineData CreateCartLineData(RequestContext context, string itemId, string barcode, long productId, string inventDimId, string unitOfMeasureSymbol, decimal quantity, decimal barcodePrice)
        {
            ThrowIf.NullOrWhiteSpace(itemId, "itemId");

            var cartlineData = new CartLineData
            {
                ItemId = itemId,
                Barcode = barcode,
                UnitOfMeasureSymbol = unitOfMeasureSymbol,
                ProductId = productId,
                InventoryDimensionId = inventDimId,
                Quantity = quantity
            };

            if (barcodePrice > 0)
            {
                cartlineData.Price = barcodePrice;
            }

            return cartlineData;
        }

        /// <summary>
        /// Gets the variant details by variant id.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="variantId">Variant identifier.</param>
        /// <returns>Returns the variant object.</returns>
        public static ProductVariant GetVariantById(RequestContext context, string variantId)
        {
            ProductVariant variantInfo = null;

            if (!string.IsNullOrEmpty(variantId))
            {
                var itemDataManager = new ItemDataManager(context);
                variantInfo = itemDataManager.GetVariantByVariantId(variantId, new ColumnSet());
            }

            return variantInfo;
        }

        /// <summary>
        /// Validate user permissions on cart.
        /// </summary>
        /// <param name="transaction">The sales transaction.</param>
        /// <param name="newCart">The new cart being updated.</param>
        /// <param name="context">The request context.</param>
        public static void ValidateCartPermissions(SalesTransaction transaction, Cart newCart, RequestContext context)
        {
            ValidateCommonPermissionsForCart(transaction, newCart, context);
        }

        /// <summary>
        /// Generates a new random transaction id.
        /// </summary>
        /// <returns>
        /// The new transaction id.
        /// </returns>
        public static string GenerateRandomTransactionId()
        {
            // Using format 'N' to remove dashes from the GUID.
            return Guid.NewGuid().ToString("N");
        }

        /// <summary>
        /// Generates a new permanent transaction id formatted as "StoreId-TerminalId-SequenceNumber".
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <param name="context">The context.</param>
        /// <returns>
        /// If the current channel type is retail store, returns a generated transaction id formatted as "StoreId-TerminalId-SequenceNumber"; otherwise, returns the existing transaction id.
        /// </returns>
        public static string GeneratePermanentTransactionId(SalesTransaction transaction, RequestContext context)
        {
            ThrowIf.Null(transaction, "transaction");
            ThrowIf.Null(context, "context");
            ThrowIf.Null(transaction.Id, "transaction.Id");

            if (string.IsNullOrWhiteSpace(transaction.Id))
            {
                if (context.GetChannelConfiguration() != null &&
                    context.GetChannelConfiguration().ChannelType == RetailChannelType.RetailStore &&
                    context.GetTerminal() != null)
                {
                    return NumberSequenceSeedDataManager.GenerateTransactionId(context);
                }
            }

            return transaction.Id;
        }

        /// <summary>
        /// Calculates the paid and due amounts for the transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="salesTransaction">The transaction to calculate the amount due for.</param>
        [SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "Microsoft.Dynamics.Retail.Diagnostics.NetTracer.Warning(System.String)", Justification = "Logging, not user message.")]
        [SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "CalculateAmountsPaidAndDue", Justification = "Method name")]
        [SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "CartWorkflowHelper", Justification = "Class name")]
        [SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "CartType", Justification = "Property name")]
        public static void CalculateAmountsPaidAndDue(RequestContext context, SalesTransaction salesTransaction)
        {
            CalculateAmountPaidAndDueServiceRequest calculateRequest = new CalculateAmountPaidAndDueServiceRequest(salesTransaction);
            CalculateAmountPaidAndDueServiceResponse response = context.Execute<CalculateAmountPaidAndDueServiceResponse>(calculateRequest);
            context.SetSalesTransaction(response.Transaction);
        }

        /// <summary>
        /// Validate whether the employee/cashier is able to perform return item or return transaction within the permissible limit.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="salesTransaction">The sales transaction object.</param>
        /// <param name="cartType">The cart type.</param>
        public static void ValidateReturnPermission(RequestContext context, SalesTransaction salesTransaction, CartType cartType)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(salesTransaction, "salesTransaction");

            // Set to true if the transaction contains any return item.
            bool isReturnTransaction = salesTransaction.ActiveSalesLines.Any(l => l.Quantity < 0);

            cartType = cartType == CartType.None ? salesTransaction.CartType : cartType;

            if (!isReturnTransaction || (cartType == CartType.CustomerOrder))
            {
                return;
            }

            // The permission validation logic is copied from EPOS (ItemSale.cs) for return item and transaction.
            EmployeePermissions employeePermisssion = EmployeePermissionHelper.GetEmployeePermissions(context, context.GetPrincipal().UserId);
            decimal maxLineReturnAmount = employeePermisssion.MaximumLineReturnAmount;
            decimal maxTotalReturnAmount = employeePermisssion.MaxTotalReturnAmount;
            var activeSalesLines = salesTransaction.ActiveSalesLines;

            // Check if the return item permission is satisified
            if (maxLineReturnAmount > 0)
            {
                foreach (SalesLine salesLine in activeSalesLines)
                {
                    if ((salesLine.Quantity < 0) &&
                        (decimal.Negate(salesLine.TotalAmount) > maxLineReturnAmount))
                    {
                        throw new DataValidationException(
                            DataValidationErrors.ReturnItemPriceExceeded,
                            string.Format(CultureInfo.CurrentUICulture, "The return item price exceeds the limit set for user: {0}", context.GetPrincipal().UserId));
                    }
                }
            }

            // Check if the return transaction permission is satisified
            decimal totalReturnAmount = activeSalesLines.Where(l => l.Quantity < 0).Sum(l => l.TotalAmount);

            if ((maxTotalReturnAmount > 0) &&
                (decimal.Negate(totalReturnAmount) > maxTotalReturnAmount))
            {
                throw new DataValidationException(
                    DataValidationErrors.ReturnTransactionTotalExceeded,
                    string.Format(CultureInfo.CurrentUICulture, "The return transaction total amount exceeds the limit set for user: {0}", context.GetPrincipal().UserId));
            }
        }

        /// <summary>
        /// Filters the sales transactions by customer id if filterByCustomer on request is true.
        /// </summary>
        /// <param name="customerId">The customer id.</param>
        /// <param name="filterByCustomer">Filter by customer boolean value.</param>
        /// <param name="salesTransactions">Sales transactions.</param>
        /// <returns>Sales transactions after applying the filter.</returns>
        public static IEnumerable<SalesTransaction> ApplyCustomerFilter(string customerId, bool filterByCustomer, IEnumerable<SalesTransaction> salesTransactions)
        {
            if (filterByCustomer)
            {
                IEnumerable<SalesTransaction> violations = salesTransactions.Where(t => !string.IsNullOrEmpty(t.CustomerId) && !t.CustomerId.Equals(customerId, StringComparison.OrdinalIgnoreCase));
                foreach (SalesTransaction transaction in violations)
                {
                    NetTracer.Warning("The specified transaction does not belong to the requested customer. Id: {0}, customer on transaction: {1}, customer from request: {2}", transaction.Id, transaction.CustomerId, customerId);
                }

                salesTransactions = salesTransactions.Except(violations);
            }

            return salesTransactions;
        }

        /// <summary>
        /// Tries to delete a cart represented by the <paramref name="salesTransaction"/> and traces warning in case of failure.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="salesTransaction">The transaction representing the cart.</param>
        public static void TryDeleteCart(RequestContext context, SalesTransaction salesTransaction)
        {
            try
            {
                var deleteCartRequest = new DeleteCartDataRequest(new[] { salesTransaction });
                context.Runtime.Execute<NullResponse>(deleteCartRequest, context);
            }
            catch (Exception ex)
            {
                NetTracer.Warning(ex, ex.Message);
            }
        }

        /// <summary>
        /// Add or update affiliations automatically during customer added, updated or removed.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="existingTransaction">The sales transaction.</param>
        /// <param name="newCart">The cart with updates/ new cart from the client.</param>
        internal static void AddOrUpdateAffiliationLinesFromCustomer(RequestContext context, SalesTransaction existingTransaction, Cart newCart)
        {
            // Check whether the customer account number is updated.
            // 1. Add a customer: The old cart does not have a customer or there is no cart; the new cart has a customer.
            // 2. Clear the customer: The old cart already has a customer; the new cart has the customer number set to empty.
            // 3. Change a customer: Both the old cart and the new cart have a customer number set, but they are different.
            bool addCustomer = false;
            bool clearCustomer = false;
            bool updateCustomer = false;

            if (newCart.AffiliationLines == null)
            {
                newCart.AffiliationLines = new List<AffiliationLoyaltyTier>();
                if (existingTransaction != null)
                {
                    // If we are not modifying the affiliations on the cart copy existing ones from the sales transaction.
                    CopyAffiliationLoyaltyTierToCart(existingTransaction, newCart);
                }
            }

            if (existingTransaction != null)
            {
                addCustomer = string.IsNullOrWhiteSpace(existingTransaction.CustomerId) && !string.IsNullOrWhiteSpace(newCart.CustomerId);
                clearCustomer = !addCustomer
                                       && !string.IsNullOrWhiteSpace(existingTransaction.CustomerId)
                                       && newCart.CustomerId != null
                                       && newCart.CustomerId.Length == 0;
                updateCustomer = !addCustomer
                                       && !clearCustomer
                                       && !string.IsNullOrWhiteSpace(existingTransaction.CustomerId)
                                       && !string.IsNullOrWhiteSpace(newCart.CustomerId)
                                       && !newCart.CustomerId.Equals(existingTransaction.CustomerId, StringComparison.OrdinalIgnoreCase);
            }
            else
            {
                addCustomer = !string.IsNullOrWhiteSpace(newCart.CustomerId);
            }

            bool customerChanged = addCustomer || clearCustomer || updateCustomer;

            if (context.GetChannelConfiguration() == null)
            {
                throw new ArgumentException("ChannelConfiguration is not set in context", "context");
            }

            // If the request comes from RetailStore, the customer affiliations will be added to the Cart only when the customer is changed.
            if (context.GetChannelConfiguration().ChannelType == RetailChannelType.RetailStore)
            {
                if (customerChanged)
                {
                    UpdateCartWithFilteredAffiliations(existingTransaction, newCart, true);

                    if (addCustomer || updateCustomer)
                    {
                        AddCustomerAffiliationsToCart(context, newCart);
                    }
                }
            }
            else if (context.GetChannelConfiguration().ChannelType == RetailChannelType.OnlineStore ||
                context.GetChannelConfiguration().ChannelType == RetailChannelType.SharePointOnlineStore)
            {
                // If the request comes from OnlineStore, then re-add the customer and Loyalty affiliations to the Cart forcibly,
                // because during the shopping process, the customer's affiliations may be changed.
                UpdateCartWithFilteredAffiliations(existingTransaction, newCart, false);
                AddCustomerAffiliationsToCart(context, newCart);
            }
        }

        /// <summary>
        /// Sets default Sales Tax Group (STG) for the cart based on a channel tax configuration for non-return transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        private static void SetSalesTaxGroupOnNonReturn(RequestContext context)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "salesTransaction");

            SalesTransaction salesTransaction = context.GetSalesTransaction();

            Channel channel;
            ChannelDataManager channelDataManager = new ChannelDataManager(context);
            long currentChannelId = channelDataManager.GetCurrentChannelId();
            string channelTaxGroup;

            if (context.GetChannelConfiguration().ChannelType == RetailChannelType.RetailStore)
            {
                OrgUnit store = CartWorkflowHelper.GetStoreById(context, currentChannelId);
                channel = store;
                channelTaxGroup = store.TaxGroup;
            }
            else
            {
                channel = channelDataManager.GetChannelById(currentChannelId);
                channelTaxGroup = string.Empty;
            }

            Address headerAddress = Address.IsNullOrEmpty(context.GetSalesTransaction().ShippingAddress)
                ? null
                : context.GetSalesTransaction().ShippingAddress;

            // Header charges follows header when taxed
            SalesTaxGroupPicker headerPicker = SalesTaxGroupPicker.Create(
                channel,
                context,
                headerAddress,
                salesTransaction.DeliveryMode,
                salesTransaction.StoreId ?? string.Empty,
                salesTransaction.InventoryLocationId);
            FillChargeLinesSalesTaxGroup(context, salesTransaction.ChargeLines, headerPicker.SalesTaxGroup, channelTaxGroup);

            // Consider active lines for taxation purpose only.
            foreach (SalesLine salesLine in context.GetSalesTransaction().ActiveSalesLines)
            {
                // On Return By Receipt carts, we don't change tax groups as they come populated from the headquarters
                // Tax groups have been set during sales transaction creation and could be from a different store than the one the return is being made.
                if (!salesLine.IsReturnByReceipt)
                {
                    Address shippingAddress = Address.IsNullOrEmpty(salesLine.ShippingAddress)
                        ? headerAddress
                        : salesLine.ShippingAddress;

                    SalesTaxGroupPicker linePicker = SalesTaxGroupPicker.Create(
                        channel,
                        context,
                        shippingAddress,
                        salesLine.DeliveryMode,
                        salesLine.Store ?? string.Empty,
                        salesTransaction.InventoryLocationId);
                    salesLine.SalesTaxGroupId = linePicker.SalesTaxGroup;

                    FillChargeLinesSalesTaxGroup(context, salesLine.ChargeLines, linePicker.SalesTaxGroup, channelTaxGroup);
                }
            }
        }

        private static bool IsLabelPrintingRequired(SalesTransaction retailTransaction, SalesLine saleItem, RequestContext context)
        {
            saleItem.ReturnLabelProperties = new ReturnLabelContent();
            DeviceConfigurationDataManager deviceConfigurationDataManager = new DeviceConfigurationDataManager(context);
            DeviceConfiguration deviceConfiguration = deviceConfigurationDataManager.GetDeviceConfiguration(retailTransaction.StoreId, context.GetPrincipal().ChannelId, retailTransaction.TerminalId, new QueryResultSettings());

            // If this is customer order transaction, than we obtain return label fields by S & M reason code; otherwise by info codes.
            GetReturnLocationTransactionServiceResponse response = retailTransaction.TransactionType == SalesTransactionType.CustomerOrder ? ObtainByReasonCode(retailTransaction, saleItem, null, context)
                                                                : ObtainByInfoCode(retailTransaction, saleItem, context);

            if (saleItem.ReasonCodeLines.Any())
            {
                saleItem.ReturnLabelProperties.ReturnReasonText = saleItem.ReasonCodeLines.First().Information ?? string.Empty;
            }

            saleItem.ReturnLabelProperties.ReturnWarehouseText = response.ReturnWarehouseText;
            saleItem.ReturnLabelProperties.ReturnLocationText = response.ReturnLocationText;
            saleItem.ReturnLabelProperties.ReturnPalleteText = response.ReturnPalleteText;
            return response.PrintReturnLabel;
        }

        private static GetReturnLocationTransactionServiceResponse ObtainByReasonCode(SalesTransaction order, SalesLine line, string reasonCodeId, RequestContext context)
        {
            GetReturnLocationTransactionServiceRequest request = new GetReturnLocationTransactionServiceRequest(order, line, reasonCodeId, false);

            GetReturnLocationTransactionServiceResponse response = context.Execute<GetReturnLocationTransactionServiceResponse>(request);
            return response;
        }

        private static GetReturnLocationTransactionServiceResponse ObtainByInfoCode(SalesTransaction order, SalesLine line, RequestContext context)
        {
            GetReturnLocationTransactionServiceRequest request = new GetReturnLocationTransactionServiceRequest(order, line, null, true);

            GetReturnLocationTransactionServiceResponse response = context.Execute<GetReturnLocationTransactionServiceResponse>(request);
            return response;
        }

        /// <summary>
        /// Gets identifier of transaction that is being returned.
        /// </summary>
        /// <param name="updatedCart">Updated cart (changed lines only).</param>
        /// <param name="originalCart">Original cart.</param>
        /// <returns>Transaction identifier or 'Null' if no transaction being returned.</returns>
        private static string ValidateAndGetReturnTransactionId(Cart updatedCart, SalesTransaction originalCart)
        {
            // First get original return transaction ids.
            Dictionary<string, string> returnTransactionIdsPerLine = originalCart.ActiveSalesLines
                .Where(l => l.IsReturnByReceipt && !string.IsNullOrWhiteSpace(l.ReturnTransactionId))
                .ToDictionary(k => k.LineId, v => v.ReturnTransactionId);

            // Remove transaction ids that were voided.
            IEnumerable<string> voidedCartLineIds = updatedCart.CartLines.Where(l => l.IsVoided).Select(l => l.LineId);
            foreach (string lineId in voidedCartLineIds)
            {
                returnTransactionIdsPerLine.Remove(lineId);
            }

            // Do not expect violations here because existing cart was already validated.
            string originalReturnTransactionId = returnTransactionIdsPerLine.Values.Distinct().SingleOrDefault();

            // Get newly added return transaction ids.
            IEnumerable<string> newReturnTransactionIds = updatedCart.CartLines
                .Where(l => string.IsNullOrEmpty(l.LineId) && l.LineData != null && l.LineData.IsReturnByReceipt && !string.IsNullOrWhiteSpace(l.LineData.ReturnTransactionId))
                .Select(l => l.ReturnTransactionId).Distinct();

            string newReturnTransactionId = null;

            if (!newReturnTransactionIds.IsNullOrEmpty())
            {
                // Check if several cart lines were added with different return transaction ids.
                if (newReturnTransactionIds.HasMultiple())
                {
                    throw new DataValidationException(
                        DataValidationErrors.CannotReturnMultipleTransactions,
                        "Only one transaction can be returned per cart. Return transaction ids: {0}",
                        string.Join(", ", newReturnTransactionIds));
                }

                // Check if existing cart (non voided lines) and newly added lines return different transactions.
                newReturnTransactionId = newReturnTransactionIds.Single();
                if (!string.IsNullOrWhiteSpace(originalReturnTransactionId) && (originalReturnTransactionId != newReturnTransactionId))
                {
                    throw new DataValidationException(
                        DataValidationErrors.CannotReturnMultipleTransactions,
                        "Only one transaction can be returned per cart. Existing return transaction id: {0}, new return transaction id {1}",
                        originalReturnTransactionId,
                        newReturnTransactionId);
                }
            }

            return newReturnTransactionId ?? originalReturnTransactionId;
        }

        /// <summary>
        /// Gets the product details.
        /// </summary>
        /// <param name="requestContext">Request context.</param>
        /// <param name="productId">Product identifier.</param>
        /// <param name="itemId">The item identifier.</param>
        /// <returns>Returns the product object.</returns>
        private static Product GetProductInfo(RequestContext requestContext, long? productId, string itemId)
        {
            ThrowIf.Null(requestContext, "requestContext");

            ProductSearchCriteria searchCriteria = new ProductSearchCriteria(requestContext.GetPrincipal().ChannelId);

            if (productId.HasValue)
            {
                searchCriteria.Ids.Add(productId.Value);
            }

            if (!string.IsNullOrWhiteSpace(itemId))
            {
                searchCriteria.ItemIds.Add(new ProductLookupClause(itemId));
            }

            searchCriteria.DataLevel = CommerceEntityDataLevel.Identity;
            searchCriteria.Context = new ProjectionDomain(requestContext.GetPrincipal().ChannelId, 0);

            return requestContext.Runtime.Execute<ProductSearchServiceResponse>(
                    new ProductSearchServiceRequest(searchCriteria, new QueryResultSettings()), requestContext).ProductSearchResult.Results.SingleOrDefault();
        }

        /// <summary>
        /// Gets the product details.
        /// </summary>
        /// <param name="requestContext">Request context.</param>
        /// <param name="productId">Product identifier.</param>
        /// <returns>Returns the product object.</returns>
        private static Product GetProductInfo(RequestContext requestContext, long productId)
        {
            return GetProductInfo(requestContext, productId: productId, itemId: null);
        }

        /// <summary>
        /// Gets the product details.
        /// </summary>
        /// <param name="requestContext">Request context.</param>
        /// <param name="itemId">The item identifier.</param>
        /// <returns>Returns the product object.</returns>
        private static Product GetProductInfo(RequestContext requestContext, string itemId)
        {
            return GetProductInfo(requestContext, productId: null, itemId: itemId);
        }

        /// <summary>
        /// Adds affiliations with the type General to the cart.
        /// </summary>
        /// <param name="existingTransaction">The sales transaction.</param>
        /// <param name="newCart">The cart with updates/ new cart from the client.</param>
        /// <param name="isRetailStore">True is the request is done against Retail Store cart.</param>
        /// <remarks>If the request was done not against Retail Store cart then affiliations like Loyalty or Unknown or those who have customer assigned are not added while copying from the SalesTransaction.</remarks>
        private static void UpdateCartWithFilteredAffiliations(SalesTransaction existingTransaction, Cart newCart, bool isRetailStore)
        {
            if (existingTransaction != null)
            {
                // Only keep the affiliations added manually and loyalty card affiliations, remove the old customer affiliations.
                IEnumerable<AffiliationLoyaltyTier> affiliations = from a in existingTransaction.AffiliationLoyaltyTierLines
                                                                   where string.IsNullOrWhiteSpace(a.CustomerId)
                                                                    && !IsExistSameAffiliation(a.AffiliationId, newCart.AffiliationLines)
                                                                    && ((!isRetailStore && (a.AffiliationType == RetailAffiliationType.Loyalty || a.AffiliationType == RetailAffiliationType.Unknown)) || isRetailStore)
                                                                   select new AffiliationLoyaltyTier
                                                                   {
                                                                       AffiliationId = a.AffiliationId,
                                                                       AffiliationType = a.AffiliationType,
                                                                       LoyaltyTierId = 0,
                                                                       ReasonCodeLines = a.ReasonCodeLines
                                                                   };

                newCart.AffiliationLines.AddRange(affiliations);
            }
        }

        /// <summary>
        /// Adds customer's affiliations to Cart.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="newCart">The cart with updates/ new cart from the client.</param>
        private static void AddCustomerAffiliationsToCart(RequestContext context, Cart newCart)
        {
            // If customer ID is specified, means the cart is not for anonymous customer, then try to retrieve affiliations.
            // this check is needed to avoid performance issue - the method CustomerDataManager.GetCustomers retrieves all information about customers
            // according to input creteria, but if the input criteria is not specified then the method will retrive all customers from the DB which is not needed in this case.
            if (!string.IsNullOrWhiteSpace(newCart.CustomerId))
            {
                // Get the customer affiliations.
                var getCustomersServiceRequest = new GetCustomersServiceRequest(new QueryResultSettings(), newCart.CustomerId);
                var getCustomerServiceResponse = context.Execute<GetCustomersServiceResponse>(getCustomersServiceRequest);

                var customerAffiliations = getCustomerServiceResponse.Customers.FirstOrDefault().CustomerAffiliations;
                if (customerAffiliations != null && customerAffiliations.Count > 0)
                {
                    // Select affiliations that are not already in Cart from customer affiliations.
                    IEnumerable<AffiliationLoyaltyTier> cartAffiliationLoyaltyTiers = customerAffiliations.Where(cA => { return !IsExistSameAffiliation(cA.RetailAffiliationId, newCart.AffiliationLines); }).Select(
                       cA => new AffiliationLoyaltyTier()
                       {
                           AffiliationId = cA.RetailAffiliationId,
                           AffiliationType = RetailAffiliationType.General,
                           CustomerId = newCart.CustomerId,
                           LoyaltyTierId = 0,
                       });

                    newCart.AffiliationLines.AddRange(cartAffiliationLoyaltyTiers);
                }
            }
        }

        /// <summary>
        ///  Copies the sales transaction affiliations to the cart.
        /// </summary>
        /// <param name="salesTransaction">Sales transaction that contains affiliations.</param>
        /// <param name="cart">Cart to copy affiliations to.</param>
        private static void CopyAffiliationLoyaltyTierToCart(SalesTransaction salesTransaction, Cart cart)
        {
            if (cart.AffiliationLines == null)
            {
                cart.AffiliationLines = new List<AffiliationLoyaltyTier>();
            }

            foreach (SalesAffiliationLoyaltyTier tier in salesTransaction.AffiliationLoyaltyTierLines)
            {
                AffiliationLoyaltyTier affiliationLoyaltyTier = new AffiliationLoyaltyTier { AffiliationId = tier.AffiliationId, LoyaltyTierId = tier.LoyaltyTierId, AffiliationType = tier.AffiliationType, CustomerId = tier.CustomerId };
                affiliationLoyaltyTier.CopyPropertiesFrom(tier);

                affiliationLoyaltyTier.ReasonCodeLines.Clear();
                affiliationLoyaltyTier.ReasonCodeLines.AddRange(tier.ReasonCodeLines);
                cart.AffiliationLines.Add(affiliationLoyaltyTier);
            }
        }

        /// <summary>
        ///  Copies the cart affiliations to the sales transaction.
        /// </summary>
        /// <param name="cart">Cart that contains affiliations.</param>
        /// <param name="salesTransaction">SalesTransaction to copy affiliations to.</param>
        private static void CopyAffiliationLoyaltyTierToSalesTransaction(Cart cart, SalesTransaction salesTransaction)
        {
            if (salesTransaction.AffiliationLoyaltyTierLines == null)
            {
                salesTransaction.AffiliationLoyaltyTierLines = new Collection<SalesAffiliationLoyaltyTier>();
            }

            foreach (AffiliationLoyaltyTier tier in cart.AffiliationLines)
            {
                SalesAffiliationLoyaltyTier affiliationLoyaltyTier = new SalesAffiliationLoyaltyTier { AffiliationId = tier.AffiliationId, LoyaltyTierId = tier.LoyaltyTierId, AffiliationType = tier.AffiliationType, CustomerId = tier.CustomerId };
                affiliationLoyaltyTier.CopyPropertiesFrom(tier);

                affiliationLoyaltyTier.ReasonCodeLines.Clear();
                affiliationLoyaltyTier.ReasonCodeLines.AddRange(tier.ReasonCodeLines);
                salesTransaction.AffiliationLoyaltyTierLines.Add(affiliationLoyaltyTier);
            }
        }

        /// <summary>
        /// Perform save operations for cart lines.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="request">The request object.</param>
        /// <param name="salesLineByLineId">The mapping sales line id to sales line object.</param>
        [SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "By design.")]
        private static void PerformSaveCartLineOperations(
            RequestContext context,
            SaveCartRequest request,
            Dictionary<string, SalesLine> salesLineByLineId)
        {
            Dictionary<long, Product> productsById = GetProductsInCartLines(context, request.Cart.CartLines);

            // This dictionary is used to keep track of products that are newly added to cart. This is required to validate checks for linked products.
            Dictionary<long, Collection<string>> addproductIdLineIdsMap = new Dictionary<long, Collection<string>>();

            CartType cartType = context.GetSalesTransaction().CartType;

            foreach (CartLine cartLine in request.Cart.CartLines)
            {
                SalesLine salesLine = null;
                ReasonCodeSourceType reasonCodeSourceType = ReasonCodeSourceType.None;

                Product product = null;
                long productId = (cartLine.LineData != null) ? cartLine.LineData.ProductId : salesLineByLineId[cartLine.LineId].ProductId;
                bool isProductFound = productsById.TryGetValue(productId, out product);

                if (AggregateSalesLines(context, context.GetSalesTransaction().ActiveSalesLines, cartLine, product))
                {
                    // If item is aggregated simply increase quantity and skip additional lookups.
                    continue;
                }

                if (context.GetSalesTransaction().SalesLines.Where(i => i.LineId == cartLine.LineId).Count() == 0)
                {
                    salesLine = new SalesLine();
                    if (!cartLine.LineData.IsReturnByReceipt)
                    {
                        // Creates a sales line base on the cart line
                        salesLine.CopyPropertiesFrom(cartLine.LineData);

                        // Set ItemId and VariantInventDimId of the sales line, if the cart line is constructed from listing.
                        if (cartLine.LineData.IsProductLine)
                        {
                            if (!isProductFound)
                            {
                                string message = string.Format("The specified product identifier ({0}) could not be found.", cartLine.LineData.ProductId);
                                throw new DataValidationException(DataValidationErrors.ObjectNotFound, message);
                            }

                            if (product.IsRemote && cartType != CartType.CustomerOrder)
                            {
                                string message = string.Format("Failed to add product '{0}' to cart. Remote products are only supported in customer order mode.", cartLine.LineData.ProductId);
                                throw new DataValidationException(
                                    DataValidationErrors.RemoteProductsNotSupportedWithCurrentTransactionType, message);
                            }

                            if (string.IsNullOrWhiteSpace(salesLine.SerialNumber) && product.Rules.IsSerialized
                                && (cartType != CartType.CustomerOrder
                                || (cartType == CartType.CustomerOrder && !product.Rules.IsActiveInSalesProcess)))
                            {
                                // Throw data serial number validation error if:
                                // * Cart type is non customer order, product has serial number properties, but serial number is null.
                                // * Cart type is customer order, product has serial number properties, serial number is null,
                                //   and product does not have active in sales process.
                                // If cart type is customer order and product has active in sales process, do not capture serial number.
                                // Serial number will be captured when pick up the products or doing packing / invoice (for shipped products).
                                throw new DataValidationException(DataValidationErrors.SerialNumberMissing, "Serial number for item {0} missing", salesLine.ItemId);
                            }

                            salesLine.ProductSource = product.IsRemote ? ProductSource.Remote : ProductSource.Local;
                            salesLine.ItemId = product.ItemId;
                            salesLine.ProductId = cartLine.ProductId;

                            ProductVariant crtVariant = null;
                            if (product.IsMasterProduct
                                && product.TryGetVariant(cartLine.LineData.ProductId, out crtVariant))
                            {
                                salesLine.InventoryDimensionId = crtVariant.InventoryDimensionId;
                                salesLine.Variant = crtVariant;
                            }
                            else
                            {
                                salesLine.InventoryDimensionId = string.Empty; // product.InventoryDimension;
                            }

                            // set the catalog context as inherited from this product.
                            salesLine.CatalogId = product.Context.CatalogId.GetValueOrDefault();
                        }
                    }
                    else
                    {
                        // Creates a sales line base on the retuned sales line
                        var returnedSalesLine = GetSalesLineByNumber(
                            context.GetReturnedSalesTransaction(), cartLine.LineData.ReturnLineNumber);
                        SetSalesLineBasedOnReturnedSalesLine(
                            salesLine, returnedSalesLine, context.GetReturnedSalesTransaction().Id, cartLine.LineData.Quantity);

                        // Set reason code source type for return item.
                        reasonCodeSourceType = ReasonCodeSourceType.ReturnItem;
                    }

                    salesLine.Found = true;

                    if (string.IsNullOrEmpty(salesLine.LineId))
                    {
                        AssignUniqueLineId(salesLine);
                    }

                    if (!cartLine.LineData.IsReturnByReceipt && !cartLine.IsCustomerAccountDeposit)
                    {
                        // When a product is added to cart newly, it is added to the dictionary that keep tracks of new adds, to perform linked product checks.
                        // The same product can be added multiple times, the else clause takes care of first time add to the dictionary and the if clause otherwise.
                        if (addproductIdLineIdsMap.ContainsKey(salesLine.ProductId))
                        {
                            addproductIdLineIdsMap[salesLine.ProductId].Add(salesLine.LineId);
                        }
                        else
                        {
                            addproductIdLineIdsMap.Add(salesLine.ProductId, new Collection<string>(new List<string> { salesLine.LineId }));
                        }
                    }

                    salesLineByLineId.Add(salesLine.LineId, salesLine);
                }
                else
                {
                    // If no line data is set, it means remove
                    if (cartLine.LineData == null)
                    {
                        salesLine = salesLineByLineId[cartLine.LineId];

                        // Removing the linked products' sales lines if any.
                        if (salesLine.LineIdsLinkedProductMap.Any())
                        {
                            foreach (string lineId in salesLine.LineIdsLinkedProductMap.Keys)
                            {
                                salesLineByLineId.Remove(lineId);
                            }
                        }

                        // Removing the reference to the linked product from the parent product sales line if the linked product is removed from cart.
                        if (!string.IsNullOrWhiteSpace(salesLine.LinkedParentLineId))
                        {
                            salesLineByLineId[salesLine.LinkedParentLineId].LineIdsLinkedProductMap.Remove(salesLine.LineId);
                        }

                        salesLineByLineId.Remove(cartLine.LineId);
                    }
                    else
                    {
                        salesLine = salesLineByLineId[cartLine.LineId];

                        if (!salesLine.IsReturnByReceipt)
                        {
                            // If voiding salesline, void the sales lines of the linked products if any.
                            if (cartLine.IsVoided && !salesLine.IsVoided && salesLine.LineIdsLinkedProductMap.Any())
                            {
                                foreach (string lineId in salesLine.LineIdsLinkedProductMap.Keys)
                                {
                                    if (salesLineByLineId[lineId] != null)
                                    {
                                        salesLineByLineId[lineId].IsVoided = true;
                                    }
                                    else
                                    {
                                        throw new DataValidationException(DataValidationErrors.ObjectNotFound, "Sales line of the linked product with id : {0} was not found.", lineId);
                                    }
                                }
                            }

                            // If unvoiding salesline, unvoid the sales lines of the linked products if any.
                            if (!cartLine.IsVoided && salesLine.IsVoided && salesLine.LineIdsLinkedProductMap.Any())
                            {
                                foreach (string lineId in salesLine.LineIdsLinkedProductMap.Keys)
                                {
                                    if (salesLineByLineId[lineId] != null)
                                    {
                                        salesLineByLineId[lineId].IsVoided = false;
                                    }
                                    else
                                    {
                                        throw new DataValidationException(DataValidationErrors.ObjectNotFound, "Sales line of the linked product with id : {0} was not found.", lineId);
                                    }
                                }
                            }

                            // Copy the properties from the cart line
                            salesLine.CopyPropertiesFrom(cartLine.LineData);

                            // we have to preserve the LineId, regardless what is set on line data
                            salesLine.LineId = cartLine.LineId;
                            salesLine.IsPriceOverridden = cartLine.IsPriceOverridden;

                            // Set reason code source type for prive override
                            if (cartLine.IsPriceOverridden)
                            {
                                reasonCodeSourceType = ReasonCodeSourceType.OverridePrice;
                            }
                        }
                        else
                        {
                            // For return
                            // Keep the properties on the sales line and only copy the quantity from the cart line
                            salesLine.Quantity = cartLine.LineData.Quantity;

                            // Set reason code source type for return item.
                            reasonCodeSourceType = ReasonCodeSourceType.ReturnItem;
                        }

                        // If user provides an empty address, we need to empty current address, since property copy will not change the address
                        if (cartLine.ShippingAddress != null && cartLine.ShippingAddress.IsEmpty())
                        {
                            salesLine.ShippingAddress = new Address();
                        }

                        // if the sales line has linked sales lines, update the quantity of linked sales line also.
                        if (salesLine.LineIdsLinkedProductMap != null)
                        {
                            foreach (string lineId in salesLine.LineIdsLinkedProductMap.Keys)
                            {
                                salesLineByLineId[lineId].Quantity = salesLine.Quantity * salesLine.LineIdsLinkedProductMap[lineId].Quantity;
                            }
                        }
                    }
                }

                // Process reason code lines on the cart line.
                ReasonCodesWorkflowHelper.AddOrUpdateReasonCodeLinesOnSalesLine(
                    salesLine, cartLine, context.GetSalesTransaction().Id);

                // Calculate the required reason code on the sales line.
                if (context.GetSalesTransaction().CartType != CartType.CustomerOrder)
                {
                    // calculate required reason codes for discounts
                    if (salesLine.LineManualDiscountAmount != 0 || salesLine.LineManualDiscountPercentage != 0)
                    {
                        ReasonCodesWorkflowHelper.CalculateRequiredReasonCodesOnSalesLine(context, salesLine, ReasonCodeSourceType.ItemDiscount);
                    }

                    ReasonCodesWorkflowHelper.CalculateRequiredReasonCodesOnSalesLine(context, salesLine, reasonCodeSourceType);
                }
            }

            decimal lineNumber = 1;
            foreach (var salesLine in salesLineByLineId.Values)
            {
                salesLine.LineNumber = lineNumber++;
            }

            if (addproductIdLineIdsMap.Any())
            {
                ValidateLinkedProductsAdd(context, addproductIdLineIdsMap, request.Cart, salesLineByLineId);
            }

            // Validate quantity limit for the added or updated cart lines
            CartWorkflowHelper.ValidateSalesLineQuantityLimit(context, request.Cart.CartLines);
        }

        /// <summary>
        /// Validate permission for unit of measure operation.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="cart">The Cart request.</param>
        /// <param name="salesLines">The sales line value.</param>
        private static void CheckChangeUnitOfMeasureOperation(RequestContext context, Cart cart, IEnumerable<SalesLine> salesLines)
        {
            foreach (CartLine cartLine in cart.CartLines)
            {
                if (!string.IsNullOrWhiteSpace(cartLine.UnitOfMeasureSymbol))
                {
                    SalesLine salesLine = salesLines.Where(sl => sl.LineId == cartLine.LineId)
                                                                .SingleOrDefault();

                    if (salesLine != null && (cartLine.UnitOfMeasureSymbol != salesLine.UnitOfMeasureSymbol))
                    {
                        context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.ChangeUnitOfMeasure);
                    }
                }
            }
        }

        /// <summary>
        /// Set the unit of measure conversion in sales line.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="conversions">The list of item unit conversions on sales lines.</param>
        /// <returns>The collection of unit of measure conversions.</returns>
        private static ReadOnlyCollection<UnitOfMeasureConversion> GetUnitOfMeasureConversions(RequestContext context, IEnumerable<ItemUnitConversion> conversions)
        {
            var getUomConvertionDataRequest = new GetUnitOfMeasureConversionDataRequest(conversions, new QueryResultSettings());
            return context.Runtime.Execute<GetUnitOfMeasureConversionDataResponse>(getUomConvertionDataRequest, context).UnitConversions;
        }

        /// <summary>
        /// Gets the calculation modes based on the operation being performed.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>The filtered calculation modes.</returns>
        private static CalculationModes GetCalculationModes(RequestContext context)
        {
            switch (context.GetSalesTransaction().CartType)
            {
                case CartType.Checkout:
                case CartType.Shopping:
                    return CalculationModes.All;

                case CartType.IncomeExpense:
                case CartType.AccountDeposit:
                    return CalculationModes.Totals | CalculationModes.AmountDue;

                case CartType.CustomerOrder:
                    return CustomerOrderWorkflowHelper.GetCalculationModes(context, context.GetSalesTransaction());

                default:
                    string message = string.Format(CultureInfo.InvariantCulture, "Cart type {0} is not supported.", context.GetSalesTransaction().CartType);
                    throw new NotSupportedException(message);
            }
        }

        /// <summary>
        /// Validate linked products cart lines add.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="addproductIdLineIdsMap">The map between product Ids that are added to cart and the lineIds.</param>
        /// <param name="newCart">The cart with the new cart lines.</param>
        /// <param name="salesLineByLineId">The map between the line ids and the sales lines.</param>
        private static void ValidateLinkedProductsAdd(RequestContext context, Dictionary<long, Collection<string>> addproductIdLineIdsMap, Cart newCart, Dictionary<string, SalesLine> salesLineByLineId)
        {
            string productLineId = null;
            string linkedProductLineId;
            SalesLine parentSalesLine = new SalesLine();

            // Get the linked products of all the products that were added to the cart.
            var getLinkedProductsRequest = new GetLinkedProductsDataRequest(addproductIdLineIdsMap.Keys);
            ReadOnlyCollection<LinkedProduct> linkedProducts = context.Runtime.Execute<GetLinkedProductsDataResponse>(getLinkedProductsRequest, context).LinkedProducts;

            // The cart contains the variant ids of the products and GetLinkedProducts returns the master product ids (if the product is a master, not a standalone).
            // In order to map between the variant ids and the corresponding master product ids, get the product details of the product ids.
            IList<long> productIds = addproductIdLineIdsMap.Keys.ToList();
            ProductSearchCriteria productSearchCriteria = new ProductSearchCriteria(context.GetPrincipal().ChannelId);
            productSearchCriteria.DataLevel = CommerceEntityDataLevel.Identity;
            productSearchCriteria.Ids = productIds;
            QueryResultSettings settings = new QueryResultSettings(new PagingInfo(productSearchCriteria.Ids.Count, 0));

            // Get the product details to determine if the product is a master, if yes then the variant id is required.
            ProductSearchResult results = context.Runtime.Execute<ProductSearchServiceResponse>(
                    new ProductSearchServiceRequest(productSearchCriteria, settings), context).ProductSearchResult;

            ReadOnlyCollection<Product> productsFromDB = null;

            if (results != null && results.Results.Any())
            {
                productsFromDB = results.Results;
            }

            foreach (LinkedProduct linkedProduct in linkedProducts)
            {
                Product parentProductFromDB = productsFromDB.Where(lpm => lpm.RecordId == linkedProduct.ProductRecordId).SingleOrDefault();
                Product linkedProductFromDB = productsFromDB.Where(lpm => lpm.RecordId == linkedProduct.LinkedProductRecordId).SingleOrDefault();

                ProductVariant productVariant = null;
                long parentDistinctProductVariantId = linkedProduct.ProductRecordId;
                long linkedDistinctProductVariantId = linkedProduct.LinkedProductRecordId;

                if (parentProductFromDB == null)
                {
                    throw new DataValidationException(DataValidationErrors.ListingIdNotFound, "Unable to obtain the id of product that was added to the cart");
                }

                if (linkedProductFromDB == null)
                {
                    NetTracer.Warning("Linked product was not added with the product to the cart", newCart.Id);
                    continue;
                }

                // If the parent product is a master determine the variant that was added to cart.
                if (parentProductFromDB.IsMasterProduct)
                {
                    productVariant = parentProductFromDB.CompositionInformation.VariantInformation.Variants.Where(v => addproductIdLineIdsMap.Keys.Contains(v.DistinctProductVariantId)).SingleOrDefault();
                    if (productVariant == null)
                    {
                        throw new DataValidationException(DataValidationErrors.ListingIdNotFound, "Unable to obtain the id of product that was added to the cart");
                    }

                    parentDistinctProductVariantId = productVariant.DistinctProductVariantId;
                }

                // If the linked product is a master determine the variant that was added to cart.
                if (linkedProductFromDB.IsMasterProduct)
                {
                    productVariant = linkedProductFromDB.CompositionInformation.VariantInformation.Variants.Where(v => addproductIdLineIdsMap.Keys.Contains(v.DistinctProductVariantId)).SingleOrDefault();
                    if (productVariant == null)
                    {
                        NetTracer.Warning("Linked product was not added with the product to the cart", newCart.Id);
                        continue;
                    }

                    linkedDistinctProductVariantId = productVariant.DistinctProductVariantId;
                }

                if (addproductIdLineIdsMap[linkedDistinctProductVariantId] == null)
                {
                    NetTracer.Warning("Linked product was not added with the product to the cart", newCart.Id);
                    continue;
                }

                // Get the line id and the sales line of linked product.
                linkedProductLineId = addproductIdLineIdsMap[linkedDistinctProductVariantId].First();

                SalesLine linkedSalesLine = salesLineByLineId[linkedProductLineId];

                // Get the line id and sales line of the product it is linked to.
                foreach (string lineId in addproductIdLineIdsMap[parentDistinctProductVariantId])
                {
                    parentSalesLine = salesLineByLineId[lineId];

                    // This check is required beacuse same product can be added multiple times to the cart.
                    if (!parentSalesLine.LineIdsLinkedProductMap.ContainsValue(linkedProduct))
                    {
                        productLineId = lineId;
                    }
                }

                // Check if the linked product has been added with right quantity.
                if (linkedSalesLine.Quantity != linkedProduct.Quantity * parentSalesLine.Quantity)
                {
                    NetTracer.Warning("Invalid linked product quantity on add to cart", newCart.Id);
                }

                // Add reference from parent product to linked product and vice-versa.
                parentSalesLine.LineIdsLinkedProductMap.Add(linkedProductLineId, linkedProduct);
                linkedSalesLine.LinkedParentLineId = productLineId;
                addproductIdLineIdsMap[linkedDistinctProductVariantId].Remove(linkedProductLineId);
            }
        }

        /// <summary>
        /// Performs invoice line specific validations for the transaction.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="newCart">The new cart.</param>
        /// <param name="salesTransaction">The old transaction.</param>
        /// <param name="validationResults">The validation results.</param>
        private static void ValidateTransactionWithInvoiceLines(RequestContext context, Cart newCart, SalesTransaction salesTransaction, CartLineValidationResults validationResults)
        {
            bool hasInvoiceLines = newCart.CartLines.Any(c => c.IsInvoiceLine);

            if (hasInvoiceLines)
            {
                DataValidationFailure failure;

                // Customer is read-only if there are invoice lines.
                ReadOnlyAttribute.AssertPropertyNotChanged(RetailTransactionTableSchema.CustomerIdColumn, newCart, salesTransaction, out failure);
                if (failure != null)
                {
                    validationResults.AddLineResult(0, failure);
                }
            }
        }

        /// <summary>
        /// Calculates the totals on the context's sales transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        private static void CalculateTotals(RequestContext context)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");

            NetTracer.Information("CartWorkflowHelper.CalculateTotals(): TransactionId = {0}, CustomerId = {1}", context.GetSalesTransaction().Id, context.GetSalesTransaction().CustomerId);

            CalculateTotalsServiceRequest request = new CalculateTotalsServiceRequest(context.GetSalesTransaction());
            CalculateTotalsServiceResponse response = context.Execute<CalculateTotalsServiceResponse>(request);

            context.SetSalesTransaction(response.Transaction);
        }

        /// <summary>
        /// Gets the sales lines prices for each item on the context's sales transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="restoreItemPrices">The boolean indicating if prices should be restored ignoring any changes.</param>
        private static void CalculatePrices(RequestContext context, bool restoreItemPrices)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");

            NetTracer.Information("CartWorkflowHelper.CalculatePrices(): TransactionId = {0}, CustomerId = {1}", context.GetSalesTransaction().Id, context.GetSalesTransaction().CustomerId);

            UpdatePriceServiceRequest updatePriceRequest = new UpdatePriceServiceRequest(context.GetSalesTransaction(), restoreItemPrices);
            GetPriceServiceResponse getPriceResponse = context.Execute<GetPriceServiceResponse>(updatePriceRequest);

            context.SetSalesTransaction(getPriceResponse.Transaction);
        }

        /// <summary>
        /// Calculate the charges on the context's sales transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        private static void CalculateCharges(RequestContext context)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");

            NetTracer.Information("CartWorkflowHelper.CalculateCharges(): TransactionId = {0}, CustomerId = {1}", context.GetSalesTransaction().Id, context.GetSalesTransaction().CustomerId);

            GetChargesServiceRequest request = new GetChargesServiceRequest(context.GetSalesTransaction());
            GetChargesServiceResponse response = context.Execute<GetChargesServiceResponse>(request);

            context.SetSalesTransaction(response.Transaction);
        }

        /// <summary>
        /// Calculate the discounts on the context's sales transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        private static void CalculateDiscounts(RequestContext context)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");

            NetTracer.Information("CartWorkflowHelper.CalculateDiscounts(): TransactionId = {0}, CustomerId = {1}", context.GetSalesTransaction().Id, context.GetSalesTransaction().CustomerId);

            ValidateEmployeeDiscountPermission(context);

            CalculateDiscountsServiceRequest request = new CalculateDiscountsServiceRequest(context.GetSalesTransaction());
            GetPriceServiceResponse response = context.Execute<GetPriceServiceResponse>(request);

            context.SetSalesTransaction(response.Transaction);
        }

        /// <summary>
        /// Update tax override codes on the transaction and lines.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="transaction">The sales transaction.</param>
        /// <param name="cart">The cart.</param>
        private static void UpdateTaxOverrideCodes(RequestContext context, SalesTransaction transaction, Cart cart)
        {
            // copy cart level tax override
            if (!string.IsNullOrWhiteSpace(cart.TaxOverrideCode))
            {
                transaction.TaxOverrideCode = cart.TaxOverrideCode;

                ReasonCodesWorkflowHelper.CalculateRequiredReasonCodesOnTransaction(context, transaction, ReasonCodeSourceType.TransactionTaxChange);
            }

            // copy line level tax override
            // inspect line level overrides
            // note: it is not a meaningful business case to have both cart and line level overrides, but technically it is still possible
            // on the off chance, this happens, line's will override cart-inherited overrides 
            IList<CartLine> cartLines = cart.CartLines.Where(p => !p.IsVoided).AsReadOnly();

            foreach (CartLine cartLine in cartLines)
            {
                if (!string.IsNullOrWhiteSpace(cartLine.TaxOverrideCode))
                {
                    // update line
                    var salesLine = transaction.SalesLines.Single(p => string.Equals(p.LineId, cartLine.LineId, StringComparison.OrdinalIgnoreCase));
                    salesLine.TaxOverrideCode = cartLine.TaxOverrideCode;

                    ReasonCodesWorkflowHelper.CalculateRequiredReasonCodesOnSalesLine(context, salesLine, ReasonCodeSourceType.LineItemTaxChange);
                }
            }
        }

        /// <summary>
        /// Aggregate sales line with existing lines.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="existingLines">Collection of existing lines.</param>
        /// <param name="updatedOrAddedLine">Updated or newly added line.</param>
        /// <param name="product">The product.</param>
        /// <returns>
        /// True if line is aggregated; False otherwise.
        /// </returns>
        private static bool AggregateSalesLines(RequestContext context, IList<SalesLine> existingLines, CartLine updatedOrAddedLine, Product product)
        {
            if (context.GetChannelConfiguration().ChannelType != RetailChannelType.RetailStore)
            {
                return false;
            }

            if (existingLines.Any(l => l.LineId == updatedOrAddedLine.LineId))
            {
                // Line already exists. Skip aggregation.
                return false;
            }

            var deviceConfiguration = GetDeviceConfiguration(context);

            if (deviceConfiguration.AllowItemsAggregation)
            {
                foreach (SalesLine existingLine in existingLines)
                {
                    if (AllowAggregation(updatedOrAddedLine, existingLine, product))
                    {
                        // Updating line to represent a quantity update.
                        existingLine.Quantity += updatedOrAddedLine.Quantity;
                        updatedOrAddedLine.LineData.CopyPropertiesFrom(existingLine);
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Gets device configuration.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>The <see cref="DeviceConfiguration"/> device configuration.</returns>
        private static DeviceConfiguration GetDeviceConfiguration(RequestContext context)
        {
            var deviceConfigurationDataRequest = new GetDeviceConfigurationDataServiceRequest(
                context.GetPrincipal().ChannelId,
                context.GetTerminal().TerminalId);

            DeviceConfiguration deviceConfiguration =
                context.Runtime.Execute<SingleEntityDataServiceResponse<DeviceConfiguration>>(deviceConfigurationDataRequest, context).Entity;
            if (deviceConfiguration == null)
            {
                throw new DataValidationException(
                    DataValidationErrors.ObjectNotFound,
                    string.Format(CultureInfo.InvariantCulture, "There is no device configuration for the current channel {0} and terminal {1}", context.GetPrincipal().ChannelId, context.GetTerminal().TerminalId));
            }

            return deviceConfiguration;
        }

        /// <summary>
        /// Checks if lines can be aggregated.
        /// </summary>
        /// <param name="newLine">Newly added line.</param>
        /// <param name="existingLine">Existing line.</param>
        /// <param name="product">The product.</param>
        /// <returns>True if lines can be aggregated; False otherwise.</returns>
        private static bool AllowAggregation(CartLine newLine, SalesLine existingLine, Product product)
        {
            if (existingLine.IsGiftCardLine || newLine.IsGiftCardLine)
            {
                return false;
            }

            if (existingLine.IsVoided || newLine.IsVoided)
            {
                return false;
            }

            if (existingLine.ProductId != newLine.ProductId)
            {
                // If non-variant product is added by barcode only ItemId is set on the line.
                if (newLine.ProductId != 0 || (existingLine.ItemId != newLine.ItemId))
                {
                    return false;
                }
            }

            if (existingLine.SerialNumber != newLine.SerialNumber)
            {
                return false;
            }

            if (product != null && product.Rules.IsActiveInSalesProcess)
            {
                return false;
            }

            if (!string.IsNullOrEmpty(newLine.UnitOfMeasureSymbol) && existingLine.SalesOrderUnitOfMeasure != newLine.UnitOfMeasureSymbol)
            {
                return false;
            }

            if (newLine.IsPriceOverridden || existingLine.IsPriceOverridden)
            {
                return false;
            }

            if (existingLine.LinkedParentLineId != null || existingLine.LineIdsLinkedProductMap.Any())
            {
                return false;
            }

            if (Math.Sign(newLine.Quantity) != Math.Sign(existingLine.Quantity))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check permissions for operations that are common for both customer orders and sales orders cart types.
        /// Such as comment, adding a customer etc.
        /// </summary>
        /// <param name="transaction">The sales transaction.</param>
        /// <param name="newCart">The new cart being updated.</param>
        /// <param name="context">The request context.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1820:TestForEmptyStringsUsingStringLength", Justification = "String will be null in case of patch when no change is to be made")]
        private static void ValidateCommonPermissionsForCart(SalesTransaction transaction, Cart newCart, RequestContext context)
        {
            // Check if the comment has been added.
            if (!string.IsNullOrWhiteSpace(newCart.Comment))
            {
                context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.TransactionComment);
            }

            if (transaction == null)
            {
                // Check if a customer is being added.
                if (!string.IsNullOrWhiteSpace(newCart.CustomerId))
                {
                    context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.Customer);
                }

                // Check if a loyalty card is being added
                if (!string.IsNullOrWhiteSpace(newCart.LoyaltyCardId))
                {
                    context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.LoyaltyRequest);
                }
            }
            else
            {
                // Check if a customer is being added.
                if (string.IsNullOrWhiteSpace(transaction.CustomerId) && !string.IsNullOrWhiteSpace(newCart.CustomerId))
                {
                    context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.Customer);
                }
                else if (!string.IsNullOrWhiteSpace(transaction.CustomerId) && string.IsNullOrWhiteSpace(newCart.CustomerId))
                {
                    context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.CustomerClear);
                }
                else if (!string.IsNullOrWhiteSpace(transaction.CustomerId) &&
                    newCart.CustomerId != string.Empty &&
                    transaction.CustomerId.Equals(newCart.CustomerId))
                {
                    context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.CustomerClear);
                    context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.Customer);
                }

                // Check if a loyalty card is being added or updated
                bool addLoyaltyCard = string.IsNullOrWhiteSpace(transaction.LoyaltyCardId) && !string.IsNullOrWhiteSpace(newCart.LoyaltyCardId);
                bool updateLoyaltyCard = !addLoyaltyCard
                                        && !string.IsNullOrWhiteSpace(transaction.LoyaltyCardId)
                                        && !string.IsNullOrWhiteSpace(newCart.LoyaltyCardId)
                                        && !newCart.LoyaltyCardId.Equals(transaction.LoyaltyCardId, StringComparison.OrdinalIgnoreCase);

                if (addLoyaltyCard || updateLoyaltyCard)
                {
                    context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.LoyaltyRequest);
                }
            }
        }

        /// <summary>
        /// Validate whether the employee/cashier is able to set discounts within the permissible limit.
        /// </summary>
        /// <param name="context">The request context.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Adding discount validation")]
        private static void ValidateEmployeeDiscountPermission(RequestContext context)
        {
            if (context.GetSalesTransaction() != null)
            {
                EmployeePermissions employeePermisssions = null;

                if (context.GetSalesTransaction().TotalManualDiscountAmount != 0 &&
                    context.GetSalesTransaction().TotalManualDiscountPercentage != 0)
                {
                    throw new DataValidationException(DataValidationErrors.MultipleEmployeeTotalDiscountsNotAllowed, "Total Discount Amount and Percentage cannot be set at the same time for user");
                }

                if (context.GetSalesTransaction().TotalManualDiscountAmount != 0 || context.GetSalesTransaction().TotalManualDiscountPercentage != 0)
                {
                    if (context.GetSalesTransaction().TotalManualDiscountAmount != 0)
                    {
                        context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.TotalDiscountAmount);
                    }

                    if (context.GetSalesTransaction().TotalManualDiscountPercentage != 0)
                    {
                        context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.TotalDiscountPercent);
                    }

                    employeePermisssions = EmployeePermissionHelper.GetEmployeePermissions(context, context.GetPrincipal().UserId);

                    if (employeePermisssions.MaximumTotalDiscountAmount != 0
                        && context.GetSalesTransaction().TotalManualDiscountAmount != 0 && context.GetSalesTransaction().TotalManualDiscountAmount > employeePermisssions.MaximumTotalDiscountAmount)
                    {
                        throw new DataValidationException(DataValidationErrors.EmployeeDiscountExceeded, string.Format(CultureInfo.CurrentUICulture, "Total Discount Amount exceeds the limit set for user: {0}", context.GetPrincipal().UserId));
                    }

                    if (employeePermisssions.MaximumTotalDiscountPercentage != 0
                        && context.GetSalesTransaction().TotalManualDiscountPercentage != 0
                        && context.GetSalesTransaction().TotalManualDiscountPercentage > employeePermisssions.MaximumTotalDiscountPercentage)
                    {
                        throw new DataValidationException(DataValidationErrors.EmployeeDiscountExceeded, string.Format(CultureInfo.CurrentUICulture, "Total Discount Percentage exceeds the limit set for user: {0}", context.GetPrincipal().UserId));
                    }
                }

                foreach (var salesLine in context.GetSalesTransaction().PriceCalculableSalesLines)
                {
                    if (salesLine.LineManualDiscountAmount != 0 && salesLine.LineManualDiscountPercentage != 0)
                    {
                        throw new DataValidationException(DataValidationErrors.MultipleEmployeeLineDiscountsNotAllowed, "Line Discount Amount and Percentage cannot be set at the same time for user");
                    }

                    if (salesLine.LineManualDiscountAmount != 0 || salesLine.LineManualDiscountPercentage != 0)
                    {
                        if (salesLine.LineManualDiscountAmount != 0)
                        {
                            context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.LineDiscountAmount);
                        }

                        if (salesLine.LineManualDiscountPercentage != 0)
                        {
                            context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.LineDiscountPercent);
                        }

                        if (employeePermisssions == null)
                        {
                            employeePermisssions = EmployeePermissionHelper.GetEmployeePermissions(context, context.GetPrincipal().UserId);
                        }

                        if (employeePermisssions.MaximumLineDiscountAmount != 0
                            && salesLine.LineManualDiscountAmount != 0 && salesLine.LineManualDiscountAmount > employeePermisssions.MaximumLineDiscountAmount)
                        {
                            throw new DataValidationException(DataValidationErrors.EmployeeDiscountExceeded, string.Format(CultureInfo.CurrentUICulture, "Line Discount Amount exceeds the limit set for user: {0}", context.GetPrincipal().UserId));
                        }

                        if (employeePermisssions.MaximumDiscountPercentage != 0
                            && salesLine.LineManualDiscountPercentage != 0 && salesLine.LineManualDiscountPercentage > employeePermisssions.MaximumDiscountPercentage)
                        {
                            throw new DataValidationException(DataValidationErrors.EmployeeDiscountExceeded, string.Format(CultureInfo.CurrentUICulture, "Line Discount Percentage exceeds the limit set for user: {0}", context.GetPrincipal().UserId));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Calculates taxes on the sales line for each item on the context's sales transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        private static void CalculateTaxes(RequestContext context)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");

            NetTracer.Information("CartWorkflowHelper.CalculateTaxes(): TransactionId = {0}, CustomerId = {1}", context.GetSalesTransaction().Id, context.GetSalesTransaction().CustomerId);

            CartWorkflowHelper.SetSalesTaxGroup(context);

            // apply tax overrides if any
            SalesTaxOverrideHelper.CalculateTaxOverrides(context);

            // Compute tax on items
            CalculateTaxServiceRequest calculateTaxRequest = new CalculateTaxServiceRequest();
            CalculateTaxServiceResponse calculateTaxResponse = context.Execute<CalculateTaxServiceResponse>(calculateTaxRequest);

            context.SetSalesTransaction(calculateTaxResponse.Transaction);
        }

        /// <summary>
        /// Rounds the grand total on the sales transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        private static void RoundTotals(RequestContext context)
        {
            ChannelConfiguration channelConfiguration = context.GetChannelConfiguration();

            GetRoundedValueServiceRequest request = new GetRoundedValueServiceRequest(
                context.GetSalesTransaction().TotalAmount,
                channelConfiguration.Currency,
                numberOfDecimals: 0,
                useSalesRounding: false);
            GetRoundedValueServiceResponse response = context.Execute<GetRoundedValueServiceResponse>(request);

            context.GetSalesTransaction().TotalAmount = response.RoundedValue;
        }

        /// <summary>
        /// Gets the sales transaction inventory location and site identifiers.
        /// </summary>
        /// <param name="context">The request context.</param>
        private static void GetSalesTransactionWarehouseInformation(RequestContext context)
        {
            if (string.IsNullOrWhiteSpace(context.GetSalesTransaction().InventoryLocationId))
            {
                ChannelConfiguration configuration = context.GetChannelConfiguration();

                context.GetSalesTransaction().InventoryLocationId = configuration.InventLocation;
            }
        }

        #region Cart Validations

        /// <summary>
        /// Validates whether a serialized item has more than one quantity. For a serialized item the quantity on the cart line should be one.
        /// </summary>
        /// <param name="cartLine">The cart line.</param>
        /// <param name="results">The cart line validation results.</param>
        private static void ValidateSerializedItemsViolations(CartLine cartLine, Collection<DataValidationFailure> results)
        {
            if (!string.IsNullOrWhiteSpace(cartLine.SerialNumber) && cartLine.Quantity > 1)
            {
                results.Add(new DataValidationFailure(DataValidationErrors.SerializedQuantityGreaterThanOne));
            }
        }

        /// <summary>
        /// Validates whether two operations are supposed to be performed on the same sales line.
        /// </summary>
        /// <param name="cartLines">The cart line collection.</param>
        /// <param name="lineIdSet">The line id set.</param>
        /// <param name="cartLineValidationResults">The cart line validation results.</param>
        private static void ValidateConflicts(IEnumerable<CartLine> cartLines, HashSet<string> lineIdSet, CartLineValidationResults cartLineValidationResults)
        {
            int index = 0;
            foreach (CartLine salesLine in cartLines)
            {
                if (salesLine == null)
                {
                    // If we have no sales line, there's no conflict.
                    continue;
                }

                DataValidationFailure result = null;
                if (lineIdSet.Contains(salesLine.LineId))
                {
                    result = new DataValidationFailure(OrderWorkflowErrors.ConflictingCartLineOperation);
                }

                if (!string.IsNullOrWhiteSpace(salesLine.LineId))
                {
                    lineIdSet.Add(salesLine.LineId);
                }

                if (result != null)
                {
                    cartLineValidationResults.AddLineResult(index, result);
                }

                index++;
            }
        }

        /// <summary>
        /// Validates whether sales lines have conflicts on return transaction identifier or return line numbers.
        /// </summary>
        /// <param name="returnedSalesTransaction">The returned sales transaction.</param>
        /// <param name="cartLines">The cart line collection.</param>
        /// <param name="cartLineValidationResults">The cart line validation results.</param>
        private static void ValidateReturnConflicts(SalesTransaction returnedSalesTransaction, IList<CartLine> cartLines, CartLineValidationResults cartLineValidationResults)
        {
            string returnTransactionId = null;
            if (returnedSalesTransaction != null)
            {
                returnTransactionId = returnedSalesTransaction.Id;
            }

            List<decimal> returnLineNumberList = new List<decimal>();
            foreach (CartLine cl in cartLines)
            {
                if (cl != null && cl.LineData != null && cl.LineData.IsReturnByReceipt)
                {
                    DataValidationFailure result = null;

                    if (string.IsNullOrWhiteSpace(returnTransactionId) || !returnTransactionId.Equals(cl.LineData.ReturnTransactionId, StringComparison.OrdinalIgnoreCase))
                    {
                        // Check whehter the return transaction is found and whether the cart line matches the return transaction Id
                        result = new DataValidationFailure(OrderWorkflowErrors.ConflictingCartLineOperation);
                    }
                    else if (returnLineNumberList.Contains(cl.LineData.ReturnLineNumber))
                    {
                        // Check whether the return line number already exists in the cart
                        result = new DataValidationFailure(OrderWorkflowErrors.ConflictingCartLineOperation);
                    }
                    else
                    {
                        returnLineNumberList.Add(cl.LineData.ReturnLineNumber);
                    }

                    if (result != null)
                    {
                        cartLineValidationResults.AddLineResult(cartLines.IndexOf(cl), result);
                    }
                }
            }
        }

        /// <summary>
        /// Validates whether the update operations can be performed.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="newCart">The cart with updates/ new cart from the client.</param>
        /// <param name="existingTransaction">The existing sales transaction from the DB.</param>
        /// <param name="salesLineByLineId">The dictionary of sales lines by line id.</param>
        /// <param name="returnSalesLineByLineNumber">The dictionary of return sales lines by line number.</param>
        /// <param name="isGiftCardOperation">True if request is a result of gift card operation.</param>
        /// <param name="cartLineValidationResults">The cart line validation results.</param>
        private static void ValidateSalesLineOperations(
            RequestContext context,
            Cart newCart,
            SalesTransaction existingTransaction,
            Dictionary<string, SalesLine> salesLineByLineId,
            Dictionary<decimal, SalesLine> returnSalesLineByLineNumber,
            bool isGiftCardOperation,
            CartLineValidationResults cartLineValidationResults)
        {
            int index = 0;
            bool checkReturnItemOperation = true;

            var productsById = GetProductsInCartLines(context, newCart.CartLines);

            foreach (CartLine cartLine in newCart.CartLines)
            {
                Collection<DataValidationFailure> results;

                if (cartLine == null || cartLine.LineData == null)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                                DataValidationErrors.InvalidRequest,
                                "Cart line or cart line data cannot be null.");

                    cartLineValidationResults.AddLineResult(index, validationFailure);
                }
                else
                {
                    // check for return item permission
                    // if ReturnTransactionId is present, this is return by receipt, covered by ReturnTransaction operation and should not be checked here
                    if (checkReturnItemOperation && string.IsNullOrWhiteSpace(cartLine.LineData.ReturnTransactionId) && cartLine.Quantity < 0m && !cartLine.IsVoided)
                    {
                        context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.ReturnItem);
                        checkReturnItemOperation = false;
                    }

                    if (existingTransaction.SalesLines.All(i => i.LineId != cartLine.LineId))
                    {
                        results = CartWorkflowHelper.ValidateCartLineForAdd(context, newCart, existingTransaction, salesLineByLineId, productsById, returnSalesLineByLineNumber, cartLine, isGiftCardOperation);
                    }
                    else
                    {
                        results = CartWorkflowHelper.ValidateCartLineForUpdate(context, newCart, existingTransaction, salesLineByLineId, returnSalesLineByLineNumber, cartLine);
                    }

                    ValidateSerializedItemsViolations(cartLine, results);

                    foreach (DataValidationFailure dataValidationFailure in results)
                    {
                        cartLineValidationResults.AddLineResult(index, dataValidationFailure);
                    }
                }

                index++;
            }
        }

        /// <summary>
        /// Validates whether a line can be added.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="newCart">The cart with updates/ new cart from the client.</param>
        /// <param name="existingTransaction">The existing sales transaction from the DB.</param>
        /// <param name="salesLineByLineId">The dictionary of sales lines by line id.</param>
        /// <param name="productsById">A dictionary mapping product identifiers to products for the items in the cart.</param>
        /// <param name="returnSalesLineByLineNumber">The dictionary of return sales lines by line number.</param>
        /// <param name="cartLine">The cart line.</param>
        /// <param name="isGiftCardOperation">True if request is a result of gift card operation.</param>
        /// <returns>Return the collection of validation failures for the line.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "Cart line data validation needs new objects.")]
        private static Collection<DataValidationFailure> ValidateCartLineForAdd(
            RequestContext context,
            Cart newCart,
            SalesTransaction existingTransaction,
            Dictionary<string, SalesLine> salesLineByLineId,
            Dictionary<long, Product> productsById,
            Dictionary<decimal, SalesLine> returnSalesLineByLineNumber,
            CartLine cartLine,
            bool isGiftCardOperation)
        {
            Collection<DataValidationFailure> validationFailures = new Collection<DataValidationFailure>();

            validationFailures.AddRange(ReadOnlyAttribute.CheckReadOnlyProperties(cartLine.LineData));

            // Verify that IsGiftCardLine property is set only when executing gift card operation and vice versa.
            if (context.GetChannelConfiguration().ChannelType == RetailChannelType.RetailStore && isGiftCardOperation != cartLine.LineData.IsGiftCardLine)
            {
                DataValidationFailure validationFailure = new DataValidationFailure(DataValidationErrors.PropertyUpdateNotAllowed);
                validationFailures.Add(validationFailure);
            }

            if (!cartLine.LineData.IsReturnByReceipt)
            {
                // Regular cart line (not for return)
                if (cartLine.LineData.IsProductLine
                    && string.IsNullOrWhiteSpace(cartLine.LineData.ItemId)
                    && cartLine.LineData.ProductId <= 0)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                            DataValidationErrors.ListingIdNotFound,
                            "ItemId or ListingId are not valid.");

                    validationFailures.Add(validationFailure);
                }

                // If the quantity is greater than zero, the sales line is a normal sale.
                // If the quantity is less than zero, it is a negative sale (manual return).
                if (cartLine.LineData.Quantity == 0)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                        DataValidationErrors.SalesMustHaveQuantityGreaterThanZero,
                        "Quantity must be greater than 0 for sales.");

                    validationFailures.Add(validationFailure);
                }

                // Validate if the cartline contains product Id - master/ standalone / variant.
                if (cartLine.LineData.IsProductLine)
                {
                    Product product = null;
                    if (!productsById.TryGetValue(cartLine.LineData.ProductId, out product))
                    {
                        string message = string.Format("The specified product identifier ({0}) could not be found.", cartLine.LineData.ProductId);
                        throw new DataValidationException(DataValidationErrors.ObjectNotFound, message);
                    }

                    if (product != null && product.IsMasterProduct && product.RecordId == cartLine.ProductId)
                    {
                        NotifyProductMaster(context, cartLine.ProductId);
                    }

                    ValidateProductForSale(context, product, validationFailures);
                }

                // Validate if the cart line contains an item.
                if (!string.IsNullOrWhiteSpace(cartLine.ItemId))
                {
                    Product product = CartWorkflowHelper.GetProductInfo(context, cartLine.ItemId);

                    if (product == null)
                    {
                        DataValidationFailure validationFailure = new DataValidationFailure(
                            OrderWorkflowErrors.InvalidCartSalesLineAdd,
                            "ItemId : {0} is not found",
                            cartLine.ItemId);

                        validationFailures.Add(validationFailure);
                        return validationFailures;
                    }

                    ValidateProductForSale(context, product, validationFailures);

                    ProductVariant productVariant;

                    // Check if the barcode contains the variant information, if the barcode does not have variants then try to list the product master details.
                    if (product.IsMasterProduct &&
                        !product.TryGetVariant(cartLine.InventoryDimensionId, out productVariant))
                    {
                        NotifyProductMaster(context, product.RecordId);
                    }
                }
            }
            else
            {
                // Cart line for return
                if (string.IsNullOrWhiteSpace(cartLine.LineData.ReturnTransactionId)
                    || cartLine.LineData.ReturnLineNumber <= 0
                    || returnSalesLineByLineNumber == null)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                        OrderWorkflowErrors.InvalidCartSalesLineAdd,
                        "ReturnTransactionId must be set, along with return line number.");

                    validationFailures.Add(validationFailure);

                    // cannot proceed with validations if we failed this one
                    return validationFailures;
                }

                // If the quantity is less than zero, it is a negative sale (manual return).
                if (cartLine.LineData.Quantity >= 0)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                        DataValidationErrors.ReturnsMustHaveQuantityLesserThanZero,
                        "Quantity must be lesser than 0 for returns.");

                    validationFailures.Add(validationFailure);
                }

                // Check whether the return transaction ID matches the existing cart lines for return
                // and whether the ReturnLineNumber is already in the cart
                foreach (var sl in salesLineByLineId.Values)
                {
                    if (!sl.IsVoided && sl.IsReturnByReceipt
                        && (!cartLine.LineData.ReturnTransactionId.Equals(sl.ReturnTransactionId, StringComparison.OrdinalIgnoreCase)
                            || sl.ReturnLineNumber == cartLine.LineData.ReturnLineNumber))
                    {
                        DataValidationFailure validationFailure = new DataValidationFailure(
                            OrderWorkflowErrors.InvalidCartSalesLineAdd,
                            "ReturnTransactionId or ReturnLineNumber do not match expected value.");

                        validationFailures.Add(validationFailure);
                        return validationFailures;
                    }
                }

                // Check whether the cart line is allowed for return
                // The returnSaleLine cannot be voided
                // The returnSaleLine must have the item Id
                // The return quantity cannot exceed the quantity
                var returnSalesLine = returnSalesLineByLineNumber[cartLine.LineData.ReturnLineNumber];
                if (returnSalesLine == null
                    || string.IsNullOrWhiteSpace(returnSalesLine.ItemId)
                    || returnSalesLine.IsVoided)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                            OrderWorkflowErrors.InvalidCartSalesLineAdd,
                            "Original sales line was not found or line is not valid.");

                    validationFailures.Add(validationFailure);
                    return validationFailures;
                }

                if (returnSalesLine.Quantity < returnSalesLine.ReturnQuantity + Math.Abs(cartLine.LineData.Quantity))
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                            DataValidationErrors.CannotReturnMoreThanPurchased,
                            "It is not allowed to return more quantities than the amount purchased.");

                    validationFailures.Add(validationFailure);
                }
            }

            CartType cartType = newCart.CartType != CartType.None ? newCart.CartType : existingTransaction.CartType;

            if (cartType == CartType.CustomerOrder)
            {
                CustomerOrderWorkflowHelper.ValidateCartLineForAdd(cartLine, newCart, existingTransaction, validationFailures);
            }

            return validationFailures;
        }

        private static void ValidateProductForSale(RequestContext context, Product product, ICollection<DataValidationFailure> validationFailures)
        {
            ProductRules rules = product.Rules;

            if (rules == null)
            {
                return;
            }

            DateTime currentDate = context.GetNowInChannelTimeZone().Date;

            if (currentDate < rules.DateToActivate.Date)
            {
                DataValidationFailure validationFailure = new DataValidationFailure(
                    DataValidationErrors.ProductIsNotActive,
                    "Selected product has not been activated for sale. ProductId: {0}",
                    product.RecordId);

                validationFailures.Add(validationFailure);
            }

            if (rules.IsBlocked || (currentDate >= rules.DateToBlock.Date))
            {
                DataValidationFailure validationFailure = new DataValidationFailure(
                    DataValidationErrors.ProductIsBlocked,
                    "Selected product is blocked and cannot be sold. ProductId: {0}",
                    product.RecordId);

                validationFailures.Add(validationFailure);
            }
        }

        private static void NotifyProductMaster(RequestContext context, long productId)
        {
            // Load the product details for the given Item Id and return the product master response.
            // See how we can return the product master along with the error, for now including the Product Id in the error message.
            // The client should display the product master page with all variants to choose.
            ProductMasterPageNotification notification = new ProductMasterPageNotification(productId);

            if (context.Runtime.Notify(context, notification))
            {
                var innerException = new DataValidationException(DataValidationErrors.ProductMasterPageRequired, string.Format(CultureInfo.CurrentUICulture, "The variant information for Item Id is not present, please load the product master page for product: {0}", productId));

                // throw the notification exception.
                throw new NotificationException(
                    NotificationErrors.DataValidationError,
                    notification,
                    innerException.Message,
                    innerException);
            }
        }

        /// <summary>
        /// Validates whether a line can be updated.
        /// </summary>
        /// <param name="context">The Request context.</param>
        /// <param name="newCart">The cart with updates/ new cart from the client.</param>
        /// <param name="existingTransaction">The existing sales transaction from the DB.</param>
        /// <param name="salesLineByLineId">The dictionary of sales lines by line id.</param>
        /// <param name="returnSalesLineByLineNumber">The dictionary of return sales lines by line number.</param>
        /// <param name="cartLine">The cart line.</param>
        /// <returns>Return the collection of validation failures for the line.</returns>
        private static Collection<DataValidationFailure> ValidateCartLineForUpdate(
            RequestContext context,
            Cart newCart,
            SalesTransaction existingTransaction,
            Dictionary<string, SalesLine> salesLineByLineId,
            Dictionary<decimal, SalesLine> returnSalesLineByLineNumber,
            CartLine cartLine)
        {
            Collection<DataValidationFailure> validationFailures = new Collection<DataValidationFailure>();

            if (!cartLine.IsGiftCardLine
                && !cartLine.IsInvoiceLine
                && string.IsNullOrWhiteSpace(cartLine.LineId))
            {
                // Not valid. Missing required LineId field.
                DataValidationFailure validationFailure = new DataValidationFailure(
                            OrderWorkflowErrors.InvalidCartSalesLineUpdate,
                            "LineId is missing.");

                validationFailures.Add(validationFailure);
                return validationFailures;
            }

            string cartLineDataId = cartLine.LineData.LineId;
            if (cartLineDataId != null &&
                !string.Equals(cartLineDataId, cartLine.LineId, StringComparison.Ordinal))
            {
                // Cart.LineId did not match Cart.LineData.LineId.
                DataValidationFailure validationFailure = new DataValidationFailure(
                            OrderWorkflowErrors.InvalidCartSalesLineUpdate,
                            "CartLine.LineId does not match CartLineData.LineId.");

                validationFailures.Add(validationFailure);
                return validationFailures;
            }

            if (!salesLineByLineId.ContainsKey(cartLine.LineId))
            {
                // Cart line to be updated was not found.
                DataValidationFailure validationFailure = new DataValidationFailure(
                    OrderWorkflowErrors.InvalidCartSalesLineUpdate,
                    "Cart line to be updated could not be found.");

                validationFailures.Add(validationFailure);
                return validationFailures;
            }

            var salesLine = salesLineByLineId[cartLine.LineId];
            validationFailures.AddRange(ReadOnlyAttribute.CheckReadOnlyProperties(cartLine.LineData, salesLine));

            // For POS stores the Gift card line should not be updated. This is not the cases for the online Store front sceanrio.
            if (context.GetChannelConfiguration().ChannelType == RetailChannelType.RetailStore)
            {
                if (salesLine.IsGiftCardLine != cartLine.IsGiftCardLine)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(DataValidationErrors.PropertyUpdateNotAllowed);
                    validationFailures.Add(validationFailure);
                }

                // Fail if trying to update gift card line except cases when gift card line is voided.
                if (salesLine.IsGiftCardLine && (!cartLine.IsVoided || salesLine.IsVoided))
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(DataValidationErrors.PropertyUpdateNotAllowed);
                    validationFailures.Add(validationFailure);
                }
            }

            // Depending on cart type we need different validations for operations performed on cart
            CartType cartType = newCart.CartType != CartType.None ? newCart.CartType : existingTransaction.CartType;

            switch (cartType)
            {
                case CartType.Shopping:
                case CartType.Checkout:
                    CartWorkflowHelper.ValidateCashAndCarryCartLineForUpdate(context, salesLineByLineId, returnSalesLineByLineNumber, cartLine, validationFailures);
                    break;

                case CartType.CustomerOrder:
                    CustomerOrderWorkflowHelper.ValidateCartLineForUpdate(context, newCart, existingTransaction, salesLineByLineId, cartLine, validationFailures);
                    break;

                case CartType.IncomeExpense:
                    CartWorkflowHelper.ValidateIncomeExpenseLines(existingTransaction, validationFailures);
                    break;

                default:
                    string message = string.Format(CultureInfo.InvariantCulture, "Cart type {0} is not supported.", cartType);
                    throw new NotSupportedException(message);
            }

            return validationFailures;
        }

        /// <summary>
        /// Validate income and expense lines present in the transaction.
        /// </summary>
        /// <param name="salesTransaction">Sales transaction.</param>
        /// <param name="validationFailures">Validation failures collection.</param>        
        private static void ValidateIncomeExpenseLines(SalesTransaction salesTransaction, Collection<DataValidationFailure> validationFailures)
        {
            ThrowIf.Null(salesTransaction, "salesTransaction");

            // Sales line is anot allowed in the income/ expense transactions.
            if (salesTransaction.SalesLines.Any())
            {
                var validationFailure = new DataValidationFailure(
                    DataValidationErrors.SalesLineNotAllowed,
                    "It is not possible to include sales line in income / expense transaction.");

                validationFailures.Add(validationFailure);
            }
        }

        /// <summary>
        /// Validates whether a line can be updated for a cash and carry transaction.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="salesLineByLineId">The dictionary of sales lines by line id.</param>
        /// <param name="returnSalesLineByLineNumber">The dictionary of return sales lines by line number.</param>
        /// <param name="cartLine">The cart line.</param>
        /// <param name="validationFailures">The validation result collection.</param>
        private static void ValidateCashAndCarryCartLineForUpdate(RequestContext context, Dictionary<string, SalesLine> salesLineByLineId, Dictionary<decimal, SalesLine> returnSalesLineByLineNumber, CartLine cartLine, Collection<DataValidationFailure> validationFailures)
        {
            SalesLine cartlineForUpdate = salesLineByLineId[cartLine.LineId];

            // Validates set quantity permission
            if (Math.Abs(cartlineForUpdate.Quantity) != Math.Abs(cartLine.Quantity))
            {
                context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.SetQuantity);
            }

            if (!cartlineForUpdate.IsReturnByReceipt)
            {
                // For regular cart line (not return)
                // If the quantity is greater than zero, the sales line is a normal sale.
                // If the quantity is less than zero, it is a negative sale (manual return).
                if (cartLine.LineData.Quantity == 0)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                            DataValidationErrors.SalesMustHaveQuantityGreaterThanZero,
                            "Quantity must be positive.");

                    validationFailures.Add(validationFailure);
                }

                // No price override for gift card lines.
                if (cartlineForUpdate.IsGiftCardLine && cartLine.Price != cartlineForUpdate.Price)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                            DataValidationErrors.NoPriceOverrideForGiftCards,
                            "Cannot override price for gift card items.");

                    validationFailures.Add(validationFailure);
                }

                // No price override for invoice lines.
                if (cartlineForUpdate.IsInvoiceLine && cartLine.Price != cartlineForUpdate.Price)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                            DataValidationErrors.NoPriceOverrideForInvoiceLines,
                            "Cannot override price for invoice lines.");

                    validationFailures.Add(validationFailure);
                }
            }
            else
            {
                // For cart line for return
                // Quantity must be less than zero
                if (cartLine.LineData.Quantity >= 0)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                            DataValidationErrors.ReturnsMustHaveQuantityLesserThanZero,
                            "Quantity must be less than zero.");

                    validationFailures.Add(validationFailure);
                }

                // Try to find the return sales line
                if (returnSalesLineByLineNumber == null || !returnSalesLineByLineNumber.ContainsKey(cartlineForUpdate.ReturnLineNumber))
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                            OrderWorkflowErrors.InvalidCartSalesLineUpdate,
                            "Could not find return sales line by ReturnLineNumber.");

                    validationFailures.Add(validationFailure);

                    // if we couldn't find return line, we cannot proceed with validation
                    return;
                }

                // The return quantity cannot exceed the quantity
                var returnSalesLine = returnSalesLineByLineNumber[cartlineForUpdate.ReturnLineNumber];
                if (returnSalesLine.Quantity < returnSalesLine.ReturnQuantity + Math.Abs(cartLine.LineData.Quantity))
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                            DataValidationErrors.CannotReturnMoreThanPurchased,
                            "The returned quantity exceeds purchased quantity.");

                    validationFailures.Add(validationFailure);
                }

                // No price override for return lines
                if (cartLine.Price != cartlineForUpdate.Price)
                {
                    DataValidationFailure validationFailure = new DataValidationFailure(
                            DataValidationErrors.NoPriceOverrideForReturns,
                            "Cannot override price for return items.");

                    validationFailures.Add(validationFailure);
                }
            }
        }

        /// <summary>
        /// Validates whether the unit of measure and the quantity of cart lines are set correctly.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="newCart">The cart whose cart line's unit of measure and quantity to be validated.</param>
        /// <param name="salesTransaction">The sales transaction.</param>
        /// <param name="cartLineValidationResults">The cart line validation result collection.</param>
        private static void ValidateCartLineUnitOfMeasureAndQuantity(RequestContext context, Cart newCart, SalesTransaction salesTransaction, CartLineValidationResults cartLineValidationResults)
        {
            // Retrieve all UoMs info in the cart
            IEnumerable<string> unitIds = GetUnitOfMeasureSymbols(context, newCart.CartLines);

            if (unitIds.Any())
            {
                var manager = new UnitOfMeasureConversionDataManager(context);
                var result = manager.GetUnitsOfMeasure(unitIds);

                if (result == null || !result.Any())
                {
                    throw new DataValidationException(DataValidationErrors.InvalidUnitOfMeasure, "Cart contains invalid unit of measure.");
                }

                // Verify if cart line quantity respects UoM's decimal point
                int index = 0;
                foreach (CartLine cartLine in newCart.CartLines)
                {
                    string symbol = cartLine.UnitOfMeasureSymbol;
                    decimal quantity = cartLine.Quantity;

                    // Check if the quantity and UoM symbol are set
                    if (quantity != 0 && !string.IsNullOrWhiteSpace(symbol))
                    {
                        SalesLine salesLine = salesTransaction.SalesLines.Where(l => l.LineId == cartLine.LineId).FirstOrDefault();

                        if (salesLine == null || quantity != salesLine.Quantity)
                        {
                            UnitOfMeasure uom = result.Where(unit => unit.Symbol.Equals(symbol, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                            if (uom != null)
                            {
                                // Check if decimal precision of the UoM is respected
                                if (!VerifyQuantityDecimalPrecision(quantity, uom.DecimalPrecision))
                                {
                                    DataValidationFailure validationFailure = new DataValidationFailure(
                                        DataValidationErrors.InvalidQuantity,
                                        "The quantity of item exceeds the allowed decimal precision for the unit of measure.");

                                    cartLineValidationResults.AddLineResult(index, validationFailure);
                                }
                            }
                        }
                    }

                    index++;
                }
            }
        }

        /// <summary>
        /// Retrieves the unit of measure symbols for the items in the cart.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="cartLines">The cart lines.</param>
        /// <returns>The unit of measure symbols.</returns>
        private static IEnumerable<string> GetUnitOfMeasureSymbols(RequestContext context, IEnumerable<CartLine> cartLines)
        {
            // Retreive the cart lines whose units of measure are not set
            IEnumerable<CartLine> updatingLines = cartLines.Where(cartLine => string.IsNullOrWhiteSpace(cartLine.UnitOfMeasureSymbol));

            if (updatingLines.Any())
            {
                IEnumerable<long> productIds = updatingLines.Select(l => l.ProductId).Distinct();

                var getItemsRequest = new GetItemsDataRequest(productIds);
                IEnumerable<Item> items = context.Runtime.Execute<GetItemsDataResponse>(getItemsRequest, context).Items;
                Dictionary<long, string> unitOfMeasureDict = items.ToDictionary(i => i.Product, i => i.SalesUnitOfMeasure);

                // Update the cart line unit of measure with the default value if it is not set
                foreach (CartLine cartLine in updatingLines)
                {
                    if (unitOfMeasureDict.ContainsKey(cartLine.ProductId))
                    {
                        cartLine.UnitOfMeasureSymbol = unitOfMeasureDict[cartLine.ProductId];
                    }
                }
            }

            // Retrieve the non-empty units of measure on the cart
            IEnumerable<string> units = cartLines.Where(cartLine => !string.IsNullOrWhiteSpace(cartLine.UnitOfMeasureSymbol)).Select(l => l.UnitOfMeasureSymbol).Distinct();
            return units;
        }

        /// <summary>
        /// Verifies the decimal precision of the item's unit of measure is respected.
        /// </summary>
        /// <param name="quantity">Actual quantity value of the item from cart line.</param>
        /// <param name="decimalPrecision">Allowed decimal precision for the unit of measure.</param>
        /// <returns>Return true if the quantity's decimal precision respects the unit of measure.</returns>
        private static bool VerifyQuantityDecimalPrecision(decimal quantity, int decimalPrecision)
        {
            decimal order = (decimal)Math.Pow(10, decimalPrecision);
            quantity = Math.Abs(quantity);

            if ((quantity == 0) ||
                (decimalPrecision < 0) ||
                (decimal.MaxValue / order < quantity))
            {
                return false;
            }

            decimal shift = quantity * order;           // Right shift the decimal point
            decimal integer = decimal.Truncate(shift);  // Get the integer part
            decimal fractional = shift - integer;       // Get the fractional part

            // If fractional part is 0, the quantity is in the decimal precision
            return fractional.Equals(0M);
        }

        /// <summary>
        /// Validate income or expense transactions.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="cart">Request cart.</param>
        /// <param name="cartLineValidationResults">Cart line validation results.</param>
        private static void ValidateIncomeExpenseTransaction(RequestContext context, Cart cart, CartLineValidationResults cartLineValidationResults)
        {
            if (cart.IncomeExpenseLines != null)
            {
                // Adding both income and expense account types are not valid.
                if (cart.IncomeExpenseLines.Any(incomeLine => incomeLine.AccountType == IncomeExpenseAccountType.Income)
                && cart.IncomeExpenseLines.Any(expenseLine => expenseLine.AccountType == IncomeExpenseAccountType.Expense))
                {
                    var result = new DataValidationFailure(DataValidationErrors.IncomeExpenseAccountsInSameCart, "The income/ expense line cannot contain both income and expense account types in transaction");
                    cartLineValidationResults.AddLineResult(0, result);
                }

                // Validate that the transaction status is not set on income expense lines.
                if (cart.IncomeExpenseLines.Any(incomeExpenseLine => incomeExpenseLine.TransactionStatusValue != (int)TransactionStatus.Normal))
                {
                    var result = new DataValidationFailure(DataValidationErrors.IncomeExpenseLineDoesNotAllowSettingTransactionStatus, "The income/ expense line cannot have the transaction status set.");
                }
            }

            // Validate income (or) expense permissions.
            if (cart.IncomeExpenseLines != null && cart.IncomeExpenseLines.Any())
            {
                if (cart.IncomeExpenseLines[0].AccountType == IncomeExpenseAccountType.Income)
                {
                    context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.IncomeAccounts);
                }
                else if (cart.IncomeExpenseLines[0].AccountType == IncomeExpenseAccountType.Expense)
                {
                    context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.ExpenseAccounts);
                }
            }

            // Adding customer account is not allowed.
            if (!string.IsNullOrWhiteSpace(cart.CustomerId))
            {
                var result = new DataValidationFailure(DataValidationErrors.IncomeExpenseCartDoesNotAllowCustomer, "The customer id is not allowed in income or expense transaction");
                cartLineValidationResults.AddLineResult(0, result);
            }

            // Adding cashier (or) manual discounts are not allowed.
            if (cart.TotalManualDiscountAmount != 0 || cart.TotalManualDiscountPercentage != 0)
            {
                var result = new DataValidationFailure(DataValidationErrors.IncomeExpenseCartDoesNotAllowDiscounts, "The total discount is not allowed in income or expense transaction");
                cartLineValidationResults.AddLineResult(0, result);
            }

            // Adding sales line is not allowed.
            if (cart.CartLines.Any())
            {
                var result = new DataValidationFailure(DataValidationErrors.IncomeExpenseCartDoesNotAllowSalesLine, "The sales line not allowed in income or expense transaction");
                cartLineValidationResults.AddLineResult(0, result);
            }
        }

        /// <summary>
        /// Validates whether the customer account can be updated.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="newCart">The cart with updates or new cart from the client.</param>
        /// <param name="existingTransaction">The existing sales transaction from the DB.</param>
        private static void ValidateCustomerAccount(RequestContext context, Cart newCart, SalesTransaction existingTransaction)
        {
            // Trims the customerId of new cart if it contains only blank
            if (!string.IsNullOrEmpty(newCart.CustomerId)
                && string.IsNullOrWhiteSpace(newCart.CustomerId))
            {
                newCart.CustomerId = newCart.CustomerId.Trim();
            }

            // Check if CustomerId of new cart is nonempty and does not equal to the existing CustomerId
            if (!string.IsNullOrEmpty(newCart.CustomerId)
                && !newCart.CustomerId.Equals(existingTransaction.CustomerId))
            {
                var getCustomerDataRequest = new GetCustomerDataRequest(newCart.CustomerId);
                SingleEntityDataServiceResponse<Customer> getCustomerDataResponse = context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerDataRequest, context);
                Customer customer = getCustomerDataResponse.Entity;

                // Check if the customer exists and has the same CustomerId
                if (customer == null || !customer.AccountNumber.Equals(newCart.CustomerId))
                {
                    throw new CartValidationException(DataValidationErrors.CustomerNotFound, newCart.Id, "Customer \"{0}\" not found", newCart.CustomerId);
                }
            }
        }

        /// <summary>
        /// Validates whether the loyalty card number can be updated.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="newCart">The cart with updates or new cart from the client.</param>
        /// <param name="existingTransaction">The existing sales transaction from the DB.</param>
        /// <param name="cartLineValidationResults">The cart line validation results.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "The method code is pretty short and clear.")]
        private static void ValidateLoyaltyCard(RequestContext context, Cart newCart, SalesTransaction existingTransaction, CartLineValidationResults cartLineValidationResults)
        {
            // Check whether the loyalty card number is updated.
            // 1. Add a loyalty card: The old cart does not have a loyalty card; the new cart has a loyalty card.
            // 2. Clear the loyalty card: The old cart already has a loyalty card; the new cart has the card number set to empty.
            // 3. Change a loyalty card: Both the old cart and the new cart have a loyalty card number set, but they are different.
            bool addLoyaltyCard = string.IsNullOrWhiteSpace(existingTransaction.LoyaltyCardId) && !string.IsNullOrWhiteSpace(newCart.LoyaltyCardId);
            bool clearLoyaltyCard = !addLoyaltyCard
                                    && !string.IsNullOrWhiteSpace(existingTransaction.LoyaltyCardId)
                                    && newCart.LoyaltyCardId != null
                                    && newCart.LoyaltyCardId.Length == 0;
            bool updateLoyaltyCard = !addLoyaltyCard
                                    && !clearLoyaltyCard
                                    && !string.IsNullOrWhiteSpace(existingTransaction.LoyaltyCardId)
                                    && !string.IsNullOrWhiteSpace(newCart.LoyaltyCardId)
                                    && !newCart.LoyaltyCardId.Equals(existingTransaction.LoyaltyCardId, StringComparison.OrdinalIgnoreCase);
            bool loyaltyCardChanged = addLoyaltyCard || clearLoyaltyCard || updateLoyaltyCard;

            // Check whether the customer account number is updated.
            // 1. Add a customer: The old cart does not have a customer; the new cart has a customer.
            // 2. Clear the customer: The old cart already has a customer; the new cart has the customer number set to empty.
            // 3. Change a customer: Both the old cart and the new cart have a customer number set, but they are different.
            bool addCustomer = string.IsNullOrWhiteSpace(existingTransaction.CustomerId) && !string.IsNullOrWhiteSpace(newCart.CustomerId);
            bool clearCustomer = !addCustomer
                                    && !string.IsNullOrWhiteSpace(existingTransaction.CustomerId)
                                    && newCart.CustomerId != null
                                    && newCart.CustomerId.Length == 0;
            bool updateCustomer = !addCustomer
                                    && !clearCustomer
                                    && !string.IsNullOrWhiteSpace(existingTransaction.CustomerId)
                                    && !string.IsNullOrWhiteSpace(newCart.CustomerId)
                                    && !newCart.CustomerId.Equals(existingTransaction.CustomerId, StringComparison.OrdinalIgnoreCase);
            bool customerChanged = addCustomer || clearCustomer || updateCustomer;

            if (loyaltyCardChanged && customerChanged)
            {
                // Change the loyalty card and the customer at the same time.
                var result = new DataValidationFailure(LoyaltyWorkflowErrors.CannotUpdateCustomerAndLoyaltyCardAtTheSameTime);
                cartLineValidationResults.AddLineResult(0, result);
            }
            else if (loyaltyCardChanged || customerChanged)
            {
                if (addLoyaltyCard || updateLoyaltyCard)
                {
                    // Add or update loyalty card.
                    SetCustomerFromLoyaltyCard(context, newCart, existingTransaction, cartLineValidationResults);
                }
                else if (addCustomer || updateCustomer)
                {
                    // Add or update customer
                    SetLoyaltyCardFromCustomer(context, newCart);
                }
                else if (clearLoyaltyCard)
                {
                    // Clear loyalty card should also clear the customer
                    newCart.CustomerId = string.Empty;
                }
                else if (clearCustomer)
                {
                    // Clear customer should also clear the loyalty card
                    newCart.LoyaltyCardId = string.Empty;
                }
            }
        }

        /// <summary>
        /// Sets the customer in the cart from the loyalty card.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="newCart">The cart with updates or new cart from the client.</param>
        /// <param name="existingTransaction">The existing sales transaction from the DB.</param>
        /// <param name="cartLineValidationResults">The cart line validation results.</param>
        private static void SetCustomerFromLoyaltyCard(RequestContext context, Cart newCart, SalesTransaction existingTransaction, CartLineValidationResults cartLineValidationResults)
        {
            // Check if the loyalty card exists and not blocked
            var request = new GetLoyaltyCardDataRequest(newCart.LoyaltyCardId);
            var response = context.Runtime.Execute<SingleEntityDataServiceResponse<LoyaltyCard>>(request, context);
            var loyaltyCard = response.Entity;
            if (loyaltyCard == null || string.IsNullOrWhiteSpace(loyaltyCard.CardNumber))
            {
                var result = new DataValidationFailure(LoyaltyWorkflowErrors.InvalidLoyaltyCardNumber);
                cartLineValidationResults.AddLineResult(0, result);
            }
            else if (loyaltyCard.CardTenderType == LoyaltyCardTenderType.Blocked)
            {
                var result = new DataValidationFailure(LoyaltyWorkflowErrors.BlockedLoyaltyCard);
                cartLineValidationResults.AddLineResult(0, result);
            }
            else
            {
                // For valid loyalty card...
                if (!string.IsNullOrWhiteSpace(loyaltyCard.PartyNumber))
                {
                    // The loyalty card has an owner, however the customer does not exist in the currenct channel.
                    // Try to create the customer in the current channel.
                    if (string.IsNullOrWhiteSpace(loyaltyCard.CustomerAccount))
                    {
                        Customer newCustomer = new Customer { NewCustomerPartyNumber = loyaltyCard.PartyNumber };
                        var saveCustomerServiceRequest = new SaveCustomerServiceRequest(newCustomer);
                        var saveCustomerServiceResponse = context.Execute<SaveCustomerServiceResponse>(saveCustomerServiceRequest);

                        loyaltyCard.CustomerAccount = saveCustomerServiceResponse.UpdatedCustomer.AccountNumber;
                    }

                    if (!string.IsNullOrWhiteSpace(loyaltyCard.CustomerAccount))
                    {
                        if (string.IsNullOrWhiteSpace(existingTransaction.CustomerId))
                        {
                            // Add the owner customer to the transaction.
                            newCart.CustomerId = loyaltyCard.CustomerAccount;
                        }
                        else if (!loyaltyCard.CustomerAccount.Equals(existingTransaction.CustomerId, StringComparison.OrdinalIgnoreCase))
                        {
                            // Throw error if it's different from the current customer.
                            var result = new DataValidationFailure(LoyaltyWorkflowErrors.ConflictLoyaltyCardCustomerAndTransactionCustomer);
                            cartLineValidationResults.AddLineResult(0, result);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Validates whether the quantity of each line in the aggregated cart lines is within the permissible quantity limit.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="cartLines">The cart lines whose quantity is to be checked.</param>
        private static void ValidateSalesLineQuantityLimit(RequestContext context, IEnumerable<CartLine> cartLines)
        {
            if (context.GetChannelConfiguration().ChannelType != RetailChannelType.RetailStore)
            {
                return;
            }

            ThrowIf.Null(cartLines, "cartLines");

            // Retrieve the quantity limit
            DeviceConfiguration deviceConfiguration = GetDeviceConfiguration(context);
            decimal maximumQuantity = deviceConfiguration.MaximumQuantity;

            // Verify if cart line quantity respects the allowable quantity limit
            bool quantityLimitExceeded = (maximumQuantity > 0) && cartLines.Any(cl => Math.Abs(cl.LineData.Quantity) > maximumQuantity);
            if (quantityLimitExceeded)
            {
                throw new DataValidationException(DataValidationErrors.QuantityExceeded, "The quantity of item exceeds the allowed maximum quantity.");
            }
        }

        #endregion

        /// <summary>
        /// Sets the default properties on the sales line from the item information.
        /// </summary>
        /// <param name="salesLine">The sales line from the sales transaction.</param>
        /// <param name="item">The item corresponding to the sales line.</param>
        private static void SetSalesLineDefaultsFromItemData(SalesLine salesLine, Item item)
        {
            salesLine.OriginalSalesOrderUnitOfMeasure = item.SalesUnitOfMeasure;

            if (string.IsNullOrWhiteSpace(salesLine.SalesOrderUnitOfMeasure))
            {
                salesLine.SalesOrderUnitOfMeasure = item.SalesUnitOfMeasure;
            }

            //// apply default only if there is no Unit Of Measure on the line
            if (string.IsNullOrWhiteSpace(salesLine.UnitOfMeasureSymbol))
            {
                salesLine.UnitOfMeasureSymbol = salesLine.SalesOrderUnitOfMeasure;
            }

            salesLine.InventOrderUnitOfMeasure = item.InventoryUnitOfMeasure;

            salesLine.ItemTaxGroupId = item.ItemTaxGroupId;
        }

        /// <summary>
        /// Retrieves the item data for the items in the cart.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="salesLines">The sales lines for which to retrieve item information.</param>
        /// <returns>The cart items.</returns>
        private static IEnumerable<Item> GetItemsForSalesLines(RequestContext context, IEnumerable<SalesLine> salesLines)
        {
            IEnumerable<string> itemIds = salesLines.Where(l => l.IsProductLine).Select(l => l.ItemId);

            IEnumerable<Item> items = null;
            if (itemIds.Any())
            {
                var getItemsRequest = new GetItemsDataRequest(itemIds);
                getItemsRequest.QueryResultSettings = new QueryResultSettings(new ColumnSet("ITEMID", "UNITID", "ITEMTAXGROUPID", "INVENTUNITID", "NAME", "PRODUCT"));
                var getItemsResponse = context.Runtime.Execute<GetItemsDataResponse>(getItemsRequest, context);
                items = getItemsResponse.Items;
            }
            else
            {
                items = new Collection<Item>();
            }

            return items;
        }

        /// <summary>
        /// Assigns id for the new line.
        /// </summary>
        /// <param name="salesLine">Line to update.</param>
        private static void AssignUniqueLineId(SalesLine salesLine)
        {
            // Using format 'N' to remove dashes from the GUID.
            salesLine.LineId = Guid.NewGuid().ToString("N");
            foreach (ReasonCodeLine reasonCodeLine in salesLine.ReasonCodeLines)
            {
                reasonCodeLine.ParentLineId = salesLine.LineId;
            }
        }

        /// <summary>
        /// Adds or updates affiliation lines on the cart.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="affiliationLoyaltyTiers">The affiliation lines of the cart to be updated or added.</param>
        private static void AddOrUpdateAffiliationLines(RequestContext context, IEnumerable<AffiliationLoyaltyTier> affiliationLoyaltyTiers)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(affiliationLoyaltyTiers, "affiliationLoyaltyTiers");

            SalesAffiliationLoyaltyTier salesAffiliationLoyaltyTier = null;
            SalesTransaction salesTransaction = context.GetSalesTransaction();

            // Divide affiliation and loyalty tier lines
            List<SalesAffiliationLoyaltyTier> affiliationLines = (from line in salesTransaction.AffiliationLoyaltyTierLines
                                                                  where line.AffiliationType == RetailAffiliationType.General
                                                                    && affiliationLoyaltyTiers.Any(tiers => tiers.AffiliationId == line.AffiliationId)
                                                                  select line).ToList();

            List<SalesAffiliationLoyaltyTier> loyaltyTierLines = (from line in salesTransaction.AffiliationLoyaltyTierLines
                                                                  where line.AffiliationType != RetailAffiliationType.General
                                                                  select line).ToList();

            // Keeps track of the affiliation line being created or updated. 
            Dictionary<long, SalesAffiliationLoyaltyTier> salesAffiliationLoyaltyTierByLineId = affiliationLines.ToDictionary(a => a.AffiliationId, a => a);

            foreach (AffiliationLoyaltyTier affiliationLoyaltyTier in affiliationLoyaltyTiers)
            {
                if (!salesAffiliationLoyaltyTierByLineId.ContainsKey(affiliationLoyaltyTier.AffiliationId))
                {
                    salesAffiliationLoyaltyTier = new SalesAffiliationLoyaltyTier
                    {
                        AffiliationType = affiliationLoyaltyTier.AffiliationType,
                        AffiliationId = affiliationLoyaltyTier.AffiliationId,
                        ChannelId = context.GetPrincipal().ChannelId,
                        LoyaltyTierId = affiliationLoyaltyTier.LoyaltyTierId,
                        ReceiptId = salesTransaction.ReceiptId,
                        StaffId = salesTransaction.StaffId,
                        TerminalId = salesTransaction.TerminalId,
                        TransactionId = salesTransaction.Id,
                        CustomerId = affiliationLoyaltyTier.CustomerId
                    };

                    salesAffiliationLoyaltyTierByLineId.Add(salesAffiliationLoyaltyTier.AffiliationId, salesAffiliationLoyaltyTier);
                }
                else
                {
                    salesAffiliationLoyaltyTier = salesAffiliationLoyaltyTierByLineId[affiliationLoyaltyTier.AffiliationId];
                }

                // Add or update reason code lines on the affiliation line.
                ReasonCodesWorkflowHelper.AddOrUpdateReasonCodeLinesOnAffiliationLine(salesAffiliationLoyaltyTier, affiliationLoyaltyTier, salesTransaction.Id);
            }

            // Check whether current request is related terminal or not,
            // this is in order to distinguish request from RS service or eCommerce.
            CommercePrincipal principal = context.GetPrincipal();

            if (principal != null && !principal.IsTerminalAgnostic)
            {
                // Calculate the required reason codes on the affiliation lines.
                // All the affiliations' reason codes should be calculated at the same time, 
                // because selecting multi affiliations at the same time is supported at client side.
                if (context.GetSalesTransaction().CartType != CartType.CustomerOrder || string.IsNullOrEmpty(context.GetSalesTransaction().SalesId))
                {
                    ReasonCodesWorkflowHelper.CalculateRequiredReasonCodesOnAffiliationLines(context, salesAffiliationLoyaltyTierByLineId.Values, ReasonCodeSourceType.None);
                }
            }

            // Update the affiliation lines.
            salesTransaction.AffiliationLoyaltyTierLines.Clear();
            salesTransaction.AffiliationLoyaltyTierLines.AddRange(salesAffiliationLoyaltyTierByLineId.Values);
            salesTransaction.AffiliationLoyaltyTierLines.AddRange(loyaltyTierLines);

            // Copy the affiliations of the returned transaction to current transaction.
            if (context.GetReturnedSalesTransaction() != null
                && context.GetReturnedSalesTransaction().AffiliationLoyaltyTierLines.Count > 0)
            {
                var returnedSalesAffiliationLoyaltyTier = context.GetReturnedSalesTransaction().AffiliationLoyaltyTierLines.Where(a => !IsLoyaltyTierAffiliation(context, a.AffiliationId) && !IsExistSameAffiliation(a.AffiliationId, salesTransaction.AffiliationLoyaltyTierLines));
                salesTransaction.AffiliationLoyaltyTierLines.AddRange(returnedSalesAffiliationLoyaltyTier);
            }
        }

        /// <summary>
        /// Refresh the loyalty group and tier lines on the cart.
        /// </summary>
        /// <param name="context">The request context.</param>
        private static void RefreshSalesLoyaltyTierLines(RequestContext context)
        {
            ThrowIf.Null(context, "context");

            SalesTransaction salesTransaction = context.GetSalesTransaction();

            // Clear the loyalty group and tier lines, but keep the affiliation lines.
            List<SalesAffiliationLoyaltyTier> affiliationLines = (from line in salesTransaction.AffiliationLoyaltyTierLines
                                                                  where line.AffiliationType == RetailAffiliationType.General
                                                                  select line).ToList();
            salesTransaction.AffiliationLoyaltyTierLines.Clear();
            salesTransaction.AffiliationLoyaltyTierLines.AddRange(affiliationLines);

            // Add lines from loyalty groups and tiers of the loyalty card
            if (!string.IsNullOrWhiteSpace(salesTransaction.LoyaltyCardId))
            {
                var getLoyaltyCardAffiliationsDataRequest = new GetLoyaltyCardAffiliationsDataRequest(salesTransaction.LoyaltyCardId);

                ReadOnlyCollection<SalesAffiliationLoyaltyTier> salesAffiliationLoyaltyTiers = context.Runtime
                    .Execute<EntityDataServiceResponse<SalesAffiliationLoyaltyTier>>(getLoyaltyCardAffiliationsDataRequest, context).EntityCollection;

                salesTransaction.AffiliationLoyaltyTierLines.AddRange(salesAffiliationLoyaltyTiers);
            }
        }

        /// <summary>
        /// Remotely retrieves product data from AX and saves it to the local database.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="productIdItemIdMap">A dictionary of product identifiers to item identifiers.</param>
        /// <returns>The collection of products.</returns>
        private static ReadOnlyCollection<Product> DownloadAndRetrieveRemoteProducts(RequestContext context, IDictionary<long, string> productIdItemIdMap)
        {
            NetTracer.Information(
                "Attempting to download data for folllowing products: {0} from AX.",
                string.Join(", ", productIdItemIdMap.Select(p => string.Format("ProductId: {0} ItemId: {1}", p.Key, p.Value))));

            if (!productIdItemIdMap.Any())
            {
                return Enumerable.Empty<Product>().AsReadOnly();
            }

            try
            {
                // Fetch the product data from AX.
                var getProductDataServiceRequest = new GetProductDataServiceRequest(productIdItemIdMap.Values.Distinct());
                var getProductDataServiceResponse = context.Runtime.Execute<GetProductDataServiceResponse>(getProductDataServiceRequest, context);
                var productXml = getProductDataServiceResponse.ProductDataXml;

                // Save the products locally.
                var productManager = new ProductDataManager(context);
                productManager.SaveProductData(productXml);

                var criteria = new ProductSearchCriteria(context.GetPrincipal().ChannelId);
                criteria.DataLevel = CommerceEntityDataLevel.Identity;
                criteria.Ids.AddRange(productIdItemIdMap.Keys);

                var paging = new PagingInfo(productIdItemIdMap.Count(), 0);
                var settings = new QueryResultSettings(paging);

                var products = context.Runtime.Execute<ProductSearchServiceResponse>(
                    new ProductSearchServiceRequest(criteria, settings), context).ProductSearchResult;

                return products.Results;
            }
            catch (Exception ex)
            {
                // There was a problem retrieving or saving the remote product. In either case,
                // we treat this as the scenario as if the product was simply not found.
                NetTracer.Warning(
                    ex,
                    "An exception occurred while trying to download remote products: {0} for channel {1}.",
                    string.Join(", ", productIdItemIdMap.Select(p => string.Format("ProductId: {0} ItemId: {1}", p.Key, p.Value))),
                    context.GetPrincipal().ChannelId);
            }

            return Enumerable.Empty<Product>().AsReadOnly();
        }

        /// <summary>
        /// Check whether exist same affiliation or not.
        /// </summary>
        /// <param name="affiliationId">The affiliation Id.</param>
        /// <param name="salesAffiliationLoyaltyTiers">The affiliationLoyaltyTier collection.</param>
        /// <returns>Return true if exist same affiliation, otherwise return false.</returns>
        private static bool IsExistSameAffiliation(long affiliationId, IList<SalesAffiliationLoyaltyTier> salesAffiliationLoyaltyTiers)
        {
            if (salesAffiliationLoyaltyTiers != null)
            {
                return salesAffiliationLoyaltyTiers.Any(s => s.AffiliationId == affiliationId);
            }

            return false;
        }

        /// <summary>
        /// Check whether the affiliation is loyalty affiliation or not.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="affiliationId">The affiliation Id.</param>
        /// <returns>Return true if it is loyalty affiliation, otherwise return false.</returns>
        private static bool IsLoyaltyTierAffiliation(RequestContext context, long affiliationId)
        {
            GetAffiliationsDataRequest getAffiliationsDataRequest = new GetAffiliationsDataRequest(
                RetailAffiliationType.Loyalty,
                new QueryResultSettings(new PagingInfo(PagingInfo.MaximumPageSize, 0)));

            ReadOnlyCollection<Affiliation> affiliations = context.Execute<EntityDataServiceResponse<Affiliation>>(getAffiliationsDataRequest).EntityCollection;

            return affiliations.Any(a => a.RecordId == affiliationId);
        }

        /// <summary>
        /// Check whether exist same affiliation or not.
        /// </summary>
        /// <param name="affiliationId">The affiliation Id.</param>
        /// <param name="affiliationLoyaltyTiers">The affiliationLoyaltyTier collection.</param>
        /// <returns>Return true if exist same affiliation, otherwise return false.</returns>
        private static bool IsExistSameAffiliation(long affiliationId, IList<AffiliationLoyaltyTier> affiliationLoyaltyTiers)
        {
            if (affiliationLoyaltyTiers != null)
            {
                return affiliationLoyaltyTiers.Any(s => s.AffiliationId == affiliationId);
            }

            return false;
        }

        /// <summary>
        /// Fill in charge group for charge lines.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="chargeLines">The charge lines to be updated.</param>
        /// <param name="deliverableTaxGroup">The deliverable (sales line or sales transaction) that contains the charge lines.</param>
        /// <param name="channelTaxGroup">The channel tax group.</param>
        private static void FillChargeLinesSalesTaxGroup(RequestContext context, IEnumerable<ChargeLine> chargeLines, string deliverableTaxGroup, string channelTaxGroup)
        {
            string shippingChargeCode = context.GetChannelConfiguration().ShippingChargeCode ?? string.Empty;
            string cancellationChargeCode = context.GetChannelConfiguration().CancellationChargeCode ?? string.Empty;

            // Charges follow sales line charge group
            foreach (ChargeLine chargeLine in chargeLines)
            {
                if (shippingChargeCode.Equals(chargeLine.ChargeCode, StringComparison.OrdinalIgnoreCase))
                {
                    // Shipping charge follows sales line tax group
                    chargeLine.SalesTaxGroupId = deliverableTaxGroup;
                }
                else if (cancellationChargeCode.Equals(chargeLine.ChargeCode, StringComparison.OrdinalIgnoreCase))
                {
                    // Cancellation charges must always follow current channel's tax group
                    chargeLine.SalesTaxGroupId = channelTaxGroup;
                }
                else
                {
                    // All other charges follows sales line tax group
                    chargeLine.SalesTaxGroupId = deliverableTaxGroup;
                }
            }
        }

        private static CartStatus GetCartStatus(SalesTransaction transaction)
        {
            ThrowIf.Null(transaction, "transaction");

            if (transaction.IsSuspended)
            {
                return CartStatus.Suspended;
            }

            if (transaction.EntryStatus == TransactionStatus.Normal)
            {
                return CartStatus.Created;
            }

            throw new DataValidationException(DataValidationErrors.InvalidStatus, "Cannot get the supported cart status from existing SalesTransaction record, TransactionID = {0}.", transaction.Id);
        }

        /// <summary>
        /// Gets the store by identifier.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <returns>The store.</returns>
        private static OrgUnit GetStoreById(RequestContext context, long channelId)
        {
            GetStoreDataServiceRequest request = new GetStoreDataServiceRequest(channelId);
            return context.Runtime.Execute<SingleEntityDataServiceResponse<OrgUnit>>(request, context).Entity;
        }

        /// <summary>
        /// Insert tax detail on sales line and charge line into tax details of cart.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="cart">The cart convert to.</param>
        /// <param name="salesTransaction">The salesTransaction convert from.</param>
        private static void InsertTaxDetailIntoCart(RequestContext context, Cart cart, SalesTransaction salesTransaction)
        {
            ThrowIf.Null(cart, "cart");
            ThrowIf.Null(salesTransaction, "theTransaction");

            IList<TaxLineIndia> indiaTaxItems = new List<TaxLineIndia>();

            foreach (SalesLine salesLine in salesTransaction.ActiveSalesLines)
            {
                // Add tax line on sales line
                foreach (TaxLine taxLine in salesLine.TaxLines)
                {
                    TaxLineIndia taxLineIndia = taxLine as TaxLineIndia;
                    if (taxLineIndia != null)
                    {
                        indiaTaxItems.Add(taxLineIndia);
                    }
                }

                // Add tax line on charge line of sales line
                foreach (ChargeLine chargeLine in salesLine.ChargeLines)
                {
                    foreach (TaxLine taxLine in chargeLine.TaxLines)
                    {
                        TaxLineIndia taxLineIndia = taxLine as TaxLineIndia;
                        if (taxLineIndia != null)
                        {
                            indiaTaxItems.Add(taxLineIndia);
                        }
                    }
                }
            }

            // Add tax line on charge of sales head
            foreach (ChargeLine chargeLine in salesTransaction.ChargeLines)
            {
                foreach (TaxLine taxLineInCharge in chargeLine.TaxLines)
                {
                    TaxLineIndia taxLineIndia = taxLineInCharge as TaxLineIndia;
                    if (taxLineIndia != null)
                    {
                        indiaTaxItems.Add(taxLineIndia);
                    }
                }
            }

            if (indiaTaxItems.Count > 0)
            {
                cart.TaxViewLines = BuildTaxSummaryPerComponentNotShowTaxonTax(context, indiaTaxItems);
            }
        }

        /// <summary>
        /// Build tax summary line for India, with tax amounts be aggregated by "main" tax codes (which are not India tax on tax codes).
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="indiaTaxItems">All tax items of the India retail transaction.</param>
        /// <returns>The tax summary lines of the India.</returns>
        /// <remarks>
        /// For example, the retail transaction has four sale line items, as follows,
        /// <c>
        /// Item ID | Price | Tax code | Formula       | Tax basis | Tax rate | Tax amount
        /// 0001    | 100   | SERV5    | Line amount   | 100.00    |  5%      |  5.00
        ///         |       | E-CSS5   | Excl.+[SERV5] |   5.00    |  5%      |  0.25
        /// 0002    | 100   | VAT10    | Line amount   | 100.00    | 10%      | 10.00
        ///         |       | Surchg2  | Excl.+[VAT10] |  10.00    |  2%      |  0.20
        /// 0003    | 100   | SERV4    | Line amount   | 100.00    |  4%      |  4.00
        ///         |       | E-CSS5   | Excl.+[SERV4] |   4.00    |  5%      |  0.20
        /// 0004    | 100   | VAT12    | Line amount   | 100.00    | 12%      | 12.00
        ///         |       | Surchg2  | Excl.+[VAT12] |  12.00    |  2%      |  0.24       
        /// And the tax summary lines will be as follows,
        ///  SERV5 @ 5.25    $5.25
        ///  SERV4 @ 4.20    $4.20
        /// VAT10 @ 10.20   $10.20
        /// VAT12 @ 12.24   $12.24.
        /// </c>
        /// </remarks>
        private static IList<TaxViewLine> BuildTaxSummaryPerComponentNotShowTaxonTax(RequestContext context, IList<TaxLineIndia> indiaTaxItems)
        {
            ThrowIf.Null(indiaTaxItems, "indiaTaxItems");

            if (indiaTaxItems.Count == 0)
            {
                throw new ArgumentException("The specified collection cannot be empty.", "indiaTaxItems");
            }

            // Group tax lines as per tax code and depended tax code for tax on tax line. 
            // Tax on tax lines will be grouped into the first depended line.
            List<TaxLineIndia> lines = new List<TaxLineIndia>();
            var groups = indiaTaxItems.GroupBy(x =>
            {
                string taxCode = x.IsTaxOnTax ?
                    x.TaxCodesInFormula.First() :
                    x.TaxCode;
                return new { x.TaxGroup, taxCode };
            });

            // Calculate and re-create tax lines by populating tax compoenent and tax rate as per above groups. 
            foreach (var gp in groups)
            {
                TaxLineIndia t = new TaxLineIndia();
                t.TaxGroup = gp.Key.TaxGroup;
                t.TaxCode = gp.Key.taxCode;

                var taxBasisList = from u in gp
                                   where u.IsTaxOnTax != true
                                   select u.TaxBasis;
                t.TaxBasis = taxBasisList.Sum();

                t.Amount = gp.Sum(x => x.Amount);
                t.Percentage = t.TaxBasis != decimal.Zero ? (100 * t.Amount / t.TaxBasis) : decimal.Zero;
                t.TaxComponent = gp.First(x => !x.IsTaxOnTax).TaxComponent;
                lines.Add(t);
            }

            // Build tax summary view lines based on above calculated tax lines.
            IList<TaxViewLine> taxSummary = new Collection<TaxViewLine>();
            var taxlines = from taxline in lines
                           orderby taxline.TaxComponent
                           group taxline by new { taxline.TaxComponent, taxline.Percentage };
            foreach (var group in taxlines)
            {
                TaxViewLine t = new TaxViewLine();
                ChannelConfiguration channelConfiguration = context.GetChannelConfiguration();
                GetRoundedValueServiceRequest request = new GetRoundedValueServiceRequest(
                    group.Key.Percentage,
                    channelConfiguration.Currency,
                    numberOfDecimals: 0,
                    useSalesRounding: true);
                GetRoundedValueServiceResponse response = context.Execute<GetRoundedValueServiceResponse>(request);
                decimal taxRate = response.RoundedValue;

                t.TaxId = group.Key.TaxComponent + " @ " + taxRate.ToString() + "%";
                t.TaxAmount = group.Sum(x => x.Amount);
                taxSummary.Add(t);
            }

            return taxSummary;
        }

        private static void AssociateCatalogsToSalesLines(RequestContext context)
        {
            SalesTransaction transaction = context.GetSalesTransaction();

            if (transaction != null && transaction.ActiveSalesLines.Any())
            {
                CartWorkflowHelper.PopulateMasterProductIds(context, transaction.ActiveSalesLines);

                List<long> productIds = new List<long>();
                foreach (SalesLine salesLine in transaction.PriceCalculableSalesLines)
                {
                    productIds.Add(salesLine.ProductId);
                    if (salesLine.MasterProductId != 0 && salesLine.MasterProductId != salesLine.ProductId)
                    {
                        productIds.Add(salesLine.MasterProductId);
                    }
                }

                GetProductCatalogAssociationsDataRequest getProductCatalogAssociationsRequest = new GetProductCatalogAssociationsDataRequest(productIds);
                ReadOnlyCollection<ProductCatalogAssociation> productCatalogs = context.Runtime
                    .Execute<GetProductCatalogAssociationsDataResponse>(getProductCatalogAssociationsRequest, context).CatalogAssociations;

                foreach (SalesLine salesLine in transaction.PriceCalculableSalesLines)
                {
                    foreach (ProductCatalogAssociation productCatalogAssociation in productCatalogs)
                    {
                        if (productCatalogAssociation.ProductRecordId == salesLine.ProductId || (salesLine.MasterProductId != 0 && productCatalogAssociation.ProductRecordId == salesLine.MasterProductId))
                        {
                            salesLine.CatalogIds.Add(productCatalogAssociation.CatalogRecordId);
                        }
                    }
                }
            }
        }

        private static void PopulateMasterProductIds(RequestContext context, IEnumerable<SalesLine> salesLines)
        {
            IEnumerable<string> masterItemIds = salesLines.Where(l => !string.IsNullOrWhiteSpace(l.ItemId) && !string.IsNullOrWhiteSpace(l.InventoryDimensionId)).Select(l => l.ItemId).Distinct(StringComparer.OrdinalIgnoreCase);
            if (masterItemIds.Any())
            {
                GetItemsDataRequest getItemsRequest = new GetItemsDataRequest(masterItemIds);
                getItemsRequest.QueryResultSettings = new QueryResultSettings(new ColumnSet("ItemId", "PRODUCT"));
                GetItemsDataResponse getItemsResponse = context.Execute<GetItemsDataResponse>(getItemsRequest);
                IDictionary<string, Item> itemIdDictionary = getItemsResponse.Items.ToDictionary(p => p.ItemId, p => p, StringComparer.OrdinalIgnoreCase);

                foreach (SalesLine salesLine in salesLines)
                {
                    if (salesLine.MasterProductId == 0)
                    {
                        Item item = null;
                        if (!string.IsNullOrWhiteSpace(salesLine.ItemId) && itemIdDictionary.TryGetValue(salesLine.ItemId, out item))
                        {
                            if (item.Product != salesLine.ProductId)
                            {
                                salesLine.MasterProductId = item.Product;
                            }
                        }
                    }
                }
            }
        }
    }
}