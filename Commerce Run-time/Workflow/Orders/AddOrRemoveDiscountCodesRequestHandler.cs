﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handles workflow for AddDiscountCodeToCart and RemoveDiscountCodeFromCart.
    /// </summary>
    public sealed class AddOrRemoveDiscountCodesRequestHandler : WorkflowRequestHandler<AddOrRemoveDiscountCodesRequest, SaveCartResponse>
    {
        /// <summary>
        /// Executes the workflow to add or delete discount codes in cart. 
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override SaveCartResponse Process(AddOrRemoveDiscountCodesRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.CartId, "request.CartId");
            ThrowIf.Null(request.DiscountCodes, "request.DiscountCodes");

            // In order to create or modify a cart, we need an open shift.
            this.Context.GetCheckAccessIsShiftOpenAction().Invoke();

            this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.DiscountCodeBarcode);

            // Load sales transaction.
            this.Context.SetSalesTransaction(CartWorkflowHelper.LoadSalesTransaction(this.Context, request.CartId));

            if (this.Context.GetSalesTransaction() == null)
            {
                throw new CartValidationException(DataValidationErrors.CartNotFound, request.CartId);
            }

            IEnumerable<SalesTransaction> salesTransactions = new SalesTransaction[] { this.Context.GetSalesTransaction() };
            this.Context.SetSalesTransaction(CartWorkflowHelper.ApplyCustomerFilter(request.CustomerAccountNumber, request.FilterByCustomer, salesTransactions).SingleOrDefault());

            if (this.Context.GetSalesTransaction() == null)
            {
                return new SaveCartResponse(new Cart());
            }

            if (request.DiscountCodes.Any())
            {
                this.Context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.DiscountCodeBarcode);
            }

            bool update = false;

            switch (request.DiscountCodesOperation)
            {
                case DiscountCodesOperation.Add: 
                    foreach (string discountCode in request.DiscountCodes)
                    {
                        if (!this.Context.GetSalesTransaction().DiscountCodes.Contains(discountCode))
                        {
                            this.Context.GetSalesTransaction().DiscountCodes.Add(discountCode);
                            update = true;
                        }
                    }

                    break;

                case DiscountCodesOperation.Remove:
                    foreach (string discountCode in request.DiscountCodes)
                    {
                        this.Context.GetSalesTransaction().DiscountCodes.Remove(discountCode);
                        update = true;
                    }

                    break;
                default:
                    throw new DataValidationException(DataValidationErrors.InvalidRequest, "Invalid discount code operation value: {0}", request.DiscountCodesOperation);
            }

            if (update)
            {
                // Calculate totals
                CartWorkflowHelper.Calculate(this.Context, null);

                // Save the sales transaction
                CartWorkflowHelper.SaveSalesTransaction(this.Context, this.Context.GetSalesTransaction());
            }

            return new SaveCartResponse(CartWorkflowHelper.ConvertToCart(this.Context, this.Context.GetSalesTransaction()));
        }        
    }
}
