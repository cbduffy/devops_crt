﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles workflow to get gift card.
    /// </summary>
    public sealed class GetCreditMemoRequestHandler : WorkflowRequestHandler<GetCreditMemoRequest, GetCreditMemoResponse>
    {
        /// <summary>
        /// Executes the workflow to get credit memo.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetCreditMemoRequest"/>.</param>
        /// <returns>Instance of <see cref="GetCreditMemoResponse"/>.</returns>
        protected override GetCreditMemoResponse Process(GetCreditMemoRequest request)
        {
            ThrowIf.Null(request, "request");

            // Get the invoices
            var serviceRequest = new GetCreditMemoServiceRequest(
                request.Id);
            GetCreditMemoServiceResponse serviceResponse = this.Context.Execute<GetCreditMemoServiceResponse>(serviceRequest);
            return new GetCreditMemoResponse(serviceResponse.CreditMemo);
        }
    }
}