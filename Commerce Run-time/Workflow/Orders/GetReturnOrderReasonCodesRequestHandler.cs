﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Request handler to get the return reason codes.
    /// </summary>
    public sealed class GetReturnOrderReasonCodesRequestHandler :
        WorkflowRequestHandler<GetReturnOrderReasonCodesRequest, GetReturnOrderReasonCodesResponse>
    {
        /// <summary>
        /// Gets the return reason codes from the reason code service.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.ConfigurationException">Required Service missing.</exception>
        protected override GetReturnOrderReasonCodesResponse Process(GetReturnOrderReasonCodesRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.QueryResultSettings, "request.QueryResultSettings");

            // Create service request.
            var serviceRequest = new GetReturnOrderReasonCodesServiceRequest(
                request.QueryResultSettings);

            // Execute service request.
            var serviceResponse = this.Context.Execute<GetReturnOrderReasonCodesServiceResponse>(serviceRequest);

            // Convert service response to response.
            return new GetReturnOrderReasonCodesResponse(serviceResponse.ReturnOrderReasonCodes);
        }
    }
}
