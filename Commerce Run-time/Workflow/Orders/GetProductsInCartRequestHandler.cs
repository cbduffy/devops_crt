﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Gets the products used in context of the shopping cart specified by cart id.
    /// </summary>
    public sealed class GetProductsInCartRequestHandler : WorkflowRequestHandler<GetProductsInCartRequest, GetProductsInCartResponse>
    {
        /// <summary>
        /// Processes the <see cref="GetProductsInCartRequest"/> .
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns><see cref="GetProductsInCartResponse"/> Response object containing the products used in context of the cart specified by the cart identifier.</returns>
        protected override GetProductsInCartResponse Process(GetProductsInCartRequest request)
        {
            ThrowIf.Null(request, "request");

            ReadOnlyCollection<Product> productsInCart = null;

            if (string.IsNullOrEmpty(request.CartId))
            {
                throw new DataValidationException(DataValidationErrors.InvalidRequest, "The cart identifier must be set in the request");
            }
            else
            {
                SalesTransactionDataManager datamanager = new SalesTransactionDataManager(this.Context);
                productsInCart = datamanager.GetProductsInCart(request.CartId).Results;
            }

            return new GetProductsInCartResponse(productsInCart);
        }
    }
}