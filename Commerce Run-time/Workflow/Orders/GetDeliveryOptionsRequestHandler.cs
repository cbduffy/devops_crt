﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles workflow for GetDeliveryOptions.
    /// </summary>
    public sealed class GetDeliveryOptionsRequestHandler : WorkflowRequestHandler<GetDeliveryOptionsRequest, GetDeliveryOptionsResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch line level delivery options for given cart.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetDeliveryOptionsRequest"/>.</param>
        /// <returns>Instance of <see cref="GetDeliveryOptionsResponse"/>.</returns>
        protected override GetDeliveryOptionsResponse Process(GetDeliveryOptionsRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.CartId, "request.CartId");

            // Get the Sales Transaction
            SalesTransaction salesTransaction = CartWorkflowHelper.LoadSalesTransaction(this.Context, request.CartId);
            if (salesTransaction == null)
            {
                throw new CartValidationException(DataValidationErrors.ObjectNotFound, request.CartId);
            }

            ICollection<SalesLine> requestedSalesLines = new Collection<SalesLine>();
            if (request.FetchDeliveryOptionsForLines)
            {
                requestedSalesLines = GetDeliveryOptionsRequestHandler.GetSalesLinesFromCartLineIds(salesTransaction, request.CartLineIds);

                // If shipping address was explicitly provided in the incoming request, then delivery options for the specified sales lines should
                // be calculated against the specified shipping address. However, if the shipping address is not set in the request, then the 
                // shipping address already existing on the sales line should be used for applicable delivery options calculations.
                if (request.ShippingAddress != null)
                {
                    foreach (var requestedSalesLine in requestedSalesLines)
                    {
                        requestedSalesLine.ShippingAddress = request.ShippingAddress;
                    }
                }
            }

            this.Context.SetSalesTransaction(salesTransaction);

            // Validate and resolve addresses.
            ShippingHelper.ValidateAndResolveAddresses(this.Context);

            // Get the delivery options.
            GetDeliveryOptionsResponse response;
            if (request.FetchDeliveryOptionsForLines)
            {
                // Get the delivery options for each line.
                var serviceRequest = new GetLineDeliveryOptionsServiceRequest(
                    salesTransaction,
                    requestedSalesLines,
                    request.ShippingAddress);

                var serviceResponse = this.Context.Execute<GetLineDeliveryOptionsServiceResponse>(serviceRequest);
                
                response = new GetDeliveryOptionsResponse(serviceResponse.LineDeliveryOptions);
            }
            else
            {
                // Get the delivery options that are common to all the cart lines.
                var serviceRequest = new GetOrderDeliveryOptionsServiceRequest(salesTransaction);
                var serviceResponse = this.Context.Execute<GetOrderDeliveryOptionsServiceResponse>(serviceRequest);
                
                response = new GetDeliveryOptionsResponse(serviceResponse.DeliveryOptions);
            }

            return response;
        }

        /// <summary>
        /// Gets the sales lines from the transaction that match the cart line ids provided.
        /// </summary>
        /// <param name="salesTransaction">The sales transaction.</param>
        /// <param name="cartLineIds">The cart line ids collection.</param>
        /// <returns>The sales lines from the transaction that match the cart line ids provided.</returns>
        private static ICollection<SalesLine> GetSalesLinesFromCartLineIds(
            SalesTransaction salesTransaction, ICollection<string> cartLineIds)
        {
            ICollection<SalesLine> salesLines = new List<SalesLine>();

            if (cartLineIds != null &&
                cartLineIds.Any())
            {
                salesLines = salesTransaction.ActiveSalesLines.Where(salesLine => cartLineIds.Contains(salesLine.LineId)).ToList();
            }

            return salesLines;
        }
    }
}