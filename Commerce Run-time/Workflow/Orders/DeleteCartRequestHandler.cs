﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;    

    /// <summary>
    /// Deletes a previously saved shopping cart.
    /// </summary>
    public sealed class DeleteCartRequestHandler : WorkflowRequestHandler<DeleteCartRequest, NullResponse>
    {
        /// <summary>
        /// Deletes a previously saved shopping cart.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns><see cref="NullResponse"/> object.</returns>
        protected override NullResponse Process(DeleteCartRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.CartIds, "request.CartIds");

            // if no id is supposed to be deleted
            if (!request.CartIds.Any())
            {
                return new NullResponse();
            }

            IEnumerable<SalesTransaction> transactionsToBeRemoved = request.CartIds.Select(
                id =>
                new SalesTransaction
                {
                    Id = id,
                    CustomerId = request.CustomerAccountNumber,
                    TerminalId = request.TerminalId,
                });

            var deleteCartRequest = new DeleteCartDataRequest(transactionsToBeRemoved);
            request.RequestContext.Execute<NullResponse>(deleteCartRequest);

            return new NullResponse();
        }
    }
}