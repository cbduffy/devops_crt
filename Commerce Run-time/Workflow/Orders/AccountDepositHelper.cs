﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AccountDepositHelper.cs" company="Microsoft Corporation">
// THIS CODE IS MADE AVAILABLE AS IS. MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
// OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
// THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
// NO TECHNICAL SUPPORT IS PROVIDED. YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
// </copyright>
// <summary>
// Customer Account Deposit Helper class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Customer Account Deposit Helper class.
    /// </summary>
    internal static class AccountDepositHelper
    {
        /// <summary>
        /// Validate customer account deposit transactions.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="cart">Request cart.</param>
        /// <param name="cartLineValidationResults">Cart line validation results.</param>
        public static void ValidateCustomerAccountDepositTransaction(RequestContext context, Cart cart, CartLineValidationResults cartLineValidationResults)
        {
            if (cart.CartLines == null)
            {
                return;
            }

            CartType cartType = cart.CartType == CartType.None ? context.GetSalesTransaction().CartType : cart.CartType;

            if (cart.CartLines.Any(cartline => cartline.IsCustomerAccountDeposit))
            {
                // Deposit line is only allowed if CartType is set to AccountDeposit.
                if (cartType != CartType.AccountDeposit)
                {
                    var result = new DataValidationFailure(
                        DataValidationErrors.CustomerAccountDepositCartTypeMismatch,
                        "The customer account deposit line can't be added in this type of cart.");
                    cartLineValidationResults.AddLineResult(0, result);
                }
            }

            // Make sure it is a customer account deposit transaction before applying additional validation
            if (cartType != CartType.AccountDeposit)
            {
                return;
            }

            // Check that having any other sales lines, except single account deposit line is not allowed. Even 2 account deposit lines are not allowed.
            if (cart.CartLines.HasMultiple())
            {
                var result = new DataValidationFailure(
                    DataValidationErrors.CustomerAccountDepositMultipleCartLinesNotAllowed,
                    "The customer account deposit request passed has a multiple cart line in cart");
                cartLineValidationResults.AddLineResult(0, result);
            }

            // Transaction has to have customer id
            if (string.IsNullOrWhiteSpace(context.GetSalesTransaction().CustomerId))
            {
                var result = new DataValidationFailure(DataValidationErrors.CustomerAccountNumberIsNotSet, "The customer id must be set for account deposit transaction.");
                cartLineValidationResults.AddLineResult(0, result);
            }

            // Adding cashier (or) manual discounts are not allowed.
            if (cart.TotalManualDiscountAmount != 0 || cart.TotalManualDiscountPercentage != 0)
            {
                cart.TotalManualDiscountAmount = cart.TotalManualDiscountPercentage = 0;
                var result = new DataValidationFailure(DataValidationErrors.DiscountAmountInvalidated, "The discount is not allowed in account deposit transaction.");
                cartLineValidationResults.AddLineResult(0, result);
            }

            // Adding any other lines are not allowed, except tender lines.
            if (cart.IncomeExpenseLines.Any() || cart.PromotionLines.Any())
            {
                var result = new DataValidationFailure(DataValidationErrors.CustomerAccountDepositMultipleCartLinesNotAllowed, "The customer account deposit request passed has multiple cart lines in cart.");
                cartLineValidationResults.AddLineResult(0, result);
            }

            // Analyze deposit cart line
            if (cart.CartLines.Any())
            {
                CartLine depositLine = cart.CartLines[0];

                // Only deposit, non-product line is allowed.
                if (depositLine.LineData.IsProductLine || !depositLine.IsCustomerAccountDeposit)
                {
                    var result = new DataValidationFailure(
                        DataValidationErrors.CustomerAccountDepositMultipleCartLinesNotAllowed,
                        "The customer account deposit cannot have mixed cart lines in the cart.");
                    cartLineValidationResults.AddLineResult(0, result);
                    return;
                }

                // Only positive deposit is allowed.
                if (depositLine.Price <= 0)
                {
                    var result = new DataValidationFailure(
                        DataValidationErrors.CustomerAccountDepositCannotBeNegative,
                        "The customer account deposit amount must be positive.");
                    cartLineValidationResults.AddLineResult(0, result);
                }

                // Only non-voided deposit is allowed.
                if (depositLine.IsVoided)
                {
                    var result = new DataValidationFailure(
                        DataValidationErrors.CustomerAccountDepositCannotBeVoided,
                        "The customer account deposit line cannot be voided.");
                    cartLineValidationResults.AddLineResult(0, result);
                }

                // Only quantity 1 during deposit is allowed.
                if (depositLine.Quantity != 1.0m)
                {
                    var result = new DataValidationFailure(
                        DataValidationErrors.InvalidQuantity,
                        "The customer account deposit line cannot have quantity other than one.");
                    cartLineValidationResults.AddLineResult(0, result);
                }
            }
        }

        /// <summary>
        /// Customer account deposit transactions cannot be tendered with the 'On Account' payment method.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="transaction">The transaction.</param>
        /// <param name="tenderLineBase">The tender line.</param>
        public static void ValidateCustomerAccountDepositPaymentRestrictions(RequestContext context, SalesTransaction transaction, TenderLineBase tenderLineBase)
        {
            if (transaction.CartType != CartType.AccountDeposit)
            {
                return;
            }

            var channelDataManager = new ChannelDataManager(context);
            IEnumerable<TenderType> tenderTypes = channelDataManager.GetChannelTenderTypes(
                context.GetPrincipal().ChannelId,
                new QueryResultSettings());
            TenderType tenderType = tenderTypes.Single(t => t.TenderTypeId.Equals(tenderLineBase.TenderTypeId));

            // It should not be allowed to pay with and deposit to the same customer account.
            if (tenderType.OperationType == RetailOperation.PayCustomerAccount &&
                tenderLineBase.CustomerId.Equals(transaction.CustomerId))
            {
                throw new DataValidationException(DataValidationErrors.CannotPayForCustomerAccountDepositWithCustomerAccountPaymentMethod, "Customer account deposit transactions cannot be tendered with the 'On Account' payment method.");
            }
        }
    }
}
