﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow.Composition;

    /// <summary>
    /// Handles workflow for GetBarcodesById.
    /// </summary>
    public class GetItemBarcodesByIdRequestHandler : WorkflowRequestHandler<GetItemBarcodesByIdRequest, GetItemBarcodesByIdResponse>
    {
        /// <summary>
        /// Gets batch of barcodes given list of Item Ids. an offline sales order.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetItemBarcodesByIdResponse Process(GetItemBarcodesByIdRequest request)
        {
            ThrowIf.Null(request, "request");

            ItemDataManager dataManager = new ItemDataManager(this.Context);

            return new GetItemBarcodesByIdResponse(dataManager.GetItemBarcodes(request.ItemIds));
        }
    }
}
