﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Notifications;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Gets the shopping cart specified by cart id and optionally calculates the totals on the cart.
    /// </summary>
    /// <remarks>Upon calculating the totals, the cart is saved to the database.</remarks>
    public sealed class GetCartRequestHandler : WorkflowRequestHandler<GetCartRequest, GetCartResponse>
    {
        /// <summary>
        /// Gets the shopping cart specified by cart identifier and optionally calculates the totals on the cart.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns><see cref="GetCartResponse"/> object containing the shopping cart or a new one if the flag to create is set and no cart was found.</returns>
        protected override GetCartResponse Process(GetCartRequest request)
        {
            ThrowIf.Null(request, "request");

            IEnumerable<SalesTransaction> salesTransactions = null;

            if (request.IsActive)
            {
                if (string.IsNullOrEmpty(request.CustomerAccountNumber))
                {
                    throw new NotSupportedException("Retrieving the latest cart for an anonymous customer is not supported.");
                }
                else
                {
                    // Get latest cart for specific customer
                    salesTransactions = CartWorkflowHelper.LoadActiveSalesTransaction(this.Context, request.CustomerAccountNumber);
                }
            }
            else if (request.IsSuspended)
            {
                salesTransactions = CartWorkflowHelper.LoadSuspendedSalesTransactions(this.Context);
            }
            else
            {
                if (string.IsNullOrEmpty(request.CartId) && string.IsNullOrEmpty(request.CustomerAccountNumber))
                {
                    throw new NotSupportedException("Retrieving all anonymous carts is not supported.");
                }
                else if (!string.IsNullOrWhiteSpace(request.CartId))
                {            
                    SalesTransaction transaction = CartWorkflowHelper.LoadSalesTransaction(this.Context, request.CartId);

                    if (transaction != null)
                    {
                        salesTransactions = new SalesTransaction[] { transaction };
                        salesTransactions = CartWorkflowHelper.ApplyCustomerFilter(request.CustomerAccountNumber, request.FilterByCustomer, salesTransactions);
                    }
                }
                else if (!string.IsNullOrEmpty(request.CustomerAccountNumber))
                {
                    salesTransactions = CartWorkflowHelper.LoadSalesTransactionsByCustomer(this.Context, request.CustomerAccountNumber);
                }
            }

            var carts = new List<Cart>();
            if (salesTransactions != null)
            {
                foreach (SalesTransaction salesTransaction in salesTransactions)
                {
                    this.CheckItemDiscontinuity(salesTransaction);

                    this.Context.SetSalesTransaction(salesTransaction);

                    // Calculate totals and saves the sales transaction
                    CartWorkflowHelper.Calculate(this.Context, request.CalculationModes);

                    var cart = CartWorkflowHelper.ConvertToCart(this.Context, this.Context.GetSalesTransaction());
                    carts.Add(cart);
                }
            }

            return new GetCartResponse(carts.AsReadOnly());
        }        

        /// <summary>
        /// Checks the item discontinuity.
        /// </summary>
        /// <param name="salesTransaction">The sales transaction.</param>
        private void CheckItemDiscontinuity(SalesTransaction salesTransaction)
        {
            // check whether the re-hydrated transaction has valid items - to prevent discontinued items 
            var itemDataManager = new ItemDataManager(this.Context);

            // Consider calculable lines only. Ignore voided or return-by-receipt lines.
            var itemIds = salesTransaction.InventorySalesLines.Where(l => l.IsProductLine).Select(sl => sl.ItemId).Distinct();

            var getItemsRequest = new GetItemsDataRequest(itemIds);
            getItemsRequest.QueryResultSettings = new QueryResultSettings(new ColumnSet("ITEMID"));
            var getItemsResponse = this.Context.Runtime.Execute<GetItemsDataResponse>(getItemsRequest, this.Context);

            var itemResults = getItemsResponse.Items;

            if (itemIds.Count() != itemResults.Count())
            {
                // looks like some items are missing - raise a notification
                var discontinuedItemIds = itemIds.Except(itemResults.Select(i => i.ItemId));

                var notification = new ProductDiscontinuedFromChannelNotification(discontinuedItemIds);

                // terminate the flow if it is configured so.
                if (this.Context.Runtime.Notify(this.Context, notification))
                {
                    var innerException = new DataValidationException(
                        DataValidationErrors.ItemDiscontinuedFromChannel, 
                        "Item(s) {0} have been discontinued from channel.", 
                        string.Join(", ", discontinuedItemIds));

                    throw new NotificationException(
                        NotificationErrors.DataValidationError,
                        notification,
                        innerException.Message,
                        innerException);
                }
            }
        }
    }
}