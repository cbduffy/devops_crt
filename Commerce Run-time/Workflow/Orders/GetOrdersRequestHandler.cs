﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles workflow for GetSalesOrders.
    /// </summary>
    public sealed class GetOrdersRequestHandler : WorkflowRequestHandler<GetOrdersRequest, GetOrdersResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch the sales orders.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetOrdersRequest"/>.</param>
        /// <returns>Instance of <see cref="GetOrdersResponse"/>.</returns>
        protected override GetOrdersResponse Process(GetOrdersRequest request)
        {
            ThrowIf.Null(request, "request");

            // Check permissions if trying to get transactions for a customer.
            if (!string.IsNullOrWhiteSpace(request.Criteria.CustomerAccountNumber) ||
                !string.IsNullOrWhiteSpace(request.Criteria.CustomerFirstName) ||
                !string.IsNullOrWhiteSpace(request.Criteria.CustomerLastName))
            {
                this.Context.GetCheckAccessRetailOperationAction().Invoke(DataModel.RetailOperation.CustomerTransactions);
            }

            if (request.Criteria == null || request.Criteria.IsEmpty())
            {
                throw new ArgumentException("Must pass at least one search criteria");
            }

            if (request.Criteria.SearchLocationType == SearchLocation.None)
            {
                request.Criteria.SearchLocationType = SearchLocation.All;
            }

            if (request.Criteria.SearchType == OrderSearchType.None)
            {
                request.Criteria.SearchType = OrderSearchType.SalesTransaction;
            }

            // Get the orders
            var serviceRequest = new GetOrdersServiceRequest(
                request.Criteria,
                request.QueryResultSettings);

            var serviceResponse = this.Context.Execute<GetOrdersServiceResponse>(serviceRequest);
            return new GetOrdersResponse(serviceResponse.Orders);
        }
    }
}