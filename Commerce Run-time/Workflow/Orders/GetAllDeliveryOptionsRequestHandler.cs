﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handles workflow for GetAllDeliveryOptionsRequestHandler.
    /// </summary>
    public sealed class GetAllDeliveryOptionsRequestHandler : WorkflowRequestHandler<GetAllDeliveryOptionsRequest, GetAllDeliveryOptionsResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch line level delivery options for given cart.
        /// </summary>
        /// <param name="request">Instance of <see cref="GetAllDeliveryOptionsRequest"/>.</param>
        /// <returns>Instance of <see cref="GetAllDeliveryOptionsResponse"/>.</returns>
        protected override GetAllDeliveryOptionsResponse Process(GetAllDeliveryOptionsRequest request)
        {
            ThrowIf.Null(request, "request");

            var dataServiceRequest = new GetAllDeliveryOptionsDataRequest();
            var dataServiceResponse = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<DeliveryOption>>(dataServiceRequest, this.Context);

            return new GetAllDeliveryOptionsResponse(dataServiceResponse.EntityCollection);
        }
    }
}