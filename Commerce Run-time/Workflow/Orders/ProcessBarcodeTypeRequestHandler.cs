﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    
    /// <summary>
    /// Workflow class helps to process the scanned barcode based on its type.
    /// </summary>
    public class ProcessBarcodeTypeRequestHandler : WorkflowRequestHandler<ProcessBarcodeTypeRequest, ProcessBarcodeTypeResponse>
    {
        /// <summary>
        /// Executes the workflow to process the incoming barcode.
        /// </summary>
        /// <param name="request">Instance of <see cref="ProcessBarcodeTypeRequest"/>.</param>
        /// <returns>Instance of <see cref="ProcessBarcodeTypeResponse"/>.</returns>
        protected override ProcessBarcodeTypeResponse Process(ProcessBarcodeTypeRequest request)
        {
            ThrowIf.Null(request, "request");

            ProcessBarcodeTypeResponse barcodeTypeResponse = null;

            var getBarcodeRequest = new GetBarcodeRequest(new ScanInfo { ScanDataLabel = request.Barcode });
            var getBarcodeResponse = this.Context.Runtime.Execute<GetBarcodeResponse>(getBarcodeRequest, this.Context);

            Barcode barcode = getBarcodeResponse.Barcode;

            // throw barcode not found validation exception.
            if (barcode == null)
            {
                throw new DataValidationException(DataValidationErrors.BarcodeNotFound, string.Format(CultureInfo.InvariantCulture, "Cannot find barcode details for the given barcode: {0}", request.Barcode));
            }

            // create a new cart with the cart id
            var cart = new Cart { Id = request.CartId };
            SaveCartResponse saveCartResponse;
            
            switch (barcode.MaskType)
            {
                case BarcodeMaskType.Item:
                    {
                        // Validates the item id in the barcode before proceeding
                        if (string.IsNullOrWhiteSpace(barcode.ItemId))
                        {
                            throw new DataValidationException(DataValidationErrors.BarcodeNotFound, string.Format(CultureInfo.InvariantCulture, "Cannot find item associated with the given barcode: {0}", request.Barcode));
                        }

                        // Client handles linked items search through product search call, and CRT 
                        // will not process linked item through barcode search.
                        if (barcode.HasLinkedItem)
                        {
                            throw new DataValidationException(
                                DataValidationErrors.LinkedItemSearchByBarcodeNotSupported,
                                "Search linked item {0} through barcode {1} will be processed through product searching instead.",
                                barcode.ItemId,
                                barcode.BarcodeId);
                        }

                        // QuantitySold is the default quantity set in the invent item barcode entity
                        // BarcodeQuantity is the quantity embedded in barcode
                        // BarcodePrice is the price embedded in barcode.
                        if (barcode.QuantitySold > 0 && barcode.BarcodeQuantity == 0 && barcode.BarcodePrice == 0)
                        {
                            barcode.BarcodeQuantity = barcode.QuantitySold;
                        }

                        // Calculate the quantity of the item: if the quantity was not manually set, we take the barcode quantity instead
                        decimal quantity = request.Quantity.GetValueOrDefault(0M);
                        if (quantity == 0M)
                        {
                            quantity = barcode.BarcodeQuantity > 0 ? barcode.BarcodeQuantity : 1M;
                        }

                        CartLine cartLine = CartWorkflowHelper.CreateCartLine(this.Context, request.Barcode, barcode.ItemId, barcode.VariantId, barcode.UnitId, quantity, barcode.BarcodePrice);
                        CartWorkflowHelper.AddCartLines(cart, new[] { cartLine });

                        break;
                    }

                case BarcodeMaskType.Customer:
                    {
                        cart.CustomerId = barcode.CustomerId;
                        break;
                    }

                case BarcodeMaskType.DiscountCode:
                    {
                        AddOrRemoveDiscountCodesRequest addDiscountCodesRequest = new AddOrRemoveDiscountCodesRequest(cart.Id, null, new Collection<string>() { barcode.DiscountCode }, DiscountCodesOperation.Add, filterByCustomer: false);
                        saveCartResponse = this.Context.Runtime.Execute<SaveCartResponse>(addDiscountCodesRequest, this.Context);

                        barcodeTypeResponse = new ProcessBarcodeTypeResponse(saveCartResponse.Cart);

                        return barcodeTypeResponse;
                    }

                case BarcodeMaskType.LoyaltyCard:
                    {
                        cart.LoyaltyCardId = barcode.LoyaltyCardNumber;
                        break;
                    }

                case BarcodeMaskType.None:
                    {
                        return new ProcessBarcodeTypeResponse();
                    }

                // Barcode scanning for Employee, GiftCard are just used for input to the screen. There is no requirement to add to the transaction.
                default:
                    {
                        throw new DataValidationException(DataValidationErrors.UnsupportedType, string.Format(CultureInfo.CurrentUICulture, "The barcode type : {0} is not supported to add to transaction", barcode.MaskType));
                    }
            }

            var saveCartRequest = new SaveCartRequest(cart);
            saveCartResponse = this.Context.Runtime.Execute<SaveCartResponse>(saveCartRequest, this.Context);

            barcodeTypeResponse = new ProcessBarcodeTypeResponse(saveCartResponse.Cart);

            return barcodeTypeResponse;
        }
    }
}
