﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Helper for orders related workflows.
    /// </summary>
    internal static class OrderWorkflowHelper
    {
        /// <summary>
        /// Creates a sales order with the provided payment properties.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="transaction">Sales transaction.</param>
        /// <param name="cartId">The shopping cart identifier.</param>
        /// <returns>The sales order created.</returns>
        public static SalesOrder CreateSalesOrder(RequestContext context, SalesTransaction transaction, string cartId)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(transaction, "transaction");

            NetTracer.Information("OrderWorkflowHelper.CreateSalesOrder(): TransactionId = {0}, CustomerId = {1}", transaction.Id, transaction.CustomerId);
            
            CreateSalesOrderServiceRequest createSalesOrderRequest = new CreateSalesOrderServiceRequest(cartId, transaction.CustomerId);

            var response = context.Execute<CreateSalesOrderServiceResponse>(createSalesOrderRequest);

            if (string.IsNullOrWhiteSpace(transaction.Id))
            {
                throw new DataValidationException(
                    DataValidationErrors.RequiredValueNotFound,
                    "Sales transaction id is empty");
            }

            return response.SalesOrder;
        }

        /// <summary>
        /// Validates the request context for sales order creation.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="transaction">Sales transaction.</param>
        /// <exception cref="DataValidationException">If order is not valid.</exception>
        public static void ValidateContextForCreateOrder(RequestContext context, SalesTransaction transaction)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(transaction, "context.GetSalesTransaction()");

            switch (transaction.TransactionType)
            {
                case SalesTransactionType.CustomerOrder:
                case SalesTransactionType.Sales:
                {
                    ValidateSalesLinesForCreateOrder(transaction);
                    CalculateAndValidateAmountPaidForCheckout(context, transaction);
                    break;
                }

                case SalesTransactionType.IncomeExpense:
                case SalesTransactionType.CustomerAccountDeposit:
                {
                    break;
                }

                case SalesTransactionType.PendingSalesOrder:
                {
                    // If we don't have the inventory location identifier set, we cannot create the order
                    if (string.IsNullOrWhiteSpace(transaction.InventoryLocationId))
                    {
                        throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "It is not possible to create an order without the inventory location identifier.");
                    }

                    // no additional validation required. Payment amount is validated in ProcessPendingOrderPayments() method.
                    ValidateSalesLinesForCreateOrder(transaction);
                    break;
                }
                
                default:
                    throw new InvalidOperationException(string.Format("Transaction type '{0}' is not supported.", transaction.TransactionType));
            }
        }

        /// <summary>
        /// Calculate due and paid amounts, validate they fulfill checkout requirements.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="salesTransaction">Sales transaction.</param>
        public static void CalculateAndValidateAmountPaidForCheckout(RequestContext context, SalesTransaction salesTransaction)
        {
            CartWorkflowHelper.CalculateAmountsPaidAndDue(context, salesTransaction);

            if (!salesTransaction.IsRequiredAmountPaid)
            {
                throw new DataValidationException(DataValidationErrors.AmountDueMustBePaidBeforeCheckout, "Amount due must be paid before checkout.");
            }
        }

        /// <summary>
        /// Reserves the items.
        /// </summary>
        /// <param name="context">The request context.</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This method is being exposed to be symmetrical to ReleaseItems.")]
        public static void ReserveItems(RequestContext context)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");

            NetTracer.Information("OrderWorkflowHelper.ReserveItems(): TransactionId = {0}, CustomerId = {1}", context.GetSalesTransaction().Id, context.GetSalesTransaction().CustomerId);

            // Consider calculable lines only. Ignore voided and return-by-receipt lines.
            List<ItemReservation> itemReservations = context.GetSalesTransaction().InventorySalesLines.Select(
                sl => new ItemReservation
                {
                    ItemId = sl.ItemId,
                    ExpireDateTime = DateTime.UtcNow.AddMinutes(30),
                    Quantity = sl.Quantity,
                    ReservationId = Guid.NewGuid(),
                    WarehouseInventoryDimensionId = sl.InventoryDimensionId,
                }).ToList();

            // Consider calculable lines only. Ignore voided and return-by-receipt lines.
            IEnumerable<ItemUnitConversion> itemUnitConversions = context.GetSalesTransaction().InventorySalesLines.Select(salesLine => new ItemUnitConversion()
            {
                ItemId = salesLine.ItemId,
                FromUnitOfMeasure = salesLine.SalesOrderUnitOfMeasure,
                ToUnitOfMeasure = salesLine.InventOrderUnitOfMeasure,
            });
            ChannelAvailabilityHelper.ConvertUnitOfMeasure(context, itemReservations, itemUnitConversions);

            var request = new ReserveItemsDataRequest(itemReservations);
            context.Runtime.Execute<NullResponse>(request, context);

            // If no errors occurred, update item reservation ids on the sales transaction
            int i = 0;

            // Consider calculable lines only. Ignore voided and return-by-receipt lines.
            foreach (SalesLine salesLine in context.GetSalesTransaction().InventorySalesLines)
            {
                salesLine.ReservationId = itemReservations[i++].ReservationId;
            }
        }

        /// <summary>
        /// Handle payments for the order.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="cartTenderLines">The tender lines containing authorization request.</param>
        /// <returns>The tender line created after processing payment.</returns>
        public static List<TenderLine> ProcessPendingOrderPayments(RequestContext context, IEnumerable<CartTenderLine> cartTenderLines)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");
            ThrowIf.Null(cartTenderLines, "cartTenderLines");

            // Assign id to each cart tender lines
            foreach (CartTenderLine cartTenderLine in cartTenderLines)
            {
                cartTenderLine.TenderLineId = Guid.NewGuid().ToString();
            }

            decimal totalTenderedAmount = GetPaymentsSum(cartTenderLines);
            
            // If the total of tender line amounts do not match cart total we cannot create order
            if (context.GetSalesTransaction().TotalAmount != totalTenderedAmount)
            {
                var exception = new DataValidationException(
                    DataValidationErrors.AmountDueMustBePaidBeforeCheckout, "Tender line totals do not match cart total. Transaction = {0}, Tender Total = {1}, Cart Total = {2}", context.GetSalesTransaction().Id, totalTenderedAmount, context.GetSalesTransaction().TotalAmount);
                
                throw exception;
            }
            
            NetTracer.Information("OrderWorkflowHelper.AuthorizePayments(): Transaction = {0}, CustomerId = {1}", context.GetSalesTransaction().Id, context.GetSalesTransaction().CustomerId);

            List<TenderLine> tenderLines = new List<TenderLine>();

            ChannelDataManager channelDataManager = new ChannelDataManager(context);
            channelDataManager.GetChannelConfiguration();

            try
            {
                //NS by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
                //Call genrate token for first payment tender only. Reuse the generateToke response for subsequent tender lines.
                //If genrateToken called multiple times, it will create different UniqueCardId which will lead to adverse effect in back office
                object tokenResponse = null;
                foreach (CartTenderLine cartTenderLine in cartTenderLines)
                {
                    ReadOnlyCollection<TenderType> channelTenderTypes =
                    new ChannelDataManager(context).GetChannelTenderTypes(context.GetPrincipal().ChannelId, new QueryResultSettings());
                    TenderType type = channelTenderTypes.SingleOrDefault(channelTenderType => string.Equals(channelTenderType.TenderTypeId, cartTenderLine.TenderTypeId, StringComparison.OrdinalIgnoreCase));
                    TenderLine tenderLine = new TenderLine();
                    if (type.OperationId == (int)RetailOperation.PayCard)
                    {
                        cartTenderLine["GenerateTokenResponse"] = tokenResponse;
                        tenderLine = GenerateCardTokenAndGetAuthorization(context, cartTenderLine);
                        tokenResponse = tenderLine["GenerateTokenResponse"];
                       //NE by RxL
                    }
                    else
                    {
                        tenderLine = AuthorizeAndCapturePayment(context, cartTenderLine, skipLimitValidation: false);
                    }

                    tenderLines.Add(tenderLine);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    // Cancel the payment authorizations
                    if (tenderLines.Any())
                    {
                        CancelPayments(context, tenderLines, cartTenderLines);
                    }
                }
                catch (Exception cancelPaymentsEx)
                {
                    NetTracer.Error("Voiding payments failed with: {0}, {1}, {2}", cancelPaymentsEx, cancelPaymentsEx.ToString(), ex);
                    throw;
                }

                throw;
            }

            // Setting tender lines on the salesTransaction here so they can be used for saving order in the database.
            context.GetSalesTransaction().TenderLines.Clear();
            context.GetSalesTransaction().TenderLines.AddRange(tenderLines);

            return tenderLines;
        }

        /// <summary>
        /// Cancels the card authorized payments.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="tenderLines">The tender lines containing authorization responses.</param>
        /// <param name="cartTenderLines">The cart tender lines containing authorization.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "AggregateException is rethrown at the end of method call. We like to recast any exception encountered to a PaymentException type.")]
        public static void CancelPayments(RequestContext context, IEnumerable<TenderLine> tenderLines, IEnumerable<CartTenderLine> cartTenderLines)
        {
            if (tenderLines == null || !tenderLines.Any())
            {
                // Nothing to do as there are no authorization responses.
                return;
            }

            foreach (TenderLine tenderLine in tenderLines)
            {
                // For each authorization response, cancel/void the payment.
                if (tenderLine.Status == TenderLineStatus.Committed || tenderLine.Status == TenderLineStatus.PendingCommit)
                {
                    try
                    {
                        ReadOnlyCollection<TenderType> channelTenderTypes =
                                        new ChannelDataManager(context).GetChannelTenderTypes(context.GetPrincipal().ChannelId, new QueryResultSettings());

                        TenderType tenderType = channelTenderTypes.SingleOrDefault(channelTenderType => string.Equals(channelTenderType.TenderTypeId, tenderLine.TenderTypeId, StringComparison.OrdinalIgnoreCase));
                        if (tenderType.OperationId == (int)RetailOperation.PayCard)
                        {
                            VoidCardAuthorization(context, tenderLine);
                        }
                        else
                        {
                            CartTenderLine refundCartTenderLine = cartTenderLines.SingleOrDefault(t => t.TenderLineId == tenderLine.TenderLineId);

                            if (refundCartTenderLine == null)
                            {
                                var message = string.Format(CultureInfo.InvariantCulture, "Voiding payment failed due to not being able find original tender line that represents credit card authorization. Line id: {0}.", tenderLine.TenderLineId);
                                throw new PaymentException(PaymentErrors.UnableToCancelPayment, message);
                            }

                            RefundPayment(context, refundCartTenderLine);
                        }
                    }
                    catch (PaymentException)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        var message = string.Format(CultureInfo.InvariantCulture, "Payment cancellation failed for tender line ({0}) with tender type id ({1}).", tenderLine.TenderLineId, tenderLine.TenderTypeId);
                        throw new PaymentException(PaymentErrors.UnableToCancelPayment, message, ex);
                    }
                }
            }
        }

        /// <summary>
        /// Cancels the authorized payments for only authorized card type.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="tenderLine">The tender lines containing authorization responses.</param>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "PaymentException is rethrown at the end of method call.")]
        public static void VoidCardAuthorization(RequestContext context, TenderLine tenderLine)
        {
            try
            {
                VoidPaymentServiceRequest voidRequest = new VoidPaymentServiceRequest(tenderLine);
                IRequestHandler cardPaymentHandler = context.Runtime.GetRequestHandler(voidRequest.GetType(), (int)RetailOperation.PayCard);
                context.Execute<VoidPaymentServiceResponse>(voidRequest, cardPaymentHandler);
            }
            catch (PaymentException)
            {
                throw;
            }
            catch (Exception ex)
            {
                var message = string.Format(CultureInfo.InvariantCulture, "Payment cancellation failed for tender line ({0}) with tender type id ({1}).", tenderLine.TenderLineId, tenderLine.TenderTypeId);
                throw new PaymentException(PaymentErrors.UnableToCancelPayment, message, ex);
            }
        }

        /// <summary>
        /// Refunds payment for captured card type.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="refundTenderLine">The tender lines containing authorization responses.</param>
        /// <returns>The Tender line created after processing payment for return.</returns>
        public static TenderLine RefundPayment(RequestContext context, CartTenderLine refundTenderLine)
        {
                refundTenderLine.Amount = decimal.Negate(refundTenderLine.Amount);
                return AuthorizeAndCapturePayment(context, refundTenderLine, skipLimitValidation: true);
        }

        /// <summary>
        /// Releases the items.
        /// </summary>
        /// <param name="context">The request context.</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "This method is being exposed to be symmetrical to ReserveItems.")]
        public static void ReleaseItems(RequestContext context)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(context.GetSalesTransaction(), "context.GetSalesTransaction()");

            NetTracer.Information("OrderWorkflowHelper.ReleaseItems(): TransactionId = {0}, CustomerId = {1}", context.GetSalesTransaction().Id, context.GetSalesTransaction().CustomerId);

            // Consider all lines.
            var request = new ReleaseItemsDataRequest(context.GetSalesTransaction().SalesLines.Select(s => s.ReservationId));
            context.Runtime.Execute<NullResponse>(request, context);
        }

        /// <summary>
        /// Fills the missing requirements for order creation.
        /// In this case, defaults the CustomerId to the Default Customer set on the channel if none specified.
        /// Creates an empty shipping address on null address at the order level.
        /// Populate inventory location based on the store number.
        /// </summary>
        /// <param name="context">The request context.</param>
        public static void FillMissingRequirementsForOrder(RequestContext context)
        {
            if (string.IsNullOrWhiteSpace(context.GetSalesTransaction().CustomerId))
            {
                var manager = new ChannelDataManager(context);
                var channel = manager.GetChannelById(manager.GetCurrentChannelId());

                context.GetSalesTransaction().CustomerId = channel.DefaultCustomerAccount;
            }

            if (context.GetSalesTransaction().ShippingAddress == null)
            {
                context.GetSalesTransaction().ShippingAddress = new Address();
            }

            if (!context.GetSalesTransaction().RequestedDeliveryDate.HasValue)
            {
                DateTimeOffset channelDateTime = context.GetNowInChannelTimeZone();
                context.GetSalesTransaction().RequestedDeliveryDate = channelDateTime;
            }

            // Need to fill fields for all non-voided lines.
            foreach (SalesLine salesLine in context.GetSalesTransaction().ActiveSalesLines)
            {
                if (!salesLine.RequestedDeliveryDate.HasValue)
                {
                    salesLine.RequestedDeliveryDate = context.GetSalesTransaction().RequestedDeliveryDate;
                }
            }

            // Fill store information
            OrderWorkflowHelper.FillDeliveryStoreInformation(context, context.GetSalesTransaction());
        }

        /// <summary>
        /// Gets the authorization for the given tender line.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="tenderLine">The authorization tender line.</param>
        /// <param name="paymentCard">Payment card information.</param>
        /// <returns>The response tender line.</returns>
        public static TenderLine GetAuthorization(RequestContext context, TenderLine tenderLine, PaymentCard paymentCard)
        {
            ThrowIf.Null(tenderLine, "tenderLine");
            GenerateCardTokenPaymentServiceRequest tokenRequest = new GenerateCardTokenPaymentServiceRequest(tenderLine, paymentCard);
            GenerateCardTokenPaymentServiceResponse tokenResponse = context.Execute<GenerateCardTokenPaymentServiceResponse>(tokenRequest);

            AuthorizeTokenizedCardPaymentServiceRequest authorizeRequest = new AuthorizeTokenizedCardPaymentServiceRequest(
                tokenResponse.TenderLine,
                tokenResponse.TokenizedPaymentCard,
                tokenResponse.TokenResponsePaymentProperties);

            AuthorizePaymentServiceResponse authorizeResponse = context.Execute<AuthorizePaymentServiceResponse>(authorizeRequest);

            tenderLine = authorizeResponse.TenderLine;

            return tenderLine;
        }

        /// <summary>
        /// Fill sales transaction with data from request context. 
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="transaction">The sales transaction object.</param>
        internal static void FillTransactionWithContextData(RequestContext context, SalesTransaction transaction)
        {
            ThrowIf.Null(transaction, "transaction");

            if (context.GetPrincipal() == null)
            {
                return; // do nothing
            }
        
            // Only retail stores having terminals / store identefiers
            if (context.GetChannelConfiguration().ChannelType == RetailChannelType.RetailStore)
            {
                if (context.GetTerminal() != null)
                {
                    transaction.TerminalId = context.GetTerminal().TerminalId;
                }

                if (context.GetOrgUnit() != null)
                {
                    transaction.StoreId = context.GetOrgUnit().OrgUnitNumber;
                }
            }

            // For Customer orders we allow to change Sales person
            transaction.StaffId = transaction.CartType == CartType.CustomerOrder && transaction.StaffId != null 
                ? transaction.StaffId 
                : context.GetPrincipal().UserId;

            transaction.ChannelId = context.GetPrincipal().ChannelId;
        }

        /// <summary>
        /// Fills in the receipt identifier into the request context.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="salesTransaction">The sales transaction.</param>
        /// <param name="receiptNumberSequence">The receipt number sequence.</param>
        internal static void FillInReceiptId(RequestContext context, SalesTransaction salesTransaction, string receiptNumberSequence)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(salesTransaction, "salesTransaction");

            if (context.GetPrincipal() == null)
            {
                return; // do nothing
            }

            // Only fill in receipt Id for store (not online channel)
            if (context.GetOrgUnit() != null)
            {
                GetNextReceiptIdServiceRequest getNextReceiptIdServiceRequest = new GetNextReceiptIdServiceRequest(
                    salesTransaction.TransactionType,
                    salesTransaction.NetAmountWithNoTax,
                    receiptNumberSequence,
                    salesTransaction.CustomerOrderMode);

                GetNextReceiptIdServiceResponse getNextReceiptIdServiceResponse = context.Execute<GetNextReceiptIdServiceResponse>(getNextReceiptIdServiceRequest);

                salesTransaction.ReceiptId = getNextReceiptIdServiceResponse.NextReceiptId;
            }
        }

        /// <summary>
        /// Sets the variant information on the sales lines.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="salesTransaction">The transaction to update.</param>
        internal static void FillVariantInformation(RequestContext context, SalesTransaction salesTransaction)
        {
            // Retrieve all sales lines that have variant information specified and construct the
            // item variant inventory dimension identifier for each line.
            var salesLinesWithVariants = from line in salesTransaction.SalesLines
                                         where !string.IsNullOrWhiteSpace(line.InventoryDimensionId)
                                            && (line.Variant == null || line.Variant.DistinctProductVariantId == 0)
                                         select new
                                         {
                                             SalesLine = line,
                                             ItemVariantId = new ItemVariantInventoryDimension(line.ItemId, line.InventoryDimensionId),
                                         };

            if (!salesLinesWithVariants.Any())
            {
                return;
            }

            // Retrieve all of the variants in a single database roundtrip and create a map for lookups.
            var variantsMap = new Dictionary<ItemVariantInventoryDimension, ProductVariant>();
            var itemVariantIds = salesLinesWithVariants.Select(key => key.ItemVariantId);

            var getVariantsRequest = new GetProductVariantsDataRequest(itemVariantIds);
            ReadOnlyCollection<ProductVariant> variants = context.Runtime.Execute<EntityDataServiceResponse<ProductVariant>>(getVariantsRequest, context).EntityCollection;
            variantsMap = variants.ToDictionary(key => new ItemVariantInventoryDimension(key.ItemId, key.InventoryDimensionId));

            // For all sales lines that had variants, we update the variant information.
            foreach (var result in salesLinesWithVariants)
            {
                ProductVariant variant;
                if (variantsMap.TryGetValue(result.ItemVariantId, out variant))
                {
                    result.SalesLine.Variant = variant;
                }
            }
        }

        /// <summary>
        /// Gets the payments sum.
        /// </summary>
        /// <param name="tenderLines">The tender lines.</param>
        /// <returns>The total payment made across all the tender lines.</returns>
        internal static decimal GetPaymentsSum(IEnumerable<TenderLineBase> tenderLines)
        {
            if (tenderLines == null)
            {
                throw new ArgumentNullException("tenderLines");
            }

            var notVoidedTenderLines = tenderLines.Where(t => t.Status != TenderLineStatus.Voided && t.Status != TenderLineStatus.Historical);
            decimal paymentsSum = notVoidedTenderLines.Sum(t => t.Amount);

            return paymentsSum;
        }

        /// <summary>
        /// Updates/adds tender lines such that the payments total becomes equal to order total.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="salesTransaction">Sales transaction.</param>
        /// <returns>Change tender line; null if change amount is zero.</returns>
        internal static TenderLine GetChangeTenderLine(RequestContext context, SalesTransaction salesTransaction)
        {
            if (salesTransaction.AmountDue > 0)
            {
                throw new DataValidationException(DataValidationErrors.AmountDueMustBePaidBeforeCheckout, "Amount due must be paid before checkout.");
            }

            if (salesTransaction.AmountDue == 0
                || !salesTransaction.TenderLines.Any())
            {
                // Exact payment or exchange scenario.
                return null;
            }

            decimal changeBackAmount = decimal.Negate(salesTransaction.AmountDue);
            
            // Change should be given in store currency
            string changeCurrencyCode = context.GetChannelConfiguration().Currency;

            // We will always use the very latest method of payment for providing change.
            var lastTenderLine = salesTransaction.TenderLines.Last();

            var getChangeRequest = new GetChangePaymentServiceRequest(changeBackAmount, changeCurrencyCode, lastTenderLine.TenderTypeId);
            IRequestHandler paymentManagerHandler = context.Runtime.GetRequestHandler(getChangeRequest.GetType(), ServiceTypes.PaymentManagerService);
            GetChangePaymentServiceResponse getChangeResponse = context.Execute<GetChangePaymentServiceResponse>(getChangeRequest, paymentManagerHandler);

            return getChangeResponse.TenderLine;
        }

        /// <summary>
        /// Process payments for sales transaction checkout.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="salesTransaction">Sales transaction.</param>
        /// <returns>Updated sales transaction.</returns>
        internal static SalesTransaction ProcessCheckoutPayments(RequestContext context, SalesTransaction salesTransaction)
        {
            // Add tender lines for over payment, if needed.
            TenderLine changeBackTenderLine = GetChangeTenderLine(context, salesTransaction);

            // Capture payments (will save/reload transaction).
            salesTransaction = CapturePayments(context, salesTransaction, salesTransaction.TenderLines, changeBackTenderLine);

            return salesTransaction;
        }

        /// <summary>
        /// Captures the payments and save sales transaction.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="transaction">Sales transaction.</param>
        /// <param name="tenderLines">The tender lines that represents payments.</param>
        /// <param name="changeBackTenderLine">The tender line that represents change back.</param>
        /// <returns>Returns the sales transaction updated after captures.</returns>
        internal static SalesTransaction CapturePayments(RequestContext context, SalesTransaction transaction, IList<TenderLine> tenderLines, TenderLine changeBackTenderLine)
        {
            try
            {
                // Capture payments
                for (int i = 0; i < tenderLines.Count; i++)
                {
                    var captureTenderRequest = new CapturePaymentServiceRequest(tenderLines[i]);
                    IRequestHandler paymentManagerService = context.Runtime.GetRequestHandler(captureTenderRequest.GetType(), ServiceTypes.PaymentManagerService);
                    var captureTenderResponse = context.Execute<CapturePaymentServiceResponse>(captureTenderRequest, paymentManagerService);
                    tenderLines[i] = captureTenderResponse.TenderLine;
                }

                if (changeBackTenderLine != null)
                {
                    // Authorize and capture change.
                    TenderLine authorizedChangeBackTenderLine = AuthorizePayment(context, changeBackTenderLine, paymentCard: null, skipLimitValidation: true);
                    transaction.TenderLines.Add(authorizedChangeBackTenderLine);
                    TenderLine capturedChangeBackTenderLine = CapturePayment(context, changeBackTenderLine);
                    transaction.TenderLines.Remove(authorizedChangeBackTenderLine);
                    transaction.TenderLines.Add(capturedChangeBackTenderLine);
                }
            }
            finally
            {
                // we need to save the transaction to keep tender line state up to date even in case of failures, but not suppress exception.
                CartWorkflowHelper.Calculate(context, CalculationModes.AmountDue);
                CartWorkflowHelper.SaveSalesTransaction(context, transaction);
            }

            // Loads transaction from database so future saves don't hit a version mismatch exception
            transaction = CartWorkflowHelper.LoadSalesTransaction(context, transaction.Id);

            return transaction;
        }

        /// <summary>
        /// Settle any invoice sales lines.
        /// </summary>
        /// <param name="requestContext">Request context.</param>
        /// <param name="salesTransaction">The sales transaction.</param>
        internal static void SettleInvoiceSalesLines(RequestContext requestContext, SalesTransaction salesTransaction)
        {
            if (salesTransaction != null)
            {
                foreach (SalesLine salesLine in salesTransaction.ActiveSalesLines)
                {
                    // Consder only active lines - ignore voided lines
                    if ((salesLine != null)
                        && salesLine.IsInvoiceLine
                        && !salesLine.IsInvoiceSettled)
                    {
                        try
                        {
                            // Settle the invoice lines
                            var settleInvoiceRequest = new SettleInvoiceServiceRequest(salesLine.InvoiceId, salesLine.NetAmount);
                            requestContext.Execute<NullResponse>(settleInvoiceRequest);
                            salesLine.IsInvoiceSettled = true;
                        }
                        catch (Exception ex)
                        {
                            // Inform the cashier that the invoice could not be settled. 
                            throw new DataValidationException(
                                DataValidationErrors.SettleInvoiceFailed,
                                string.Format(CultureInfo.InvariantCulture, "Exception while trying to settle payment of invoice: {0}", salesLine.InvoiceId),
                                ex);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Fills transaction with store information for pickup and shipping lines.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="salesTransaction">The sales transaction.</param>
        private static void FillDeliveryStoreInformation(RequestContext context, SalesTransaction salesTransaction)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(salesTransaction, "salesTransaction");

            // Get all non-empty stores numbers for all lines plus header
            var storeNumbers = salesTransaction.ActiveSalesLines.Select(s => s.Store).Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

            // This check is required for online channel ship to address/multi-mode shipping options scanario, where the header will not have a store id set.
            if (!string.IsNullOrWhiteSpace(salesTransaction.StoreId))
            {
                storeNumbers.Add(salesTransaction.StoreId);
            }

            // In case of online channel if the entire transaction is shipped to a specific address or 
            // If multi-mode shipping option is chosen where each item is shipped to specific address then this condition will be false.
            if (storeNumbers.Any())
            {
                // Fill store information using header's for lines that don't have it
                foreach (SalesLine salesLine in salesTransaction.ActiveSalesLines)
                {
                    if (string.IsNullOrEmpty(salesLine.Store))
                    {
                        salesLine.Store = salesTransaction.StoreId;
                    }
                }

                // Get stores
                var stores = new ChannelDataManager(context).GetStoreByStoreNumber(storeNumbers, new QueryResultSettings()).Results;
                var storeByNumber = stores.ToDictionary(s => s.OrgUnitNumber);

                // for each line, populate store information
                foreach (SalesLine salesLine in salesTransaction.ActiveSalesLines)
                {
                    OrgUnit orgUnit;
                    string storeNumber = string.IsNullOrEmpty(salesLine.Store) ? salesTransaction.StoreId : salesLine.Store;

                    // In case of online channel if multi-mode shipping option is chosen this condition will be false for items which are being shipped to an address.
                    if (!string.IsNullOrWhiteSpace(storeNumber))
                    {
                        if (!storeByNumber.TryGetValue(storeNumber, out orgUnit))
                        {
                            throw new DataValidationException(DataValidationErrors.InvalidStoreNumber, "Store with number {0} was not found for line {1}.", storeNumber, salesLine.LineId);
                        }

                        string lineDeliveryMode = string.IsNullOrWhiteSpace(salesLine.DeliveryMode)
                                                      ? salesTransaction.DeliveryMode
                                                      : salesLine.DeliveryMode;

                        salesLine.InventoryLocationId = OrderWorkflowHelper.GetInventoryLocationId(context, orgUnit, lineDeliveryMode);
                    }
                }

                // populate store info on header if set
                if (!string.IsNullOrEmpty(salesTransaction.StoreId))
                {
                    OrgUnit orgUnit;

                    if (!storeByNumber.TryGetValue(salesTransaction.StoreId, out orgUnit))
                    {
                        throw new DataValidationException(DataValidationErrors.InvalidStoreNumber, "Store with number {0} was not found for transaction header.", salesTransaction.StoreId);
                    }

                    salesTransaction.InventoryLocationId = OrderWorkflowHelper.GetInventoryLocationId(context, orgUnit, salesTransaction.DeliveryMode);
                }
            }
        }

        /// <summary>
        /// Gets the inventory location id for a specific store based on delivery mode.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="store">The store related to the location.</param>
        /// <param name="deliveryMode">Delivery mode used.</param>
        /// <returns>The warehouse id (invent location id) for the store based on the delivery mode.</returns>
        private static string GetInventoryLocationId(RequestContext context, OrgUnit store, string deliveryMode)
        {
            string pickupDeliveryMode = context.GetChannelConfiguration().PickupDeliveryModeCode ?? string.Empty;
            bool isPickup = pickupDeliveryMode.Equals(deliveryMode, StringComparison.OrdinalIgnoreCase);

            if (isPickup)
            {
                // for pickup, we use default store inventory location
                return store.InventoryLocationId ?? string.Empty;
            }
            else
            {
                // for shipping, we use the shipping warehouse, if configure, otherwise we use the default warehouse
                return string.IsNullOrWhiteSpace(store.ShippingInventLocationId)
                    ? (store.InventoryLocationId ?? string.Empty)
                    : store.ShippingInventLocationId;
            }
        }

        /// <summary>
        /// Validates the sales line collection for sales order creation.
        /// </summary>
        /// <param name="salesTransaction">The collection of sales lines.</param>
        private static void ValidateSalesLinesForCreateOrder(SalesTransaction salesTransaction)
        {
            // Verify whether items were added
            // Consider only active lines. Ignore voided lines.
            if (!salesTransaction.ActiveSalesLines.Any())
            {
                var exception = new DataValidationException(
                    DataValidationErrors.RequiredValueNotFound, "It is not possible to create an order without items.");

                throw exception;
            }

            bool hasShippingAddressAtOrderLevel = salesTransaction.ShippingAddress != null;
            bool hasDeliveryModeAtOrderLevel = !string.IsNullOrWhiteSpace(salesTransaction.DeliveryMode);

            // Consider only active lines. Ignore voided lines.
            foreach (SalesLine salesLine in salesTransaction.ActiveSalesLines)
            {
                if (salesLine == null)
                {
                    var exception = new DataValidationException(
                        DataValidationErrors.RequiredValueNotFound, "It is not possible to create an order with a null sales line.");

                    throw exception;
                }

                if (salesLine.Quantity == 0 && salesTransaction.CustomerOrderMode != CustomerOrderMode.Return)
                {
                    var exception = new DataValidationException(
                        DataValidationErrors.ValueOutOfRange, "It is not possible to create an order with no quantities set on a sales line.");

                    throw exception;
                }

                if (salesLine.IsProductLine && string.IsNullOrWhiteSpace(salesLine.ItemId))
                {
                    var exception = new DataValidationException(
                        DataValidationErrors.RequiredValueNotFound, "It is not possible to create an order with an empty sales line item identifier.");

                    throw exception;
                }

                if (!salesTransaction.IsSales && !hasShippingAddressAtOrderLevel && salesLine.ShippingAddress == null)
                {
                    var exception = new DataValidationException(
                        DataValidationErrors.RequiredValueNotFound, "It is not possible to create an order without a shipping address.");

                    throw exception;
                }

                if (!salesTransaction.IsSales && !hasDeliveryModeAtOrderLevel && string.IsNullOrWhiteSpace(salesLine.DeliveryMode))
                {
                    var exception = new DataValidationException(
                        DataValidationErrors.RequiredValueNotFound, "It is not possible to create an order without a delivery mode code.");

                    throw exception;
                }
            }
        }

        /// <summary>
        /// Generates token and gets authorization.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="cartTenderLine">The authorization cart tender line.</param>
        /// <returns>The tender line created after processing payment.</returns>
        private static TenderLine GenerateCardTokenAndGetAuthorization(RequestContext context, CartTenderLine cartTenderLine)
        {
            ThrowIf.Null(cartTenderLine, "tenderLine");
            TenderLine tenderLine = new TenderLine { TenderLineId = cartTenderLine.TenderLineId };

            tenderLine.CopyPropertiesFrom(cartTenderLine);

            TokenizedPaymentCard tokenizedPaymentCard;
            string tokenResponsePaymentProperties = string.Empty;
            
            if (cartTenderLine.PaymentCard != null)
            {
                //NS by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
                GenerateCardTokenPaymentServiceResponse tokenResponse =  null;
                if(cartTenderLine["GenerateTokenResponse"] != null)
                {
                    tokenResponse = cartTenderLine["GenerateTokenResponse"] as GenerateCardTokenPaymentServiceResponse;
                                       
                    tokenResponse.TokenizedPaymentCard["LineConfirmationGroupId"] = cartTenderLine.PaymentCard["LineConfirmationGroupId"];
                }
                else
                {
                    GenerateCardTokenPaymentServiceRequest tokenRequest = new GenerateCardTokenPaymentServiceRequest(tenderLine, cartTenderLine.PaymentCard);
                    tokenResponse = context.Execute<GenerateCardTokenPaymentServiceResponse>(tokenRequest);
                }
                tenderLine["GenerateTokenResponse"] = tokenResponse;

                tenderLine.Authorization = tokenResponse.TenderLine.Authorization;
                //NE by RxL
                tokenizedPaymentCard = tokenResponse.TokenizedPaymentCard;
                tokenResponsePaymentProperties = tokenResponse.TokenResponsePaymentProperties;
            }
            else if (cartTenderLine.TokenizedPaymentCard != null)
            {
                // Token response properties blob will remain empty since this token was generated externally.
                tokenizedPaymentCard = cartTenderLine.TokenizedPaymentCard;
            }
            else
            {
                throw new DataValidationException(PaymentErrors.UnableToAuthorizePayment, "Either one of PaymentCard or TokenizedPaymentCard must be set on cartTenderLine.");
            }

            AuthorizeTokenizedCardPaymentServiceRequest authorizeRequest = new AuthorizeTokenizedCardPaymentServiceRequest(
                tenderLine,
                tokenizedPaymentCard,
                tokenResponsePaymentProperties);

            IRequestHandler cardPaymentHandler = context.Runtime.GetRequestHandler(authorizeRequest.GetType(), (int)RetailOperation.PayCard);
            AuthorizePaymentServiceResponse authorizeResponse = context.Execute<AuthorizePaymentServiceResponse>(authorizeRequest, cardPaymentHandler);
                       
            tenderLine = authorizeResponse.TenderLine;

            return tenderLine;
        }

        /// <summary>
        /// Authorizes and captures the payment.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="cartTenderLine">The authorization tender line.</param>
        /// <param name="skipLimitValidation">If set to 'true' limits validation (over tender, under tender etc.) will be skipped.</param>
        /// <returns>The tender line created after payment processing.</returns>
        private static TenderLine AuthorizeAndCapturePayment(RequestContext context, CartTenderLine cartTenderLine, bool skipLimitValidation)
        {
            ThrowIf.Null(cartTenderLine, "cartTenderLine");
            TenderLine tenderLine = new TenderLine { TenderLineId = cartTenderLine.TenderLineId };
            tenderLine.CopyPropertiesFrom(cartTenderLine);
            var authorizedTenderLine = AuthorizePayment(context, tenderLine, cartTenderLine.PaymentCard, skipLimitValidation);
            return CapturePayment(context, authorizedTenderLine);
        }
        
        /// <summary>
        /// Authorizes the payment.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="tenderLine">Tender line to authorize.</param>
        /// <param name="paymentCard">Payment card information (optional).</param>
        /// <param name="skipLimitValidation">If set to 'true' limits validation (over-tender, under-tender etc.) will be skipped.</param>
        /// <returns>Tender line that represents authorized payment.</returns>
        private static TenderLine AuthorizePayment(RequestContext context, TenderLine tenderLine, PaymentCard paymentCard, bool skipLimitValidation)
        {
            AuthorizePaymentServiceRequest authorizeRequest = new AuthorizePaymentServiceRequest(tenderLine, paymentCard, skipLimitValidation);

            IRequestHandler paymentManagerHandler = context.Runtime.GetRequestHandler(authorizeRequest.GetType(), ServiceTypes.PaymentManagerService);

            var authorizeResponse = context.Execute<AuthorizePaymentServiceResponse>(authorizeRequest, paymentManagerHandler);
            if (authorizeResponse.TenderLine == null)
            {
                throw new PaymentException(PaymentErrors.UnableToAuthorizePayment, "Payment service did not return tender line.");
            }

            return authorizeResponse.TenderLine;
        }

        /// <summary>
        /// Captures the payment.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="tenderLine">Tender line to capture.</param>
        /// <returns>Tender line that represents captured payment.</returns>
        private static TenderLine CapturePayment(RequestContext context, TenderLine tenderLine)
        {
            switch (tenderLine.Status)
            {
                case TenderLineStatus.PendingCommit:
                    {
                        CapturePaymentServiceRequest captureRequest = new CapturePaymentServiceRequest(tenderLine);
                        IRequestHandler paymentManagerHandler = context.Runtime.GetRequestHandler(captureRequest.GetType(), ServiceTypes.PaymentManagerService);
                        CapturePaymentServiceResponse captureResponse = context.Execute<CapturePaymentServiceResponse>(captureRequest, paymentManagerHandler);
                        return captureResponse.TenderLine;
                    }

                case TenderLineStatus.Committed:
                    {
                        return tenderLine;
                    }

                default:
                    {
                        throw new InvalidOperationException(string.Format("Payment authorization returned unexpected tender line status: {0}", tenderLine.Status));
                    }
            }
        }
    }
}