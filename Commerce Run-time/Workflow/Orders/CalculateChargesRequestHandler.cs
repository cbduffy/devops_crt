﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Handles workflow for CalculateCharges.
    /// </summary>
    public sealed class CalculateChargesRequestHandler : WorkflowRequestHandler<CalculateChargesRequest, CalculateChargesResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch the sales orders.
        /// </summary>
        /// <param name="request">Instance of <see cref="CalculateChargesRequest"/>.</param>
        /// <returns>Instance of <see cref="CalculateChargesResponse"/>.</returns>
        protected override CalculateChargesResponse Process(CalculateChargesRequest request)
        {
            ThrowIf.Null(request, "request");

            // Get the orders
            var serviceRequest = new GetChargesServiceRequest(request.Transaction);

            var serviceResponse = this.Context.Execute<GetChargesServiceResponse>(serviceRequest);
            return new CalculateChargesResponse(serviceResponse.Transaction);
        }
    }
}