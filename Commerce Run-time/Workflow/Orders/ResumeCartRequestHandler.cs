﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handles workflow to resume suspended cart.
    /// </summary>
    public sealed class ResumeCartRequestHandler : WorkflowRequestHandler<ResumeCartRequest, ResumeCartResponse>
    {
        /// <summary>
        /// Executes the workflow to resume suspended cart.
        /// </summary>
        /// <param name="request">Instance of <see cref="ResumeCartRequest"/>.</param>
        /// <returns>Instance of <see cref="ResumeCartResponse"/>.</returns>
        protected override ResumeCartResponse Process(ResumeCartRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(DataModel.RetailOperation.RecallTransaction);

            SalesTransaction transaction = CartWorkflowHelper.LoadSalesTransaction(this.Context, request.CartId);

            if (!transaction.IsSuspended)
            {
                throw new CartValidationException(DataValidationErrors.InvalidStatus, request.CartId, "Cart is not suspended.");
            }

            // Resume the suspended transaction to normal state.
            transaction.EntryStatus = TransactionStatus.Normal;
            transaction.IsSuspended = false;
            transaction.TerminalId = this.Context.GetTerminal().TerminalId;
            transaction.BeginDateTime = this.Context.GetNowInChannelTimeZone();
            this.Context.SetSalesTransaction(transaction);
            CartWorkflowHelper.Calculate(this.Context, CalculationModes.Totals | CalculationModes.AmountDue | CalculationModes.Deposit);
            CartWorkflowHelper.SaveSalesTransaction(this.Context, transaction);
            Cart cart = CartWorkflowHelper.ConvertToCart(this.Context, transaction);
            return new ResumeCartResponse(cart);
        }
    }
}