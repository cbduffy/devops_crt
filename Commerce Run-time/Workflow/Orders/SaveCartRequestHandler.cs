﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Saves a shopping cart.
    /// </summary>
    public sealed class CreateOrUpdateCartRequestHandler : WorkflowRequestHandler<SaveCartRequest, SaveCartResponse>
    {
        /// <summary>
        /// Saves (updating if it exists and creating a new one if it does not) the shopping cart on the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns><see cref="SaveCartResponse"/> object containing the cart with updated item quantities.</returns>
        protected override SaveCartResponse Process(SaveCartRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.Cart, "request.Cart");

            SalesTransaction salesTransaction;

            // in order to create or modify a cart, we need an open shift
            this.Context.GetCheckAccessIsShiftOpenAction().Invoke();

            // For "add to gift card" and "issue gift card" operations permission check is done in GiftCardService.
            // For "returns", permission check is done in workflow helper.
            if (request.Cart.CartLines.Any(l => !l.IsGiftCardLine && !l.IsVoided && l.Quantity >= 0m))
            {
                // Validate permissions.
                this.Context.GetCheckAccessRetailOperationAction().Invoke(RetailOperation.ItemSale);
            }

            if (string.IsNullOrWhiteSpace(request.Cart.Id))
            {
                request.Cart.Id = CartWorkflowHelper.GenerateRandomTransactionId();
            }

            // Copy the logic from CartService.CreateCart().
            foreach (CartLine line in request.Cart.CartLines)
            {
                // Sets the IsReturn flag to true, when ReturnTransactionId is specified.
                // The reason of doing so is that the IsReturn is not currently exposed on CartLine entity.
                if (!string.IsNullOrEmpty(line.ReturnTransactionId))
                {
                    line.LineData.IsReturnByReceipt = true;
                }
            }

            // Get the Sales Transaction
            salesTransaction = CartWorkflowHelper.LoadSalesTransaction(this.Context, request.Cart.Id);
            
            if (salesTransaction == null)
            {
                // New transaction - set the default cart type to shopping if none
                if (request.Cart.CartType == CartType.None)
                {
                    request.Cart.CartType = CartType.Shopping;
                }
            }

            CartWorkflowHelper.ValidateCartPermissions(salesTransaction, request.Cart, this.Context);

            if (salesTransaction == null)
            {
                // New transaction - set the default cart type to shopping if none
                if (request.Cart.CartType == CartType.None)
                {
                    request.Cart.CartType = CartType.Shopping;
                }

                // Set loyalty card from the customer number
                CartWorkflowHelper.SetLoyaltyCardFromCustomer(this.Context, request.Cart);

                // Set affiliations from the customer number
                CartWorkflowHelper.AddOrUpdateAffiliationLinesFromCustomer(this.Context, null, request.Cart);

                // If cannot find the transaction, create a new transaction.
                salesTransaction = CartWorkflowHelper.CreateSalesTransaction(this.Context, request.Cart.Id, request.Cart.CustomerId);

                // Update transaction level reason code lines.
                ReasonCodesWorkflowHelper.AddOrUpdateReasonCodeLinesOnTransaction(salesTransaction, request.Cart);

                // Calculate required reason code lines for start of transaction.
                ReasonCodesWorkflowHelper.CalculateRequiredReasonCodesOnTransaction(this.Context, salesTransaction, ReasonCodeSourceType.StartOfTransaction);
            }

            // If cart or the sales transaction is suspended then update is not permitted
            if (salesTransaction.IsSuspended)
            {
                throw new CartValidationException(DataValidationErrors.CartNotActive, request.Cart.Id);
            }
            
            // If the terminal id of the cart is not same as the context then it means that the cart is active on another terminal.
            GetCurrentTerminalIdDataRequest dataRequest = new GetCurrentTerminalIdDataRequest();
            if (!(salesTransaction.TerminalId ?? string.Empty).Equals(this.Context.Execute<SingleEntityDataServiceResponse<string>>(dataRequest).Entity ?? string.Empty, StringComparison.OrdinalIgnoreCase))
            {
                throw new CartValidationException(DataValidationErrors.LoadingActiveCartFromAnotherTerminalNotAllowed, request.Cart.Id);
            }

            // At this point, the sales transaction is either newly created with no sales lines or just loaded from DB. 
            // We are yet to add new sales lines or update existing sales lines.
            this.Context.SetSalesTransaction(salesTransaction);
           
            // Get the returned sales transaction if the cart contains a return line
            this.Context.SetReturnedSalesTransaction(CartWorkflowHelper.LoadSalesTransactionForReturn(this.Context, request.Cart));

            // If customer account number is not specified on the request it should not be overriden.
            if (request.Cart.CustomerId == null)
            {
                request.Cart.CustomerId = this.Context.GetSalesTransaction().CustomerId;
            }

            // Validate update cart request
            CartWorkflowHelper.ValidateUpdateCartRequest(this.Context, this.Context.GetSalesTransaction(), this.Context.GetReturnedSalesTransaction(), request.Cart, request.IsGiftCardOperation);

            request.Cart.IsReturnByReceipt = this.Context.GetReturnedSalesTransaction() != null;
            request.Cart.ReturnTransactionHasLoyaltyPayment = this.Context.GetReturnedSalesTransaction() != null && this.Context.GetReturnedSalesTransaction().HasLoyaltyPayment;
            
            if (this.Context.GetReturnedSalesTransaction() != null
                && !string.IsNullOrWhiteSpace(this.Context.GetReturnedSalesTransaction().LoyaltyCardId)
                && string.IsNullOrWhiteSpace(this.Context.GetSalesTransaction().LoyaltyCardId))
            {
                // Set the loyalty card of the returned transaction to the current transaction
                request.Cart.LoyaltyCardId = this.Context.GetReturnedSalesTransaction().LoyaltyCardId;
            }
            
            // Perform update cart operations
            CartWorkflowHelper.PerformSaveCartOperations(this.Context, request);

            // Sets the wharehouse id and invent location id for each line
            ItemAvailabilityHelper.SetSalesLineInventory(this.Context);

            // Calculate totals and saves the sales transaction
            CartWorkflowHelper.Calculate(this.Context, request.CalculationModes);

            // Validate return item and return transaction permissions
            CartWorkflowHelper.ValidateReturnPermission(this.Context, this.Context.GetSalesTransaction(), request.Cart.CartType);

            // Calculate the required reason codes after the price calculation
            ReasonCodesWorkflowHelper.CalculateRequiredReasonCodes(this.Context, ReasonCodeSourceType.None);

            CartWorkflowHelper.SaveSalesTransaction(this.Context, this.Context.GetSalesTransaction());

            return new SaveCartResponse(CartWorkflowHelper.ConvertToCart(this.Context, this.Context.GetSalesTransaction()));
        }
    }
}