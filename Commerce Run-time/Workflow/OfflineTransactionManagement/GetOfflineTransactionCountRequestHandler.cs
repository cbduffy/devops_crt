﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handles request to get number of offline transactions to upload to retail server.
    /// </summary>
    public sealed class GetOfflineTransactionCountRequestHandler : WorkflowRequestHandler<GetOfflineTransactionCountRequest, GetOfflineTransactionCountResponse>
    {
        /// <summary>
        /// Entry point of the workflow.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetOfflineTransactionCountResponse Process(GetOfflineTransactionCountRequest request)
        {
            ThrowIf.Null(request, "request");

            GetOfflineTransactionCountDataServiceRequest dataServiceRequest = new GetOfflineTransactionCountDataServiceRequest();
            var response = request.RequestContext.Runtime.Execute<GetOfflineTransactionCountDataServiceResponse>(dataServiceRequest, this.Context);

            return new GetOfflineTransactionCountResponse(response.OfflineTransactionCount);
        }
    }
}