﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handles request to read offline transactions.
    /// </summary>
    public sealed class GetOfflineTransactionsRequestHandler : WorkflowRequestHandler<GetOfflineTransactionsRequest, GetOfflineTransactionsResponse>
    {
        /// <summary>
        /// Entry point for the workflow.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetOfflineTransactionsResponse Process(GetOfflineTransactionsRequest request)
        {
            ThrowIf.Null(request, "request");

            GetOfflineTransactionsDataServiceRequest dataServiceRequest = new GetOfflineTransactionsDataServiceRequest(request.TransactionIds);
            var response = request.RequestContext.Runtime.Execute<GetOfflineTransactionsDataServiceResponse>(dataServiceRequest, this.Context);

            return new GetOfflineTransactionsResponse(response.CompressedTransactions);
        }
    }
}