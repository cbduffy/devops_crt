﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;

    /// <summary>
    /// Handles request to get top N transaction IDs from offline transaction database.
    /// </summary>
    public sealed class GetOfflineTransactionIdsRequestHandler : WorkflowRequestHandler<GetOfflineTransactionIdsRequest, GetOfflineTransactionIdsResponse>
    {
        /// <summary>
        /// Entry point of the workflow.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetOfflineTransactionIdsResponse Process(GetOfflineTransactionIdsRequest request)
        {
            ThrowIf.Null(request, "request");

            GetOfflineTransactionIdsDataServiceRequest dataServiceRequest = new GetOfflineTransactionIdsDataServiceRequest(request.NumberOfTransactions);
            var response = request.RequestContext.Runtime.Execute<GetOfflineTransactionIdsDataServiceResponse>(dataServiceRequest, this.Context);

            return new GetOfflineTransactionIdsResponse(response.TransactionIds);
        }
    }
}