﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;

    /// <summary>
    /// Encapsulates helper functions for getting shift data.
    /// </summary>
    internal static class ShiftDataDataServiceHelper
    {
        internal static IList<Shift> GetAllOpenedShiftsOnTerminal(RequestContext context, long channelId, string terminalId, bool includeSharedShifts)
        {
            ThrowIf.NullOrWhiteSpace(terminalId, "terminalId");

            ShiftDataQueryCriteria criteria = new ShiftDataQueryCriteria();
            criteria.ChannelId = channelId;
            criteria.TerminalId = terminalId;
            criteria.Status = (int)ShiftStatus.Open;
            criteria.SearchByCurrentTerminalId = true;
            criteria.IncludeSharedShifts = includeSharedShifts;

            GetShiftDataDataRequest dataServiceRequest = new GetShiftDataDataRequest(criteria, new QueryResultSettings());
            return context.Runtime.Execute<EntityDataServiceResponse<Shift>>(dataServiceRequest, context).EntityCollection;
        }

        internal static IList<Shift> GetOpenedShiftsOnTerminalForStaff(RequestContext context, long channelId, string staffId, string terminalId, bool includeSharedShifts)
        {
            ThrowIf.NullOrWhiteSpace(staffId, "staffId");
            ThrowIf.NullOrWhiteSpace(terminalId, "terminalId");

            ShiftDataQueryCriteria criteria = new ShiftDataQueryCriteria();
            criteria.ChannelId = channelId;
            criteria.StaffId = staffId;
            criteria.TerminalId = terminalId;
            criteria.Status = (int)ShiftStatus.Open;
            criteria.SearchByCurrentTerminalId = true;
            criteria.SearchByStaffId = true;
            criteria.IncludeSharedShifts = includeSharedShifts;

            GetShiftDataDataRequest dataServiceRequest = new GetShiftDataDataRequest(criteria, new QueryResultSettings());
            return context.Runtime.Execute<EntityDataServiceResponse<Shift>>(dataServiceRequest, context).EntityCollection;
        }

        internal static Shift GetActiveShift(RequestContext context, long channelId, string staffId, string terminalId, bool includeSharedShifts)
        {
            // ThrowIf.NullOrWhiteSpace(staffId, "staffId");
            ThrowIf.NullOrWhiteSpace(terminalId, "terminalId");

            ShiftDataQueryCriteria criteria = new ShiftDataQueryCriteria();
            criteria.ChannelId = channelId;
            criteria.TerminalId = terminalId;
            criteria.StaffId = staffId;
            criteria.Status = (int)ShiftStatus.Open;
            criteria.SearchByStaffId = true;
            criteria.SearchByCurrentStaffId = true;
            criteria.SearchByTerminalId = true;
            criteria.SearchByCurrentTerminalId = true;
            criteria.IncludeSharedShifts = includeSharedShifts;

            GetShiftDataDataRequest dataServiceRequest = new GetShiftDataDataRequest(criteria, new QueryResultSettings());
            return context.Runtime.Execute<EntityDataServiceResponse<Shift>>(dataServiceRequest, context).EntityCollection.FirstOrDefault();
        }

        internal static Shift GetShift(RequestContext context, long channelId, string terminalId, long shiftId, bool includeSharedShifts)
        {
            return GetShift(context, channelId, string.Empty, terminalId, shiftId, includeSharedShifts);
        }

        internal static Shift GetShift(RequestContext context, long channelId, string staffId, string terminalId, long shiftId, bool includeSharedShifts)
        {
            ThrowIf.NullOrWhiteSpace(terminalId, "terminalId");

            ShiftDataQueryCriteria criteria = new ShiftDataQueryCriteria();
            criteria.ChannelId = channelId;
            criteria.TerminalId = terminalId;
            criteria.ShiftId = shiftId;
            criteria.SearchByTerminalId = true;
            criteria.IncludeSharedShifts = includeSharedShifts;

            if (!string.IsNullOrEmpty(staffId))
            {
                criteria.StaffId = staffId;
                criteria.SearchByStaffId = true;
                criteria.SearchByCurrentStaffId = true;
            }

            GetShiftDataDataRequest dataServiceRequest = new GetShiftDataDataRequest(criteria, new QueryResultSettings());
            return context.Runtime.Execute<EntityDataServiceResponse<Shift>>(dataServiceRequest, context).EntityCollection.FirstOrDefault();
        }

        internal static IList<Shift> GetShiftsForStaffWithStatus(RequestContext context, long channelId, string staffId, ShiftStatus status, bool includeSharedShifts)
        {
            ThrowIf.NullOrWhiteSpace(staffId, "staffId");

            ShiftDataQueryCriteria criteria = new ShiftDataQueryCriteria();
            criteria.ChannelId = channelId;
            criteria.StaffId = staffId;
            criteria.Status = (int)status;
            criteria.SearchByStaffId = true;
            criteria.IncludeSharedShifts = includeSharedShifts;

            GetShiftDataDataRequest dataServiceRequest = new GetShiftDataDataRequest(criteria, new QueryResultSettings());
            return context.Runtime.Execute<EntityDataServiceResponse<Shift>>(dataServiceRequest, context).EntityCollection;
        }

        internal static IList<Shift> GetAllStoreShiftsWithStatus(RequestContext context, long channelId, ShiftStatus status, QueryResultSettings settings, bool includeSharedShifts)
        {
            ShiftDataQueryCriteria criteria = new ShiftDataQueryCriteria();
            criteria.ChannelId = channelId;
            criteria.Status = (int)status;
            criteria.IncludeSharedShifts = includeSharedShifts;

            GetShiftDataDataRequest dataServiceRequest = new GetShiftDataDataRequest(criteria, settings);
            return context.Runtime.Execute<EntityDataServiceResponse<Shift>>(dataServiceRequest, context).EntityCollection;
        }
    }
}