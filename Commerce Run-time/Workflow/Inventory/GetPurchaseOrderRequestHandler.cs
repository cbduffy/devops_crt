﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// The class to handle GetPurchaseOrderRequest.
    /// </summary>
    public sealed class GetPurchaseOrderRequestHandler : WorkflowRequestHandler<GetPurchaseOrderRequest, GetPurchaseOrderResponse>
    {
        /// <summary>
        /// Processes the GetPurchaseOrderRequest to return the purchase orders.
        /// </summary>
        /// <param name="request">The request parameter.</param>
        /// <returns>The GetPurchaseOrderResponse.</returns>
        protected override GetPurchaseOrderResponse Process(GetPurchaseOrderRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(DataModel.RetailOperation.PickingAndReceiving);
            var pickingReceivingServiceRequest = new GetPurchaseOrderServiceRequest(request.OrderId, request.OrderType);
            var pickingReceivingServiceResponse = this.Context.Execute<GetPurchaseOrderServiceResponse>(pickingReceivingServiceRequest);
            return new GetPurchaseOrderResponse(pickingReceivingServiceResponse.PurchaseOrders);
        }
    }
}
