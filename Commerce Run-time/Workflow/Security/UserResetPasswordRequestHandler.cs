﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Encapsulates the workflow required to do user authentication.
    /// </summary>
    public sealed class UserResetPasswordRequestHandler : WorkflowRequestHandler<UserResetPasswordRequest, NullResponse>
    {
        /// <summary>
        /// Executes the workflow to do user authentication.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override NullResponse Process(UserResetPasswordRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(DataModel.RetailOperation.ResetPassword);

            try
            {
                AuthenticationHelper.ResetPassword(this.Context, request);                
            }
            catch (Exception ex)
            {
                NetTracer.Error("Login failed exception with message {0} and stack trace {1}", ex.Message, ex.StackTrace);
                throw new UserAuthenticationException(SecurityErrors.ResetPasswordFailed);
            }

            return new NullResponse();
        }
    }
}