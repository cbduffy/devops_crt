﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow for validating a hardware station token.
    /// </summary>
    public sealed class ValidateHardwareStationTokenRequestHandler : WorkflowRequestHandler<ValidateHardwareStationTokenRequest, ValidateHardwareStationTokenResponse>
    {
        /// <summary>
        /// Executes the workflow for validating a hardware station token.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override ValidateHardwareStationTokenResponse Process(ValidateHardwareStationTokenRequest request)
        {
            ThrowIf.Null(request, "request");

            var serviceRequest = new ValidateHardwareStationTokenServiceRequest(request.DeviceNumber, request.HardwareStationToken);
            ValidateHardwareStationTokenServiceResponse serviceResponse = this.Context.Execute<ValidateHardwareStationTokenServiceResponse>(serviceRequest);

            return new ValidateHardwareStationTokenResponse(serviceResponse.Result);
        }
    }
}