﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Encapsulates the workflow required to activate the device.
    /// </summary>
    public sealed class DeviceActivationRequestHandler : WorkflowRequestHandler<DeviceActivationRequest, DeviceActivationResponse>
    {
        /// <summary>
        /// Executes the workflow to activate a device.
        /// </summary>
        /// <param name="request">The device activation request.</param>
        /// <returns>The device activation response.</returns>
        protected override DeviceActivationResponse Process(DeviceActivationRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(DataModel.RetailOperation.ActivateDevice);
            var serviceRequest = new ActivateDeviceServiceRequest(request.DeviceNumber, request.TerminalId, this.Context.GetPrincipal().UserId);
            IRequestHandler deviceManagementHandler = this.Context.Runtime.GetRequestHandler(serviceRequest.GetType(), ServiceTypes.DeviceManagementService);
            
            // Call device activation service.
            ActivateDeviceServiceResponse serviceResponse = this.Context.Execute<ActivateDeviceServiceResponse>(serviceRequest, deviceManagementHandler);
            var response = new DeviceActivationResponse(serviceResponse.DeviceActivationResult);

            return response;
        }
    }
}