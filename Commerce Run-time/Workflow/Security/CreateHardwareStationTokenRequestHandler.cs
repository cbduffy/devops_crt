﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow for creating a hardware station token.
    /// </summary>
    public sealed class CreateHardwareStationTokenRequestHandler : WorkflowRequestHandler<CreateHardwareStationTokenRequest, CreateHardwareStationTokenResponse>
    {
        /// <summary>
        /// Executes the workflow for creating a hardware station token.
        /// </summary>
        /// <param name="request">The create hardware station token request.</param>
        /// <returns>The create hardware station token response.</returns>
        protected override CreateHardwareStationTokenResponse Process(CreateHardwareStationTokenRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(DataModel.RetailOperation.PairHardwareStation);
            var serviceRequest = new CreateHardwareStationTokenServiceRequest(this.Context.GetPrincipal().DeviceId);
            CreateHardwareStationTokenServiceResponse serviceResponse = this.Context.Execute<CreateHardwareStationTokenServiceResponse>(serviceRequest);
            var response = new CreateHardwareStationTokenResponse(serviceResponse.Result);
            return response;
        }
    }
}