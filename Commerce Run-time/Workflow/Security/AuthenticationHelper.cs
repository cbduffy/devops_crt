﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Authentication logic helper.
    /// </summary>
    internal class AuthenticationHelper
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="AuthenticationHelper"/> class from being created.
        /// </summary>
        private AuthenticationHelper()
        {
        }

        /// <summary>
        /// Makes a TS call to do device authentication.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="deviceNumber">The device number.</param>
        /// <param name="deviceToken">The device token.</param>
        /// <returns>Authenticated device.</returns>
        internal static Device AuthenticateDevice(RequestContext context, string deviceNumber, string deviceToken)
        {
            var serviceRequest = new AuthenticateDeviceServiceRequest(deviceNumber, deviceToken);
            IRequestHandler deviceService = GetService(context, serviceRequest, ServiceTypes.DeviceManagementService);
            AuthenticateDeviceServiceResponse serviceResponse = context.Execute<AuthenticateDeviceServiceResponse>(serviceRequest, deviceService);
            return serviceResponse.Device;
        }

        /// <summary>
        /// Creates instance of the device by decrypting the token and verifying it against the passed device Number.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="deviceNumber">The device number.</param>
        /// <param name="deviceToken">The device token.</param>
        /// <returns>Authenticated device.</returns>
        internal static Device AuthenticateDevicePartial(RequestContext context, string deviceNumber, string deviceToken)
        {
            var serviceRequest = new AuthenticateDevicePartialServiceRequest(deviceNumber, deviceToken);
            IRequestHandler deviceService = GetService(context, serviceRequest, ServiceTypes.DeviceManagementService);
            AuthenticateDevicePartialServiceResponse serviceResponse = context.Execute<AuthenticateDevicePartialServiceResponse>(serviceRequest, deviceService);
            return serviceResponse.Device;
        }

        /// <summary>
        /// Makes a TS call to do user authentication.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="device">The device object.</param>
        /// <param name="request">UserAuthenticationRequest request object.</param>
        /// <param name="logOnConfiguration">Logon configuration.</param>
        /// <returns>Employee object.</returns>
        internal static Employee AuthenticateUser(RequestContext context, Device device, UserAuthenticationRequest request, out LogOnConfiguration logOnConfiguration)
        {
            var serviceRequest = new UserLogOnServiceRequest(device, request.LogOnType, request.RetailOperation, request.StaffId, request.Password, request.LogOnKey, request.ExtraData);
            IRequestHandler userService = GetUserService(context, serviceRequest, request.AuthenticationProvider);
            UserLogOnServiceResponse serviceResponse = context.Execute<UserLogOnServiceResponse>(serviceRequest, userService);
            LogTransaction(context, device, serviceResponse.Employee.StaffId, TransactionType.LogOn, request.TransactionId);
            logOnConfiguration = serviceResponse.LogOnConfiguration;
            return serviceResponse.Employee;
        }

        /// <summary>
        /// User authentication renewal.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="device">The device object.</param>
        /// <returns>Employee object.</returns>
        internal static Employee AuthenticateRenewalUser(RequestContext context, Device device)
        {
            var serviceRequest = new UserLogOnRenewalServiceRequest(device, context.GetPrincipal().UserId);
            IRequestHandler userService = GetUserService(context, serviceRequest, context.GetPrincipal().AuthenticationProvider);
            UserLogOnRenewalServiceResponse serviceResponse = context.Execute<UserLogOnRenewalServiceResponse>(serviceRequest, userService);
            return serviceResponse.Employee;
        }

        /// <summary>
        /// Performs a user logs off.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="request">The request object.</param>
        internal static void LogOff(RequestContext context, UserLogOffRequest request)
        {
            ThrowIf.Null(context, "context");
            ThrowIf.Null(request, "request");

            Device device = null;
            if (!string.IsNullOrWhiteSpace(request.DeviceId))
            {
                device = new Device 
                {
                    ChannelId = context.GetPrincipal().ChannelId,
                    TerminalRecordId = context.GetPrincipal().TerminalId,
                    DeviceNumber = context.GetPrincipal().DeviceId 
                };
            }

            var serviceRequest = new UserLogOffServiceRequest(device, request.StaffId, request.AuthenticationProvider, request.LogOnConfiguration);
            IRequestHandler userService = GetUserService(context, serviceRequest, request.AuthenticationProvider);
            context.Execute<Response>(serviceRequest, userService);

            // Log transaction in database.
            LogTransaction(context, device, request.StaffId, TransactionType.LogOff, request.TransactionId);
        }

        /// <summary>
        /// Performs unlock register.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="device">The device object.</param>
        /// <param name="request">The request object.</param>
        internal static void UnlockRegister(RequestContext context, Device device, UnlockRegisterRequest request)
        {
            var serviceRequest = new UnlockRegisterServiceRequest(device, request.LogOnType, request.RetailOperation, request.StaffId, request.Password, request.LogOnKey, request.ExtraData);
            IRequestHandler userService = GetUserService(context, serviceRequest, request.AuthenticationProvider);
            context.Execute<Response>(serviceRequest, userService);
        }

        /// <summary>
        /// Performs a user password reset.
        /// </summary>
        /// <param name="context">The request context.</param>               
        /// <param name="request">The request object.</param>        
        internal static void ResetPassword(RequestContext context, UserResetPasswordRequest request)
        {
            var serviceRequest = new UserResetPasswordServiceRequest(context.GetPrincipal().UserId, request.TargetUserId, request.NewPassword, request.AuthenticationProvider, request.LogOnConfiguration, request.ChangePassword);
            IRequestHandler userService = GetUserService(context, serviceRequest, request.AuthenticationProvider);
            context.Execute<Response>(serviceRequest, userService);

            LogTransaction(context, context.GetPrincipal().UserId, TransactionType.ResetPassword, request.TransactionId);
        }

        /// <summary>
        /// Performs a user password reset.
        /// </summary>
        /// <param name="context">The request context.</param>        
        /// <param name="request">The request object.</param>                       
        internal static void ChangePassword(RequestContext context, UserChangePasswordRequest request)
        {
            var serviceRequest = new UserChangePasswordServiceRequest(request.StaffId, request.AuthenticationProvider, request.OldPassword, request.NewPassword, request.LogOnConfiguration, request.ChangePassword);
            IRequestHandler userService = GetUserService(context, serviceRequest, request.AuthenticationProvider);
            context.Execute<Response>(serviceRequest, userService);

            LogTransaction(context, request.StaffId, TransactionType.ChangePassword, request.TransactionId);
        }

        private static void LogTransaction(RequestContext context, string staffId, TransactionType transactionType, string transactionId)
        {
            AuthenticationHelper.LogTransaction(context, null, staffId, transactionType, transactionId);
        }

        private static void LogTransaction(RequestContext context, Device device, string staffId, TransactionType transactionType, string transactionId)
        {
            var serviceRequest = new SaveTransactionLogRequest(transactionType, transactionId, device, staffId);
            IRequestHandler service = GetService(context, serviceRequest, ServiceTypes.TransactionLogService);
            context.Execute<Response>(serviceRequest, service);
        }

        private static IRequestHandler GetService(RequestContext context, Request request, string serviceType)
        {
            Type requestType = request.GetType();
            IRequestHandler service = context.Runtime.GetRequestHandler(requestType, serviceType);
            if (service == null)
            {
                throw new DataValidationException(DataValidationErrors.UnknownRequestResponsePair, "Handler for request type {0} not found.", requestType);
            }

            return service;
        }

        private static IRequestHandler GetUserService(RequestContext context, Request request, string authenticationProvider)
        {
            Type requestType = request.GetType();
            IRequestHandler userService = context.Runtime.GetRequestHandler(requestType, authenticationProvider);
            if (userService == null)
            {
                throw new DataValidationException(DataValidationErrors.UnknownRequestResponsePair, "Handler for request type {0} not found.", requestType);
            }

            return userService;
        }
    }
}