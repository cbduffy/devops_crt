﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow required to deactivate the device.
    /// </summary>
    public sealed class DeactivateDeviceRequestHandler : WorkflowRequestHandler<DeactivateDeviceRequest, NullResponse>
    {
        /// <summary>
        /// Executes the workflow to deactivate a device.
        /// </summary>
        /// <param name="request">The deactivate device request.</param>
        /// <returns>The response.</returns>
        protected override NullResponse Process(DeactivateDeviceRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.DeactivateDevice);
            GetCurrentTerminalIdDataRequest dataRequest = new GetCurrentTerminalIdDataRequest();
            string terminalId = this.Context.Execute<SingleEntityDataServiceResponse<string>>(dataRequest).Entity;

            // Slect all shifts.
            IList<Shift> shifts = ShiftDataDataServiceHelper.GetAllOpenedShiftsOnTerminal(this.Context, this.Context.GetPrincipal().ChannelId, terminalId, true);

            if (shifts.HasMultiple())
            {
                throw new DataValidationException(DataValidationErrors.TerminalHasAnOpenShift);
            }

            var serviceRequest = new DeactivateDeviceServiceRequest(this.Context.GetPrincipal().DeviceId, terminalId, this.Context.GetPrincipal().UserId, this.Context.GetPrincipal().DeviceToken);

            this.Context.Execute<DeactivateDeviceServiceResponse>(serviceRequest);

            // Log off the user.
            var userLogOffRequest = new UserLogOffRequest
            {
                StaffId = this.Context.GetPrincipal().UserId, 
                AuthenticationProvider = this.Context.GetPrincipal().AuthenticationProvider, 
                LogOnConfiguration = this.Context.GetPrincipal().LogOnConfiguration,
                TransactionId = request.TransactionId
            };

            AuthenticationHelper.LogOff(this.Context, userLogOffRequest);

            return new NullResponse();
        }
    }
}