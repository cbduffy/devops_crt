﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handles AuthenticateDeviceRequest.
    /// </summary>
    public class AuthenticateDevicePartialRequestHandler : WorkflowRequestHandler<AuthenticateDevicePartialRequest, AuthenticateDevicePartialResponse>
    {
        /// <summary>
        /// Executes the workflow to authenticate the device.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override AuthenticateDevicePartialResponse Process(AuthenticateDevicePartialRequest request)
        {
            ThrowIf.Null(request, "request");

            Device device = AuthenticationHelper.AuthenticateDevicePartial(this.Context, request.DeviceId, request.DeviceToken);
            return new AuthenticateDevicePartialResponse(device);
        }
    }
}
