﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to do user authentication.
    /// </summary>
    public sealed class UserLogOffRequestHandler : WorkflowRequestHandler<UserLogOffRequest, NullResponse>
    {
        /// <summary>
        /// Executes the workflow to do user log off.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override NullResponse Process(UserLogOffRequest request)
        {
            ThrowIf.Null(request, "request");

            CommercePrincipal principal = this.Context.GetPrincipal();
            request.DeviceId = principal.DeviceId;
            request.DeviceToken = principal.DeviceToken;
            request.AuthenticationProvider = principal.AuthenticationProvider;
            request.LogOnConfiguration = principal.LogOnConfiguration;
            request.StaffId = principal.UserId;

            // User logs off.
            AuthenticationHelper.LogOff(this.Context, request);

            return new NullResponse();
        }
    }
}