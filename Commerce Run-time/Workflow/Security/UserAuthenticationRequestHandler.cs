﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Encapsulates the workflow required to do user authentication.
    /// </summary>
    public sealed class UserAuthenticationRequestHandler : WorkflowRequestHandler<UserAuthenticationRequest, UserAuthenticationResponse>
    {
        /// <summary>
        /// Executes the workflow to do user authentication.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override UserAuthenticationResponse Process(UserAuthenticationRequest request)
        {
            const string ErrorMessage = "An error occurred during logon.";

            ThrowIf.Null(request, "request");
            Device device = null;
            bool successfulLogOn = false;
            Employee employee;
            CommerceIdentity identity;
            LogOnConfiguration logOnConfiguration = LogOnConfiguration.LocalDatabase;

            try
            {
                // Authenticate device only when DeviceId is specified
                if (request.DeviceId != null && request.DeviceToken != null)
                {
                    device = AuthenticationHelper.AuthenticateDevice(
                        this.Context,
                        request.DeviceId,
                        request.DeviceToken);

                    // User logs on.
                    employee = AuthenticationHelper.AuthenticateUser(this.Context, device, request, out logOnConfiguration);
                    identity = new CommerceIdentity(employee, device);
                }
                else
                {
                    employee = AuthenticationHelper.AuthenticateUser(this.Context, null, request, out logOnConfiguration);
                    identity = new CommerceIdentity(employee, null);
                }

                successfulLogOn = true;

                // Add the password and logon key to the claim.
                identity.UserPassword = request.Password;
                identity.LogonKey = request.LogOnKey;

                // Add the authentication provider to the claim.
                identity.AuthenticationProvider = request.AuthenticationProvider;

                // Add the LogOn Configuration to the claim.
                identity.LogOnConfiguration = logOnConfiguration;

                // If the request is for elevate operation
                if (request.RetailOperation != RetailOperation.None)
                {
                    // Add the Elevation properties to the claim.
                    identity.OriginalUserId = this.Context.GetPrincipal().UserId;
                    identity.ElevatedRetailOperation = request.RetailOperation;

                    // successful manager override for operation with id and operator with id
                    var message = string.Format(
                        "Manager with id '{0}' has approved override for operation with id '{1}' to the operator with id '{2}'.",
                        employee.StaffId,
                        identity.ElevatedRetailOperation,
                        identity.OriginalUserId);
                    LogAuditEntry(request.RequestContext, "ElevateUser", message);
                }
                else
                {
                    var message = string.Format(
                        "User has successfully logged in. OperatorID: {0}",
                        employee.StaffId);
                    LogAuditEntry(request.RequestContext, "UserAuthenticationRequestHandler.Process", message);
                }

                return new UserAuthenticationResponse(employee, device, identity);
            }
            catch (DeviceAuthenticationException ex)
            {
                var message = string.Format(
                    "User '{0}' logon request (type - '{1}') on device '{2}' using '{3}' threw an exception: {4}",
                    request.StaffId,
                    request.LogOnType,
                    request.DeviceId,
                    logOnConfiguration,
                    ex);
                NetTracer.Error(message);
                LogAuditEntry(request.RequestContext, "UserAuthenticationRequestHandler.Process", message, AuditLogTraceLevel.Error);

                throw;
            }
            catch (UserAuthenticationException e)
            {
                var message = string.Format(
                    "User '{0}' logon request on device '{1}' using '{2}' threw an exception: {3}",
                    request.StaffId,
                    request.DeviceId,
                    logOnConfiguration,
                    e);
                NetTracer.Error(message);
                LogAuditEntry(request.RequestContext, "UserAuthenticationRequestHandler.Process", message, AuditLogTraceLevel.Error);

                // For User elevation throw UserAuthorization error
                if (request.RetailOperation != RetailOperation.None)
                {
                    throw new UserAuthorizationException(SecurityErrors.AuthenticationFailed, ErrorMessage, e);
                }
                else
                {
                    string errorResourceId = !string.IsNullOrEmpty(e.ErrorResourceId)
                        ? e.ErrorResourceId
                        : SecurityErrors.AuthenticationFailed;

                    throw new UserAuthenticationException(errorResourceId, ErrorMessage, e);
                }
            }
            catch (StorageException ex)
            {
                var message = string.Format(
                    "User '{0}' logon request (type - '{1}') on device '{2}' using '{3}' threw an exception: {4}",
                    request.StaffId,
                    request.LogOnType,
                    request.DeviceId,
                    logOnConfiguration,
                    ex);
                NetTracer.Error(message);
                LogAuditEntry(request.RequestContext, "UserAuthenticationRequestHandler.Process", message, AuditLogTraceLevel.Error);

                ////In this case the RS is not able to connect to the channel database and hence we need to send this exception back to the client. For other storage exceptions we do not send them to the client.
                if (request.DeviceId == null)
                {
                    throw new UserAuthenticationException(SecurityErrors.ChannelDatabaseConnectionFailed, ErrorMessage, ex);
                }

                // If logon with device, rethrow the same storage exception if caused by database connection error.
                // This is required by MPOS-pro device online/offline database seamless switch scenario.
                if (request.DeviceId != null && ex.CausedByConnectionError)
                {
                    throw;
                }

                throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed, ErrorMessage, ex);
            }
            catch (CommunicationException ex)
            {
                var message = string.Format(
                    "User '{0}' logon request (type - '{1}') on device '{2}' using '{3}' threw an exception: {4}. The Retail Server was not able to connect to transaction service.",
                    request.StaffId,
                    request.LogOnType,
                    request.DeviceId,
                    logOnConfiguration,
                    ex);
                NetTracer.Error(message);
                LogAuditEntry(request.RequestContext, "UserAuthenticationRequestHandler.Process", message, AuditLogTraceLevel.Error);

                throw new UserAuthenticationException(SecurityErrors.RealTimeServiceConnectionFailed, ErrorMessage, ex);
            }
            catch (HeadquarterTransactionServiceException ex)
            {
                var message = string.Format(
                    "User '{0}' logon request (type - '{1}') on device '{2}' using '{3}' threw an exception: {4}",
                    request.StaffId,
                    request.LogOnType,
                    request.DeviceId,
                    logOnConfiguration,
                    ex);
                NetTracer.Error(message);
                LogAuditEntry(request.RequestContext, "UserAuthenticationRequestHandler.Process", message, AuditLogTraceLevel.Error);

                throw new UserAuthenticationException(SecurityErrors.HeadquarterTransactionServiceMethodCallFailure, ex.Message, ex);
            }
            catch (Exception ex)
            {
                var message = string.Format(
                    "User '{0}' logon request (type - '{1}') on device '{2}' using '{3}' threw an exception: {4}",
                    request.StaffId,
                    request.LogOnType,
                    request.DeviceId,
                    logOnConfiguration,
                    ex);
                NetTracer.Error(message);
                LogAuditEntry(request.RequestContext, "UserAuthenticationRequestHandler.Process", message, AuditLogTraceLevel.Error);

                // For User elevation throw UserAuthorization error
                if (request.RetailOperation != RetailOperation.None)
                {
                    throw new UserAuthorizationException(SecurityErrors.AuthenticationFailed, ErrorMessage, ex);
                }
                else
                {
                    throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed, ErrorMessage, ex);
                }
            }
            finally
            {
                if (!successfulLogOn)
                {
                    var message = string.Format("Failed user login attempt. OperatorID: {0}", request.StaffId);
                    LogAuditEntry(request.RequestContext, "UserAuthenticationRequestHandler.Process", message, AuditLogTraceLevel.Error);
                }
            }
        }

        /// <summary>
        /// Writes an entry into the audit table.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="source">The log source.</param>
        /// <param name="value">The log entry.</param>
        /// <param name="logTraceLevel">The log trace level.</param>
        private static void LogAuditEntry(RequestContext context, string source, string value, AuditLogTraceLevel logTraceLevel = AuditLogTraceLevel.Trace)
        {
            var auditLogDataRequest = new InsertAuditLogServiceRequest(source, value, logTraceLevel, unchecked((int)context.RequestTimer.ElapsedMilliseconds));
            context.Execute<NullResponse>(auditLogDataRequest);
        }
    }
}