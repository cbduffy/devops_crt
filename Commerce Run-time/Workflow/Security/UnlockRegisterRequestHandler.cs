﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Globalization;
    using System.Security.Principal;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow.Composition;
    using Microsoft.Dynamics.Retail.Diagnostics;
  
    /// <summary>
    /// Encapsulates the workflow required to do unlock a register.
    /// </summary>
    public sealed class UnlockRegisterRequestRequestHandler : WorkflowRequestHandler<UnlockRegisterRequest, NullResponse>
    {
        /// <summary>
        /// Executes the workflow to unlock a register.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override NullResponse Process(UnlockRegisterRequest request)
        {
            ThrowIf.Null(request, "request");
            Device device = null;

            CommercePrincipal principal = this.Context.GetPrincipal();
            string userId = principal.UserId;

            try
            {
                if (userId != request.StaffId)
                {
                    throw new UserAuthenticationException(SecurityErrors.UnlockRegisterFailed);
                }

                if (request.DeviceId != null && request.DeviceToken != null)
                {
                    // Authenticate device only when DeviceId is specified
                    device = AuthenticationHelper.AuthenticateDevice(
                        this.Context,
                        request.DeviceId,
                        request.DeviceToken);
                }
                
                // Unlock the terminal
                AuthenticationHelper.UnlockRegister(this.Context, device, request);
                return new NullResponse();
            }
            catch (DeviceAuthenticationException ex)
            {
                NetTracer.Error(
                   "User '{0}' unlock register request (type - '{1}') on device '{2}' threw an exception: {3}",
                   request.StaffId,
                   request.LogOnType,
                   request.DeviceId,
                   ex);

                throw;
            }
            catch (UserAuthenticationException ex)
            {
                NetTracer.Error(
                   "User '{0}' unlock register request (type - '{1}') on device '{2}' threw an exception: {3}",
                   request.StaffId,
                   request.LogOnType,
                   request.DeviceId,
                   ex);

                throw new UserAuthenticationException(SecurityErrors.UnlockRegisterFailed);
            }
            catch (Exception ex)
            {
                NetTracer.Error(
                   "User '{0}' unlock register request (type - '{1}') on device '{2}' threw an exception: {3}",
                   request.StaffId,
                   request.LogOnType,
                   request.DeviceId,
                   ex);

                throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
            }
        }
    }
}