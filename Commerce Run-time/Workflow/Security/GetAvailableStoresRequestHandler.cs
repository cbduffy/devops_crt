﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow.Composition;
    
    /// <summary>
    /// Encapsulates the workflow required to get available stores for device.
    /// </summary>
    public sealed class GetAvailableStoresRequestHandler : WorkflowRequestHandler<GetAvailableStoresRequest, GetAvailableStoresResponse>
    {
        /// <summary>
        /// Executes the workflow to get available stores.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetAvailableStoresResponse Process(GetAvailableStoresRequest request)
        {
            ThrowIf.Null(request, "request");

            // Validate device token
            AuthenticationHelper.AuthenticateDevice(this.Context, request.DeviceId, request.DeviceToken);

            ChannelDataManager manager = new ChannelDataManager(this.Context);
            var stores = manager.GetStores(new QueryResultSettings());

            var response = new GetAvailableStoresResponse(stores);
            return response;
        }
    }
}