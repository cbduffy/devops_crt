﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow.Composition;

    /// <summary>
    /// Encapsulates the workflow required to perform check access.
    /// </summary>
    public sealed class CheckAccessRequestHandler : WorkflowRequestHandler<CheckAccessRequest, NullResponse>
    {
        /// <summary>
        /// Executes the workflow to perform Check Access.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override NullResponse Process(CheckAccessRequest request)
        {
            ThrowIf.Null(request, "request");

            var serviceRequest = new CheckAccessServiceRequest(request.CommercePrincipal, request.AllowedRoles, request.RetailOperation, request.DeviceTokenRequired);
            return this.Context.Execute<NullResponse>(serviceRequest);
        }
    }
}