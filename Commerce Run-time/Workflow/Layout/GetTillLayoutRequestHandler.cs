﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Request handler to get the screen layout.
    /// </summary>
    public sealed class GetTillLayoutRequestHandler : WorkflowRequestHandler<GetTillLayoutRequest, GetTillLayoutResponse>
    {
        /// <summary>
        /// Gets the screen layout.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        /// <exception cref="Microsoft.Dynamics.Commerce.Runtime.ConfigurationException">Required Service missing.</exception>
        protected override GetTillLayoutResponse Process(GetTillLayoutRequest request)
        {
            ThrowIf.Null(request, "request");
           
            var dataServiceRequest = new GetTillLayoutDataServiceRequest();
            var dataServiceResponse = request.RequestContext.Runtime.Execute<SingleEntityDataServiceResponse<TillLayout>>(dataServiceRequest, this.Context);
            return new GetTillLayoutResponse(dataServiceResponse.Entity);
        }
    }
}
