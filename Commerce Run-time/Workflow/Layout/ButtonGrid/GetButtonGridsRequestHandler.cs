﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow required to fetch button grids.
    /// </summary>
    public sealed class GetButtonGridsRequestHandler : WorkflowRequestHandler<GetButtonGridsRequest, GetButtonGridsResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch button grids.
        /// </summary>
        /// <param name="request">Request instance.</param>
        /// <returns>The response.</returns>
        protected override GetButtonGridsResponse Process(GetButtonGridsRequest request)
        {
            ThrowIf.Null(request, "request");

            var serviceRequest = new GetButtonGridsServiceRequest(
                request.ButtonGridQueryResultSettings);
            var serviceResponse = this.Context.Execute<GetButtonGridsServiceResponse>(serviceRequest);
            return new GetButtonGridsResponse(serviceResponse.ButtonGrids);
        }
    }
}