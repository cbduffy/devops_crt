﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulates the workflow required to fetch button grids by identifiers.
    /// </summary>
    public sealed class GetButtonGridsByIdsRequestHandler : WorkflowRequestHandler<GetButtonGridsByIdsRequest, GetButtonGridsByIdsResponse>
    {
        /// <summary>
        /// Executes the workflow to fetch a collection of button grids by their identifiers.
        /// </summary>
        /// <param name="request">Request instance.</param>
        /// <returns>The response.</returns>
        protected override GetButtonGridsByIdsResponse Process(GetButtonGridsByIdsRequest request)
        {
            ThrowIf.Null(request, "request");

            var serviceRequest = new GetButtonGridsByIdsServiceRequest(
                request.ButtonGridIds);
            var serviceResponse = this.Context.Execute<GetButtonGridsByIdsServiceResponse>(serviceRequest);
            return new GetButtonGridsByIdsResponse(serviceResponse.ButtonGrids);
        }
    }
}