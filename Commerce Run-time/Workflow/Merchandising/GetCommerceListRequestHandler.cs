﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Gets the commerce lists specified by the request.
    /// </summary>
    public sealed class GetCommerceListRequestHandler : WorkflowRequestHandler<GetCommerceListRequest, GetCommerceListResponse>
    {
        /// <summary>
        /// Gets the commerce lists specified by the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns><see cref="GetCommerceListResponse"/>Object containing the commerce lists.</returns>
        protected override GetCommerceListResponse Process(GetCommerceListRequest request)
        {
            ThrowIf.Null(request, "request");

            this.Context.GetCheckAccessRetailOperationAction()(RetailOperation.CustomerSearch);

            if (string.IsNullOrEmpty(request.CustomerAccountNumber))
            {
                throw new NotSupportedException("Requests must specify a customer.");
            }

            // Form a request and get the response
            GetCommerceListServiceRequest serviceRequest = new GetCommerceListServiceRequest
            {
                Id = request.Id,
                CustomerAccountNumber = request.CustomerAccountNumber,
                FavoriteFilter = request.FavoriteFilter,
                PublicFilter = request.PublicFilter
            };

            GetCommerceListServiceResponse serviceResponse = this.Context.Execute<GetCommerceListServiceResponse>(serviceRequest);
            return new GetCommerceListResponse(serviceResponse.Clists);            
        }      
    }
}