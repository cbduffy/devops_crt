﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Encapsulates the workflow required to retrieve an item.
    /// </summary>
    /// <remarks>
    /// If both ItemId and RecordId have been specified, ItemId takes precedence.
    /// </remarks>
    public sealed class GetItemByIdRequestHandler : WorkflowRequestHandler<GetItemByIdRequest, GetItemByIdResponse>
    {
        /// <summary>
        /// Executes the workflow to retrieve an item.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetItemByIdResponse Process(GetItemByIdRequest request)
        {
            ThrowIf.Null(request, "request");

            ItemDataManager manager = new ItemDataManager(this.Context);

            List<Item> items = new List<Item>();
            if (request.ItemIds.Any())
            {
                var getItemsRequest = new GetItemsDataRequest(request.ItemIds);
                
                getItemsRequest.QueryResultSettings = new QueryResultSettings(request.ColumnSet);
                var getItemsResponse = request.RequestContext.Runtime.Execute<GetItemsDataResponse>(getItemsRequest, request.RequestContext);

                items.AddRange(getItemsResponse.Items);
            }

            if (request.ProductIds.Any())
            {
                items.AddRange(manager.GetItems(request.ProductIds, request.ColumnSet));
            }

            var response = new GetItemByIdResponse(items);
            return response;
        }
    }
}