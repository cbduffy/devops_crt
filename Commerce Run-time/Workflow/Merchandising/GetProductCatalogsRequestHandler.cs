﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Retrieves the collection of product catalogs.
    /// </summary>
    public sealed class GetProductCatalogsRequestHandler : WorkflowRequestHandler<GetProductCatalogsRequest, GetProductCatalogsResponse>
    {
        /// <summary>
        /// Executes the workflow to retrieve product catalogs.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetProductCatalogsResponse Process(GetProductCatalogsRequest request)
        {
            ThrowIf.Null(request, "request");

            if (request.QueryResultSettings == null)
            {
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "Criteria must be specified");
            }

            // get product catalogs
            ProductDataManager manager = new ProductDataManager(this.Context);
            CatalogSearchCriteria criteria = new CatalogSearchCriteria(request.ChannelId, request.ActiveOnly);

            return new GetProductCatalogsResponse(manager.GetProductCatalogs(criteria, request.QueryResultSettings));
        }
    }
}
