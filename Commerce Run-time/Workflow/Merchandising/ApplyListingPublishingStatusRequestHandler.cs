﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Updates the publishing status for the specified listings.
    /// </summary>
    public sealed class UpdateListingPublishingStatusRequestHandler : WorkflowRequestHandler<UpdateListingPublishingStatusRequest, NullResponse>
    {
        /// <summary>
        /// Processes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override NullResponse Process(UpdateListingPublishingStatusRequest request)
        {
            ThrowIf.Null(request, "request");

            if (request.StatusList == null)
            {
                throw new InvalidOperationException("The status list must be specified.");
            }

            ProductDataManager manager = new ProductDataManager(this.Context);
            manager.UpdateListingStatuses(request.StatusList);

            return new NullResponse();
        }
    }
}