﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services;

    /// <summary>
    /// Workflow class helps to retrieve the CardType details.
    /// </summary>
    public class GetCardTypeRequestHandler : WorkflowRequestHandler<GetCardTypesRequest, GetCardTypesResponse>
    {
        /// <summary>
        /// CardType workflow handler to process the incoming workflow requests.
        /// </summary>
        /// <param name="request">GetCardTypesRequest request object.</param>
        /// <returns>Returns GetCardTypesResponse response object.</returns>
        protected override GetCardTypesResponse Process(GetCardTypesRequest request)
        {
            ThrowIf.Null(request, "request");

            IEnumerable<CardTypeInfo> cardTypeData = GetCardTypes(request.RequestContext, request.ChannelId);

            IEnumerable<CardTypeInfo> cardTypes;

            if (!string.IsNullOrWhiteSpace(request.CardNumber))
            {
                cardTypes = FindMatchingCardTypes(cardTypeData, request.CardNumber);
            }
            else
            {
                cardTypes = cardTypeData;
            }

            return new GetCardTypesResponse(cardTypes);
        }

        private static IEnumerable<CardTypeInfo> GetCardTypes(RequestContext context, long channelId)
        {
            var cardTypeDataRequest = new GetCardTypeDataRequest(channelId);
            var response = context.Runtime.Execute<EntityDataServiceResponse<CardTypeInfo>>(cardTypeDataRequest, context);

            return response.EntityCollection;
        }

        private static IEnumerable<CardTypeInfo> FindMatchingCardTypes(IEnumerable<CardTypeInfo> cardTypes, string cardNumber)
        {
            ThrowIf.Null(cardNumber, "cardNumber");
            ThrowIf.Null(cardTypes, "cardTypes");

            cardNumber = cardNumber.Replace(" ", string.Empty);
            IDictionary<string, CardTypeInfo> matchingCardTypes = new Dictionary<string, CardTypeInfo>();
            foreach (CardTypeInfo cardType in cardTypes)
            {
                if (matchingCardTypes.ContainsKey(cardType.TypeId))
                {
                    // Due to the nature of sql join CARDTYPESVIEW has multiple rows for each card type (one per each number mask).
                    // To not return duplicated card type matches we only add card type with same identifiers once.
                    continue;
                }

                int numberStartsFrom, numberEndWith;
                if (!int.TryParse(cardType.NumberFrom, out numberStartsFrom)
                    || !int.TryParse(cardType.NumberTo, out numberEndWith))
                {
                    throw new DataValidationException(DataValidationErrors.ValueOutOfRange, "Number masks for card type '{0}' are not valid.", cardType.TypeId);
                }

                int lengthToCompare = Math.Max(cardType.NumberFrom.Length, cardType.NumberTo.Length);

                if (cardNumber.Length < lengthToCompare)
                {
                    // Card number provided is shorter than defined mask.
                    continue;
                }

                int cardNumberToCompare;
                if (!int.TryParse(cardNumber.Substring(0, lengthToCompare), out cardNumberToCompare))
                {
                    throw new DataValidationException(DataValidationErrors.ValueOutOfRange, "Format of specified card number is not valid.");
                }
                
                if ((cardNumberToCompare >= numberStartsFrom) && (cardNumberToCompare <= numberEndWith))
                {   
                    matchingCardTypes.Add(cardType.TypeId, cardType);
                }
            }

            return matchingCardTypes.Values;
        }
    }
}
