﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;

    /// <summary>
    /// Handles request to verify products existence.
    /// </summary>
    public class VerifyProductExistenceRequestHandler : WorkflowRequestHandler<VerifyProductExistenceRequest, VerifyProductExistenceResponse>
    {
        /// <summary>
        /// Executes the workflow to retrieve changed products.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override VerifyProductExistenceResponse Process(VerifyProductExistenceRequest request)
        {
            ProductDataManager manager = new ProductDataManager(this.Context);
            ReadOnlyCollection<ProductExistenceId> ids = manager.VerifyProductExistence(request.Criteria);

            VerifyProductExistenceResponse response = new VerifyProductExistenceResponse(ids);
            return response;
        }
    }
}