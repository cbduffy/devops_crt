﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow.Composition;    

    /// <summary>
    /// Retrieves all listing publish statuses matching the specified listing identifiers.
    /// </summary>
    public sealed class GetListingPublishStatusesRequestHandler : WorkflowRequestHandler<GetListingPublishStatusesRequest, GetListingPublishStatusesResponse>
    {
        /// <summary>
        /// Retrieves specified listings.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetListingPublishStatusesResponse Process(GetListingPublishStatusesRequest request)
        {
            ThrowIf.Null(request, "request");

            ProductDataManager manager = new ProductDataManager(this.Context);
            IEnumerable<ListingPublishStatus> matches = manager.GetListingPublishStatuses(request.ListingIds, request.ColumnSet);

            return new GetListingPublishStatusesResponse(matches);
        }
    }
}