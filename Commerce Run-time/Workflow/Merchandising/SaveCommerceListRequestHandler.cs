﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Saves a commerce list.
    /// </summary>
    public sealed class SaveCommerceListRequestHandler : WorkflowRequestHandler<SaveCommerceListRequest, SaveCommerceListResponse>
    {
        /// <summary>
        /// Saves the commerce list on the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns><see cref="SaveCommerceListResponse"/>Object containing the saved commerce list.</returns>
        protected override SaveCommerceListResponse Process(SaveCommerceListRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.CommerceList, "request.CommerceList");

            // Form a request and get the response
            var serviceRequest = new SaveCommerceListServiceRequest()
            {
                CommerceList = request.CommerceList,
                OperationType = request.OperationType
            };

            SaveCommerceListServiceResponse serviceResponse = this.Context.Execute<SaveCommerceListServiceResponse>(serviceRequest);
            return new SaveCommerceListResponse(serviceResponse.CommerceList);
        }
    }
}