﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Workflow
{
    using System;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Workflow class helps to retrieve the Barcode details.
    /// </summary>
    public class GetBarcodeRequestHandler : WorkflowRequestHandler<GetBarcodeRequest, GetBarcodeResponse>
    {
        /// <summary>
        /// Barcode workflow handler to process the incoming workflow requests.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        protected override GetBarcodeResponse Process(GetBarcodeRequest request)
        {
            ThrowIf.Null(request, "request");
            ThrowIf.Null(request.ScanInfo, "request.ScanInfo");
            ThrowIf.NullOrWhiteSpace(request.ScanInfo.ScanDataLabel, "request.ScanInfo.ScanDataLabel");
            
            string barcodeId = request.ScanInfo.ScanDataLabel;

            var barcodeInfo = new Barcode
            {
                BarcodeId = barcodeId,
                ItemBarcode = barcodeId,
                TimeStarted = DateTime.UtcNow,
                EntryMethodType = request.ScanInfo.EntryMethodType
            };

            // Barcode Workflow response.
            GetBarcodeResponse response;
            
            // Find whether the Items is present in the Item Barcode lookup table, if it is present return the Item response.
            if (this.FindItemByBarCode(barcodeInfo, out response))
            {
                return response;
            }

            // First, find the Barcode internal type from the Mask (using Prefix), the barcode prefix should match the Mask prefix set in AX.
            var barcodeMask = GetBarcodeTypeFromMask(this.Context, barcodeId);

            // The barcodeMask now contains the internal type { for instance: Item, Customer, Employee, DiscountCode, LoyaltyCard, GiftCard, Salesperson etc}
            // Now, get Barcode details from the barcode mask segments.
            response = this.GetBarcodeResponse(request, barcodeMask) ?? new GetBarcodeResponse();

            return response;
        }

        private static BarcodeMask GetBarcodeTypeFromMask(RequestContext context, string barcodeId)
        {
            var serviceRequest = new GetBarcodeTypeServiceRequest(barcodeId);

            var serviceResponse = context.Execute<GetBarcodeTypeServiceResponse>(serviceRequest);

            return serviceResponse.BarcodeMask;
        }

        private static ProductPrice GetProductPrice(string itemId, string inventoryDimensionId, decimal basePrice, decimal tradeAgreementPrice, decimal adjustedPrice, string currencyCode)
        {
            ProductPrice productPrice = new ProductPrice();

            productPrice.ItemId = itemId;
            productPrice.InventoryDimensionId = inventoryDimensionId;
            productPrice.BasePrice = basePrice;
            productPrice.TradeAgreementPrice = tradeAgreementPrice;
            productPrice.AdjustedPrice = adjustedPrice;
            productPrice.CurrencyCode = currencyCode;

            return productPrice;
        }
        
        private GetBarcodeResponse GetBarcodeResponse(GetBarcodeRequest request, BarcodeMask barcodeMask)
        {
            GetBarcodeResponse response = null;
            
            // If given Barcode prefix matches with configured in AX, then barcode should fall under anyone of Internal types defined.
            if (barcodeMask != null)
            {
                switch (barcodeMask.MaskType)
                {
                    case BarcodeMaskType.Item:
                    case BarcodeMaskType.Customer:
                    case BarcodeMaskType.DataEntry:
                    case BarcodeMaskType.Employee:
                    case BarcodeMaskType.Salesperson:
                    case BarcodeMaskType.DiscountCode:
                    case BarcodeMaskType.GiftCard:
                    case BarcodeMaskType.LoyaltyCard:
                        {
                            // Get the Barcode details by processing the masked segments.
                            var serviceRequest = new ProcessMaskSegmentsServiceRequest(request.ScanInfo.ScanDataLabel, barcodeMask);

                            var serviceResponse = this.Context.Execute<ProcessMaskSegmentsServiceResponse>(serviceRequest);

                            response = new GetBarcodeResponse(serviceResponse.Barcode);

                            break;
                        }

                    case BarcodeMaskType.Coupon:
                        break;

                    default:
                        throw new DataValidationException(DataValidationErrors.UnsupportedType, "Process Barcode: Unsupported barcode type {0}", barcodeMask.MaskType);
                }
            }

            return response;
        }

        private bool FindItemByBarCode(Barcode barcode, out GetBarcodeResponse process)
        {
            process = null;

            GetProductBarcodeDataRequest productBarcodeRequest = new GetProductBarcodeDataRequest(barcode.ItemBarcode);
            GetProductBarcodeDataResponse productBarcodeResponse = this.Context.Execute<GetProductBarcodeDataResponse>(productBarcodeRequest);
            
            ItemBarcode itemBarcode = productBarcodeResponse.Barcode;
            if (itemBarcode != null)
            {
                barcode.MaskType = BarcodeMaskType.Item;
                barcode.Convert(itemBarcode);

                // Recalculate quantity if the barcode price is embedded.
                if (barcode.BarcodePrice > 0)
                {
                    barcode.BarcodeQuantity = this.BackCalculateQuantityFromPrice(barcode);
                }

                process = new GetBarcodeResponse(barcode);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Invoke the calculate quantity method in barcode service.
        /// </summary>
        /// <param name="barcode">Barcode object.</param>
        /// <returns>Returns the barcode quantity calculated.</returns>
        private decimal BackCalculateQuantityFromPrice(Barcode barcode)
        {
            var defaultProductPrice = this.GetItemPrice(barcode.ItemId, barcode.InventoryDimensionId, barcode.UnitId);

            var serviceRequest = new CalculateQuantityFromPriceServiceRequest(barcode.BarcodePrice, defaultProductPrice.AdjustedPrice, barcode.UnitId);
            var serviceResponse = this.Context.Execute<CalculateQuantityFromPriceServiceResponse>(serviceRequest);

            return serviceResponse.BarcodeQuantity;
        }

        private ProductPrice GetItemPrice(string itemId, string inventoryDimensionId, string unitOfMeasureSymbol)
        {
            SalesTransaction salesTransaction = new SalesTransaction()
            {
                Id = Guid.NewGuid().ToString(),
                CustomerId = string.Empty,
            };

            SalesLine salesLine = new SalesLine()
            {
                ItemId = itemId,
                InventoryDimensionId = inventoryDimensionId,
                SalesOrderUnitOfMeasure = unitOfMeasureSymbol,
                Quantity = 1m,
                LineId = Guid.NewGuid().ToString()
            };

            salesTransaction.SalesLines.Add(salesLine);

            GetIndependentPriceDiscountServiceRequest priceRequest = new GetIndependentPriceDiscountServiceRequest(salesTransaction);

            GetPriceServiceResponse pricingServiceResponse = this.Context.Execute<GetPriceServiceResponse>(priceRequest);

            SalesLine resultLine = pricingServiceResponse.Transaction.SalesLines[0];

            ProductPrice productPrice = GetProductPrice(
                resultLine.ItemId,
                resultLine.InventoryDimensionId,
                resultLine.BasePrice,
                resultLine.AgreementPrice,
                resultLine.AdjustedPrice,
                this.Context.GetChannelConfiguration().Currency);

            return productPrice;
        }
    }
}
