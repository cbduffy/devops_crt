﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.Common
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Channel data services that contains methods to retrieve the information by calling views.
    /// </summary>
    public class ChannelDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new Type[]
                {
                    typeof(GetDeviceConfigurationDataServiceRequest),
                    typeof(GetChannelConfigurationDataServiceRequest),
                    typeof(GetStoreDataServiceRequest),
                    typeof(GetChannelTenderTypesDataRequest),
                    typeof(GetDefaultLanguageIdDataRequest),
                    typeof(GetCardTypeDataRequest)
                };
            }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;

            if (requestType == typeof(GetDeviceConfigurationDataServiceRequest))
            {
                response = this.GetDeviceConfiguration((GetDeviceConfigurationDataServiceRequest)request);
            }
            else if (requestType == typeof(GetChannelConfigurationDataServiceRequest))
            {
                response = this.GetChannelConfiguration((GetChannelConfigurationDataServiceRequest)request);
            }
            else if (requestType == typeof(GetChannelTenderTypesDataRequest))
            {
                response = this.GetChannelTenderTypes((GetChannelTenderTypesDataRequest)request);
            }
            else if (requestType == typeof(GetStoreDataServiceRequest))
            {
                response = this.GetStore((GetStoreDataServiceRequest)request);
            }
            else if (requestType == typeof(GetDefaultLanguageIdDataRequest))
            {
                response = this.GetDefaultLanguageId((GetDefaultLanguageIdDataRequest)request);
            }
            else if (requestType == typeof(GetCardTypeDataRequest))
            {
                response = this.GetCardType((GetCardTypeDataRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// The data service method to execute the data manager to get the channel tender types.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private EntityDataServiceResponse<TenderType> GetChannelTenderTypes(GetChannelTenderTypesDataRequest request)
        {
            var channelDataManager = this.GetChannelDataManagerInstance(request.RequestContext);
            var tenderTypes = channelDataManager.GetChannelTenderTypes(request.ChannelId, request.CountingRequired, request.QueryResultSettings);
            return new EntityDataServiceResponse<TenderType>(tenderTypes);
        }

        /// <summary>
        /// The data service method to execute the data manager to get the channel configuration.
        /// Gets the store by identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        private SingleEntityDataServiceResponse<OrgUnit> GetStore(GetStoreDataServiceRequest request)
        {
            ChannelDataManager channelDataManager = this.GetChannelDataManagerInstance(request.RequestContext);
            OrgUnit storeById = channelDataManager.GetStoreById(request.ChannelId);
            return new SingleEntityDataServiceResponse<OrgUnit>(storeById);
        }

        /// <summary>
        /// Gets the channel configuration by channel identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        private SingleEntityDataServiceResponse<ChannelConfiguration> GetChannelConfiguration(GetChannelConfigurationDataServiceRequest request)
        {
            var channelDataManager = this.GetChannelDataManagerInstance(request.RequestContext);
            var channelConfiguration = request.ChannelId.HasValue ? channelDataManager.GetChannelConfiguration(request.ChannelId.Value) : channelDataManager.GetChannelConfiguration();
            return new SingleEntityDataServiceResponse<ChannelConfiguration>(channelConfiguration);
        }

        /// <summary>
        /// The data service method to execute the data manager to get the device configuration.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private SingleEntityDataServiceResponse<DeviceConfiguration> GetDeviceConfiguration(GetDeviceConfigurationDataServiceRequest request)
        {
            var deviceConfigurationDataManager = this.GetDataManagerInstance(request.RequestContext);

            DeviceConfiguration deviceConfiguration = deviceConfigurationDataManager.GetDeviceConfiguration(request.StoreNumber, request.ChannelId, request.TerminalId, request.QueryResultSettings);

            return new SingleEntityDataServiceResponse<DeviceConfiguration>(deviceConfiguration);
        }

        private SingleEntityDataServiceResponse<string> GetDefaultLanguageId(GetDefaultLanguageIdDataRequest request)
        {
            ChannelDataManager channelDataManager = this.GetChannelDataManagerInstance(request.RequestContext);
            string languageId = channelDataManager.GetChannelDefaultLanguageId();
            return new SingleEntityDataServiceResponse<string>(languageId);
        }

        private EntityDataServiceResponse<CardTypeInfo> GetCardType(GetCardTypeDataRequest request)
        {
            ChannelDataManager channelDataManager = this.GetChannelDataManagerInstance(request.RequestContext);
            ReadOnlyCollection<CardTypeInfo> infos = channelDataManager.GetCardTypes(request.ChannelId, request.CardTypeId, request.QueryResultSettings);
            return new EntityDataServiceResponse<CardTypeInfo>(infos);
        }

        private DeviceConfigurationDataManager GetDataManagerInstance(RequestContext context)
        {
            return new DeviceConfigurationDataManager(context);
        }

        /// <summary>
        /// Gets the channel data manager instance.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The channel data manager.</returns>
        private ChannelDataManager GetChannelDataManagerInstance(RequestContext context)
        {
            return new ChannelDataManager(context);
        }
    }
}
