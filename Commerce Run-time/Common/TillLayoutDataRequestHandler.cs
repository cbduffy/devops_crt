﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.Common
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Channel data services that contains methods to retrieve the information by calling views.
    /// </summary>
    public class TillLayoutDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetTillLayoutDataServiceRequest)
                };
            }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();

            if (requestType == typeof(GetTillLayoutDataServiceRequest))
            {
                return this.GetTillLayout((GetTillLayoutDataServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }
        }
        
        /// <summary>
        /// Gets till layout by given parameters in request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        private SingleEntityDataServiceResponse<TillLayout> GetTillLayout(GetTillLayoutDataServiceRequest request)
        {
            var context = request.RequestContext;

            var tillLayoutDataManager = this.GetTillLayoutDataManagerInstance(context);

            string userId;
            long terminalId;
            long channelId;

            if (request.IsPrincipalSpecified)
            {
                userId = request.UserId;
                terminalId = request.TerminalId.Value;
                channelId = request.ChannelId.Value;
            }
            else
            {
                var principal = context.GetPrincipal();

                channelId = principal.ChannelId;
                terminalId = principal.TerminalId;
                userId = principal.UserId;
            }

            var tillLayout = tillLayoutDataManager.GetTillLayout(channelId, terminalId, userId);
            return new SingleEntityDataServiceResponse<TillLayout>(tillLayout);
        }

        /// <summary>
        /// Gets the till layout data manager instance.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The till layout data manager.</returns>
        private TillLayoutDataManager GetTillLayoutDataManagerInstance(RequestContext context)
        {
            return new TillLayoutDataManager(context);
        }
    }
}
