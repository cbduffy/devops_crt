﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Product data service class.
    /// </summary>
    public sealed class ProductDataRequestHandler : IRequestHandler
    {
        private static readonly Type[] SupportedRequestTypesArray = new Type[]
        {
            typeof(GetProductRefinersDataRequest),
            typeof(GetProductBarcodeDataRequest)
        };

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get { return SupportedRequestTypesArray; }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            Response response;

            if (request is GetProductRefinersDataRequest)
            {
                response = GetProductRefinersData((GetProductRefinersDataRequest)request);
            }
            else if (request is GetProductBarcodeDataRequest)
            {
                response = GetProductBarcodeData((GetProductBarcodeDataRequest)request);
            }
            else
            {
                string message = string.Format("Request type '{0}' is not supported", request.GetType().FullName);
                throw new NotSupportedException(message);
            }

            return response;
        }

        private static GetProductRefinersDataResponse GetProductRefinersData(GetProductRefinersDataRequest request)
        {
            ReadOnlyCollection<ProductRefiner> refiners;
            ReadOnlyCollection<ProductRefinerValue> refinerValues;

            GetProductDataManager(request.RequestContext).GetProductRefiners(request.SearchCriteria, request.Locale, out refiners, out refinerValues);

            return new GetProductRefinersDataResponse(refiners, refinerValues);
        }

        private static GetProductBarcodeDataResponse GetProductBarcodeData(GetProductBarcodeDataRequest request)
        {
            var itemDataManager = new ItemDataManager(request.RequestContext);
            ItemBarcode itemBarcode = itemDataManager.GetItemsByBarcode(request.Barcode, new ColumnSet());

            return new GetProductBarcodeDataResponse(itemBarcode);
        }

        private static ProductDataManager GetProductDataManager(RequestContext context)
        {
            return new ProductDataManager(context);
        }
    }
}