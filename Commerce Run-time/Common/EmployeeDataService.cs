﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.Common
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Employee data services that contains methods to retrieve the information by calling views.
    /// </summary>
    public class EmployeeDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new Type[]
                {
                    typeof(EntityDataServiceRequest<Employee>),
                    typeof(GetEmployeeDataRequest),
                    typeof(GetEmployeeStoresFromAddressBookDataRequest),
                    typeof(GetEmployeeBreakCategoriesByJobDataRequest),
                    typeof(GetEmployeePermissionsDataRequest),
                    typeof(EmployeeLogOnStoreDataRequest),
                    typeof(GetEmployeeIdFromLogOnKeyDataRequest),
                    typeof(GetOperationPermissionsDataRequest),
                    typeof(GetEmployeeBreakCategoriesByActivityDataRequest),
                    typeof(ValidateEmployeePasswordDataRequest)
                };
            }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;

            if (requestType == typeof(EntityDataServiceRequest<Employee>))
            {
                response = this.GetAllStoreEmployees((EntityDataServiceRequest<Employee>)request);
            }
            else if (requestType == typeof(GetEmployeeDataRequest))
            {
                response = this.GetEmployee((GetEmployeeDataRequest)request);
            }
            else if (requestType == typeof(GetEmployeeStoresFromAddressBookDataRequest))
            {
                response = this.GetEmployeeStoresFromAddressBook((GetEmployeeStoresFromAddressBookDataRequest)request);
            }
            else if (requestType == typeof(GetEmployeeBreakCategoriesByJobDataRequest))
            {
                response = this.GetEmployeeBreakCategoriesByJob((GetEmployeeBreakCategoriesByJobDataRequest)request);
            }
            else if (requestType == typeof(GetEmployeePermissionsDataRequest))
            {
                response = this.GetEmployeePermissions((GetEmployeePermissionsDataRequest)request);
            }
            else if (requestType == typeof(EmployeeLogOnStoreDataRequest))
            {
                response = this.EmployeeLogOnStore((EmployeeLogOnStoreDataRequest)request);
            }
            else if (requestType == typeof(GetEmployeeIdFromLogOnKeyDataRequest))
            {
                response = this.GetEmployeeIdFromLogOnKey((GetEmployeeIdFromLogOnKeyDataRequest)request);
            }
            else if (requestType == typeof(GetOperationPermissionsDataRequest))
            {
                response = this.GetOperationPermissions((GetOperationPermissionsDataRequest)request);
            }
            else if (requestType == typeof(GetEmployeeBreakCategoriesByActivityDataRequest))
            {
                response = this.GetEmployeeBreakCategoriesByActivity((GetEmployeeBreakCategoriesByActivityDataRequest)request);
            }
            else if (requestType == typeof(ValidateEmployeePasswordDataRequest))
            {
                response = this.ValidateEmployeePassword((ValidateEmployeePasswordDataRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// Gets Operation Permissions for the operation.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private EntityDataServiceResponse<OperationPermission> GetOperationPermissions(GetOperationPermissionsDataRequest request)
        {
            EmployeeDataManager dataManager = this.GetDataManagerInstance(request.RequestContext);
            var result = dataManager.GetOperationPermissions(request.OperationId, request.ColumnSet);
            return new EntityDataServiceResponse<OperationPermission>(result);
        }

        /// <summary>
        /// Gets the employee identifier from the specified logon key.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private SingleEntityDataServiceResponse<string> GetEmployeeIdFromLogOnKey(GetEmployeeIdFromLogOnKeyDataRequest request)
        {
            EmployeeDataManager dataManager = this.GetDataManagerInstance(request.RequestContext);
            var result = dataManager.GetEmployeeIdFromLogOnKey(request.LogOnKeyHash, request.LogOnType);
            return new SingleEntityDataServiceResponse<string>(result);
        }

        /// <summary>
        /// Logs On the user in the local store database.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private SingleEntityDataServiceResponse<Employee> EmployeeLogOnStore(EmployeeLogOnStoreDataRequest request)
        {
            EmployeeDataManager dataManager = this.GetDataManagerInstance(request.RequestContext);
            var result = dataManager.EmployeeLogOnStore(request.ChannelId, request.StaffId, request.PasswordHash, request.LogOnKeyHash, request.LogOnType, request.ExtraData, request.ColumnSet);
            return new SingleEntityDataServiceResponse<Employee>(result);
        }

        /// <summary>
        /// Gets the employee permission group for the staff identifier.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private SingleEntityDataServiceResponse<EmployeePermissions> GetEmployeePermissions(GetEmployeePermissionsDataRequest request)
        {
            EmployeeDataManager dataManager = this.GetDataManagerInstance(request.RequestContext);
            var result = dataManager.GetEmployeePermissions(request.StaffId, request.ColumnSet);
            return new SingleEntityDataServiceResponse<EmployeePermissions>(result);
        }

        /// <summary>
        /// Gets the employee break categories by job identifier.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private EntityDataServiceResponse<EmployeeActivity> GetEmployeeBreakCategoriesByJob(GetEmployeeBreakCategoriesByJobDataRequest request)
        {
            EmployeeDataManager dataManager = this.GetDataManagerInstance(request.RequestContext);
            var result = dataManager.GetEmployeeBreakCategoriesByJob(request.JobId);
            return new EntityDataServiceResponse<EmployeeActivity>(result);
        }

        /// <summary>
        /// Gets the employee stores from address book.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private EntityDataServiceResponse<OrgUnit> GetEmployeeStoresFromAddressBook(GetEmployeeStoresFromAddressBookDataRequest request)
        {
            EmployeeDataManager dataManager = this.GetDataManagerInstance(request.RequestContext);
            var result = dataManager.GetEmployeeStoresFromAddressBook(request.StaffId, request.Settings);
            return new EntityDataServiceResponse<OrgUnit>(result);
        }

        /// <summary>
        /// Gets the employee.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private SingleEntityDataServiceResponse<Employee> GetEmployee(GetEmployeeDataRequest request)
        {
            EmployeeDataManager dataManager = this.GetDataManagerInstance(request.RequestContext);
            Employee result = dataManager.GetEmployee(request.StaffId, request.Settings);
            return new SingleEntityDataServiceResponse<Employee>(result);
        }

        /// <summary>
        /// The data service method to execute the data manager to get all employees.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private EntityDataServiceResponse<Employee> GetAllStoreEmployees(EntityDataServiceRequest<Employee> request)
        {
            var employeeDataManager = this.GetDataManagerInstance(request.RequestContext);
            var employees = employeeDataManager.GetAllStoreEmployees(request.QueryResultSettings);
            return new EntityDataServiceResponse<Employee>(employees);
        }

        /// <summary>
        /// Gets the employee break categories by activity names.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private EntityDataServiceResponse<EmployeeActivity> GetEmployeeBreakCategoriesByActivity(GetEmployeeBreakCategoriesByActivityDataRequest request)
        {
            EmployeeDataManager dataManager = this.GetDataManagerInstance(request.RequestContext);
            var result = dataManager.GetEmployeeBreakCategoriesByActivity(request.ActivityNames);
            return new EntityDataServiceResponse<EmployeeActivity>(result);
        }

        /// <summary>
        /// Validates the employee password.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private SingleEntityDataServiceResponse<bool> ValidateEmployeePassword(ValidateEmployeePasswordDataRequest request)
        {
            EmployeeDataManager dataManager = this.GetDataManagerInstance(request.RequestContext);
            bool result = dataManager.ValidateEmployeePassword(request.ChannelId, request.StaffId, request.PasswordHash, request.LogOnKeyHash, request.LogOnType, request.QueryResultSettings.ColumnSet);
            return new SingleEntityDataServiceResponse<bool>(result);
        }

        /// <summary>
        /// Gets the data manager instance.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>An instance of <see cref="EmployeeDataManager"/></returns>
        private EmployeeDataManager GetDataManagerInstance(RequestContext context)
        {
            return new EmployeeDataManager(context);
        }
    }
}
