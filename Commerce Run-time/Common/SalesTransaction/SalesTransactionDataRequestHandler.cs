﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer.DataServices
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// The data request handler for sales transaction.
    /// </summary>
    public sealed class SalesTransactionDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(SaveSalesTransactionDataRequest);
                yield return typeof(SearchSalesTransactionDataRequest);
                yield return typeof(GetReceiptMaskDataRequest);
                yield return typeof(UpdateReturnQuantitiesDataRequest);
                yield return typeof(PurgeSalesTransactionsDataRequest);
            }
        }

        /// <summary>
        /// Gets the sales transaction to be saved.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <returns>The response message.</returns>
        public Response Execute(Request request)
        {
            Response response;

            if (request is SaveSalesTransactionDataRequest)
            {
                response = SaveSalesTransaction((SaveSalesTransactionDataRequest)request);
            }
            else if (request is SearchSalesTransactionDataRequest)
            {
                response = SearchSalesTransaction((SearchSalesTransactionDataRequest)request);
            }
            else if (request is UpdateReturnQuantitiesDataRequest)
            {
                response = UpdateReturnQuantities((UpdateReturnQuantitiesDataRequest)request);
            }
            else if (request is GetReceiptMaskDataRequest)
            {
                response = GetReceiptMask((GetReceiptMaskDataRequest)request);
            }
            else if (request is PurgeSalesTransactionsDataRequest)
            {
                response = PurgeSalesTransactions((PurgeSalesTransactionsDataRequest)request);
            }
            else
            {
                string message = string.Format("Request type '{0}' is not supported", request.GetType().FullName);
                throw new NotSupportedException(message);
            }

            return response;
        }

        private static EntityDataServiceResponse<SalesOrder> SearchSalesTransaction(SearchSalesTransactionDataRequest request)
        {
            // retrieve transactions
            var getSalesTransactionsRequest = new GetSalesTransactionDataRequest(request.SearchCriteria, request.QueryResultSettings);
            EntityDataServiceResponse<SalesOrder> response = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<SalesOrder>>(getSalesTransactionsRequest, request.RequestContext);
            new SalesOrderDataManager(request.RequestContext).FillSalesOrderMembers(response.EntityCollection, true);

            return new EntityDataServiceResponse<SalesOrder>(response.EntityCollection);
        }

        private static NullResponse SaveSalesTransaction(SaveSalesTransactionDataRequest request)
        {
            new SalesOrderDataManager(request.RequestContext).SaveSalesOrder(request.SalesTransaction);
            return new NullResponse();
        }

        private static NullResponse UpdateReturnQuantities(UpdateReturnQuantitiesDataRequest request)
        {
            new SalesOrderDataManager(request.RequestContext).UpdateReturnQuantities(request.SalesLines);
            return new NullResponse();
        }

        private static SingleEntityDataServiceResponse<ReceiptMask> GetReceiptMask(GetReceiptMaskDataRequest request)
        {
            var salesOrderDataManager = new SalesOrderDataManager(request.RequestContext);
            ReceiptMask mask = salesOrderDataManager.GetReceiptMask(request.FunctionalityProfileId, request.ReceiptTransactionType);
            return new SingleEntityDataServiceResponse<ReceiptMask>(mask);
        }

        private static NullResponse PurgeSalesTransactions(PurgeSalesTransactionsDataRequest request)
        {
            var salesOrderDataManager = new SalesOrderDataManager(request.RequestContext);
            salesOrderDataManager.PurgeSalesTransactions(request.ChannelId, request.TerminalId, request.RetentionDays);
            return new NullResponse();
        }    
    }
}