﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// The data request handler for transaction log.
    /// </summary>
    public class TransactionLogDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(SaveTransactionLogDataServiceRequest);
            }
        }

        /// <summary>
        /// Gets the sales transaction to be saved.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <returns>The response message.</returns>
        public Response Execute(Request request)
        {
            Response response;

            if (request is SaveTransactionLogDataServiceRequest)
            {
                response = SaveTransactionLog((SaveTransactionLogDataServiceRequest)request);
            }
            else
            {
                string message = string.Format("Request type '{0}' is not supported", request.GetType().FullName);
                throw new NotSupportedException(message);
            }

            return response;
        }

        private static NullResponse SaveTransactionLog(SaveTransactionLogDataServiceRequest request)
        {
            new TransactionLogDataManager(request.RequestContext).Save(request.TransactionLog);
            return new NullResponse();
        }
    }
}
