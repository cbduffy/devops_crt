﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.Common
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Pricing data services that contains methods to retrieve the information by calling views.
    /// </summary>    
    public class PricingDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(EntityDataServiceRequest<RetailDiscount>);
                yield return typeof(GetDiscountCodesDataServiceRequest);
                yield return typeof(EntityDataServiceRequest<IEnumerable<long>, CatalogPriceGroup>);
                yield return typeof(EntityDataServiceRequest<IEnumerable<string>, DiscountCode>);
                yield return typeof(EntityDataServiceRequest<IEnumerable<string>, Item>);
                yield return typeof(EntityDataServiceRequest<IEnumerable<string>, ProductVariant>);
                yield return typeof(EntityDataServiceRequest<IEnumerable<long>, RetailCategoryMember>);
                yield return typeof(EntityDataServiceRequest<IEnumerable<string>, MixAndMatchLineGroup>);
                yield return typeof(EntityDataServiceRequest<IEnumerable<string>, QuantityDiscountLevel>);
                yield return typeof(EntityDataServiceRequest<PriceGroup>);
                yield return typeof(EntityDataServiceRequest<PriceParameters>);
                yield return typeof(EntityDataServiceRequest<IEnumerable<string>, RetailDiscountPriceGroup>);
                yield return typeof(EntityDataServiceRequest<IEnumerable<string>, ThresholdDiscountTier>);
                yield return typeof(EntityDataServiceRequest<string, ValidationPeriod>);
                yield return typeof(EntityDataServiceRequest<IEnumerable<string>, ValidationPeriod>);
            }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;

            if (requestType == typeof(EntityDataServiceRequest<RetailDiscount>))
            {
                response = this.GetAllRetailDiscounts((EntityDataServiceRequest<RetailDiscount>)request);
            }
            else if (requestType == typeof(GetDiscountCodesDataServiceRequest))
            {
                response = this.GetDiscountCodes((GetDiscountCodesDataServiceRequest)request);
            }            
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<long>, CatalogPriceGroup>))
            {
                response = this.GetCatalogPriceGroups((EntityDataServiceRequest<IEnumerable<long>, CatalogPriceGroup>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<string>, DiscountCode>))
            {
                response = this.GetDiscountCodesByOfferId((EntityDataServiceRequest<IEnumerable<string>, DiscountCode>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<string>, Item>))
            {
                response = this.GetItems((EntityDataServiceRequest<IEnumerable<string>, Item>)request);
            }            
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<string>, ProductVariant>))
            {
                response = this.GetVariantDimensionsByItemIds((EntityDataServiceRequest<IEnumerable<string>, ProductVariant>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<long>, RetailCategoryMember>))
            {
                response = this.GetRetailCategoryMembersForItems((EntityDataServiceRequest<IEnumerable<long>, RetailCategoryMember>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<string>, MixAndMatchLineGroup>))
            {
                response = this.GetMixAndMatchLineGroupsByOfferIds((EntityDataServiceRequest<IEnumerable<string>, MixAndMatchLineGroup>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<string>, QuantityDiscountLevel>))
            {
                response = this.GetMultipleBuyDiscountLinesByOfferIds((EntityDataServiceRequest<IEnumerable<string>, QuantityDiscountLevel>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<PriceGroup>))
            {
                response = this.GetPriceGroups((EntityDataServiceRequest<PriceGroup>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<PriceParameters>))
            {
                response = this.GetPriceParameters((EntityDataServiceRequest<PriceParameters>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<string>, RetailDiscountPriceGroup>))
            {
                response = this.GetRetailDiscountPriceGroups((EntityDataServiceRequest<IEnumerable<string>, RetailDiscountPriceGroup>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<string>, ThresholdDiscountTier>))
            {
                response = this.GetThresholdTiersByOfferIds((EntityDataServiceRequest<IEnumerable<string>, ThresholdDiscountTier>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<string, ValidationPeriod>))
            {
                response = this.GetValidationPeriodById((EntityDataServiceRequest<string, ValidationPeriod>)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<string>, ValidationPeriod>))
            {
                response = this.GetValidationPeriodsByIds((EntityDataServiceRequest<IEnumerable<string>, ValidationPeriod>)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType().ToString()));
            }

            return response;
        }

        private PricingDataManager GetDataManagerInstance(RequestContext context)
        {
            return new PricingDataManager(context);
        }

        private EntityDataServiceResponse<RetailDiscount> GetAllRetailDiscounts(EntityDataServiceRequest<RetailDiscount> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var retailDiscounts = pricingDataManager.GetAllRetailDiscounts();

            return new EntityDataServiceResponse<RetailDiscount>(retailDiscounts);
        }

        private EntityDataServiceResponse<DiscountCode> GetDiscountCodes(GetDiscountCodesDataServiceRequest request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var discountCodes = pricingDataManager.GetDiscountCodes(request.OfferId, request.DiscountCode, request.Keyword, request.MinActiveDate.DateTime, request.QueryResultSettings);

            return new EntityDataServiceResponse<DiscountCode>(discountCodes);
        }        

        private EntityDataServiceResponse<CatalogPriceGroup> GetCatalogPriceGroups(EntityDataServiceRequest<IEnumerable<long>, CatalogPriceGroup> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var catalogPriceGroups = pricingDataManager.GetCatalogPriceGroups(request.RequestParameter as ISet<long>);

            return new EntityDataServiceResponse<CatalogPriceGroup>(catalogPriceGroups);
        }

        private EntityDataServiceResponse<DiscountCode> GetDiscountCodesByOfferId(EntityDataServiceRequest<IEnumerable<string>, DiscountCode> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var discountCodes = pricingDataManager.GetDiscountCodesByOfferId(request.RequestParameter, new ColumnSet());

            return new EntityDataServiceResponse<DiscountCode>(discountCodes);
        }

        private EntityDataServiceResponse<Item> GetItems(EntityDataServiceRequest<IEnumerable<string>, Item> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var items = pricingDataManager.GetItems(request.RequestParameter);

            return new EntityDataServiceResponse<Item>(items);
        }

        private EntityDataServiceResponse<ProductVariant> GetVariantDimensionsByItemIds(EntityDataServiceRequest<IEnumerable<string>, ProductVariant> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var productVariants = pricingDataManager.GetVariantDimensionsByItemIds(request.RequestParameter);

            return new EntityDataServiceResponse<ProductVariant>(productVariants);
        }

        private EntityDataServiceResponse<RetailCategoryMember> GetRetailCategoryMembersForItems(EntityDataServiceRequest<IEnumerable<long>, RetailCategoryMember> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var retailCategoryMembers = pricingDataManager.GetRetailCategoryMembersForItems(request.RequestParameter as ISet<long>);

            return new EntityDataServiceResponse<RetailCategoryMember>(retailCategoryMembers);
        }

        private EntityDataServiceResponse<MixAndMatchLineGroup> GetMixAndMatchLineGroupsByOffer(EntityDataServiceRequest<string, MixAndMatchLineGroup> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var mixAndMatchLineGroups = pricingDataManager.GetMixAndMatchLineGroupsByOffer(request.RequestParameter, new QueryResultSettings());

            return new EntityDataServiceResponse<MixAndMatchLineGroup>(mixAndMatchLineGroups);
        }

        private EntityDataServiceResponse<MixAndMatchLineGroup> GetMixAndMatchLineGroupsByOfferIds(EntityDataServiceRequest<IEnumerable<string>, MixAndMatchLineGroup> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var mixAndMatchLineGroups = pricingDataManager.GetMixAndMatchLineGroupsByOfferIds(request.RequestParameter);

            return new EntityDataServiceResponse<MixAndMatchLineGroup>(mixAndMatchLineGroups);
        }

        private EntityDataServiceResponse<QuantityDiscountLevel> GetMultipleBuyDiscountLinesByOfferIds(EntityDataServiceRequest<IEnumerable<string>, QuantityDiscountLevel> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var qtyDiscountLevels = pricingDataManager.GetMultipleBuyDiscountLinesByOfferIds(request.RequestParameter);

            return new EntityDataServiceResponse<QuantityDiscountLevel>(qtyDiscountLevels);
        }

        private EntityDataServiceResponse<PriceGroup> GetPriceGroups(EntityDataServiceRequest<PriceGroup> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var priceGroups = pricingDataManager.GetPriceGroups(new QueryResultSettings());

            return new EntityDataServiceResponse<PriceGroup>(priceGroups);
        }

        private EntityDataServiceResponse<PriceParameters> GetPriceParameters(EntityDataServiceRequest<PriceParameters> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var priceParameters = new ReadOnlyCollection<PriceParameters>(new[] { pricingDataManager.GetPriceParameters(new ColumnSet()) });

            return new EntityDataServiceResponse<PriceParameters>(priceParameters);
        }

        private EntityDataServiceResponse<RetailDiscountPriceGroup> GetRetailDiscountPriceGroups(EntityDataServiceRequest<IEnumerable<string>, RetailDiscountPriceGroup> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var retailDiscountPriceGroups = pricingDataManager.GetRetailDiscountPriceGroups(request.RequestParameter as ISet<string>);

            return new EntityDataServiceResponse<RetailDiscountPriceGroup>(retailDiscountPriceGroups);
        }

        private EntityDataServiceResponse<ThresholdDiscountTier> GetThresholdTiersByOfferIds(EntityDataServiceRequest<IEnumerable<string>, ThresholdDiscountTier> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var thresholdDiscountTiers = pricingDataManager.GetThresholdTiersByOfferIds(request.RequestParameter);

            return new EntityDataServiceResponse<ThresholdDiscountTier>(thresholdDiscountTiers);
        }

        private EntityDataServiceResponse<ValidationPeriod> GetValidationPeriodById(EntityDataServiceRequest<string, ValidationPeriod> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var validationPeriods = new ReadOnlyCollection<ValidationPeriod>(new[] { pricingDataManager.GetValidationPeriodById(request.RequestParameter, new ColumnSet()) });

            return new EntityDataServiceResponse<ValidationPeriod>(validationPeriods);
        }

        private EntityDataServiceResponse<ValidationPeriod> GetValidationPeriodsByIds(EntityDataServiceRequest<IEnumerable<string>, ValidationPeriod> request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);

            var validationPeriods = pricingDataManager.GetValidationPeriodsByIds(request.RequestParameter, new ColumnSet());

            return new EntityDataServiceResponse<ValidationPeriod>(validationPeriods);
        }
    }
}
