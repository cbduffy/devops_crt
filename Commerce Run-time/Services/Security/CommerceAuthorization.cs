﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

// --------------------------------------------------------------------------------------------------------------------
// <copyright company="Microsoft Corporation" file="CommerceAuthorization.cs">
//   THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
// OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
// THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
// NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
// </copyright>
// <summary>
//   Commerce Authorization Helper functions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;

    /// <summary>
    /// Commerce Authorization Helper functions.
    /// </summary>
    public static class CommerceAuthorization
    {
        /// <summary>
        /// The manager privilege.
        /// </summary>
        private const string ManagerPrivilege = "MANAGERPRIVILEGES";

        /// <summary>
        /// Checks if the principal has permission to do the operation.If not, throws UserAuthorizationException for permission check within workflows.
        /// </summary>
        /// <param name="principal">The request.</param>
        /// <param name="operationId">Operation Id.</param>
        /// <param name="context">Request Context.</param>
        public static void CheckAccess(CommercePrincipal principal, RetailOperation operationId, RequestContext context)
        {
            CheckAccess(principal, operationId, context, null, false, true);
        }

        /// <summary>
        /// Checks if the principal has permission to do the operation.If not, throws UserAuthorizationException for permission check within workflows.
        /// </summary>
        /// <param name="principal">The request.</param>
        /// <param name="operationId">Operation Id.</param>
        /// <param name="context">Request Context.</param>
        /// <param name="nonDrawerOperationCheckRequired">Is non-drawer mode operation check required.</param>
        public static void CheckAccess(CommercePrincipal principal, RetailOperation operationId, RequestContext context, bool nonDrawerOperationCheckRequired)
        {
            CheckAccess(principal, operationId, context, null, false, nonDrawerOperationCheckRequired);
        }

        /// <summary>
        /// Checks if the principal has permission to do the operation.If not, throws UserAuthorizationException.
        /// </summary>
        /// <param name="principal">The request.</param>
        /// <param name="operationId">Operation Id.</param>
        /// <param name="context">Request Context.</param>
        /// <param name="allowedRoles">Allowed roles.</param>
        /// <param name="deviceTokenRequired">Device token required.</param>
        public static void CheckAccess(CommercePrincipal principal, RetailOperation operationId, RequestContext context, string[] allowedRoles, bool deviceTokenRequired)
        {
            CheckAccess(principal, operationId, context, allowedRoles, deviceTokenRequired, true);
        }

        /// <summary>
        /// Checks if the principal has permission to do the operation.If not, throws UserAuthorizationException.
        /// </summary>
        /// <param name="principal">The request.</param>
        /// <param name="operationId">Operation Id.</param>
        /// <param name="context">Request Context.</param>
        /// <param name="allowedRoles">Allowed roles.</param>
        /// <param name="deviceTokenRequired">Device token required.</param>
        /// <param name="nonDrawerOperationCheckRequired">Is non-drawer mode operation check required.</param>
        public static void CheckAccess(CommercePrincipal principal, RetailOperation operationId, RequestContext context, string[] allowedRoles, bool deviceTokenRequired, bool nonDrawerOperationCheckRequired)
        {
            if (principal == null)
            {
                throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Invalid principal.");
            }

            if (context == null)
            {
                throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Invalid context.");
            }

            // Access check not enabled for storefront operations.
            if (principal.IsInRole(CommerceRoles.Storefront))
            {
                return;
            }

            // Check device token
            if (deviceTokenRequired)
            {
                if (string.IsNullOrEmpty(principal.DeviceToken))
                {
                    throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Invalid token.");
                }
            }

            // Check if the principal is one of the allowed roles.
            if (allowedRoles != null)
            {
                bool allowedRole = false;
                foreach (string role in allowedRoles)
                {
                    if (principal.IsInRole(role))
                    {
                        allowedRole = true;
                        break;
                    }
                }

                // If the user is not in the allowed roles, throw unauthorized exception.
                if (!allowedRole)
                {
                    throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Assigned role is not allowed to perform this operation.");
                }
            }

            // Validates the non drawer operation permission.
            if (nonDrawerOperationCheckRequired)
            {
                CheckNonDrawerOperationPermission(operationId, context);
            }

            // If the principal has Manager privilege, always allow operation.
            if (IsManager(principal))
            {
                return;
            }

            // Only employees users have access to the retail operation specified in the attribute. 
            if (operationId != RetailOperation.None)
            {
                GetOperationPermissionsDataRequest dataRequest = new GetOperationPermissionsDataRequest(operationId, new ColumnSet());
                OperationPermission operationPermission = context.Execute<EntityDataServiceResponse<OperationPermission>>(dataRequest).EntityCollection.FirstOrDefault();
                if (operationPermission == null)
                {
                    throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Access denied for employee to perform this operation.");
                }

                // If CheckUserAccess flag is not enabled, return.
                if (operationPermission.CheckUserAccess == false)
                {
                    return;
                }

                // see if user is elevated to perform this operation only and throw an exception if not
                CheckUserIsElevatedForOperation(principal, operationId);

                // Enumerate the permissions for the operation and ensure user have access.
                foreach (string permission in operationPermission.Permissions)
                {
                    if (!principal.IsInRole(permission.ToUpperInvariant()))
                    {
                        throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Access denied for employee to perform this operation (permissions).");
                    }
                }
            }
        }

        /// <summary>
        /// Throw exception if user settings does not match expected value.
        /// </summary>
        /// <param name="message">Error message.</param>
        public static void UserSettingsOutOfRange(string message)
        {
            throw new UserAuthorizationException(message);
        }

        /// <summary>
        /// Method to check if the principal has manager permission.
        /// </summary>
        /// <param name="principal">Commerce Principal.</param>
        public static void CheckAccessManager(CommercePrincipal principal)
        {
            if (principal == null)
            {
                throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Invalid principal.");
            }

            if (!IsManager(principal))
            {
                throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Access denied for non-manager.");
            }
        }

        /// <summary>
        /// Method to check if the principal has a shift.
        /// </summary>
        /// <param name="principal">Commerce Principal.</param>
        public static void CheckAccessHasShift(CommercePrincipal principal)
        {
            if (principal == null)
            {
                throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Invalid principal.");
            }

            // Access check not enabled for storefront operations.
            if (principal.IsInRole(CommerceRoles.Storefront))
            {
                return;
            }

            if (principal.ShiftId == 0)
            {
                throw new UserAuthorizationException(SecurityErrors.NonDrawerOperationsOnly, "Shift is not open.");
            }
        }

        /// <summary>
        /// Method to check if the principal has manager permission.
        /// </summary>
        /// <param name="principal">Commerce Principal.</param>
        /// <returns>True or False.</returns>
        private static bool IsManager(CommercePrincipal principal)
        {
            if (principal == null)
            {
                throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Invalid principal.");
            }

            return principal.IsInRole(ManagerPrivilege);
        }

        /// <summary>
        /// This method performs validation for non drawer operation permissions.
        /// </summary>
        /// <param name="operationId">Retail operation Id.</param>
        /// <param name="context">The request context.</param>
        private static void CheckNonDrawerOperationPermission(RetailOperation operationId, RequestContext context)
        {
            // If the shift Id is not set then it is non-drawer operation performed by user.
            // Then we validate against the white list of retail operations that can be performed by user.
            // The terminal should be always set to support non-drawer operation. 
            if (context.GetPrincipal() != null && context.GetPrincipal().ShiftId == 0 && operationId != RetailOperation.None && !context.GetPrincipal().IsTerminalAgnostic)
            {
                // Few operations are added to the white list which are not related to non-drawer operations like:
                // ActivateDevice, Logon, DeactivateDevice, Logoff and CloseShift.
                switch (operationId)
                {
                    case RetailOperation.ActivateDevice:
                    case RetailOperation.ChangePassword:
                    case RetailOperation.LogOn:
                    case RetailOperation.DeactivateDevice:
                    case RetailOperation.LogOff:
                    case RetailOperation.CloseShift:
                    case RetailOperation.CustomerClear:
                    case RetailOperation.CustomerSearch:
                    case RetailOperation.CustomerAdd:
                    case RetailOperation.CustomerEdit:
                    case RetailOperation.CustomerTransactions:
                    case RetailOperation.CustomerTransactionsReport:
                    case RetailOperation.DatabaseConnectionStatus:
                    case RetailOperation.GiftCardBalance:
                    case RetailOperation.InventoryLookup:
                    case RetailOperation.ItemSearch:
                    case RetailOperation.MinimizePOSWindow:
                    case RetailOperation.PairHardwareStation:
                    case RetailOperation.PriceCheck:
                    case RetailOperation.ShippingAddressSearch:
                    case RetailOperation.ShowJournal:
                    case RetailOperation.ShowBlindClosedShifts:
                    case RetailOperation.Search:
                    case RetailOperation.ExtendedLogOn:
                    case RetailOperation.TimeRegistration:
                    case RetailOperation.ViewReport:
                    case RetailOperation.PrintZ:
                    case RetailOperation.PrintX:
                        break;

                    default:
                        {
                            throw new UserAuthorizationException(SecurityErrors.NonDrawerOperationsOnly, string.Format(@"Operation {0} cannot be executed; Shift is not open.", operationId));
                        }
                }
            }
        }

        /// <summary>
        /// Checks if user has been elevated for a given operation.
        /// </summary>
        /// <param name="principal">
        /// The principal.
        /// </param>
        /// <param name="operationId">
        /// The operation id.
        /// </param>
        private static void CheckUserIsElevatedForOperation(CommercePrincipal principal, RetailOperation operationId)
        {
            if (!string.IsNullOrWhiteSpace(principal.OriginalUserId)
                && principal.ElevatedRetailOperation != operationId)
            {
                throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed, @"Access denied; elevated to perform another operation.");
            }
        }
    }
}
