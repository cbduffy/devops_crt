﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Handlers;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Security;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Runtime.Messages;

    /// <summary>
    /// Device Service.
    /// </summary>
    public class DeviceManagementService : INamedRequestHandler
    {
        private const char Seperator = ':';

        /// <summary>
        /// Gets the unique name for this request handler.
        /// </summary>
        public string HandlerName
        {
            get { return ServiceTypes.DeviceManagementService; }
        }

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(ActivateDeviceServiceRequest),
                    typeof(AuthenticateDeviceServiceRequest),
                    typeof(AuthenticateDevicePartialServiceRequest),
                    typeof(DeactivateDeviceServiceRequest),
                    typeof(CreateHardwareStationTokenServiceRequest),
                    typeof(ValidateHardwareStationTokenServiceRequest)
                };
            }
        }

        /// <summary>
        /// Executes the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;
            if (requestType == typeof(ActivateDeviceServiceRequest))
            {
                response = ActivateDevice((ActivateDeviceServiceRequest)request);
            }
            else if (requestType == typeof(AuthenticateDeviceServiceRequest))
            {
                response = AuthenticateDevice((AuthenticateDeviceServiceRequest)request);
            }
            else if (requestType == typeof(AuthenticateDevicePartialServiceRequest))
            {
                response = AuthenticateDevicePartial((AuthenticateDevicePartialServiceRequest)request);
            }
            else if (requestType == typeof(DeactivateDeviceServiceRequest))
            {
                response = DeactivateDevice((DeactivateDeviceServiceRequest)request);
            }
            else if (requestType == typeof(CreateHardwareStationTokenServiceRequest))
            {
                response = CreateHardwareStationToken((CreateHardwareStationTokenServiceRequest)request);
            }
            else if (requestType == typeof(ValidateHardwareStationTokenServiceRequest))
            {
                response = ValidateHardwareStationToken((ValidateHardwareStationTokenServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// Activates the device.
        /// </summary>
        /// <param name="request">The device activation request.</param>
        /// <returns>The device activation response.</returns>
        private static ActivateDeviceServiceResponse ActivateDevice(ActivateDeviceServiceRequest request)
        {
            IRequestHandler transactionServiceHandler = request.RequestContext.Runtime.GetRequestHandler(request.GetType(), ServiceTypes.DeviceManagementTransactionService);
            ActivateDeviceServiceResponse response = request.RequestContext.Execute<ActivateDeviceServiceResponse>(request, transactionServiceHandler);
            
            var deviceActivationResult = response.DeviceActivationResult;

            // Generate the device token.
            if (deviceActivationResult != null && deviceActivationResult.Device != null)
            {
                deviceActivationResult.Device.Token = GenerateDeviceToken(request.RequestContext, deviceActivationResult.Device, GetDeviceTokenAlgorithm(request.RequestContext));
            }

            return response;
        }

        /// <summary>
        /// Deactivates the device.
        /// </summary>
        /// <param name="request">The device deactivation request.</param>
        /// <returns>The device deactivation response.</returns>
        private static DeactivateDeviceServiceResponse DeactivateDevice(DeactivateDeviceServiceRequest request)
        {
            IRequestHandler transactionServiceHandler = request.RequestContext.Runtime.GetRequestHandler(request.GetType(), ServiceTypes.DeviceManagementTransactionService);
            return request.RequestContext.Execute<DeactivateDeviceServiceResponse>(request, transactionServiceHandler);
        }

        /// <summary>
        /// Authenticates the device.
        /// </summary>
        /// <param name="request">The device authentication request.</param>
        /// <returns>The response.</returns>
        private static AuthenticateDeviceServiceResponse AuthenticateDevice(AuthenticateDeviceServiceRequest request)
        {
            Device device;

            if (string.IsNullOrWhiteSpace(request.DeviceNumber))
            {
                throw new ArgumentException("request.DeviceNumber is not set", "request");
            }

            if (string.IsNullOrWhiteSpace(request.Token))
            {
                throw new ArgumentException("request.Token is not set", "request");
            }

            device = ConstructDeviceFromToken(request.RequestContext, request.Token, request.DeviceNumber, GetDeviceTokenAlgorithm(request.RequestContext));

            if ((device == null) 
                || string.IsNullOrWhiteSpace(device.DeviceNumber) 
                || (device.TerminalRecordId == 0) 
                || (device.ChannelId == 0))
            {
                NetTracer.Error("Constructed record of device '{0}' from token is not valid.", request.DeviceNumber);
                throw new DeviceAuthenticationException(SecurityErrors.InvalidDevice);
            }

            try
            {
                AuthenticateDeviceTransactionServiceRequest serviceRequest = new AuthenticateDeviceTransactionServiceRequest(device);
                device = request.RequestContext.Execute<AuthenticateDeviceServiceResponse>(serviceRequest).Device;
            }
            catch (HeadquarterTransactionServiceException ex)
            {
                NetTracer.Error("Authenticating device '{0}' failed with exception: {1}.", request.DeviceNumber, ex);
                throw new DeviceAuthenticationException(SecurityErrors.InvalidDevice);
            }
            catch (Exception)
            {
                // If real time service authentication fails, try to do local database authentication once before throwing error
                NetTracer.Warning("Device Authentication failed for Device Id {0} from real time service, retrying with local database", device.DeviceNumber);

                GetDeviceDataServiceRequest getDeviceRequest = new GetDeviceDataServiceRequest(device.DeviceNumber);

                Device localDevice = request.RequestContext.Execute<SingleEntityDataServiceResponse<Device>>(getDeviceRequest).Entity;
                
                if (localDevice == null || localDevice.ChannelId != device.ChannelId || localDevice.TerminalId != device.TerminalId)
                {
                    NetTracer.Error("Valid device '{0}' record was not found in channel (ID: {1}) database.", request.DeviceNumber, device.ChannelId);
                    throw new DeviceAuthenticationException(SecurityErrors.InvalidDevice);
                }
            }

            return new AuthenticateDeviceServiceResponse(device);
        }

        /// <summary>
        /// Performs partial authentication of the device without communicating to a TS.
        /// </summary>
        /// <param name="request">The device authentication request.</param>
        /// <returns>The response.</returns>
        private static AuthenticateDevicePartialServiceResponse AuthenticateDevicePartial(AuthenticateDevicePartialServiceRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.DeviceNumber))
            {
                throw new ArgumentNullException("request", "request.DeviceNumber is not set");
            }

            if (string.IsNullOrWhiteSpace(request.Token))
            {
                throw new ArgumentNullException("request", "request.Token is not set");
            }

            Device device = ConstructDeviceFromToken(request.RequestContext, request.Token, request.DeviceNumber, GetDeviceTokenAlgorithm(request.RequestContext));

            return new AuthenticateDevicePartialServiceResponse(device);
        }

        /// <summary>
        /// Creates hardware station token.
        /// </summary>
        /// <param name="request">The create authentication token request.</param>
        /// <returns>Create authentication token response.</returns>
        private static CreateHardwareStationTokenServiceResponse CreateHardwareStationToken(CreateHardwareStationTokenServiceRequest request)
        {
            // Authorization token (pairing key) for the client and hardware station.
            string pairingKey = Guid.NewGuid().ToString();

            // Validateion token for HS to get validated from Retail Server
            string validationToken = request.DeviceNumber + Seperator + pairingKey + Seperator + DateTimeOffset.UtcNow.ToFileTime();

            try
            {
                CreateHardwareStationTokenResult result = new CreateHardwareStationTokenResult();

                result.PairingKey = pairingKey;
                result.HardwareStationToken = DeviceManagementService.EncryptData(
                                                                            request.RequestContext,
                                                                            validationToken,
                                                                            GetDeviceTokenAlgorithm(request.RequestContext),
                                                                            request.DeviceNumber);

                return new CreateHardwareStationTokenServiceResponse(result);
            }
            catch (Exception ex)
            {
                throw new SecurityException(SecurityErrors.HardwareStationTokenCreationFailed, "Failed to create a hardware station token.", ex);
            }
        }

        /// <summary>
        /// Validates hardware station token.
        /// </summary>
        /// <param name="request">The validate authentication token request.</param>
        /// <returns>Validate hardware station token response.</returns>
        private static ValidateHardwareStationTokenServiceResponse ValidateHardwareStationToken(ValidateHardwareStationTokenServiceRequest request)
        {
            const int TokenExpirationInMinutes = 1;
            string deviceNumber = string.Empty;
            long tokenCreationFileTime;
            ValidateHardwareStationTokenResult result = new ValidateHardwareStationTokenResult();

            try
            {
                string decryptedToken = DeviceManagementService.DecryptData(
                                            request.RequestContext,
                                            request.HardwareStationToken,
                                            GetDeviceTokenAlgorithm(request.RequestContext),
                                            request.DeviceNumber);

                string[] fields = decryptedToken.Split(Seperator);
                deviceNumber = fields[0];
                result.PairingKey = fields[1];
                tokenCreationFileTime = long.Parse(fields[2], CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                throw new SecurityException(SecurityErrors.InvalidHardwareStationToken, "Hardware station token validation failed.", ex);
            }

            if ((!request.DeviceNumber.Equals(deviceNumber, StringComparison.OrdinalIgnoreCase))
                || (DateTimeOffset.UtcNow.CompareTo(DateTimeOffset.FromFileTime(tokenCreationFileTime).AddMinutes(TokenExpirationInMinutes)) > 0))
            {
                throw new SecurityException(SecurityErrors.InvalidHardwareStationToken);
            }

            return new ValidateHardwareStationTokenServiceResponse(result);
        }

        private static string GenerateDeviceToken(RequestContext context, Device device, string securityAlgorithm)
        {
            if (device == null)
            {
                return null;
            }

            string token = device.DeviceNumber + Seperator + device.ChannelId + Seperator + device.TerminalRecordId + Seperator + device.TerminalId;
            try
            {
                return DeviceManagementService.EncryptData(
               context,
               token,
               securityAlgorithm,
               device.DeviceNumber);
            }
            catch (Exception e)
            {
                NetTracer.Error("Device '{0}' token encryption failed with exception: {1}", device.DeviceNumber, e);
                throw new DeviceAuthenticationException(SecurityErrors.InvalidDeviceToken, "Data encryption failed.", e);
            }
        }

        private static Device ConstructDeviceFromToken(RequestContext context, string token, string deviceNumber, string securityAlgorithm)
        {
            Device device;

            try
            {
                string decryptedToken = DeviceManagementService.DecryptData(
                                            context,
                                            token,
                                            securityAlgorithm,
                                            deviceNumber);

                string[] fields = decryptedToken.Split(Seperator);
                device = new Device
                {
                    DeviceNumber = fields[0], 
                    ChannelId = Convert.ToInt64(fields[1], CultureInfo.InvariantCulture), 
                    TerminalRecordId = Convert.ToInt64(fields[2], CultureInfo.InvariantCulture), 
                    TerminalId = fields[3], 
                    Token = token
                };
            }
            catch (Exception e)
            {
                NetTracer.Error("Device '{0}' token decryption failed with exception: {1}",  deviceNumber, e);

                // Incorrect format of the token
                throw new DeviceAuthenticationException(SecurityErrors.InvalidDeviceToken, "Data decryption failed.", e);
            }

            if (device.DeviceNumber != deviceNumber)
            {
                NetTracer.Error("Requested device number '{0}' mismatched with device token.", device.DeviceNumber);

                // Invalid device token
                throw new DeviceAuthenticationException(SecurityErrors.InvalidDeviceToken);
            }

            return device;
        }

        private static string GetDeviceTokenAlgorithm(RequestContext context)
        {
            TransactionServiceProfile transactionServiceProfile = ChannelDataManager.GetValidTransactionServiceProfile(context);

            return transactionServiceProfile.DeviceTokenAlgorithm;
        }

        /// <summary>
        /// Encrypts the data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="data">The data.</param>
        /// <param name="algorithm">The algorithm.</param>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns>The encrypted data.</returns>
        private static string EncryptData(RequestContext context, string data, string algorithm, string deviceId)
        {
            EncryptDataServiceRequest encryptDataServiceRequest = new EncryptDataServiceRequest(data, algorithm, deviceId, Secret.SecurityContext, Secret.SecurityRegKey);

            string encryptedData = context.Execute<EncryptDataServiceResponse>(encryptDataServiceRequest).Data;

            return encryptedData;
        }

        /// <summary>
        /// Decrypts the data.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="data">The data.</param>
        /// <param name="algorithm">The algorithm.</param>
        /// <param name="deviceId">The device identifier.</param>
        /// <returns>The decrypted data.</returns>
        private static string DecryptData(RequestContext context, string data, string algorithm, string deviceId)
        {
            DecryptDataServiceRequest decryptDataServiceRequest = new DecryptDataServiceRequest(data, algorithm, deviceId, Secret.SecurityContext, Secret.SecurityRegKey);

            string decryptedData = context.Execute<DecryptDataServiceResponse>(decryptDataServiceRequest).Data;

            return decryptedData;
        }
    }
}
