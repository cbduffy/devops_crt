﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Handlers;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Security;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Runtime.Messages;

    /// <summary>
    /// User authentication Service.
    /// </summary>
    public class UserAuthenticationService : INamedRequestHandler
    {
        /// <summary>
        /// Gets the unique name for this request handler.
        /// </summary>
        public string HandlerName
        {
            get { return ServiceTypes.UserAuthenticationService; }
        }

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(UserLogOnServiceRequest),
                    typeof(UserLogOffServiceRequest),
                    typeof(CheckAccessServiceRequest),
                    typeof(CheckAccessIsManagerServiceRequest),
                    typeof(CheckAccessHasShiftServiceRequest),
                    typeof(UserLogOnRenewalServiceRequest),
                    typeof(UserResetPasswordServiceRequest),
                    typeof(UserChangePasswordServiceRequest),
                    typeof(UnlockRegisterServiceRequest)
                };
            }
        }

        /// <summary>
        /// Hashes the password.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="password">The password.</param>
        /// <param name="algorithm">The algorithm.</param>
        /// <param name="staffId">The staff identifier.</param>
        /// <returns>The hash of the password.</returns>
        public static string HashPassword(RequestContext context, string password, string algorithm, string staffId)
        {
            HashDataServiceRequest hashDataServiceRequest = new HashDataServiceRequest(password, algorithm, staffId, Secret.SecurityContext, Secret.SecurityRegKey);

            string passwordHash = context.Execute<HashDataServiceResponse>(hashDataServiceRequest).Data;

            return passwordHash;
        }

        /// <summary>
        /// Executes the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;
            if (requestType == typeof(UserLogOnServiceRequest))
            {
                response = LogOnUser((UserLogOnServiceRequest)request);
            }
            else if (requestType == typeof(UserLogOffServiceRequest))
            {
                response = LogOffUser((UserLogOffServiceRequest)request);
            }
            else if (requestType == typeof(CheckAccessServiceRequest))
            {
                response = CheckAccess((CheckAccessServiceRequest)request);
            }
            else if (requestType == typeof(CheckAccessIsManagerServiceRequest))
            {
                response = CheckManagerAccess((CheckAccessIsManagerServiceRequest)request);
            }
            else if (requestType == typeof(CheckAccessHasShiftServiceRequest))
            {
                response = CheckHasShift((CheckAccessHasShiftServiceRequest)request);
            }
            else if (requestType == typeof(UserLogOnRenewalServiceRequest))
            {
                response = LogOnUserRenewal((UserLogOnRenewalServiceRequest)request);
            }
            else if (requestType == typeof(UserResetPasswordServiceRequest))
            {
                response = ResetPassword((UserResetPasswordServiceRequest)request);
            }
            else if (requestType == typeof(UserChangePasswordServiceRequest))
            {
                response = ChangePassword((UserChangePasswordServiceRequest)request);
            }
            else if (requestType == typeof(UnlockRegisterServiceRequest))
            {
                response = UnlockRegister((UnlockRegisterServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// Authenticate the user.
        /// </summary>
        /// <param name="request">The device activation request.</param>
        /// <returns>The device activation response.</returns>
        private static UserLogOnServiceResponse LogOnUser(UserLogOnServiceRequest request)
        {
            Employee employee;
            DeviceConfiguration configuration = null;

            ChannelConfiguration channelConfiguration = null;
            LogOnConfiguration logOnConfiguration;

            if (request.Device != null)
            {
                OrgUnit orgUnit = GetStoreFromDeviceUsingCurrentContext(request.RequestContext, request.Device);
                if (orgUnit != null && !orgUnit.IsPublished)
                {
                    throw new DataValidationException(DataValidationErrors.ChannelIsNotPublished);
                }

                configuration = UserAuthenticationService.GetDeviceConfiguration(request.RequestContext, request.Device);

                channelConfiguration = request.RequestContext.GetChannelConfiguration();

                if (channelConfiguration == null || !channelConfiguration.RecordId.Equals(request.Device.ChannelId))
                {
                    request.RequestContext.SetChannelId(request.Device.ChannelId);
                    channelConfiguration = UserAuthenticationService.GetChannelConfiguration(request);

                    if (channelConfiguration == null)
                    {
                        throw new DataValidationException(DataValidationErrors.TerminalNotFound);
                    }

                    request.RequestContext.SetChannelConfiguration(channelConfiguration);
                }

                if (request.RequestContext.GetTerminal() == null || !request.RequestContext.GetTerminal().RecordId.Equals(request.Device.TerminalRecordId))
                {
                    GetTerminalByRecordIdDataRequest dataRequest = new GetTerminalByRecordIdDataRequest(request.Device.TerminalRecordId, new ColumnSet());
                    Terminal terminal = request.RequestContext.Execute<SingleEntityDataServiceResponse<Terminal>>(dataRequest).Entity;

                    request.RequestContext.SetTerminal(terminal);

                    if (request.RequestContext.GetTerminal() == null)
                    {
                        throw new DataValidationException(DataValidationErrors.TerminalNotFound);
                    }
                }
            }

            // Validate the logon parameters. Function throws exception for invalid parameters
            ValidateLogOnParameters(request, configuration, channelConfiguration);

            // Authenticate the user using real time service if device is not specified or if terminal configuration is set with RealTime Service. 
            if (request.Device == null || channelConfiguration.TransactionServiceStaffLogOnConfiguration == LogOnConfiguration.RealTimeService)
            {
                logOnConfiguration = LogOnConfiguration.RealTimeService;
                employee = EmployeeLogOnRealTimeService(request);
            }
            else if (channelConfiguration.TransactionServiceStaffLogOnConfiguration == LogOnConfiguration.LocalDatabase)
            {
                logOnConfiguration = LogOnConfiguration.LocalDatabase;
                employee = EmployeeLogOnStore(request, channelConfiguration);
            }
            else
            {
                Debug.Assert(false, "Invalid Login configuration");
                throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
            }

            // If the user is marked to password change, only provide password change permission.
            if (employee.ChangePassword)
            {
                if (employee.Permissions.AllowPasswordChange)
                {
                    employee.Permissions = new EmployeePermissions();
                    employee.Permissions.AllowPasswordChange = true;
                }
                else
                {
                    NetTracer.Warning("User Login failed for user {0} with password change at logon and without change password permission", employee.StaffId);
                    
                    // If the user does not have password change permission, login is not allowed.
                    throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                }
            }

            // If the request is for logon with specific operation, check if the employee can execute requested operation. 
            if (request.RetailOperation != RetailOperation.None)
            {
                GetOperationPermissionsDataRequest dataRequest = new GetOperationPermissionsDataRequest(request.RetailOperation, new ColumnSet());
                OperationPermission operationPermission = request.RequestContext.Execute<EntityDataServiceResponse<OperationPermission>>(dataRequest).EntityCollection.FirstOrDefault();
                if (operationPermission == null)
                {
                    throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed);
                }

                // Check if principal has all required permissions for the Operation or have manager privilege.
                foreach (string permission in operationPermission.Permissions)
                {
                    if (!employee.Permissions.Roles.Contains(permission) && !employee.Permissions.HasManagerPrivileges)
                    {
                        throw new UserAuthorizationException(SecurityErrors.AuthorizationFailed);
                    }
                }
            }

            return new UserLogOnServiceResponse(employee, logOnConfiguration);
        }

        /// <summary>
        /// Writes an entry into the audit table.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="source">The log source.</param>
        /// <param name="value">The log entry.</param>
        /// <param name="traceLevel">The trace level.</param>
        private static void LogAuditEntry(RequestContext context, string source, string value, AuditLogTraceLevel traceLevel = AuditLogTraceLevel.Trace)
        {
            var auditLogServiceRequest = new InsertAuditLogServiceRequest(source, value, traceLevel, unchecked((int)context.RequestTimer.ElapsedMilliseconds));
            context.Execute<NullResponse>(auditLogServiceRequest);
        }

        /// <summary>
        /// LogOff the user.
        /// </summary>
        /// <param name="request">The device activation request.</param>
        /// <returns>The device activation response.</returns>
        private static Employee EmployeeLogOnRealTimeService(UserLogOnServiceRequest request)
        {
            Employee employee;

            try
            {
                employee = UserAuthenticationService.ExecuteTransactionService<UserLogOnServiceResponse>(request.RequestContext, request).Employee;
            }
            catch (HeadquarterTransactionServiceException)
            {
                throw;
            }
            catch (Exception)
            {
                // Retry login with local db only for staff with continue on TS Errors
                GetEmployeePermissionsDataRequest dataRequest = new GetEmployeePermissionsDataRequest(request.StaffId, new ColumnSet());
                EmployeePermissions permission = request.RequestContext.Execute<SingleEntityDataServiceResponse<EmployeePermissions>>(dataRequest).Entity;
                if (permission == null || !permission.ContinueOnTSErrors || request.Device == null)
                {
                    throw;
                }

                employee = EmployeeLogOnStore(request, request.RequestContext.GetChannelConfiguration());
            }

            return employee;
        }

        /// <summary>
        /// Logs on the user.
        /// </summary>
        /// <param name="request">The device activation request.</param>
        /// <param name="channelConfiguration">The channel configuration.</param>
        /// <returns>The device activation response.</returns>
        private static Employee EmployeeLogOnStore(UserLogOnServiceRequest request, ChannelConfiguration channelConfiguration)
        {
            Employee employee;
            Employee localEmployee;
            
            string staffId = request.StaffId;
            string passwordHash = string.Empty;
            string logonKeyHash = string.Empty;
            string passwordHashFunction = UserAuthenticationService.GetTransactionServiceProfile(request.RequestContext).StaffPasswordHash;

            if (!string.IsNullOrEmpty(request.LogOnKey))
            {
                logonKeyHash = UserAuthenticationService.HashPassword(request.RequestContext, request.LogOnKey, passwordHashFunction, ((int)request.LogOnType).ToString());
            }

            // Get the StaffId based on the LogOn key
            if (request.LogOnType == LogOnType.BarcodeReader || request.LogOnType == LogOnType.MagneticCardReader)
            {
                GetEmployeeIdFromLogOnKeyDataRequest dataRequest = new GetEmployeeIdFromLogOnKeyDataRequest(logonKeyHash, request.LogOnType);
                staffId = request.RequestContext.Execute<SingleEntityDataServiceResponse<string>>(dataRequest).Entity;
                if (string.IsNullOrWhiteSpace(staffId))
                {
                    throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                }
            }

            if (!string.IsNullOrEmpty(request.Password))
            {
                passwordHash = UserAuthenticationService.HashPassword(request.RequestContext, request.Password, passwordHashFunction, staffId);
            }

            // Logon to the store
            EmployeeLogOnStoreDataRequest employeeDataRequest = new EmployeeLogOnStoreDataRequest(request.Device.ChannelId, staffId, passwordHash, logonKeyHash, request.LogOnType, request.ExtraData, new ColumnSet());
            localEmployee = request.RequestContext.Execute<SingleEntityDataServiceResponse<Employee>>(employeeDataRequest).Entity;
            if (localEmployee == null || localEmployee.Permissions == null)
            {
                throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
            }

            // Return new object for the logged-on employee with only the required fields to ensure not returning any sensitive data.
            employee = new Employee();
            employee.Permissions = new EmployeePermissions();
            employee.Name = localEmployee.Name;
            employee.StaffId = localEmployee.StaffId;
            employee.NameOnReceipt = localEmployee.NameOnReceipt;
            employee.ChangePassword = localEmployee.ChangePassword;
            employee.Permissions = localEmployee.Permissions;

            // Lock the user if multiple logins are not allowed.
            if (!employee.Permissions.AllowMultipleLogins && channelConfiguration != null)
            {
                LockUserAtLogOnDataRequest dataRequest = new LockUserAtLogOnDataRequest(request.Device.ChannelId, request.Device.TerminalId, staffId, channelConfiguration.InventLocationDataAreaId);
                bool locked = request.RequestContext.Execute<SingleEntityDataServiceResponse<bool>>(dataRequest).Entity;
                if (!locked)
                {
                    throw new UserAuthenticationException(SecurityErrors.UserLogOnAnotherTerminal);
                }
            }

            return employee;
        }

        /// <summary>
        /// Logs off the user.
        /// </summary>
        /// <param name="request">The device activation request.</param>
        /// <returns>The device activation response.</returns>
        private static NullResponse LogOffUser(UserLogOffServiceRequest request)
        {
            if (request.LogOnConfiguration == LogOnConfiguration.RealTimeService)
            {
                try
                {
                    UserAuthenticationService.ExecuteTransactionService<Response>(request.RequestContext, request);
                }
                catch (Exception exception)
                {
                    // Allow logoff during internal error
                    NetTracer.Warning("Transaction service error during logoff", exception.Message);
                    return new NullResponse();
                }
            }

            if (request.LogOnConfiguration == LogOnConfiguration.LocalDatabase && request.Device != null && request.RequestContext.GetChannelConfiguration() != null)
            {
                UnLockUserAtLogOffDataRequest dataRequest = new UnLockUserAtLogOffDataRequest(request.Device.ChannelId, request.StaffId, request.RequestContext.GetChannelConfiguration().InventLocationDataAreaId);
                bool unlocked = request.RequestContext.Execute<SingleEntityDataServiceResponse<bool>>(dataRequest).Entity;
                if (!unlocked)
                {
                    throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                }
            }

            // Audit Log logoff request
            var auditMessage = string.Format("User has successfully logged off. OperatorID: {0}", request.StaffId);
            LogAuditEntry(request.RequestContext, "UserAuthenticationService.LogOffUser", auditMessage);

            return new NullResponse();
        }

        /// <summary>
        /// Reset password for the user.
        /// </summary>
        /// <param name="request">The device activation request.</param>
        /// <returns>The device activation response.</returns>
        private static NullResponse ResetPassword(UserResetPasswordServiceRequest request)
        {
            if (request.LogOnConfiguration == LogOnConfiguration.RealTimeService)
            {
                UserAuthenticationService.ExecuteTransactionService<Response>(request.RequestContext, request);
            }

            return new NullResponse();
        }

        /// <summary>
        /// Change password for the user.
        /// </summary>
        /// <param name="request">The device activation request.</param>
        /// <returns>The device activation response.</returns>
        private static NullResponse ChangePassword(UserChangePasswordServiceRequest request)
        {
            if (request.LogOnConfiguration == LogOnConfiguration.RealTimeService)
            {
                UserAuthenticationService.ExecuteTransactionService<Response>(request.RequestContext, request);
            }

            return new NullResponse();
        }

        /// <summary>
        /// Unlock the register.
        /// </summary>
        /// <param name="request">The unlock register request.</param>
        /// <returns>The unlock register response.</returns>
        private static NullResponse UnlockRegister(UnlockRegisterServiceRequest request)
        {
            ChannelConfiguration channelConfiguration = null;

            if (request.Device != null)
            {
                channelConfiguration = request.RequestContext.GetChannelConfiguration();

                if (channelConfiguration == null || !channelConfiguration.RecordId.Equals(request.Device.ChannelId))
                {
                    ChannelDataManager channelDataManager = new ChannelDataManager(request.RequestContext);

                    channelConfiguration = channelDataManager.GetChannelConfiguration(request.Device.ChannelId);

                    if (channelConfiguration == null)
                    {
                        throw new DataValidationException(DataValidationErrors.TerminalNotFound);
                    }
                }
            }

            string passwordHash = string.Empty;
            string logonKeyHash = string.Empty;
            string passwordHashFunction = UserAuthenticationService.GetTransactionServiceProfile(request.RequestContext).StaffPasswordHash;

            if (!string.IsNullOrEmpty(request.LogOnKey))
            {
                logonKeyHash = UserAuthenticationService.HashPassword(request.RequestContext, request.LogOnKey, passwordHashFunction, ((int)request.LogOnType).ToString());
            }

            if (!string.IsNullOrEmpty(request.Password))
            {
                passwordHash = HashPassword(request.RequestContext, request.Password, passwordHashFunction, request.StaffId);
            }

            // Authenticate the user using real time service if terminal configuration is set with RealTime Service. 
            if (channelConfiguration.TransactionServiceStaffLogOnConfiguration == LogOnConfiguration.RealTimeService)
            {
                try
                {
                    var validatePasswordRequest = new ValidateStaffPasswordTransactionServiceRequest(request.StaffId, passwordHash);
                    ExecuteTransactionService<Response>(request.RequestContext, validatePasswordRequest);
                }
                catch (HeadquarterTransactionServiceException)
                {
                    throw new UserAuthenticationException(SecurityErrors.UnlockRegisterFailed);
                }
            }
            else if (channelConfiguration.TransactionServiceStaffLogOnConfiguration == LogOnConfiguration.LocalDatabase)
            {
                ValidateEmployeePasswordDataRequest dataRequest = new ValidateEmployeePasswordDataRequest(request.Device.ChannelId, request.StaffId, passwordHash, logonKeyHash, request.LogOnType, new ColumnSet());
                bool isValid = request.RequestContext.Execute<SingleEntityDataServiceResponse<bool>>(dataRequest).Entity;
                if (!isValid)
                {
                    throw new UserAuthenticationException(SecurityErrors.UnlockRegisterFailed);
                }
            }

            return new NullResponse();
        }

        /// <summary>
        /// Authenticate the user.
        /// </summary>
        /// <param name="request">The device activation request.</param>
        /// <returns>The device activation response.</returns>
        private static UserLogOnRenewalServiceResponse LogOnUserRenewal(UserLogOnRenewalServiceRequest request)
        {
            Employee employee;

            try
            {
                // Authenticate the user using real time service if device is not specified or if terminal configuration is set with RealTime Service. 
                if (request.RequestContext.GetPrincipal().LogOnConfiguration == LogOnConfiguration.RealTimeService)
                {
                    employee = UserAuthenticationService.ExecuteTransactionService<UserLogOnRenewalServiceResponse>(request.RequestContext, request).Employee;
                }
                else if (request.RequestContext.GetPrincipal().LogOnConfiguration == LogOnConfiguration.LocalDatabase)
                {
                    EmployeeLogOnStoreDataRequest dataRequest = new EmployeeLogOnStoreDataRequest(request.Device.ChannelId, request.StaffId, null, null, LogOnType.None, null, new ColumnSet());
                    Employee localEmployee = request.RequestContext.Execute<SingleEntityDataServiceResponse<Employee>>(dataRequest).Entity;
                    if (localEmployee == null)
                    {
                        throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                    }

                    // Return new object for the logged-on employee with only the required fields.
                    employee = new Employee();
                    employee.Name = localEmployee.Name;
                    employee.StaffId = localEmployee.StaffId;
                    employee.NameOnReceipt = localEmployee.NameOnReceipt;

                    GetEmployeePermissionsDataRequest permissionsDataRequest = new GetEmployeePermissionsDataRequest(request.StaffId, new ColumnSet());
                    employee.Permissions = request.RequestContext.Execute<SingleEntityDataServiceResponse<EmployeePermissions>>(permissionsDataRequest).Entity;
                }
                else
                {
                    Debug.Assert(false, "Invalid Login configuration");
                    throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                }
            }
            catch (Exception ex)
            {
                throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed, ex.Message, ex);
            }

            return new UserLogOnRenewalServiceResponse(employee);
        }

        /// <summary>
        /// Validate inputs for the logon user. Throws exception for invalid parameters.
        /// </summary>
        /// <param name="request">LogOn service request.</param>
        /// <param name="configuration">The device configuration request.</param>
        /// <param name="channelConfiguration">Channel configuration for logon.</param>
        private static void ValidateLogOnParameters(UserLogOnServiceRequest request, DeviceConfiguration configuration, ChannelConfiguration channelConfiguration)
        {
            switch (request.LogOnType)
            {
                case LogOnType.None:

                    throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed, "Invalid LogOnType");

                case LogOnType.PasswordWithNoDevice:

                    // Staff Id and Password is required
                    if (string.IsNullOrWhiteSpace(request.StaffId) || string.IsNullOrWhiteSpace(request.Password))
                    {
                        throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed, "Invalid LogOnType");
                    }

                    break;

                case LogOnType.BarcodeReader:

                    // Only LogOnKey is required for Barcode reader login.
                    if (string.IsNullOrWhiteSpace(request.LogOnKey) || !string.IsNullOrWhiteSpace(request.StaffId))
                    {
                        throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                    }

                    // Check if valid password is provided based on the configuration for the terminal.
                    if (request.Device == null || configuration == null || (configuration.StaffBarcodeLogOnRequiresPassword && string.IsNullOrWhiteSpace(request.Password)))
                    {
                        throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                    }

                    // Logon with Barcode reader is supported only for LocalStore logon configuration.
                    if (request.Device == null || channelConfiguration == null || channelConfiguration.TransactionServiceStaffLogOnConfiguration != LogOnConfiguration.LocalDatabase)
                    {
                        throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                    }

                    break;

                case LogOnType.MagneticCardReader:

                    // Only LogOnKey is required for Magnetic card reader login.
                    if (string.IsNullOrWhiteSpace(request.LogOnKey) || !string.IsNullOrWhiteSpace(request.StaffId))
                    {
                        throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                    }

                    if (request.Device == null || configuration == null || (configuration.StaffCardLogOnRequiresPassword && string.IsNullOrWhiteSpace(request.Password)))
                    {
                        throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                    }

                    // Logon with Magnetic Card reader is supported only for LocalStore logon configuration.
                    if (request.Device == null || channelConfiguration == null || channelConfiguration.TransactionServiceStaffLogOnConfiguration != LogOnConfiguration.LocalDatabase)
                    {
                        throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed);
                    }

                    break;

                default:
                    throw new UserAuthenticationException(SecurityErrors.AuthenticationFailed, "Invalid LogOn Type");
            }
        }

        /// <summary>
        /// Check Access.
        /// </summary>
        /// <param name="request">The Check access request.</param>
        /// <returns>Service response.</returns>
        private static NullResponse CheckAccess(CheckAccessServiceRequest request)
        {
            CommerceAuthorization.CheckAccess(request.CommercePrincipal, request.RetailOperation, request.RequestContext, request.AllowedRoles, request.DeviceTokenRequired);
            return new NullResponse();
        }

        /// <summary>
        /// Check manager access.
        /// </summary>
        /// <param name="request">The Check access request.</param>
        /// <returns>Service response.</returns>
        private static NullResponse CheckManagerAccess(CheckAccessIsManagerServiceRequest request)
        {
            CommerceAuthorization.CheckAccessManager(request.CommercePrincipal);
            return new NullResponse();
        }

        /// <summary>
        /// Check if principal has shift.
        /// </summary>
        /// <param name="request">The check has shift request.</param>
        /// <returns>Service response.</returns>
        private static NullResponse CheckHasShift(CheckAccessHasShiftServiceRequest request)
        {
            CommerceAuthorization.CheckAccessHasShift(request.CommercePrincipal);
            return new NullResponse();
        }

        /// <summary>
        /// Gets the transaction service profile.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The transaction service profile.</returns>
        private static TransactionServiceProfile GetTransactionServiceProfile(RequestContext context)
        {
            return ChannelDataManager.GetValidTransactionServiceProfile(context);
        }

        /// <summary>
        /// Executes the transaction service.
        /// </summary>
        /// <typeparam name="T">The response type.</typeparam>
        /// <param name="context">The request context.</param>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        private static T ExecuteTransactionService<T>(RequestContext context, Request request) where T : Response
        {
            IRequestHandler handler = context.Runtime.GetRequestHandler(request.GetType(), ServiceTypes.UserAuthenticationTransactionService);
            return context.Execute<T>(request, handler);
        }

        /// <summary>
        /// Gets the device configuration.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="device">The device.</param>
        /// <returns>The device configuration.</returns>
        private static DeviceConfiguration GetDeviceConfiguration(RequestContext context, Device device)
        {
            GetDeviceConfigurationDataServiceRequest configurationRequest = new GetDeviceConfigurationDataServiceRequest(device.ChannelId, device.TerminalId);
            return context.Runtime.Execute<SingleEntityDataServiceResponse<DeviceConfiguration>>(configurationRequest, context).Entity;
        }

        /// <summary>
        /// Gets the device configuration.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The device configuration.</returns>
        private static ChannelConfiguration GetChannelConfiguration(UserLogOnServiceRequest request)
        {
            RequestContext context = request.RequestContext;
            Device device = request.Device;
            GetChannelConfigurationDataServiceRequest configurationRequest = new GetChannelConfigurationDataServiceRequest(device.ChannelId);
            return context.Runtime.Execute<SingleEntityDataServiceResponse<ChannelConfiguration>>(configurationRequest, context).Entity;
        }

        /// <summary>
        /// Gets the store by device.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="device">The device.</param>
        /// <returns>The store.</returns>
        private static OrgUnit GetStoreFromDeviceUsingCurrentContext(RequestContext context, Device device)
        {
            GetStoreDataServiceRequest request = new GetStoreDataServiceRequest(device.ChannelId);
            return context.Execute<SingleEntityDataServiceResponse<OrgUnit>>(request).Entity;
        }
    }
}
