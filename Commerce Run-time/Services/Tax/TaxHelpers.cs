/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;

    /// <summary>
    /// Encapsulate helpers for tax-related functionality.
    /// </summary>
    internal static class TaxHelpers
    {
        private const string CurrencyOperationName = "CURRENCYTOCURRENCY";

        /// <summary>
        /// Creates the rounding service request.
        /// </summary>
        /// <param name="storeCurrency">The store currency.</param>
        /// <param name="amount">The amount.</param>
        /// <returns>
        /// The rounded amount.
        /// </returns>
        public static GetRoundedValueServiceRequest CreateRoundingServiceRequest(string storeCurrency, decimal amount)
        {
            return new GetRoundedValueServiceRequest(amount, storeCurrency, 0, useSalesRounding: false);
        }

        /// <summary>
        /// Creates the currency service request.
        /// </summary>
        /// <param name="taxAmount">The tax amount.</param>
        /// <param name="fromCurrency">From currency.</param>
        /// <param name="toCurrency">To currency.</param>
        /// <returns>
        /// The currency request object.
        /// </returns>
        public static GetCurrencyValueServiceRequest CreateCurrencyServiceRequest(decimal taxAmount, string fromCurrency, string toCurrency)
        {
            return new GetCurrencyValueServiceRequest(fromCurrency, toCurrency, taxAmount);
        }
    }
}