﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.Dynamics.Commerce.Runtime.Data;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
using Microsoft.Dynamics.Commerce.Runtime.Messages;
using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using Microsoft.Dynamics.Retail.Diagnostics;
using Avalara.Ax.CRT;

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    public partial class TaxService
    {
        private static CalculateTaxServiceResponse CalculateTax(CalculateTaxServiceRequest request)
        {
            SalesTransaction transaction = null;

            if (request != null &&
                request.RequestContext != null &&
                request.RequestContext.GetSalesTransaction() != null)
            {
                transaction = request.RequestContext.GetSalesTransaction();
                //CS Developed by spriya - Clear old Avatax Error
                if (transaction["Avatax-Error"] != null && !string.IsNullOrEmpty(transaction["AvaTax-Error"].ToString()))
                transaction["Avatax-Error"]=null;
                //CE Devloped by spriya
                // Consider active (non-void) lines for tax. 
                // Need to recalculate tax on return-by-receipt lines because we cannot reconstruct tax lines from return transaction lines alone. 
                // A few key information like IsExempt, IsTaxInclusive, TaxCode are not available on return transaction line.
                foreach (var saleItem in transaction.ActiveSalesLines)
                {
                    saleItem.TaxRatePercent = 0;
                    saleItem.TaxLines.Clear();
                }

                var totaler = new SalesTransactionTotaler(transaction);
                totaler.CalculateTotals(request.RequestContext);

                ClearChargeTaxLines(transaction);

                //NS Developed by spriya - Avalara Merge - Dated 10/11/2014
                // TaxCodeProvider defaultProvider = GetTaxProvider(request.Context);      
                // defaultProvider.CalculateTax(request);
                Avalara.Ax.CRT.AvaTaxService avaTaxService = new AvaTaxService();
                avaTaxService.CalculateTax(request);
            }
            else
            {
                NetTracer.Error("Invalid request received: CalculateTax");
            }

            return new CalculateTaxServiceResponse(transaction);
        }
    }
}
