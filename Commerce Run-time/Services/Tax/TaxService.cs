﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Tax Service implementation.
    /// </summary>
    public partial class TaxService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(CalculateTaxServiceRequest),
                    typeof(GetTaxRegimeServiceRequest),
                    typeof(IndiaGetInterStateTaxRegimeServiceRequest)
                };
            }
        }

        /// <summary>
        /// Executes the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;
            if (requestType == typeof(CalculateTaxServiceRequest))
            {
                response = CalculateTax((CalculateTaxServiceRequest)request);
            }
            else if (requestType == typeof(GetTaxRegimeServiceRequest))
            {
                response = GetTaxRegime((GetTaxRegimeServiceRequest)request);
            }
            else if (requestType == typeof(IndiaGetInterStateTaxRegimeServiceRequest))
            {
                response = GetInterStateTaxRegimeIndia((IndiaGetInterStateTaxRegimeServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// Gets the tax regime.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        /// The sales tax regime information.
        /// </returns>
        private static GetTaxRegimeServiceResponse GetTaxRegime(GetTaxRegimeServiceRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            if (request.RequestContext == null)
            {
                throw new ArgumentNullException("request", "request.RequestContext");
            }

            if (request.RequestContext.Runtime == null)
            {
                throw new ArgumentNullException("request", "request.RequestContext.Runtime");
            }

            if (request.ShippingAddress == null)
            {
                throw new ArgumentNullException("request", "request.ShippingAddress");
            }

            if (string.IsNullOrWhiteSpace(request.ShippingAddress.ThreeLetterISORegionName) &&
                string.IsNullOrWhiteSpace(request.ShippingAddress.TwoLetterISORegionName))
            {
                throw new ArgumentNullException("request", "request.ShippingAddress ISORegionName");
            }

            Address address = request.ShippingAddress;
            string taxRegime = string.Empty;

            // If address has a tax group, then return this tax group.
            if (!string.IsNullOrWhiteSpace(address.TaxGroup))
            {
                taxRegime = address.TaxGroup;
                return new GetTaxRegimeServiceResponse(taxRegime);
            }

            Dictionary<string, string> predicates = new Dictionary<string, string>();

            var addressComponentsFilterHandlers = BuildDbtFilterList();
            for (int i = 0; i < addressComponentsFilterHandlers.Count; i++)
            {
                predicates.Clear();

                for (int j = i; j < addressComponentsFilterHandlers.Count; j++)
                {
                    if (addressComponentsFilterHandlers[j].CanHandle(address))
                    {
                        addressComponentsFilterHandlers[j].Handle(address, predicates);
                    }
                }

                GetSalesTaxGroupDataRequest dataRequest = new GetSalesTaxGroupDataRequest(predicates);
                taxRegime = request.RequestContext.Execute<SingleEntityDataServiceResponse<string>>(dataRequest).Entity;

                if (!string.IsNullOrWhiteSpace(taxRegime))
                {
                    break;
                }
            }

            if (string.IsNullOrWhiteSpace(taxRegime))
            {
                InvalidTaxGroupNotification notification = new InvalidTaxGroupNotification(request.ShippingAddress);

                if (request.RequestContext.Runtime.Notify(request.RequestContext, notification))
                {
                    var innerException = new ConfigurationException(
                        ConfigurationErrors.UnableToFindSalesTaxGroupForAddress,
                        "Unable to find a proper tax group for the given address. Ensure the required job is run properly.");

                    throw new NotificationException(
                        NotificationErrors.ConfigurationError,
                        notification,
                        innerException.Message,
                        innerException);
                }
            }

            return new GetTaxRegimeServiceResponse(taxRegime);
        }

        /// <summary>
        /// Calculates the tax for the last item.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        /// 
        //Moved to partial class
       /* private static CalculateTaxServiceResponse CalculateTax(CalculateTaxServiceRequest request)
        {
            SalesTransaction transaction = null;

            if (request != null &&
                request.RequestContext != null &&
                request.RequestContext.GetSalesTransaction() != null)
            {
                transaction = request.RequestContext.GetSalesTransaction();

                // Consider active (non-void) lines for tax. 
                // Need to recalculate tax on return-by-receipt lines because we cannot reconstruct tax lines from return transaction lines alone. 
                // A few key information like IsExempt, IsTaxInclusive, TaxCode are not available on return transaction line.
                foreach (var saleItem in transaction.ActiveSalesLines)
                {
                    saleItem.TaxRatePercent = 0;
                    saleItem.TaxLines.Clear();
                }

                var totaler = new SalesTransactionTotaler(transaction);
                totaler.CalculateTotals(request.RequestContext);

                ClearChargeTaxLines(transaction);

                TaxCodeProvider defaultProvider = GetTaxProvider(request.RequestContext);
                defaultProvider.CalculateTax(request.RequestContext);
            }
            else
            {
                NetTracer.Error("Invalid request received: CalculateTax");
            }

            return new CalculateTaxServiceResponse(transaction);
        }
        */

        /// <summary>
        /// Get the tax code provider.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>The tax code provider.</returns>
        private static TaxCodeProvider GetTaxProvider(RequestContext context)
        {
            TaxCodeProvider taxCodeProvider;
            switch (context.GetChannelConfiguration().CountryRegionISOCode)
            {
                case CountryRegionISOCode.IN:
                    taxCodeProvider = new TaxCodeProviderIndia();
                    break;
                default:
                    taxCodeProvider = new TaxCodeProvider();
                    break;
            }

            return taxCodeProvider;
        }

        /// <summary>
        /// Clears the tax lines in miscellaneous charge.
        /// </summary>
        /// <param name="transaction">The sales transaction.</param>
        private static void ClearChargeTaxLines(SalesTransaction transaction)
        {
            foreach (var charge in transaction.ChargeLines)
            {
                charge.TaxLines.Clear();
            }

            // Consider active (non-void) lines for tax. 
            // Need to recalculate tax on return-by-receipt lines because we cannot reconstruct tax lines from return transaction lines alone. 
            // A few key information like IsExempt, IsTaxInclusive, TaxCode are not available on return transaction line.
            foreach (var line in transaction.ActiveSalesLines)
            {
                foreach (var charge in line.ChargeLines)
                {
                    charge.TaxLines.Clear();
                }
            }
        }

        /// <summary>
        /// Builds the chain of destination-based tax filters.
        /// </summary>
        /// <returns>A collection of tax filters.</returns>
        private static List<DestinationFilterHandler> BuildDbtFilterList()
        {
            var filterList = new List<DestinationFilterHandler>();

            // This is order dependent.
            filterList.Add(DestinationFilterHandler.BuildZipPostalCodeFilter());
            filterList.Add(DestinationFilterHandler.BuildDistrictFilter());
            filterList.Add(DestinationFilterHandler.BuildCityFilter());
            filterList.Add(DestinationFilterHandler.BuildCountyFilter());
            filterList.Add(DestinationFilterHandler.BuildStateFilter());
            filterList.Add(DestinationFilterHandler.BuildCountryFilter());

            return filterList;
        }

        /// <summary>
        /// Gets the tax group for India inter-state transaction.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        /// The sales tax group information.
        /// </returns>
        private static IndiaGetInterStateTaxRegimeServiceResponse GetInterStateTaxRegimeIndia(IndiaGetInterStateTaxRegimeServiceRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            if (request.RequestContext == null)
            {
                throw new ArgumentNullException("request.Context");
            }

            if (request.RequestContext.Runtime == null)
            {
                throw new ArgumentNullException("request.Context.Runtime");
            }

            if (request.ShippingToAddress == null)
            {
                throw new ArgumentNullException("request", "request.ShippingAddress");
            }

            if (string.IsNullOrWhiteSpace(request.ShippingToAddress.ThreeLetterISORegionName) &&
                string.IsNullOrWhiteSpace(request.ShippingToAddress.TwoLetterISORegionName))
            {
                throw new ArgumentNullException("request", "request.ShippingAddress ISORegionName");
            }

            Address address = request.ShippingToAddress;
            string taxRegime = string.Empty;
            bool isInterState = false;
            string inventLocationId = request.ShippingFromInventLocationId;
            bool isApplyInterStateTax = false;

            IndiaTaxDataManager taxDataManager = new IndiaTaxDataManager(request.RequestContext);
            if (!string.IsNullOrWhiteSpace(inventLocationId))
            {
                ApplyInterStateTaxIndia interStateTaxSetting = taxDataManager.GetApplyInterStateTaxIndia(new ColumnSet()); 
                if (interStateTaxSetting != null)
                {
                    isApplyInterStateTax = interStateTaxSetting.IsApplyInterStateTaxIndia;
                }

                if (isApplyInterStateTax)
                {
                    Address shippingFromAddress = taxDataManager.GetWarehouseAddressIndia(inventLocationId);

                    if (shippingFromAddress != null &&
                        (shippingFromAddress.ThreeLetterISORegionName == address.ThreeLetterISORegionName) &&
                        !string.IsNullOrWhiteSpace(shippingFromAddress.State) &&
                        !string.IsNullOrWhiteSpace(address.State) &&
                        (shippingFromAddress.State != address.State))
                    {
                        isInterState = true;
                    }

                    if (isInterState)
                    {
                        taxRegime = taxDataManager.GetTaxRegimeIndia(new ColumnSet());

                        if (string.IsNullOrWhiteSpace(taxRegime))
                        {
                            InvalidTaxGroupNotification notification = new InvalidTaxGroupNotification(shippingFromAddress);

                            if (request.RequestContext.Runtime.Notify(request.RequestContext, notification))
                            {
                                var innerException = new ConfigurationException(
                                    ConfigurationErrors.UnableToFindSalesTaxGroupForAddress,
                                    "Unable to find a proper tax group for the inter-state transaction. Ensure inter-state transaction sales tax group is setup.");

                                throw new NotificationException(
                                    NotificationErrors.ConfigurationError,
                                    notification,
                                    innerException.Message,
                                    innerException);
                            }
                        }
                    }                    
                }                
            }

            return new IndiaGetInterStateTaxRegimeServiceResponse(isInterState, taxRegime);
        }
    }
}