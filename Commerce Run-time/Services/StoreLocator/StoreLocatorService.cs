﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// The store locator service.
    /// </summary>
    public class StoreLocatorService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetStoresServiceRequest)
                };
            }
        }

        /// <summary>
        /// Implements the Execute method for the IStoreLocatorService service interface.
        /// </summary>
        /// <param name="request">The request object.</param>
        /// <returns>The response object.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Response response;
            GetStoresServiceRequest getStoresRequest;
            if ((getStoresRequest = request as GetStoresServiceRequest) != null)
            {
                response = GetStores(getStoresRequest);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request '{0}' is not supported.", request.GetType().ToString()));
            }

            return response;
        }
    
        /// <summary>
        /// Gets all stores.
        /// </summary>
        /// <param name="request">Request instance.</param>
        /// <returns>Response instance.</returns>
        private static GetStoresServiceResponse GetStores(GetStoresServiceRequest request)
        {
            var storeLocatorDataManager = new StoreLocatorDataManager(request.RequestContext);

            IEnumerable<OrgUnitLocation> storeLocations = new Collection<OrgUnitLocation>();

            try
            {
                storeLocations = storeLocatorDataManager.GetStores(request.ChannelId, request.SearchArea, request.QueryResultSettings);
            }
            catch (StorageException innerException)
            {
                // suppress exception and raise the notification instead
                var notification = new UnableToRetrieveStoresNotification(request.ChannelId, request.SearchArea);

                if (request.RequestContext.Runtime.Notify(request.RequestContext, notification))
                {
                    throw new NotificationException(
                        NotificationErrors.StorageError,
                        notification,
                        innerException.Message,
                        innerException);
                }
            }

            return new GetStoresServiceResponse(storeLocations, request.QueryResultSettings.Paging.TotalNumberOfRecords);
        }
    }
}
