﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// The reason code service.
    /// </summary>
    public class ReasonCodeService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetReasonCodesServiceRequest),
                    typeof(CalculateRequiredReasonCodesServiceRequest),
                    typeof(GetReturnOrderReasonCodesServiceRequest)
                };
            }
        }

        /// <summary>
        /// Executes the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;
            if (requestType == typeof(GetReasonCodesServiceRequest))
            {
                response = GetReasonCodes((GetReasonCodesServiceRequest)request);
            }
            else if (requestType == typeof(CalculateRequiredReasonCodesServiceRequest))
            {
                response = ReasonCodesCalculator.CalculateRequiredReasonCodes((CalculateRequiredReasonCodesServiceRequest)request);
            }
            else if (requestType == typeof(GetReturnOrderReasonCodesServiceRequest))
            {
                response = GetReturnOrderReasonCodes((GetReturnOrderReasonCodesServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// Gets the reason codes.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The info codes response.</returns>
        private static GetReasonCodesServiceResponse GetReasonCodes(GetReasonCodesServiceRequest request)
        {
            RequestContext context = request.RequestContext;

            GetReasonCodesDataRequest getReasonCodeRequest = new GetReasonCodesDataRequest(request.ReasonCodeId, request.QueryResultSettings);
            var reasonCodes = context.Execute<EntityDataServiceResponse<ReasonCode>>(getReasonCodeRequest).EntityCollection;

            return new GetReasonCodesServiceResponse(reasonCodes);
        }

        /// <summary>
        /// Gets the return reason codes.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The info codes response.</returns>
        private static GetReturnOrderReasonCodesServiceResponse GetReturnOrderReasonCodes(GetReturnOrderReasonCodesServiceRequest request)
        {
            var reasonCodeDataManager = new ReasonCodeDataManager(request.RequestContext);

            var reasonCodes = reasonCodeDataManager.GetReturnOrderReasonCodes(request.QueryResultSettings);

            return new GetReturnOrderReasonCodesServiceResponse(reasonCodes);
        }
    }
}
