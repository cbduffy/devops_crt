﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;
    using DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Data.Types;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices;
    using Microsoft.Dynamics.Commerce.Runtime.Helpers;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.ReceiptIndia;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// The receipt service to get the formatted receipts.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "By design.")]
    public class ReceiptService : IRequestHandler
    {
        private const string Esc = "&#x1B;";
        private const string LegacyLogoMessage = "<L>";
        private const string LogoMessage = "<L:{0}>";
        private const string SingleSpace = "|1C";
        private const string DoubleSpace = "|2C";
        private const string CRLF = "CRLF";
        private const string Text = "Text";
        private const string ConstantFormat = "G29";
        private const string Taxes = "Taxes";
        private const string LoyaltyItem = "LoyaltyItem";
        private const string LoyaltyEarnText = "LoyaltyEarnText";
        private const string LoyaltyRedeemText = "LoyaltyRedeemText";
        private const string LoyaltyEarnLines = "LoyaltyEarnLines";
        private const string LoyaltyRedeemLines = "LoyaltyRedeemLines";

        private const string EARNEDREWARDPOINTID = "EARNEDREWARDPOINTID";
        private const string REDEEMEDREWARDPOINTID = "REDEEMEDREWARDPOINTID";
        private const string EARNEDREWARDPOINTAMOUNTQUANTITY = "EARNEDREWARDPOINTAMOUNTQUANTITY";
        private const string REDEEMEDREWARDPOINTAMOUNTQUANTITY = "REDEEMEDREWARDPOINTAMOUNTQUANTITY";

        private const string LineFormat = "{0}:{1}";
        private const char DottedPadding = '.';
        private const int PaperWidth = 55;
        private const string TypeFormat = "{0} [{1}]";
        private const string CurrencyFormat = "{0} ({1})";
        private const string ForiegnCurrencyFormat = "{0} - {1}";
        private const string CharPos = "charpos";
        private const string LineId = "line_id";

        private static readonly string SingleLine = string.Empty.PadLeft(55, '-');
        private static readonly string DoubleLine = string.Empty.PadLeft(55, '=');

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetReceiptServiceRequest),
                    typeof(GetEmailReceiptServiceRequest)
                };
            }
        }

        /// <summary>
        /// Executes the GetReceiptServiceRequest.
        /// </summary>
        /// <param name="request">The request parameter.</param>
        /// <returns>The GetReceiptServiceResponse that contains the formatted receipts.</returns>
        public Runtime.Messages.Response Execute(Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestedType = request.GetType();

            if (requestedType == typeof(GetReceiptServiceRequest))
            {
                return GetFormattedReceipt((GetReceiptServiceRequest)request);
            }
            else if (requestedType == typeof(GetEmailReceiptServiceRequest))
            {
                return GetEmailReceipt((GetEmailReceiptServiceRequest)request);
            }

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }

        internal static string CreateWhitespace(char seperator, int stringLength)
        {
            // If the length exceeds the printable length then no need to add additional whitespaces.
            if (stringLength <= 0)
            {
                stringLength = 0;
            }

            string whiteString = new string(seperator, stringLength);

            return whiteString.ToString();
        }

        internal static string GetFormattedValue(decimal value, string currencyCode, RequestContext context)
        {
            GetRoundedStringServiceRequest roundingRequest = null;
            GetRoundedStringServiceResponse serviceResponse = null;
            string currencySymbol = string.Empty;

            if (!string.IsNullOrWhiteSpace(currencyCode))
            {
                ChannelDataManager dataManager = new ChannelDataManager(context);
                Currency currency = dataManager.GetCurrencies(currencyCode, new QueryResultSettings()).FirstOrDefault();
                currencySymbol = currency.CurrencySymbol;
            }

            roundingRequest = new GetRoundedStringServiceRequest(
                value,
                currencyCode,
                0,
                false,
                true);
            serviceResponse = context.Execute<GetRoundedStringServiceResponse>(roundingRequest);

            return currencySymbol + serviceResponse.RoundedValue;
        }

        internal static string GetAlignmentSettings(string parsedString, ReceiptItemInfo itemInfo)
        {
            // the indication of a logo is passed on unchanged.
            if (parsedString.Equals(LegacyLogoMessage) || itemInfo.ImageId != 0)
            {
                return parsedString;
            }

            if (parsedString.Length > itemInfo.Length)
            {
                // don't trim strings that contain carrage return, they are considered to be previously handled
                if (!parsedString.Contains(Environment.NewLine))
                {
                    // The value seems to need to be trimmed
                    switch (itemInfo.VerticalAlignment)
                    {
                        case Alignment.Right:
                            parsedString = parsedString.Substring(parsedString.Length - itemInfo.Length, itemInfo.Length);
                            break;
                        default:
                            parsedString = parsedString.Substring(0, itemInfo.Length);
                            break;
                    }
                }
                else
                {
                    // If it contains carriage returns, don't forget to insert indents
                    string indent = Environment.NewLine + CreateWhitespace(' ', itemInfo.CharIndex - 1);
                    parsedString = parsedString.Replace(Environment.NewLine, indent);
                }
            }
            else if (parsedString.Length < itemInfo.Length)
            {
                // The value seems to need to be filled
                int charCountUsableForSpace = itemInfo.Length - CountWordLength(parsedString);
                switch (itemInfo.VerticalAlignment)
                {
                    case Alignment.Left:
                        parsedString = parsedString.PadRight(itemInfo.Length, itemInfo.Fill);
                        break;
                    case Alignment.Center:
                        int spaceOnLeftSide = (int)charCountUsableForSpace / 2;
                        int spaceOnRightSide = charCountUsableForSpace - spaceOnLeftSide;
                        parsedString = parsedString.PadLeft(spaceOnLeftSide + parsedString.Length, itemInfo.Fill);
                        parsedString = parsedString.PadRight(spaceOnRightSide + parsedString.Length, itemInfo.Fill);
                        break;
                    case Alignment.Right:
                        parsedString = parsedString.PadLeft(charCountUsableForSpace + parsedString.Length, itemInfo.Fill);
                        break;
                }
            }

            return parsedString;
        }

        /// <summary>
        /// Get the formatted receipt for email.
        /// </summary>
        /// <param name="request">The request parameter.</param>
        /// <returns>The request response.</returns>
        private static GetEmailReceiptServiceResponse GetEmailReceipt(GetEmailReceiptServiceRequest request)
        {
            SalesOrder salesOrder = request.SalesOrder;
            SalesOrderDataManager dataManager = new SalesOrderDataManager(request.RequestContext);
            List<Receipt> receipts = new List<Receipt>();

            ChannelDataManager channelDataManager = new ChannelDataManager(request.RequestContext);
            OrgUnit store = ReceiptService.GetStoreFromContext(request.RequestContext);
            ReadOnlyCollection<TenderType> tenderTypes = channelDataManager.GetChannelTenderTypes(request.RequestContext.GetPrincipal().ChannelId, new QueryResultSettings());

            foreach (var receiptType in request.ReceiptTypes)
            {
                if (!string.IsNullOrWhiteSpace(store.EmailReceiptProfileId))
                {
                    string receiptLayout = dataManager.GetReceiptLayoutId(receiptType, store.EmailReceiptProfileId, new QueryResultSettings());
                    ReceiptInfo receiptInfo = dataManager.GetReceiptInfo(false, receiptLayout, new QueryResultSettings());

                    if (receiptInfo == null)
                    {
                        throw new DataValidationException(DataValidationErrors.InvalidReceiptTemplate, "The specified receipt layout ({0}) was not valid.", receiptLayout);
                    }

                    switch (receiptType)
                    {
                        // Email is only sent out for sales receipt.
                        case ReceiptType.SalesReceipt:
                            receipts.Add(GetReceiptFromTransaction(tenderTypes, receiptType, receiptLayout, receiptInfo, salesOrder, request.RequestContext));
                            break;
                        default:
                            throw new DataValidationException(DataValidationErrors.ReceiptTypeNotSupported, "Receipt for an invalid receipt type {0} was requested", receiptType.ToString());
                    }
                }
            }

            var response = new GetEmailReceiptServiceResponse(receipts.AsReadOnly());
            return response;
        }

        /// <summary>
        /// Gets the formatted receipt. The logic for bringing the receipt templates and creating the formatted string goes here
        /// this is similar to the FormModulation class in POS.
        /// </summary>
        /// <param name="request">The service request.</param>
        /// <returns>The response containing the set of receipts.</returns>
        private static GetReceiptServiceResponse GetFormattedReceipt(GetReceiptServiceRequest request)
        {
            SalesOrder salesOrder = request.SalesOrder;

            // the request may contain no sales order (e.g. non sales operation receipt)
            if (salesOrder != null)
            {
                salesOrder.TenderLines.Clear();
                salesOrder.TenderLines.AddRange(request.TenderLines);
            }

            SalesOrderDataManager dataManager = new SalesOrderDataManager(request.RequestContext);
            List<Receipt> receipts = new List<Receipt>();

            ChannelDataManager channelDataManager = new ChannelDataManager(request.RequestContext);

            ReadOnlyCollection<TenderType> tenderTypes = channelDataManager.GetChannelTenderTypes(request.RequestContext.GetPrincipal().ChannelId, new QueryResultSettings());

            if (request.IsPreview)
            {
                receipts.Add(GetReceiptForPreview(tenderTypes, salesOrder, request.RequestContext));
            }

            foreach (var receiptType in request.ReceiptTypes)
            {
                IEnumerable<Printer> printers = dataManager.GetPrintersByReceiptType(request.RequestContext.GetPrincipal().TerminalId.ToString(), receiptType, new QueryResultSettings()).GroupBy(p => p.ReceiptLayoutId).Select(l => l.First());

                foreach (Printer printer in printers)
                {
                    ReceiptInfo receiptInfo = dataManager.GetReceiptInfo(false, printer.ReceiptLayoutId, new QueryResultSettings());
                    if (receiptInfo == null)
                    {
                        throw new DataValidationException(DataValidationErrors.InvalidReceiptTemplate, "The specified receipt layout ({0}) was not valid.", printer.ReceiptLayoutId);
                    }

                    switch (receiptType)
                    {
                        case ReceiptType.CardReceiptForShop:
                        case ReceiptType.CardReceiptForCustomer:
                        case ReceiptType.CardReceiptForShopReturn:
                        case ReceiptType.CardReceiptForCustomerReturn:
                            receipts.AddRange(GetReceiptFromCard(tenderTypes, printer, receiptType, printer.ReceiptLayoutId, receiptInfo, salesOrder, request));
                            break;

                        case ReceiptType.CustomerAccountReceiptForShop:
                        case ReceiptType.CustomerAccountReceiptForCustomer:
                        case ReceiptType.CustomerAccountReceiptForShopReturn:
                        case ReceiptType.CustomerAccountReceiptForCustomerReturn:
                        case ReceiptType.CreditMemo:
                            receipts.AddRange(GetReceiptFromTenderLine(tenderTypes, printer, receiptType, printer.ReceiptLayoutId, receiptInfo, salesOrder, request));
                            break;

                        case ReceiptType.SalesReceipt:
                        case ReceiptType.SalesOrderReceipt:
                        case ReceiptType.CustomerAccountDeposit:
                        case ReceiptType.PickupReceipt:
                        case ReceiptType.PackingSlip:
                        case ReceiptType.QuotationReceipt:
                            receipts.Add(GetReceiptFromTransaction(tenderTypes, receiptType, printer.ReceiptLayoutId, receiptInfo, salesOrder, request.RequestContext));
                            break;
                        case ReceiptType.ReturnLabel:
                            receipts.AddRange(GetReturnLables(tenderTypes, receiptType, printer.ReceiptLayoutId, receiptInfo, salesOrder, request.RequestContext));
                            break;
                        default:
                            throw new DataValidationException(DataValidationErrors.ReceiptTypeNotSupported, "Receipt for an invalid receipt type {0} was requested", receiptType.ToString());
                    }
                }

                // Check if the receipt is for a hardcoded template
                switch (receiptType)
                {
                    case ReceiptType.SafeDrop:
                        receipts.Add(GetReceiptForSafeDrop(request.DropAndDeclareTransaction, request.RequestContext));
                        break;
                    case ReceiptType.BankDrop:
                        receipts.Add(GetReceiptForBankDrop(request.DropAndDeclareTransaction, request.RequestContext));
                        break;
                    case ReceiptType.TenderDeclaration:
                        receipts.Add(GetReceiptForTenderDeclaration(request.DropAndDeclareTransaction, request.RequestContext));
                        break;
                    case ReceiptType.RemoveTender:
                        receipts.Add(GetReceiptForRemoveTender(request.NonSalesTenderTransaction, request.RequestContext));
                        break;
                    case ReceiptType.FloatEntry:
                        receipts.Add(GetReceiptForFloatEntry(request.NonSalesTenderTransaction, request.RequestContext));
                        break;
                    case ReceiptType.StartingAmount:
                        receipts.Add(GetReceiptForStartingAmount(request.NonSalesTenderTransaction, request.RequestContext));
                        break;
                    case ReceiptType.XReport:
                    case ReceiptType.ZReport:
                        receipts.Add(GetReceiptForXZReport(request.ShiftDetails, receiptType, request.RequestContext));
                        break;
                    case ReceiptType.GiftCertificate:
                        GetAllGiftCardReceipts(receipts, salesOrder, request.RequestContext);
                        break;
                    default:
                        // Do nothing on default.
                        break;
                }
            }

            var response = new GetReceiptServiceResponse(receipts.AsReadOnly());
            return response;
        }

        private static Receipt GetReceiptForPreview(ReadOnlyCollection<TenderType> tenderTypes, SalesOrder salesOrder, RequestContext context)
        {
            HardwareProfileDataManager hardwareProfileDataManager = new HardwareProfileDataManager(context);
            SalesOrderDataManager dataManager = new SalesOrderDataManager(context);

            Terminal terminal = context.GetTerminal();
            HardwareProfile hardwareProfile = hardwareProfileDataManager.GetHardwareProfile(terminal.HardwareProfile);

            // The hardware profile should return two pritners with DeviceType set to None in case no printers are configured in Ax.
            if (hardwareProfile.Printers.Any())
            {
                string receiptLayoutId = dataManager.GetReceiptLayoutId(ReceiptType.SalesReceipt, hardwareProfile.Printers.First().ReceiptProfileId, new QueryResultSettings());
                ReceiptInfo receiptInfo = dataManager.GetReceiptInfo(false, receiptLayoutId, new QueryResultSettings());
                return GetReceiptFromTransaction(tenderTypes, ReceiptType.SalesReceipt, receiptLayoutId, receiptInfo, salesOrder, context);
            }
            else
            {
                throw new DataValidationException(DataValidationErrors.NoPrintersReturned, "A receipt profile identifer could not be retrieved from the hardware profile.");
            }
        }

        /// <summary>
        /// Get receipts for all gift cards. Each gift card has a separate receipt that needs to be printed.
        /// </summary>
        /// <param name="receipts">The collection of receipts.</param>
        /// <param name="salesOrder">The sales order.</param>
        /// <param name="context">The request context.</param>
        private static void GetAllGiftCardReceipts(List<Receipt> receipts, SalesOrder salesOrder, RequestContext context)
        {
            foreach (SalesLine line in salesOrder.SalesLines)
            {
                if (line.IsGiftCardLine && !line.IsVoided)
                {
                    var serviceRequest = new GetGiftCardServiceRequest(line.Comment);

                    GetGiftCardServiceResponse serviceResponse = context.Execute<GetGiftCardServiceResponse>(serviceRequest);

                    receipts.Add(GetReceiptForGiftCard(salesOrder, serviceResponse.GiftCard, context));
                }
            }
        }

        /// <summary>
        /// Get formatted receipt for a single gift card.
        /// </summary>
        /// <param name="salesOrder">The sales order.</param>
        /// <param name="giftCard">The gift card.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The formatted receipt.</returns>
        private static Receipt GetReceiptForGiftCard(SalesOrder salesOrder, GiftCard giftCard, RequestContext context)
        {
            StringBuilder reportLayout = new StringBuilder();
            Receipt receipt = new Receipt();
            string currency = context.GetOrgUnit().Currency;

            ReceiptHelper.PrepareGiftCardHeader(reportLayout, salesOrder);
            reportLayout.AppendLine(SingleLine);
            reportLayout.AppendLine();
            receipt.Header = reportLayout.ToString();

            reportLayout.Clear();
            reportLayout.AppendLine(ReceiptHelper.FormatTenderLine("<T:string_6100>.", giftCard.Id));
            reportLayout.AppendLine(ReceiptHelper.FormatTenderLine(
                "<T:string_6101>",
                GetFormattedValue(giftCard.Balance, currency, context)));
            reportLayout.AppendLine();
            reportLayout.AppendLine(DoubleLine);
            reportLayout.AppendLine();
            receipt.Footer = reportLayout.ToString();

            receipt.Printers = GetAllPrintersForTerminal(context);
            return receipt;
        }

        /// <summary>
        /// Get the receipt for safe drop.
        /// </summary>
        /// <param name="transaction">The drop and declare transaction.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The safe drop receipt.</returns>
        private static Receipt GetReceiptForSafeDrop(DropAndDeclareTransaction transaction, RequestContext context)
        {
            StringBuilder reportLayout = new StringBuilder();
            Receipt receipt = new Receipt();

            ReceiptHelper.PrepareReceiptHeader("<T:string_6148>", reportLayout, transaction);
            reportLayout.AppendLine(string.Empty.PadLeft(55, '-'));
            receipt.Header = reportLayout.ToString();

            reportLayout.Clear();
            PrepareDropAndDeclareTenders(reportLayout, transaction, context);
            reportLayout.AppendLine(string.Empty.PadLeft(55, '='));
            reportLayout.AppendLine();
            receipt.Body = reportLayout.ToString();
            receipt.Printers = GetAllPrintersForTerminal(context);
            return receipt;
        }

        /// <summary>
        /// Get the bank drop receipt.
        /// </summary>
        /// <param name="transaction">The bank drop transaction.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The formatted receipt.</returns>
        private static Receipt GetReceiptForBankDrop(DropAndDeclareTransaction transaction, RequestContext context)
        {
            StringBuilder reportLayout = new StringBuilder();
            Receipt receipt = new Receipt();

            ReceiptHelper.PrepareReceiptHeader("<T:string_6151>", reportLayout, transaction);

            string bagId = string.Empty;
            if (transaction.TenderDetails != null && transaction.TenderDetails.Count > 0)
            {
                bagId = transaction.TenderDetails.First().BankBagNumber;
            }

            reportLayout.AppendLine(ReceiptHelper.FormatHeaderLine("<T:string_6102>", bagId ?? string.Empty, true));
            reportLayout.AppendLine(SingleLine);
            receipt.Header = reportLayout.ToString();

            reportLayout.Clear();
            PrepareDropAndDeclareTenders(reportLayout, transaction, context);
            reportLayout.AppendLine(DoubleLine);
            reportLayout.AppendLine();
            receipt.Body = reportLayout.ToString();

            receipt.Printers = GetAllPrintersForTerminal(context);
            return receipt;
        }

        /// <summary>
        /// Get receipt for tender declaration.
        /// </summary>
        /// <param name="transaction">The tender declaration transaction.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The formatted receipt.</returns>
        private static Receipt GetReceiptForTenderDeclaration(DropAndDeclareTransaction transaction, RequestContext context)
        {
            Receipt receipt = new Receipt();
            StringBuilder reportLayout = new StringBuilder();

            ReceiptHelper.PrepareReceiptHeader("<T:string_6154>", reportLayout, transaction);
            reportLayout.AppendLine(SingleLine);
            receipt.Header = reportLayout.ToString();

            reportLayout.Clear();
            PrepareDropAndDeclareTenders(reportLayout, transaction, context);
            reportLayout.AppendLine(DoubleLine);
            reportLayout.AppendLine();
            receipt.Body = reportLayout.ToString();

            receipt.Printers = GetAllPrintersForTerminal(context);
            return receipt;
        }

        /// <summary>
        /// Get receipt for tender removal.
        /// </summary>
        /// <param name="transaction">The remove tender transaction.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The formatted receipt.</returns>
        private static Receipt GetReceiptForRemoveTender(NonSalesTransaction transaction, RequestContext context)
        {
            Receipt receipt = new Receipt();
            StringBuilder reportLayout = new StringBuilder();
            ReceiptHelper.PrepareReceiptHeader("<T:string_6152>", reportLayout, transaction);
            reportLayout.AppendLine(SingleLine);
            receipt.Header = reportLayout.ToString();

            reportLayout.Clear();
            reportLayout.AppendLine();
            reportLayout.AppendLine(ReceiptHelper.FormatTenderLine("<T:string_6103>", GetFormattedValue(transaction.Amount, transaction.ForeignCurrency, context)));

            reportLayout.AppendLine(transaction.Description.ToString());
            reportLayout.AppendLine();
            reportLayout.AppendLine(DoubleLine);
            reportLayout.AppendLine();
            receipt.Body = reportLayout.ToString();

            receipt.Printers = GetAllPrintersForTerminal(context);
            return receipt;
        }

        /// <summary>
        /// Get receipt for float entry.
        /// </summary>
        /// <param name="transaction">The float entry transaction.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The formatted receipt.</returns>
        private static Receipt GetReceiptForFloatEntry(NonSalesTransaction transaction, RequestContext context)
        {
            StringBuilder reportLayout = new StringBuilder();
            Receipt receipt = new Receipt();

            ReceiptHelper.PrepareReceiptHeader("<T:string_6153>", reportLayout, transaction);
            reportLayout.AppendLine(SingleLine);

            reportLayout.AppendLine();
            reportLayout.AppendLine(ReceiptHelper.FormatTenderLine("<T:string_6104>", GetFormattedValue(transaction.Amount, transaction.ForeignCurrency, context)));
            reportLayout.AppendLine(transaction.Description.ToString());
            receipt.Header = reportLayout.ToString();
            reportLayout.Clear();
            reportLayout.AppendLine();
            reportLayout.AppendLine(DoubleLine);
            reportLayout.AppendLine();
            receipt.Body = reportLayout.ToString();

            receipt.Printers = GetAllPrintersForTerminal(context);
            return receipt;
        }

        /// <summary>
        /// Get receipt for declaring starting amount.
        /// </summary>
        /// <param name="transaction">The float entry transaction.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The formatted receipt.</returns>
        private static Receipt GetReceiptForStartingAmount(NonSalesTransaction transaction, RequestContext context)
        {
            StringBuilder reportLayout = new StringBuilder();
            Receipt receipt = new Receipt();

            ReceiptHelper.PrepareReceiptHeader("<T:string_6155>", reportLayout, transaction);
            reportLayout.AppendLine(SingleLine);
            receipt.Header = reportLayout.ToString();

            reportLayout.Clear();
            reportLayout.AppendLine();
            reportLayout.AppendLine(ReceiptHelper.FormatTenderLine("<T:string_6105>", GetFormattedValue(transaction.Amount, transaction.ForeignCurrency, context)));

            reportLayout.AppendLine(transaction.Description.ToString());
            reportLayout.AppendLine();
            reportLayout.AppendLine(DoubleLine);
            reportLayout.AppendLine();
            receipt.Body = reportLayout.ToString();

            receipt.Printers = GetAllPrintersForTerminal(context);
            return receipt;
        }

        /// <summary>
        /// Get all printers for a terminal. Used for hardcoded templates.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>A collection of printers.</returns>
        private static ReadOnlyCollection<Printer> GetAllPrintersForTerminal(RequestContext context)
        {
            SalesOrderDataManager dataManager = new SalesOrderDataManager(context);
            return dataManager.GetPrintersByTerminal(context.GetPrincipal().TerminalId.ToString(), new QueryResultSettings()).GroupBy(p => p.Name).Select(l => l.First()).AsReadOnly();
        }

        /// <summary>
        /// Get receipt for X or Z report.
        /// </summary>
        /// <param name="shift">The current shift.</param>
        /// <param name="receiptType">The receipt type.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The formatted receipt.</returns>
        private static Receipt GetReceiptForXZReport(Shift shift, ReceiptType receiptType, RequestContext context)
        {
            // TextID's for the Z/X Report are reserved at 7000 - 7099
            StringBuilder reportLayout = new StringBuilder(2500);
            Receipt receipt = new Receipt();

            // Header
            PrepareHeader(reportLayout, shift, receiptType, context);
            receipt.Header = reportLayout.ToString();
            reportLayout.Clear();
            string currency = context.GetOrgUnit().Currency;

            // Total Amounts
            ReceiptHelper.AppendReportLine(reportLayout, "<T:string_6106>");
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6107>", GetFormattedValue(shift.SalesTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6108>", GetFormattedValue(shift.ReturnsTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6109>", GetFormattedValue(shift.TaxTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6110>", GetFormattedValue(shift.DiscountTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6111>", GetFormattedValue(shift.RoundedAmountTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6112>", GetFormattedValue(shift.PaidToAccountTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6113>", GetFormattedValue(shift.IncomeAccountTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6114>", GetFormattedValue(shift.ExpenseAccountTotal, currency, context));
            reportLayout.AppendLine();

            // Statistics
            ReceiptHelper.AppendReportLine(reportLayout, "<T:string_6115>");
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6107>", shift.SaleTransactionCount);
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6116>", shift.CustomerCount);
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6117>", shift.VoidTransactionCount);
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6118>", shift.LogOnTransactionCount);
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6119>", shift.NoSaleTransactionCount);

            if (receiptType == ReceiptType.XReport)
            {
                ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6120>", shift.SuspendedTransactionCount);
            }

            reportLayout.AppendLine();

            // Tender totals
            ReceiptHelper.AppendReportLine(reportLayout, "<T:string_6121>");
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6122>", GetFormattedValue(shift.TenderedTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6123>", GetFormattedValue(shift.ChangeTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6105>", GetFormattedValue(shift.StartingAmountTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6124>", GetFormattedValue(shift.FloatingEntryAmountTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6125>", GetFormattedValue(shift.RemoveTenderAmountTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6126>", GetFormattedValue(shift.BankDropTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6127>", GetFormattedValue(shift.SafeDropTotal, currency, context));
            ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6128>", GetFormattedValue(shift.DeclareTenderAmountTotal, currency, context));

            bool amountShort = shift.OverShortTotal < 0;

            ReceiptHelper.AppendReportLine(reportLayout, amountShort ? "<F:string_6129>" : "<F:string_6130>", GetFormattedValue(amountShort ? decimal.Negate(shift.OverShortTotal) : shift.OverShortTotal, currency, context));
            reportLayout.AppendLine();

            // Income/Expense
            if (shift.AccountLines.Count > 0)
            {
                ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6131>/<F:string_6132>");
                foreach (ShiftAccountLine accountLine in shift.AccountLines.OrderBy(a => a.AccountType))
                {
                    string typeResourceId = string.Empty;

                    switch (accountLine.AccountType)
                    {
                        case IncomeExpenseAccountType.Income:
                            typeResourceId = "<F:string_6131>";
                            break;

                        case IncomeExpenseAccountType.Expense:
                            typeResourceId = "<F:string_6132>";
                            break;

                        default:
                            string message = string.Format("Unsupported account Type '{0}'.", accountLine.AccountType);
                            throw new NotSupportedException(message);
                    }

                    ReceiptHelper.AppendReportLine(reportLayout, string.Format(TypeFormat, accountLine.AccountNumber, typeResourceId), GetFormattedValue(accountLine.Amount, currency, context));
                }

                reportLayout.AppendLine();
            }

            // Tenders
            if (receiptType == ReceiptType.ZReport && shift.TenderLines.Count > 0)
            {
                ReceiptHelper.AppendReportLine(reportLayout, "<T:string_6133>");
                foreach (ShiftTenderLine tenderLine in shift.TenderLines.OrderBy(t => t.TenderTypeName))
                {
                    string formatedTenderName = tenderLine.TenderTypeName;

                    if (currency != tenderLine.TenderCurrency)
                    {
                        formatedTenderName = string.Format(CurrencyFormat, tenderLine.TenderTypeName, tenderLine.TenderCurrency);
                    }

                    ReceiptHelper.AppendReportLine(reportLayout, string.Format(TypeFormat, formatedTenderName, "<F:string_6124>"), GetFormattedValue(tenderLine.AddToTenderAmountOfTenderCurrency, tenderLine.TenderCurrency, context));
                    ReceiptHelper.AppendReportLine(reportLayout, string.Format(TypeFormat, formatedTenderName, "<F:string_6134>"), GetFormattedValue(tenderLine.ShiftAmountOfTenderCurrency, tenderLine.TenderCurrency, context));
                    ReceiptHelper.AppendReportLine(reportLayout, string.Format(TypeFormat, formatedTenderName, "<F:string_6125>"), GetFormattedValue(tenderLine.TotalRemovedFromTenderAmountOfTenderCurrency, tenderLine.TenderCurrency, context));

                    if (tenderLine.CountingRequired)
                    {
                        ReceiptHelper.AppendReportLine(reportLayout, string.Format(TypeFormat, formatedTenderName, "<F:string_6128>"), GetFormattedValue(tenderLine.DeclareTenderAmountOfTenderCurrency, tenderLine.TenderCurrency, context));

                        amountShort = tenderLine.OverShortAmountOfTenderCurrency < 0;

                        ReceiptHelper.AppendReportLine(reportLayout, string.Format(TypeFormat, formatedTenderName, amountShort ? "<F:string_6129>" : "<F:string_6130>"), GetFormattedValue(amountShort ? decimal.Negate(tenderLine.OverShortAmountOfTenderCurrency) : tenderLine.OverShortAmountOfTenderCurrency, tenderLine.TenderCurrency, context));
                    }

                    ReceiptHelper.AppendReportLine(reportLayout, string.Format(TypeFormat, formatedTenderName, "<F:string_6135>"), tenderLine.Count);

                    reportLayout.AppendLine();
                }
            }

            reportLayout.AppendLine();
            reportLayout.AppendLine();
            reportLayout.AppendLine();

            receipt.Body = reportLayout.ToString();

            receipt.Printers = GetAllPrintersForTerminal(context);
            return receipt;
        }

        /// <summary>
        /// Prepare the tender part of receipt.
        /// </summary>
        /// <param name="reportLayout">The receipt string.</param>
        /// <param name="transaction">The drop and declare transaction.</param>
        /// <param name="context">The request context.</param>
        private static void PrepareDropAndDeclareTenders(StringBuilder reportLayout, DropAndDeclareTransaction transaction, RequestContext context)
        {
            string tenderName = string.Empty;
            string amount = string.Empty;

            reportLayout.AppendLine();

            string storeCurrency = context.GetOrgUnit().Currency;

            foreach (TenderDetail tenderLine in transaction.TenderDetails)
            {
                if (tenderLine.ForeignCurrency == storeCurrency)
                {
                    // Tenders in the store currency
                    tenderName = tenderLine.EntityName;
                    amount = GetFormattedValue(tenderLine.Amount, tenderLine.ForeignCurrency, context);
                }
                else
                {
                    // Foreign currency {Currency - CAD}
                    tenderName = string.Format(ForiegnCurrencyFormat, tenderLine.EntityName, tenderLine.ForeignCurrency);
                    amount = GetFormattedValue(tenderLine.AmountInForeignCurrency, tenderLine.ForeignCurrency, context);
                }

                // {Credit Card:......$50}
                reportLayout.AppendLine(ReceiptHelper.FormatTenderLine(tenderName, amount));
            }

            reportLayout.AppendLine();
        }

        /// <summary>
        /// Prepare report header.
        /// </summary>
        /// <param name="reportLayout">The receipt string.</param>
        /// <param name="shift">The current shift.</param>
        /// <param name="receiptType">The receipt type to print.</param>
        /// <param name="context">The request context.</param>
        private static void PrepareHeader(StringBuilder reportLayout, Shift shift, ReceiptType receiptType, RequestContext context)
        {
            // check for nulls
            ThrowIf.Null(context, "requestContext");
            ThrowIf.Null(shift, "shift");

            // get channel configuration
            ChannelConfiguration channelConfiguration = context.GetChannelConfiguration();
            ThrowIf.Null(channelConfiguration, "context.GetChannelConfiguration()");

            // convert start and end date / time in channel date / time
            string shiftStartDate = string.Empty;
            string shiftStartTime = string.Empty;
            string shiftCloseDate = string.Empty;
            string shiftCloseTime = string.Empty;

            if (shift.StartDateTime.HasValue)
            {
                // get shift's startdatetime in channel datetimeoffset
                var shiftChannelStartDateTimeOffset =
                    channelConfiguration.TimeZoneRecords.GetChannelDateTimeOffset(shift.StartDateTime.Value);

                if (receiptType == ReceiptType.ZReport)
                {
                    var shiftStartDateTime = shiftChannelStartDateTimeOffset.DateTime;
                    shiftStartDate = shiftStartDateTime.ToString("d");
                    shiftStartTime = shiftStartDateTime.ToString("T");
                }
                else
                {
                    shiftStartDate = shift.StartDateTime.Value.DateTime.ToString("d");
                    shiftStartTime = shift.StartDateTime.Value.DateTime.ToString("T");
                }
            }

            if (shift.CloseDateTime.HasValue)
            {
                // get shift's closedatetime in channel datetimeoffset
                var shiftChannelCloseDateTimeOffset =
                    channelConfiguration.TimeZoneRecords.GetChannelDateTimeOffset(shift.CloseDateTime.Value);

                var shiftCloseDateTime = shiftChannelCloseDateTimeOffset.DateTime;
                shiftCloseDate = shiftCloseDateTime.ToString("d");
                shiftCloseTime = shiftCloseDateTime.ToString("T");
            }

            string staffId = string.Empty;

            reportLayout.AppendLine(SingleLine);

            switch (receiptType)
            {
                case ReceiptType.XReport:
                    staffId = context.GetPrincipal().UserId;     // for X report, uses the current staff identifier
                    ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6136>");
                    break;

                case ReceiptType.ZReport:
                    staffId = shift.StaffId;     // for Z report, uses the staff identifier from the RETAILPOSBATCHTABLE
                    ReceiptHelper.AppendReportLine(reportLayout, "<F:string_6137>");
                    break;

                default:
                    string message = string.Format("Unsupported Report Type '{0}'.", receiptType);
                    throw new NotSupportedException(message);
            }

            ReceiptHelper.AppendReportHeaderLine(reportLayout, "<F:string_6138>", shift.StoreId, true);
            ReceiptHelper.AppendReportHeaderLine(reportLayout, "<F:string_6139>", DateTime.Now.ToString("d"), false);
            ReceiptHelper.AppendReportHeaderLine(reportLayout, "<F:string_6140>", shift.TerminalId, true);
            ReceiptHelper.AppendReportHeaderLine(reportLayout, "<F:string_6141>", DateTime.Now.ToString("T"), false);
            ReceiptHelper.AppendReportHeaderLine(reportLayout, "<F:string_6142>", staffId, true);
            reportLayout.AppendLine();
            reportLayout.AppendLine();
            ReceiptHelper.AppendReportHeaderLine(reportLayout, "<F:string_6143>", string.Format(ReceiptService.LineFormat, shift.TerminalId, shift.ShiftId), true);
            reportLayout.AppendLine();

            ReceiptHelper.AppendReportHeaderLine(reportLayout, "<F:string_6144>", shiftStartDate, true);

            if (receiptType == ReceiptType.ZReport)
            {
                ReceiptHelper.AppendReportHeaderLine(reportLayout, "<F:string_6145>", shiftCloseDate, false);
            }
            else
            {
                reportLayout.AppendLine();
            }

            ReceiptHelper.AppendReportHeaderLine(reportLayout, "<F:string_6146>", shiftStartTime, true);

            if (receiptType == ReceiptType.ZReport)
            {
                ReceiptHelper.AppendReportHeaderLine(reportLayout, "<F:string_6147>", shiftCloseTime, false);
            }
            else
            {
                reportLayout.AppendLine();
            }

            reportLayout.AppendLine();
        }

        private static ReadOnlyCollection<Receipt> GetReturnLables(ReadOnlyCollection<TenderType> tenderTypes, ReceiptType receiptType, string receiptLayoutId, ReceiptInfo receiptInfo, SalesOrder salesOrder, RequestContext context)
        {
            if (receiptInfo == null)
            {
                throw new ArgumentNullException("receiptInfo");
            }

            List<Receipt> returnLables = new List<Receipt>();

            foreach (SalesLine line in salesOrder.SalesLines)
            {
                if (line.IsReturnByReceipt || line.Quantity < 0)
                {
                    Receipt receipt = new Receipt();

                    // Getting a dataset containing the headerpart of the current receipt
                    DataSet ds = ConvertToDataSet(receiptInfo.HeaderTemplate, receiptInfo);
                    receipt.Header += ReadTemplateDataset(ds, tenderTypes, null, salesOrder, context);
                    receiptInfo.HeaderLines = ds.Tables[0].Rows.Count;

                    // Getting a dataset containing the footerpart of the current receipt
                    ds = ConvertToDataSet(receiptInfo.FooterTemplate, receiptInfo);
                    receipt.Footer = ReadTemplateDataset(ds, tenderTypes, null, salesOrder, context);
                    receiptInfo.FooterLines = ds.Tables[0].Rows.Count;

                    for (int i = 0; i < Math.Abs(line.Quantity); ++i)
                    {
                        ds = ConvertToDataSet(receiptInfo.BodyTemplate, receiptInfo);

                        // Getting a dataset containing the linepart of the current receipt
                        receipt.Body = ReadItemDataSet(ds, line, salesOrder, context);
                        receiptInfo.Bodylines = ds.Tables[0].Rows.Count;

                        PopulateReceiptData(receipt, receiptType, receiptLayoutId, receiptInfo, salesOrder, context);
                        returnLables.Add(receipt);
                    }

                    ds.Dispose();
                }
            }

            return returnLables.AsReadOnly();
        }

        private static Receipt GetReceiptFromTransaction(ReadOnlyCollection<TenderType> tenderTypes, ReceiptType receiptType, string receiptLayoutId, ReceiptInfo receiptInfo, SalesOrder salesOrder, RequestContext context)
        {
            Receipt receipt = null;
            receipt = GetTransformedTransaction(tenderTypes, receiptInfo, salesOrder, context);
            PopulateReceiptData(receipt, receiptType, receiptLayoutId, receiptInfo, salesOrder, context);

            return receipt;
        }

        private static List<Receipt> GetReceiptFromCard(ReadOnlyCollection<TenderType> tenderTypes, Printer printer, ReceiptType receiptType, string receiptLayoutId, ReceiptInfo receiptInfo, SalesOrder salesOrder, GetReceiptServiceRequest request)
        {
            Receipt receipt = null;
            List<Receipt> receipts = new List<Receipt>();

            foreach (var tenderLine in request.TenderLines)
            {
                TenderType tenderType = tenderTypes.Where(type => type.TenderTypeId == tenderLine.TenderTypeId).Single();

                if (tenderType.OperationId == (int)RetailOperation.PayCard)
                {
                    receipt = GetTransformedCardTender(receiptInfo, tenderLine, tenderType, salesOrder, request);
                    PopulateReceiptData(receipt, receiptType, receiptLayoutId, receiptInfo, salesOrder, request.RequestContext);
                    receipts.Add(receipt);
                }
            }

            return receipts;
        }

        private static List<Receipt> GetReceiptFromTenderLine(ReadOnlyCollection<TenderType> tenderTypes, Printer printer, ReceiptType receiptType, string receiptLayoutId, ReceiptInfo receiptInfo, SalesOrder salesOrder, GetReceiptServiceRequest request)
        {
            Receipt receipt = null;
            List<Receipt> receipts = new List<Receipt>();

            foreach (var tenderLine in request.TenderLines)
            {
                IEnumerable<TenderType> tenderTypesForId = tenderTypes.Where(type => type.TenderTypeId == tenderLine.TenderTypeId);

                if (tenderTypesForId.Count() != 1)
                {
                    throw new DataValidationException(DataValidationErrors.MoreThanOneTenderTypeForTenderTypeId, "More than one tender type was returned for tender type id {0}", tenderLine.TenderTypeId.ToString());
                }

                TenderType tenderType = tenderTypesForId.First();

                RetailOperation operation = (RetailOperation)tenderType.OperationId;

                switch (operation)
                {
                    case RetailOperation.PayCustomerAccount:
                        {
                            receipt = GetTransformedTender(receiptInfo, salesOrder, tenderLine, request);
                            PopulateReceiptData(receipt, receiptType, receiptLayoutId, receiptInfo, salesOrder, request.RequestContext);
                            receipts.Add(receipt);
                        }

                        break;
                    case RetailOperation.PayCreditMemo:
                        {
                            if (tenderLine.Amount <= 0)
                            {
                                receipt = GetTransformedTender(receiptInfo, salesOrder, tenderLine, request);
                                PopulateReceiptData(receipt, receiptType, receiptLayoutId, receiptInfo, salesOrder, request.RequestContext);
                                receipts.Add(receipt);
                            }
                        }

                        break;
                    default:
                        // Nothing else needs to be done for other tender types except credit cards which is handled separately. So do nothing.
                        break;
                }
            }

            return receipts;
        }

        private static void PopulateReceiptData(Receipt receipt, ReceiptType receiptType, string receiptLayoutId, ReceiptInfo receiptInfo, SalesOrder salesOrder, RequestContext context)
        {
            SalesOrderDataManager dataManager = new SalesOrderDataManager(context);
            ReadOnlyCollection<Printer> printersForReceipt = dataManager.GetPrintersByLayoutId(context.GetPrincipal().TerminalId.ToString(), receiptLayoutId, new QueryResultSettings());

            receipt.Printers = printersForReceipt;
            receipt.TransactionId = salesOrder.Id;
            receipt.ReceiptId = salesOrder.ReceiptId;
            receipt.ReceiptType = receiptType;
            receipt.ReceiptTitle = receiptInfo.Title;
            receipt.LayoutId = receiptLayoutId;
        }

        /// <summary>
        /// Reads all the sales line in a sales order for receipt printing.
        /// </summary>
        /// <param name="receiptDataset">The dataset.</param>
        /// <param name="theTransaction">The sales order or transaction.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The string in printable format.</returns>
        private static string ReadItemDataSet(DataSet receiptDataset, SalesOrder theTransaction, RequestContext context)
        {
            ReceiptItemInfo itemInfo = null;
            StringBuilder lineStringBuilder = new StringBuilder(string.Empty);

            // Go through the sale items and parse each line
            if (theTransaction != null)
            {
                foreach (SalesLine saleLineItem in theTransaction.SalesLines)
                {
                    ParseSaleItem(receiptDataset, out itemInfo, lineStringBuilder, saleLineItem, theTransaction, context);
                }
            }

            return lineStringBuilder.ToString();
        }

        private static string ReadItemDataSet(DataSet receiptDataset, SalesLine salesLine, SalesOrder theTransaction, RequestContext context)
        {
            ReceiptItemInfo itemInfo = null;
            StringBuilder lineStringBuilder = new StringBuilder(string.Empty);

            // Go through the sale items and parse each line
            if (salesLine != null)
            {
                ParseSaleItem(receiptDataset, out itemInfo, lineStringBuilder, salesLine, theTransaction, context);
            }

            return lineStringBuilder.ToString();
        }

        /// <summary>
        /// Determines the value of a sales line item variable.
        /// </summary>
        /// <param name="ds">The dataset.</param>
        /// <param name="itemInfo">Receipt properties.</param>
        /// <param name="lineStringBuilder">The string that contains the receipt variables.</param>
        /// <param name="saleLineItem">The sales line.</param>
        /// <param name="salesOrder">The sales order.</param>
        /// <param name="context">The request context.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "By design."), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "By design.")]
        private static void ParseSaleItem(DataSet ds, out ReceiptItemInfo itemInfo, StringBuilder lineStringBuilder, SalesLine saleLineItem, SalesOrder salesOrder, RequestContext context)
        {
            string variable = string.Empty;
            string lineId = string.Empty;
            itemInfo = null;

            // Only non-voided items will be printed
            if (!saleLineItem.IsVoided)
            {
                DataTable lineTable = ds.Tables.Where(p => p.TableName == "line").Single();
                DataTable charPosTable;

                foreach (DataRow dr in lineTable.Rows.Where(p => p != null).OrderBy(p => p["nr"]))
                {
                    variable = dr["ID"].ToString();
                    switch (variable)
                    {
                        case CRLF:
                            {
                                lineStringBuilder.Append(Environment.NewLine);
                                break;
                            }

                        default:
                            {
                                lineId = dr[LineId].ToString();

                                if (variable.Equals("PharmacyLine"))
                                {
                                    /*donothing*/
                                }
                                else if (variable.Equals("TotalDiscount") && (saleLineItem.TotalDiscount == 0))
                                {
                                    /*donothing*/
                                }
                                else if (variable.Equals("LineDiscount") && (saleLineItem.LineDiscount == 0))
                                {
                                    /*donothing*/
                                }
                                else if (variable.Equals("PeriodicDiscount") && (saleLineItem.PeriodicDiscount == 0))
                                {
                                    /*donothing*/
                                }
                                else if (variable.Equals("Dimension")
                                    && string.IsNullOrEmpty(saleLineItem.Variant.Color)
                                    && string.IsNullOrEmpty(saleLineItem.Variant.Size)
                                    && string.IsNullOrEmpty(saleLineItem.Variant.Style)
                                    && string.IsNullOrEmpty(saleLineItem.Variant.Configuration))
                                {
                                    /*donothing*/
                                }
                                else if (variable.Equals("Comment") && string.IsNullOrEmpty(saleLineItem.Comment))
                                {
                                    /*donothing*/
                                }
                                else if (variable.Equals("KitComponentName"))
                                {
                                    var dataRequest = new GetDeviceConfigurationDataServiceRequest(context.GetPrincipal().ChannelId, context.GetTerminal().TerminalId);
                                    DeviceConfiguration deviceConfiguration = context.Runtime.Execute<SingleEntityDataServiceResponse<DeviceConfiguration>>(dataRequest, context).Entity;

                                    // Only parse kit components if functionality profile indicates that components should be printed
                                    if (deviceConfiguration.IncludeKitComponents)
                                    {
                                        // Get the product entity for the sale line item
                                        ProductDataManager productManager = new ProductDataManager(context);
                                        List<long> productIds = new List<long>();
                                        productIds.Add(saleLineItem.ProductId);
                                        ProductSearchCriteria queryCriteria = new ProductSearchCriteria(context.GetPrincipal().ChannelId);
                                        QueryResultSettings querySettings = new QueryResultSettings();
                                        queryCriteria.DataLevel = CommerceEntityDataLevel.Complete;
                                        queryCriteria.Ids = productIds;
                                        Product kitProduct = context.Runtime.Execute<ProductSearchServiceResponse>(
                                            new ProductSearchServiceRequest(queryCriteria, querySettings), context).ProductSearchResult.Results.FirstOrDefault();

                                        // Parse kit variables if item is a kit and if functionality profile indicates that components should be printed
                                        if (kitProduct != null && kitProduct.IsKit)
                                        {
                                            // Get kit component information for this variant of the kit product
                                            var kitVariantToProductMap = kitProduct.CompositionInformation.KitDefinition.IndexedKitVariantToComponentMap[saleLineItem.ProductId];
                                            var kitComponentKeys = kitVariantToProductMap.KitComponentKeyList;

                                            // For each component in the kit, parse the kit component data and append the line data to lineStringBuilder
                                            foreach (KitComponentKey componentKey in kitComponentKeys)
                                            {
                                                KitLineDefinition kitLine = kitProduct.CompositionInformation.KitDefinition.KitLineDefinitions.First(k => k.KitLineIdentifier == componentKey.KitLineIdentifier);

                                                ParseKitComponent(lineStringBuilder, ds, lineTable, lineId, kitLine, componentKey, salesOrder.IsTaxIncludedInPrice, context);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    // options for idVariable:
                                    // Itemlines
                                    // TotalDiscount
                                    // LineDiscount
                                    charPosTable = ds.Tables.Where(p => p.TableName == CharPos).Single();
                                    if (charPosTable != null)
                                    {
                                        int nextCharNr = 1;
                                        foreach (DataRow row in charPosTable.Rows.Where(p => p[lineTable.TableName + "_id"].ToString() == lineId).OrderBy(p => p["nr"]))
                                        {
                                            itemInfo = new ReceiptItemInfo(row);

                                            // Adding possible whitespace at the beginning of line
                                            lineStringBuilder.Append(CreateWhitespace(' ', itemInfo.CharIndex - nextCharNr));

                                            if (itemInfo.FontStyle == FontStyle.Bold)
                                            {
                                                lineStringBuilder.Append(Esc + DoubleSpace);
                                            }
                                            else
                                            {
                                                lineStringBuilder.Append(Esc + SingleSpace);
                                            }

                                            // Parsing the itemInfo
                                            string itemVariable = ParseItemVariable(itemInfo, saleLineItem, salesOrder.IsTaxIncludedInPrice, context);
                                            lineStringBuilder.Append(itemVariable);

                                            // Closing the string with a single space command to make sure spaces are always single spaced
                                            lineStringBuilder.Append(Esc + SingleSpace);

                                            // Specifing the position of the next char in the current line - bold take twice as much space
                                            nextCharNr = itemInfo.CharIndex + (itemInfo.Length * itemInfo.SizeFactor);
                                        }
                                    }

                                    lineStringBuilder.Append(Environment.NewLine);
                                }

                                break;
                            }
                    }
                }
            }
        }

        /// <summary>
        /// Parses a kit component for printing.
        /// </summary>
        /// <param name="lineStringBuilder">The string that contains the receipt variables.</param>
        /// <param name="ds">The dataset.</param>
        /// <param name="lineTable">The line table.</param>
        /// <param name="lineId">The line id.</param>
        /// <param name="kitLineDefinition">The kit line definition for the kit component.</param>
        /// <param name="componentKey">The kit component key.</param>
        /// <param name="isTaxIncludedInPrice">Indicates whether the tax is included in the price.</param>
        /// <param name="context">The request context.</param>
        private static void ParseKitComponent(StringBuilder lineStringBuilder, DataSet ds, DataTable lineTable, string lineId, KitLineDefinition kitLineDefinition, KitComponentKey componentKey, bool isTaxIncludedInPrice, RequestContext context)
        {
            // Get the product for the kit component in order to retrieve product information for printing
            ProductDataManager productManager = new ProductDataManager(context);
            ProductSearchCriteria queryCriteria = new ProductSearchCriteria(context.GetPrincipal().ChannelId);
            QueryResultSettings querySettings = new QueryResultSettings();
            List<long> componentIds = new List<long>();
            componentIds.Add(componentKey.DistinctProductId);
            queryCriteria.Ids = componentIds;
            queryCriteria.DataLevel = CommerceEntityDataLevel.Complete;
            Product component = context.Runtime.Execute<ProductSearchServiceResponse>(
                new ProductSearchServiceRequest(queryCriteria, querySettings), context)
                                       .ProductSearchResult.Results.FirstOrDefault();

            DataTable charPosTable = ds.Tables.Where(p => p.TableName == CharPos).Single();
            if (component != null && charPosTable != null)
            {
                // Format and print each field in the line being printed
                int nextCharNr = 1;
                foreach (DataRow row in charPosTable.Rows.Where(p => p[lineTable.TableName + "_id"].ToString() == lineId).OrderBy(p => p["nr"]))
                {
                    ReceiptItemInfo itemInfo = new ReceiptItemInfo(row);

                    // Adding possible whitespace at the beginning of line
                    lineStringBuilder.Append(CreateWhitespace(' ', itemInfo.CharIndex - nextCharNr));

                    // Formatting for different font styles
                    if (itemInfo.FontStyle == FontStyle.Bold)
                    {
                        lineStringBuilder.Append(Esc + DoubleSpace);
                    }
                    else
                    {
                        lineStringBuilder.Append(Esc + SingleSpace);
                    }

                    // Create a sale item to store kit component information that may be printed
                    // this item gets passed to ParseItemVariable to parse component fields in the same manner as other line fields
                    SalesLine componentItem = new SalesLine();
                    componentItem.ItemId = component.ItemId;
                    componentItem.Quantity = kitLineDefinition.IndexedComponentProperties[componentKey.DistinctProductId].Quantity;
                    componentItem.SalesOrderUnitOfMeasure = kitLineDefinition.IndexedComponentProperties[componentKey.DistinctProductId].Unit;

                    // Parsing the itemInfo for kit component field information
                    lineStringBuilder.Append(ParseItemVariable(itemInfo, componentItem, isTaxIncludedInPrice, context));

                    // Closing the string with a single space command to make sure spaces are always single spaced
                    lineStringBuilder.Append(Esc + SingleSpace);

                    // Specifing the position of the next char in the current line - bold take twice as much space
                    nextCharNr = itemInfo.CharIndex + (itemInfo.Length * itemInfo.SizeFactor);
                }
            }

            lineStringBuilder.Append(Environment.NewLine);
        }

        private static string ParseCardTenderVariable(ReceiptItemInfo itemInfo, TenderLine eftInfo, SalesOrder theTransaction, GetReceiptServiceRequest request)
        {
            string tmpString = string.Empty;

            if (itemInfo.IsVariable)
            {
                tmpString = GetInfoFromTransaction(itemInfo, eftInfo, theTransaction, request.RequestContext);
            }
            else
            {
                tmpString = itemInfo.ValueString;
            }

            // Setting the align if neccessary
            tmpString = GetAlignmentSettings(tmpString, itemInfo);

            return tmpString;
        }

        /// <summary>
        /// For a tender template variable return the data for the receipt.
        /// </summary>
        /// <param name="itemInfo">The template properties.</param>
        /// <param name="tenderLineItem">The tender line.</param>
        /// <param name="theTransaction">The sales order.</param>
        /// <param name="request">The request parameter.</param>
        /// <returns>The value for the template variable.</returns>
        private static string ParseTenderVariable(ReceiptItemInfo itemInfo, TenderLine tenderLineItem, SalesOrder theTransaction, GetReceiptServiceRequest request)
        {
            string tmpString = string.Empty;

            if (itemInfo.IsVariable)
            {
                tmpString = GetInfoFromTransaction(itemInfo, tenderLineItem, theTransaction, request.RequestContext);
            }
            else
            {
                tmpString = itemInfo.ValueString;
            }

            // Setting the align if neccessary
            tmpString = GetAlignmentSettings(tmpString, itemInfo);

            return tmpString;
        }

        private static string ParseTenderLineVariable(ReadOnlyCollection<TenderType> tenderTypes, ReceiptItemInfo itemInfo, TenderLine tenderLineItem, RequestContext context)
        {
            string templateVariable = GetInfoFromTenderLineItem(tenderTypes, itemInfo, tenderLineItem, context);

            // Setting the align if neccessary
            return GetAlignmentSettings(templateVariable, itemInfo);
        }

        /// <summary>
        /// Determines if a sales line item is a variable for receipt printing.
        /// </summary>
        /// <param name="itemInfo">The receipt properties.</param>
        /// <param name="saleLineItem">The sales line.</param>
        /// <param name="isTaxIncludedInPrice">Indicates whether the tax is included in price of the sale line.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The string with the data to print.</returns>
        private static string ParseItemVariable(ReceiptItemInfo itemInfo, SalesLine saleLineItem, bool isTaxIncludedInPrice, RequestContext context)
        {
            string parsedString = string.Empty;

            if (itemInfo.IsVariable)
            {
                parsedString = GetInfoFromSaleLineItem(itemInfo, saleLineItem, isTaxIncludedInPrice, context);
            }
            else
            {
                parsedString = itemInfo.ValueString;
            }

            // Setting the align if neccessary
            parsedString = GetAlignmentSettings(parsedString, itemInfo);

            return parsedString;
        }

        /// <summary>
        /// Returns transformed card tender as string.
        /// </summary>
        /// <param name="receiptInfo">The receipt info.</param>
        /// <param name="eftInfo">The tender line.</param>
        /// <param name="tenderType">The tender type.</param>
        /// <param name="theTransaction">The sales order or transaction.</param>
        /// <param name="request">The request parameter.</param>
        /// <returns>The formatted receipt.</returns>
        private static Receipt GetTransformedCardTender(ReceiptInfo receiptInfo, TenderLine eftInfo, TenderType tenderType, SalesOrder theTransaction, GetReceiptServiceRequest request)
        {
            if (receiptInfo == null)
            {
                throw new ArgumentNullException("ReceiptInfo");
            }

            StringBuilder returnString = new StringBuilder();
            DataSet ds = null;
            Receipt receipt = new Receipt();

            // Getting a dataset containing the headerpart of the current receipt
            ds = ConvertToDataSet(receiptInfo.HeaderTemplate, receiptInfo);
            returnString.Append(ReadCardTenderDataSet(ds, eftInfo, theTransaction, tenderType, request));
            receipt.Header = ReadCardTenderDataSet(ds, eftInfo, theTransaction, tenderType, request);

            // Getting a dataset containing the footerpart of the current receipt
            ds = ConvertToDataSet(receiptInfo.FooterTemplate, receiptInfo);
            returnString.Append(ReadCardTenderDataSet(ds, eftInfo, theTransaction, tenderType, request));
            receipt.Footer = ReadCardTenderDataSet(ds, eftInfo, theTransaction, tenderType, request);

            return receipt;
        }

        /// <summary>
        /// Gets the transformed receipt from a transaction.
        /// </summary>
        /// <param name="tenderTypes">The tender types.</param>
        /// <param name="receiptInfo">The receipt properties.</param>
        /// <param name="theTransaction">The sales order or transaction.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The receipt used for printing.</returns>
        private static Receipt GetTransformedTransaction(ReadOnlyCollection<TenderType> tenderTypes, ReceiptInfo receiptInfo, SalesOrder theTransaction, RequestContext context)
        {
            if (receiptInfo == null)
            {
                throw new ArgumentNullException("receiptInfo");
            }

            Receipt receipt = new Receipt();

            DataSet ds = ConvertToDataSet(receiptInfo.HeaderTemplate, receiptInfo);

            // Getting a dataset containing the headerpart of the current receipt
            receipt.Header += ReadTemplateDataset(ds, tenderTypes, null, theTransaction, context);
            if (ds.Tables == null || ds.Tables.Count == 0)
            {
                throw new DataValidationException(DataValidationErrors.InvalidReceiptTemplate);
            }

            receiptInfo.HeaderLines = ds.Tables[0].Rows.Count;

            ds = ConvertToDataSet(receiptInfo.BodyTemplate, receiptInfo);

            // Getting a dataset containing the linepart of the current receipt
            receipt.Body = ReadItemDataSet(ds, theTransaction, context);
            receiptInfo.Bodylines = ds.Tables[0].Rows.Count;

            ds = ConvertToDataSet(receiptInfo.FooterTemplate, receiptInfo);

            // Getting a dataset containing the footerpart of the current receipt
            receipt.Footer = ReadTemplateDataset(ds, tenderTypes, null, theTransaction, context);
            receiptInfo.FooterLines = ds.Tables[0].Rows.Count;

            return receipt;
        }

        /// <summary>
        /// Gets the receipt object when payment is made using a customer account or credit memo.
        /// </summary>
        /// <param name="receiptInfo">The receipt properties.</param>
        /// <param name="theTransaction">The sales order.</param>
        /// <param name="tenderLineItem">The tender line.</param>
        /// <param name="request">The request parameter.</param>
        /// <returns>The receipt used for printing.</returns>
        private static Receipt GetTransformedTender(ReceiptInfo receiptInfo, SalesOrder theTransaction, TenderLine tenderLineItem, GetReceiptServiceRequest request)
        {
            if (receiptInfo == null)
            {
                throw new ArgumentNullException("receiptInfo");
            }

            Receipt receipt = new Receipt();
            StringBuilder returnString = new StringBuilder();

            DataSet ds = ConvertToDataSet(receiptInfo.HeaderTemplate, receiptInfo);

            // Getting a dataset containing the headerpart of the current form
            receipt.Header += ReadTenderDataSet(ds, tenderLineItem, theTransaction, request);

            ds = ConvertToDataSet(receiptInfo.FooterTemplate, receiptInfo);

            // Getting a dataset containing the footerpart of the current form
            receipt.Footer += ReadTenderDataSet(ds, tenderLineItem, theTransaction, request);

            return receipt;
        }

        /// <summary>
        /// Determines if a tag is a variable for receipt or not.
        /// </summary>
        /// <param name="itemInfo">The receipt properties.</param>
        /// <param name="tenderItem">The tender line.</param>
        /// <param name="theTransaction">The sales order or transaction.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The string with printable data.</returns>
        private static string ParseVariable(ReceiptItemInfo itemInfo, TenderLine tenderItem, SalesOrder theTransaction, RequestContext context)
        {
            string tmpString;

            if (itemInfo.IsVariable)
            {
                tmpString = GetInfoFromTransaction(itemInfo, tenderItem, theTransaction, context);
            }
            else
            {
                tmpString = itemInfo.ValueString;
            }

            if (tmpString == null)
            {
                tmpString = string.Empty;
            }

            // Setting the align if neccessary
            tmpString = GetAlignmentSettings(tmpString, itemInfo);

            return tmpString;
        }

        private static string ReadTenderDataSet(DataSet ds, TenderLine tenderLineItem, SalesOrder theTransaction, GetReceiptServiceRequest request)
        {
            string returnString = string.Empty;

            DataTable lineTable = ds.Tables.Where(p => p.TableName == "line").Single();
            DataTable table;
            ReceiptItemInfo itemInfo = null;

            if (lineTable != null)
            {
                foreach (DataRow dr in lineTable.Rows.Where(p => p != null).OrderBy(p => p["nr"]))
                {
                    string lineString = string.Empty;
                    string variable = (string)dr["ID"];

                    switch (variable)
                    {
                        case CRLF:
                            lineString += Environment.NewLine;
                            break;
                        case Text:
                            string lineId = dr[LineId].ToString();
                            table = ds.Tables.Where(p => p.TableName == CharPos).Single();
                            if (table != null)
                            {
                                int nextChar = 1;
                                foreach (DataRow row in table.Rows.Where(p => p[lineTable.TableName + "_id"].ToString() == lineId).OrderBy(p => p["nr"]))
                                {
                                    try
                                    {
                                        itemInfo = new ReceiptItemInfo(row);

                                        // Adding possible whitespace at the beginning of line
                                        lineString += CreateWhitespace(' ', itemInfo.CharIndex - nextChar);

                                        if (itemInfo.FontStyle == FontStyle.Bold)
                                        {
                                            lineString += Esc + DoubleSpace;
                                        }
                                        else
                                        {
                                            lineString += Esc + SingleSpace;
                                        }

                                        // Parsing the itemInfo
                                        lineString += ParseTenderVariable(itemInfo, tenderLineItem, theTransaction, request);

                                        // Closing the string with a single space command to make sure spaces are always single spaced
                                        lineString += Esc + SingleSpace;

                                        // Specifing the position of the next char in the current line - bold take twice as much space
                                        nextChar = itemInfo.CharIndex + (itemInfo.Length * itemInfo.SizeFactor);
                                    }
                                    catch (Exception)
                                    {
                                    }
                                }
                            }

                            lineString += Environment.NewLine;
                            break;
                    }

                    returnString += lineString;
                }
            }

            return returnString.ToString();
        }

        private static string ReadCardTenderDataSet(DataSet ds, TenderLine eftInfo, SalesOrder theTransaction, TenderType tenderType, GetReceiptServiceRequest request)
        {
            string returnString = string.Empty;

            // Note: Receipt templates are persisted as searilzed DataTable objects. Don't change the case
            // of these tables/fields for backward compatibility.
            DataTable lineTable = ds.Tables.Where(p => p.TableName == "line").Single();
            DataTable charPosTable;
            ReceiptItemInfo itemInfo = null;

            if (lineTable != null)
            {
                foreach (DataRow dr in lineTable.Rows.Where(p => p != null).OrderBy(p => p["nr"]))
                {
                    string lineString = string.Empty;
                    string idVariable = (string)dr["ID"];

                    if (idVariable == CRLF)
                    {
                        lineString += Environment.NewLine;
                    }
                    else if ((idVariable == "CardHolderSignature") && (tenderType.OperationId != (int)RetailOperation.PayCard))
                    {
                        // Skip card holder signature line for other than Credit Cards.
                    }
                    else
                    {
                        string lineId = dr[LineId].ToString();
                        charPosTable = ds.Tables.Where(p => p.TableName == CharPos).Single();
                        if (charPosTable != null)
                        {
                            int nextCharNr = 1;
                            foreach (DataRow row in charPosTable.Rows.Where(p => p[lineTable.TableName + "_id"].ToString() == lineId).OrderBy(p => p["nr"]))
                            {
                                try
                                {
                                    itemInfo = new ReceiptItemInfo(row);

                                    // Adding possible whitespace at the beginning of line
                                    lineString += CreateWhitespace(' ', itemInfo.CharIndex - nextCharNr);

                                    if (itemInfo.FontStyle == FontStyle.Bold)
                                    {
                                        lineString += Esc + DoubleSpace;
                                    }
                                    else
                                    {
                                        lineString += Esc + SingleSpace;
                                    }

                                    // Parsing the itemInfo
                                    lineString += ParseCardTenderVariable(itemInfo, eftInfo, theTransaction, request);

                                    // Closing the string with a single space command to make sure spaces are always single spaced
                                    lineString += Esc + SingleSpace;

                                    // Specifing the position of the next char in the current line - bold take twice as much space
                                    nextCharNr = itemInfo.CharIndex + (itemInfo.Length * itemInfo.SizeFactor);
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }

                        lineString += Environment.NewLine;
                    }

                    returnString += lineString;
                }
            }

            return returnString.ToString();
        }

        /// <summary>
        /// Reads the dataset for validations.
        /// </summary>
        /// <param name="templateDataset">The receipt template as a dataset.</param>
        /// <param name="tenderTypes">The tender types.</param>
        /// <param name="tenderItem">The tender line.</param>
        /// <param name="theTransaction">The sales order or transaction.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The string with printable data.</returns>
        private static string ReadTemplateDataset(DataSet templateDataset, ReadOnlyCollection<TenderType> tenderTypes, TenderLine tenderItem, SalesOrder theTransaction, RequestContext context)
        {
            StringBuilder tempString = new StringBuilder();
            DataTable lineTable = templateDataset.Tables.Where(p => p.TableName == "line").Single();

            ReceiptItemInfo itemInfo = null;

            // foreach (DataRow dr in lineTable.Rows)
            foreach (DataRow dr in lineTable.Rows.Where(p => p != null).OrderBy(p => p["nr"]))
            {
                string idVariable = dr["ID"].ToString();
                string lineId = string.Empty;
                DataTable charPosTable;
                switch (idVariable)
                {
                    case CRLF:
                        tempString.Append(Environment.NewLine);
                        break;
                    case Text:
                        lineId = dr[LineId].ToString();
                        charPosTable = templateDataset.Tables.Where(p => p.TableName == "charpos").Single();
                        if (charPosTable != null)
                        {
                            int nextCharNr = 1;

                            foreach (DataRow row in charPosTable.Rows.Where(p => p[lineTable.TableName + "_id"].ToString() == lineId).OrderBy(p => p["nr"]))
                            {
                                try
                                {
                                    itemInfo = new ReceiptItemInfo(row);

                                    // Adding possible whitespace at the beginning of line
                                    tempString.Append(CreateWhitespace(' ', itemInfo.CharIndex - nextCharNr));

                                    // Parsing the itemInfo
                                    if (itemInfo.FontStyle == FontStyle.Bold)
                                    {
                                        tempString.Append(Esc + DoubleSpace);
                                    }
                                    else
                                    {
                                        tempString.Append(Esc + SingleSpace);
                                    }

                                    tempString.Append(ParseVariable(itemInfo, tenderItem, theTransaction, context));

                                    // Closing the string with a single space command to make sure spaces are always single spaced
                                    tempString.Append(Esc + SingleSpace);

                                    // Specifing the position of the next char in the current line - bold take twice as much space
                                    nextCharNr = itemInfo.CharIndex + (itemInfo.Length * itemInfo.SizeFactor);
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }

                        tempString.Append(Environment.NewLine);
                        break;
                    case "Tenders":
                        charPosTable = templateDataset.Tables.Where(p => p.TableName == CharPos).Single();
                        lineId = dr[LineId].ToString();

                        if (charPosTable != null)
                        {
                            foreach (var tenderLineItem in theTransaction.TenderLines)
                            {
                                if (tenderLineItem.TransactionStatus == TransactionStatus.Normal)
                                {
                                    int nextCharNr = 1;
                                    foreach (DataRow row in charPosTable.Rows.Where(p => p[lineTable.TableName + "_id"].ToString() == lineId).OrderBy(p => p["nr"]))
                                    {
                                        try
                                        {
                                            itemInfo = new ReceiptItemInfo(row);

                                            // If tender is a Change Back tender, then a carrage return is put in front of the next line
                                            if ((tenderLineItem.Amount < 0) && (nextCharNr == 1))
                                            {
                                                tempString.Append(Environment.NewLine);
                                            }

                                            // Adding possible whitespace at the beginning of line
                                            tempString.Append(CreateWhitespace(' ', itemInfo.CharIndex - nextCharNr));

                                            if (itemInfo.FontStyle == FontStyle.Bold)
                                            {
                                                tempString.Append(Esc + DoubleSpace);
                                            }
                                            else
                                            {
                                                tempString.Append(Esc + SingleSpace);
                                            }

                                            // Parsing the itemInfo
                                            tempString.Append(ParseTenderLineVariable(tenderTypes, itemInfo, tenderLineItem, context));

                                            // Closing the string with a single space command to make sure spaces are always single spaced
                                            tempString.Append(Esc + SingleSpace);

                                            // Specifing the position of the next char in the current line - bold take twice as much space
                                            nextCharNr = itemInfo.CharIndex + (itemInfo.Length * itemInfo.SizeFactor);
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }

                                    tempString.Append(Environment.NewLine);
                                }
                            }
                        }

                        break;

                    case Taxes:
                        tempString.Append(IndiaReceiptServiceHelper.ReadDataRowTax(templateDataset, dr, theTransaction, context));
                        break;

                    case LoyaltyItem:
                        charPosTable = templateDataset.Tables.Where(p => p.TableName == CharPos).Single();
                        if (charPosTable != null && !string.IsNullOrEmpty(theTransaction.LoyaltyCardId))
                        {
                            tempString.Append(ParseLoyaltyText(theTransaction, charPosTable, lineTable, dr[LineId].ToString(), tenderItem, context));
                        }

                        break;

                    case LoyaltyEarnText:
                        charPosTable = templateDataset.Tables.Where(p => p.TableName == CharPos).Single();

                        // check if there are loyalty card and earned reward lines with redeemable points 
                        if (charPosTable != null
                            && theTransaction.LoyaltyRewardPointLines != null
                            && theTransaction.LoyaltyRewardPointLines.Any(l => (l.EntryType == LoyaltyRewardPointEntryType.Earn || l.EntryType == LoyaltyRewardPointEntryType.ReturnEarned) && l.RewardPointIsRedeemable))
                        {
                            tempString.Append(ParseLoyaltyText(theTransaction, charPosTable, lineTable, dr[LineId].ToString(), tenderItem, context));
                        }

                        break;

                    case LoyaltyRedeemText:
                        charPosTable = templateDataset.Tables.Where(p => p.TableName == CharPos).Single();

                        // check if there are loyalty card and redeemed reward lines
                        if (charPosTable != null
                            && theTransaction.LoyaltyRewardPointLines != null
                            && theTransaction.LoyaltyRewardPointLines.Any(l => l.EntryType == LoyaltyRewardPointEntryType.Redeem || l.EntryType == LoyaltyRewardPointEntryType.Refund))
                        {
                            tempString.Append(ParseLoyaltyText(theTransaction, charPosTable, lineTable, dr[LineId].ToString(), tenderItem, context));
                        }

                        break;

                    case LoyaltyEarnLines:
                        charPosTable = templateDataset.Tables.Where(p => p.TableName == CharPos).Single();
                        if (charPosTable != null)
                        {
                            tempString.Append(ParseLoyaltyLines(theTransaction, charPosTable, lineTable, dr[LineId].ToString(), LoyaltyRewardPointEntryType.ReturnEarned, context));
                            tempString.Append(ParseLoyaltyLines(theTransaction, charPosTable, lineTable, dr[LineId].ToString(), LoyaltyRewardPointEntryType.Earn, context));
                        }

                        break;

                    case LoyaltyRedeemLines:
                        charPosTable = templateDataset.Tables.Where(p => p.TableName == CharPos).Single();
                        if (charPosTable != null)
                        {
                            tempString.Append(ParseLoyaltyLines(theTransaction, charPosTable, lineTable, dr[LineId].ToString(), LoyaltyRewardPointEntryType.Refund, context));
                            tempString.Append(ParseLoyaltyLines(theTransaction, charPosTable, lineTable, dr[LineId].ToString(), LoyaltyRewardPointEntryType.Redeem, context));
                        }

                        break;
                }
            }

            return tempString.ToString();
        }

        /// <summary>
        /// Returns the receipt data for a loyalty text.
        /// </summary>
        /// <param name="theTransaction">The sales order.</param>
        /// <param name="charPosTable">The char Pos data table.</param>
        /// <param name="lineTable">The line data table.</param>
        /// <param name="lineId">The line identifier.</param>
        /// <param name="tenderItem">The tender line.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The string with the receipt data to print.</returns>
        private static string ParseLoyaltyText(SalesOrder theTransaction, DataTable charPosTable, DataTable lineTable, string lineId, TenderLine tenderItem, RequestContext context)
        {
            StringBuilder tempString = new StringBuilder();

            int nextCharNr = 1;
            foreach (DataRow row in charPosTable.Rows.Where(p => p[lineTable.TableName + "_id"].ToString() == lineId).OrderBy(p => p["nr"]))
            {
                ReceiptItemInfo itemInfo = new ReceiptItemInfo(row);

                // Adding possible whitespace at the beginning of line
                tempString.Append(CreateWhitespace(' ', itemInfo.CharIndex - nextCharNr));

                tempString.Append(Esc + (itemInfo.FontStyle == FontStyle.Bold ? DoubleSpace : SingleSpace));

                // Parsing the itemInfo
                tempString.Append(ParseVariable(itemInfo, tenderItem, theTransaction, context));

                // Closing the string with a single space command to make sure spaces are always single spaced
                tempString.Append(Esc + SingleSpace);

                // Specifing the position of the next char in the current line - bold take twice as much space
                nextCharNr = itemInfo.CharIndex + (itemInfo.Length * itemInfo.SizeFactor);
            }

            tempString.Append(Environment.NewLine);

            return tempString.ToString();
        }

        /// <summary>
        /// Returns the receipt data for a loyalty reward point line.
        /// </summary>
        /// <param name="theTransaction">The sales order.</param>
        /// <param name="charPosTable">The char Pos data table.</param>
        /// <param name="lineTable">The line data table.</param>
        /// <param name="lineId">The line identifier.</param>
        /// <param name="entryType">The loyalty entry type.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The string with the receipt data to print.</returns>
        private static string ParseLoyaltyLines(SalesOrder theTransaction, DataTable charPosTable, DataTable lineTable, string lineId, LoyaltyRewardPointEntryType entryType, RequestContext context)
        {
            StringBuilder tempString = new StringBuilder();

            // Grouping reward point lines
            IEnumerable<LoyaltyRewardPointLine> groupedRewardPointLines =
                from l in theTransaction.LoyaltyRewardPointLines
                where l.EntryType == entryType && l.RewardPointIsRedeemable
                group l by new { l.RewardPointRecordId, l.RewardPointId, l.RewardPointType, l.RewardPointIsRedeemable, l.RewardPointCurrency }
                    into g
                    select new LoyaltyRewardPointLine
                    {
                        RewardPointRecordId = g.Key.RewardPointRecordId,
                        RewardPointId = g.Key.RewardPointId,
                        RewardPointType = g.Key.RewardPointType,
                        RewardPointIsRedeemable = g.Key.RewardPointIsRedeemable,
                        RewardPointCurrency = g.Key.RewardPointCurrency,
                        RewardPointAmountQuantity = g.Sum(a => a.RewardPointAmountQuantity)
                    };

            // iterate over loyalty reward points
            foreach (LoyaltyRewardPointLine rewardPointLine in groupedRewardPointLines)
            {
                int nextCharNr = 1;
                foreach (DataRow row in charPosTable.Rows.Where(p => p[lineTable.TableName + "_id"].ToString() == lineId).OrderBy(p => p["nr"]))
                {
                    ReceiptItemInfo itemInfo = new ReceiptItemInfo(row);

                    // Adding possible whitespace at the beginning of line
                    tempString.Append(CreateWhitespace(' ', itemInfo.CharIndex - nextCharNr));

                    tempString.Append(Esc + (itemInfo.FontStyle == FontStyle.Bold ? DoubleSpace : SingleSpace));

                    // Parsing the itemInfo
                    tempString.Append(ParseLoyaltyRewardPointLine(itemInfo, rewardPointLine, entryType, context));

                    // Closing the string with a single space command to make sure spaces are always single spaced
                    tempString.Append(Esc + SingleSpace);

                    // Specifing the position of the next char in the current line - bold take twice as much space
                    nextCharNr = itemInfo.CharIndex + (itemInfo.Length * itemInfo.SizeFactor);
                }

                tempString.Append(Environment.NewLine);
            }

            return tempString.ToString();
        }

        /// <summary>
        /// Returns the receipt data for a loyalty reward point line.
        /// </summary>
        /// <param name="itemInfo">The receipt item information.</param>
        /// <param name="rewardPointLine">The reward point line.</param>
        /// <param name="entryType">The loyalty entry type.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The string with the receipt data to print.</returns>
        private static string ParseLoyaltyRewardPointLine(ReceiptItemInfo itemInfo, LoyaltyRewardPointLine rewardPointLine, LoyaltyRewardPointEntryType entryType, RequestContext context)
        {
            string tempString = string.Empty;

            if (rewardPointLine != null)
            {
                switch (itemInfo.Variable.ToUpperInvariant())
                {
                    case EARNEDREWARDPOINTID:
                    case REDEEMEDREWARDPOINTID:
                        tempString = rewardPointLine.RewardPointId;
                        break;
                    case EARNEDREWARDPOINTAMOUNTQUANTITY:
                    case REDEEMEDREWARDPOINTAMOUNTQUANTITY:
                        decimal rewardPoints = rewardPointLine.RewardPointAmountQuantity;
                        if (entryType == LoyaltyRewardPointEntryType.Redeem || entryType == LoyaltyRewardPointEntryType.Refund)
                        {
                            rewardPoints = rewardPoints * -1;
                        }

                        switch (rewardPointLine.RewardPointType)
                        {
                            case LoyaltyRewardPointType.Quantity:
                                tempString = string.Format("{0:0}", rewardPoints);
                                break;
                            case LoyaltyRewardPointType.Amount:
                                tempString = GetFormattedValue(rewardPoints, rewardPointLine.RewardPointCurrency, context);
                                break;
                        }

                        break;
                }
            }

            return GetAlignmentSettings(tempString, itemInfo);
        }

        /// <summary>
        /// Converts a template to a dataset.
        /// </summary>
        /// <param name="encodedTemplateXml">The hex encoded xml in string.</param>
        /// <param name="receiptInfo">The receipt properties.</param>
        /// <returns>The dataset.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "The scope exists more than the method")]
        private static DataSet ConvertToDataSet(string encodedTemplateXml, ReceiptInfo receiptInfo)
        {
            DataSet template = null;

            if (encodedTemplateXml.Length > 0)
            {
                template = new DataSet();
                byte[] buffer = null;
                int discarded;
                buffer = ReceiptService.GetBytes(encodedTemplateXml, out discarded);

                using (MemoryStream myStream = new MemoryStream())
                {
                    myStream.Write(buffer, 0, buffer.Length);
                    myStream.Position = 0;

                    XDocument xml = XDocument.Load(myStream);
                    HashSet<string> tableNames = new HashSet<string>();
                    int lineId = 0;

                    foreach (XElement element in xml.Root.Elements())
                    {
                        foreach (XElement child in element.DescendantsAndSelf())
                        {
                            if (!tableNames.Contains(child.Name.LocalName))
                            {
                                template = CreateDataTable(template, child);
                                AddDataRow(template.Tables.Where(p => p.TableName == child.Name.LocalName).Single(), child, lineId);
                                tableNames.Add(child.Name.LocalName);
                            }
                            else
                            {
                                DataTable table = template.Tables.Where(p => p.TableName == child.Name.LocalName).Single();
                                DataRow dataRow = table.NewRow();

                                foreach (XAttribute attribute in child.Attributes())
                                {
                                    dataRow[attribute.Name.LocalName] = attribute.Value;
                                }

                                dataRow["line_id"] = lineId;
                                table.Rows.Add(dataRow);
                            }
                        }

                        lineId++;
                    }
                }

                // Adding detail table to the dataset
                DataTable receiptDetails = new DataTable();
                receiptDetails.TableName = "FORMDETAILS";

                // Adding columns to items data
                receiptDetails.Columns.Add("ID", typeof(string));
                receiptDetails.Columns.Add("TITLE", typeof(string));
                receiptDetails.Columns.Add("DESCRIPTION", typeof(string));
                receiptDetails.Columns.Add("UPPERCASE", typeof(bool));

                object row = new object[]
                {
                    receiptInfo.ReceiptLayoutId,
                    receiptInfo.Title,
                    receiptInfo.Description,
                    receiptInfo.Uppercase == 1
                };

                receiptDetails.Rows.Add(row);
                template.Tables.Add(receiptDetails);
            }

            return template;
        }

        private static DataSet CreateDataTable(DataSet dataSet, XElement element)
        {
            DataTable table = new DataTable(element.Name.LocalName);
            table.Columns.Add(new DataColumn("line_id", typeof(int)));

            foreach (XAttribute attribute in element.Attributes())
            {
                table.Columns.Add(new DataColumn(attribute.Name.LocalName, typeof(string)));
            }

            dataSet.Tables.Add(table);

            return dataSet;
        }

        private static void AddDataRow(DataTable table, XElement element, int id)
        {
            DataRow row = table.NewRow();
            row["line_id"] = id;

            foreach (XAttribute attribute in element.Attributes())
            {
                row[attribute.Name.LocalName] = attribute.Value;
            }

            table.Rows.Add(row);
        }

        /// <summary>
        /// Creates a byte array from the hexadecimal string. Each two characters are combined
        /// to create one byte. First two hexadecimal characters become first byte in returned array.
        /// Non-hexadecimal characters are ignored. 
        /// </summary>
        /// <param name="hexEncodedValue">String to convert to byte array.</param>
        /// <param name="discarded">Number of characters in string ignored.</param>
        /// <returns>Byte array, in the same left-to-right order as the hexString.</returns>
        private static byte[] GetBytes(string hexEncodedValue, out int discarded)
        {
            discarded = 0;

            // string newString = "";
            System.Text.StringBuilder newString = new StringBuilder();
            char c;

            // remove all none A-F, 0-9, characters
            for (int i = 0; i < hexEncodedValue.Length; i++)
            {
                c = hexEncodedValue[i];
                if (IsHexDigit(c))
                {
                    newString.Append(c); // newString += c;
                }
                else
                {
                    discarded++;
                }
            }

            // if odd number of characters, discard last character
            if (newString.Length % 2 != 0)
            {
                discarded++;
                newString = new StringBuilder(newString.ToString(0, newString.Length - 1));
            }

            // Converts the hexadecimal receipt template header, lines or footer to a bytes array
            // which is then consumed to convert it to a xml
            int byteLength = newString.Length / 2;
            byte[] bytes = new byte[byteLength];
            string hex;
            int j = 0;
            int k = 0;
            for (int i = 0; i < bytes.Length; i++)
            {
                hex = new string(new char[] { newString[j], newString[j + 1] });
                byte byteValue = HexToByte(hex);

                if (byteValue != 0x00)
                {
                    bytes[k] = byteValue;
                    k++;
                }

                j = j + 2;
            }

            return bytes.Take(k).ToArray();
        }

        /// <summary>
        /// Returns true is c is a hexadecimal digit (A-F, a-f, 0-9).
        /// </summary>
        /// <param name="c">Character to test.</param>
        /// <returns>True if hex digit, false if not.</returns>
        private static bool IsHexDigit(char c)
        {
            byte convertedByte;
            return byte.TryParse(c.ToString(), System.Globalization.NumberStyles.AllowHexSpecifier, null, out convertedByte);
        }

        /// <summary>
        /// Converts 1 or 2 character string into equivalent byte value.
        /// </summary>
        /// <param name="hex">1 or 2 character string.</param>
        /// <returns>Byte converted from hex.</returns>
        private static byte HexToByte(string hex)
        {
            if (hex.Length > 2 || hex.Length == 0)
            {
                throw new ArgumentException("Hex string must be 1 or 2 characters in length. The Hex string passed was " + hex);
            }

            byte newByte = byte.Parse(hex, System.Globalization.NumberStyles.HexNumber);

            return newByte;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "By design.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1505:AvoidUnmaintainableCode", Justification = "By design.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "By design.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1809:AvoidExcessiveLocals", Justification = "By design.")]
        private static string GetInfoFromTransaction(ReceiptItemInfo itemInfo, TenderLine tenderItem, SalesOrder theTransaction, RequestContext context)
        {
            DeliveryOption deliveryOption;
            GetCardPaymentPropertiesServiceRequest cardPaymentPropertiesRequest = null;
            GetCardPaymentPropertiesServiceResponse cardPaymentPropertiesResponse = null;

            if (tenderItem != null && !string.IsNullOrWhiteSpace(tenderItem.Authorization))
            {
                cardPaymentPropertiesRequest = new GetCardPaymentPropertiesServiceRequest(tenderItem.Authorization);
                cardPaymentPropertiesResponse = context.Execute<GetCardPaymentPropertiesServiceResponse>(cardPaymentPropertiesRequest);
            }

            if (theTransaction != null)
            {
                IndiaTaxDataManager taxDataManager = new IndiaTaxDataManager(context);
                DateTimeFormatInfo channelDateTimeFormat = string.IsNullOrEmpty(context.GetChannelConfiguration().DefaultLanguageId)
                    ? CultureInfo.CurrentCulture.DateTimeFormat
                    : new CultureInfo(context.GetChannelConfiguration().DefaultLanguageId).DateTimeFormat;

                switch (itemInfo.Variable.ToUpperInvariant().Replace(" ", string.Empty))
                {
                    case "DATE":
                    case "EFTDATE":
                        return context.ConvertDateTimeToChannelDate(theTransaction.OrderPlacedDate).ToString("d", channelDateTimeFormat);
                    case "TIME24H":
                    case "EFTTIME24H":
                        return context.ConvertDateTimeToChannelDate(theTransaction.OrderPlacedDate).ToString("HH:mm", channelDateTimeFormat);
                    case "EFTACCOUNTTYPE":
                        {
                            if (!string.IsNullOrWhiteSpace(tenderItem.Authorization))
                            {
                                if (cardPaymentPropertiesResponse != null)
                                {
                                    return cardPaymentPropertiesResponse.AccountType;
                                }
                            }

                            return string.Empty;
                        }

                    case "EFTAPPLICATIONID":
                        return string.Empty;
                    case "TIME12H":
                    case "EFTTIME12H":
                        return context.ConvertDateTimeToChannelDate(theTransaction.OrderPlacedDate).ToString("hh:mm tt", channelDateTimeFormat);
                    case "TRANSNO":
                    case "TRANSACTIONNUMBER":
                        {
                            switch (theTransaction.TransactionType)
                            {
                                case SalesTransactionType.CustomerOrder:
                                    return theTransaction.SalesId ?? string.Empty;
                                case SalesTransactionType.Sales:
                                default:
                                    return theTransaction.Id ?? string.Empty;
                            }
                        }

                    case "RECEIPTNUMBER":
                        return theTransaction.ReceiptId;
                    case "STAFF_ID":
                    case "OPERATORID":
                    case "EMPLOYEEID":
                    case "SALESPERSONID":
                        return theTransaction.StaffId ?? string.Empty;
                    case "EMPLOYEENAME":
                    case "OPERATORNAME":
                    case "OPERATORNAMEONRECEIPT":
                    case "SALESPERSONNAME":
                    case "CASHIER":
                    case "SALESPERSONNAMEONRECEIPT":
                        {
                            GetEmployeeDataRequest dataRequest = new GetEmployeeDataRequest(theTransaction.StaffId, new QueryResultSettings());
                            Employee employee = context.Execute<SingleEntityDataServiceResponse<Employee>>(dataRequest).Entity;
                            return employee.Name ?? string.Empty;
                        }

                    case "TOTALWITHTAX":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            return GetFormattedValue(theTransaction.TotalAmount, currency, context);
                        }

                    case "REMAININGBALANCE":
                        {
                            if (!string.IsNullOrWhiteSpace(tenderItem.Authorization))
                            {
                                if (cardPaymentPropertiesResponse != null)
                                {
                                    return GetFormattedValue(cardPaymentPropertiesResponse.AvailableBalance, context.GetOrgUnit().Currency, context);
                                }

                                return string.Empty;
                            }

                            return string.Empty;
                        }

                    case "TOTAL":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            return GetFormattedValue(theTransaction.SubtotalAmount, currency, context);
                        }

                    case "TAXTOTAL":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            return GetFormattedValue(theTransaction.TaxAmount, currency, context);
                        }

                    case "SUBTOTALWITHOUTTAX":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            return GetFormattedValue(theTransaction.SubtotalAmountWithoutTax, currency, context);
                        }

                    case "SUMTOTALDISCOUNT":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            return GetFormattedValue(theTransaction.TotalDiscount, currency, context);
                        }

                    case "SUMLINEDISCOUNT":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            return GetFormattedValue(theTransaction.LineDiscount, currency, context);
                        }

                    case "SUMALLDISCOUNT":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            return GetFormattedValue(theTransaction.DiscountAmount, currency, context);
                        }

                    case "TERMINALID":
                        return theTransaction.TerminalId;
                    case "CUSTOMERNAME":
                        {
                            if (string.IsNullOrEmpty(theTransaction.CustomerId))
                            {
                                return string.Empty;
                            }
                            else
                            {
                                var getCustomerDataRequest = new GetCustomerDataRequest(theTransaction.CustomerId);
                                SingleEntityDataServiceResponse<Customer> getCustomerDataResponse = context.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerDataRequest);
                                Customer customer = getCustomerDataResponse.Entity;

                                return customer.Name;
                            }
                        }

                    case "CUSTOMERACCOUNTNUMBER":
                        return theTransaction.CustomerId;
                    case "CUSTOMERADDRESS":
                        {
                            if (string.IsNullOrEmpty(theTransaction.CustomerId))
                            {
                                return string.Empty;
                            }
                            else
                            {
                                var getCustomerDataRequest = new GetCustomerDataRequest(theTransaction.CustomerId);
                                SingleEntityDataServiceResponse<Customer> getCustomerDataResponse = context.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerDataRequest);
                                Customer customer = getCustomerDataResponse.Entity;

                                if (customer == null)
                                {
                                    throw new DataValidationException(DataValidationErrors.ObjectNotFound, "The specified customer identifier ({0}) was not found.", theTransaction.CustomerId);
                                }

                                string customerAddress = customer.GetPrimaryAddress().FullAddress;

                                return customerAddress ?? string.Empty;
                            }
                        }

                    case "CUSTOMERAMOUNT":
                        return GetFormattedValue(tenderItem.Amount, tenderItem.Currency, context);
                    case "CUSTOMERVAT":
                        {
                            if (string.IsNullOrEmpty(theTransaction.CustomerId))
                            {
                                return string.Empty;
                            }
                            else
                            {
                                var getCustomerDataRequest = new GetCustomerDataRequest(theTransaction.CustomerId);
                                SingleEntityDataServiceResponse<Customer> getCustomerDataResponse = context.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerDataRequest);
                                Customer customer = getCustomerDataResponse.Entity;

                                if (customer != null)
                                {
                                    return customer.VatNumber ?? string.Empty;
                                }

                                return string.Empty;
                            }
                        }

                    case "CUSTOMERTAXOFFICE":
                        {
                            if (string.IsNullOrEmpty(theTransaction.CustomerId))
                            {
                                return string.Empty;
                            }
                            else
                            {
                                var getCustomerDataRequest = new GetCustomerDataRequest(theTransaction.CustomerId);
                                SingleEntityDataServiceResponse<Customer> getCustomerDataResponse = context.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerDataRequest);
                                Customer customer = getCustomerDataResponse.Entity;

                                if (customer != null)
                                {
                                    return customer.TaxOffice ?? string.Empty;
                                }

                                return string.Empty;
                            }
                        }

                    case "CARDEXPIREDATE":
                        // Not supported
                        return string.Empty;
                    case "CARDNUMBER":
                        return tenderItem.MaskedCardNumber;
                    case "CARDNUMBERPARTLYHIDDEN":
                        return tenderItem.MaskedCardNumber;
                    case "CARDTYPE":
                        return tenderItem.CardTypeId;
                    case "CARDISSUERNAME":
                        // Not supported
                        return string.Empty;
                    case "CARDAMOUNT":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            return GetFormattedValue(tenderItem.Amount, currency, context);
                        }

                    case "CARDAUTHNUMBER":
                        {
                            if (!string.IsNullOrWhiteSpace(tenderItem.Authorization))
                            {
                                return cardPaymentPropertiesResponse.ApprovalCode;
                            }

                            return string.Empty;
                        }

                    case "BATCHCODE":
                        // Not supported
                        return string.Empty;
                    case "ACQUIRERNAME":
                        {
                            if (!string.IsNullOrWhiteSpace(tenderItem.Authorization))
                            {
                                return cardPaymentPropertiesResponse.ConnectorName;
                            }

                            return string.Empty;
                        }

                    case "VISAAUTHCODE":
                    case "EUROAUTHCODE":
                        return string.Empty;
                    case "EFTSTORECODE":
                        return theTransaction.StoreId;
                    case "EFTTERMINALNUMBER":
                        return theTransaction.TerminalId;
                    case "EFTINFOMESSAGE":
                        {
                            if (!string.IsNullOrWhiteSpace(tenderItem.Authorization))
                            {
                                return cardPaymentPropertiesResponse.InfoMessage;
                            }

                            return string.Empty;
                        }

                    case "EFTTERMINALID":
                        return theTransaction.TerminalId;
                    case "EFTMERCHANTID":
                        return string.Empty;
                    case "ENTRYSOURCECODE":
                        // Not supported
                        return string.Empty;
                    case "AUTHSOURCECODE":
                        // Not supported
                        return string.Empty;
                    case "AUTHORIZATIONCODE":
                        {
                            if (!string.IsNullOrWhiteSpace(tenderItem.Authorization))
                            {
                                return cardPaymentPropertiesResponse.ApprovalCode;
                            }

                            return string.Empty;
                        }

                    case "SEQUENCECODE":
                        // Not supported
                        return string.Empty;
                    case "EFTMESSAGE":
                        // Not supported
                        return string.Empty;
                    case "EFTRETRIEVALREFERENCENUMBER":
                        {
                            if (!string.IsNullOrWhiteSpace(tenderItem.Authorization))
                            {
                                return cardPaymentPropertiesResponse.ProviderTransactionId;
                            }

                            return string.Empty;
                        }

                    case "CUSTOMERTENDERAMOUNT":
                        {
                            return GetFormattedValue(tenderItem.Amount, tenderItem.Currency, context);
                        }

                    case "TENDERROUNDING":
                        {
                            decimal tenderRoundingAmount = decimal.Negate(theTransaction.AmountPaid - theTransaction.GrossAmount);
                            return GetFormattedValue(tenderRoundingAmount, context.GetOrgUnit().Currency, context);
                        }

                    case "INVOICECOMMENT":
                        return theTransaction.InvoiceComment;
                    case "TRANSACTIONCOMMENT":
                        return theTransaction.Comment;
                    case "LOGO":
                        if (itemInfo.ImageId == 0)
                        {
                            return LegacyLogoMessage;
                        }
                        else
                        {
                            ChannelDataManager channelDataManager = new ChannelDataManager(context);
                            string pictureAsBase64 = channelDataManager.GetPictureByPictureId(itemInfo.ImageId, new QueryResultSettings()).PictureAsBase64;
                            if (pictureAsBase64 == null)
                            {
                                return LegacyLogoMessage;
                            }
                            else
                            {
                                return string.Format(LogoMessage, pictureAsBase64);
                            }
                        }

                    case "RECEIPTNUMBERBARCODE":
                        return "<B: " + theTransaction.ReceiptId + ">";
                    case "CUSTOMERORDERBARCODE":
                        return "<B: " + theTransaction.SalesId + ">";
                    case "REPRINTMESSAGE":
                        return string.Empty;
                    case "OFFLINEINDICATOR":
                        // Not supported
                        return string.Empty;
                    case "STOREID":
                        return theTransaction.StoreId;
                    case "STORENAME":
                        return context.GetOrgUnit().OrgUnitName;
                    case "STOREADDRESS":
                        return context.GetOrgUnit().OrgUnitFullAddress;
                    case "STOREPHONE":
                        return context.GetOrgUnit().OrgUnitAddress.Phone;
                    case "STORETAXIDENTIFICATIONNUMBER":
                        // Not supported
                        return string.Empty;
                    case "TENDERAMOUNT":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            return GetFormattedValue(tenderItem.Amount, currency, context);
                        }

                    case "LOYALTYCARDNUMBER":
                        {
                            return theTransaction.LoyaltyCardId;
                        }

                    case "CREDITMEMONUMBER":
                        {
                            return tenderItem.CreditMemoId;
                        }

                    case "CREDITMEMOAMOUNT":
                        {
                            GetCreditMemoServiceRequest serviceRequest = null;
                            GetCreditMemoServiceResponse serviceResponse = null;

                            serviceRequest = new GetCreditMemoServiceRequest(
                                tenderItem.CreditMemoId);
                            serviceResponse = context.Execute<GetCreditMemoServiceResponse>(serviceRequest);

                            if (serviceResponse.CreditMemo != null)
                            {
                                return GetFormattedValue(serviceResponse.CreditMemo.Balance, serviceResponse.CreditMemo.CurrencyCode, context);
                            }
                            else
                            {
                                throw new DataValidationException(DataValidationErrors.CreditVoucherNull, "Credit voucher with number {0} cannot be null", tenderItem.CreditMemoId);
                            }
                        }

                    case "ALLTENDERCOMMENTS":
                        // Not supported
                        return string.Empty;
                    case "ALLITEMCOMMENTS":
                        {
                            string comments = string.Empty;
                            foreach (SalesLine line in theTransaction.SalesLines)
                            {
                                if (line.Comment != null)
                                {
                                    comments += line.Comment;
                                }
                            }

                            return comments;
                        }

                    case "DEPOSITDUE":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            if (theTransaction.IsDepositOverridden)
                            {
                                return GetFormattedValue(theTransaction.OverriddenDepositAmount.Value, currency, context);
                            }
                            else
                            {
                                return GetFormattedValue(theTransaction.RequiredDepositAmount, currency, context);
                            }
                        }

                    case "DEPOSITAPPLIED":
                    case "DEPOSITPAID":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            return GetFormattedValue(theTransaction.RequiredDepositAmount, currency, context);
                        }

                    case "DELIVERYTYPE":
                        {
                            if (theTransaction.SalesLines.Count(line => line.DeliveryMode == context.GetChannelConfiguration().PickupDeliveryModeCode) == theTransaction.SalesLines.Count)
                            {
                                var dataRequest = new GetDeliveryOptionDataRequest(context.GetChannelConfiguration().PickupDeliveryModeCode, new QueryResultSettings(new ColumnSet("TXT")));
                                var dataResponse = context.Execute<EntityDataServiceResponse<DeliveryOption>>(dataRequest);

                                deliveryOption = dataResponse.EntityCollection.FirstOrDefault();
                                return (deliveryOption != null) ? deliveryOption.Description : context.GetChannelConfiguration().PickupDeliveryModeCode;
                            }
                            else if (theTransaction.SalesLines.All(line => line.DeliveryMode != context.GetChannelConfiguration().PickupDeliveryModeCode))
                            {
                                return "Delivery";
                            }
                            else
                            {
                                return "Mixed";
                            }
                        }

                    case "DELIVERYMETHOD":
                        var deliveryOptionDataRequest = new GetDeliveryOptionDataRequest(theTransaction.DeliveryMode, new QueryResultSettings(new ColumnSet("TXT")));
                        var deliveryOptionDataResponse = context.Execute<EntityDataServiceResponse<DeliveryOption>>(deliveryOptionDataRequest);
                        deliveryOption = deliveryOptionDataResponse.EntityCollection.FirstOrDefault();
                        return (deliveryOption != null) ? deliveryOption.Description : theTransaction.DeliveryMode;
                    case "DELIVERYDATE":
                        return theTransaction.RequestedDeliveryDate.HasValue
                            ? theTransaction.RequestedDeliveryDate.Value.DateTime.ToString("d")
                            : string.Empty;
                    case "ORDERTYPE":
                        return string.Empty;
                    case "ORDERSTATUS":
                        return theTransaction.Status.ToString();
                    case "REFERENCENO":
                        return theTransaction.SalesId;
                    case "EXPIRYDATE":
                        return theTransaction.QuotationExpiryDate.ToString();
                    case "ORDERID":
                        return theTransaction.SalesId;
                    case "TOTALSHIPPIINGCHARGES":
                    // Fall through to ShippingCharge case
                    case "SHIPPINGCHARGE":
                        {
                            decimal shippingCharges = theTransaction.SalesLines.Where(line => !line.IsVoided).Sum(shippingCharge => shippingCharge.DeliveryModeChargeAmount ?? 0M);
                            string currency = context.GetOrgUnit().Currency;

                            return GetFormattedValue(shippingCharges, currency, context);
                        }

                    case "CANCELLATIONCHARGE":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            decimal sum = decimal.Zero;

                            string cancellationChargeCode = context.GetChannelConfiguration().CancellationChargeCode ?? string.Empty;
                            var cancellationChargeLines = theTransaction.ChargeLines.Where(c => cancellationChargeCode.Equals(c.ChargeCode, StringComparison.OrdinalIgnoreCase));
                            if (IsExcludeTaxInCancellationCharge(context))
                            {
                                sum = cancellationChargeLines.Sum(c => c.CalculatedAmount - c.TaxAmountInclusive);
                            }
                            else
                            {
                                sum = cancellationChargeLines.Sum(c => c.CalculatedAmount);
                            }

                            return GetFormattedValue(sum, currency, context);
                        }

                    case "TAXONCANCELLATIONCHARGE":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            string cancellationChargeCode = context.GetChannelConfiguration().CancellationChargeCode ?? string.Empty;
                            var cancellationChargeLines = theTransaction.ChargeLines.Where(c => cancellationChargeCode.Equals(c.ChargeCode, StringComparison.OrdinalIgnoreCase));
                            decimal sum = cancellationChargeLines.Sum(c => c.TaxAmount);
                            return GetFormattedValue(sum, currency, context);
                        }

                    case "TOTALCANCELLATIONCHARGE":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            string cancellationChargeCode = context.GetChannelConfiguration().CancellationChargeCode ?? string.Empty;
                            var cancellationChargeLines = theTransaction.ChargeLines.Where(c => cancellationChargeCode.Equals(c.ChargeCode, StringComparison.OrdinalIgnoreCase));
                            decimal sum = cancellationChargeLines.Sum(c => c.CalculatedAmount + c.TaxAmountExclusive);
                            return GetFormattedValue(sum, currency, context);
                        }

                    case "TAXONSHIPPING":
                        return string.Empty;
                    case "MISCCHARGETOTAL":
                        return string.Empty;
                    case "TOTALLINEITEMSHIPPINGCHARGES":
                        return string.Empty;
                    case "ORDERSHIPPINGCHARGE":
                        return string.Empty;
                    case "TOTALPAYMENTS":
                        {
                            if (theTransaction.TransactionType == SalesTransactionType.CustomerOrder)
                            {
                                if (theTransaction.CustomerOrderType == CustomerOrderType.SalesOrder)
                                {
                                    return GetFormattedValue(theTransaction.AmountPaid, context.GetOrgUnit().Currency, context);
                                }
                            }

                            return string.Empty;
                        }

                    case "BALANCE":
                        {
                            if (theTransaction.TransactionType == SalesTransactionType.CustomerOrder)
                            {
                                if (theTransaction.CustomerOrderType == CustomerOrderType.SalesOrder)
                                {
                                    decimal balance = (theTransaction.ChargeAmount + theTransaction.NetAmountWithTax) - theTransaction.AmountPaid;
                                    return GetFormattedValue(balance, context.GetOrgUnit().Currency, context);
                                }
                            }

                            return string.Empty;
                        }

                    // India receipt tax summary
                    case "COMPANYPANNO_IN":
                        {
                            var receiptHeaderInfoIndia = taxDataManager.GetReceiptHeaderInfoIndia(new ColumnSet());
                            return receiptHeaderInfoIndia != null ? receiptHeaderInfoIndia.CompanyPermanentAccountNumber : string.Empty;
                        }

                    case "VATTINNO_IN":
                        {
                            var receiptHeaderTaxInfoIndia = taxDataManager.GetReceiptHeaderTaxInfoIndia(new ColumnSet());
                            return receiptHeaderTaxInfoIndia != null ? receiptHeaderTaxInfoIndia.ValueAddedTaxTINNumber : string.Empty;
                        }

                    case "CSTTINNO_IN":
                        {
                            var receiptHeaderTaxInfoIndia = taxDataManager.GetReceiptHeaderTaxInfoIndia(new ColumnSet());
                            return receiptHeaderTaxInfoIndia != null ? receiptHeaderTaxInfoIndia.CentralSalesTaxTINNumber : string.Empty;
                        }

                    case "STCNUMBER_IN":
                        {
                            var receiptHeaderTaxInfoIndia = taxDataManager.GetReceiptHeaderTaxInfoIndia(new ColumnSet());
                            return receiptHeaderTaxInfoIndia != null ? receiptHeaderTaxInfoIndia.ServiceTaxNumber : string.Empty;
                        }

                    case "ECCNUMBER_IN":
                        {
                            var receiptHeaderTaxInfoIndia = taxDataManager.GetReceiptHeaderTaxInfoIndia(new ColumnSet());
                            return receiptHeaderTaxInfoIndia != null ? receiptHeaderTaxInfoIndia.ExciseTaxNumber : string.Empty;
                        }

                    default:
                        return string.Empty;
                }
            }

            return string.Empty;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "By design.")]
        private static string GetInfoFromSaleLineItem(ReceiptItemInfo itemInfo, SalesLine saleLine, bool isTaxIncludedInPrice, RequestContext context)
        {
            string returnValue = string.Empty;

            if (saleLine == null)
            {
                return returnValue;
            }

            switch (itemInfo.Variable.ToUpperInvariant().Replace(" ", string.Empty))
            {
                case "TAXID":
                    returnValue = saleLine.SalesTaxGroupId;
                    break;
                case "TAXPERCENT":
                    returnValue = saleLine.TotalPercentageDiscount.ToString();
                    break;
                case "ITEMNAME":
                    {
                        if (saleLine.IsGiftCardLine)
                        {
                            // Return "Gift Card"
                            returnValue = "<T:string_6150>";
                        }
                        else if (saleLine.IsInvoiceLine)
                        {
                            returnValue = saleLine.Comment;
                        }
                        else
                        {
                            List<string> itemIds = new List<string>();
                            itemIds.Add(saleLine.ItemId);

                            var getItemsRequest = new GetItemsDataRequest(itemIds);
                            var getItemsResponse = context.Execute<GetItemsDataResponse>(getItemsRequest);

                            ReadOnlyCollection<Item> items = getItemsResponse.Items;

                            returnValue = items[0].Name;
                        }

                        break;
                    }

                case "ITEMID":
                    returnValue = saleLine.ItemId;
                    break;
                case "QTY":
                    returnValue = saleLine.Quantity.ToString(ConstantFormat);
                    break;
                case "UNITPRICE":
                    {
                        string currency = context.GetOrgUnit().Currency;
                        returnValue = GetFormattedValue(saleLine.Price, currency, context);
                    }

                    break;

                case "UNITPRICEWITHTAX":
                    {
                        var store = ReceiptService.GetStoreFromContext(context);

                        if (isTaxIncludedInPrice)
                        {
                            returnValue = GetFormattedValue(saleLine.NetAmountWithTaxPerUnit(), store.Currency, context);
                        }
                        else
                        {
                            returnValue = GetFormattedValue((saleLine.NetAmountWithTax() + saleLine.TaxAmount) / saleLine.Quantity, store.Currency, context);
                        }
                    }

                    break;

                case "TOTALPRICE":
                    {
                        string currency = context.GetOrgUnit().Currency;
                        returnValue = GetFormattedValue(saleLine.NetAmount, currency, context);
                    }

                    break;
                case "TOTALPRICEWITHTAX":
                    {
                        string currency = context.GetOrgUnit().Currency;
                        returnValue = GetFormattedValue(saleLine.TotalAmount, currency, context);
                    }

                    break;
                case "ITEMUNITID":
                    returnValue = saleLine.SalesOrderUnitOfMeasure;
                    break;
                case "ITEMUNITIDNAME":
                    returnValue = saleLine.OriginalSalesOrderUnitOfMeasure;
                    break;
                case "LINEDISCOUNTAMOUNT":
                    {
                        string currency = context.GetOrgUnit().Currency;
                        returnValue = GetFormattedValue(saleLine.LineDiscount, currency, context);
                    }

                    break;
                case "LINEDISCOUNTPERCENT":
                    returnValue = saleLine.LinePercentageDiscount.ToString(ConstantFormat);
                    break;
                case "PERIODICDISCOUNTAMOUNT":
                    {
                        string currency = context.GetOrgUnit().Currency;
                        returnValue = GetFormattedValue(saleLine.PeriodicDiscount, currency, context);
                    }

                    break;
                case "PERIODICDISCOUNTPERCENT":
                    returnValue = saleLine.PeriodicPercentageDiscount.ToString(ConstantFormat);
                    break;
                case "TOTALDISCOUNTAMOUNT":
                    {
                        string currency = context.GetOrgUnit().Currency;
                        returnValue = GetFormattedValue(saleLine.TotalDiscount, currency, context);
                    }

                    break;
                case "TOTALDISCOUNTPERCENT":
                    returnValue = saleLine.TotalPercentageDiscount.ToString(ConstantFormat);
                    break;
                case "DIMENSIONCOLORID":
                    returnValue = saleLine.Variant.ColorId;
                    break;
                case "DIMENSIONCOLORVALUE":
                    returnValue = saleLine.Variant.Color;
                    break;
                case "DIMENSIONSIZEID":
                    returnValue = saleLine.Variant.SizeId;
                    break;
                case "DIMENSIONSIZEVALUE":
                    returnValue = saleLine.Variant.Size;
                    break;
                case "DIMENSIONSTYLEID":
                    returnValue = saleLine.Variant.StyleId;
                    break;
                case "DIMENSIONSTYLEVALUE":
                    returnValue = saleLine.Variant.Style;
                    break;
                case "DIMENSIONCONFIGID":
                    returnValue = saleLine.Variant.ConfigId;
                    break;
                case "DIMENSIONCONFIGVALUE":
                    returnValue = saleLine.Variant.Configuration;
                    break;
                case "LINEDELIVERYTYPE":
                    returnValue = string.Empty;
                    break;
                case "LINEDELIVERYMETHOD":
                    returnValue = saleLine.DeliveryMode;
                    break;
                case "LINEDELIVERYDATE":
                    returnValue = saleLine.RequestedDeliveryDate.ToString();
                    break;
                case "PICKUPQTY":
                    returnValue = saleLine.Quantity.ToString();
                    break;
                case "ITEMTAX":
                    returnValue = saleLine.TaxAmount.ToString();
                    break;
                case "KITCOMPONENTNAME":
                    {
                        ItemDataManager itemDataManager = new ItemDataManager(context);
                        List<string> itemIds = new List<string>();
                        itemIds.Add(saleLine.ItemId);
                        ReadOnlyCollection<Item> items = itemDataManager.GetItems(itemIds, new ColumnSet());

                        returnValue = items[0].Name;
                        break;
                    }

                case "KITCOMPONENTQTY":
                    returnValue = saleLine.Quantity.ToString(ConstantFormat);
                    break;
                case "KITCOMPONENTUNIT":
                    returnValue = saleLine.SalesOrderUnitOfMeasure;
                    break;
                case "ITEMCOMMENT":
                    {
                        // If the line is an invoice line then item name will contain the comment
                        if (!saleLine.IsInvoiceLine)
                        {
                            returnValue = saleLine.Comment;
                        }

                        break;
                    }

                case "RETURNREASON":
                    returnValue = saleLine.ReturnLabelProperties.ReturnReasonText ?? string.Empty;
                    break;
                case "RETURNLOCATION":
                    returnValue = saleLine.ReturnLabelProperties.ReturnLocationText ?? string.Empty;
                    break;
                case "RETURNWAREHOUSE":
                    returnValue = saleLine.ReturnLabelProperties.ReturnWarehouseText ?? string.Empty;
                    break;
                case "RETURNPALLETE":
                    returnValue = saleLine.ReturnLabelProperties.ReturnPalleteText ?? string.Empty;
                    break;
                default:
                    returnValue = string.Empty;
                    break;
            }

            if (returnValue == null)
            {
                returnValue = string.Empty;
            }
            else
            {
                if (itemInfo.Prefix.Length > 0)
                {
                    returnValue = itemInfo.Prefix + returnValue;
                }
            }

            return returnValue;
        }

        private static string GetInfoFromTenderLineItem(ReadOnlyCollection<TenderType> tenderTypes, ReceiptItemInfo itemInfo, TenderLine tenderLine, RequestContext context)
        {
            string returnValue = string.Empty;
            TenderType tenderTypeId = tenderTypes.Where(type => type.TenderTypeId == tenderLine.TenderTypeId).Single();

            if (tenderLine != null)
            {
                switch (itemInfo.Variable.ToUpperInvariant().Replace(" ", string.Empty))
                {
                    case "TENDERNAME":
                        string tenderType = string.Empty;

                        if (tenderTypeId.OperationId == (int)RetailOperation.PayCash)
                        {
                            tenderType = "CASH";
                        }
                        else if (tenderTypeId.OperationId == (int)RetailOperation.PayCard)
                        {
                            tenderType = "CARD";
                        }

                        if (tenderLine.Amount < 0)
                        {
                            if (tenderTypeId.OperationId == (int)RetailOperation.PayCard)
                            {
                                returnValue = "charge back";
                            }
                            else
                            {
                                returnValue = "change back";
                            }

                            returnValue += " (" + tenderType + ")";
                        }
                        else
                            returnValue = tenderTypeId.Name.Replace("Pay", string.Empty);

                        // Check if the tenderline contains foreign currency.
                        if (tenderLine.AmountInTenderedCurrency != tenderLine.Amount)
                        {
                            returnValue += " (" + GetFormattedValue(tenderLine.AmountInTenderedCurrency, tenderLine.Currency, context) + " " + tenderLine.Currency + ")";
                        }

                        break;
                    case "TENDERAMOUNT":
                        {
                            string currency = context.GetOrgUnit().Currency;
                            returnValue = GetFormattedValue(tenderLine.Amount, currency, context);
                        }

                        break;
                    case "TENDERCOMMENT":
                        {
                            switch ((RetailOperation)tenderTypeId.OperationId)
                            {
                                case RetailOperation.PayCreditMemo:
                                    returnValue = tenderLine.CreditMemoId;
                                    break;
                                case RetailOperation.PayGiftCertificate:
                                    returnValue = tenderLine.GiftCardId;
                                    break;
                                case RetailOperation.PayLoyalty:
                                    returnValue = tenderLine.LoyaltyCardId;
                                    break;
                                default:
                                    returnValue = string.Empty;
                                    break;
                            }
                        }

                        break;
                }

                if (returnValue == null)
                {
                    returnValue = string.Empty;
                }

                if (returnValue.Length > itemInfo.Length)
                {
                    returnValue = Wrap(returnValue, itemInfo);
                }
            }

            return returnValue;
        }

        /// <summary>
        /// Wrap the string into a new line if the length exceeds the template length.
        /// </summary>
        /// <param name="text">The text to wrap.</param>
        /// <param name="itemInfo">The receipt item info.</param>
        /// <returns>The wrapped string.</returns>
        private static string Wrap(string text, ReceiptItemInfo itemInfo)
        {
            // Define our regex
            string pattern = @"(?<Line>.{1," + itemInfo.Length + @"})(?:\W)";

            // Split the string based on the pattern
            string[] lines = Regex.Split(
                text,
                pattern,
                RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline | RegexOptions.ExplicitCapture | RegexOptions.CultureInvariant);

            // Empty string to return value
            string returnValue = string.Empty;

            // Concacitante all lines adding a hard return on each line
            foreach (string line in lines)
            {
                if (!string.IsNullOrEmpty(line))
                {
                    returnValue += GetAlignmentSettings(line.Trim(), itemInfo) + Environment.NewLine;
                }
            }

            // No NewLine at the end of the string.
            return returnValue.TrimEnd(Environment.NewLine.ToCharArray());
        }

        private static int CountWordLength(string wordString)
        {
            int count = 0;
            foreach (char ch in wordString)
            {
                if ((ch != '\r') && (ch != '\n'))
                {
                    count += 1;
                }
            }

            return count;
        }

        /// <summary>
        /// This method decide whether display cancellation charge without any tax on customer order.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>The flag indicating whether to display cancellation charge without any tax.</returns>
        private static bool IsExcludeTaxInCancellationCharge(RequestContext context)
        {
            return context.GetChannelConfiguration().CountryRegionISOCode == CountryRegionISOCode.IN;
        }

        /// <summary>
        /// Gets the store by identifier.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The store.</returns>
        private static OrgUnit GetStoreFromContext(RequestContext context)
        {
            GetStoreDataServiceRequest request = new GetStoreDataServiceRequest(context.GetPrincipal().ChannelId);
            return context.Execute<SingleEntityDataServiceResponse<OrgUnit>>(request).Entity;
        }
    }
}
