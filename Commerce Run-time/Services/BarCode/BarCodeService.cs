﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/


namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Represents an implementation of the BarCode service.
    /// </summary>
    public class BarcodeService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(ProcessMaskSegmentsServiceRequest),
                    typeof(GetBarcodeTypeServiceRequest),
                    typeof(CalculateQuantityFromPriceServiceRequest)
                };
            }
        }

        /// <summary>
        /// Executes the specified service serviceRequest.
        /// </summary>
        /// <param name="request">The type of serviceRequest.</param>
        /// <returns>The response.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;

            if (requestType == typeof(ProcessMaskSegmentsServiceRequest))
            {
                response = ProcessMaskSegments((ProcessMaskSegmentsServiceRequest)request);
            }
            else if (requestType == typeof(GetBarcodeTypeServiceRequest))
            {
                response = GetBarcodeType((GetBarcodeTypeServiceRequest)request);
            }
            else if (requestType == typeof(CalculateQuantityFromPriceServiceRequest))
            {
                response = CalculateQuantityFromPrice((CalculateQuantityFromPriceServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        private static CalculateQuantityFromPriceServiceResponse CalculateQuantityFromPrice(CalculateQuantityFromPriceServiceRequest request)
        {
            decimal quantity = 0;

            if (request.BarcodePrice > 0)
            {
                if (request.DefaultProductPrice != request.BarcodePrice && request.DefaultProductPrice != decimal.Zero)
                {
                    quantity = request.BarcodePrice / request.DefaultProductPrice;

                    var roundingRequest = new GetRoundQuantityServiceRequest(quantity, request.UnitOfMeasure);
                    var response = request.RequestContext.Execute<GetRoundQuantityServiceResponse>(roundingRequest);

                    quantity = response.RoundedValue;
                }
            }

            return new CalculateQuantityFromPriceServiceResponse(quantity);
        }

        private static GetBarcodeTypeServiceResponse GetBarcodeType(GetBarcodeTypeServiceRequest serviceRequest)
        {
            if (string.IsNullOrEmpty(serviceRequest.BarcodeId))
            {
                throw new ArgumentNullException(serviceRequest.BarcodeId);
            }

            var barcodeDataManager = new BarcodeDataManager(serviceRequest.RequestContext);
            string barcodePrefix = serviceRequest.BarcodeId.Substring(0, 1);

            IEnumerable<BarcodeMask> barcodeMasks = barcodeDataManager.GetBarcodeMask(barcodePrefix);
            BarcodeMask barcodeMask = null;

            bool found = false;

            foreach (var bcmask in barcodeMasks.Where(mask => !string.IsNullOrEmpty(mask.Prefix) && serviceRequest.BarcodeId.Length >= mask.Prefix.Length))
            {
                barcodeMask = bcmask;
                barcodePrefix = serviceRequest.BarcodeId.Substring(0, bcmask.Prefix.Length);

                if (bcmask.Prefix == barcodePrefix)
                {
                    if (serviceRequest.BarcodeId.Length == bcmask.Mask.Length)
                    {
                        found = true;
                        break;
                    }
                }
            }

            if (found == false)
            {
                barcodeMask = null;
            }

            return new GetBarcodeTypeServiceResponse(barcodeMask);
        }

        private static ProcessMaskSegmentsServiceResponse ProcessMaskSegments(ProcessMaskSegmentsServiceRequest serviceRequest)
        {
            var barcodeDataManager = new BarcodeDataManager(serviceRequest.RequestContext);

            var barcodeInfo = new Barcode { BarcodeId = serviceRequest.BarcodeId };

            IEnumerable<BarcodeMaskSegment> barCodeMaskSegments = barcodeDataManager.GetBarcodeMaskSegment(serviceRequest.BarcodeMask.MaskId);

            int position = serviceRequest.BarcodeMask.Prefix.Length;

            barcodeInfo.Convert(serviceRequest.BarcodeMask);

            foreach (BarcodeMaskSegment segment in barCodeMaskSegments)
            {
                var segmentType = (BarcodeSegmentType)segment.MaskType;

                switch (segmentType)
                {
                    case BarcodeSegmentType.Item:
                        {
                            LoadItemInfo(serviceRequest, barcodeInfo, position, segment);

                            break;
                        }

                    case BarcodeSegmentType.AnyNumber:
                    case BarcodeSegmentType.CheckDigit: // Check Digit is not implemented yet functionality in RetailServer. 
                        {
                            break;    
                        }

                    case BarcodeSegmentType.Price:
                        {
                            LoadPriceInfo(serviceRequest, barcodeInfo, position, segment);

                            if (barcodeInfo.BarcodePrice > 0 && barcodeInfo.ItemId != null)
                            {
                                ProductPrice productPrice = GetItemPrice(serviceRequest.RequestContext, barcodeInfo.ItemId, barcodeInfo.InventoryDimensionId, barcodeInfo.UnitId, string.Empty);
                                decimal defaultProductPrice = productPrice.AdjustedPrice;

                                var calculateQuantityRequest = new CalculateQuantityFromPriceServiceRequest(barcodeInfo.BarcodePrice, defaultProductPrice, barcodeInfo.UnitId);
                                CalculateQuantityFromPriceServiceResponse calculateQuantityResponse = serviceRequest.RequestContext.Execute<CalculateQuantityFromPriceServiceResponse>(calculateQuantityRequest);

                                barcodeInfo.BarcodeQuantity = calculateQuantityResponse.BarcodeQuantity;
                            }
                            
                            break;
                        }

                    case BarcodeSegmentType.Quantity:
                        {
                            LoadQuantityInfo(serviceRequest, barcodeInfo, position, segment);
                            break;
                        }

                    case BarcodeSegmentType.DiscountCode:
                        {
                            barcodeInfo.DiscountCode = serviceRequest.BarcodeId.Substring(position, segment.Length).TrimStart('0');
                            break;
                        }

                    case BarcodeSegmentType.GiftCard:
                        {
                            barcodeInfo.GiftCardNumber = barcodeInfo.Prefix + serviceRequest.BarcodeId.Substring(position, segment.Length);
                            break;
                        }

                    case BarcodeSegmentType.LoyaltyCard:
                        {
                            barcodeInfo.LoyaltyCardNumber = barcodeInfo.Prefix + serviceRequest.BarcodeId.Substring(position, segment.Length);
                            break;
                        }

                    case BarcodeSegmentType.SizeDigit:
                        {
                            barcodeInfo.InventorySizeId = serviceRequest.BarcodeId.Substring(position, segment.Length);
                            break;
                        }

                    case BarcodeSegmentType.ColorDigit:
                        {
                            barcodeInfo.InventoryColorId = serviceRequest.BarcodeId.Substring(position, segment.Length);
                            break;
                        }

                    case BarcodeSegmentType.StyleDigit:
                        {
                            barcodeInfo.InventoryStyleId = serviceRequest.BarcodeId.Substring(position, segment.Length);
                            break;
                        }

                    case BarcodeSegmentType.EANLicenseCode:
                        {
                            barcodeInfo.EANLicenseId = serviceRequest.BarcodeId.Substring(position, segment.Length);
                            break;
                        }

                    case BarcodeSegmentType.Employee:
                        {
                            barcodeInfo.EmployeeId = serviceRequest.BarcodeId.Substring(position, segment.Length);
                            break;
                        }

                    case BarcodeSegmentType.Salesperson:
                        {
                            barcodeInfo.SalespersonId = serviceRequest.BarcodeId.Substring(position, segment.Length);
                            break;
                        }

                    case BarcodeSegmentType.Customer:
                        {
                            barcodeInfo.CustomerId = serviceRequest.BarcodeId.Substring(position, segment.Length);
                            break;
                        }

                    case BarcodeSegmentType.DataEntry:
                        {
                            barcodeInfo.DataEntry = serviceRequest.BarcodeId.Substring(position, segment.Length);
                            break;
                        }
                        
                    default:
                        {
                            break;
                        }
                }

                position = position + segment.Length;
            }

            return new ProcessMaskSegmentsServiceResponse(barcodeInfo);
        }

        private static void LoadQuantityInfo(ProcessMaskSegmentsServiceRequest serviceRequest, Barcode barcode, int position, BarcodeMaskSegment segment)
        {
            string strBarCodeQuantity = serviceRequest.BarcodeId.Substring(position, segment.Length);
            
            decimal barCodeQuantity;
            
            if (decimal.TryParse(strBarCodeQuantity, out barCodeQuantity))
            {
                barCodeQuantity = barCodeQuantity / (decimal)Math.Pow(10, segment.Decimals);    
            }
            else
            {
                throw new DataValidationException(DataValidationErrors.InvalidFormat, "Cannot Parse to decimal, Invalid format {0}", strBarCodeQuantity);
            }

            barcode.BarcodeQuantity = barCodeQuantity;
            barcode.Decimals = segment.Decimals;
        }

        private static void LoadPriceInfo(ProcessMaskSegmentsServiceRequest serviceRequest, Barcode barcode, int position, BarcodeMaskSegment segment)
        {
            string strBarCodePrice = serviceRequest.BarcodeId.Substring(position, segment.Length);

            decimal barCodePrice;

            if (decimal.TryParse(strBarCodePrice, out barCodePrice))
            {
                barCodePrice = barCodePrice / (decimal)Math.Pow(10, segment.Decimals);
            }
            else
            {
                throw new DataValidationException(DataValidationErrors.InvalidFormat, "Cannot Parse to decimal, Invalid format {0}", strBarCodePrice);
            }

            barcode.BarcodePrice = barCodePrice;
            barcode.Decimals = segment.Decimals;
        }

        private static void LoadItemInfo(ProcessMaskSegmentsServiceRequest serviceRequest, Barcode barcode, int position, BarcodeMaskSegment segment)
        {
            var itemDataManager = new ItemDataManager(serviceRequest.RequestContext);

            barcode.ItemBarcode = serviceRequest.BarcodeId.Substring(0, position + segment.Length);
            barcode.ItemBarcode += '%';

            var itemBarcode = itemDataManager.GetItemsByBarcode(barcode.ItemBarcode, new ColumnSet());

            if (itemBarcode == null)
            {
                barcode.ItemBarcode = barcode.ItemBarcode.Substring(0, barcode.ItemBarcode.Length - 1);
                barcode.ItemBarcode = barcode.ItemBarcode + Convert.ToString(CalculateCheckDigit(barcode.ItemBarcode), CultureInfo.CurrentCulture);

                itemBarcode = itemDataManager.GetItemsByBarcode(barcode.ItemBarcode, new ColumnSet());
            }

            if (itemBarcode != null)
            {
                barcode.Convert(itemBarcode);
            }
        }

        private static ProductPrice GetItemPrice(RequestContext requestContext, string itemId, string inventDimId, string unitOfMeasure, string customerAcctNumber)
        {
            SalesTransaction salesTransaction = new SalesTransaction
            {
                Id = Guid.NewGuid().ToString(),
                CustomerId = customerAcctNumber,
            };

            SalesLine salesLine = new SalesLine()
            {
                LineId = System.Guid.NewGuid().ToString("N"),
                ItemId = itemId,
                InventoryDimensionId = inventDimId,
                SalesOrderUnitOfMeasure = unitOfMeasure,
                Quantity = 1m,
            };
            salesTransaction.SalesLines.Add(salesLine);

            GetIndependentPriceDiscountServiceRequest priceRequest = new GetIndependentPriceDiscountServiceRequest(salesTransaction);

            GetPriceServiceResponse pricingServiceResponse = requestContext.Execute<GetPriceServiceResponse>(priceRequest);

            SalesLine resultLine = pricingServiceResponse.Transaction.SalesLines[0];

            ProductPrice productPrice = GetProductPrice(
                resultLine.ItemId,
                resultLine.InventoryDimensionId,
                resultLine.BasePrice,
                resultLine.AgreementPrice,
                resultLine.AdjustedPrice,
                requestContext.GetChannelConfiguration().Currency);

            return productPrice;
        }

        private static ProductPrice GetProductPrice(string itemId, string inventoryDimensionId, decimal basePrice, decimal tradeAgreementPrice, decimal adjustedPrice, string currencyCode)
        {
            ProductPrice productPrice = new ProductPrice();

            productPrice.ItemId = itemId;
            productPrice.InventoryDimensionId = inventoryDimensionId;
            productPrice.BasePrice = basePrice;
            productPrice.TradeAgreementPrice = tradeAgreementPrice;
            productPrice.AdjustedPrice = adjustedPrice;
            productPrice.CurrencyCode = currencyCode;

            return productPrice;
        }

        /// <summary>
        /// Calculates the check digit for a barcode, without a check digit using the Universal Product Code (UPC) algorithm.
        /// </summary>
        /// <remarks>If the barcode contains non-digits then -1 is returned.</remarks>
        /// <param name="barcode">Barcode without a check digit.</param>
        /// <returns>The calculated check digit.</returns>
        private static int CalculateCheckDigit(string barcode)
        {
            int even = 0;
            int odd = 0;
            int total = 0;
            int checkDigit = 0;

            for (int i = 0; i < barcode.Length; i++)
            {
                int temp;

                if (int.TryParse(barcode.Substring(barcode.Length - 1 - i, 1), out temp))
                {
                    if (((i + 1) % 2) == 0)
                    {
                        even += temp;
                    }
                    else
                    {
                        odd += temp;
                    }
                }
                else
                {   // Not valid as it contians non-numeric data
                    return -1;
                }
            }

            total = (odd * 3) + even;
            checkDigit = 10 - (total % 10);

            return checkDigit;
        }
    }
}
