﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Implementation for product service.
    /// </summary>
    public class ProductService : IRequestHandler
    {
        private static readonly Type[] SupportedRequestTypesArray = new Type[]
        {
            typeof(ProductSearchServiceRequest),
            typeof(GetProductsServiceRequest),
            typeof(GetProductRefinersRequest)
        };

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get { return SupportedRequestTypesArray; }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            ThrowIf.Null(request, "request");
            Response response;

            if (request is ProductSearchServiceRequest)
            {
                response = SearchProducts((ProductSearchServiceRequest)request);
            }
            else if (request is GetProductsServiceRequest)
            {
                response = GetProducts((GetProductsServiceRequest)request);
            }
            else if (request is GetProductRefinersRequest)
            {
                response = GetProductRefiners((GetProductRefinersRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request type '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        private static ProductSearchServiceResponse SearchProducts(ProductSearchServiceRequest request)
        {
            ProductSearchServiceResponse response;

            long currentChannelId = request.RequestContext.GetPrincipal().ChannelId;
            if (request.QueryCriteria.Context.IsRemoteLookup(currentChannelId))
            {
                response = SearchRemoteProducts(request);
            }
            else
            {
                response = SearchProductsLocal(request);
                var results = response.ProductSearchResult;

                // Download product data from AX if it is not found locally and the DownloadProductData flag is set.
                if (results.Results.Count < request.QueryCriteria.ItemIds.Distinct().Count() && request.QueryCriteria.DownloadProductData == true)
                {
                    var manager = new ProductDataManager(request.RequestContext);

                    List<string> itemIds = new List<string>();

                    foreach (ProductLookupClause productLookupClause in request.QueryCriteria.ItemIds)
                    {
                        itemIds.Add(productLookupClause.ItemId);
                    }

                    var getProductDataServiceRequest = new GetProductDataServiceRequest(itemIds);

                    var getProductDataServiceResponse = request.RequestContext.Runtime.Execute<GetProductDataServiceResponse>(getProductDataServiceRequest, request.RequestContext);
                    var productsXml = getProductDataServiceResponse.ProductDataXml;

                    manager.SaveProductData(productsXml);

                    // Reset the channel context since the remote product data has been saved locally.
                    request.QueryCriteria.Context.ChannelId = request.RequestContext.GetPrincipal().ChannelId;
                    request.QueryCriteria.Context.CatalogId = 0;

                    response = SearchProductsLocal(request);
                }
            }

            return response;
        }

        private static ProductSearchServiceResponse SearchProductsLocal(ProductSearchServiceRequest request)
        {
            var queryCriteria = request.QueryCriteria;

            if (queryCriteria.Context == null || queryCriteria.Context.ChannelId.HasValue == false)
            {
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "The channel identifier on the query criteria context must be specified.");
            }

            GetProductsServiceRequest getProductsServiceRequest = new GetProductsServiceRequest(
                queryCriteria,
                request.RequestContext.GetChannelConfiguration().DefaultLanguageId,
                false /*FetchProductsAvailableInFuture*/,
                request.QueryResultSettings,
                queryCriteria.IsOnline);

            ProductSearchResult result = request.RequestContext.Runtime.Execute<ProductSearchServiceResponse>(
                getProductsServiceRequest,
                request.RequestContext).ProductSearchResult;

            // populate the active variant product id if this was a search by item or barcode
            if ((!queryCriteria.ItemIds.IsNullOrEmpty() || !queryCriteria.Barcodes.IsNullOrEmpty()) && !queryCriteria.SkipVariantExpansion)
            {
                ProductDatabaseAccessor.SetActiveVariants(queryCriteria, result);
            }

            if (!queryCriteria.Refinement.IsNullOrEmpty())
            {
                result.Results = ProductRefinement.RefineProducts(result.Results, queryCriteria.Refinement);
            }

            return new ProductSearchServiceResponse(result);
        }

        /// <summary>
        /// Get products based on the <paramref name="serviceRequest"/>.
        /// </summary>
        /// <param name="serviceRequest">The product retrieval request.</param>
        /// <returns>The product retrieval response.</returns>
        private static ProductSearchServiceResponse GetProducts(GetProductsServiceRequest serviceRequest)
        {
            GetProductsHelper getProductsHandler = new GetProductsHelper(serviceRequest);
            return new ProductSearchServiceResponse(getProductsHandler.GetProducts());
        }

        private static ProductSearchServiceResponse SearchRemoteProducts(ProductSearchServiceRequest request)
        {
            ProductSearchResult results = null;

            bool searchByProduct = request.QueryCriteria.Ids != null && request.QueryCriteria.Ids.Any();
            bool searchByCategory = request.QueryCriteria.CategoryIds != null && request.QueryCriteria.CategoryIds.Any();
            bool searchByKeyword = !string.IsNullOrEmpty(request.QueryCriteria.SearchCondition);

            if (searchByCategory && request.QueryCriteria.CategoryIds.HasMultiple())
            {
                throw new NotSupportedException("Only a single category identifier can be specified when searching remotely.");
            }

            if (!(searchByProduct ^ searchByCategory ^ searchByKeyword))
            {
                throw new NotSupportedException("When searching remotely you can search only by id, category or keyword.");
            }

            string attributeIds = string.Concat(RetailProductChannelProductAttributeId.ProductName, ",", RetailProductChannelProductAttributeId.Image);

            if (searchByProduct)
            {
                GetProductDataServiceRequest getProductDataServiceRequest = new GetProductDataServiceRequest(request.QueryCriteria.Ids);
                var getProductDataResponse = request.RequestContext.Execute<GetProductDataServiceResponse>(getProductDataServiceRequest);
                var productsXml = getProductDataResponse.ProductDataXml;

                var manager = new ProductDataManager(request.RequestContext);
                manager.SaveProductData(productsXml);

                // Reset the channel context since the remote product data has been saved locally.
                request.QueryCriteria.Context.ChannelId = request.RequestContext.GetPrincipal().ChannelId;
                request.QueryCriteria.Context.CatalogId = 0;

                results = SearchProductsLocal(request).ProductSearchResult;
            }
            else if (searchByCategory)
            {
                var getProductsByCategoryRequest = new GetRemoteProductsByCategoryServiceRequest(
                    request.QueryCriteria.Context.ChannelId.Value,
                    request.QueryCriteria.Context.CatalogId,
                    request.QueryCriteria.CategoryIds.First(),
                    attributeIds,
                    request.QueryCriteria.IncludeProductsFromDescendantCategories);

                getProductsByCategoryRequest.QueryResultSettings = request.QueryResultSettings;

                var response = request.RequestContext.Execute<ProductSearchServiceResponse>(getProductsByCategoryRequest);
                results = response.ProductSearchResult;
            }
            else if (searchByKeyword)
            {
                var getProductsByKeywordRequest = new GetRemoteProductsByKeywordServiceRequest(
                    request.QueryCriteria.Context.ChannelId.Value,
                    request.QueryCriteria.Context.CatalogId,
                    request.QueryCriteria.SearchCondition,
                    attributeIds);

                getProductsByKeywordRequest.QueryResultSettings = request.QueryResultSettings;

                var response = request.RequestContext.Execute<ProductSearchServiceResponse>(getProductsByKeywordRequest);
                results = response.ProductSearchResult;
            }

            return new ProductSearchServiceResponse(results);
        }

        private static GetProductRefinersServiceResponse GetProductRefiners(GetProductRefinersRequest request)
        {
            return ProductRefinement.GetProductRefiners(request);
        }
    }
}