﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Handlers;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Encapsulates the implementation of the service to save transaction for certain operations.
    /// </summary>
    public class TransactionLogService : INamedRequestHandler
    {
        /// <summary>
        /// The transaction id of the transaction that will not be saved into the channel database.
        /// </summary>
        private const string SkippedTransactionId = "-1";

        /// <summary>
        /// Gets the unique name for this request handler.
        /// </summary>
        public string HandlerName
        {
            get { return ServiceTypes.TransactionLogService; }
        }

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(SaveTransactionLogRequest)
                };
            }
        }

        /// <summary>
        /// Executes the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;
            if (requestType == typeof(SaveTransactionLogRequest))
            {
                response = this.SaveTransactionLog((SaveTransactionLogRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        private static void SaveLogonTransaction(RequestContext context, TransactionType transactionType, string transactionId, Device device, string staffId)
        {
            // whenver we perform a logon-logoff, if transaction id is provided with const value SkippedTransactionId
            // it means it is a non-interactive logon/logoff (i.e. a background operation like transaction upload is being performed)
            // and it should not be considered as a transaction
            if (!TransactionLogService.SkippedTransactionId.Equals(transactionId, StringComparison.OrdinalIgnoreCase))
            {
                TransactionLogService.SaveTransaction(context, transactionType, transactionId, device, staffId);
            }
        }
        
        private static void SaveTransaction(RequestContext context, TransactionType transactionType, string transactionId, Device device, string staffId)
        {
            string terminalId;
            string storeId;

            if (device != null)
            {
                terminalId = device.TerminalId;
                var store = TransactionLogService.GetStoreById(context, device.ChannelId);
                storeId = store.OrgUnitNumber;
            }
            else
            {
                if (context.GetPrincipal() != null && context.GetTerminal() != null)
                {
                    terminalId = context.GetTerminal().TerminalId;
                    var store = new ChannelDataManager(context).GetStoreById(context.GetPrincipal().ChannelId);
                    storeId = store.OrgUnitNumber;
                }
                else
                {
                    return;
                }
            }

            TransactionLog transaction = new TransactionLog()
            {
                TransactionType = transactionType,
                Id = transactionId,
                StaffId = string.IsNullOrWhiteSpace(staffId) ? context.GetPrincipal().UserId : staffId,
                TerminalId = terminalId,
                StoreId = storeId
            };

            TransactionLogService.LogTransaction(context, transaction);
        }

        /// <summary>
        /// Logs the transaction.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="log">The log.</param>
        private static void LogTransaction(RequestContext context, TransactionLog log)
        {
            SaveTransactionLogDataServiceRequest request = new SaveTransactionLogDataServiceRequest(log);
            context.Runtime.Execute<NullResponse>(request, context);
        }

        /// <summary>
        /// Gets the store by identifier.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="channelId">The channel identifier.</param>
        /// <returns>The store.</returns>
        private static OrgUnit GetStoreById(RequestContext context, long channelId)
        {
            GetStoreDataServiceRequest request = new GetStoreDataServiceRequest(channelId);
            return context.Runtime.Execute<SingleEntityDataServiceResponse<OrgUnit>>(request, context).Entity;
        }

        /// <summary>
        /// Saves the transaction to channel database.
        /// </summary>
        /// <param name="request">The request to save transaction log.</param>
        /// <returns>The empty response.</returns>
        private NullResponse SaveTransactionLog(SaveTransactionLogRequest request)
        {
            switch (request.TransactionType)
            {
                case TransactionType.LogOn:
                case TransactionType.LogOff:
                    TransactionLogService.SaveLogonTransaction(request.RequestContext, request.TransactionType, request.TransactionId, request.Device, request.StaffId);
                    break;

                case TransactionType.ResetPassword:
                case TransactionType.ChangePassword:
                case TransactionType.PrintX:
                case TransactionType.PrintZ:
                    TransactionLogService.SaveTransaction(request.RequestContext, request.TransactionType, request.TransactionId, request.Device, request.StaffId);
                    break;

                default:
                    throw new NotSupportedException(
                        string.Format("The transaction type {0} is not supported in TransactionLogService.", request.TransactionType.ToString()));
            }

            return new NullResponse();
        }
    }
}
