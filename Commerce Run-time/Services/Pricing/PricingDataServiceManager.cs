﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine;

    /// <summary>
    /// Pricing data service manager class.
    /// </summary>
    public class PricingDataServiceManager : IPricingDataManagerV2
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PricingDataServiceManager"/> class.
        /// </summary>
        /// <param name="requestContext">The request context.</param>
        public PricingDataServiceManager(RequestContext requestContext)
        {
            this.RequestContext = requestContext;
        }

        /// <summary>
        /// Gets channel price configuration.
        /// </summary>
        public ChannelPriceConfiguration ChannelPriceConfiguration
        {
            get
            {
                return new ChannelPriceConfiguration()
                {
                    Company = this.RequestContext.GetChannelConfiguration().InventLocationDataAreaId,
                    CompanyCurrency = this.RequestContext.GetChannelConfiguration().CompanyCurrency,
                    ChannelTimeZoneId = this.RequestContext.GetChannelConfiguration().TimeZoneInfoId,
                };
            }
        }

        /// <summary>
        /// Gets a value indicating whether the cache is disabled.
        /// </summary>
        public bool IsCacheDisabled
        {
            get { return this.RequestContext.Runtime.Configuration.CacheControl.IsCachingDisabled; }
        }

        private RequestContext RequestContext { get; set; }

        /// <summary>
        /// Get all retail price adjustments matching the given product, channel price groups, currency, unit of measure, and date.
        /// </summary>
        /// <param name="productId">Record identifier of the product to look for.</param>
        /// <param name="distinctProductVariantId">Record identifier of the discount product variant to look for. 0 if not a variant.</param>
        /// <param name="priceGroupIds">Record identifiers which the channel belongs to.</param>
        /// <param name="currencyCode">Currency code to filter by.</param>
        /// <param name="unitOfMeasure">Optional unit of measure to filter by.</param>
        /// <param name="dateToCheck">Get only price adjustments active on this date.</param>
        /// <returns>All price adjustment rules which may apply to the item and context given.</returns>
        public ReadOnlyCollection<PriceAdjustment> FindPriceAdjustments(
            long productId, 
            long distinctProductVariantId, 
            IEnumerable<long> priceGroupIds,
            string currencyCode, 
            string unitOfMeasure, 
            DateTimeOffset dateToCheck)
        {
            return this.ExecuteDataService<PriceAdjustment>(new FindPriceAdjustmentsDataServiceRequest(
                                                                    productId, 
                                                                    distinctProductVariantId, 
                                                                    priceGroupIds, 
                                                                    currencyCode, 
                                                                    unitOfMeasure, 
                                                                    dateToCheck));
        }

        /// <summary>
        /// This function retrieves all possible price agreements for the given arguments (item, customer, currency, etc.),
        /// item relation code (item/group/all), and account relation code (customer/price group/all).
        /// </summary>
        /// <param name="agreementType">The type of the trade agreement to find (price, line discount, etc).</param>
        /// <param name="itemCode">Item relation code to look for (in order: item/group/all).</param>
        /// <param name="itemRelation">Item relation code to search by, e.g. item Id, item group Id, etc.</param>
        /// <param name="accountCode">Account relation code to look for (in order: customer/price group/all).</param>
        /// <param name="accountRelations">Account relation codes to search by, e.g. customer Id, customer price group Id, etc.</param>
        /// <param name="unitId">Optional unit to filter trade agreements. Ignored if empty string.</param>
        /// <param name="currencyCode">Currency code to filter trade agreements by.</param>
        /// <param name="quantity">Current quantity. Used to select only trade agreements that apply to this amount of items.</param>
        /// <param name="variant">Optional variant. Contains product dimension Ids to search by.</param>
        /// <param name="activeDate">The date to check.</param>
        /// <returns>
        /// Collection of applicable price agreements, sorted by price amount ascending.
        /// </returns>
        public ReadOnlyCollection<TradeAgreement> FindTradeAgreements(
            PriceDiscountType agreementType,
            PriceDiscountItemCode itemCode,
            string itemRelation,
            PriceDiscountAccountCode accountCode,
            IEnumerable<string> accountRelations,
            string unitId,
            string currencyCode,
            decimal quantity,
            ProductVariant variant,
            DateTimeOffset activeDate)
        {
            return this.ExecuteDataService<TradeAgreement>(new FindTradeAgreementsDataServiceRequest(
                                                                    agreementType, 
                                                                    itemCode, 
                                                                    itemRelation, 
                                                                    accountCode, 
                                                                    accountRelations, 
                                                                    unitId, 
                                                                    currencyCode, 
                                                                    quantity, 
                                                                    variant, 
                                                                    activeDate));
        }

        /// <summary>
        /// Get the discount codes (aka 'promo codes') associated with the given discount offer identifiers.
        /// </summary>
        /// <param name="offerIds">The offer identifiers whose discount codes are being found.</param>
        /// <param name="columns">The columns to retrieve.</param>
        /// <returns>Discounts codes found for given offers. Empty if none found.</returns>
        public ReadOnlyCollection<DiscountCode> GetDiscountCodesByOfferId(IEnumerable<string> offerIds, ColumnSet columns)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetDiscountCodesByOfferId", 1))
            {
                return this.ExecuteDataService<DiscountCode>(new EntityDataServiceRequest<IEnumerable<string>, DiscountCode>(offerIds));
            }
        }

        /// <summary>
        /// Returns all line group configurations assigned to the given mix and match offer
        /// and used by the lines on the offer.
        /// </summary>
        /// <param name="offerId">The mix and match offer identifier whose line groups are returned.</param>
        /// <param name="settings">The query result settings.</param>
        /// <returns>Collection of line groups with required triggered amounts assigned to this offer.</returns>
        public ReadOnlyCollection<MixAndMatchLineGroup> GetMixAndMatchLineGroupsByOffer(string offerId, QueryResultSettings settings)
        {
            return this.ExecuteDataService<MixAndMatchLineGroup>(new EntityDataServiceRequest<string, MixAndMatchLineGroup>(offerId));
        }

        /// <summary>
        /// Gets the discount codes.
        /// </summary>
        /// <param name="offerId">The offer id (optional, exact match with OfferId).</param>
        /// <param name="discountCode">The discount code (optional, exact match with DiscountCode).</param>
        /// <param name="keyword">The keyword (optional, partial match with OfferId or discount name).</param>
        /// <param name="activeDate">The active date.</param>
        /// <param name="resultSettings">The result settings.</param>
        /// <returns>
        /// The discount codes that matches the given conditions.
        /// </returns>
        /// <remarks>
        /// It seems this method is called only very rarely, and the results are fairly volatile. As such, the call/result is not cached.
        /// </remarks>
        public ReadOnlyCollection<DiscountCode> GetDiscountCodes(
            string offerId,
            string discountCode,
            string keyword,
            DateTime activeDate,
            QueryResultSettings resultSettings)
        {
            return this.ExecuteDataService<DiscountCode>(new GetDiscountCodesDataServiceRequest(
                                                                    offerId,
                                                                    discountCode,
                                                                    keyword,
                                                                    activeDate,
                                                                    resultSettings));
        }

        /// <summary>
        /// Get all retail periodic discounts (offer, quantity discounts, and mix and match discount) configurations
        ///  which match the given product and context.
        /// </summary>
        /// <param name="productId">Record identifier of the product to search by.</param>
        /// <param name="variantId">Record identifier of the distinct product variant to search by. 0 if not a variant.</param>
        /// <param name="priceGroupIds">Record identifiers of the channel's price groups.</param>
        /// <param name="currencyCode">Currency code to filter by.</param>
        /// <param name="activeDate">Get offers which are active this date.</param>
        /// <returns>All periodic discount rules which may apply to the item and context given.</returns>
        public ReadOnlyCollection<PeriodicDiscount> GetPeriodicDiscounts(long productId, long variantId, IEnumerable<long> priceGroupIds, string currencyCode, DateTimeOffset activeDate)
        {
            return this.ExecuteDataService<PeriodicDiscount>(
                new GetPeriodicDiscountsDataServiceRequest(
                        productId, 
                        variantId, 
                        priceGroupIds, 
                        currencyCode, 
                        activeDate));
        }

        /// <summary>
        /// Gets the price groups.
        /// </summary>
        /// <param name="settings">The query result settings.</param>
        /// <returns>
        /// A collection of price groups.
        /// </returns>
        public ReadOnlyCollection<PriceGroup> GetPriceGroups(QueryResultSettings settings)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetPriceGroups", 1))
            {
                return this.ExecuteDataService<PriceGroup>(new EntityDataServiceRequest<PriceGroup>());
            }
        }

        /// <summary>
        /// Retrieves PriceParameters from the database. This indicates which types of 
        /// trade agreements are active for various combinations of customer and item types.
        /// </summary>
        /// <param name="columns">The columns to retrieve.</param>
        /// <returns>The first (and only) row in PriceParameters in the database.</returns>
        public PriceParameters GetPriceParameters(ColumnSet columns)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetPriceParameters", 1))
            {
                var response = this.ExecuteDataService<PriceParameters>(new EntityDataServiceRequest<PriceParameters>());

                return response.Single();
            }
        }

        /// <summary>
        /// Get the quantity discount threshold level which the given quantity qualifies for on the given offer.
        /// </summary>
        /// <param name="offerId">The identifier of the quantity discount offer to use.</param>
        /// <param name="quantity">Number of items to apply to the given offer.</param>
        /// <returns>Threshold configuration with amount for the given quantity and discount.</returns>
        public QuantityDiscountLevel GetQuantityDiscountLevelByQuantity(string offerId, decimal quantity)
        {
            var response = this.ExecuteDataService<QuantityDiscountLevel>(new GetQuantityDiscountDataServiceRequest(offerId, quantity));

            return response.Single();
        }

        /// <summary>
        /// Get the periodic discount validation period with specified identifier.
        /// </summary>
        /// <param name="periodId">The identifier of the period to find.</param>
        /// <param name="columns">The columns to retrieve.</param>
        /// <returns>Periodic discount validation period matching given identifier. Null if none found.</returns>
        public ValidationPeriod GetValidationPeriodById(string periodId, ColumnSet columns)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetValidationPeriodById", 1))
            {
                var response = this.ExecuteDataService<ValidationPeriod>(new EntityDataServiceRequest<string, ValidationPeriod>(periodId));

                return response.Single();
            }
        }

        /// <summary>
        /// Gets the items using the specified item identifiers.
        /// </summary>
        /// <param name="itemIds">The collection of item identifiers.</param>
        /// <returns>The collection of items.</returns>
        public ReadOnlyCollection<Item> GetItems(IEnumerable<string> itemIds)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetItems", 1))
            {
                return this.ExecuteDataService<Item>(new EntityDataServiceRequest<IEnumerable<string>, Item>(itemIds));
            }
        }

        /// <summary>
        /// Gets the variant info for given item Id and variant inventory dimension identifier.
        /// </summary>
        /// <param name="itemId">The item identifier to find variant of.</param>
        /// <param name="inventDimId">The variant inventory dimension.</param>
        /// <param name="columnSet">The set of columns to retrieve from variant.</param>
        /// <returns>The variant with specified columns populated. Null if variant not found.</returns>
        public ProductVariant GetVariantByItemIdAndInventDimId(string itemId, string inventDimId, ColumnSet columnSet)
        {
            var itemAndInventoryDimensionIds = new ItemVariantInventoryDimension[] { new ItemVariantInventoryDimension(itemId, inventDimId) };
            GetProductVariantsDataRequest getVariantRequest = new GetProductVariantsDataRequest(itemAndInventoryDimensionIds);
            ReadOnlyCollection<ProductVariant> variants = this.ExecuteDataService<ProductVariant>(getVariantRequest);

            return variants.Single();
        }

        /// <summary>
        /// Fetch the superset of discount trade agreements which could apply to all of these items and customer for the given dates.
        /// </summary>
        /// <param name="itemIds">The item Ids to fetch for agreements for.</param>
        /// <param name="customerAccount">Optional. Customer account number to search by.</param>
        /// <param name="minActiveDate">The earliest inclusive active date to search by. Must be less than or equal to maxActiveDate.</param>
        /// <param name="maxActiveDate">The latest inclusive active date to search by. Must be greater than or equal to minActiveDate.</param>
        /// <param name="currencyCode">Currency code to filter by.</param>
        /// <returns>Collection of trade agreements which may be applied to the given items.</returns>
        public ReadOnlyCollection<TradeAgreement> ReadDiscountTradeAgreements(
            ISet<string> itemIds,
            string customerAccount,
            DateTimeOffset minActiveDate,
            DateTimeOffset maxActiveDate,
            string currencyCode)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("ReadDiscountTradeAgreements", 1))
            {
                return this.ExecuteDataService<TradeAgreement>(new ReadDiscountTradeAgreementsDataServiceRequest(
                                                                        itemIds,
                                                                        customerAccount,
                                                                        minActiveDate,
                                                                        maxActiveDate,
                                                                        currencyCode));
            }
        }

        /// <summary>
        /// Fetch the superset of trade agreements which could apply to all of these items and customer for the given date.
        /// </summary>
        /// <param name="itemIds">The item Ids to fetch for agreements for.</param>
        /// <param name="priceGroups">The price groups (probably channel) to query by.</param>
        /// <param name="customerAccount">Optional. Customer account number to search by.</param>
        /// <param name="minActiveDate">The earliest inclusive active date to search by. Must be less than or equal to maxActiveDate.</param>
        /// <param name="maxActiveDate">The latest inclusive active date to search by. Must be greater than or equal to minActiveDate.</param>
        /// <param name="currencyCode">Currency code to filter by.</param>
        /// <returns>Collection of trade agreements which may be applied to the given items.</returns>
        /// <remarks>This method may be called in a publishing context, when it may deal with thousands of items. Caching is suspended
        /// for these scenarios.</remarks>
        public ReadOnlyCollection<TradeAgreement> ReadPriceTradeAgreements(
            ISet<string> itemIds,
            ISet<string> priceGroups,
            string customerAccount,
            DateTimeOffset minActiveDate,
            DateTimeOffset maxActiveDate,
            string currencyCode)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("ReadPriceTradeAgreements", 1))
            {
                return this.ExecuteDataService<TradeAgreement>(new ReadPriceTradeAgreementsDataServiceRequest(
                                                                        itemIds,
                                                                        priceGroups,
                                                                        customerAccount,
                                                                        minActiveDate,
                                                                        maxActiveDate,
                                                                        currencyCode));
            }
        }

        /// <summary>
        /// Fetch all price adjustments for the given items, striped by item Id and dimension Id.
        /// </summary>
        /// <param name="items">The set of items to search by. Set of pairs of item Id and variant dimension Id. Ignores the unit.</param>
        /// <param name="priceGroups">Set of price groups to search by.</param>
        /// <param name="minActiveDate">The earliest inclusive active date to search by. Must be less than or equal to maxActiveDate.</param>
        /// <param name="maxActiveDate">The latest inclusive active date to search by. Must be greater than or equal to minActiveDate.</param>
        /// <param name="validationPeriods">Output all the validation periods used by the discovered price adjustments.</param>
        /// <returns>Collection of price adjustments striped by item Id and variant dimension Id (if any).</returns>
        /// <remarks>This method may be called in a publishing context, when it may deal with thousands of items. Caching is suspended
        /// for these scenarios.</remarks>
        public ReadOnlyCollection<PriceAdjustment> ReadPriceAdjustments(
            IEnumerable<ItemUnit> items, 
            ISet<string> priceGroups, 
            DateTimeOffset minActiveDate,
            DateTimeOffset maxActiveDate, 
            out ReadOnlyCollection<ValidationPeriod> validationPeriods)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("ReadPriceAdjustments", 1))
            {
                return this.ExecuteDataService<PriceAdjustment, ValidationPeriod>(
                    new ReadPriceAdjustmentsDataServiceRequest(
                            items,
                            priceGroups,
                            minActiveDate,
                            maxActiveDate),
                    out validationPeriods);
            }
        }

        /// <summary>
        /// Fetch all retail discounts for the given items, striped by item Id and dimension Id.
        /// </summary>
        /// <param name="items">The set of items to search by. Set of pairs of item Id and variant dimension Id. Ignores the unit.</param>
        /// <param name="priceGroups">Set of price groups to search by.</param>
        /// <param name="minActiveDate">The earliest inclusive active date to search by. Must be less than or equal to maxActiveDate.</param>
        /// <param name="maxActiveDate">The latest inclusive active date to search by. Must be greater than or equal to minActiveDate.</param>
        /// <param name="currencyCode">Currency code to filter by.</param>
        /// <param name="validationPeriods">Output all the validation periods used by the discovered price adjustments.</param>
        /// <returns>Collection of price adjustments striped by item Id and variant dimension Id (if any).</returns>
        public ReadOnlyCollection<PeriodicDiscount> ReadRetailDiscounts(
            IEnumerable<ItemUnit> items, 
            ISet<string> priceGroups, 
            DateTimeOffset minActiveDate,
            DateTimeOffset maxActiveDate, 
            string currencyCode, 
            out ReadOnlyCollection<ValidationPeriod> validationPeriods)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("ReadRetailDiscounts", 1))
            {
                return this.ExecuteDataService<PeriodicDiscount, ValidationPeriod>(
                    new ReadRetailDiscountsDataServiceRequest(
                            items,
                            priceGroups,
                            minActiveDate,
                            maxActiveDate,
                            currencyCode),
                    out validationPeriods);
            }
        }

        /// <summary>
        /// Get the variant dimensions populated for the given dimension Ids. This is lightweight and 
        ///  only returns the dimension Ids, not translations.
        /// </summary>
        /// <param name="inventoryDimensionIds">The dimension Ids which need dimension values fetched.</param>
        /// <returns>Collection of dimension values.</returns>
        public ReadOnlyCollection<ProductVariant> GetVariantDimensionsByItemIds(IEnumerable<string> inventoryDimensionIds)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetVariantDimensionsByItemIds", 1))
            {
                return this.ExecuteDataService<ProductVariant>(new EntityDataServiceRequest<IEnumerable<string>, ProductVariant>(inventoryDimensionIds));
            }
        }

        /// <summary>
        /// Get all the threshold tiers associated with the given offers.
        /// </summary>
        /// <param name="offerIds">Offer Ids to fetch tiers by.</param>
        /// <returns>Collection of tiers (if any) associated with the given offer Ids.</returns>
        public ReadOnlyCollection<ThresholdDiscountTier> GetThresholdTiersByOfferIds(IEnumerable<string> offerIds)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetThresholdTiersByOfferIds", 1))
            {
                return this.ExecuteDataService<ThresholdDiscountTier>(new EntityDataServiceRequest<IEnumerable<string>, ThresholdDiscountTier>(offerIds));
            }
        }

        /// <summary>
        /// Get all the multi buy discount lines associated with the given offers.
        /// </summary>
        /// <param name="offerIds">Offer Ids to fetch discount lines by.</param>
        /// <returns>Collection of multi buy discount lines associated with the given offer Ids.</returns>
        public ReadOnlyCollection<QuantityDiscountLevel> GetMultipleBuyDiscountLinesByOfferIds(IEnumerable<string> offerIds)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetMultipleBuyDiscountLinesByOfferIds", 1))
            {
                return this.ExecuteDataService<QuantityDiscountLevel>(new EntityDataServiceRequest<IEnumerable<string>, QuantityDiscountLevel>(offerIds));
            }
        }

        /// <summary>
        /// Get all the mix and match line groups associated with the given offers.
        /// </summary>
        /// <param name="offerIds">Offer Ids to fetch mix and match line groups by.</param>
        /// <returns>Collection of mix and match line groups associated with the given offer Ids.</returns>
        public ReadOnlyCollection<MixAndMatchLineGroup> GetMixAndMatchLineGroupsByOfferIds(IEnumerable<string> offerIds)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetMixAndMatchLineGroupsByOfferIds", 1))
            {
                return this.ExecuteDataService<MixAndMatchLineGroup>(new EntityDataServiceRequest<IEnumerable<string>, MixAndMatchLineGroup>(offerIds));
            }
        }

        /// <summary>
        /// Gets the catalog price groups.
        /// </summary>
        /// <param name="catalogIds">Catalog recId's.</param>
        /// <returns>
        /// A collection of catalog price groups.
        /// </returns>
        public ReadOnlyCollection<CatalogPriceGroup> GetCatalogPriceGroups(ISet<long> catalogIds)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetCatalogPriceGroups", 1))
            {
                return this.ExecuteDataService<CatalogPriceGroup>(new EntityDataServiceRequest<IEnumerable<long>, CatalogPriceGroup>(catalogIds));
            }
        }

        /// <summary>
        /// Gets the affiliation price groups.
        /// </summary>
        /// <param name="affiliationLoyaltyTiers">A collection of affiliation Id or loyalty tier Id.</param>
        /// <returns>
        /// A collection of affiliation price groups.
        /// </returns>
        public ReadOnlyCollection<PriceGroup> GetAffiliationPriceGroups(IEnumerable<AffiliationLoyaltyTier> affiliationLoyaltyTiers)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetAffiliationPriceGroups", 1))
            {
                return this.ExecuteDataService<PriceGroup>(new EntityDataServiceRequest<IEnumerable<AffiliationLoyaltyTier>, PriceGroup>(affiliationLoyaltyTiers));
            }
        }

        /// <summary>
        /// Gets retail discount price groups.
        /// </summary>
        /// <param name="offerIds">Offer identifiers.</param>
        /// <returns>
        /// A collection of retail discount price groups.
        /// </returns>
        public ReadOnlyCollection<RetailDiscountPriceGroup> GetRetailDiscountPriceGroups(ISet<string> offerIds)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetRetailDiscountPriceGroups", 1))
            {
                return this.ExecuteDataService<RetailDiscountPriceGroup>(new EntityDataServiceRequest<IEnumerable<string>, RetailDiscountPriceGroup>(offerIds));
            }
        }

        /// <summary>
        /// Get all of the discounts configured in the system.
        /// </summary>
        /// <returns>The collection of discounts.</returns>
        public ReadOnlyCollection<RetailDiscount> GetAllRetailDiscounts()
        {
            return this.ExecuteDataService<RetailDiscount>(new EntityDataServiceRequest<RetailDiscount>());
        }

        /// <summary>
        /// Get the category membership information for the product or variant identifiers passed in.
        /// </summary>
        /// <param name="productOrVariantIds">A set of product or variant identifiers.</param>
        /// <returns>The collection of mappings between the product or variant identifier and the category identifier.</returns>
        public ReadOnlyCollection<RetailCategoryMember> GetRetailCategoryMembersForItems(ISet<long> productOrVariantIds)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetRetailCategoryMembersForItems", 1))
            {
                return this.ExecuteDataService<RetailCategoryMember>(new EntityDataServiceRequest<IEnumerable<long>, RetailCategoryMember>(productOrVariantIds));
            }
        }

        /// <summary>
        /// Gets the collection of product variants.
        /// </summary>
        /// <param name="itemVariants">The collection of item variant inventory dimension.</param>
        /// <param name="columnSet">The set of columns to retrieve.</param>
        /// <returns>The collection of product variants.</returns>
        public ReadOnlyCollection<ProductVariant> GetVariants(IEnumerable<ItemVariantInventoryDimension> itemVariants, ColumnSet columnSet)
        {
            using (SimpleProfiler profiler = new SimpleProfiler("GetVariants", 1))
            {
                GetProductVariantsDataRequest getVariantsRequest = new GetProductVariantsDataRequest(itemVariants);
                getVariantsRequest.QueryResultSettings = new QueryResultSettings(columnSet);
                return this.ExecuteDataService<ProductVariant>(getVariantsRequest);
            }
        }

        private ReadOnlyCollection<T> ExecuteDataService<T>(Request request) where T : CommerceEntity
        {
            var response = this.RequestContext.Runtime.Execute<EntityDataServiceResponse<T>>(request, this.RequestContext);

            return response.EntityCollection;
        }

        private ReadOnlyCollection<TResponse> ExecuteDataService<TResponse, TOutParam>(Request request, out ReadOnlyCollection<TOutParam> outputParams)
            where TResponse : CommerceEntity
            where TOutParam : CommerceEntity
        {
            var response = this.RequestContext.Runtime.Execute<EntityDataServiceResponse<TResponse, TOutParam>>(request, this.RequestContext);

            outputParams = response.OutputParams.AsReadOnly();

            return response.EntityCollection;
        }
    }
}
