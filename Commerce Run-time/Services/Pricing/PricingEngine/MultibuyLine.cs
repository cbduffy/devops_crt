/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;

    /// <summary>
    /// Multi buy line associates a discount value with a quantity of items.
    /// It is used by quantity discounts, and by using many together, the various discounts
    /// thresholds are defined for increasing discount item quantities.
    /// This class can also initialize itself to the applicable quantity and discount value
    /// for a given multi buy offer and item quantity.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Multibuy", Justification = "Multibuy is a type of retail discount, not abbreviated or misspelled")]
    internal class MultibuyLine
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MultibuyLine"/> class.
        /// </summary>
        /// <param name="qualifyingQuantity">Number of items to activate this discount.</param>
        /// <param name="discountValue">Value of the discount (unit price of percent, based on offer header method).</param>
        public MultibuyLine(decimal qualifyingQuantity, decimal discountValue)
        {
            this.MinQuantity = qualifyingQuantity;
            this.UnitPriceOrDiscountPercentage = discountValue;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="MultibuyLine"/> class from being created.
        /// </summary>
        private MultibuyLine()
        {
        }

        /// <summary>
        /// Gets the minimum quantity of items to activate this multi buy line offer.
        /// </summary>
        public decimal MinQuantity { get; private set; }

        /// <summary>
        /// Gets the discount value of the line.
        /// </summary>
        /// <remarks>
        /// This could be unit price or a percentage as determined by the offer this line belongs to.
        /// </remarks>
        public decimal UnitPriceOrDiscountPercentage { get; private set; }

        /// <summary>
        /// Construct instance of <see cref="MultibuyLine"/> for the given Offer and item quantity. This will find the first
        /// multi buy line for the offer whose quantity meets but does not exceed the given quantity and return it.
        /// </summary>
        /// <param name="pricingDataManager">Provides data access to the calculation.</param>
        /// <param name="offerId">Identifier of the offer being attempted.</param>
        /// <param name="quantity">Number of items for the offer.</param>
        /// <returns>Multi buy rule for given offer and quantity threshold.</returns>
        public static MultibuyLine FetchForOfferAndQuantity(IPricingDataManager pricingDataManager, string offerId, decimal quantity)
        {
            var discountLevel = pricingDataManager.GetQuantityDiscountLevelByQuantity(offerId, quantity);

            if (discountLevel != null)
            {
                return new MultibuyLine(discountLevel.MinimumQuantity, discountLevel.DiscountPriceOrPercent);
            }

            return MultibuyLine.Empty();
        }

        /// <summary>
        /// Creates an empty <see cref="MultibuyLine"/> object..
        /// </summary>
        /// <returns>
        /// An instance representing an empty multi buy line offer.
        /// </returns>
        public static MultibuyLine Empty()
        {
            return new MultibuyLine(0, 0);
        }
    }
}