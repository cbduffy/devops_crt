﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Represents the overall settings and configuration to use when calculating prices for set of lines.
    /// </summary>
    public sealed class PriceContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PriceContext"/> class.
        /// </summary>
        public PriceContext()
        {
            this.ChannelPriceGroups = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            this.CatalogPriceGroups = new Dictionary<long, ISet<string>>();
            this.AffiliationPriceGroups = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            this.PriceGroupIdsToRecordIdsDictionary = new Dictionary<string, long>();
            this.RecordIdsToPriceGroupIdsDictionary = new Dictionary<long, string>();
            this.ItemCache = new Dictionary<string, Item>(StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the collection of channel price groups to search by.
        /// </summary>
        public ISet<string> ChannelPriceGroups { get; private set; }

        /// <summary>
        /// Gets the collection of catalog price groups to search by.
        /// </summary>
        /// <remarks>
        /// For catalog specific discounts, we need to ensure discounted items are in the catalogs.
        /// Hence, given discount price groups, we need to figure out whether an item is qualified if the discount is catalog only.
        /// More details in PriceContextHelper in price engine.
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "this is the type of the class which this type is modeling.")]
        public IDictionary<long, ISet<string>> CatalogPriceGroups { get; private set; }

        /// <summary>
        /// Gets the collection of catalog price groups to search by.
        /// </summary>
        public ISet<string> AffiliationPriceGroups { get; private set; }

        /// <summary>
        /// Gets the translation between price group identifiers and record ids.
        /// </summary>
        public IDictionary<string, long> PriceGroupIdsToRecordIdsDictionary { get; private set; }

        /// <summary>
        /// Gets the translation between record ids and price group identifiers.
        /// </summary>
        public IDictionary<long, string> RecordIdsToPriceGroupIdsDictionary { get; private set; }

        /// <summary>
        /// Gets or sets the customer account number for customer-specific prices. Optional.
        /// </summary>
        public string CustomerAccount { get; set; }

        /// <summary>
        /// Gets or sets the customer price group Id for customer-specific prices. Optional.
        /// </summary>
        public string CustomerPriceGroup { get; set; }

        /// <summary>
        /// Gets or sets the customer line discount price group Id for customer-specific prices. Optional.
        /// </summary>
        public string CustomerLinePriceGroup { get; set; }

        /// <summary>
        /// Gets or sets the customer multiple line discount price group Id for customer-specific prices. Optional.
        /// </summary>
        public string CustomerMultipleLinePriceGroup { get; set; }

        /// <summary>
        /// Gets or sets the customer multiple line discount price group Id for customer-specific prices. Optional.
        /// </summary>
        public string CustomerTotalPriceGroup { get; set; }

        /// <summary>
        /// Gets or sets the date and time on which to calculate the prices.
        /// </summary>
        public DateTimeOffset ActiveDate { get; set; }

        /// <summary>
        /// Gets or sets the currency code to search by when pricing. Usually the channel's currency.
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Gets or sets the configuration of which trade agreement combinations are allowed.
        /// </summary>
        public PriceParameters PriceParameters { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether prices are fetched in tax-inclusive (e.g. VAT) channel or tax-exclusive (e.g. US taxes).
        /// </summary>
        public bool IsTaxInclusive { get; set; }

        /// <summary>
        /// Gets or sets the price calculation mode for the lines being calculated.
        /// </summary>
        public PricingCalculationMode PriceCalculationMode { get; set; }

        /// <summary>
        /// Gets or sets the discount calculation mode for the lines being calculated.
        /// </summary>
        public DiscountCalculationMode DiscountCalculationMode { get; set; }

        /// <summary>
        /// Gets or sets the rounding delegate.
        /// </summary>
        public RoundingRule Rounding { get; set; }

        /// <summary>
        /// Gets or sets the currency converter delegate.
        /// </summary>
        public CurrencyConverter CurrencyConverter { get; set; }

        /// <summary>
        /// Gets the item identifier to item object cache.
        /// </summary>
        internal Dictionary<string, Item> ItemCache { get; private set; }
    }
}
