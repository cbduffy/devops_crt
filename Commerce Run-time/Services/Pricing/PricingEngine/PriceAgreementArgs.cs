/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Enumeration to capture whether a customer or price-group price has specified an override value for Price-includes-tax.
    /// </summary>
    internal enum PriceGroupIncludesTax
    {
        /// <summary>
        /// 0 if we don't know whether price group includes tax.
        /// </summary>
        NotSpecified = 0,

        /// <summary>
        /// 1 if the price group excludes tax from price.
        /// </summary>
        PriceExcludesTax = 1,

        /// <summary>
        /// 2 if the price group includes tax in price.
        /// </summary>
        PriceIncludesTax = 2
    }

    /// <summary>
    /// Result from a price lookup, contains both the Price value and whether or not that price definition has specified an value for 'Price includes tax'.
    /// </summary>
    internal struct PriceResult
    {
        /// <summary>
        /// Price value.
        /// </summary>
        public readonly decimal Price;
        
        /// <summary>
        /// Customer Price Group.
        /// </summary>
        public readonly string CustPriceGroup;

        /// <summary>
        /// Whether or not the price includes taxes.
        /// </summary>
        public readonly PriceGroupIncludesTax IncludesTax;

        /// <summary>
        /// Maximum retail price.
        /// </summary>
        public decimal MaximumRetailPriceIndia;

        /// <summary>
        /// Initializes a new instance of the <see cref="PriceResult"/> struct.
        /// </summary>
        /// <param name="price">Price of the result.</param>
        /// <param name="includesTax">Does the result include tax.</param>
        /// <param name="maximumRetailPriceIndia">Maximum retail price.</param>
        /// <param name="custPriceGroup">Customer price group.</param>
        public PriceResult(decimal price, PriceGroupIncludesTax includesTax, decimal maximumRetailPriceIndia = decimal.Zero, string custPriceGroup = null)
        {
            this.Price = price;
            this.IncludesTax = includesTax;
            this.MaximumRetailPriceIndia = maximumRetailPriceIndia;
            this.CustPriceGroup = custPriceGroup;
        }
    }

    /// <summary>
    /// Arguments for a Retail price lookup operation.
    /// </summary>
    internal struct RetailPriceArgs
    {
        /// <summary>
        /// The optional customer account number to consider.
        /// </summary>
        public string CustomerId;

        /// <summary>
        /// The price group Ids to search by.
        /// </summary>
        public ReadOnlyCollection<string> PriceGroups;

        /// <summary>
        /// The currency code to filter by.
        /// </summary>
        public string CurrencyCode;

        /// <summary>
        /// The quantity of the item or total cost to consider.
        /// </summary>
        public decimal Quantity;

        /// <summary>
        /// The item Id to find a price for.
        /// </summary>
        public string ItemId;

        /// <summary>
        /// The barcode for this line. Deprecated.
        /// </summary>
        public string Barcode;

        /// <summary>
        /// The default sales unit of measure.
        /// </summary>
        public string DefaultSalesUnitOfMeasure;

        /// <summary>
        /// The sales unit of measure of the item. Used to search by unit.
        /// </summary>
        public string SalesUOM;

        /// <summary>
        /// The class which manages unit conversion for this item.
        /// </summary>
        public UnitOfMeasureConversion UnitOfMeasureConversion;

        /// <summary>
        /// Optional parameter which specifies the product variant dimensions to consider for price search.
        /// </summary>
        public ProductVariant Dimensions;

        /// <summary>
        /// Convert to <see cref="PriceAgreementArgs"/> using the Sale UOM.
        /// </summary>
        /// <returns>Sales price agreement arguments.</returns>
        public PriceAgreementArgs ArgreementArgsForSale()
        {
            return new PriceAgreementArgs()
            {
                CurrencyCode = this.CurrencyCode,
                CustomerId = this.CustomerId,
                Dimensions = this.Dimensions,
                ItemId = this.ItemId,
                PriceGroups = this.PriceGroups,
                Quantity = this.Quantity,
                UnitOfMeasure = this.SalesUOM
            };
        }

        /// <summary>
        /// Convert to <see cref="PriceAgreementArgs"/> using the Inventory UOM.
        /// </summary>
        /// <returns>The inventory price agreement arguments.</returns>
        public PriceAgreementArgs AgreementArgsForDefaultSales()
        {
            return new PriceAgreementArgs()
            {
                CurrencyCode = this.CurrencyCode,
                CustomerId = this.CustomerId,
                Dimensions = this.Dimensions,
                ItemId = this.ItemId,
                PriceGroups = this.PriceGroups,
                Quantity = this.Quantity,
                UnitOfMeasure = this.DefaultSalesUnitOfMeasure
            };
        }
    }

    /// <summary>
    /// Arguments for a Price Agreement lookup operation and methods for reading.
    /// </summary>
    internal struct PriceAgreementArgs
    {
        /// <summary>
        /// The optional customer account number to consider.
        /// </summary>
        public string CustomerId;

        /// <summary>
        /// The price group Ids to search by.
        /// </summary>
        public ReadOnlyCollection<string> PriceGroups;

        /// <summary>
        /// The currency code to filter by.
        /// </summary>
        public string CurrencyCode;

        /// <summary>
        /// The quantity of the item or total cost to consider.
        /// </summary>
        public decimal Quantity;

        /// <summary>
        /// The item Id to find a price for.
        /// </summary>
        public string ItemId;

        /// <summary>
        /// The sales unit of measure of the item. Used to search by unit.
        /// </summary>
        public string UnitOfMeasure;

        /// <summary>
        /// Optional parameter which specifies the product variant dimensions to consider for price search.
        /// </summary>
        public ProductVariant Dimensions;

        /// <summary>
        /// Gets a value indicating whether the item is a variant.
        /// </summary>
        public bool IsVariant
        {
            get 
            {
                return !(string.IsNullOrWhiteSpace(this.Dimensions.ConfigId)
                      && string.IsNullOrWhiteSpace(this.Dimensions.ColorId)
                      && string.IsNullOrWhiteSpace(this.Dimensions.SizeId)
                      && string.IsNullOrWhiteSpace(this.Dimensions.StyleId));
            }
        }

        /// <summary>
        /// Gets a value indicating whether it's a match of variant.
        /// </summary>
        /// <param name="variant">The product variant.</param>
        /// <returns>A value indicating whether it's a match of variant.</returns>
        public bool IsVariantMatch(ProductVariant variant)
        {
            if (variant != null)
            {
                return (string.IsNullOrWhiteSpace(this.Dimensions.Configuration) || this.Dimensions.ConfigId.Equals(variant.ConfigId, StringComparison.OrdinalIgnoreCase))
                        && (string.IsNullOrWhiteSpace(this.Dimensions.ColorId) || this.Dimensions.ColorId.Equals(variant.ColorId, StringComparison.OrdinalIgnoreCase))
                        && (string.IsNullOrWhiteSpace(this.Dimensions.SizeId) || this.Dimensions.SizeId.Equals(variant.SizeId, StringComparison.OrdinalIgnoreCase))
                        && (string.IsNullOrWhiteSpace(this.Dimensions.StyleId) || this.Dimensions.StyleId.Equals(variant.StyleId, StringComparison.OrdinalIgnoreCase));
            }
            else
            {
                return !this.IsVariant;
            }
        }

        /// <summary>
        /// Gets price agreement 'item relation' based on arguments and given item relation code.
        /// </summary>
        /// <param name="itemCode">Item relation code (item/group/all).</param>
        /// <returns>
        /// Returns item if 'item' relation code given, otherwise empty string.
        /// </returns>
        public string GetItemRelation(PriceDiscountItemCode itemCode)
        {
            string itemRelation = string.Empty;
            if (itemCode == PriceDiscountItemCode.Item && !string.IsNullOrEmpty(this.ItemId))
            {
                itemRelation = this.ItemId;
            }

            return itemRelation;
        }

        /// <summary>
        /// Gets price agreement 'account relations' based on arguments and given account relation code.
        /// </summary>
        /// <param name="accountCode">Account relation code (customer/price group/all).</param>
        /// <returns>
        /// Returns customer if 'customer' code given, price groups if 'group' code given, otherwise empty.
        /// </returns>
        public ReadOnlyCollection<string> GetAccountRelations(PriceDiscountAccountCode accountCode)
        {
            ReadOnlyCollection<string> accountRelations = new ReadOnlyCollection<string>(new List<string> { string.Empty });
            if (accountCode == PriceDiscountAccountCode.Customer && !string.IsNullOrEmpty(this.CustomerId))
            {
                accountRelations = new ReadOnlyCollection<string>(new List<string> { this.CustomerId });
            }
            else if (accountCode == PriceDiscountAccountCode.CustomerGroup && this.PriceGroups.Count > 0)
            {
                accountRelations = this.PriceGroups;
            }

            return accountRelations;
        }

        /// <summary>
        /// Gets price agreement unit of measure based on arguments and given item relation code.
        /// </summary>
        /// <param name="itemCode">Item relation code (item/group/all).</param>
        /// <returns>
        /// Return unit of measure id if 'item' code specified, otherwise empty.
        /// </returns>
        public string GetUnitId(PriceDiscountItemCode itemCode)
        {
            return itemCode == PriceDiscountItemCode.Item && !string.IsNullOrEmpty(this.UnitOfMeasure) ? this.UnitOfMeasure : string.Empty;
        }
    }
}