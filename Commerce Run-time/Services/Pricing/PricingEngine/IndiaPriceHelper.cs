﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine
{
    using System;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;

    /// <summary>
    /// Helper of India retail price.
    /// </summary>
    public sealed class IndiaPriceHelper
    {
        private decimal maxRetailPrice;
        private IPricingDataManagerV2 pricingDataManager;
        private ChannelDataManager channelDataManager;
        private ChannelConfiguration channelConfiguration;
        private string customerId;
        private Customer customer;
        private string priceGroup;
        private SalesTransaction salesTransaction;

        /// <summary>
        /// Get an instance of the <see cref="IndiaPriceHelper"/> class.
        /// This method will throw ArgumentNullException when context is null or context.GetSalesTransaction() is null.
        /// Besides, this method will throw ArgumentException when no customer can be find by CustomerId.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="pricingDataManager">Pricing data manager.</param>
        /// <returns>The instance of IndiaPriceHelper.</returns>
        public static IndiaPriceHelper GetIndiaPriceHelper(RequestContext context, IPricingDataManagerV2 pricingDataManager) 
        {
            if (context == null || context.GetSalesTransaction() == null)
            {
                throw new ArgumentNullException("context");
            }

            IndiaPriceHelper indiaPriceHelper = new IndiaPriceHelper();
            indiaPriceHelper.Initiate(context, pricingDataManager);
            return indiaPriceHelper;
        }

        /// <summary>
        /// Gets maximum retail price from trade agreement.
        /// </summary>
        /// <param name="salesLine">The sales line.</param>
        /// <returns>Maximum retail price from trade agreement.</returns>
        public decimal GetMaximumRetailPriceFromTradeAgreement(SalesLine salesLine)
        {
            if (salesLine == null)
            {
                throw new ArgumentNullException("salesLine");
            }

            decimal quantity = this.salesTransaction.ActiveSalesLines.Where(x => (x.ItemId == salesLine.ItemId) && (x.InventoryDimensionId == salesLine.InventoryDimensionId)).Sum(x => x.Quantity);
            if (quantity == decimal.Zero)
            {
                quantity = 1;
            }

            if (!salesLine.BeginDateTime.IsValidSqlDateTime())
            {
                salesLine.BeginDateTime = this.channelDataManager.Context.GetNowInChannelTimeZone();
            }

            PriceResult priceResult = PricingEngine.GetActiveTradeAgreement(
                this.pricingDataManager,
                DiscountParameters.CreateAndInitialize(this.pricingDataManager),
                this.channelConfiguration.Currency,
                salesLine,
                quantity,
                this.customerId,
                this.priceGroup,
                salesLine.BeginDateTime);

            this.maxRetailPrice = priceResult.MaximumRetailPriceIndia;

            return this.maxRetailPrice;
        }

        /// <summary>
        /// Initiates the instance of IndiaPriceHelper.
        /// This method will throw ArgumentException when no customer can be find by CustomerId.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <param name="pricingDataManager">Pricing data manager.</param>
        private void Initiate(RequestContext context, IPricingDataManagerV2 pricingDataManager)
        {
            this.pricingDataManager = pricingDataManager;
            this.channelDataManager = new ChannelDataManager(context);
            this.channelConfiguration = this.channelDataManager.GetChannelConfiguration();
            this.customerId = context.GetSalesTransaction().CustomerId;

            if (!string.IsNullOrWhiteSpace(this.customerId))
            {
                var getCustomerDataRequest = new GetCustomerDataRequest(this.customerId);
                var getCustomerDataResponse = context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerDataRequest, context);
                this.customer = getCustomerDataResponse.Entity;
            }

            this.priceGroup = (this.customer != null) ? this.customer.PriceGroup : string.Empty;
            this.salesTransaction = context.GetSalesTransaction();
        }
    }
}
