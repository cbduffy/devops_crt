﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// This class handles handles calculating the actual prices for each 
    ///  item line based on the price lines for each item line line Id.
    /// </summary>
    internal static class PriceLineResolver
    {
        /// <summary>
        /// For the given item lines calculate and set their prices based on the set
        ///  of price lines provided (keyed by item line line Id).
        /// </summary>
        /// <param name="salesLines">Item lines to have their prices set.</param>
        /// <param name="priceLines">Set of price lines used to set the item line prices.</param>
        /// <param name="roundingRule">Delegate to perform price rounding for amounts in current currency.</param>
        public static void ResolveAndApplyPriceLines(IEnumerable<SalesLine> salesLines, Dictionary<string, IEnumerable<PriceLine>> priceLines, RoundingRule roundingRule)
        {
            foreach (var sl in salesLines)
            {
                IEnumerable<PriceLine> prices;
                if (!priceLines.TryGetValue(sl.LineId, out prices))
                {
                    prices = new PriceLine[0];
                }

                ResolveAndApplyPriceForSalesLine(sl, prices);
            }

            RoundPrices(salesLines, roundingRule);
        }

        private static void ResolveAndApplyPriceForSalesLine(SalesLine item, IEnumerable<PriceLine> itemPriceLines)
        {
            var agreementLine = itemPriceLines.OfType<TradeAgreementPriceLine>().FirstOrDefault();
            var baseLine = itemPriceLines.OfType<BasePriceLine>().FirstOrDefault();

            bool hasTradeAgreementPrice = agreementLine != null;
            bool hasBasePrice = baseLine != null;

            item.AgreementPrice = hasTradeAgreementPrice ? agreementLine.Value : 0m;
            item.BasePrice = hasBasePrice ? baseLine.Value : 0m;

            // use the trade agreement price if any, otherwise use the base price
            if (hasTradeAgreementPrice)
            {
                SetPriceOnSalesLine(item, item.AgreementPrice);
                item.TradeAgreementPriceGroup = agreementLine.CustPriceGroup;
            }
            else if (hasBasePrice)
            {
                SetPriceOnSalesLine(item, item.BasePrice);
                item.AgreementPrice = item.BasePrice;
            }
            else
            {
                SetPriceOnSalesLine(item, 0);
            }

            // now try to apply any price adjustments
            var adjustmentLines = itemPriceLines.OfType<PriceAdjustmentPriceLine>();
            item.AdjustedPrice = PriceAdjustmentCalculator.CalculatePromotionPrice(adjustmentLines, item.Price);

            if (Math.Abs(item.AdjustedPrice) < Math.Abs(item.Price))
            {
                SetPriceOnSalesLine(item, item.AdjustedPrice);
                item.TradeAgreementPriceGroup = null;
            }
        }

        private static void RoundPrices(IEnumerable<SalesLine> lines, RoundingRule roundingRule)
        {
            Dictionary<decimal, decimal> roundingCache = new Dictionary<decimal, decimal>();
            foreach (var line in lines)
            {
                line.Price = RoundPricesCached(roundingCache, roundingRule, line.Price);
                if (line.OriginalPrice.HasValue)
                {
                    line.OriginalPrice = RoundPricesCached(roundingCache, roundingRule, line.OriginalPrice.Value);
                }

                line.BasePrice = RoundPricesCached(roundingCache, roundingRule, line.BasePrice);
                line.AgreementPrice = RoundPricesCached(roundingCache, roundingRule, line.AgreementPrice);
                line.AdjustedPrice = RoundPricesCached(roundingCache, roundingRule, line.AdjustedPrice);
            }
        }

        private static decimal RoundPricesCached(Dictionary<decimal, decimal> roundingCache, RoundingRule roundingRule, decimal valueToRound)
        {
            decimal roundedValue;
            if (!roundingCache.TryGetValue(valueToRound, out roundedValue))
            {
                roundedValue = roundingRule(valueToRound);
                roundingCache.Add(valueToRound, roundedValue);
            }

            return roundedValue;
        }

        /// <summary>
        /// Set the price or original price depending on if price is overridden or keyed-in.
        /// </summary>
        /// <param name="line">Sales line to set price on.</param>
        /// <param name="priceToSet">Price to set.</param>
        private static void SetPriceOnSalesLine(SalesLine line, decimal priceToSet)
        {
            if (line.IsPriceOverridden)
            {
                line.OriginalPrice = priceToSet;
            }
            else
            {
                line.Price = priceToSet;
            }
        }
    }
}
