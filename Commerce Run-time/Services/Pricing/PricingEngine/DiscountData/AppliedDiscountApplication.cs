﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine.DiscountData
{
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Applied discount application.
    /// </summary>
    public class AppliedDiscountApplication
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedDiscountApplication" /> class.
        /// </summary>
        /// <param name="discountApplication">Discount application.</param>
        /// <param name="value">The value of the discount application.</param>
        /// <param name="appliedQuantities">The applied quantities.</param>
        /// <param name="isDiscountLineGenerated">A value indicating whether the discount lines have been generated.</param>
        internal AppliedDiscountApplication(DiscountApplication discountApplication, decimal value, decimal[] appliedQuantities, bool isDiscountLineGenerated)
            : this(discountApplication, value, appliedQuantities, decimal.Zero, null, null, isDiscountLineGenerated)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppliedDiscountApplication" /> class.
        /// </summary>
        /// <param name="discountApplication">Discount application.</param>
        /// <param name="value">The value of the discount application.</param>
        /// <param name="appliedQuantities">The applied quantities.</param>
        /// <param name="totalAmountForCoveredLines">Total amount for the covered lines.</param>
        /// <param name="discountedItems">Discount items.</param>
        /// <param name="itemPrices">Item prices.</param>
        /// <param name="isDiscountLineGenerated">A value indicating whether the discount lines have been generated.</param>
        internal AppliedDiscountApplication(DiscountApplication discountApplication, decimal value, decimal[] appliedQuantities, decimal totalAmountForCoveredLines, ISet<int> discountedItems, decimal[] itemPrices, bool isDiscountLineGenerated)
        {
            this.DiscountApplication = discountApplication;

            this.Value = value;
            this.TotalAmountForCoveredLines = totalAmountForCoveredLines;
            this.DiscountedItems = discountedItems;
            this.ItemPrices = itemPrices;

            this.DiscountLinesDictionary = new Dictionary<int, IList<DiscountLineQuantity>>();
            this.ItemQuantities = (decimal[])appliedQuantities.Clone();

            this.IsDiscountLineGenerated = isDiscountLineGenerated;
        }

        internal DiscountApplication DiscountApplication { get; private set; }
        
        internal decimal[] ItemQuantities { get; set; }

        internal ISet<int> DiscountedItems { get; private set; }

        internal decimal Value { get; private set; }

        internal decimal TotalAmountForCoveredLines { get; private set; }

        internal decimal[] ItemPrices { get; private set; }

        internal IDictionary<int, IList<DiscountLineQuantity>> DiscountLinesDictionary { get; set; }

        internal bool IsDiscountLineGenerated { get; private set; }

        internal void AddDiscountLine(int itemIndex, DiscountLineQuantity discountLine)
        {
            if (this.DiscountLinesDictionary.ContainsKey(itemIndex))
            {
                this.DiscountLinesDictionary[itemIndex].Add(discountLine);
            }
            else
            {
                this.DiscountLinesDictionary.Add(itemIndex, new List<DiscountLineQuantity>() { discountLine });
            }
        }

        internal void Apply(DiscountableItemGroup[] discountableItemGroups, PriceContext priceContext)
        {
            if (!this.IsDiscountLineGenerated)
            {
                this.DiscountApplication.Discount.GenerateDiscountLines(this, discountableItemGroups, priceContext);
            }

            if (discountableItemGroups == null || discountableItemGroups.Length == 0)
            {
                return;
            }

            foreach (var item in this.DiscountLinesDictionary)
            {
                foreach (DiscountLineQuantity line in item.Value)
                {
                    discountableItemGroups[item.Key].AddDiscountLine(line);
                }
            }
        }
    }
}
