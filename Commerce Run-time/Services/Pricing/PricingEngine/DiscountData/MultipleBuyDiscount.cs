﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine.DiscountData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// This class implements the multi-buy (quantity threshold) discount processing, including the determination of which ways 
    /// the discount can apply to the transaction and the value of the discount applied to specific lines.
    /// </summary>
    public class MultipleBuyDiscount : DiscountBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MultipleBuyDiscount" /> class.
        /// </summary>
        public MultipleBuyDiscount()
            : base()
        {
            this.QuantityDiscountLevels = new List<QuantityDiscountLevel>();
        }

        private enum LevelTrend
        {
            Undecided,
            DealBetterAsLevelIncreases,
            DealWorseAsLevelIncreases,
        }

        /// <summary>
        /// Gets the collection of MultiBuy discount tiers.
        /// </summary>
        public IList<QuantityDiscountLevel> QuantityDiscountLevels { get; private set; }

        /// <summary>
        /// Gets all of the possible applications of this discount to the specified transaction and line items.
        /// </summary>
        /// <param name="transaction">The transaction to consider for discounts.</param>
        /// <param name="discountableItemGroups">The valid sales line items on the transaction to consider.</param>
        /// <param name="remainingQuantities">The remaining quantities of each of the sales lines to consider.</param>
        /// <param name="storePriceGroups">The collection of price groups that this store belongs to.</param>
        /// <param name="currencyCode">The currency code in use for the current transaction.</param>
        /// <param name="pricingDataManager">The IPricingDataManager instance to use.</param>
        /// <param name="isReturn">The flag indicating whether or not it's for return.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <returns>The possible permutations of line items that this discount can apply to, or an empty collection if this discount cannot apply.</returns>
        public override IEnumerable<DiscountApplication> GetDiscountApplications(
            SalesTransaction transaction,
            DiscountableItemGroup[] discountableItemGroups,
            decimal[] remainingQuantities,
            IEnumerable<long> storePriceGroups,
            string currencyCode,
            IPricingDataManagerV2 pricingDataManager,
            bool isReturn,
            PriceContext priceContext)
        {
            if (priceContext == null)
            {
                throw new ArgumentNullException("priceContext");
            }

            // Figure whether deal is better or worse as quantity level increases. We will use it later to limit number of times applied for some discount applications.
            LevelTrend levelTrend = LevelTrend.Undecided;
            decimal previousLevelPriceOrPercentage = decimal.Zero;
            decimal highestQuantityLevel = decimal.Zero;

            SortedDictionary<decimal, QuantityDiscountLevel> sortedLevels = new SortedDictionary<decimal, QuantityDiscountLevel>(this.QuantityDiscountLevels.ToDictionary(p => p.MinimumQuantity, p => p));
            MultipleBuyDiscountMethod discountMethod = (MultipleBuyDiscountMethod)this.DiscountType;

            bool isFirst = true;
            foreach (KeyValuePair<decimal, QuantityDiscountLevel> pair in sortedLevels)
            {
                highestQuantityLevel = pair.Key;

                if (isFirst)
                {
                    isFirst = false;
                }
                else
                {
                    LevelTrend newLevelTrend = ComputeLevelTrend(discountMethod, previousLevelPriceOrPercentage, pair.Value.DiscountPriceOrPercent);
                    switch (levelTrend)
                    {
                        case LevelTrend.Undecided:
                            levelTrend = newLevelTrend;
                            break;
                        case LevelTrend.DealBetterAsLevelIncreases:
                        case LevelTrend.DealWorseAsLevelIncreases:
                            if (levelTrend != newLevelTrend)
                            {
                                levelTrend = LevelTrend.Undecided;
                            }

                            break;
                    }

                    if (levelTrend == LevelTrend.Undecided)
                    {
                        break;
                    }
                }

                previousLevelPriceOrPercentage = pair.Value.DiscountPriceOrPercent;
            }

            List<DiscountApplication> discountApplications = new List<DiscountApplication>();

            // If this discount cannot be applied, return an empty collection.
            if (discountableItemGroups == null
                || remainingQuantities == null
                || !this.CanDiscountApply(transaction, storePriceGroups, currencyCode, pricingDataManager, isReturn, priceContext))
            {
                return discountApplications;
            }

            // Get the discount code to use for any discount lines, if one is required.
            string discountCodeUsed = this.GetDiscountCodeForDiscount(transaction);

            // Otherwise, find each possible permutation.
            IDictionary<long, IList<RetailDiscountLine>> triggeringProducts = this.GetProductOrVariantIdToRetailDiscountLinesMap();

            HashSet<RetailDiscountLine> triggeredDiscountLines = new HashSet<RetailDiscountLine>();

            foreach (long key in triggeringProducts.Keys)
            {
                foreach (RetailDiscountLine line in triggeringProducts[key])
                {
                    triggeredDiscountLines.Add(line);
                }
            }

            HashSet<string> discountPriceGroups = new HashSet<string>(ConvertPriceDiscountGroupIdsToGroups(this.PriceDiscountGroupIds, priceContext));

            // Create a BitSet that can hold the valid items for both positive and negative quantities
            BitSet availableItemGroups = new BitSet((uint)discountableItemGroups.Length);

            for (int x = 0; x < discountableItemGroups.Length; x++)
            {
                DiscountableItemGroup discountableItemGroup = discountableItemGroups[x];

                if (remainingQuantities[x] != 0M
                    && DiscountBase.IsDiscountAllowedForDiscountableItemGroup(discountableItemGroup)
                    && DiscountBase.IsDiscountAllowedForCatalogIds(priceContext, discountPriceGroups, discountableItemGroup.CatalogIds)
                    && this.ContainsProductForRetailDiscountLines(discountableItemGroup.ProductId, discountableItemGroup.MasterProductId, discountableItemGroup.SalesOrderUnitOfMeasure))
                {
                    availableItemGroups[x] = true;
                }
            }

            // Now we have the different groups that could get a discount from this, as well as the discount lines that can apply.
            discountApplications.AddRange(this.GetDiscountApplicationsForItems(availableItemGroups, triggeredDiscountLines, discountableItemGroups, remainingQuantities, discountCodeUsed));

            SetNumberOfTimesAppliedForDiscountApplications(levelTrend, highestQuantityLevel, discountApplications);

            return discountApplications;
        }

        /// <summary>
        /// Applies the discount application and gets the value, taking into account previously applied discounts.
        /// </summary>
        /// <param name="discountableItemGroups">The transaction line items.</param>
        /// <param name="remainingQuantities">The quantities remaining for each item.</param>
        /// <param name="appliedDiscounts">The previously applied discounts.</param>
        /// <param name="discountApplication">The specific application of the discount to use.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <param name="hasCompetingApplications">A value indicating whether it has competing applications.</param>
        /// <returns>The value of the discount application.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Parameter is correct for this usage.")]
        public override AppliedDiscountApplication GetAppliedDiscountApplication(DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, Stack<AppliedDiscountApplication> appliedDiscounts, DiscountApplication discountApplication, PriceContext priceContext, bool hasCompetingApplications)
        {
            decimal result = 0M;

            if (discountApplication == null || discountableItemGroups == null)
            {
                return null;
            }

            decimal[] prices;
            Dictionary<int, IList<DiscountLineQuantity>> discountDictionary = this.GetExistingDiscountDictionaryAndDiscountedPrices(discountableItemGroups, remainingQuantities, appliedDiscounts, discountApplication, out prices);

            MultipleBuyDiscountMethod discountMethod = (MultipleBuyDiscountMethod)discountApplication.Discount.DiscountType;
            Dictionary<int, decimal> itemIndexToDiscountValueMap = new Dictionary<int, decimal>();
            foreach (RetailDiscountLineItem item in discountApplication.RetailDiscountLines)
            {
                decimal discountValue = decimal.Zero;
                switch (discountMethod)
                {
                    case MultipleBuyDiscountMethod.PercentOff:
                        discountValue = prices[item.ItemIndex] * (discountApplication.DiscountPercentValue / 100M);
                        break;
                    case MultipleBuyDiscountMethod.UnitPrice:
                        decimal dealPrice = discountApplication.DealPriceValue;

                        decimal bestExistingDealPrice = 0m;
                        bool hasExistingDealPrice = DiscountBase.TryGetBestExistingDealPrice(discountDictionary, item.ItemIndex, out bestExistingDealPrice);

                        discountValue = DiscountBase.GetDiscountAmountFromDealPrice(prices[item.ItemIndex], hasExistingDealPrice, bestExistingDealPrice, dealPrice);
                        itemIndexToDiscountValueMap[item.ItemIndex] = discountValue;

                        break;

                    default:
                        break;
                }

                result += discountValue * discountApplication.ItemQuantities[item.ItemIndex];
            }

            AppliedDiscountApplication newAppliedDiscountApplication = null;
            if (result > decimal.Zero)
            {
                newAppliedDiscountApplication = new AppliedDiscountApplication(discountApplication, result, (decimal[])discountApplication.ItemQuantities.Clone(), isDiscountLineGenerated: true);
                foreach (RetailDiscountLineItem retailDiscountLineItem in discountApplication.RetailDiscountLines)
                {
                    DiscountLine discountLine = this.NewDiscountLine(discountApplication.DiscountCode, discountableItemGroups[retailDiscountLineItem.ItemIndex].ItemId);

                    discountLine.PeriodicDiscountType = PeriodicDiscountOfferType.MultipleBuy;
                    discountLine.DealPrice = discountApplication.DealPriceValue;
                    discountLine.DealPrice = discountApplication.DealPriceValue;
                    if (discountMethod == MultipleBuyDiscountMethod.UnitPrice)
                    {
                        decimal discountAmount = decimal.Zero;
                        itemIndexToDiscountValueMap.TryGetValue(retailDiscountLineItem.ItemIndex, out discountAmount);
                        discountLine.Amount = discountAmount;
                    }
                    
                    discountLine.Percentage = discountApplication.DiscountPercentValue;

                    newAppliedDiscountApplication.AddDiscountLine(retailDiscountLineItem.ItemIndex, new DiscountLineQuantity(discountLine, discountApplication.ItemQuantities[retailDiscountLineItem.ItemIndex]));
                }
            }

            return newAppliedDiscountApplication;
        }

        /// <summary>
        /// Generate discount lines for the applied discount application.
        /// </summary>
        /// <param name="appliedDiscountApplication">The applied discount application.</param>
        /// <param name="discountableItemGroups">The discountable item groups.</param>
        /// <param name="priceContext">The price context.</param>
        public override void GenerateDiscountLines(AppliedDiscountApplication appliedDiscountApplication, DiscountableItemGroup[] discountableItemGroups, PriceContext priceContext)
        {
            // Nothing here, already generated in GetAppliedDiscountApplication.
        }

        /// <summary>
        /// Gets the sort index to use for a discount application using the specified discount line.
        /// </summary>
        /// <param name="line">The discount line to determine the sort index on.</param>
        /// <returns>The sort index to use.</returns>
        protected override int GetSortIndexForRetailDiscountLine(RetailDiscountLine line)
        {
            return this.DiscountType == DiscountMethodType.DealPrice ? (int)DiscountOfferMethod.OfferPrice : (int)DiscountOfferMethod.DiscountPercent;
        }

        /// <summary>
        /// Converts the collection of selected items into an array of decimals.
        /// </summary>
        /// <param name="size">The size of the array.</param>
        /// <param name="selectedItems">The items selected for the discount application.</param>
        /// <returns>The array containing the quantities used for each item.</returns>
        private static decimal[] GetItemQuantities(int size, IEnumerable<int> selectedItems)
        {
            decimal[] result = new decimal[size];

            foreach (int selectedItem in selectedItems)
            {
                result[selectedItem]++;
            }

            return result;
        }

        private static LevelTrend ComputeLevelTrend(MultipleBuyDiscountMethod discountMethod, decimal previousPriceOrPercentage, decimal priceOrPercentage)
        {
            bool isBetterLevel;
            if (discountMethod == MultipleBuyDiscountMethod.UnitPrice)
            {
                isBetterLevel = previousPriceOrPercentage >= priceOrPercentage;
            }
            else
            {
                isBetterLevel = priceOrPercentage >= previousPriceOrPercentage;
            }

            return isBetterLevel ? LevelTrend.DealBetterAsLevelIncreases : LevelTrend.DealWorseAsLevelIncreases;
        }

        private static void SetNumberOfTimesAppliedForDiscountApplications(LevelTrend levelTrend, decimal highestQuantityLevel, List<DiscountApplication> discountApplications)
        {
            if (levelTrend == LevelTrend.DealBetterAsLevelIncreases)
            {
                foreach (DiscountApplication discountApplication in discountApplications)
                {
                    decimal quantityLevel = discountApplication.ItemQuantities.Sum();
                    if (quantityLevel < highestQuantityLevel && quantityLevel > 0)
                    {
                        discountApplication.NumberOfTimesApplicable = (int)Math.Ceiling(highestQuantityLevel / quantityLevel) - 1;
                    }
                    else if (quantityLevel > highestQuantityLevel)
                    {
                        discountApplication.NumberOfTimesApplicable = 1;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the sort value to use for the selected items.  This value will be an approximation of the amount or percentage for the discount application.
        /// </summary>
        /// <param name="discountableItemGroups">The item groups on the transaction.</param>
        /// <param name="discountLineItems">Retail discount line plus item index.</param>
        /// <param name="itemQuantities">Item quantities.</param>
        /// <param name="discountUnitPriceOrPercent">Discount unit price or percentage.</param>
        /// <returns>The sort value.</returns>
        private decimal GetSortValue(DiscountableItemGroup[] discountableItemGroups, List<RetailDiscountLineItem> discountLineItems, decimal[] itemQuantities, decimal discountUnitPriceOrPercent)
        {
            decimal sortValue = discountUnitPriceOrPercent;

            if (this.DiscountType == DiscountMethodType.DealPrice)
            {
                decimal totalPrice = decimal.Zero;
                decimal totalQuantity = decimal.Zero;

                foreach (RetailDiscountLineItem discountLineItem in discountLineItems)
                {
                    int itemIndex = discountLineItem.ItemIndex;
                    if (discountableItemGroups[itemIndex].Price > discountUnitPriceOrPercent)
                    {
                        totalPrice += discountableItemGroups[itemIndex].Price * itemQuantities[itemIndex];
                        totalQuantity += itemQuantities[itemIndex];
                    }
                }

                sortValue = totalPrice - (discountUnitPriceOrPercent * totalQuantity);
            }

            return sortValue;
        }

        /// <summary>
        /// Gets the collection of discount applications for the items on the transaction.
        /// </summary>
        /// <param name="availableItemGroups">The available item groups to consider.</param>
        /// <param name="triggeredDiscountLines">The collection of discount lines triggered by the item groups.</param>
        /// <param name="discountableItemGroups">The item groups on the transaction.</param>
        /// <param name="remainingQuantities">The remaining quantities of the item groups.</param>
        /// <param name="discountCodeUsed">The discount code used for triggering this discount.</param>
        /// <returns>The collection of discount applications for this discount on the transaction.</returns>
        private IEnumerable<DiscountApplication> GetDiscountApplicationsForItems(BitSet availableItemGroups, HashSet<RetailDiscountLine> triggeredDiscountLines, DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, string discountCodeUsed)
        {
            List<DiscountApplication> result = new List<DiscountApplication>();

            if (availableItemGroups.IsZero())
            {
                return result;
            }

            int currentIndex = 0;

            // Include maxDepth of up to 2 * max tier - 1 (since 2 * max would be 2 separate discounts).
            int minDepth = decimal.ToInt32(this.QuantityDiscountLevels.Min(p => p.MinimumQuantity));
            int maxDepth = (decimal.ToInt32(this.QuantityDiscountLevels.Max(p => p.MinimumQuantity)) * 2) - 1;

            decimal[] qtyRemaining = (decimal[])remainingQuantities.Clone();

            Stack<int> itemsUsed = new Stack<int>();

            // Loop through all of the triggered discount lines
            foreach (RetailDiscountLine line in triggeredDiscountLines)
            {
                currentIndex = this.GetApplicationsForLine(availableItemGroups, discountableItemGroups, remainingQuantities, discountCodeUsed, result, currentIndex, minDepth, maxDepth, qtyRemaining, itemsUsed, line);
            }

            return result;
        }

        /// <summary>
        /// Gets the possible discount applications for the specified discount line.
        /// </summary>
        /// <param name="availableItemGroups">The available item groups to consider.</param>
        /// <param name="discountableItemGroups">The item groups on the transaction.</param>
        /// <param name="remainingQuantities">The remaining quantities of the item groups.</param>
        /// <param name="discountCodeUsed">The discount code used for triggering this discount.</param>
        /// <param name="result">The collection of DiscountApplication objects that will become the result for this discount.</param>
        /// <param name="currentIndex">The current index within the available item groups.</param>
        /// <param name="minDepth">The minimum number of items required to trigger the discount.</param>
        /// <param name="maxDepth">The maximum number of items required to trigger the discount.</param>
        /// <param name="qtyRemaining">The remaining quantities of the item groups after other items have been used.</param>
        /// <param name="itemsUsed">The items used on the current path.</param>
        /// <param name="line">The discount line to determine the possible applications for.</param>
        /// <returns>The currentIndex value to use for the next iteration.</returns>
        private int GetApplicationsForLine(BitSet availableItemGroups, DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, string discountCodeUsed, List<DiscountApplication> result, int currentIndex, int minDepth, int maxDepth, decimal[] qtyRemaining, Stack<int> itemsUsed, RetailDiscountLine line)
        {
            bool doFractional = this.HasFractionalLevel();

            if (!doFractional)
            {
                doFractional = this.HasFractionalQuantity(discountableItemGroups, line);
            }

            if (doFractional)
            {
                this.AddAllQualifiedQuantitiesAsOneDiscountAppliationToResult(discountableItemGroups, line, result, discountCodeUsed);
            }
            else
            {
                while (true)
                {
                    currentIndex = availableItemGroups.GetNextNonZeroBit(currentIndex);

                    if (currentIndex == BitSet.UnknownBit || itemsUsed.Count >= maxDepth)
                    {
                        if (itemsUsed.Count > 0)
                        {
                            // No next non-zero bit exists, move to the next item from the previous parent
                            int lastItem = itemsUsed.Pop();
                            qtyRemaining[lastItem] += 1;
                            currentIndex = lastItem + 1;
                        }
                        else
                        {
                            // No next non-zero bit exists and we are at the top level, exit the loop
                            currentIndex = 0;
                            break;
                        }
                    }
                    else if (this.IsItemValidForLine(discountableItemGroups[currentIndex], line)
                        && (qtyRemaining[currentIndex] >= 1M || qtyRemaining[currentIndex] <= -1M))
                    {
                        itemsUsed.Push(currentIndex);
                        qtyRemaining[currentIndex] -= 1;

                        // Determine if a tier exists for this depth
                        if (itemsUsed.Count >= minDepth)
                        {
                            this.AddDiscountApplicationToAvailableResult(discountableItemGroups, remainingQuantities, discountCodeUsed, result, itemsUsed, line);
                        }
                    }
                    else
                    {
                        // Not enough items left in this index or this item is not in the correct discount line, move to the next one
                        currentIndex++;
                    }
                }
            }

            return currentIndex;
        }

        private bool HasFractionalLevel()
        {
            bool hasFractionalLevel = false;

            foreach (QuantityDiscountLevel level in this.QuantityDiscountLevels)
            {
                hasFractionalLevel = DiscountableItemGroup.IsFraction(level.MinimumQuantity);
                if (hasFractionalLevel)
                {
                    break;
                }
            }

            return hasFractionalLevel;
        }

        private bool HasFractionalQuantity(DiscountableItemGroup[] discountableItemGroups, RetailDiscountLine retailDiscountLine)
        {
            bool hasFranctionalQuantity = false;

            foreach (DiscountableItemGroup item in discountableItemGroups)
            {
                if (this.IsItemValidForLine(item, retailDiscountLine) && DiscountableItemGroup.IsFraction(item.Quantity))
                {
                    hasFranctionalQuantity = true;
                    break;
                }
            }

            return hasFranctionalQuantity;
        }

        private void AddAllQualifiedQuantitiesAsOneDiscountAppliationToResult(DiscountableItemGroup[] discountableItemGroups, RetailDiscountLine retailDiscountLine, List<DiscountApplication> result, string discountCodeUsed)
        {
            List<RetailDiscountLineItem> discountLineItems = new List<RetailDiscountLineItem>();
            decimal[] itemQuantities = new decimal[discountableItemGroups.Length];
            decimal totalQuantity = 0;

            for (int i = 0; i < discountableItemGroups.Length; i++)
            {
                DiscountableItemGroup item = discountableItemGroups[i];
                if (this.IsItemValidForLine(item, retailDiscountLine))
                {
                    discountLineItems.Add(new RetailDiscountLineItem(i, retailDiscountLine));
                    itemQuantities[i] = item.Quantity;
                    totalQuantity += item.Quantity;
                }
            }

            QuantityDiscountLevel quantityLevel = this.GetQuantityLevel(totalQuantity);

            if (quantityLevel != null)
            {
                this.AddDiscountApplicationToAvailableResult(discountableItemGroups, discountLineItems, retailDiscountLine, quantityLevel.DiscountPriceOrPercent, itemQuantities, discountCodeUsed, result);
            }
        }

        /// <summary>
        /// Adds a discount application to the available discount applications result.
        /// </summary>
        /// <param name="discountableItemGroups">The item groups on the transaction.</param>
        /// <param name="remainingQuantities">The remaining quantities of items on the transaction.</param>
        /// <param name="discountCodeUsed">The discount code used for triggering this discount.</param>
        /// <param name="result">The collection of DiscountApplication objects that will be included in the result.</param>
        /// <param name="itemsUsed">The items used for this particular application.</param>
        /// <param name="line">The discount line used for this particular application.</param>
        private void AddDiscountApplicationToAvailableResult(DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, string discountCodeUsed, List<DiscountApplication> result, Stack<int> itemsUsed, RetailDiscountLine line)
        {
            // We don't put duplicate line items in DiscountApplication, instead we rely on DiscountApplication.ItemQuantities to tell applicable quantity for each line item.
            HashSet<int> itemIndexSet = new HashSet<int>();
            decimal[] itemQuantities = new decimal[discountableItemGroups.Length];
            foreach (int x in itemsUsed)
            {
                itemIndexSet.Add(x);
                itemQuantities[x] += 1m;
            }

            List<RetailDiscountLineItem> discountLineItems = new List<RetailDiscountLineItem>(itemIndexSet.Count);
            foreach (int itemIndex in itemIndexSet)
            {
                discountLineItems.Add(new RetailDiscountLineItem(itemIndex, line));
            }

            QuantityDiscountLevel quantityLevel = this.GetQuantityLevel(itemsUsed.Count);

            if (quantityLevel != null)
            {
                this.AddDiscountApplicationToAvailableResult(discountableItemGroups, discountLineItems, line, quantityLevel.DiscountPriceOrPercent, GetItemQuantities(remainingQuantities.Length, itemsUsed), discountCodeUsed, result);
            }
        }

        private void AddDiscountApplicationToAvailableResult(
            DiscountableItemGroup[] discountableItemGroups,
            List<RetailDiscountLineItem> discountLineItems, 
            RetailDiscountLine retailDiscountLine, 
            decimal discountUnitPriceOrPercent,
            decimal[] itemQuantites, 
            string discountCodeUsed, 
            List<DiscountApplication> result)
        {
            DiscountApplication app = new DiscountApplication(this)
            {
                RetailDiscountLines = discountLineItems,
                SortIndex = this.GetSortIndexForRetailDiscountLine(retailDiscountLine),
                SortValue = this.GetSortValue(discountableItemGroups, discountLineItems, itemQuantites, discountUnitPriceOrPercent),
                ItemQuantities = itemQuantites,
                DiscountCode = discountCodeUsed,
                DealPriceValue = this.DiscountType == DiscountMethodType.DealPrice ? discountUnitPriceOrPercent : decimal.Zero,
                DiscountPercentValue = this.DiscountType == DiscountMethodType.DiscountPercent ? discountUnitPriceOrPercent : decimal.Zero,
                DiscountAmountValue = decimal.Zero,
            };

            result.Add(app);
        }

        private QuantityDiscountLevel GetQuantityLevel(decimal quantity)
        {
            return this.QuantityDiscountLevels.Where(p => p.MinimumQuantity <= quantity).OrderByDescending(p => p.MinimumQuantity).FirstOrDefault();
        }
    }
}
