﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine.DiscountData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Abstract class containing all of the standard properties that are shared across discount types.  For specific discount types, see one of the implementations of this class.
    /// </summary>
    public abstract class DiscountBase
    {
        /// <summary>
        /// The value for the NumberOfTimesApplicable property that indicates that there is no limit.
        /// </summary>
        public static readonly int UnlimitedNumberOfTimesApplicable = 0;

        /// <summary>
        /// Constant used to determine if a product ID or variant ID was not set.
        /// </summary>
        public static readonly long AnyProductOrVariant = 0;

        /// <summary>
        /// Constant used to determine if an index value is invalid or not found.
        /// </summary>
        protected const int InvalidIndex = -1;

        /// <summary>
        /// Initializes a new instance of the <see cref="DiscountBase" /> class.
        /// </summary>
        protected DiscountBase()
        {
            this.DiscountLines = new Dictionary<decimal, RetailDiscountLine>();
            this.DiscountCodes = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            this.PriceDiscountGroupIds = new HashSet<long>();

            //// DiscountBase list is loaded in 2 ways:
            //// 1. load all retail discounts and then filter by sales lines
            ////    product to category map is needed to filter discount lines and IsProductOrVariantIdToDiscountLinesMapSet is false initially.
            //// 2. load retail discounts by sales lines
            ////    filtering is done already and no more addition processing. IsProductOrVariantIdToDiscountLinesMapSet is true from the beginning.
            this.ProductOfVariantToDiscountLinesMap = new Dictionary<long, IList<RetailDiscountLine>>();
            this.CategoryToProductOrVariantIdsMap = new Dictionary<long, IList<RetailCategoryMember>>();
            this.IsCategoryToProductOrVariantIdsMapSet = false;
        }

        /// <summary>
        /// Gets or sets the offer ID for the discount.
        /// </summary>
        public string OfferId { get; set; }

        /// <summary>
        /// Gets or sets the currency code required for this discount.
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Gets the pricing group identifiers for this discount.
        /// </summary>
        public ISet<long> PriceDiscountGroupIds { get; private set; }

        /// <summary>
        /// Gets or sets the name of this discount offer.
        /// </summary>
        public string OfferName { get; set; }

        /// <summary>
        /// Gets or sets the type of discount (mix and match, quantity, threshold, etc.).
        /// </summary>
        public PeriodicDiscountOfferType PeriodicDiscountType { get; set; }

        /// <summary>
        /// Gets or sets the concurrency mode for this discount (exclusive, best-price, compound).
        /// </summary>
        public ConcurrencyMode ConcurrencyMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not a discount code is required to trigger this discount.
        /// </summary>
        public bool IsDiscountCodeRequired { get; set; }

        /// <summary>
        /// Gets the collection containing all of the discount codes that can trigger this discount, if one is required.
        /// </summary>
        public ISet<string> DiscountCodes { get; private set; }

        /// <summary>
        /// Gets or sets the validation period ID for testing if this offer is valid.
        /// </summary>
        public string DateValidationPeriodId { get; set; }

        /// <summary>
        /// Gets or sets the validation type to use for date validation for this offer.
        /// </summary>
        public DateValidationType DateValidationType { get; set; }

        /// <summary>
        /// Gets or sets the starting date for this offer.
        /// </summary>
        public DateTimeOffset ValidFrom { get; set; }

        /// <summary>
        /// Gets or sets the expiration date for this offer.
        /// </summary>
        public DateTimeOffset ValidTo { get; set; }

        /// <summary>
        /// Gets or sets the discount method type for this offer.
        /// </summary>
        public DiscountMethodType DiscountType { get; set; }

        /// <summary>
        /// Gets or sets the deal-price value of this offer.
        /// </summary>
        public decimal DealPriceValue { get; set; }

        /// <summary>
        /// Gets or sets the discount percentage value for this offer.
        /// </summary>
        public decimal DiscountPercentValue { get; set; }

        /// <summary>
        /// Gets or sets the discount amount value for this offer.
        /// </summary>
        public decimal DiscountAmountValue { get; set; }

        /// <summary>
        /// Gets or sets the number of least-expensive lines to use in triggering this offer.
        /// </summary>
        public int NumberOfLeastExpensiveLines { get; set; }

        /// <summary>
        /// Gets or sets the number of times this offer can be applied to a transaction.
        /// </summary>
        public int NumberOfTimesApplicable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether non-discount items contribute to threshold amount trigger.
        /// </summary>
        public bool ShouldCountNonDiscountItems { get; set; }

        /// <summary>
        /// Gets the retail discount line number to retail discount line map.
        /// </summary>
        internal IDictionary<decimal, RetailDiscountLine> DiscountLines { get; private set; }

        /// <summary>
        /// Gets or sets product or variant identifiers in transaction.
        /// </summary>
        internal ISet<long> ProductOrVariantIdsInTransaction { get; set; }

        /// <summary>
        /// Gets product or variant id to retail discount lines map that are relevant for the transaction.
        /// </summary>
        internal IDictionary<long, IList<RetailDiscountLine>> ProductOfVariantToDiscountLinesMap { get; private set; }

        /// <summary>
        /// Gets or sets product or variant to categories map that is relevant for the transaction.
        /// </summary>
        internal Dictionary<long, IList<RetailCategoryMember>> CategoryToProductOrVariantIdsMap { get; set; }

        internal bool IsCategoryToProductOrVariantIdsMapSet { get; set; }

        /// <summary>
        /// Gets all of the possible applications of this discount to the specified transaction and line items.
        /// </summary>
        /// <param name="transaction">The transaction to consider for discounts.</param>
        /// <param name="discountableItemGroups">The valid sales line items on the transaction to consider.</param>
        /// <param name="remainingQuantities">The remaining quantities of each of the sales lines to consider.</param>
        /// <param name="storePriceGroups">The collection of price groups that this store belongs to.</param>
        /// <param name="currencyCode">The currency code in use for the current transaction.</param>
        /// <param name="pricingDataManager">The IPricingDataManager instance to use.</param>
        /// <param name="isReturn">The flag indicating whether or not it's for return.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <returns>The possible permutations of line items that this discount can apply to, or an empty collection if this discount cannot apply.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Parameter is correct for this usage.")]
        public abstract IEnumerable<DiscountApplication> GetDiscountApplications(
            SalesTransaction transaction,
            DiscountableItemGroup[] discountableItemGroups,
            decimal[] remainingQuantities,
            IEnumerable<long> storePriceGroups,
            string currencyCode,
            IPricingDataManagerV2 pricingDataManager,
            bool isReturn,
            PriceContext priceContext);

        /// <summary>
        /// Applies the discount application and gets the value, taking into account previously applied discounts.
        /// </summary>
        /// <param name="discountableItemGroups">The transaction line items.</param>
        /// <param name="remainingQuantities">The quantities remaining for each item.</param>
        /// <param name="appliedDiscounts">The previously applied discounts.</param>
        /// <param name="discountApplication">The specific application of the discount to use.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <param name="hasCompetingApplications">A value indicating whether it has competing applications.</param>
        /// <returns>The value of the discount application.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Parameter is correct for this usage.")]
        public abstract AppliedDiscountApplication GetAppliedDiscountApplication(DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, Stack<AppliedDiscountApplication> appliedDiscounts, DiscountApplication discountApplication, PriceContext priceContext, bool hasCompetingApplications);

        /// <summary>
        /// Generate discount lines for the applied discount application.
        /// </summary>
        /// <param name="appliedDiscountApplication">The applied discount application.</param>
        /// <param name="discountableItemGroups">The discountable item groups.</param>
        /// <param name="priceContext">The price context.</param>
        public abstract void GenerateDiscountLines(AppliedDiscountApplication appliedDiscountApplication, DiscountableItemGroup[] discountableItemGroups, PriceContext priceContext);

        /// <summary>
        /// Gets product or variant Id to retail discount lines map.
        /// </summary>
        /// <returns>Product or variant Id to retail discount lines map.</returns>
        public IDictionary<long, IList<RetailDiscountLine>> GetProductOrVariantIdToRetailDiscountLinesMap()
        {
            //// DiscountBase list is loaded in 2 ways:
            //// 1. load all retail discounts and then filter by sales lines
            ////    product to category map is needed to filter discount lines and IsProductOrVariantIdToDiscountLinesMapSet is false initially.
            //// 2. load retail discounts by sales lines
            ////    filtering is done already and no more addition processing. IsProductOrVariantIdToDiscountLinesMapSet is true from the beginning.
            if (!this.IsCategoryToProductOrVariantIdsMapSet && this.ProductOrVariantIdsInTransaction != null)
            {
                foreach (KeyValuePair<decimal, RetailDiscountLine> pair in this.DiscountLines)
                {
                    RetailDiscountLine line = pair.Value;

                    if (line.DistinctProductVariantId != AnyProductOrVariant)
                    {
                        if (this.ProductOrVariantIdsInTransaction.Contains(line.DistinctProductVariantId))
                        {
                            this.AddDiscountLineToDictionary(line.DistinctProductVariantId, line);
                        }
                    }
                    else if (line.ProductId != AnyProductOrVariant)
                    {
                        if (this.ProductOrVariantIdsInTransaction.Contains(line.ProductId))
                        {
                            this.AddDiscountLineToDictionary(line.ProductId, line);
                        }
                    }
                    else if (this.CategoryToProductOrVariantIdsMap != null)
                    {
                        if (this.CategoryToProductOrVariantIdsMap.ContainsKey(line.CategoryId))
                        {
                            foreach (RetailCategoryMember member in this.CategoryToProductOrVariantIdsMap[line.CategoryId])
                            {
                                this.AddDiscountLineToDictionary(member.ProductOrVariantId, line);
                            }
                        }
                    }
                }

                this.IsCategoryToProductOrVariantIdsMapSet = true;
            }

            return this.ProductOfVariantToDiscountLinesMap;
        }

        /// <summary>
        /// Determines if the second array passed in as a parameter can fit into the first array by comparing the values at each index of the array.
        /// </summary>
        /// <param name="possibleSubset">The first (possible subset) array.</param>
        /// <param name="superset">The second (superset) array.</param>
        /// <returns>True if array2 can fit inside of array1, false otherwise.</returns>
        internal static bool IsDecimalArrayASubset(decimal[] possibleSubset, decimal[] superset)
        {
            if (possibleSubset == null || superset == null || possibleSubset.Length != superset.Length)
            {
                return false;
            }

            for (int x = 0; x < possibleSubset.Length; x++)
            {
                if (possibleSubset[x] > superset[x])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Creates a new DiscountLine that is a copy of the specified line.
        /// </summary>
        /// <param name="original">The original discount line.</param>
        /// <returns>The new discount line.</returns>
        protected static DiscountLine CloneDiscountItem(DiscountLine original)
        {
            DiscountLine newLine = new DiscountLine();
            newLine.CopyFrom(original);

            return newLine;
        }

        /// <summary>
        /// Determines whether the specified item group is eligible for discounts (based on the NoDiscount flag).
        /// </summary>
        /// <param name="discountableItemGroup">The item group to examine.</param>
        /// <returns>True if the item may be discounted, false otherwise.</returns>
        protected static bool IsDiscountAllowedForDiscountableItemGroup(DiscountableItemGroup discountableItemGroup)
        {
            if (discountableItemGroup != null && discountableItemGroup.ExtendedProperties != null)
            {
                return !discountableItemGroup.ExtendedProperties.NoDiscountAllowed;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Translates price discount groups from record identifiers into the text identifiers for the groups.
        /// </summary>
        /// <param name="priceDiscountGroupIds">The record identifiers for the discount groups.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <returns>A collection of text identifiers for the price discount groups.</returns>
        protected static IEnumerable<string> ConvertPriceDiscountGroupIdsToGroups(ISet<long> priceDiscountGroupIds, PriceContext priceContext)
        {
            foreach (long id in priceDiscountGroupIds)
            {
                if (priceContext.RecordIdsToPriceGroupIdsDictionary.ContainsKey(id))
                {
                    yield return priceContext.RecordIdsToPriceGroupIdsDictionary[id];
                }
            }
        }

        /// <summary>
        /// Determines if the discount is allowed for the specified catalog identifiers, based on the discount price groups for this discount. 
        /// </summary>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <param name="discountPriceGroups">The set of discount price group text identifiers.</param>
        /// <param name="itemCatalogIds">The catalog IDs for the item.</param>
        /// <returns>True if the discount is allowed for this catalog, false otherwise.</returns>
        protected static bool IsDiscountAllowedForCatalogIds(PriceContext priceContext, ISet<string> discountPriceGroups, ISet<long> itemCatalogIds)
        {
            return PriceContextHelper.IsApplicableForDiscount(priceContext, discountPriceGroups, itemCatalogIds);
        }

        /// <summary>
        /// Get discount amount from deal price, taking into account existing deal price discounts.
        /// </summary>
        /// <param name="price">The price.</param>
        /// <param name="hasExistingDealPrice">A flag indicating whether or not it has existing deal price discounts.</param>
        /// <param name="bestExistingDealPrice">Best existing deal price.</param>
        /// <param name="dealPrice">Deal price.</param>
        /// <returns>The discount amount.</returns>
        protected static decimal GetDiscountAmountFromDealPrice(decimal price, bool hasExistingDealPrice, decimal bestExistingDealPrice, decimal dealPrice)
        {
            decimal discountAmount = decimal.Zero;

            if (hasExistingDealPrice)
            {
                discountAmount = bestExistingDealPrice - dealPrice;
            }
            else
            {
                discountAmount = price - dealPrice;
            }

            return discountAmount > decimal.Zero ? discountAmount : decimal.Zero;
        }

        /// <summary>
        /// Try getting the best existing deal price.
        /// </summary>
        /// <param name="discountDictionary">Discount dictionary of existing discount lines.</param>
        /// <param name="itemIndex">Item index.</param>
        /// <param name="bestExistingDealPrice">Bet existing deal price.</param>
        /// <returns>true if it has best exiting deal price; false otherwise.</returns>
        protected static bool TryGetBestExistingDealPrice(Dictionary<int, IList<DiscountLineQuantity>> discountDictionary, int itemIndex, out decimal bestExistingDealPrice)
        {
            bool hasExistingDealPrice = false;
            bestExistingDealPrice = 0m;
            IList<DiscountLineQuantity> existingLines = null;
            if (discountDictionary != null && discountDictionary.TryGetValue(itemIndex, out existingLines))
            {
                foreach (DiscountLineQuantity existingLine in existingLines)
                {
                    if (existingLine.DiscountLine.DealPrice > 0m && (!hasExistingDealPrice || existingLine.DiscountLine.DealPrice < bestExistingDealPrice))
                    {
                        bestExistingDealPrice = existingLine.DiscountLine.DealPrice;
                        hasExistingDealPrice = true;
                    }
                }
            }

            return hasExistingDealPrice;
        }

        /// <summary>
        /// Try getting retail discount lines by variant Id or product identifier.
        /// </summary>
        /// <param name="productOrVariantId">Product or variant identifier.</param>
        /// <param name="masterProductId">Master product identifier.</param>
        /// <param name="unitOfMeasure">Unit of measure.</param>
        /// <param name="lines">Retail discount lines.</param>
        /// <returns>True if found.</returns>
        /// <remarks>The key of triggering product dictionary is one of variant Id, product identifier or master product identifier.</remarks>
        protected bool TryGetRetailDiscountLines(long productOrVariantId, long masterProductId, string unitOfMeasure, out IList<RetailDiscountLine> lines)
        {
            IList<RetailDiscountLine> discountLines = null;
            IDictionary<long, IList<RetailDiscountLine>> productOrVariantIdToRetailDiscountLinesMap = this.GetProductOrVariantIdToRetailDiscountLinesMap();

            productOrVariantIdToRetailDiscountLinesMap.TryGetValue(productOrVariantId, out discountLines);

            if (productOrVariantId != masterProductId)
            {
                IList<RetailDiscountLine> moreLines = null;
                productOrVariantIdToRetailDiscountLinesMap.TryGetValue(masterProductId, out moreLines);

                if (moreLines != null && moreLines.Count > 0)
                {
                    if (discountLines == null || discountLines.Count == 0)
                    {
                        discountLines = moreLines;
                    }
                    else
                    {
                        foreach (RetailDiscountLine discountLine in moreLines)
                        {
                            if (!discountLines.Where(p => p.DiscountLineNumber == discountLine.DiscountLineNumber).Any())
                            {
                                discountLines.Add(discountLine);
                            }
                        }
                    }
                }
            }

            lines = new List<RetailDiscountLine>();
            if (discountLines != null)
            {
                lines.AddRange(discountLines.Where(p => string.IsNullOrWhiteSpace(p.UnitOfMeasureSymbol) || string.Equals(p.UnitOfMeasureSymbol, unitOfMeasure, StringComparison.OrdinalIgnoreCase)));
            }

            return lines.Count > 0;
        }

        /// <summary>
        /// Determines if the trigger products contain the product or the variant identifier.
        /// </summary>
        /// <param name="productOrVariantId">Product or variant identifier.</param>
        /// <param name="masterProductId">Master product identifier.</param>
        /// <param name="unitOfMeasure">Unit of measure.</param>
        /// <returns>True if found.</returns>
        protected bool ContainsProductForRetailDiscountLines(long productOrVariantId, long masterProductId, string unitOfMeasure)
        {
            IDictionary<long, IList<RetailDiscountLine>> productOrVariantIdToRetailDiscountLinesMap = this.GetProductOrVariantIdToRetailDiscountLinesMap();

            IList<RetailDiscountLine> discountLines = null;
            bool found = false;
            if (productOrVariantIdToRetailDiscountLinesMap.TryGetValue(productOrVariantId, out discountLines))
            {
                found = discountLines != null && discountLines.Where(p => string.IsNullOrWhiteSpace(p.UnitOfMeasureSymbol) || string.Equals(p.UnitOfMeasureSymbol, unitOfMeasure, StringComparison.OrdinalIgnoreCase)).Any();
            }

            if (!found && productOrVariantId != masterProductId)
            {
                if (productOrVariantIdToRetailDiscountLinesMap.TryGetValue(masterProductId, out discountLines))
                {
                    found = discountLines != null && discountLines.Where(p => string.IsNullOrWhiteSpace(p.UnitOfMeasureSymbol) || string.Equals(p.UnitOfMeasureSymbol, unitOfMeasure, StringComparison.OrdinalIgnoreCase)).Any();
                }
            }

            return found;
        }

        /// <summary>
        /// Determines if the specified item is valid for the specified discount line.
        /// </summary>
        /// <param name="discountableItemGroup">The item to consider.</param>
        /// <param name="discountLine">The discount line to consider.</param>
        /// <returns>True if the item is valid, false otherwise.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Parameter is correct for this usage.")]
        protected bool IsItemValidForLine(DiscountableItemGroup discountableItemGroup, RetailDiscountLine discountLine)
        {
            bool result = false;

            if (discountableItemGroup == null || discountLine == null)
            {
                return result;
            }

            IList<RetailDiscountLine> retailDiscountLines;

            if (this.TryGetRetailDiscountLines(discountableItemGroup.ProductId, discountableItemGroup.MasterProductId, discountableItemGroup.SalesOrderUnitOfMeasure, out retailDiscountLines))
            {
                result = retailDiscountLines.Where(p => p.DiscountLineNumber == discountLine.DiscountLineNumber).Any();
            }

            return result;
        }

        /// <summary>
        /// Determines if the specified item is valid for the specified discount line.
        /// </summary>
        /// <param name="discountableItemGroup">The item to consider.</param>
        /// <param name="discountLines">The discount lines to consider.</param>
        /// <returns>True if the item is valid, false otherwise.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Parameter is correct for this usage.")]
        protected RetailDiscountLine GetFirstMatchingLineForItem(DiscountableItemGroup discountableItemGroup, IEnumerable<RetailDiscountLine> discountLines)
        {
            if (discountLines == null)
            {
                return null;
            }

            foreach (RetailDiscountLine discountLine in discountLines)
            {
                if (this.IsItemValidForLine(discountableItemGroup, discountLine))
                {
                    return discountLine;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the sort index to use for a discount application using the specified discount line.
        /// </summary>
        /// <param name="line">The discount line to determine the sort index on.</param>
        /// <returns>The sort index to use.</returns>
        protected virtual int GetSortIndexForRetailDiscountLine(RetailDiscountLine line)
        {
            if (line == null)
            {
                return InvalidIndex;
            }

            // The discount offer type enum has the values in the proper order, discount method type does not.
            switch (this.DiscountType)
            {
                case DiscountMethodType.LeastExpensive:
                    return (int)DiscountOfferMethod.DiscountPercent;
                case DiscountMethodType.DealPrice:
                case DiscountMethodType.MultiplyDealPrice:
                    return (int)DiscountOfferMethod.OfferPrice;
                case DiscountMethodType.DiscountAmount:
                    return (int)DiscountOfferMethod.DiscountAmount;
                case DiscountMethodType.DiscountPercent:
                case DiscountMethodType.MultiplyDiscountPercent:
                    return (int)DiscountOfferMethod.DiscountPercent;
                case DiscountMethodType.LineSpecific:
                    return line.DiscountMethod;
                default:
                    return -1;
            }
        }

        /// <summary>
        /// Determines if this discount can possibly apply to the specified transaction by examining all of the triggering rules not related to the actual line items on the transaction.
        /// </summary>
        /// <param name="transaction">The transaction to use for checking the triggering rules.</param>
        /// <param name="storePriceGroups">The collection of price groups that the store belongs to.</param>
        /// <param name="currencyCode">The currency code for the current transaction.</param>
        /// <param name="pricingDataManager">The IPricingDataManager instance to use for this method.</param>
        /// <param name="isReturn">The flag indicating whether or not it's for return.</param>
        /// <param name="priceContext">Price context object.</param>
        /// <returns>True if the discount could apply if the correct line items exist, false otherwise.</returns>
        protected bool CanDiscountApply(SalesTransaction transaction, IEnumerable<long> storePriceGroups, string currencyCode, IPricingDataManagerV2 pricingDataManager, bool isReturn, PriceContext priceContext)
        {
            bool canApply = false;

            if (transaction == null || storePriceGroups == null)
            {
                return canApply;
            }

            // Check to see if a discount code is required first.
            if (this.IsDiscountCodeRequired && isReturn == false)
            {
                foreach (var discountCode in transaction.DiscountCodes)
                {
                    if (this.DiscountCodes.Contains(discountCode))
                    {
                        canApply = true;
                        break;
                    }
                }
            }
            else
            {
                canApply = true;
            }

            if (!canApply)
            {
                return false;
            }

            // Now examine the price/discount groups
            canApply = false;

            foreach (var priceDiscountGroup in storePriceGroups)
            {
                if (this.PriceDiscountGroupIds.Contains(priceDiscountGroup))
                {
                    canApply = true;
                    break;
                }
            }

            if (!canApply)
            {
                return false;
            }

            // Check the valid dates and validation type
            // User active date from price context object
            canApply = false;

            if ((this.ValidFrom.Date <= priceContext.ActiveDate.Date || this.ValidFrom.Date <= InternalValidationPeriod.NoDate)
                && (this.ValidTo.Date >= priceContext.ActiveDate.Date || this.ValidTo.Date <= InternalValidationPeriod.NoDate))
            {
                canApply = PricingEngine.IsPromoPeriodValid(pricingDataManager, this.DateValidationType, this.DateValidationPeriodId, this.ValidFrom, this.ValidTo, priceContext.ActiveDate);
            }

            if (!canApply)
            {
                return false;
            }

            // Check the currency code
            canApply = false;
            if (string.IsNullOrWhiteSpace(this.CurrencyCode) || this.CurrencyCode.Equals(currencyCode, StringComparison.OrdinalIgnoreCase))
            {
                canApply = true;
            }

            return canApply;
        }

        /// <summary>
        /// Gets the (first) discount code from the transaction that triggered the discount.
        /// </summary>
        /// <param name="transaction">The transaction that the discount will be applied to.</param>
        /// <returns>The first matching discount code from the transaction that is contained in the collection of required discount codes for this discount.</returns>
        protected string GetDiscountCodeForDiscount(SalesTransaction transaction)
        {
            string discountCodeUsed = string.Empty;

            if (transaction != null && transaction.DiscountCodes != null && this.IsDiscountCodeRequired)
            {
                foreach (var discountCode in transaction.DiscountCodes)
                {
                    if (this.DiscountCodes.Contains(discountCode))
                    {
                        discountCodeUsed = discountCode;
                        break;
                    }
                }
            }

            return discountCodeUsed;
        }

        /// <summary>
        /// Initializes the dictionary of previously applied discounts for calculating threshold discount amounts.
        /// </summary>
        /// <param name="appliedDiscounts">The previously applied discounts.</param>
        /// <param name="discountDictionary">The dictionary to hold discounts applied to each relevant line.</param>
        /// <param name="countThreshold">A flag indicating whether to count threshold discounts.</param>
        /// <returns>True if the dictionary has been initialized, false otherwise.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Parameter is correct for this usage.")]
        protected bool InitializeDiscountDictionary(Stack<AppliedDiscountApplication> appliedDiscounts, Dictionary<int, IList<DiscountLineQuantity>> discountDictionary, bool countThreshold)
        {
            if (discountDictionary == null)
            {
                return false;
            }

            var discountApplications = appliedDiscounts.Where(p => p.DiscountApplication.Discount.ConcurrencyMode == ConcurrencyMode.Compounded && p.DiscountApplication.Discount.OfferId != this.OfferId).Reverse();

            foreach (var app in discountApplications)
            {
                if (!countThreshold && app.DiscountApplication.Discount is ThresholdDiscount)
                {
                    continue;
                }

                var discountLinesInApplication = app.DiscountLinesDictionary;

                foreach (int key in discountLinesInApplication.Keys)
                {
                    IList<DiscountLineQuantity> discountLines;
                    if (!discountDictionary.TryGetValue(key, out discountLines))
                    {
                        discountLines = new List<DiscountLineQuantity>();
                        discountDictionary.Add(key, discountLines);
                    }

                    discountLines.AddRange(discountLinesInApplication[key]);
                }
            }

            return true;
        }

        /// <summary>
        /// Create a new discount line.
        /// </summary>
        /// <param name="discountCode">Discount code.</param>
        /// <param name="itemId">Item identifier.</param>
        /// <returns>A new discount line.</returns>
        protected DiscountLine NewDiscountLine(string discountCode, string itemId)
        {
            DiscountLine discountItem = new DiscountLine()
            {
                DiscountLineType = DiscountLineType.PeriodicDiscount,
                PeriodicDiscountType = this.PeriodicDiscountType,
                OfferId = this.OfferId,
                OfferName = this.OfferName,
                DiscountCode = discountCode,
                ConcurrencyMode = this.ConcurrencyMode,
                IsCompoundable = this.ConcurrencyMode == ConcurrencyMode.Compounded,
                DiscountApplicationGroup = itemId,
            };

            return discountItem;
        }

        /// <summary>
        /// Get existing discount dictionary and discounted prices, related to currency discount application.
        /// </summary>
        /// <param name="discountableItemGroups">Discountable item groups.</param>
        /// <param name="remainingQuantities">Remaining quantities.</param>
        /// <param name="appliedDiscounts">Applied discounts.</param>
        /// <param name="discountApplication">Currency discount application.</param>
        /// <param name="discountedPrices">Discounted prices.</param>
        /// <returns>Dictionary of existing discount lines, indexed by item index.</returns>
        protected Dictionary<int, IList<DiscountLineQuantity>> GetExistingDiscountDictionaryAndDiscountedPrices(
            DiscountableItemGroup[] discountableItemGroups,
            decimal[] remainingQuantities,
            Stack<AppliedDiscountApplication> appliedDiscounts,
            DiscountApplication discountApplication,
            out decimal[] discountedPrices)
        {
            if (discountableItemGroups == null)
            {
                throw new ArgumentNullException("discountableItemGroups");
            }

            if (remainingQuantities == null)
            {
                throw new ArgumentNullException("remainingQuantities");
            }

            if (discountApplication == null)
            {
                throw new ArgumentNullException("discountApplication");
            }

            discountedPrices = new decimal[discountableItemGroups.Length];
            Dictionary<int, IList<DiscountLineQuantity>> discountDictionary = new Dictionary<int, IList<DiscountLineQuantity>>();

            if (this.ConcurrencyMode == DataModel.ConcurrencyMode.Compounded)
            {
                bool discountDictionaryInitialized = false;
                foreach (int x in discountApplication.RetailDiscountLines.Select(p => p.ItemIndex).Distinct())
                {
                    discountedPrices[x] = discountableItemGroups[x].Price;

                    if (this.ConcurrencyMode == ConcurrencyMode.Compounded && this.DiscountType != DiscountMethodType.DiscountAmount)
                    {
                        if (!discountDictionaryInitialized)
                        {
                            discountDictionaryInitialized = this.InitializeDiscountDictionary(appliedDiscounts, discountDictionary, true);
                        }

                        // Determine the value of the previously applied concurrent discounts so that we can get the proper value here.
                        // We group them together here to get the correct amount when there is a quantity greater than one and a percentage discount.
                        if (discountDictionary.ContainsKey(x))
                        {
                            List<List<DiscountLineQuantity>> sortedConcurrentDiscounts =
                                discountDictionary[x].GroupBy(p => p.DiscountLine.OfferId)
                                                     .OrderByDescending(p => p.First().DiscountLine.DealPrice)
                                                     .ThenByDescending(p => p.First().DiscountLine.Amount)
                                                     .Select(concurrentDiscountGroup => concurrentDiscountGroup.ToList())
                                                     .ToList();

                            foreach (List<DiscountLineQuantity> concurrentDiscount in sortedConcurrentDiscounts)
                            {
                                decimal startingPrice = discountedPrices[x];

                                foreach (DiscountLineQuantity discLine in concurrentDiscount)
                                {
                                    if (remainingQuantities[x] != 0m)
                                    {
                                        discountedPrices[x] -= (((startingPrice * discLine.DiscountLine.Percentage / 100M) + discLine.DiscountLine.Amount) * discLine.Quantity) / Math.Abs(remainingQuantities[x]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                discountDictionary = new Dictionary<int, IList<DiscountLineQuantity>>();
                discountedPrices = new decimal[discountableItemGroups.Length];
                foreach (int x in discountApplication.RetailDiscountLines.Select(p => p.ItemIndex).Distinct())
                {
                    discountedPrices[x] = discountableItemGroups[x].Price;
                }
            }

            return discountDictionary;
        }

        /// <summary>
        /// Adds the specified product or variant identifier and discount line to the dictionary of lines available for the product or variant.
        /// </summary>
        /// <param name="productOrVariantId">The product or variant identifier.</param>
        /// <param name="line">The RetailDiscountLine object that the product or variant can apply to.</param>
        private void AddDiscountLineToDictionary(long productOrVariantId, RetailDiscountLine line)
        {
            if (this.ProductOfVariantToDiscountLinesMap.ContainsKey(productOrVariantId))
            {
                this.ProductOfVariantToDiscountLinesMap[productOrVariantId].Add(line);
            }
            else
            {
                this.ProductOfVariantToDiscountLinesMap.Add(productOrVariantId, new List<RetailDiscountLine>() { line });
            }
        }
    }
}
