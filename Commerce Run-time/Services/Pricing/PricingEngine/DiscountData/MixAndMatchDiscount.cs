﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine.DiscountData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// This class implements the mix and match discount processing, including the determination of which ways 
    /// the discount can apply to the transaction and the value of the discount applied to specific lines.
    /// </summary>
    public class MixAndMatchDiscount : DiscountBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MixAndMatchDiscount" /> class.
        /// </summary>
        public MixAndMatchDiscount()
        {
            this.LineGroupToNumberOfItemsMap = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the map of mix and match line group to number of items.
        /// </summary>
        public IDictionary<string, int> LineGroupToNumberOfItemsMap { get; private set; }

        /// <summary>
        /// Gets all of the possible applications of this discount to the specified transaction and line items.
        /// </summary>
        /// <param name="transaction">The transaction to consider for discounts.</param>
        /// <param name="discountableItemGroups">The valid sales line items on the transaction to consider.</param>
        /// <param name="remainingQuantities">The remaining quantities of each of the sales lines to consider.</param>
        /// <param name="storePriceGroups">The collection of price groups that this store belongs to.</param>
        /// <param name="currencyCode">The currency code in use for the current transaction.</param>
        /// <param name="pricingDataManager">The IPricingDataManager instance to use.</param>
        /// <param name="isReturn">The flag indicating whether or not it's for return.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <returns>The possible permutations of line items that this discount can apply to, or an empty collection if this discount cannot apply.</returns>
        public override IEnumerable<DiscountApplication> GetDiscountApplications(
            SalesTransaction transaction,
            DiscountableItemGroup[] discountableItemGroups,
            decimal[] remainingQuantities,
            IEnumerable<long> storePriceGroups, 
            string currencyCode,
            IPricingDataManagerV2 pricingDataManager,
            bool isReturn,
            PriceContext priceContext)
        {
            if (priceContext == null)
            {
                throw new ArgumentNullException("priceContext");
            }

            List<DiscountApplication> discountApplications = new List<DiscountApplication>();

            // If this discount cannot be applied, return an empty collection.
            if (!this.CanDiscountApply(transaction, storePriceGroups, currencyCode, pricingDataManager, isReturn, priceContext)
                || discountableItemGroups == null
                || remainingQuantities == null)
            {
                return discountApplications;
            }

            // Get the discount code to use for any discount lines, if one is required.
            string discountCodeUsed = this.GetDiscountCodeForDiscount(transaction);

            // Otherwise, find each possible permutation.
            IDictionary<long, IList<RetailDiscountLine>> triggeringProducts = this.GetProductOrVariantIdToRetailDiscountLinesMap();

            HashSet<RetailDiscountLine> triggeredDiscountLines = new HashSet<RetailDiscountLine>();

            foreach (long key in triggeringProducts.Keys)
            {
                foreach (RetailDiscountLine line in triggeringProducts[key])
                {
                    triggeredDiscountLines.Add(line);
                }
            }

            HashSet<string> discountPriceGroups = new HashSet<string>(ConvertPriceDiscountGroupIdsToGroups(this.PriceDiscountGroupIds, priceContext));

            // Create a BitSet that can hold the valid items for both positive and negative quantities
            BitSet availableItemGroups = new BitSet((uint)discountableItemGroups.Length);

            for (int x = 0; x < discountableItemGroups.Length; x++)
            {
                DiscountableItemGroup discountableItemGroup = discountableItemGroups[x];

                if (remainingQuantities[x] != decimal.Zero
                    && DiscountBase.IsDiscountAllowedForDiscountableItemGroup(discountableItemGroup)
                    && DiscountBase.IsDiscountAllowedForCatalogIds(priceContext, discountPriceGroups, discountableItemGroup.CatalogIds)
                    && this.ContainsProductForRetailDiscountLines(discountableItemGroup.ProductId, discountableItemGroup.MasterProductId, discountableItemGroup.SalesOrderUnitOfMeasure))
                {
                    availableItemGroups[x] = true;
                }
            }

            // Now we have the different groups that could get a discount from this, as well as the discount lines that can apply.
            discountApplications.AddRange(this.GetDiscountApplicationsForItems(availableItemGroups, triggeredDiscountLines, discountableItemGroups, remainingQuantities, priceContext.IsTaxInclusive, discountCodeUsed));

            return discountApplications;
        }

        /// <summary>
        /// Applies the discount application and gets the value, taking into account previously applied discounts.
        /// </summary>
        /// <param name="discountableItemGroups">The transaction line items.</param>
        /// <param name="remainingQuantities">The quantities remaining for each item.</param>
        /// <param name="appliedDiscounts">The previously applied discounts.</param>
        /// <param name="discountApplication">The specific application of the discount to use.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <param name="hasCompetingApplications">A value indicating whether it has competing applications.</param>
        /// <returns>The value of the discount application.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Parameter is correct for this usage.")]
        public override AppliedDiscountApplication GetAppliedDiscountApplication(DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, Stack<AppliedDiscountApplication> appliedDiscounts, DiscountApplication discountApplication, PriceContext priceContext, bool hasCompetingApplications)
        {
            if (discountApplication == null || discountableItemGroups == null)
            {
                return null;
            }

            decimal[] prices;
            Dictionary<int, IList<DiscountLineQuantity>> discountDictionary = this.GetExistingDiscountDictionaryAndDiscountedPrices(discountableItemGroups, remainingQuantities, appliedDiscounts, discountApplication, out prices);

            decimal result = this.GetValueFromDiscountApplicationAndPrices(discountApplication, prices, discountDictionary);

            AppliedDiscountApplication currentDiscountApplication = null;

            if (result > decimal.Zero)
            {
                // Only generate the discount lines dictionary now if it's compounded.
                bool isDiscountLineGenerated = this.ConcurrencyMode == DataModel.ConcurrencyMode.Compounded;
                currentDiscountApplication = new AppliedDiscountApplication(discountApplication, result, (decimal[])discountApplication.ItemQuantities.Clone(), isDiscountLineGenerated);
                if (isDiscountLineGenerated)
                {
                    currentDiscountApplication.DiscountLinesDictionary = this.GenerateDiscountLinesDictionary(discountableItemGroups, remainingQuantities, currentDiscountApplication, priceContext);
                }
            }

            return currentDiscountApplication;
        }

        /// <summary>
        /// Generate discount lines for the applied discount application.
        /// </summary>
        /// <param name="appliedDiscountApplication">The applied discount application.</param>
        /// <param name="discountableItemGroups">The discountable item groups.</param>
        /// <param name="priceContext">The price context.</param>
        public override void GenerateDiscountLines(AppliedDiscountApplication appliedDiscountApplication, DiscountableItemGroup[] discountableItemGroups, PriceContext priceContext)
        {
            ThrowIf.Null(appliedDiscountApplication, "appliedDiscountApplication");
            ThrowIf.Null(discountableItemGroups, "discountableItemGroups");
            ThrowIf.Null(priceContext, "priceContext");

            appliedDiscountApplication.DiscountLinesDictionary = this.GenerateDiscountLinesDictionary(discountableItemGroups, appliedDiscountApplication.ItemQuantities, appliedDiscountApplication, priceContext);
        }

        /// <summary>
        /// Translates a line discount method from <see cref="DiscountOfferMethod"/> to <see cref="DiscountMethodType"/>.
        /// </summary>
        /// <param name="lineDiscountMethod">The value of the discount method.</param>
        /// <returns>The corresponding discount method enumeration value.</returns>
        private static DiscountMethodType TranslateLineDiscountMethod(int lineDiscountMethod)
        {
            switch (lineDiscountMethod)
            {
                case (int)DiscountOfferMethod.DiscountAmount:
                    return DiscountMethodType.DiscountAmount;
                case (int)DiscountOfferMethod.DiscountPercent:
                    return DiscountMethodType.DiscountPercent;
                case (int)DiscountOfferMethod.OfferPrice:
                case (int)DiscountOfferMethod.OfferPriceIncludingTax:
                    return DiscountMethodType.DealPrice;
                default:
                    return DiscountMethodType.DiscountPercent;
            }
        }

        /// <summary>
        /// Generate discount lines dictionary from the applied discount application.
        /// </summary>
        /// <param name="discountableItemGroups">The transaction line items.</param>
        /// <param name="remainingQuantities">The quantities remaining for each item.</param>
        /// <param name="discountApplicationToApply">The specific application of the discount to use.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <returns>A dictionary containing a list of DiscountLine objects for each index of discountableItemGroups that has at least one discount.</returns>
        private IDictionary<int, IList<DiscountLineQuantity>> GenerateDiscountLinesDictionary(
            DiscountableItemGroup[] discountableItemGroups,
            decimal[] remainingQuantities,
            AppliedDiscountApplication discountApplicationToApply,
            PriceContext priceContext)
        {
            IDictionary<int, IList<DiscountLineQuantity>> result = new Dictionary<int, IList<DiscountLineQuantity>>();

            if (discountApplicationToApply == null || discountableItemGroups == null || remainingQuantities == null)
            {
                return result;
            }

            // Keep track of which lines have been considered in the amount so that we don't double-count them.
            List<int> linesWithDiscount = new List<int>();

            decimal totalPrice = decimal.Zero;
            decimal discountAmount = this.DiscountAmountValue;
            decimal dealPrice = this.DealPriceValue;

            if (this.DealPriceValue != decimal.Zero || this.DiscountAmountValue != decimal.Zero)
            {
                for (int x = 0; x < discountApplicationToApply.DiscountApplication.ItemQuantities.Length; x++)
                {
                    totalPrice += discountableItemGroups[x].Price * discountApplicationToApply.DiscountApplication.ItemQuantities[x];
                }
            }

            foreach (var group in discountApplicationToApply.DiscountApplication.RetailDiscountLines.GroupBy(p => p.RetailDiscountLine.MixAndMatchLineGroup))
            {
                this.GetDiscountLinesToApplyForGroup(discountableItemGroups, discountApplicationToApply.DiscountApplication, priceContext, result, linesWithDiscount, ref totalPrice, ref discountAmount, ref dealPrice, group);
            }

            return result;
        }

        /// <summary>
        /// Arranges the items into mix-and-match line groups to determine the possible combinations of lines that are valid for this discount.
        /// </summary>
        /// <param name="availableItemGroups">The available item groups.</param>
        /// <param name="triggeredDiscountLines">The currently triggered discount lines.</param>
        /// <param name="discountableItemGroups">The item groups on the transaction.</param>
        /// <param name="isTaxIncludedInPrice">Flag indicating whether taxes are included in pricing.</param>
        /// <param name="currentIndex">The current index in the available items.</param>
        /// <param name="qtyRemaining">The remaining quantities of items after other groups have been used.</param>
        /// <param name="matchedLineGroups">The dictionary containing matching sets of items for each mix-and-match line group.</param>
        private void ArrangeLineGroups(BitSet availableItemGroups, HashSet<RetailDiscountLine> triggeredDiscountLines, DiscountableItemGroup[] discountableItemGroups, bool isTaxIncludedInPrice, int currentIndex, decimal[] qtyRemaining, Dictionary<string, List<List<RetailDiscountLineItem>>> matchedLineGroups)
        {
            Stack<int> itemsUsed = new Stack<int>();
            Stack<RetailDiscountLine> linesUsed = new Stack<RetailDiscountLine>();

            foreach (string lineGroup in this.LineGroupToNumberOfItemsMap.Keys)
            {
                List<RetailDiscountLine> groupLines = triggeredDiscountLines.Where(p => string.Equals(p.MixAndMatchLineGroup, lineGroup, StringComparison.OrdinalIgnoreCase)).ToList();
                if (groupLines.Count == 0)
                {
                    matchedLineGroups.Clear();
                    return;
                }

                RetailDiscountLine line = groupLines[0];

                if (!(line.DiscountMethod == (int)DiscountOfferMethod.OfferPriceIncludingTax && !isTaxIncludedInPrice)
                    && !(line.DiscountMethod == (int)DiscountOfferMethod.OfferPrice && isTaxIncludedInPrice))
                {
                    while (true)
                    {
                        currentIndex = availableItemGroups.GetNextNonZeroBit(currentIndex);

                        if (currentIndex == BitSet.UnknownBit || itemsUsed.Count >= line.MixAndMatchLineNumberOfItemsNeeded)
                        {
                            if (itemsUsed.Count > 0)
                            {
                                // No next non-zero bit exists, move to the next item from the previous parent
                                int lastItem = itemsUsed.Pop();
                                linesUsed.Pop();
                                qtyRemaining[lastItem] += 1;
                                currentIndex = lastItem + 1;
                            }
                            else
                            {
                                // No next non-zero bit exists and we are at the top level, exit the loop.
                                currentIndex = 0;
                                break;
                            }
                        }
                        else
                        {
                            RetailDiscountLine matchedLine = this.GetFirstMatchingLineForItem(discountableItemGroups[currentIndex], groupLines);
                            if (matchedLine != null && qtyRemaining[currentIndex] >= 1M)
                            {
                                itemsUsed.Push(currentIndex);
                                linesUsed.Push(matchedLine);
                                qtyRemaining[currentIndex] -= 1;

                                // Check to see if we have all of the required items for this group.
                                if (itemsUsed.Count == line.MixAndMatchLineNumberOfItemsNeeded)
                                {
                                    List<RetailDiscountLineItem> groupMatch = new List<RetailDiscountLineItem>(itemsUsed.Count);
                                    int[] matchedItems = itemsUsed.ToArray();
                                    RetailDiscountLine[] matchedLines = linesUsed.ToArray();

                                    for (int x = 0; x < matchedItems.Length; x++)
                                    {
                                        groupMatch.Add(new RetailDiscountLineItem(matchedItems[x], matchedLines[x]));
                                    }

                                    // Now that we have a valid number of lines, save off this path with the matched line in the matched groups.
                                    if (matchedLineGroups.ContainsKey(line.MixAndMatchLineGroup))
                                    {
                                        matchedLineGroups[line.MixAndMatchLineGroup].Add(groupMatch);
                                    }
                                    else
                                    {
                                        matchedLineGroups.Add(line.MixAndMatchLineGroup, new List<List<RetailDiscountLineItem>>(1) { groupMatch });
                                    }
                                }
                            }
                            else
                            {
                                // Not enough items left in this index or this item is not in the correct discount line, move to the next one.
                                currentIndex++;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Determines the sort value to use for a specific discount application.  This value is an approximation of the amount or percentage that this discount application will use.
        /// </summary>
        /// <param name="discountLineItems">The items used by this application.</param>
        /// <param name="discountableItemGroups">The item groups on the transaction.</param>
        /// <returns>The sort value to use.</returns>
        private decimal GetSortValue(IEnumerable<RetailDiscountLineItem> discountLineItems, DiscountableItemGroup[] discountableItemGroups)
        {
            decimal result = decimal.Zero;

            if (this.DiscountType == DiscountMethodType.DiscountAmount)
            {
                return this.DiscountAmountValue;
            }

            if (this.DiscountType == DiscountMethodType.LeastExpensive)
            {
                if (this.DiscountPercentValue != decimal.Zero)
                {
                    return this.DiscountPercentValue;
                }
                else if (this.DiscountAmountValue != decimal.Zero)
                {
                    return this.DiscountAmountValue;
                }
            }

            decimal totalPrice = decimal.Zero;

            foreach (RetailDiscountLineItem item in discountLineItems)
            {
                totalPrice += discountableItemGroups[item.ItemIndex].Price;
            }

            foreach (RetailDiscountLineItem item in discountLineItems)
            {
                decimal price = discountableItemGroups[item.ItemIndex].Price;
                decimal taxRatePercentage = discountableItemGroups[item.ItemIndex].TaxRatePercent;

                switch ((DiscountOfferMethod)item.RetailDiscountLine.DiscountMethod)
                {
                    case DiscountOfferMethod.DiscountAmount:
                        result += item.RetailDiscountLine.DiscountAmount;
                        break;
                    case DiscountOfferMethod.DiscountPercent:
                        result += price * (this.DiscountPercentValue + item.RetailDiscountLine.DiscountPercent + item.RetailDiscountLine.DiscountLinePercentOrValue) / 100M;
                        break;
                    case DiscountOfferMethod.OfferPrice:
                    case DiscountOfferMethod.OfferPriceIncludingTax:
                        result += (price * (1.0M + (taxRatePercentage / 100.0M))) - (item.RetailDiscountLine.DiscountLinePercentOrValue * price / totalPrice);
                        break;
                    default:
                        result += 0;
                        break;
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the possible mix-and-match discount applications for the item groups on the transaction.
        /// </summary>
        /// <param name="availableItemGroups">The available item groups to consider.</param>
        /// <param name="triggeredDiscountLines">The collection of discount lines triggered by the item groups.</param>
        /// <param name="discountableItemGroups">The item groups on the transaction.</param>
        /// <param name="remainingQuantities">The remaining quantities of the item groups.</param>
        /// <param name="isTaxIncludedInPrice">Flag indicating whether taxes are included in pricing.</param>
        /// <param name="discountCodeUsed">The discount code used for triggering this discount.</param>
        /// <returns>The collection of discount applications for this discount on the transaction.</returns>
        private IEnumerable<DiscountApplication> GetDiscountApplicationsForItems(BitSet availableItemGroups, HashSet<RetailDiscountLine> triggeredDiscountLines, DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, bool isTaxIncludedInPrice, string discountCodeUsed)
        {
            List<DiscountApplication> result = new List<DiscountApplication>();

            Dictionary<string, List<List<RetailDiscountLineItem>>> matchedLineGroups = new Dictionary<string, List<List<RetailDiscountLineItem>>>(StringComparer.OrdinalIgnoreCase);

            if (availableItemGroups.IsZero())
            {
                return result;
            }

            int currentIndex = 0;

            decimal[] qtyRemaining = (decimal[])remainingQuantities.Clone();

            // Loop through all of the triggered discount lines to arrange them into line groups
            this.ArrangeLineGroups(availableItemGroups, triggeredDiscountLines, discountableItemGroups, isTaxIncludedInPrice, currentIndex, qtyRemaining, matchedLineGroups);

            // Now that we have all of the possibilities for each group, add all of the possible combinations of groups matches to the result.
            // Check the original lines to make sure that we get all of the required groups.
            List<string> requiredGroups = this.LineGroupToNumberOfItemsMap.Keys.ToList();

            // Since we don't know how many groups we will have, we use a pair of stacks here to do a depth-first search on the different 
            // permutations of line groups, where the first stack contains the index into the groups and the second stack contains the selected group.
            Stack<int> groupIndices = new Stack<int>(requiredGroups.Count);
            Stack<List<RetailDiscountLineItem>> groupMember = new Stack<List<RetailDiscountLineItem>>(requiredGroups.Count);

            // If any required group doesn't exist in the matches, then we won't add any results, just return the empty list of matches.
            if (matchedLineGroups.Count == requiredGroups.Count)
            {
                // Initialize the stacks by pushing the first element from each group up to the last one.
                // Then we will do the foreach, then iterate on each available option.
                for (int x = 0; x < requiredGroups.Count - 1; x++)
                {
                    groupIndices.Push(0);
                    groupMember.Push(matchedLineGroups[requiredGroups[x]][0]);
                }

                while (true)
                {
                    decimal[] itemQuantities = new decimal[remainingQuantities.Length];

                    foreach (List<RetailDiscountLineItem> groupMatch in groupMember)
                    {
                        foreach (RetailDiscountLineItem item in groupMatch)
                        {
                            itemQuantities[item.ItemIndex] += 1;
                        }
                    }

                    // Verify that there are enough remaining items to use this amount (before looking at the last group).
                    if (DiscountBase.IsDecimalArrayASubset(itemQuantities, remainingQuantities))
                    {
                        // Save off a discount line for each member in the last required group
                        foreach (List<RetailDiscountLineItem> lastGroupLine in matchedLineGroups[requiredGroups[groupIndices.Count]])
                        {
                            decimal[] lastGroupQuantities = (decimal[])itemQuantities.Clone();

                            foreach (RetailDiscountLineItem item in lastGroupLine)
                            {
                                lastGroupQuantities[item.ItemIndex] += 1;
                            }

                            // Verify that there are enough remaining items to use this amount (including the last group).
                            if (DiscountBase.IsDecimalArrayASubset(lastGroupQuantities, remainingQuantities))
                            {
                                // Add discount line, using the groupMember stack contents plus this line.
                                groupMember.Push(lastGroupLine);

                                List<RetailDiscountLineItem> discountLineItems = groupMember.SelectMany(p => p).ToList();

                                DiscountApplication app = new DiscountApplication(this)
                                {
                                    RetailDiscountLines = discountLineItems,
                                    SortIndex = discountLineItems.Select(p => p.RetailDiscountLine).Distinct().Max(p => this.GetSortIndexForRetailDiscountLine(p)),
                                    SortValue = this.GetSortValue(discountLineItems, discountableItemGroups),
                                    ItemQuantities = lastGroupQuantities,
                                    DiscountCode = discountCodeUsed
                                };

                                if (this.DiscountType == DiscountMethodType.LeastExpensive)
                                {
                                    app.DealPriceValue = this.DealPriceValue;
                                    app.DiscountAmountValue = this.DiscountAmountValue;
                                    app.DiscountPercentValue = this.DiscountPercentValue;
                                }

                                result.Add(app);

                                groupMember.Pop();
                            }
                        }
                    }

                    // This would only happen if there is only a single group required.
                    if (groupIndices.Count == 0)
                    {
                        break;
                    }
                    else
                    {
                        currentIndex = groupIndices.Pop() + 1;
                        groupMember.Pop();

                        while (matchedLineGroups[requiredGroups[groupIndices.Count]].Count <= currentIndex && groupIndices.Count != 0)
                        {
                            currentIndex = groupIndices.Pop() + 1;
                            groupMember.Pop();
                        }

                        // Check to see if we have already examined every group
                        if (groupIndices.Count == 0 && matchedLineGroups[requiredGroups[0]].Count <= currentIndex)
                        {
                            break;
                        }
                        else
                        {
                            groupIndices.Push(currentIndex);
                            groupMember.Push(matchedLineGroups[requiredGroups[groupIndices.Count - 1]][currentIndex]);

                            // Fill in the rest of the stack with the left-hand nodes of the tree
                            for (int x = groupIndices.Count; x < requiredGroups.Count - 1; x++)
                            {
                                groupIndices.Push(0);
                                groupMember.Push(matchedLineGroups[requiredGroups[x]][0]);
                            }
                        }
                    }
                }
            }

            return result;
        }

        private void GetDiscountLinesToApplyForGroup(
            DiscountableItemGroup[] discountableItemGroups,
            DiscountApplication discountApplication, 
            PriceContext priceContext,
            IDictionary<int, IList<DiscountLineQuantity>> result,
            List<int> linesWithDiscount, 
            ref decimal totalPrice,
            ref decimal discountAmount,
            ref decimal dealPrice,
            IGrouping<string, RetailDiscountLineItem> group)
        {
            RetailDiscountLine firstDiscountLine = group.Select(p => p.RetailDiscountLine).First();

            // Line specific deal price. Note it's not firstDiscountLine.OfferPrice.
            if (this.DiscountType == DiscountMethodType.LineSpecific && (firstDiscountLine.DiscountMethod == (int)DiscountOfferMethod.OfferPrice || firstDiscountLine.DiscountMethod == (int)DiscountOfferMethod.OfferPriceIncludingTax))
            {
                totalPrice = group.Sum(p => discountableItemGroups[p.ItemIndex].Price);
                dealPrice = firstDiscountLine.DiscountLinePercentOrValue;
            }

            foreach (var item in group)
            {
                DiscountLine discountItem = this.NewDiscountLine(discountApplication.DiscountCode, discountableItemGroups[item.ItemIndex].ItemId);

                bool discountApplied = true;

                if (this.DiscountType == DiscountMethodType.LeastExpensive)
                {
                    discountApplied = this.GetDiscountLinesForRetailDiscountLineItemForLeastExpensive(discountableItemGroups, discountApplication, linesWithDiscount, item, discountItem);
                }
                else
                {
                    this.GetDiscountLinesForRetailDiscountLineItem(discountableItemGroups, priceContext, ref totalPrice, ref discountAmount, ref dealPrice, item, discountItem);
                }

                if (discountApplied)
                {
                    linesWithDiscount.Add(item.ItemIndex);

                    if (result.ContainsKey(item.ItemIndex))
                    {
                        result[item.ItemIndex].Add(new DiscountLineQuantity(discountItem, 1m));
                    }
                    else
                    {
                        result.Add(item.ItemIndex, new List<DiscountLineQuantity>() { new DiscountLineQuantity(discountItem, 1m) });
                    }
                }
            }
        }

        private void GetDiscountLinesForRetailDiscountLineItem(
            DiscountableItemGroup[] discountableItemGroups,
            PriceContext priceContext, 
            ref decimal totalPrice, 
            ref decimal discountAmount,
            ref decimal dealPrice, 
            RetailDiscountLineItem item,
            DiscountLine discountItem)
        {
            RetailDiscountLine discountLine = item.RetailDiscountLine;

            // Handle scaling and rounding loss for discount on multiple lines.
            decimal thisDiscountAmount = decimal.Zero;

            if (this.DiscountType == DiscountMethodType.DiscountAmount && discountAmount > decimal.Zero)
            {
                // $ off is distributed proportionally to all items that make the mix and match.
                discountItem.Percentage = 0;
                thisDiscountAmount = totalPrice != decimal.Zero ? priceContext.Rounding(discountAmount * discountableItemGroups[item.ItemIndex].Price / totalPrice) : decimal.Zero;

                totalPrice -= discountableItemGroups[item.ItemIndex].Price;
                discountAmount -= thisDiscountAmount;
                discountItem.Amount = thisDiscountAmount;
            }
            else if (this.DiscountType == DiscountMethodType.DiscountPercent && this.DiscountPercentValue > decimal.Zero)
            {
                // % off
                discountItem.Percentage = Math.Min(100m, this.DiscountPercentValue);
                discountItem.Amount = 0m;
            }
            else if (this.DiscountType == DiscountMethodType.LineSpecific && discountLine.DiscountMethod == (int)DiscountOfferMethod.DiscountPercent && discountLine.DiscountLinePercentOrValue > decimal.Zero)
            {
                // Line specific % off
                discountItem.Percentage = Math.Min(100m, discountLine.DiscountLinePercentOrValue);
                discountItem.Amount = 0m;
            }
            else if ((this.DiscountType == DiscountMethodType.DealPrice || discountLine.DiscountMethod == (int)DiscountOfferMethod.OfferPrice || discountLine.DiscountMethod == (int)DiscountOfferMethod.OfferPriceIncludingTax) && dealPrice != decimal.Zero)
            {
                // Deal price or line specific deal price
                discountItem.Percentage = 0m;
                thisDiscountAmount = discountableItemGroups[item.ItemIndex].Price - (totalPrice != decimal.Zero ? priceContext.Rounding(dealPrice * (discountableItemGroups[item.ItemIndex].Price / totalPrice)) : decimal.Zero);
                thisDiscountAmount = Math.Max(thisDiscountAmount, 0);
                totalPrice -= discountableItemGroups[item.ItemIndex].Price;
                dealPrice -= discountableItemGroups[item.ItemIndex].Price - thisDiscountAmount;

                discountItem.Amount = thisDiscountAmount;
                discountItem.DealPrice = discountableItemGroups[item.ItemIndex].Price - thisDiscountAmount;
            }
        }

        private bool GetDiscountLinesForRetailDiscountLineItemForLeastExpensive(
            DiscountableItemGroup[] discountableItemGroups,
            DiscountApplication discountApplication,
            List<int> linesWithDiscount,
            RetailDiscountLineItem item,
            DiscountLine discountItem)
        {
            decimal[] prices = new decimal[discountableItemGroups.Length];

            for (int i = 0; i < discountableItemGroups.Length; i++)
            {
                if (discountApplication.ItemQuantities[i] != decimal.Zero)
                {
                    prices[i] = discountableItemGroups[i].Price;
                }
            }

            List<Tuple<int, decimal>> lowestPrices = this.GetLeastExpensiveLines(discountApplication, prices);

            bool applyDiscount = (lowestPrices.Count(p => p.Item1 == item.ItemIndex) - linesWithDiscount.Count(p => p == item.ItemIndex)) > 0;

            if (applyDiscount)
            {
                if (this.DiscountPercentValue != decimal.Zero)
                {
                    discountItem.Percentage = Math.Min(100m, this.DiscountPercentValue);
                }
                else if (this.DiscountAmountValue != decimal.Zero)
                {
                    discountItem.Amount = Math.Min(this.DiscountAmountValue, discountableItemGroups[item.ItemIndex].Price);
                }
                else if (this.DealPriceValue != decimal.Zero)
                {
                    discountItem.DealPrice = this.DealPriceValue;
                    discountItem.Amount = Math.Max(discountableItemGroups[item.ItemIndex].Price - this.DealPriceValue, 0);
                }
            }

            return applyDiscount;
        }

        private List<Tuple<int, decimal>> GetLeastExpensiveLines(DiscountApplication discountApplication, decimal[] prices)
        {
            List<Tuple<int, decimal>> lowestPrices = new List<Tuple<int, decimal>>();

            for (int x = 0; x < discountApplication.ItemQuantities.Length; x++)
            {
                for (int y = 0; y < (int)discountApplication.ItemQuantities[x]; y++)
                {
                    lowestPrices.Add(new Tuple<int, decimal>(x, prices[x]));
                }
            }

            lowestPrices = lowestPrices.OrderBy(p => p.Item2).ThenByDescending(p => p.Item1).Take(this.NumberOfLeastExpensiveLines).ToList();
            return lowestPrices;
        }

        private decimal GetValueFromDiscountApplicationAndPrices(DiscountApplication discountApplication, decimal[] prices, Dictionary<int, IList<DiscountLineQuantity>> discountDictionary)
        {
            decimal result = 0M;
            decimal totalPrice;

            decimal numberOfDiscountLines = new decimal(discountApplication.RetailDiscountLines.Count());

            switch (this.DiscountType)
            {
                case DiscountMethodType.DealPrice:
                    totalPrice = discountApplication.RetailDiscountLines.Sum(p => prices[p.ItemIndex]);
                    result = totalPrice - this.DealPriceValue;
                    break;
                case DiscountMethodType.DiscountAmount:
                    result = this.DiscountAmountValue;
                    break;
                case DiscountMethodType.DiscountPercent:
                    totalPrice = discountApplication.RetailDiscountLines.Sum(p => prices[p.ItemIndex]);
                    result = totalPrice * (this.DiscountPercentValue / 100m);
                    break;
                case DiscountMethodType.LineSpecific:
                    Dictionary<decimal, decimal> retailDiscountLineNumberToDiscountAmoutMapForDealPrice = new Dictionary<decimal, decimal>();
                    foreach (RetailDiscountLineItem retailDiscountLineItem in discountApplication.RetailDiscountLines)
                    {
                        DiscountMethodType discountMethod = TranslateLineDiscountMethod(retailDiscountLineItem.RetailDiscountLine.DiscountMethod);
                        switch (discountMethod)
                        {
                            case DiscountMethodType.DiscountPercent:
                                result += prices[retailDiscountLineItem.ItemIndex] * (retailDiscountLineItem.RetailDiscountLine.DiscountLinePercentOrValue / 100m);
                                break;
                            case DiscountMethodType.DealPrice:
                                decimal dealPrice = retailDiscountLineItem.RetailDiscountLine.DiscountLinePercentOrValue;

                                // Use the deal price divided by the number of items for this line or for the mix and match group
                                int numberOfItemsForLine = retailDiscountLineItem.RetailDiscountLine.MixAndMatchLineNumberOfItemsNeeded != 0 ? retailDiscountLineItem.RetailDiscountLine.MixAndMatchLineNumberOfItemsNeeded : discountApplication.RetailDiscountLines.Count(p => p.RetailDiscountLine == retailDiscountLineItem.RetailDiscountLine);

                                if (numberOfItemsForLine >= 1m)
                                {
                                    dealPrice /= new decimal(numberOfItemsForLine);
                                }

                                decimal bestExistingDealPrice = 0m;
                                bool hasExistingDealPrice = DiscountBase.TryGetBestExistingDealPrice(discountDictionary, retailDiscountLineItem.ItemIndex, out bestExistingDealPrice);

                                decimal discountAmount;

                                // Please note dealPrice is the average one, so we may have negagive result for some items.
                                if (hasExistingDealPrice && bestExistingDealPrice > dealPrice)
                                {
                                    discountAmount = bestExistingDealPrice - dealPrice;
                                }
                                else
                                {
                                    discountAmount = prices[retailDiscountLineItem.ItemIndex] - dealPrice;
                                }

                                decimal retailDiscountLineDiscountAmount = decimal.Zero;
                                decimal lineNumber = retailDiscountLineItem.RetailDiscountLine.DiscountLineNumber;
                                retailDiscountLineNumberToDiscountAmoutMapForDealPrice.TryGetValue(lineNumber, out retailDiscountLineDiscountAmount);
                                retailDiscountLineNumberToDiscountAmoutMapForDealPrice[lineNumber] = retailDiscountLineDiscountAmount + discountAmount;

                                break;
                        }
                    }

                    foreach (KeyValuePair<decimal, decimal> pair in retailDiscountLineNumberToDiscountAmoutMapForDealPrice)
                    {
                        // If deal price is over the actual price, take 0 as discount amount, instead of negative discount amount.
                        if (pair.Value > decimal.Zero)
                        {
                            result += pair.Value;
                        }
                    }

                    break;
                case DiscountMethodType.LeastExpensive:
                    // Find the [NumberOfLeastExpensive] least expensive items by sorting prices and taking the lowest ones.
                    List<Tuple<int, decimal>> lowestPrices = this.GetLeastExpensiveLines(discountApplication, prices);

                    // We support $ off or deal price for least expensive only when number of least expensive items = 1.
                    if (discountApplication.DiscountPercentValue > decimal.Zero)
                    {
                        result = lowestPrices.Sum(p => ((p.Item2 * discountApplication.DiscountPercentValue) / 100m));
                    }
                    else if (discountApplication.DiscountAmountValue > decimal.Zero)
                    {
                        if (lowestPrices.Count >= 1)
                        {
                            result = Math.Min(lowestPrices[0].Item2, discountApplication.DiscountAmountValue);
                        }
                    }
                    else if (discountApplication.DealPriceValue > decimal.Zero)
                    {
                        if (lowestPrices.Count >= 1)
                        {
                            result = lowestPrices[0].Item2 - Math.Min(lowestPrices[0].Item2, discountApplication.DealPriceValue);
                        }
                    }

                    break;

                default:
                    break;
            }

            return result;
        }
    }
}
