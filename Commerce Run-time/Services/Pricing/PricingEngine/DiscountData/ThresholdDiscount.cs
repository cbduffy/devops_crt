﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine.DiscountData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// This class implements the threshold (by amount) discount processing, including the determination of which ways 
    /// the discount can apply to the transaction and the value of the discount applied to specific lines.
    /// </summary>
    public class ThresholdDiscount : DiscountBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ThresholdDiscount" /> class.
        /// </summary>
        public ThresholdDiscount()
            : base()
        {
            this.ThresholdDiscountTiers = new List<ThresholdDiscountTier>();
        }

        /// <summary>
        /// Gets the threshold discount tiers for this discount.
        /// </summary>
        public IList<ThresholdDiscountTier> ThresholdDiscountTiers { get; private set; }

        /// <summary>
        /// Gets all of the possible applications of this discount to the specified transaction and line items.
        /// </summary>
        /// <param name="transaction">The transaction to consider for discounts.</param>
        /// <param name="discountableItemGroups">The valid sales line items on the transaction to consider.</param>
        /// <param name="remainingQuantities">The remaining quantities of each of the sales lines to consider.</param>
        /// <param name="storePriceGroups">The collection of price groups that this store belongs to.</param>
        /// <param name="currencyCode">The currency code in use for the current transaction.</param>
        /// <param name="pricingDataManager">The IPricingDataManager instance to use.</param>
        /// <param name="isReturn">The flag indicating whether or not it's for return.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <returns>The possible permutations of line items that this discount can apply to, or an empty collection if this discount cannot apply.</returns>
        public override IEnumerable<DiscountApplication> GetDiscountApplications(
            SalesTransaction transaction,
            DiscountableItemGroup[] discountableItemGroups,
            decimal[] remainingQuantities,
            IEnumerable<long> storePriceGroups,
            string currencyCode,
            IPricingDataManagerV2 pricingDataManager,
            bool isReturn,
            PriceContext priceContext)
        {
            // If this discount cannot be applied, return an empty collection.
            if (!this.CanDiscountApply(transaction, storePriceGroups, currencyCode, pricingDataManager, isReturn, priceContext))
            {
                yield break;
            }

            // Get the discount code to use for any discount lines, if one is required.
            string discountCodeUsed = this.GetDiscountCodeForDiscount(transaction);

            HashSet<RetailDiscountLine> lines = new HashSet<RetailDiscountLine>();
            decimal totalAmountForTriggeredLines = 0M;

            for (int x = 0; x < discountableItemGroups.Length; x++)
            {
                DiscountableItemGroup item = discountableItemGroups[x];
                IList<RetailDiscountLine> triggeredLines = null;

                this.TryGetRetailDiscountLines(item.ProductId, item.MasterProductId, item.SalesOrderUnitOfMeasure, out triggeredLines);

                // As an optimization, we get the total amount here for each product or variant, so that we can determine if this discount is possible.
                if (triggeredLines.Any())
                {
                    totalAmountForTriggeredLines += item.Price * remainingQuantities[x];
                }

                foreach (RetailDiscountLine line in triggeredLines)
                {
                    if (!lines.Contains(line))
                    {
                        lines.Add(line);
                    }
                }
            }

            if (lines.Count > 0 && this.ThresholdDiscountTiers.Any(p => p.AmountThreshold <= totalAmountForTriggeredLines))
            {
                List<RetailDiscountLineItem> retailDiscountLines = new List<RetailDiscountLineItem>(lines.Count);

                foreach (RetailDiscountLine line in lines)
                {
                    retailDiscountLines.Add(new RetailDiscountLineItem(InvalidIndex, line));
                }

                DiscountApplication result = new DiscountApplication(this)
                {
                    RetailDiscountLines = retailDiscountLines,
                    SortIndex = -1,
                    SortValue = GetSortValue(lines.First()),
                    ItemQuantities = new decimal[remainingQuantities.Length],
                    DiscountCode = discountCodeUsed
                };

                yield return result;
            }
        }

        /// <summary>
        /// Gets the value of the specified discount application on the transaction, possibly including previously applied discounts.
        /// </summary>
        /// <param name="discountableItemGroups">The transaction line items.</param>
        /// <param name="remainingQuantities">The quantities remaining for each item.</param>
        /// <param name="appliedDiscounts">The previously applied discounts.</param>
        /// <param name="discountApplication">The specific application of the discount to use.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <param name="hasCompetingApplications">A value indicating whether it has competing applications.</param>
        /// <returns>The value of the discount application.</returns>
        public override AppliedDiscountApplication GetAppliedDiscountApplication(DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, Stack<AppliedDiscountApplication> appliedDiscounts, DiscountApplication discountApplication, PriceContext priceContext, bool hasCompetingApplications)
        {
            decimal totalAmount = 0M;

            // Return 0 and do not use this discount if any of the parameters are null or if another threshold discount has already been applied.
            if (discountableItemGroups == null || discountApplication == null || remainingQuantities == null || appliedDiscounts.Any(p => p.DiscountApplication.Discount.PeriodicDiscountType == PeriodicDiscountOfferType.Threshold && string.Equals(p.DiscountApplication.Discount.OfferId, this.OfferId, StringComparison.OrdinalIgnoreCase)))
            {
                return null;
            }

            decimal[] itemPrices = new decimal[discountableItemGroups.Length];

            HashSet<int> discountedItems = new HashSet<int>();

            // Need to get the total amount for the lines that are covered by this discount
            totalAmount = this.GetTotalAmountForCoveredLines(discountableItemGroups, remainingQuantities, appliedDiscounts, discountApplication, itemPrices, discountedItems, priceContext);

            // Now that we have the total and the item prices, calculate the amount.
            ThresholdDiscountTier tier =
                this.ThresholdDiscountTiers.Where(p => p.AmountThreshold <= Math.Abs(totalAmount))
                    .OrderByDescending(p => p.AmountThreshold)
                    .FirstOrDefault();

            decimal value = decimal.Zero;

            if (tier != null)
            {
                if (tier.DiscountMethod == ThresholdDiscountMethod.AmountOff)
                {
                    value = tier.DiscountValue;
                }
                else if (tier.DiscountMethod == ThresholdDiscountMethod.PercentOff)
                {
                    value = totalAmount * tier.DiscountValue / new decimal(100);
                }
            }

            AppliedDiscountApplication currentDiscountApplication = null;

            decimal[] appliedQuantities = new decimal[remainingQuantities.Length];
            foreach (int itemIndex in discountedItems)
            {
                appliedQuantities[itemIndex] = remainingQuantities[itemIndex];
            }

            if (value > decimal.Zero)
            {
                currentDiscountApplication = new AppliedDiscountApplication(discountApplication, value, appliedQuantities, totalAmount, discountedItems, itemPrices, false);
            }

            return currentDiscountApplication;
        }

        /// <summary>
        /// Generate discount lines for the applied discount application.
        /// </summary>
        /// <param name="appliedDiscountApplication">The applied discount application.</param>
        /// <param name="discountableItemGroups">The discountable item groups.</param>
        /// <param name="priceContext">The price context.</param>
        public override void GenerateDiscountLines(AppliedDiscountApplication appliedDiscountApplication, DiscountableItemGroup[] discountableItemGroups, PriceContext priceContext)
        {
            ThrowIf.Null(appliedDiscountApplication, "appliedDiscountApplication");
            ThrowIf.Null(discountableItemGroups, "discountableItemGroups");
            ThrowIf.Null(priceContext, "priceContext");

            appliedDiscountApplication.DiscountLinesDictionary = this.GenerateDiscountLinesDictionary(discountableItemGroups, appliedDiscountApplication.ItemQuantities, appliedDiscountApplication, priceContext);
        }

        /// <summary>
        /// Gets the sort value to use for the specified discount line.  For threshold discounts, this is either the value of the discount or zero for the percentage.
        /// </summary>
        /// <param name="discountLine">The discount line triggered for this application.</param>
        /// <returns>The sort value.</returns>
        private static decimal GetSortValue(RetailDiscountLine discountLine)
        {
            switch ((DiscountOfferMethod)discountLine.DiscountMethod)
            {
                case DiscountOfferMethod.DiscountAmount:
                    return discountLine.DiscountAmount;
                case DiscountOfferMethod.DiscountPercent:
                    return 0;
                case DiscountOfferMethod.OfferPrice:
                case DiscountOfferMethod.OfferPriceIncludingTax:
                    return discountLine.DiscountLinePercentOrValue;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Generate discount lines dictionary from the applied discount application.
        /// </summary>
        /// <param name="discountableItemGroups">The transaction line items.</param>
        /// <param name="remainingQuantities">The quantities remaining for each item.</param>
        /// <param name="discountApplicationToApply">The specific application of the discount to use.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <returns>A dictionary containing a list of DiscountLine objects for each index of discountableItemGroups that has at least one discount.</returns>
        private IDictionary<int, IList<DiscountLineQuantity>> GenerateDiscountLinesDictionary(DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, AppliedDiscountApplication discountApplicationToApply, PriceContext priceContext)
        {
            if (priceContext == null)
            {
                throw new ArgumentNullException("priceContext");
            }

            Dictionary<int, IList<DiscountLineQuantity>> result = new Dictionary<int, IList<DiscountLineQuantity>>();

            if (discountableItemGroups == null || discountApplicationToApply == null || remainingQuantities == null)
            {
                return result;
            }

            ISet<int> discountedItems = discountApplicationToApply.DiscountedItems;

            decimal totalAmount = discountApplicationToApply.TotalAmountForCoveredLines;
            decimal[] itemPrices = discountApplicationToApply.ItemPrices;

            // Now that we have the total and the item prices, calculate the discount lines for each discountable item.
            ThresholdDiscountTier tier =
                this.ThresholdDiscountTiers.Where(p => p.AmountThreshold <= Math.Abs(totalAmount))
                    .OrderByDescending(p => p.AmountThreshold)
                    .FirstOrDefault();

            if (tier != null)
            {
                if (tier.DiscountMethod == ThresholdDiscountMethod.AmountOff)
                {
                    decimal totalPrice = discountedItems.Sum(x => itemPrices[x] * remainingQuantities[x]);
                    decimal discountAmount = Math.Min(tier.DiscountValue, totalPrice);

                    //// We have to deal with rounding for $-off threshold discount to make sure total discount matches what's defined on the tier.
                    //// 1. Avoid split, i.e. if last one to process has fractional quantity or quantity = 1 (smallest unit).
                    //// 2. Avoid overallocation before the last one, e.g. $2 to 2 items: A of $10 with qty = 101, and B of $10 with qty = 1.
                    ////    If we process A first, then unit amount = $2 / 102 rounded to $0.02, so A got $2.02, more than it's allowed.
                    ////    In this case, we have to process A last, which will involve split A into 2 lines.
                    const int InitialItemIndex = -1;
                    int itemIndexSmallestIntegerQuantity = InitialItemIndex;
                    int itemIndexLargestNetPriceFractionalQuantity = InitialItemIndex;
                    int itemIndexLargestNetPrice = InitialItemIndex;

                    decimal largestNetPriceFractionalQuantity = decimal.Zero;
                    decimal largestNetPrice = decimal.Zero;
                    decimal smallestIngegerQuantity = decimal.Zero;
                    decimal smallestIntegerPrice = decimal.Zero;

                    // Step 1: get item index with smallest integer quantity and item index with largest fraction quantity net price.
                    for (int i = 0; i < discountableItemGroups.Length; i++)
                    {
                        if (discountApplicationToApply.ItemQuantities[i] > decimal.Zero && itemPrices[i] > decimal.Zero)
                        {
                            if (DiscountableItemGroup.IsFraction(discountApplicationToApply.ItemQuantities[i]))
                            {
                                if (itemIndexLargestNetPriceFractionalQuantity == InitialItemIndex || (itemPrices[i] * discountApplicationToApply.ItemQuantities[i]) > largestNetPriceFractionalQuantity)
                                {
                                    itemIndexLargestNetPriceFractionalQuantity = i;
                                    largestNetPriceFractionalQuantity = itemPrices[i] * discountApplicationToApply.ItemQuantities[i];
                                }
                            }
                            else
                            {
                                if (itemIndexSmallestIntegerQuantity == InitialItemIndex ||
                                    discountApplicationToApply.ItemQuantities[i] < smallestIngegerQuantity ||
                                    (discountApplicationToApply.ItemQuantities[i] == smallestIngegerQuantity && itemPrices[i] > smallestIntegerPrice))
                                {
                                    itemIndexSmallestIntegerQuantity = i;
                                    smallestIngegerQuantity = discountApplicationToApply.ItemQuantities[i];
                                    smallestIntegerPrice = itemPrices[i];
                                }
                            }

                            if (largestNetPrice == InitialItemIndex || (itemPrices[i] * discountApplicationToApply.ItemQuantities[i]) > largestNetPrice)
                            {
                                itemIndexLargestNetPrice = i;
                                largestNetPrice = itemPrices[i] * discountApplicationToApply.ItemQuantities[i];
                            }
                        }
                    }

                    int itemIndexLastOneToProcess = largestNetPriceFractionalQuantity > (smallestIngegerQuantity * smallestIntegerPrice) ? itemIndexLargestNetPriceFractionalQuantity : itemIndexSmallestIntegerQuantity;
                    int itemIndexSecondLastOneToProcess = itemIndexLargestNetPrice;

                    // Step 2: allocate discount amount to all except two items with largest net price and either largest fraction net price or smallest integer quantity.
                    foreach (int x in discountedItems)
                    {
                        decimal quantityApplied = discountApplicationToApply.ItemQuantities[x];

                        if (x != itemIndexLastOneToProcess && x != itemIndexSecondLastOneToProcess)
                        {
                            decimal unitDiscountAmount = priceContext.Rounding(discountAmount * itemPrices[x] / totalPrice);
                            decimal thisDiscountAmount = priceContext.Rounding(unitDiscountAmount * quantityApplied);
                            totalPrice -= itemPrices[x] * quantityApplied;
                            discountAmount -= thisDiscountAmount;
                            this.AddAmountOffDiscountLineToDictionary(discountApplicationToApply, discountableItemGroups, result, x, unitDiscountAmount, quantityApplied);
                        }
                    }

                    // Step 3: try allocating disocunt amount the item with largest net price, if discount overflows, move it to the last.
                    if (itemIndexSecondLastOneToProcess != itemIndexLastOneToProcess)
                    {
                        decimal quantityApplied = discountApplicationToApply.ItemQuantities[itemIndexSecondLastOneToProcess];
                        decimal unitDiscountAmount = priceContext.Rounding(discountAmount * itemPrices[itemIndexSecondLastOneToProcess] / totalPrice);
                        decimal thisDiscountAmount = priceContext.Rounding(unitDiscountAmount * quantityApplied);

                        if (thisDiscountAmount > discountAmount)
                        {
                            int itemIndexSwap = itemIndexLastOneToProcess;
                            itemIndexLastOneToProcess = itemIndexSecondLastOneToProcess;
                            itemIndexSecondLastOneToProcess = itemIndexSwap;
                        }

                        // Step 4: process the second one to last.
                        quantityApplied = discountApplicationToApply.ItemQuantities[itemIndexSecondLastOneToProcess];
                        unitDiscountAmount = priceContext.Rounding(discountAmount * itemPrices[itemIndexSecondLastOneToProcess] / totalPrice);
                        thisDiscountAmount = priceContext.Rounding(unitDiscountAmount * quantityApplied);
                        totalPrice -= itemPrices[itemIndexSecondLastOneToProcess] * quantityApplied;
                        discountAmount -= thisDiscountAmount;
                        this.AddAmountOffDiscountLineToDictionary(discountApplicationToApply, discountableItemGroups, result, itemIndexSecondLastOneToProcess, unitDiscountAmount, quantityApplied);
                    }

                    // Step 5: dole out the remaining to the last one
                    if (discountAmount > decimal.Zero)
                    {
                        decimal quantityToApply = discountApplicationToApply.ItemQuantities[itemIndexLastOneToProcess];
                        if (DiscountableItemGroup.IsFraction(quantityToApply))
                        {
                            // Last one is fractional quantity, dump leftover fully by not rounding unit discount amount.
                            decimal unitDiscountAmount = discountAmount / quantityToApply;
                            this.AddAmountOffDiscountLineToDictionary(discountApplicationToApply, discountableItemGroups, result, itemIndexLastOneToProcess, unitDiscountAmount, quantityToApply);
                        }
                        else
                        {
                            // Last one is integer quantity, let's do quantity = 1m at a time.
                            decimal quantityIncrement = 1m;
                            decimal price = itemPrices[itemIndexLastOneToProcess];
                            totalPrice = price * quantityToApply;

                            for (decimal quantity = quantityIncrement; quantity <= quantityToApply; quantity += quantityIncrement)
                            {
                                decimal unitDiscountAmount = priceContext.Rounding((discountAmount * price) / totalPrice);
                                totalPrice -= price;

                                if (unitDiscountAmount > decimal.Zero)
                                {
                                    discountAmount -= unitDiscountAmount;
                                    this.AddAmountOffDiscountLineToDictionary(discountApplicationToApply, discountableItemGroups, result, itemIndexLastOneToProcess, unitDiscountAmount, quantityIncrement);
                                }
                            }
                        }
                    }
                }
                else if (tier.DiscountMethod == ThresholdDiscountMethod.PercentOff)
                {
                    foreach (int x in discountedItems)
                    {
                        DiscountLine discountItem = this.NewDiscountLine(discountApplicationToApply.DiscountApplication.DiscountCode, discountableItemGroups[x].ItemId);
                        discountItem.Percentage = Math.Max(decimal.Zero, Math.Min(tier.DiscountValue, 100m));
                        discountItem.Amount = decimal.Zero;

                        result.Add(x, new List<DiscountLineQuantity>() { new DiscountLineQuantity(discountItem, remainingQuantities[x]) });
                    }
                }
            }

            return result;
        }

        private void AddAmountOffDiscountLineToDictionary(
            AppliedDiscountApplication discountApplicationToApply,
            DiscountableItemGroup[] discountableItemGroups,
            Dictionary<int, IList<DiscountLineQuantity>> itemIndexToDiscountLinesDictionary,
            int itemIndex,
            decimal unitDiscountAmount,
            decimal quantityApplied)
        {
            DiscountLine discountItem = this.NewDiscountLine(discountApplicationToApply.DiscountApplication.DiscountCode, discountableItemGroups[itemIndex].ItemId);
            discountItem.Amount = unitDiscountAmount;
            discountItem.Percentage = 0;

            IList<DiscountLineQuantity> discountLineQuantityList = null;
            if (itemIndexToDiscountLinesDictionary.TryGetValue(itemIndex, out discountLineQuantityList))
            {
                discountLineQuantityList.Add(new DiscountLineQuantity(discountItem, quantityApplied));
            }
            else
            {
                itemIndexToDiscountLinesDictionary.Add(itemIndex, new List<DiscountLineQuantity>() { new DiscountLineQuantity(discountItem, quantityApplied) });
            }
        }

        /// <summary>
        /// Gets the total amount on the transaction for the lines that may be used by this threshold discount.
        /// </summary>
        /// <param name="discountableItemGroups">The discountable item groups on the transaction.</param>
        /// <param name="remainingQuantities">The remaining quantities of the item groups.</param>
        /// <param name="appliedDiscounts">The previously applied discounts.</param>
        /// <param name="discountApplication">The specific discount application to determine the value for.</param>
        /// <param name="itemPrices">The average prices of items on the transaction after previously applied compounding discounts.</param>
        /// <param name="discountedItems">The items that will be discounted by this application of the threshold discount.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <returns>The total amount for the lines specified.</returns>
        private decimal GetTotalAmountForCoveredLines(DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, Stack<AppliedDiscountApplication> appliedDiscounts, DiscountApplication discountApplication, decimal[] itemPrices, ISet<int> discountedItems, PriceContext priceContext)
        {
            HashSet<string> discountPriceGroups = new HashSet<string>(ConvertPriceDiscountGroupIdsToGroups(this.PriceDiscountGroupIds, priceContext));
            decimal totalAmount = 0;
            Dictionary<int, IList<DiscountLineQuantity>> discountDictionary = new Dictionary<int, IList<DiscountLineQuantity>>();
            if (this.ConcurrencyMode == ConcurrencyMode.Compounded)
            {
                this.InitializeDiscountDictionary(appliedDiscounts, discountDictionary, false);
            }

            for (int x = 0; x < discountableItemGroups.Length; x++)
            {
                if (remainingQuantities[x] != 0M
                    && (this.ShouldCountNonDiscountItems || DiscountBase.IsDiscountAllowedForDiscountableItemGroup(discountableItemGroups[x]))
                    && DiscountBase.IsDiscountAllowedForCatalogIds(priceContext, discountPriceGroups, discountableItemGroups[x].CatalogIds)
                    && this.GetFirstMatchingLineForItem(discountableItemGroups[x], discountApplication.RetailDiscountLines.Select(p => p.RetailDiscountLine)) != null)
                {
                    itemPrices[x] = discountableItemGroups[x].Price;

                    // If this is a compounding discount, we need to determine any previous discounts that could have applied to it, unless it is an amount discount.
                    if (this.ConcurrencyMode == ConcurrencyMode.Compounded)
                    {
                        // Determine the value of the previously applied concurrent discounts so that we can get the proper value here.
                        // We group them together here to get the correct amount when there is a quantity greater than one and a percentage discount.
                        if (discountDictionary.ContainsKey(x))
                        {
                            List<List<DiscountLineQuantity>> sortedConcurrentDiscounts =
                                discountDictionary[x].GroupBy(p => p.DiscountLine.OfferId)
                                                     .OrderByDescending(p => p.First().DiscountLine.Amount)
                                                     .Select(concurrentDiscountGroup => concurrentDiscountGroup.ToList())
                                                     .ToList();

                            foreach (List<DiscountLineQuantity> concurrentDiscount in sortedConcurrentDiscounts)
                            {
                                decimal startingPrice = itemPrices[x];

                                foreach (DiscountLineQuantity discLine in concurrentDiscount)
                                {
                                    itemPrices[x] -= (((startingPrice * discLine.DiscountLine.Percentage / 100M) + discLine.DiscountLine.Amount) * discLine.Quantity) / Math.Abs(discountableItemGroups[x].Quantity);
                                }
                            }
                        }
                    }

                    totalAmount += remainingQuantities[x] * itemPrices[x];

                    if (DiscountBase.IsDiscountAllowedForDiscountableItemGroup(discountableItemGroups[x]))
                    {
                        discountedItems.Add(x);
                    }
                }
            }

            return totalAmount;
        }
    }
}