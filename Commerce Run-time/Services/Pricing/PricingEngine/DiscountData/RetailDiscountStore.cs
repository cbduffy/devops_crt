﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine.DiscountData
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Retail discount store.
    /// </summary>
    public static class RetailDiscountStore
    {
        internal static Dictionary<long, List<DiscountBase>> GetProductOrVariantToDiscontMapLive(
            SalesTransaction transaction,
            PriceContext priceContext,
            IPricingDataManagerV2 pricingDataManager)
        {
            List<ItemUnit> items = new List<ItemUnit>();
            Dictionary<string, long> itemIdInventDimIdToProductOrVariantIdMap = new Dictionary<string, long>(StringComparer.OrdinalIgnoreCase);

            foreach (SalesLine salesLine in transaction.PriceCalculableSalesLines)
            {
                itemIdInventDimIdToProductOrVariantIdMap[GetItemIdInventDimIdKey(salesLine.ItemId, salesLine.InventoryDimensionId)] = salesLine.ProductId;

                items.Add(new ItemUnit() { ItemId = salesLine.ItemId, VariantInventoryDimensionId = salesLine.InventoryDimensionId, Product = salesLine.MasterProductId == 0 ? salesLine.ProductId : salesLine.MasterProductId, DistinctProductVariant = salesLine.Variant != null ? salesLine.Variant.DistinctProductVariantId : 0, UnitOfMeasure = Discount.GetUnitOfMeasure(salesLine) });
            }

            ReadOnlyCollection<PeriodicDiscount> discountAndLines = GetRetailDiscountsAndLines(items, priceContext, pricingDataManager);
            ISet<long> productOrVariantIdsInTransaction = GetProductOrVariantIdsForTransaction(transaction);

            Dictionary<long, List<DiscountBase>> productDiscountMap = new Dictionary<long, List<DiscountBase>>();
            Dictionary<string, DiscountBase> offerIdToDiscountMap = new Dictionary<string, DiscountBase>(StringComparer.OrdinalIgnoreCase);

            foreach (PeriodicDiscount discountAndLine in discountAndLines)
            {
                if (!PriceContextHelper.MatchCalculationMode(priceContext, discountAndLine.PeriodicDiscountType))
                {
                    continue;
                }

                string key = GetItemIdInventDimIdKey(discountAndLine.ItemId, discountAndLine.InventoryDimensionId);
                long productOrVariantId = 0;
                if (itemIdInventDimIdToProductOrVariantIdMap.TryGetValue(key, out productOrVariantId))
                {
                    DiscountBase discount = null;

                    if (offerIdToDiscountMap.TryGetValue(discountAndLine.OfferId, out discount))
                    {
                        RetailDiscountLine discountLine = null;
                        if (!discount.DiscountLines.TryGetValue(discountAndLine.DiscountLineNumber, out discountLine))
                        {
                            discountLine = ConvertDiscountAndLineToDiscountLine(discountAndLine, discount);
                            discount.DiscountLines.Add(discountLine.DiscountLineNumber, discountLine);
                        }

                        IList<RetailDiscountLine> discountLines = null;
                        if (discount.ProductOfVariantToDiscountLinesMap.TryGetValue(productOrVariantId, out discountLines))
                        {
                            discountLines.Add(discountLine);
                        }
                        else
                        {
                            discount.ProductOfVariantToDiscountLinesMap[productOrVariantId] = new List<RetailDiscountLine> { discountLine };
                        }
                    }
                    else
                    {
                        discount = ConvertDiscountAndLineToDiscountBase(discountAndLine);
                        discount.ProductOrVariantIdsInTransaction = productOrVariantIdsInTransaction;
                        RetailDiscountLine discountLine = ConvertDiscountAndLineToDiscountLine(discountAndLine, discount);
                        discount.DiscountLines.Add(discountLine.DiscountLineNumber, discountLine);
                        offerIdToDiscountMap.Add(discount.OfferId, discount);
                        discount.ProductOfVariantToDiscountLinesMap[productOrVariantId] = new List<RetailDiscountLine> { discountLine };
                    }

                    List<DiscountBase> discounts;
                    if (productDiscountMap.TryGetValue(productOrVariantId, out discounts))
                    {
                        if (!discounts.Where(p => p.OfferId == discount.OfferId).Any())
                        {
                            discounts.Add(discount);
                        }
                    }
                    else
                    {
                        productDiscountMap[productOrVariantId] = new List<DiscountBase>() { discount };
                    }
                }
            }

            IEnumerable<string> offerIds = offerIdToDiscountMap.Select(p => p.Key);

            if (offerIds.Any())
            {
                ReadOnlyCollection<DiscountCode> discountCodes = pricingDataManager.GetDiscountCodesByOfferId(offerIds, new ColumnSet());

                foreach (DiscountCode discountCode in discountCodes)
                {
                    DiscountBase discountBase;
                    if (offerIdToDiscountMap.TryGetValue(discountCode.OfferId, out discountBase))
                    {
                        // Accept both discount code and barcode in retail channel.
                        discountBase.DiscountCodes.Add(discountCode.Code);
                        discountBase.DiscountCodes.Add(discountCode.Barcode);
                    }
                }

                ReadOnlyCollection<RetailDiscountPriceGroup> discountPriceGroups = pricingDataManager.GetRetailDiscountPriceGroups(new HashSet<string>(offerIds));

                foreach (RetailDiscountPriceGroup discountPriceGroup in discountPriceGroups)
                {
                    offerIdToDiscountMap[discountPriceGroup.OfferId].PriceDiscountGroupIds.Add(discountPriceGroup.PriceGroupId);
                }

                IEnumerable<string> quantityOfferIds = offerIdToDiscountMap.Where(p => p.Value.PeriodicDiscountType == PeriodicDiscountOfferType.MultipleBuy).Select(p => p.Key);

                if (quantityOfferIds.Any())
                {
                    ReadOnlyCollection<QuantityDiscountLevel> quantityLevels = pricingDataManager.GetMultipleBuyDiscountLinesByOfferIds(quantityOfferIds);

                    foreach (QuantityDiscountLevel quantityLevel in quantityLevels)
                    {
                        DiscountBase discountBase;
                        if (offerIdToDiscountMap.TryGetValue(quantityLevel.OfferId, out discountBase))
                        {
                            MultipleBuyDiscount multipleBuy = discountBase as MultipleBuyDiscount;

                            if (multipleBuy != null)
                            {
                                multipleBuy.QuantityDiscountLevels.Add(quantityLevel);
                            }
                        }
                    }
                }

                IEnumerable<string> mixMatchOfferIds = offerIdToDiscountMap.Where(p => p.Value.PeriodicDiscountType == PeriodicDiscountOfferType.MixAndMatch).Select(p => p.Key);

                if (mixMatchOfferIds.Any())
                {
                    ReadOnlyCollection<MixAndMatchLineGroup> mixMatchLineGroups = pricingDataManager.GetMixAndMatchLineGroupsByOfferIds(mixMatchOfferIds);

                    foreach (MixAndMatchLineGroup lineGroup in mixMatchLineGroups)
                    {
                        DiscountBase discountBase;
                        if (offerIdToDiscountMap.TryGetValue(lineGroup.OfferId, out discountBase))
                        {
                            MixAndMatchDiscount mixMatch = discountBase as MixAndMatchDiscount;

                            if (mixMatch != null)
                            {
                                mixMatch.LineGroupToNumberOfItemsMap.Add(lineGroup.LineGroup, lineGroup.NumberOfItemsNeeded);
                            }
                        }
                    }
                }

                IEnumerable<string> thresholdOfferIds = offerIdToDiscountMap.Where(p => p.Value.PeriodicDiscountType == PeriodicDiscountOfferType.Threshold).Select(p => p.Key);

                if (thresholdOfferIds.Any())
                {
                    ReadOnlyCollection<ThresholdDiscountTier> thresholdTiers = pricingDataManager.GetThresholdTiersByOfferIds(thresholdOfferIds);

                    foreach (ThresholdDiscountTier tier in thresholdTiers)
                    {
                        DiscountBase discountBase;
                        if (offerIdToDiscountMap.TryGetValue(tier.OfferId, out discountBase))
                        {
                            ThresholdDiscount threshold = discountBase as ThresholdDiscount;

                            if (threshold != null)
                            {
                                threshold.ThresholdDiscountTiers.Add(tier);
                            }
                        }
                    }
                }
            }

            return productDiscountMap;
        }

        internal static Dictionary<long, List<DiscountBase>> GetProductOrVarintToDiscountMapFromCache(
            IPricingDataManagerV2 pricingDataManager,
            PriceContext priceContext,
            SalesTransaction transaction)
        {
            ISet<long> productOrVariantIdsInTransaction = GetProductOrVariantIdsForTransaction(transaction);
            Dictionary<long, IList<RetailCategoryMember>> categorytoProductOrVariantIdsMap = GetCategoryToProductOrVariantIdsMapForTransaction(pricingDataManager, productOrVariantIdsInTransaction);

            ReadOnlyCollection<RetailDiscount> allDiscounts = pricingDataManager.GetAllRetailDiscounts();

            Dictionary<long, List<DiscountBase>> allApplicableDiscounts = new Dictionary<long, List<DiscountBase>>();

            foreach (RetailDiscount retailDiscount in allDiscounts)
            {
                if (!PriceContextHelper.MatchCalculationMode(priceContext, retailDiscount.PeriodicDiscountType))
                {
                    continue;
                }

                DiscountBase discount = ConvertRetailDiscountToDiscountBase(retailDiscount);
                discount.ProductOrVariantIdsInTransaction = productOrVariantIdsInTransaction;

                // Product or variant id to categories map is needed to filter which discount lines are applicable for the transaction. See DiscountBase class.
                discount.CategoryToProductOrVariantIdsMap = categorytoProductOrVariantIdsMap;
                IDictionary<long, IList<RetailDiscountLine>> itemDiscounts = discount.GetProductOrVariantIdToRetailDiscountLinesMap();
                foreach (long productOrVariantId in itemDiscounts.Keys)
                {
                    if (allApplicableDiscounts.ContainsKey(productOrVariantId))
                    {
                        allApplicableDiscounts[productOrVariantId].Add(discount);
                    }
                    else
                    {
                        allApplicableDiscounts.Add(productOrVariantId, new List<DiscountBase>() { discount });
                    }
                }
            }

            return allApplicableDiscounts;
        }

        private static ISet<long> GetProductOrVariantIdsForTransaction(SalesTransaction salesTransaction)
        {
            HashSet<long> productOrVariantIds = new HashSet<long>();
            foreach (SalesLine salesLine in salesTransaction.PriceCalculableSalesLines)
            {
                productOrVariantIds.Add(salesLine.ProductId);

                if (salesLine.ProductId != salesLine.MasterProductId)
                {
                    productOrVariantIds.Add(salesLine.MasterProductId);
                }
            }

            return productOrVariantIds;
        }

        /// <summary>
        /// Gets category to product or variant identifiers lookup for all items applicable of price on the transaction.
        /// </summary>
        /// <param name="pricingDataManager">Pricing data manager.</param>
        /// <param name="productOrVariantIdsInTransaction">Product or variant identifiers in transaction.</param>
        /// <returns>A dictionary of category to product categories.</returns>
        private static Dictionary<long, IList<RetailCategoryMember>> GetCategoryToProductOrVariantIdsMapForTransaction(
            IPricingDataManagerV2 pricingDataManager,
            ISet<long> productOrVariantIdsInTransaction)
        {
            Dictionary<long, IList<RetailCategoryMember>> result = new Dictionary<long, IList<RetailCategoryMember>>();

            ReadOnlyCollection<RetailCategoryMember> allProductCategories = pricingDataManager.GetRetailCategoryMembersForItems(productOrVariantIdsInTransaction);

            if (allProductCategories != null)
            {
                foreach (RetailCategoryMember productCategory in allProductCategories)
                {
                    IList<RetailCategoryMember> productCategories = null;
                    if (result.TryGetValue(productCategory.CategoryId, out productCategories))
                    {
                        productCategories.Add(productCategory);
                    }
                    else
                    {
                        productCategories = new List<RetailCategoryMember>() { productCategory };
                        result.Add(productCategory.CategoryId, productCategories);
                    }
                }
            }

            return result;
        }

        private static DiscountMethodType GetDiscountMethodType(PeriodicDiscountOfferType periodicDiscountType, int discountTypeFromDatabase)
        {
            DiscountMethodType discountMethod = DiscountMethodType.DealPrice;

            // ISNULL(pdmm.MIXANDMATCHDISCOUNTTYPE, ISNULL(pdmb.MULTIBUYDISCOUNTTYPE, pd.PERIODICDISCOUNTTYPE))
            switch (periodicDiscountType)
            {
                case PeriodicDiscountOfferType.Offer:
                case PeriodicDiscountOfferType.Promotion:
                    discountMethod = DiscountMethodType.LineSpecific;
                    break;
                case PeriodicDiscountOfferType.MixAndMatch:
                case PeriodicDiscountOfferType.MultipleBuy:
                    discountMethod = (DiscountMethodType)discountTypeFromDatabase;
                    break;
                case PeriodicDiscountOfferType.Threshold:
                    discountMethod = DiscountMethodType.LineSpecific;
                    break;
                default:
                    NetTracer.Warning("Unsupported discount type: {0}", discountTypeFromDatabase);
                    break;
            }

            return discountMethod;
        }

        private static DiscountOfferMethod GetLineDiscountOfferMethod(PeriodicDiscountOfferType periodicDiscountType, DiscountMethodType discountMethod, int lineDiscountMethod, int lineSpecificDiscountType)
        {
            DiscountOfferMethod offerMethod = (DiscountOfferMethod)lineDiscountMethod;

            if (periodicDiscountType == PeriodicDiscountOfferType.MixAndMatch && discountMethod == DiscountMethodType.LineSpecific)
            {
                if (lineSpecificDiscountType == (int)DiscountMethodType.DealPrice)
                {
                    offerMethod = DiscountOfferMethod.OfferPrice;
                }
                else if (lineSpecificDiscountType == (int)DiscountMethodType.DiscountPercent)
                {
                    offerMethod = DiscountOfferMethod.DiscountPercent;
                }
            }

            return offerMethod;
        }

        private static DiscountBase ConvertRetailDiscountToDiscountBase(RetailDiscount retailDiscount)
        {
            DiscountBase discount = null;
            OfferDiscount offer = null;
            MixAndMatchDiscount mixAndMatch = null;
            MultipleBuyDiscount multipleBuy = null;
            ThresholdDiscount threshold = null;

            switch (retailDiscount.PeriodicDiscountType)
            {
                case PeriodicDiscountOfferType.Offer:
                    offer = new OfferDiscount();
                    discount = offer;
                    break;
                case PeriodicDiscountOfferType.MixAndMatch:
                    mixAndMatch = new MixAndMatchDiscount();
                    mixAndMatch.DealPriceValue = retailDiscount.MixAndMatchDealPrice;
                    mixAndMatch.DiscountAmountValue = retailDiscount.MixAndMatchDiscountAmount;
                    mixAndMatch.DiscountPercentValue = retailDiscount.MixAndMatchDiscountPercent;
                    mixAndMatch.NumberOfLeastExpensiveLines = retailDiscount.MixAndMatchNumberOfLeastExpensiveLines;
                    mixAndMatch.NumberOfTimesApplicable = retailDiscount.MixAndMatchNumberOfTimeApplicable;
                    foreach (RetailDiscountLine mixMatchLine in retailDiscount.DiscountLines)
                    {
                        if (!mixAndMatch.LineGroupToNumberOfItemsMap.ContainsKey(mixMatchLine.MixAndMatchLineGroup))
                        {
                            mixAndMatch.LineGroupToNumberOfItemsMap.Add(mixMatchLine.MixAndMatchLineGroup, mixMatchLine.MixAndMatchLineNumberOfItemsNeeded);
                        }
                    }

                    discount = mixAndMatch;
                    break;
                case PeriodicDiscountOfferType.MultipleBuy:
                    multipleBuy = new MultipleBuyDiscount();
                    multipleBuy.QuantityDiscountLevels.AddRange(retailDiscount.MultibuyQuantityTiers);
                    discount = multipleBuy;
                    break;
                case PeriodicDiscountOfferType.Threshold:
                    threshold = new ThresholdDiscount();
                    threshold.ShouldCountNonDiscountItems = retailDiscount.ShouldCountNonDiscountItems != 0;
                    threshold.ThresholdDiscountTiers.AddRange(retailDiscount.ThresholdDiscountTiers);
                    discount = threshold;
                    break;
            }

            if (discount != null)
            {
                discount.IsCategoryToProductOrVariantIdsMapSet = false;

                discount.OfferId = retailDiscount.OfferId;
                discount.OfferName = retailDiscount.Name;
                discount.PeriodicDiscountType = retailDiscount.PeriodicDiscountType;
                discount.IsDiscountCodeRequired = retailDiscount.IsDiscountCodeRequired;
                discount.ConcurrencyMode = retailDiscount.ConcurrencyMode;
                discount.CurrencyCode = retailDiscount.CurrencyCode;
                discount.DateValidationPeriodId = retailDiscount.ValidationPeriodId;
                discount.DateValidationType = (DateValidationType)retailDiscount.DateValidationType;
                discount.DiscountType = GetDiscountMethodType(discount.PeriodicDiscountType, retailDiscount.DiscountType);
                discount.ValidFrom = retailDiscount.ValidFromDate;
                discount.ValidTo = retailDiscount.ValidToDate;

                foreach (RetailDiscountLine discountLine in retailDiscount.DiscountLines)
                {
                    discountLine.DiscountMethod = (int)GetLineDiscountOfferMethod(discount.PeriodicDiscountType, discount.DiscountType, discountLine.DiscountMethod, discountLine.MixAndMatchLineSpecificDiscountType);
                    discount.DiscountLines.Add(discountLine.DiscountLineNumber, discountLine);
                }

                foreach (RetailDiscountPriceGroup priceGroup in retailDiscount.PriceGroups)
                {
                    discount.PriceDiscountGroupIds.Add(priceGroup.PriceGroupId);
                }

                foreach (DiscountCode discountCode in retailDiscount.DiscountCodes)
                {
                    discount.DiscountCodes.Add(discountCode.Code);
                    discount.DiscountCodes.Add(discountCode.Barcode);
                }
            }

            return discount;
        }

        private static RetailDiscountLine ConvertDiscountAndLineToDiscountLine(PeriodicDiscount discountAndLine, DiscountBase discount)
        {
            RetailDiscountLine discountLine = new RetailDiscountLine();
            discountLine.OfferId = discountAndLine.OfferId;
            discountLine.DiscountLineNumber = discountAndLine.DiscountLineNumber;
            discountLine.ProductId = discountAndLine.ProductId;
            discountLine.DistinctProductVariantId = discountAndLine.DistinctProductVariantId;
            discountLine.DiscountAmount = discountAndLine.DiscountAmount;
            discountLine.DiscountLinePercentOrValue = discountAndLine.DiscountLinePercentOrValue;
            discountLine.DiscountMethod = (int)GetLineDiscountOfferMethod(discount.PeriodicDiscountType, discount.DiscountType, discountAndLine.DiscountMethod, discountAndLine.MixAndMatchLineSpecificDiscountType);
            discountLine.DiscountPercent = discountAndLine.DiscountPercent;
            discountLine.MixAndMatchLineGroup = discountAndLine.MixAndMatchLineGroup;
            discountLine.MixAndMatchLineNumberOfItemsNeeded = discountAndLine.MixAndMatchLineNumberOfItemsNeeded;
            discountLine.MixAndMatchLineSpecificDiscountType = discountAndLine.MixAndMatchLineSpecificDiscountType;
            discountLine.OfferPrice = discountAndLine.OfferPrice;
            discountLine.OfferPriceIncludingTax = discountAndLine.OfferPriceIncludingTax;
            discountLine.UnitOfMeasureSymbol = discountAndLine.UnitOfMeasureSymbol;

            return discountLine;
        }

        private static DiscountBase ConvertDiscountAndLineToDiscountBase(PeriodicDiscount discountAndLine)
        {
            DiscountBase discount = null;
            OfferDiscount offer = null;
            MixAndMatchDiscount mixAndMatch = null;
            MultipleBuyDiscount multipleBuy = null;
            ThresholdDiscount threshold = null;

            switch (discountAndLine.PeriodicDiscountType)
            {
                case PeriodicDiscountOfferType.Offer:
                    offer = new OfferDiscount();
                    discount = offer;
                    break;
                case PeriodicDiscountOfferType.MixAndMatch:
                    mixAndMatch = new MixAndMatchDiscount();
                    mixAndMatch.DealPriceValue = discountAndLine.MixAndMatchDealPrice;
                    mixAndMatch.DiscountAmountValue = discountAndLine.MixAndMatchDiscountAmount;
                    mixAndMatch.DiscountPercentValue = discountAndLine.MixAndMatchDiscountPercent;
                    mixAndMatch.NumberOfLeastExpensiveLines = discountAndLine.MixAndMatchNumberOfLeastExpensiveLines;
                    mixAndMatch.NumberOfTimesApplicable = discountAndLine.MixAndMatchNumberOfTimeApplicable;
                    discount = mixAndMatch;
                    break;
                case PeriodicDiscountOfferType.MultipleBuy:
                    multipleBuy = new MultipleBuyDiscount();
                    discount = multipleBuy;
                    break;
                case PeriodicDiscountOfferType.Threshold:
                    threshold = new ThresholdDiscount();
                    threshold.ShouldCountNonDiscountItems = discountAndLine.ShouldCountNonDiscountItems != 0;
                    discount = threshold;
                    break;
            }

            if (discount != null)
            {
                discount.IsCategoryToProductOrVariantIdsMapSet = true;

                discount.OfferId = discountAndLine.OfferId;
                discount.OfferName = discountAndLine.Name;
                discount.PeriodicDiscountType = discountAndLine.PeriodicDiscountType;
                discount.IsDiscountCodeRequired = discountAndLine.IsDiscountCodeRequired;
                discount.ConcurrencyMode = discountAndLine.ConcurrencyMode;
                discount.CurrencyCode = discountAndLine.CurrencyCode;
                discount.DateValidationPeriodId = discountAndLine.ValidationPeriodId;
                discount.DateValidationType = (DateValidationType)discountAndLine.DateValidationType;
                discount.DiscountType = GetDiscountMethodType(discount.PeriodicDiscountType, discountAndLine.DiscountType);
                discount.ValidFrom = discountAndLine.ValidFromDate;
                discount.ValidTo = discountAndLine.ValidToDate;
            }

            return discount;
        }

        private static string GetItemIdInventDimIdKey(string itemId, string inventDimId)
        {
            return string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0}-@-{1}", itemId, inventDimId);
        }

        private static ReadOnlyCollection<PeriodicDiscount> GetRetailDiscountsAndLines(
            IEnumerable<ItemUnit> items,
            PriceContext priceContext,
            IPricingDataManagerV2 pricingDataManager)
        {
            // don't do lookup if there aren't any price groups to search by
            HashSet<string> allPriceGroups = PriceContextHelper.GetAllPriceGroupsForDiscount(priceContext);
            if (allPriceGroups.Count == 0)
            {
                return new ReadOnlyCollection<PeriodicDiscount>(new PeriodicDiscount[0]);
            }

            ReadOnlyCollection<ValidationPeriod> validationPeriods;
            ReadOnlyCollection<PeriodicDiscount> discounts =
                pricingDataManager.ReadRetailDiscounts(items, allPriceGroups, priceContext.ActiveDate.Date, priceContext.ActiveDate.Date, priceContext.CurrencyCode, out validationPeriods);

            var periodDict = validationPeriods.ToDictionary(p => p.PeriodId, StringComparer.OrdinalIgnoreCase);
            ReadOnlyCollection<PeriodicDiscount> validDiscounts =
                discounts.Where(d => IsDiscountActive(periodDict, d, priceContext.ActiveDate)).AsReadOnly();

            return validDiscounts;
        }

        private static bool IsDiscountActive(
            Dictionary<string, ValidationPeriod> periodDict,
            PeriodicDiscount discount,
            DateTimeOffset activeDate)
        {
            // get validation period if any for this discount (otherwise default period)
            ValidationPeriod validationPeriod;
            if (!periodDict.TryGetValue(discount.ValidationPeriodId, out validationPeriod))
            {
                validationPeriod = new ValidationPeriod();
            }

            return PricingEngine.IsPromoPeriodValid(
                validationPeriod,
                (DateValidationType)discount.DateValidationType,
                discount.ValidFromDate,
                discount.ValidToDate,
                activeDate);
        }
    }
}
