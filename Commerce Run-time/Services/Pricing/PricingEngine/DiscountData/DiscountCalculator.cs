﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine.DiscountData
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// This class contains methods used to perform the discount calculations on a transaction, 
    /// determining the correct discounts to apply to the transaction.
    /// </summary>
    public class DiscountCalculator
    {
        private const int InvalidIndex = -1;
        private const string KeyDelimiter = "|";

#if DEBUG
        private static bool logDiscountDetails = false;

        private long stepCount = 0;
        private long pathCount = 0;
#endif

        private Dictionary<long, List<DiscountBase>> itemDiscounts = new Dictionary<long, List<DiscountBase>>();
        private PriceContext priceContext;
        private string currencyCode;
        private IEnumerable<long> storePriceGroups;
        private IPricingDataManagerV2 pricingDataManager;
        private bool enableCache = false;

        /// <summary>
        /// Initializes a new instance of the DiscountCalculator class for the specified transaction.
        /// </summary>
        /// <param name="transaction">The transaction to calculate the discounts for.</param>
        /// <param name="currencyCode">The currency code.</param>
        /// <param name="priceContext">The pricing context.</param>
        /// <param name="pricingDataManager">The pricing data manager.</param>
        public DiscountCalculator(SalesTransaction transaction, string currencyCode, PriceContext priceContext, IPricingDataManagerV2 pricingDataManager)
        {
            this.currencyCode = currencyCode;
            this.priceContext = priceContext;
            this.storePriceGroups = PriceContextHelper.GetAllPriceGroupIdsForDiscount(priceContext);
            this.pricingDataManager = pricingDataManager;
            this.InitializeDiscounts(transaction);
        }

#if DEBUG
        private enum DebugLogHeader : int
        {
            Step = 0,
            Path = 1,
        }
#endif

        /// <summary>
        /// Determine the best set of discounts to apply to the specified transaction, and apply those discounts.
        /// </summary>
        /// <param name="transaction">The transaction to perform discounting on.</param>
        public void ApplyDiscounts(SalesTransaction transaction)
        {
            if (transaction == null)
            {
                throw new ArgumentException("A value for transaction must be specified", "transaction");
            }

            DiscountableItemGroup[] discountableItemGroups = this.GetDiscountableItems(transaction);

            DiscountableItemGroup[] positiveDiscountableItemGroups = discountableItemGroups.Where(p => p.Quantity > 0).ToArray();
            DiscountableItemGroup[] negativeDiscountableItemGroups = discountableItemGroups.Where(p => p.Quantity < 0).ToArray();

            if (positiveDiscountableItemGroups.Length > 0)
            {
                this.CalculateAndApplyBestDiscounts(transaction, positiveDiscountableItemGroups, false);
            }

            if (negativeDiscountableItemGroups.Length > 0)
            {
                foreach (DiscountableItemGroup group in negativeDiscountableItemGroups)
                {
                    group.NegateQuantity();
                }

                this.CalculateAndApplyBestDiscounts(transaction, negativeDiscountableItemGroups, true);
            }
        }

#if DEBUG
        private static void WriteDebugInfoForDiscountApplications(string header, DiscountApplication[] exclusiveOrBestDiscountApplications, DiscountableItemGroup[] discountableItemGroups)
        {
            if (logDiscountDetails)
            {
                Debug.WriteLine("[{0}] possible discount applications: {1}", header, exclusiveOrBestDiscountApplications.Count());

                foreach (DiscountApplication discountApplication in exclusiveOrBestDiscountApplications)
                {
                    Debug.WriteLine("  [{0}] deal [{1:0.##}] $ off [{2:0.##}] % off [{3:0.##}] sort [{4}] v [{5:0.##}]  ", discountApplication.Discount.OfferId, discountApplication.DealPriceValue, discountApplication.DiscountAmountValue, discountApplication.DiscountPercentValue, discountApplication.SortIndex, discountApplication.SortValue);

                    for (int i = 0; i < discountApplication.ItemQuantities.Length; i++)
                    {
                        decimal quantity = discountApplication.ItemQuantities[i];
                        if (quantity != 0m)
                        {
                            Debug.WriteLine("    [{0}] for [{1}]-[{2}] q [{3}]", i, discountableItemGroups[i].ItemId, discountableItemGroups[i].ProductId, quantity);
                        }
                    }
                }
            }
        }
#endif

        /// <summary>
        /// Gets the remaining possible applications after using a compounding discount.
        /// </summary>
        /// <param name="appliedDiscountsStack">The currently applied discounts in this path.</param>
        /// <param name="possibleDiscountApplications">The possible discount applications.</param>
        /// <param name="nextIndex">The next index in the possible applications to consider.</param>
        /// <param name="remainingQuantities">The remaining quantities of items.</param>
        /// <param name="remainingApplications">The remaining applications that are possible.</param>
        private static void GetRemainingApplicationsForCompoundedDiscount(Stack<AppliedDiscountApplication> appliedDiscountsStack, DiscountApplication[] possibleDiscountApplications, int nextIndex, decimal[] remainingQuantities, ref BitSet remainingApplications)
        {
            // For compounding discounts, it's a bit more complicated, since this particular discount can only apply to each unit once, 
            // and each unit should also remove any non-compounding discounts from the possibilities.
            // Due to the order that we do processing in, this particular discount must be at the top of the stack, so once we encounter a different application, we can stop.
            // We also should allow the current discount to be processed again as long as the remaining quantities are still sufficient to trigger it.  
            // If they are not, the current discount cannot be applied again, so it should be removed from the remaining applications.
            // Also, since compounding discounts will be at the end of the remaining applications, we know that no discounts other than the current one can 
            // be eliminated by applying this discount (since a compounding discount cannot exclude any other compounding discount from applying.
            // The match needs to be on offer ID to prevent a compound discount that has multiple tiers (quantity, etc.) from applying more than once.
            AppliedDiscountApplication[] currentApplications = appliedDiscountsStack.ToArray();

            for (int remainingIndex = nextIndex; remainingIndex < possibleDiscountApplications.Length; remainingIndex++)
            {
                decimal[] remainingQuantitiesAfterDiscount = (decimal[])remainingQuantities.Clone();

                for (int x = 0; x < currentApplications.Length; x++)
                {
                    if (remainingApplications[remainingIndex] && !currentApplications[x].DiscountApplication.CanCompound(possibleDiscountApplications[remainingIndex]))
                    {
                        for (int y = 0; y < remainingQuantitiesAfterDiscount.Length; y++)
                        {
                            remainingQuantitiesAfterDiscount[y] -= currentApplications[x].ItemQuantities[y];
                            if (remainingQuantitiesAfterDiscount[y] < possibleDiscountApplications[remainingIndex].ItemQuantities[y])
                            {
                                remainingApplications[remainingIndex] = false;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the remaining possible applications after using a non-compounding discount.
        /// </summary>
        /// <param name="possibleDiscountApplications">The possible discount applications.</param>
        /// <param name="newAppliedDiscountApplication">The newly applied discount application.</param>
        /// <param name="nextIndex">The next index in the possible applications to consider.</param>
        /// <param name="remainingQuantities">The remaining quantities of items.</param>
        /// <param name="remainingApplications">The remaining applications that are possible.</param>
        /// <param name="availableItemDiscounts">The discounts that are available for each discountable item group.</param>
        private static void GetRemainingApplicationsForNonCompoundedDiscount(DiscountApplication[] possibleDiscountApplications, AppliedDiscountApplication newAppliedDiscountApplication, int nextIndex, decimal[] remainingQuantities, ref BitSet remainingApplications, BitSet[] availableItemDiscounts)
        {
            for (int x = 0; x < remainingQuantities.Length; x++)
            {
                remainingQuantities[x] -= newAppliedDiscountApplication.ItemQuantities[x];

                // As a slight performance benefit, if this remaining quantity went to zero, remove any remaining discount applications that needed this item
                if (remainingQuantities[x] == 0M && possibleDiscountApplications[nextIndex].ItemQuantities[x] != 0M)
                {
                    remainingApplications = remainingApplications.And(availableItemDiscounts[x].Not());
                }
            }

            // Now remove any remaining discount applications that can no longer be applied
            for (int x = 0; x < remainingApplications.Length; x++)
            {
                if (remainingApplications[x])
                {
                    for (int y = 0; y < possibleDiscountApplications[x].ItemQuantities.Length; y++)
                    {
                        if (remainingQuantities[y] < possibleDiscountApplications[x].ItemQuantities[y])
                        {
                            // This discount can't be applied
                            remainingApplications[x] = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Throws error and logs the invalid item Ids on the transaction.
        /// </summary>
        /// <param name="invalidItemIds">Item Ids for items not in the database.</param>
        /// <exception cref="DataValidationException">Thrown if the invalid item Ids are non-empty.</exception>
        private static void ThrowErrorForInvalidItems(IEnumerable<string> invalidItemIds)
        {
            if (invalidItemIds.Any())
            {
                var ex = new DataValidationException(
                    DataValidationErrors.RequiredValueNotFound,
                    "Sales line item could not be found. Try running product jobs and ensure item is assorted to current channel. See tracing log for detailed errors.");

                var formattedIds = invalidItemIds.Aggregate(
                    new StringBuilder("Items not found for item ids on transaction: "),
                    (sb, id) => sb.AppendFormat(System.Globalization.CultureInfo.InvariantCulture, "ItemId: {0}; ", id),
                    sb => sb.ToString());

                NetTracer.Error(ex, formattedIds);

                throw ex;
            }
        }

        /// <summary>
        /// Determines if all elements of two decimal arrays are equal.
        /// </summary>
        /// <param name="p1">The first array.</param>
        /// <param name="p2">The second array.</param>
        /// <returns>True if the two arrays are equal, false otherwise.</returns>
        private static bool DecimalArraysAreEqual(decimal[] p1, decimal[] p2)
        {
            if (p1 == null)
            {
                return p2 == null;
            }

            if (p2 == null)
            {
                return false;
            }

            if (p1.Length != p2.Length)
            {
                return false;
            }

            for (int x = 0; x < p1.Length; x++)
            {
                if (p1[x] != p2[x])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Applies any non-overlapping discounts to the best-discount path.  
        /// A non-overlapping discount is one where applying the discount will not prevent any other discount from being applied,
        /// and therefore must be included in the best-discount path.
        /// </summary>
        /// <param name="discountableItemGroups">The discountable items on the transaction.</param>
        /// <param name="appliedDiscountsStack">The previously applied discounts.</param>
        /// <param name="remainingQuantities">The remaining quantities of each item in the discountableItemGroups parameter.</param>
        /// <param name="availableItemDiscounts">The available discounts for each item.</param>
        /// <param name="remainingApplications">The remaining discount applications.</param>
        /// <param name="availableDiscountApplications">The available discount applications.</param>
        /// <param name="bestDiscountPath">The current best discount path.</param>
        /// <param name="bestDiscountValue">The current best discount value.</param>
        private void ApplyNonOverlappingDiscounts(DiscountableItemGroup[] discountableItemGroups, Stack<AppliedDiscountApplication> appliedDiscountsStack, decimal[] remainingQuantities, BitSet[] availableItemDiscounts, BitSet remainingApplications, DiscountApplication[] availableDiscountApplications, ref List<AppliedDiscountApplication> bestDiscountPath, ref decimal bestDiscountValue)
        {
            if (availableDiscountApplications.Any(p => p.Discount.PeriodicDiscountType == PeriodicDiscountOfferType.Threshold))
            {
                return;
            }

            for (int x = 0; x < discountableItemGroups.Length; x++)
            {
                if (availableItemDiscounts[x].GetNumberOfNonzeroBits() == 1)
                {
                    int discountIndex = availableItemDiscounts[x].GetFirstNonZeroBit();

                    // Check all remaining lines to see if this discount can also apply there and if another discount applies.
                    bool canApply = true;
                    for (int y = 0; y < discountableItemGroups.Length; y++)
                    {
                        if (y != x && availableItemDiscounts[y][discountIndex] &&
                            availableItemDiscounts[y].GetNumberOfNonzeroBits() > 1)
                        {
                            canApply = false;
                            break;
                        }
                    }

                    if (canApply)
                    {
                        remainingApplications[discountIndex] = false;

                        bool keepGoing = true;

                        // Determine how many times the discount could be applied here.
                        while (keepGoing && DiscountBase.IsDecimalArrayASubset(availableDiscountApplications[discountIndex].ItemQuantities, remainingQuantities))
                        {
                            AppliedDiscountApplication newAppliedDiscountApplication = availableDiscountApplications[discountIndex].Apply(discountableItemGroups, remainingQuantities, appliedDiscountsStack, this.priceContext, hasCompetingApplications: false);
                            if (newAppliedDiscountApplication != null)
                            {
                                bestDiscountValue += newAppliedDiscountApplication.Value;
                                bestDiscountPath.Add(newAppliedDiscountApplication);
                                appliedDiscountsStack.Push(newAppliedDiscountApplication);
                                for (int z = 0; z < remainingQuantities.Length; z++)
                                {
                                    remainingQuantities[z] -= newAppliedDiscountApplication.ItemQuantities[z];
                                }
                            }
                            else
                            {
                                keepGoing = false;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Calculates the best path for application of the available discounts.
        /// </summary>
        /// <param name="discountableItemGroups">The discountable items on the transaction.</param>
        /// <param name="appliedDiscountsStack">The previously applied discounts.</param>
        /// <param name="discountApplicationStateStack">The stack containing state information for the current and parent nodes of the best-price determination.</param>
        /// <param name="bestDiscountPath">The current best discount path.</param>
        /// <param name="bestDiscountValue">The current best discount value.</param>
        /// <param name="possibleDiscountApplications">The set of possible discount applications.</param>
        /// <param name="nextIndex">The next index of the available discounts to consider.</param>
        /// <param name="discountValue">The current value of this path.</param>
        /// <param name="remainingQuantities">The remaining quantities of each item in the discountableItemGroups parameter.</param>
        /// <param name="remainingApplications">The remaining discount applications.</param>
        /// <param name="availableItemDiscounts">The available discounts for each item.</param>
        private void CalculateBestDiscountApplications(DiscountableItemGroup[] discountableItemGroups, Stack<AppliedDiscountApplication> appliedDiscountsStack, Stack<DiscountApplicationState> discountApplicationStateStack, ref List<AppliedDiscountApplication> bestDiscountPath, ref decimal bestDiscountValue, DiscountApplication[] possibleDiscountApplications, int nextIndex, decimal discountValue, decimal[] remainingQuantities, ref BitSet remainingApplications, BitSet[] availableItemDiscounts)
        {
#if DEBUG
            DateTime startDateTime = DateTime.UtcNow;
#endif

            int numberOfTimesApplied = 1;

            decimal bestDiscountValueHere = decimal.Zero;

            while (nextIndex != InvalidIndex)
            {
                // Get the value of this application.  If the value is zero (threshold discounts could do this), do not apply this discount, move to the next index.
                AppliedDiscountApplication appliedDiscountApplication = possibleDiscountApplications[nextIndex].Apply(discountableItemGroups, remainingQuantities, appliedDiscountsStack, this.priceContext, hasCompetingApplications: true);

                if (appliedDiscountApplication == null)
                {
                    remainingApplications[nextIndex] = false;
                }
                else
                {
                    decimal thisAmount = appliedDiscountApplication.Value;

                    // Before we start processing a discount, we need to save the current state for when we pop back up.
                    discountApplicationStateStack.Push(new DiscountApplicationState()
                    {
                        Value = discountValue,
                        AppliedDiscountApplication = nextIndex,
                        RemainingApplications = new BitSet(remainingApplications),
                        RemainingQuantities = (decimal[])remainingQuantities.Clone(),
                        NumberOfTimesApplied = numberOfTimesApplied
                    });

                    // Start processing this specific discount application.
                    discountValue += thisAmount;

                    // Add the application to the applied discounts stack
                    appliedDiscountsStack.Push(appliedDiscountApplication);

                    // Remove the quantities affected by this discount from the cart.
                    if (possibleDiscountApplications[nextIndex].Discount.ConcurrencyMode != ConcurrencyMode.Compounded)
                    {
                        GetRemainingApplicationsForNonCompoundedDiscount(possibleDiscountApplications, appliedDiscountApplication, nextIndex, remainingQuantities, ref remainingApplications, availableItemDiscounts);
                    }
                    else
                    {
                        GetRemainingApplicationsForCompoundedDiscount(appliedDiscountsStack, possibleDiscountApplications, nextIndex, remainingQuantities, ref remainingApplications);
                    }

                    // If applying the current discount again would be more than the number of times this discount can be applied, 
                    // the current discount should be removed from the possible discounts.
                    if (possibleDiscountApplications[nextIndex].NumberOfTimesApplicable != DiscountBase.UnlimitedNumberOfTimesApplicable
                        && (numberOfTimesApplied + 1) > possibleDiscountApplications[nextIndex].NumberOfTimesApplicable)
                    {
                        remainingApplications[nextIndex] = false;
                    }

#if DEBUG
                    this.WriteDebugInfoForPath(DebugLogHeader.Step, thisAmount, discountValue, appliedDiscountsStack, discountApplicationStateStack, nextIndex);
#endif
                }

                // We now have applied this discount and have determined which other discounts can apply (including itself), so we should continue processing those.
                // If none are left, then we should check the value against the previous best-known value and replace it if needed.
                bool discountsRemain = false;
                int newIndex = InvalidIndex;

                // Find the next available discount.
                for (int x = nextIndex; x < remainingApplications.Length; x++)
                {
                    if (remainingApplications[x])
                    {
                        newIndex = x;
                        discountsRemain = true;
                        break;
                    }
                }

                if (discountsRemain)
                {
                    // Reset the next index to the next available application found in the step above.
                    if (nextIndex == newIndex)
                    {
                        numberOfTimesApplied++;
                    }
                    else
                    {
                        nextIndex = newIndex;
                        numberOfTimesApplied = 1;
                    }
                }
                else
                {
#if DEBUG
                    this.WriteDebugInfoForPath(DebugLogHeader.Path, 0, discountValue, appliedDiscountsStack, discountApplicationStateStack, nextIndex);
#endif

                    // No remaining discounts to process, check the value of this path and pop back up.
                    if (discountValue > bestDiscountValueHere)
                    {
                        bestDiscountValueHere = discountValue;
                        bestDiscountPath = appliedDiscountsStack.Reverse().ToList();
                    }

                    // Set the nextIndex, just in case we will not be popping back up (this will be -1 in that case).
                    nextIndex = newIndex;

                    while (discountApplicationStateStack.Count > 0 && newIndex == InvalidIndex)
                    {
                        DiscountApplicationState discountApplicationState = discountApplicationStateStack.Pop();
                        appliedDiscountsStack.Pop();
                        remainingQuantities = discountApplicationState.RemainingQuantities;
                        remainingApplications = discountApplicationState.RemainingApplications;
                        discountValue = discountApplicationState.Value;

                        // All paths for that index should have been covered to this point, start at the next index value (otherwise this would be an infinite loop).
                        // If this index is a compounding discount, continue up the stack, since there are no alternate paths with compounding discounts
                        // except for the possibility of the same offer ID for compounding having multiple possible applications.
                        if (possibleDiscountApplications[discountApplicationState.AppliedDiscountApplication].Discount.ConcurrencyMode != ConcurrencyMode.Compounded
                            || possibleDiscountApplications.Skip(discountApplicationState.AppliedDiscountApplication + 1).Any(p => !p.CanCompound(possibleDiscountApplications[discountApplicationState.AppliedDiscountApplication]))
                            || possibleDiscountApplications[discountApplicationState.AppliedDiscountApplication].Discount.PeriodicDiscountType == PeriodicDiscountOfferType.Threshold)
                        {
                            for (int x = discountApplicationState.AppliedDiscountApplication + 1; x < remainingApplications.Length; x++)
                            {
                                if (remainingApplications[x])
                                {
                                    newIndex = x;
                                    break;
                                }
                            }
                        }

                        nextIndex = newIndex;
                    }
                }
            }

            bestDiscountValue += bestDiscountValueHere;

#if DEBUG
            this.WriteDebugInfoSummary(startDateTime);
#endif
        }

        /// <summary>
        /// Calculates and applies the best path for discounts on a transaction and a set of discountable item groups.
        /// </summary>
        /// <param name="transaction">The current transaction to calculate discounts for.</param>
        /// <param name="discountableItemGroups">The groups of discountable sales lines on the transaction.</param>
        /// <param name="isReturn">True if it's return.</param>
        private void CalculateAndApplyBestDiscounts(SalesTransaction transaction, DiscountableItemGroup[] discountableItemGroups, bool isReturn)
        {
            Stack<AppliedDiscountApplication> appliedDiscountsStack = new Stack<AppliedDiscountApplication>();
            Stack<DiscountApplicationState> discountApplicationStateStack = new Stack<DiscountApplicationState>();
            List<AppliedDiscountApplication> bestDiscountPath = new List<AppliedDiscountApplication>();
            decimal bestDiscountValue = 0.0M;
            decimal discountValue = 0.0M;
            decimal[] remainingQuantities = discountableItemGroups.Select(p => p.Quantity).ToArray();

            // Start with the exclusive discounts.
            this.CalculateExclusiveDiscounts(transaction, discountableItemGroups, appliedDiscountsStack, discountApplicationStateStack, ref bestDiscountPath, ref bestDiscountValue, discountValue, remainingQuantities, isReturn);

            // Need to add the best-path exclusive discounts here first, and fix up the remaining quantities before we do the non-exclusive ones.
            discountValue = bestDiscountValue;
            appliedDiscountsStack.Clear();

            remainingQuantities = discountableItemGroups.Select(p => p.Quantity).ToArray();

            foreach (AppliedDiscountApplication exclusiveDiscount in bestDiscountPath)
            {
                appliedDiscountsStack.Push(exclusiveDiscount);
                for (int x = 0; x < remainingQuantities.Length; x++)
                {
                    remainingQuantities[x] -= exclusiveDiscount.DiscountApplication.ItemQuantities[x];
                }
            }

            // Now handle the non-exclusive discounts
            this.CalculateNonExclusiveDiscounts(transaction, discountableItemGroups, appliedDiscountsStack, discountApplicationStateStack, ref bestDiscountPath, ref bestDiscountValue, discountValue, remainingQuantities, isReturn);

            // If we found something, we now need to apply it
            if (bestDiscountValue > 0M)
            {
                // Reset the remaining quantities, Threshold discounts will use those to determine the value.
                remainingQuantities = discountableItemGroups.Select(p => p.Quantity).ToArray();

                Stack<AppliedDiscountApplication> appliedDiscounts = new Stack<AppliedDiscountApplication>();
                foreach (AppliedDiscountApplication currentApplied in bestDiscountPath)
                {
                    currentApplied.Apply(discountableItemGroups, this.priceContext);
                    appliedDiscounts.Push(currentApplied);
                }
            }

            //NS Hardik Bug : 80575 : Barstool Problem
            //Add for Stop Split item issue............
            UpdateDiscountGroupStopSplit(transaction, discountableItemGroups);
            //NE Hardik Bug : 80575 : Barstool Problem

            // Apply discount lines to the groups, which may require splitting line items.
            foreach (DiscountableItemGroup group in discountableItemGroups)
            {
                // Loop through the DiscountLineQuantities to apply the proper number, split compound and non-compound discounts.
                group.ApplyDiscountLines(transaction, isReturn);
            }
        }

        //NS Hardik Bug : 80575 : Barstool Problem
        #region Customization for stop Split in Multiple Qty configuration
        // Update groups qty so it will not split line for fonfigured items.
        private void UpdateDiscountGroupStopSplit(SalesTransaction transaction, DiscountableItemGroup[] discountableItemGroups)
        {
            foreach (DiscountableItemGroup group in discountableItemGroups)
            {
                string information = string.Empty;
                if (group.DiscountLineQuantities.Count() > 0 && group.Count != 0) // Check sales line available for Discount 
                {
                    if (Convert.ToInt32(group[0]["AFMMultipleQty"]) > 0)//Check mulitple Qty is configured for that item 
                    {
                        var preofferId = string.Empty;
                        foreach (var discountLine in group.DiscountLineQuantities.OrderBy(temp => temp.DiscountLine.OfferId).OrderByDescending(temp => temp.Quantity))
                        {
                            if (group.Quantity != discountLine.Quantity)
                            {
                                if (discountLine.DiscountLine.Amount != 0)//Check Discount Amount is not zero for line discount
                                {
                                    if (discountLine.DiscountLine.OfferId != preofferId)
                                    {
                                        decimal totalDiscountQty = group.DiscountLineQuantities.Where(temp => temp.DiscountLine.OfferId == discountLine.DiscountLine.OfferId).Sum(temp => temp.Quantity);
                                        decimal totaldiscount = group.DiscountLineQuantities.Where(temp => temp.DiscountLine.OfferId == discountLine.DiscountLine.OfferId).Sum(temp => (temp.Quantity * temp.DiscountLine.Amount));
                                        decimal unitDiscountAmount = this.priceContext.Rounding((totaldiscount / group.Quantity));
                                        while (this.priceContext.Rounding(unitDiscountAmount * group.Quantity) < totaldiscount)
                                        {
                                            unitDiscountAmount += 0.01m;
                                        }
                                        preofferId = discountLine.DiscountLine.OfferId;
                                        discountLine.Quantity = group.Quantity;
                                        if (unitDiscountAmount == decimal.Zero)
                                            discountLine.DiscountLine.Amount = 0.01m;
                                        else
                                            discountLine.DiscountLine.Amount = unitDiscountAmount;
                                        information += " From " + this.priceContext.Rounding(totaldiscount) + " to " + this.priceContext.Rounding(unitDiscountAmount * group.Quantity);
                                    }
                                    else
                                    {
                                        discountLine.Quantity = 0;
                                    }
                                }
                                else if (discountLine.DiscountLine.Percentage != 0)
                                {
                                    if (discountLine.DiscountLine.OfferId != preofferId)
                                    {
                                        var oldQty = discountLine.Quantity;
                                        discountLine.Quantity = group.Quantity;
                                        information += " From " + oldQty + " to " + group.Quantity;
                                    }
                                    else
                                    {
                                        discountLine.Quantity = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(information))
                {
                    information = Convert.ToString(transaction["AFMReasonCodeSplitDescription"]) + information;
                    AddResonCodetoSalesLine(group, transaction, information);
                }
            }
        }

        private void AddResonCodetoSalesLine(DiscountableItemGroup group, SalesTransaction transaction, string information)
        {
            string reasonCodeId = Convert.ToString(transaction["AFMReasonCodeSplit"]);
            if (!string.IsNullOrEmpty(reasonCodeId))
            {
                for (int i = 0; i < group.Count; i++)
                {
                    ReasonCodeLine reasonCode = new ReasonCodeLine()
                    {
                        LineType = ReasonCodeLineType.Sales,
                        SourceCode = group[i].ItemId,
                        //LineTypeValue = Convert.ToInt32(group[i].LineNumber),
                        TransactionId = transaction.Id,
                        LineId = group[0].LineId,
                        Information = information,
                        ReasonCodeId = reasonCodeId//TODO: update this value from new Custom Paramter
                    };
                    var existingResoncodeLine = group[0].ReasonCodeLines.FirstOrDefault(tmpline => tmpline.ReasonCodeId == reasonCodeId);
                    if (existingResoncodeLine != null)
                        group[0].ReasonCodeLines.Remove(existingResoncodeLine);
                    group[0].ReasonCodeLines.Add(reasonCode);
                }
            }
        }
        #endregion
        //NE Hardik Bug : 80575 : Barstool Problem


        /// <summary>
        /// Gets the groups of discountable items present on the transaction, grouped by product and variant identifier.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <returns>The discountable item groups.</returns>
        private DiscountableItemGroup[] GetDiscountableItems(SalesTransaction transaction)
        {
            Dictionary<string, DiscountableItemGroup> items = new Dictionary<string, DiscountableItemGroup>(StringComparer.OrdinalIgnoreCase);
            var itemIds = PriceContextHelper.GetItemIds(transaction);

            if (itemIds.Count != this.priceContext.ItemCache.Count)
            {
                var missingItemIds = itemIds.Where(id => !this.priceContext.ItemCache.ContainsKey(id));
                ThrowErrorForInvalidItems(missingItemIds);
            }

            foreach (SalesLine line in transaction.PriceCalculableSalesLines)
            {
                string key = string.Concat(line.Price, KeyDelimiter, line.ItemId, KeyDelimiter, line.ProductId, KeyDelimiter, line.SalesOrderUnitOfMeasure);

                if (items.ContainsKey(key))
                {
                    items[key].Add(line);
                }
                else
                {
                    items.Add(key, new DiscountableItemGroup(line, this.priceContext) { ExtendedProperties = PriceContextHelper.GetItem(this.priceContext, line.ItemId) });
                }
            }

            return items.Values.ToArray();
        }

        /// <summary>
        /// Initializes the internal variables that hold all of the possible discounts.
        /// </summary>
        /// <param name="transaction">The sales transaction to initialize discounts for.</param>
        private void InitializeDiscounts(SalesTransaction transaction)
        {
            if (this.enableCache)
            {
                this.itemDiscounts = RetailDiscountStore.GetProductOrVarintToDiscountMapFromCache(this.pricingDataManager, this.priceContext, transaction);
            }
            else
            {
                this.itemDiscounts = RetailDiscountStore.GetProductOrVariantToDiscontMapLive(transaction, this.priceContext, this.pricingDataManager);
            }
        }

        /// <summary>
        /// Calculates the non-exclusive (best-price and compounding) discounts for a transaction and discountable item groups.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <param name="discountableItemGroups">The discountable items on the transaction.</param>
        /// <param name="appliedDiscountsStack">The previously applied discounts.</param>
        /// <param name="discountApplicationStateStack">The stack containing state information for the current and parent nodes of the best-price determination.</param>
        /// <param name="bestDiscountPath">The current best discount path.</param>
        /// <param name="bestDiscountValue">The current best discount value.</param>
        /// <param name="discountValue">The current value of this path.</param>
        /// <param name="remainingQuantities">The remaining quantities of each item in the discountableItemGroups parameter.</param>
        /// <param name="isReturn">Flag indicating whether it's for return.</param>
        private void CalculateNonExclusiveDiscounts(
            SalesTransaction transaction,
            DiscountableItemGroup[] discountableItemGroups,
            Stack<AppliedDiscountApplication> appliedDiscountsStack,
            Stack<DiscountApplicationState> discountApplicationStateStack,
            ref List<AppliedDiscountApplication> bestDiscountPath,
            ref decimal bestDiscountValue,
            decimal discountValue,
            decimal[] remainingQuantities,
            bool isReturn)
        {
            DiscountApplication[] nonExclusiveDiscountApplications = this.GetPossibleDiscountsForLineItems(discountableItemGroups, remainingQuantities)
                                                                        .Where(p => p.ConcurrencyMode != ConcurrencyMode.Exclusive)
                                                                        .SelectMany(p => p.GetDiscountApplications(transaction, discountableItemGroups, remainingQuantities, this.storePriceGroups, this.currencyCode, this.pricingDataManager, isReturn, this.priceContext))
                                                                        .OrderBy(p => p.Discount.ConcurrencyMode)
                                                                        .ThenByDescending(p => p.SortIndex)
                                                                        .ThenByDescending(p => p.SortValue)
                                                                        .ToArray();
#if DEBUG
            WriteDebugInfoForDiscountApplications("NonExclusive", nonExclusiveDiscountApplications, discountableItemGroups);
#endif

            if (nonExclusiveDiscountApplications.Length > 0)
            {
                int nextIndex = 0;
                BitSet remainingApplications = new BitSet((uint)nonExclusiveDiscountApplications.Length, true);
                BitSet[] availableItemDiscounts = new BitSet[discountableItemGroups.Length];

                // Populate the listing of discounts applications that require each specific item
                for (int x = 0; x < discountableItemGroups.Length; x++)
                {
                    availableItemDiscounts[x] = new BitSet(nonExclusiveDiscountApplications.Select(p => p.ItemQuantities[x] != 0M).ToArray());
                }

                // Reduce the discounts to only the best for each target group
                this.ReduceAvailableDiscounts(discountableItemGroups, appliedDiscountsStack, remainingQuantities, availableItemDiscounts, nonExclusiveDiscountApplications, remainingApplications);

                // Apply discounts that do not overlap with other discounts
                this.ApplyNonOverlappingDiscounts(discountableItemGroups, appliedDiscountsStack, remainingQuantities, availableItemDiscounts, remainingApplications, nonExclusiveDiscountApplications, ref bestDiscountPath, ref bestDiscountValue);

                // Reset the next index in case the first discount was non-overlapping.
                nextIndex = remainingApplications.GetFirstNonZeroBit();

                // Get the best path within the exclusive discounts
                this.CalculateBestDiscountApplications(discountableItemGroups, appliedDiscountsStack, discountApplicationStateStack, ref bestDiscountPath, ref bestDiscountValue, nonExclusiveDiscountApplications, nextIndex, discountValue, remainingQuantities, ref remainingApplications, availableItemDiscounts);
            }
        }

        /// <summary>
        /// Calculates the exclusive discounts for a transaction and discountable item groups.
        /// </summary>
        /// <param name="transaction">The current transaction.</param>
        /// <param name="discountableItemGroups">The discountable items on the transaction.</param>
        /// <param name="appliedDiscountsStack">The previously applied discounts.</param>
        /// <param name="discountApplicationStateStack">The stack containing state information for the current and parent nodes of the best-price determination.</param>
        /// <param name="bestDiscountPath">The current best discount path.</param>
        /// <param name="bestDiscountValue">The current best discount value.</param>
        /// <param name="discountValue">The current value of this path.</param>
        /// <param name="remainingQuantities">The remaining quantities of each item in the discountableItemGroups parameter.</param>
        /// <param name="isReturn">Flag indicating whether it's for return.</param>
        private void CalculateExclusiveDiscounts(
            SalesTransaction transaction,
            DiscountableItemGroup[] discountableItemGroups,
            Stack<AppliedDiscountApplication> appliedDiscountsStack,
            Stack<DiscountApplicationState> discountApplicationStateStack,
            ref List<AppliedDiscountApplication> bestDiscountPath,
            ref decimal bestDiscountValue,
            decimal discountValue,
            decimal[] remainingQuantities,
            bool isReturn)
        {
            DiscountApplication[] exclusiveDiscountApplications = this.GetPossibleDiscountsForLineItems(discountableItemGroups, remainingQuantities)
                                                                    .Where(p => p.ConcurrencyMode == ConcurrencyMode.Exclusive)
                                                                    .SelectMany(p => p.GetDiscountApplications(transaction, discountableItemGroups, remainingQuantities, this.storePriceGroups, this.currencyCode, this.pricingDataManager, isReturn, this.priceContext))
                                                                    .OrderByDescending(p => p.SortIndex)
                                                                    .ThenByDescending(p => p.SortValue)
                                                                    .ToArray();
#if DEBUG
            WriteDebugInfoForDiscountApplications("Exclusive", exclusiveDiscountApplications, discountableItemGroups);
#endif

            // If any exist, start processing with the first one
            if (exclusiveDiscountApplications.Length > 0)
            {
                int nextIndex = 0;
                BitSet remainingApplications = new BitSet((uint)exclusiveDiscountApplications.Length, true);
                BitSet[] availableItemDiscounts = new BitSet[discountableItemGroups.Length];

                // Populate the listing of discounts applications that require each specific item
                for (int x = 0; x < discountableItemGroups.Length; x++)
                {
                    availableItemDiscounts[x] = new BitSet(exclusiveDiscountApplications.Select(p => p.ItemQuantities[x] != 0M).ToArray());
                }

                // Reduce the discounts to only the best for each target group
                this.ReduceAvailableDiscounts(discountableItemGroups, appliedDiscountsStack, remainingQuantities, availableItemDiscounts, exclusiveDiscountApplications, remainingApplications);

                // Apply discounts that do not overlap with other discounts
                this.ApplyNonOverlappingDiscounts(discountableItemGroups, appliedDiscountsStack, remainingQuantities, availableItemDiscounts, remainingApplications, exclusiveDiscountApplications, ref bestDiscountPath, ref bestDiscountValue);

                // Reset the next index in case the first discount was non-overlapping.
                nextIndex = remainingApplications.GetFirstNonZeroBit();

                // Get the best path within the exclusive discounts
                this.CalculateBestDiscountApplications(discountableItemGroups, appliedDiscountsStack, discountApplicationStateStack, ref bestDiscountPath, ref bestDiscountValue, exclusiveDiscountApplications, nextIndex, discountValue, remainingQuantities, ref remainingApplications, availableItemDiscounts);
            }
        }

        /// <summary>
        /// Reduces the available discounts by eliminating any discounts that apply to the same quantities of items as another discount but have a lower value.
        /// </summary>
        /// <param name="discountableItemGroups">The discountable items on the transaction.</param>
        /// <param name="appliedDiscountsStack">The previously applied discounts.</param>
        /// <param name="remainingQuantities">The remaining quantities of each item in the discountableItemGroups parameter.</param>
        /// <param name="availableItemDiscounts">The set of available discounts for each item group.</param>
        /// <param name="availableDiscountApplications">The available discounts.</param>
        /// <param name="remainingApplications">The remaining available discount applications.</param>
        private void ReduceAvailableDiscounts(DiscountableItemGroup[] discountableItemGroups, Stack<AppliedDiscountApplication> appliedDiscountsStack, decimal[] remainingQuantities, BitSet[] availableItemDiscounts, DiscountApplication[] availableDiscountApplications, BitSet remainingApplications)
        {
            for (int x = 0; x < discountableItemGroups.Length; x++)
            {
                int numberOfDiscounts = availableItemDiscounts[x].GetNumberOfNonzeroBits();
                if (numberOfDiscounts > 1)
                {
                    int y = availableItemDiscounts[x].GetFirstNonZeroBit();

                    while (y != BitSet.UnknownBit)
                    {
                        int z = availableItemDiscounts[x].GetNextNonZeroBit(y + 1);

                        while (z != BitSet.UnknownBit)
                        {
                            // If the item quantities that each discount applies to are the same, they affect the same items and should be reduced.
                            if (DecimalArraysAreEqual(availableDiscountApplications[y].ItemQuantities, availableDiscountApplications[z].ItemQuantities)
                                && availableDiscountApplications[y].Discount.ConcurrencyMode != ConcurrencyMode.Compounded
                                && availableDiscountApplications[z].Discount.ConcurrencyMode != ConcurrencyMode.Compounded)
                            {
                                // If they are the same, keep the first one with the highest value.
                                int discountToRemove = z;

                                AppliedDiscountApplication possibleApplied = availableDiscountApplications[y].Apply(discountableItemGroups, remainingQuantities, appliedDiscountsStack, this.priceContext, hasCompetingApplications: true);
                                AppliedDiscountApplication currentApplied = availableDiscountApplications[z].Apply(discountableItemGroups, remainingQuantities, appliedDiscountsStack, this.priceContext, hasCompetingApplications: true);

                                if (possibleApplied == null || (currentApplied != null && possibleApplied.Value < currentApplied.Value))
                                {
                                    discountToRemove = y;
                                }

                                // Remove the selected discount from the available discounts overall and by item.
                                remainingApplications[discountToRemove] = false;

                                foreach (var availableItemDiscount in availableItemDiscounts)
                                {
                                    availableItemDiscount[discountToRemove] = false;
                                }
                            }

                            z = availableItemDiscounts[x].GetNextNonZeroBit(z + 1);
                        }

                        y = availableItemDiscounts[x].GetNextNonZeroBit(y + 1);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the discounts that could apply to the discountable item groups.  Note that this does not the ones that can be applied, only those that
        /// could possibly apply based on having at least one required item on the transaction.
        /// </summary>
        /// <param name="discountableItemGroups">The discountable item groups for the transaction.</param>
        /// <param name="remainingQuantities">The remaining quantities of each item group.</param>
        /// <returns>The collection of discounts that may have applications for this transaction.</returns>
        private IEnumerable<DiscountBase> GetPossibleDiscountsForLineItems(DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities)
        {
            HashSet<string> usedOfferIds = new HashSet<string>();

            for (int x = 0; x < discountableItemGroups.Length; x++)
            {
                if (remainingQuantities[x] != 0M)
                {
                    DiscountableItemGroup item = discountableItemGroups[x];

                    if (this.itemDiscounts.ContainsKey(item.ProductId))
                    {
                        foreach (DiscountBase discount in this.itemDiscounts[item.ProductId].Where(p => !usedOfferIds.Contains(p.OfferId)))
                        {
                            usedOfferIds.Add(discount.OfferId);
                            yield return discount;
                        }
                    }

                    if (item.ProductId != item.MasterProductId && this.itemDiscounts.ContainsKey(item.MasterProductId))
                    {
                        foreach (DiscountBase discount in this.itemDiscounts[item.MasterProductId].Where(p => !usedOfferIds.Contains(p.OfferId)))
                        {
                            usedOfferIds.Add(discount.OfferId);
                            yield return discount;
                        }
                    }
                }
            }
        }

#if DEBUG
        private void WriteDebugInfoSummary(DateTime startDateTime)
        {
            Debug.WriteLine("Starts [{0:MM/dd/yyyy hh:mm:ss.fff}] Ends [{1:MM/dd/yyyy hh:mm:ss.fff}] Paths {2} Steps {3}.", startDateTime, DateTime.UtcNow, this.pathCount, this.stepCount);
        }

        private void WriteDebugInfoForPath(
            DebugLogHeader header,
            decimal newAmount,
            decimal discountValue,
            Stack<AppliedDiscountApplication> appliedDiscountsStack,
            Stack<DiscountApplicationState> discountApplicationStateStack,
            int nextIndex = 0)
        {
            if (header == DebugLogHeader.Path)
            {
                this.pathCount++;
            }
            else
            {
                this.stepCount++;
            }

            if (logDiscountDetails)
            {
                Debug.WriteLine("[{0}] next index {3} got value of {1} with new amount {2} for path: ", header, discountValue, newAmount, nextIndex);

                List<AppliedDiscountApplication> appliedDiscounts = appliedDiscountsStack.ToList();
                appliedDiscounts.Reverse();

                foreach (AppliedDiscountApplication app in appliedDiscounts)
                {
                    Debug.WriteLine(string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0} deal [{1:0.##}] $ off [{2:0.##}] % off [{3:0.##}] sort [{4}] v [{5:0.##}] on ", app.DiscountApplication.Discount.OfferId, app.DiscountApplication.DealPriceValue, app.DiscountApplication.DiscountAmountValue, app.DiscountApplication.DiscountPercentValue, app.DiscountApplication.SortIndex, app.DiscountApplication.SortValue));

                    for (int x = 0; x < app.DiscountApplication.ItemQuantities.Length; x++)
                    {
                        if (app.DiscountApplication.ItemQuantities[x] != 0M)
                        {
                            Debug.WriteLine("  " + app.ItemQuantities[x] + "x" + x + ", ");
                        }
                    }

                    Debug.WriteLine(string.Empty);
                }

                Debug.WriteLine("Application state stack: ");

                List<DiscountApplicationState> applicationStates = discountApplicationStateStack.ToList();
                applicationStates.Reverse();

                foreach (DiscountApplicationState applicationState in applicationStates)
                {
                    Debug.WriteLine(string.Format(System.Globalization.CultureInfo.InvariantCulture, "  App [{0}], # [{1}], v [{2:0.##}], r q: ", applicationState.AppliedDiscountApplication, applicationState.NumberOfTimesApplied, applicationState.Value));
                    for (int x = 0; x < applicationState.RemainingQuantities.Length; x++)
                    {
                        if (applicationState.RemainingQuantities[x] != 0M)
                        {
                            Debug.WriteLine("  " + applicationState.RemainingQuantities[x] + "x" + x + ", ");
                        }
                    }

                    Debug.WriteLine(string.Empty);
                }
            }
        }
#endif
    }
}
