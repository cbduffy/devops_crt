﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine.DiscountData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Class containing groupings of SalesLine objects so that identical items that are on multiple lines can be considered as a single line.
    /// </summary>
    public class DiscountableItemGroup
    {
        private List<SalesLine> salesLines;
        private List<DiscountLineQuantity> discountLineQuantities;
        private PriceContext priceContext;

        /// <summary>
        /// Initializes a new instance of the DiscountableItemGroup class with the specified SalesLine object included.
        /// </summary>
        /// <param name="line">The line item to add.</param>
        /// <param name="priceContext">Price context.</param>
        public DiscountableItemGroup(SalesLine line, PriceContext priceContext)
            : this(priceContext)
        {
            this.Add(line);
        }

        /// <summary>
        /// Initializes a new instance of the DiscountableItemGroup class with the specified SalesLine object included.
        /// </summary>
        /// <param name="lines">The line items to add.</param>
        /// <param name="priceContext">Price context.</param>
        public DiscountableItemGroup(IEnumerable<SalesLine> lines, PriceContext priceContext)
            : this(priceContext)
        {
            this.salesLines = new List<SalesLine>(lines);
            this.Quantity += this.salesLines.Sum(p => p.Quantity);
        }

        /// <summary>
        /// Initializes a new instance of the DiscountableItemGroup class with no SalesLine items included.
        /// </summary>
        /// <param name="priceContext">Price context.</param>
        private DiscountableItemGroup(PriceContext priceContext)
        {
            this.discountLineQuantities = new List<DiscountLineQuantity>();
            this.salesLines = new List<SalesLine>();
            this.Quantity = 0M;
            this.priceContext = priceContext;
        }

        /// <summary>
        /// Gets the item ID from the collection of SalesLines.
        /// </summary>
        public string ItemId
        {
            get { return this.salesLines.Any() ? this.salesLines[0].ItemId : string.Empty; }
        }

        /// <summary>
        /// Gets the price from the collection of SalesLines.
        /// </summary>
        public decimal Price
        {
            get { return this.salesLines.Any() ? this.salesLines[0].Price : 0M; }
        }

        /// <summary>
        /// Gets the product ID from the collection of SalesLines.
        /// </summary>
        public long ProductId
        {
            get { return this.salesLines.Any() ? this.salesLines[0].ProductId : 0; }
        }

        /// <summary>
        /// Gets product id or the master product id of the variant.
        /// </summary>
        /// <remarks>This is RecId in EcoResProduct table.</remarks>
        public long MasterProductId
        {
            get { return this.salesLines.Any() ? this.salesLines[0].MasterProductId : 0; }
        }

        /// <summary>
        /// Gets the tax rate percentage from the collection of SalesLines.
        /// </summary>
        public decimal TaxRatePercent
        {
            get { return this.salesLines.Any() ? this.salesLines[0].TaxRatePercent : 0M; }
        }

        /// <summary>
        /// Gets the unit of measure from the collection of SalesLines.
        /// </summary>
        public string SalesOrderUnitOfMeasure
        {
            get { return this.salesLines.Any() ? this.salesLines[0].SalesOrderUnitOfMeasure : string.Empty; }
        }

        /// <summary>
        /// Gets the catalog identifier from the collection of SalesLines.
        /// </summary>
        public ISet<long> CatalogIds
        {
            get { return this.salesLines.Any() ? this.salesLines[0].CatalogIds : new HashSet<long>(); }
        }

        /// <summary>
        /// Gets the number of SalesLines included in this group.
        /// </summary>
        public int Count
        {
            get { return this.salesLines.Count; }
        }

        /// <summary>
        /// Gets the collection of DiscountLineQuantity objects for this group.
        /// </summary>
        public IEnumerable<DiscountLineQuantity> DiscountLineQuantities
        {
            get { return this.discountLineQuantities; }
        }

        /// <summary>
        /// Gets or sets the Item extended properties for this item group.
        /// </summary>
        public Item ExtendedProperties { get; set; }

        /// <summary>
        /// Gets the total quantity for the line items in this group.
        /// </summary>
        public decimal Quantity { get; private set; }

        /// <summary>
        /// Gets the SalesLine object at the specified index within this group.
        /// </summary>
        /// <param name="index">The index of the line to retrieve.</param>
        /// <returns>The SalesLine object.</returns>
        public SalesLine this[int index]
        {
            get { return this.salesLines[index]; }
        }

        /// <summary>
        /// Adds the specified SalesLine object to the collection of lines included in this group.
        /// </summary>
        /// <param name="line">The line item to add.</param>
        public void Add(SalesLine line)
        {
            if (line != null)
            {
                this.salesLines.Add(line);
                this.Quantity += line.Quantity;
            }
        }

        /// <summary>
        /// Adds a discount line to this group.
        /// </summary>
        /// <param name="discountLineQuantity">The discount line quantity to add to the group.</param>
        public void AddDiscountLine(DiscountLineQuantity discountLineQuantity)
        {
            DiscountLineQuantity existingItem = this.discountLineQuantities.FirstOrDefault(p => p.DiscountLine.OfferId == discountLineQuantity.DiscountLine.OfferId && p.DiscountLine.Amount == discountLineQuantity.DiscountLine.Amount && p.DiscountLine.Percentage == discountLineQuantity.DiscountLine.Percentage);

            if (existingItem != null)
            {
                existingItem.Quantity += discountLineQuantity.Quantity;
            }
            else
            {
                this.discountLineQuantities.Add(discountLineQuantity);
            }
        }

        /// <summary>
        /// Applies the discount lines for this group to the transaction, splitting lines if necessary.
        /// </summary>
        /// <param name="transaction">The transaction that contains the lines in this group.</param>
        /// <param name="isReturn">True if it's return.</param>
        public void ApplyDiscountLines(SalesTransaction transaction, bool isReturn)
        {
            if (!this.salesLines.Any())
            {
                return;
            }

            // Keep track of available line items that have non-compoundable discounts applied to them.
            HashSet<int> availableLines = new HashSet<int>();

            // Get the lines in reverse order, so that BOGO scenarios apply to the last item by default.
            for (int x = this.salesLines.Count - 1; x >= 0; x--)
            {
                availableLines.Add(x);
            }

            //// Discounts will be in order of non-compoundable first, then compoundable, we should maintain that order.
            //// In addition, for each concurrency, threhold is the last.
            bool reallocateDiscountAmountForCompoundedThresholdDiscountAmount = false;

            // Prepare offer id to discount amount dictionary for compounded threshold discount with amount off.
            Dictionary<string, decimal> offerIdToDiscountAmountDictionary = new Dictionary<string, decimal>(StringComparer.OrdinalIgnoreCase);
            foreach (DiscountLineQuantity discount in this.DiscountLineQuantities)
            {
                if (discount.DiscountLine.PeriodicDiscountType == PeriodicDiscountOfferType.MixAndMatch && discount.DiscountLine.IsCompoundable)
                {
                    reallocateDiscountAmountForCompoundedThresholdDiscountAmount = true;
                }

                if (reallocateDiscountAmountForCompoundedThresholdDiscountAmount && 
                    discount.DiscountLine.PeriodicDiscountType == PeriodicDiscountOfferType.Threshold 
                    && discount.DiscountLine.IsCompoundable && discount.DiscountLine.Amount > decimal.Zero)
                {
                    decimal thresholdDiscountAmount = decimal.Zero;
                    offerIdToDiscountAmountDictionary.TryGetValue(discount.DiscountLine.OfferId, out thresholdDiscountAmount);

                    // Effective amount was calculated earlier in ThresholdDiscount.cs for threshold discount with amount off.
                    offerIdToDiscountAmountDictionary[discount.DiscountLine.OfferId] = thresholdDiscountAmount + this.priceContext.Rounding(discount.DiscountLine.Amount * discount.Quantity);
                }
                else
                {
                    decimal quantityNeeded = discount.Quantity;

                    bool discountFullyApplied = false;
                    bool keepGoing = true;

                    //NS Hardik Bug : 80575 : Barstool Problem
                    // if quantityNeeded=0 then discount will not apply so breaking here to stop split with 0 Qty -- Hardik : Stop split
                    if (quantityNeeded == 0)
                        keepGoing = false;
                    //NS Hardik Bug : 80575 : Barstool Problem

                    while (keepGoing)
                    {
                        bool discountPartiallyApplied = false;

                        // Look for an exact match first within lines
                        discountFullyApplied = this.ApplyDiscountLineForDirectMatch(availableLines, quantityNeeded, discount, isReturn);

                        // If that was not found, look for a combination of lower-quantity lines by selecting the largest lower-quantity line and repeating this loop.
                        if (!discountFullyApplied)
                        {
                            discountPartiallyApplied = this.ApplyDiscountLineForSmallerMatch(availableLines, ref quantityNeeded, discount, isReturn);
                        }

                        // If we still have not found a match, find the smallest higher-quantity line and split it.
                        if (!discountFullyApplied && !discountPartiallyApplied)
                        {
                            discountFullyApplied = this.ApplyDiscountLineForLargerMatch(transaction, availableLines, ref quantityNeeded, discount, isReturn);
                        }

                        // Keep going if discount is only partial - not fully - fully.
                        keepGoing = !discountFullyApplied && discountPartiallyApplied;
                    }
                }
            }

            //// We need to reallocate amount off for compounded threshold discount amount in some cases.
            //// Earlier we calculate discount amount right for the item group as a whole, but we weren't able to allocate it properly.
            //// E.g. compounded mix and match discount of bug one get one 1c, and compounded threshold discount of $10.
            ////      Transaction has a item of quantity 2, one get 1c deal price, the other one gets nothing.
            ////      Threshold discount amount would like $5 for both items, but the effective amount for the 1st one is just 1c.
            ////      As such, we need to reallocate threshold discount $10 (total), 1c to 1st item, and $9.99 to the 2nd one.
            if (reallocateDiscountAmountForCompoundedThresholdDiscountAmount)
            {
                foreach (KeyValuePair<string, decimal> offerIdDiscountAmountPair in offerIdToDiscountAmountDictionary)
                {
                    // First, build item index to discounted price dictionary, exclusing threshold percentage off.
                    Dictionary<int, decimal> itemIndexToDiscountedPriceDictionary = new Dictionary<int, decimal>();
                    availableLines.Clear();
                    decimal totalPrice = decimal.Zero;
                    for (int x = this.salesLines.Count - 1; x >= 0; x--)
                    {
                        IList<DiscountLine> existingDiscountLines = this.salesLines[x].DiscountLines;
                        decimal salesLineQuantity = Math.Abs(this.salesLines[x].Quantity);

                        // Ignore non-compounded sales lines.
                        if (salesLineQuantity != decimal.Zero && !existingDiscountLines.Where(p => !p.IsCompoundable).Any())
                        {
                            decimal discountedPrice = this.Price;
                            availableLines.Add(x);

                            // non-threshold discount $-off first
                            foreach (DiscountLine discountLineAmountOff in existingDiscountLines)
                            {
                                if (discountLineAmountOff.Amount > decimal.Zero && discountLineAmountOff.PeriodicDiscountType != PeriodicDiscountOfferType.Threshold)
                                {
                                    discountedPrice -= discountLineAmountOff.Amount;
                                }
                            }

                            // non-threshold discount %-off
                            foreach (DiscountLine discountLinePercentOff in existingDiscountLines)
                            {
                                if (discountLinePercentOff.Amount == decimal.Zero && discountLinePercentOff.PeriodicDiscountType != PeriodicDiscountOfferType.Threshold)
                                {
                                    discountedPrice -= discountedPrice * (discountLinePercentOff.Percentage / 100m);
                                }
                            }

                            // threshold discount $-off 
                            foreach (DiscountLine discountLineAmountOff in existingDiscountLines)
                            {
                                if (discountLineAmountOff.Amount > decimal.Zero && discountLineAmountOff.PeriodicDiscountType == PeriodicDiscountOfferType.Threshold)
                                {
                                    discountedPrice -= discountLineAmountOff.Amount;
                                }
                            }

                            itemIndexToDiscountedPriceDictionary[x] = discountedPrice;
                            totalPrice += discountedPrice * salesLineQuantity;
                        }
                    }

                    decimal offerDiscountAmount = offerIdDiscountAmountPair.Value;
                    foreach (KeyValuePair<int, decimal> itemIndexDiscountedPricePair in itemIndexToDiscountedPriceDictionary)
                    {
                        SalesLine salesLine = this.salesLines[itemIndexDiscountedPricePair.Key];
                        decimal salesLineQuantity = Math.Abs(salesLine.Quantity);
                        decimal price = itemIndexDiscountedPricePair.Value;
                        if (DiscountableItemGroup.IsFraction(salesLineQuantity))
                        {
                            decimal myDiscountAmount = offerDiscountAmount * ((price * salesLineQuantity) / totalPrice);
                            myDiscountAmount = this.priceContext.Rounding(myDiscountAmount);
                            decimal unitDiscountAmount = myDiscountAmount / salesLineQuantity;
                            offerDiscountAmount -= myDiscountAmount;
                            salesLine.DiscountLines.Add(NewDiscountLineCompoundedThreshold(offerIdDiscountAmountPair.Key, salesLine.LineNumber, unitDiscountAmount));
                        }
                        else
                        {
                            decimal quantityIncrement = 1m;
                            Dictionary<decimal, decimal> discountAmountToQuantityDictionary = new Dictionary<decimal, decimal>();
                            for (decimal quantity = quantityIncrement; quantity <= salesLineQuantity; quantity += quantityIncrement)
                            {
                                decimal unitDiscountAmount = this.priceContext.Rounding((offerDiscountAmount * price) / totalPrice);
                                decimal newQuantity = decimal.Zero;
                                discountAmountToQuantityDictionary.TryGetValue(unitDiscountAmount, out newQuantity);
                                discountAmountToQuantityDictionary[unitDiscountAmount] = newQuantity + quantityIncrement;
                                totalPrice -= price;

                                if (unitDiscountAmount > decimal.Zero)
                                {
                                    offerDiscountAmount -= unitDiscountAmount;
                                }
                            }

                            foreach (KeyValuePair<decimal, decimal> discountAmountQuantityPair in discountAmountToQuantityDictionary)
                            {
                                decimal discountAmount = discountAmountQuantityPair.Key;
                                decimal discountQuantity = discountAmountQuantityPair.Value;

                                if (discountQuantity != salesLineQuantity)
                                {
                                    SalesLine newLine = this.SplitLine(salesLine, discountQuantity);

                                    // Add the new line to the transaction and to the available lines for discounts.  Set the line number to the next available number.
                                    newLine.LineNumber = transaction.SalesLines.Max(p => p.LineNumber) + 1;
                                    transaction.SalesLines.Add(newLine);
                                    this.salesLines.Add(newLine);

                                    DiscountLine discountLine = NewDiscountLineCompoundedThreshold(offerIdDiscountAmountPair.Key, newLine.LineNumber, discountAmount);
                                    newLine.DiscountLines.Add(discountLine);
                                }
                                else
                                {
                                    salesLine.DiscountLines.Add(NewDiscountLineCompoundedThreshold(offerIdDiscountAmountPair.Key, salesLine.LineNumber, discountAmount));
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Multiplies the quantities for each line by -1, for use in processing negative quantities just as positive quantities are processed.
        /// </summary>
        public void NegateQuantity()
        {
            this.Quantity *= -1;
        }

        internal static bool IsFraction(decimal number)
        {
            decimal fraction = number % 1m;

            return fraction != decimal.Zero;
        }

        private static bool IsQuantityMatchSalesOrReturn(bool isReturn, decimal quantity)
        {
            return (isReturn && quantity < decimal.Zero) || (!isReturn && quantity > decimal.Zero);
        }

        private static DiscountLine NewDiscountLineCompoundedThreshold(string offerId, decimal salesLineNumber, decimal amount)
        {
            DiscountLine discountLine = new DiscountLine()
            {
                OfferId = offerId,
                SaleLineNumber = salesLineNumber,
                DiscountLineType = DataModel.DiscountLineType.PeriodicDiscount,
                PeriodicDiscountType = PeriodicDiscountOfferType.Threshold,
                ConcurrencyMode = ConcurrencyMode.Compounded,
                Amount = amount,
            };

            return discountLine;
        }

        /// <summary>
        /// Splits a SalesLine into two sales lines in cases where a discount only applies to a portion of the quantity.
        /// </summary>
        /// <param name="salesLine">The line to split.</param>
        /// <param name="quantityNeeded">The quantity to split away from this line.</param>
        /// <returns>The new SalesLine containing the needed quantity.</returns>
        private SalesLine SplitLine(SalesLine salesLine, decimal quantityNeeded)
        {
            if (salesLine.Quantity < 0)
            {
                quantityNeeded *= -1;
            }

            // Create the duplicate sale line.
            SalesLine newLine = salesLine.Clone<SalesLine>();
            newLine.Quantity = quantityNeeded;
            newLine.QuantityDiscounted = 0m;
            newLine.LineId = Guid.NewGuid().ToString();
            newLine.OriginLineId = salesLine.OriginLineId;

            if (salesLine.LineManualDiscountAmount != decimal.Zero && salesLine.Quantity > 0)
            {
                newLine.LineManualDiscountAmount = this.priceContext.Rounding((salesLine.LineManualDiscountAmount / salesLine.Quantity) * newLine.Quantity);
                salesLine.LineManualDiscountAmount -= newLine.LineManualDiscountAmount;
            }

            // Set the new quantity on the orgininal sale line item.
            salesLine.Quantity -= quantityNeeded;

            return newLine;
        }

        /// <summary>
        /// Attempts to apply a DiscountLine to a SalesLine that has a larger quantity than the required quantity, splitting the line if it is found.
        /// </summary>
        /// <param name="transaction">The current transaction containing the lines.</param>
        /// <param name="availableLines">The available line indices on the transaction.</param>
        /// <param name="quantityNeeded">The quantity needed for the DiscountLine.</param>
        /// <param name="discount">The DiscountLine and original quantity needed.</param>
        /// <param name="isReturn">True if it's return.</param>
        /// <returns>True if a match was found and the discount was applied, false otherwise.</returns>
        private bool ApplyDiscountLineForLargerMatch(SalesTransaction transaction, HashSet<int> availableLines, ref decimal quantityNeeded, DiscountLineQuantity discount, bool isReturn)
        {
            bool discountApplied = false;

            foreach (int x in availableLines.ToList().OrderBy(p => this.salesLines[p].Quantity))
            {
                SalesLine salesLine = this.salesLines[x];

                if (!IsQuantityMatchSalesOrReturn(isReturn, salesLine.Quantity))
                {
                    continue;
                }

                decimal lineQuantity = Math.Abs(salesLine.Quantity);
                if (lineQuantity > quantityNeeded)
                {
                    if (discount.DiscountLine.ConcurrencyMode != ConcurrencyMode.Compounded
                        || !salesLine.DiscountLines.Any(p => p.OfferId == discount.DiscountLine.OfferId))
                    {
                        // Perform the split of this line
                        SalesLine newLine = this.SplitLine(this.salesLines[x], quantityNeeded);

                        // Add the new line to the transaction and to the available lines for discounts.  Set the line number to the next available number.
                        newLine.LineNumber = transaction.SalesLines.Max(p => p.LineNumber) + 1;
                        transaction.SalesLines.Add(newLine);
                        this.salesLines.Add(newLine);

                        DiscountLine discountLine = discount.DiscountLine.Clone<DiscountLine>();
                        discountLine.SaleLineNumber = newLine.LineNumber;
                        newLine.DiscountLines.Add(discountLine);
                        newLine.QuantityDiscounted = quantityNeeded * Math.Sign(salesLine.Quantity);

                        // If this is a compounding discount, add the new line to the available lines.
                        if (discount.DiscountLine.ConcurrencyMode == ConcurrencyMode.Compounded)
                        {
                            availableLines.Add(this.salesLines.Count - 1);
                        }

                        discountApplied = true;
                        quantityNeeded = 0;
                        break;
                    }
                }
            }

            return discountApplied;
        }

        /// <summary>
        /// Attempts to apply a DiscountLine to a SalesLine that has a smaller quantity than the required quantity.
        /// </summary>
        /// <param name="availableLines">The available line indices on the transaction.</param>
        /// <param name="quantityNeeded">The quantity needed for the DiscountLine.</param>
        /// <param name="discount">The DiscountLine and original quantity needed.</param>
        /// <param name="isReturn">True if it's return.</param>
        /// <returns>True if the discount was applied to a line, false otherwise.</returns>
        private bool ApplyDiscountLineForSmallerMatch(HashSet<int> availableLines, ref decimal quantityNeeded, DiscountLineQuantity discount, bool isReturn)
        {
            bool discountPartiallyApplied = false;

            foreach (int x in availableLines.ToList().OrderByDescending(p => this.salesLines[p].Quantity))
            {
                SalesLine salesLine = this.salesLines[x];

                if (!IsQuantityMatchSalesOrReturn(isReturn, salesLine.Quantity))
                {
                    continue;
                }

                decimal lineQuantity = Math.Abs(salesLine.Quantity);
                if (lineQuantity < quantityNeeded)
                {
                    if (discount.DiscountLine.ConcurrencyMode != ConcurrencyMode.Compounded
                        || !salesLine.DiscountLines.Any(p => p.OfferId == discount.DiscountLine.OfferId))
                    {
                        DiscountLine discountLine = discount.DiscountLine.Clone<DiscountLine>();
                        discountLine.SaleLineNumber = salesLine.LineNumber;
                        salesLine.DiscountLines.Add(discountLine);
                        salesLine.QuantityDiscounted = salesLine.Quantity;

                        if (discount.DiscountLine.ConcurrencyMode != ConcurrencyMode.Compounded)
                        {
                            availableLines.Remove(x);
                        }

                        discountPartiallyApplied = true;
                        quantityNeeded -= lineQuantity;
                        break;
                    }
                }
            }

            return discountPartiallyApplied;
        }

        /// <summary>
        /// Attempts to apply a DiscountLine to a SalesLine that has exactly the required quantity.
        /// </summary>
        /// <param name="availableLines">The available line indices on the transaction.</param>
        /// <param name="quantityNeeded">The quantity needed for the DiscountLine.</param>
        /// <param name="discount">The DiscountLine and original quantity needed.</param>
        /// <param name="isReturn">True if it's return.</param>
        /// <returns>True if an exact match was found and the discount was applied, false otherwise.</returns>
        private bool ApplyDiscountLineForDirectMatch(HashSet<int> availableLines, decimal quantityNeeded, DiscountLineQuantity discount, bool isReturn)
        {
            bool discountApplied = false;

            foreach (int x in availableLines.ToList())
            {
                SalesLine salesLine = this.salesLines[x];

                if (!IsQuantityMatchSalesOrReturn(isReturn, salesLine.Quantity))
                {
                    continue;
                }

                decimal lineQuantity = Math.Abs(salesLine.Quantity);
                if (lineQuantity == quantityNeeded)
                {
                    if (discount.DiscountLine.ConcurrencyMode != ConcurrencyMode.Compounded
                        || !this.salesLines[x].DiscountLines.Any(p => p.OfferId == discount.DiscountLine.OfferId))
                    {
                        DiscountLine discountLine = discount.DiscountLine.Clone<DiscountLine>();
                        discountLine.SaleLineNumber = salesLine.LineNumber;
                        salesLine.DiscountLines.Add(discountLine);
                        salesLine.QuantityDiscounted = quantityNeeded * Math.Sign(salesLine.Quantity);

                        if (discount.DiscountLine.ConcurrencyMode != ConcurrencyMode.Compounded)
                        {
                            availableLines.Remove(x);
                        }

                        discountApplied = true;
                        break;
                    }
                }
            }

            return discountApplied;
        }
    }
}
