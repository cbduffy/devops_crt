﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services.PricingEngine.DiscountData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// This class implements the standard (single-line) discount processing, including the determination of which ways 
    /// the discount can apply to the transaction and the value of the discount applied to specific lines.
    /// </summary>
    public class OfferDiscount : DiscountBase
    {
        /// <summary>
        /// Gets all of the possible applications of this discount to the specified transaction and line items.
        /// </summary>
        /// <param name="transaction">The transaction to consider for discounts.</param>
        /// <param name="discountableItemGroups">The valid sales line items on the transaction to consider.</param>
        /// <param name="remainingQuantities">The remaining quantities of each of the sales lines to consider.</param>
        /// <param name="storePriceGroups">The collection of price groups that this store belongs to.</param>
        /// <param name="currencyCode">The currency code in use for the current transaction.</param>
        /// <param name="pricingDataManager">The IPricingDataManager instance to use.</param>
        /// <param name="isReturn">The flag indicating whether or not it's for return.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <returns>The possible permutations of line items that this discount can apply to, or an empty collection if this discount cannot apply.</returns>
        public override IEnumerable<DiscountApplication> GetDiscountApplications(
            SalesTransaction transaction,
            DiscountableItemGroup[] discountableItemGroups,
            decimal[] remainingQuantities,
            IEnumerable<long> storePriceGroups,
            string currencyCode,
            IPricingDataManagerV2 pricingDataManager,
            bool isReturn,
            PriceContext priceContext)
        {
            if (priceContext == null)
            {
                throw new ArgumentNullException("priceContext");
            }

            // If this discount cannot be applied, return an empty collection.
            if (!this.CanDiscountApply(transaction, storePriceGroups, currencyCode, pricingDataManager, isReturn, priceContext))
            {
                yield break;
            }

            // Get the discount code to use for any discount lines, if one is required.
            string discountCodeUsed = this.GetDiscountCodeForDiscount(transaction);

            HashSet<string> discountPriceGroups = new HashSet<string>(ConvertPriceDiscountGroupIdsToGroups(this.PriceDiscountGroupIds, priceContext));

            for (int x = 0; x < discountableItemGroups.Length; x++)
            {
                DiscountableItemGroup discountableItemGroup = discountableItemGroups[x];
                IList<RetailDiscountLine> lines = null;

                if (remainingQuantities[x] != 0M
                    && DiscountBase.IsDiscountAllowedForDiscountableItemGroup(discountableItemGroup)
                    && DiscountBase.IsDiscountAllowedForCatalogIds(priceContext, discountPriceGroups, discountableItemGroup.CatalogIds)
                    && this.TryGetRetailDiscountLines(discountableItemGroup.ProductId, discountableItemGroup.MasterProductId, discountableItemGroup.SalesOrderUnitOfMeasure, out lines))
                {
                    foreach (RetailDiscountLine line in lines)
                    {
                        // Make sure that this particular discount line is valid for tax included in price scenarios.
                        if (!(line.DiscountMethod == (int)DiscountOfferMethod.OfferPriceIncludingTax && !priceContext.IsTaxInclusive)
                            && !(line.DiscountMethod == (int)DiscountOfferMethod.OfferPrice && priceContext.IsTaxInclusive))
                        {
                            DiscountApplication result = new DiscountApplication(this)
                            {
                                RetailDiscountLines = new List<RetailDiscountLineItem>(1) { new RetailDiscountLineItem(x, line) },
                                SortIndex = GetSortIndexForRetailDiscountLine(line),
                                SortValue = GetSortValue(line, discountableItemGroup),
                                ItemQuantities = GetItemQuantitiesForDiscountApplication(remainingQuantities.Length, x, discountableItemGroup.Quantity),
                                DiscountCode = discountCodeUsed
                            };

                            yield return result;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Applies the discount application and gets the value, taking into account previously applied discounts.
        /// </summary>
        /// <param name="discountableItemGroups">The transaction line items.</param>
        /// <param name="remainingQuantities">The quantities remaining for each item.</param>
        /// <param name="appliedDiscounts">The previously applied discounts.</param>
        /// <param name="discountApplication">The specific application of the discount to use.</param>
        /// <param name="priceContext">The pricing context to use.</param>
        /// <param name="hasCompetingApplications">A value indicating whether it has competing applications.</param>
        /// <returns>The value of the discount application.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "Parameter is correct for this usage.")]
        public override AppliedDiscountApplication GetAppliedDiscountApplication(DiscountableItemGroup[] discountableItemGroups, decimal[] remainingQuantities, Stack<AppliedDiscountApplication> appliedDiscounts, DiscountApplication discountApplication, PriceContext priceContext, bool hasCompetingApplications)
        {
            if (discountApplication == null || !discountApplication.RetailDiscountLines.Any() || discountableItemGroups == null)
            {
                return null;
            }

            decimal[] prices;
            Dictionary<int, IList<DiscountLineQuantity>> discountDictionary = this.GetExistingDiscountDictionaryAndDiscountedPrices(discountableItemGroups, remainingQuantities, appliedDiscounts, discountApplication, out prices);

            RetailDiscountLineItem retailDiscountLineItem = discountApplication.RetailDiscountLines.ElementAt(0);

            DiscountOfferMethod discountMethod = (DiscountOfferMethod)retailDiscountLineItem.RetailDiscountLine.DiscountMethod;
            decimal dealPrice = decimal.Zero;

            decimal discountValue = decimal.Zero;
            decimal discountAmountForDiscountLine = decimal.Zero;
            switch (discountMethod)
            {
                case DiscountOfferMethod.DiscountAmount:
                    discountValue = retailDiscountLineItem.RetailDiscountLine.DiscountAmount;
                    discountAmountForDiscountLine = discountValue;
                    break;

                case DiscountOfferMethod.DiscountPercent:
                    discountValue = prices[retailDiscountLineItem.ItemIndex] * (retailDiscountLineItem.RetailDiscountLine.DiscountPercent / 100M);
                    break;

                case DiscountOfferMethod.OfferPrice:
                case DiscountOfferMethod.OfferPriceIncludingTax:
                    dealPrice = discountMethod == DiscountOfferMethod.OfferPrice ? retailDiscountLineItem.RetailDiscountLine.OfferPrice : retailDiscountLineItem.RetailDiscountLine.OfferPriceIncludingTax;
                    decimal bestExistingDealPrice = 0m;
                    bool hasExistingDealPrice = DiscountBase.TryGetBestExistingDealPrice(discountDictionary, retailDiscountLineItem.ItemIndex, out bestExistingDealPrice);
                    discountValue = DiscountBase.GetDiscountAmountFromDealPrice(prices[retailDiscountLineItem.ItemIndex], hasExistingDealPrice, bestExistingDealPrice, dealPrice);
                    discountAmountForDiscountLine = discountValue;
                    break;

                default:
                    break;
            }

            // When has no competing discounts or compounded, apply all remaining quantity.
            bool applyAllAvailableQuantity = !hasCompetingApplications || this.ConcurrencyMode == DataModel.ConcurrencyMode.Compounded;
            decimal quantityToApply = applyAllAvailableQuantity ? 
                remainingQuantities[retailDiscountLineItem.ItemIndex] : discountApplication.ItemQuantities[retailDiscountLineItem.ItemIndex];

            decimal result = discountValue * quantityToApply;

            AppliedDiscountApplication newAppliedDiscountApplication = null;

            if (result > decimal.Zero)
            {
                decimal[] itemQuantities;

                if (applyAllAvailableQuantity)
                {
                    itemQuantities = new decimal[discountApplication.ItemQuantities.Length];
                    itemQuantities[retailDiscountLineItem.ItemIndex] = quantityToApply;
                }
                else
                {
                    itemQuantities = discountApplication.ItemQuantities;
                }

                newAppliedDiscountApplication = new AppliedDiscountApplication(discountApplication, result, itemQuantities, isDiscountLineGenerated: true);

                DiscountLine discountLine = this.NewDiscountLine(discountApplication.DiscountCode, discountableItemGroups[retailDiscountLineItem.ItemIndex].ItemId);

                discountLine.PeriodicDiscountType = PeriodicDiscountOfferType.Offer;
                discountLine.DealPrice = dealPrice;
                discountLine.Amount = discountAmountForDiscountLine;
                discountLine.Percentage = retailDiscountLineItem.RetailDiscountLine.DiscountPercent;

                newAppliedDiscountApplication.AddDiscountLine(retailDiscountLineItem.ItemIndex, new DiscountLineQuantity(discountLine, itemQuantities[retailDiscountLineItem.ItemIndex]));
            }

            return newAppliedDiscountApplication;
        }

        /// <summary>
        /// Generate discount lines for the applied discount application.
        /// </summary>
        /// <param name="appliedDiscountApplication">The applied discount application.</param>
        /// <param name="discountableItemGroups">The discountable item groups.</param>
        /// <param name="priceContext">The price context.</param>
        public override void GenerateDiscountLines(AppliedDiscountApplication appliedDiscountApplication, DiscountableItemGroup[] discountableItemGroups, PriceContext priceContext)
        {
            // Nothing here, already generated in GetAppliedDiscountApplication.
        }

        /// <summary>
        /// Gets the sort value to use for the specified discount line and item group.
        /// </summary>
        /// <param name="discountLine">The discount line used for this application.</param>
        /// <param name="discountableItemGroup">The item group used for this application.</param>
        /// <returns>The sort value.</returns>
        private static decimal GetSortValue(RetailDiscountLine discountLine, DiscountableItemGroup discountableItemGroup)
        {
            switch ((DiscountOfferMethod)discountLine.DiscountMethod) 
            {
                case DiscountOfferMethod.DiscountAmount:
                    return discountLine.DiscountAmount;
                case DiscountOfferMethod.DiscountPercent:
                    return discountableItemGroup.Price * discountLine.DiscountPercent;
                case DiscountOfferMethod.OfferPrice:
                    return discountableItemGroup.Price - discountLine.DiscountLinePercentOrValue - discountLine.OfferPrice;
                case DiscountOfferMethod.OfferPriceIncludingTax:
                    return (discountableItemGroup.Price * (1.0M + (discountableItemGroup.TaxRatePercent / 100.0M))) - discountLine.DiscountLinePercentOrValue - discountLine.OfferPriceIncludingTax;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Gets a decimal array of the specified size with the value for the specified index set to 1.
        /// </summary>
        /// <param name="size">The size of the decimal array to create.</param>
        /// <param name="index">The index for the value that should be set to 1.</param>
        /// <param name="quantity">Item quantity.</param>
        /// <returns>The resulting decimal array.</returns>
        /// <remarks>For each line item, item quantity for discount application is 1 if line item quantity is integer and the whole quantity if fractional.</remarks>
        private static decimal[] GetItemQuantitiesForDiscountApplication(int size, int index, decimal quantity)
        {
            decimal[] result = new decimal[size];
            if (DiscountableItemGroup.IsFraction(quantity))
            {
                result[index] = Math.Abs(quantity);
            }
            else
            {
                result[index] = 1M;
            }

            return result;
        }
    }
}
