﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AFM.Commerce.Runtime.Services;
using AFM.Commerce.Runtime.Services.DataManagers;
using AFM.Commerce.Runtime.Services.Models;
using Microsoft.Dynamics.Commerce.Runtime;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
using Microsoft.Dynamics.Commerce.Runtime.Messages;
using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;
using Microsoft.Dynamics.Retail.Diagnostics;
using Microsoft.Dynamics.Commerce.Runtime.Data;

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{

    /// <summary>
    /// Create a sales order from the given context.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns>The create sales order service response.</returns>
    /// 
    public partial class SalesOrderService
    {
        private static CreateSalesOrderServiceResponse CreateSalesOrder(CreateSalesOrderServiceRequest request)
        {
            NetTracer.Information("SalesOrder.CreateSalesOrderRequest(): Begin");

            SalesTransaction transaction = request.RequestContext.GetSalesTransaction();

            var salesOrder = CreateSalesOrder(request.RequestContext, transaction);

            NetTracer.Information("SalesOrder.CreateSalesOrderRequest(): End");

            return new CreateSalesOrderServiceResponse(salesOrder);
        }

        /// <summary>
        /// Gets the next receipt identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        private static GetNextReceiptIdServiceResponse GetNextReceiptId(GetNextReceiptIdServiceRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            if (request.RequestContext == null)
            {
                throw new ArgumentNullException("request.RequestContext");
            }

            if (request.RequestContext.GetPrincipal() == null)
            {
                throw new ArgumentNullException("request.RequestContext.GetPrincipal()");
            }

            NetTracer.Information("SalesOrder.GetNextReceiptId(): Begin");

            // Get the functionality profile Id of the store
            /*
            string orgUnitNumber = request.RequestContext.GetOrgUnit().OrgUnitNumber;
            string functionalityProfileId = request.RequestContext.GetOrgUnit().FunctionalityProfileId;

            if (string.IsNullOrWhiteSpace(functionalityProfileId))
            {
                throw new ConfigurationException(
                    ConfigurationErrors.InvalidChannelConfiguration,
                    "The store '{0}' does not have a functionality profile configured.",
                    orgUnitNumber);
            }

            var receiptTransactionType = NumberSequenceSeedTypeHelper.GetReceiptTransactionType(request.TransactionType, request.NetAmountWithNoTax, request.CustomerOrderMode);
            var dataRequest = new GetReceiptMaskDataRequest(functionalityProfileId, receiptTransactionType);
            ReceiptMask mask = request.RequestContext.Runtime.Execute<SingleEntityDataServiceResponse<ReceiptMask>>(dataRequest, request.RequestContext).Entity;

            string terminalId = request.RequestContext.GetTerminal().TerminalId;

            var nextValue = !string.IsNullOrWhiteSpace(request.ReceiptNumberSequence)
                ? request.ReceiptNumberSequence
                : GetNextReceiptNumberSequenceValue(request, orgUnitNumber, mask, receiptTransactionType, terminalId).ToString();
            var channelDateTime = request.RequestContext.GetNowInChannelTimeZone();
             
            //NS Developed by spriya - RTM Upgrade


            string maskFormat = mask != null ? mask.Mask : string.Empty;

            // Generate the next receipt Id
            var nextReceiptId = ReceiptMaskFiller.FillMask(maskFormat, nextValue, orgUnitNumber, terminalId, request.RequestContext.GetPrincipal().UserId, channelDateTime.DateTime);
            */

            //NS Developed by spriya - writewebsale Dated 10/20/2014
            string orgUnitNumber = string.Empty;
            string functionalityProfileId = string.Empty;
            var mask = new ReceiptMask();
            string customMask = string.Empty;

            if (request.RequestContext != null && request.RequestContext.GetOrgUnit() != null && !string.IsNullOrEmpty(request.RequestContext.GetOrgUnit().OrgUnitNumber))
            {
                // Get the functionality profile Id of the store
                orgUnitNumber = request.RequestContext.GetOrgUnit().OrgUnitNumber;
                functionalityProfileId = request.RequestContext.GetOrgUnit().FunctionalityProfileId;



                if (string.IsNullOrWhiteSpace(functionalityProfileId))
                {
                    throw new ConfigurationException(
                        ConfigurationErrors.InvalidChannelConfiguration,
                        "The store '{0}' does not have a functionality profile configured.",
                        orgUnitNumber);
                }

                var receiptTransactionType = NumberSequenceSeedTypeHelper.GetReceiptTransactionType(request.TransactionType, request.NetAmountWithNoTax, request.CustomerOrderMode);
                var dataRequest = new GetReceiptMaskDataRequest(functionalityProfileId, receiptTransactionType);
                mask = request.RequestContext.Runtime.Execute<SingleEntityDataServiceResponse<ReceiptMask>>(dataRequest, request.RequestContext).Entity;

                if (mask == null)
                {
                    throw new ConfigurationException(
                        ConfigurationErrors.InvalidChannelConfiguration,
                        "The receipt mask could not be retrieved for functionality profile '{0}' and receipt transaction type '{1}'.",
                        functionalityProfileId,
                        receiptTransactionType);
                }

                string terminalId = request.RequestContext.GetTerminal().TerminalId;

                /* var nextValue = !string.IsNullOrWhiteSpace(request.ReceiptNumberSequence)
                      ? request.ReceiptNumberSequence 
                      : GetNextReceiptNumberSequenceValue(request, orgUnitNumber, mask, receiptTransactionType, terminalId).ToString();
                  var channelDateTime = request.RequestContext.GetNowInChannelTimeZone();*/

                customMask = mask.Mask;
            }
            else if (string.IsNullOrEmpty(orgUnitNumber) || string.IsNullOrWhiteSpace(orgUnitNumber))
            {
                AFMOrderConfirmationDataManager orderConfirmationDatamanager = new AFMOrderConfirmationDataManager(request.RequestContext);
                List<AFMConfirmationNumberMaskData> confirmationMaskData = orderConfirmationDatamanager.SetConfirmationNumberMask((request.RequestContext));
                orgUnitNumber = confirmationMaskData.First<AFMConfirmationNumberMaskData>().OMOperatingUnit;
                customMask = confirmationMaskData.First<AFMConfirmationNumberMaskData>().Mask;
            }
            // Get the next number sequence value
            NumberSequenceSeedDataManager sequenceDataManager = new NumberSequenceSeedDataManager(request.RequestContext);
            long nextValue = sequenceDataManager.GetNextNumberSequenceValue(NumberSequenceSeedType.ReceiptCustomerSalesOrder, orgUnitNumber, string.Empty);
            var channelDateTime = request.RequestContext.GetNowInChannelTimeZone();

            // Generate the next receipt Id
            var nextReceiptId = ReceiptMaskFiller.FillMask(customMask, nextValue.ToString(), orgUnitNumber, string.Empty, request.RequestContext.GetPrincipal().UserId, channelDateTime.DateTime);
            //NE Developed by spriya - writewebsale Dated 12/20/2014

            NetTracer.Information("SalesOrder.GetNextReceiptId(): End");

            return new GetNextReceiptIdServiceResponse(nextReceiptId);
        }

        //NS - RxL - 96428	ECS - Order history totals
        private static void AFMCalculateTotals(IEnumerable<SalesOrder> remoteRecords)
        {
            if (remoteRecords != null && remoteRecords.Count() > 0)
            {
                foreach (var salesOrder in remoteRecords)
                {
                    salesOrder.DiscountAmount = 0;
                    salesOrder.SubtotalAmount = 0;
                    foreach (var line in salesOrder.ActiveSalesLines)
                    {
                        salesOrder.DiscountAmount += line.DiscountAmount;
                        salesOrder.SubtotalAmount += line.NetAmount;

                        if(line.DiscountLines != null && line.DiscountLines.Count > 0)
                        {
                            foreach(var disc in line.DiscountLines)
                            {
                                if(string.IsNullOrEmpty(disc.OfferName))
                                {
                                    disc.OfferName = "Manual line discount";
                                }
                            }
                        }
                    }
                    salesOrder.NetAmount = salesOrder.SubtotalAmount;
                    salesOrder.TotalAmount = salesOrder.GrossAmount = salesOrder.NetAmountWithTax = salesOrder.NetAmount + salesOrder.TaxAmount;                      
                    salesOrder.TotalDiscount = salesOrder.DiscountAmount;
                }
            }
        }
        //NE - RxL - 96428	ECS - Order history totals
    }
}
