﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using PaymentSDK = Microsoft.Dynamics.Retail.PaymentSDK.Portable;
    using AFMRuntimeServices = AFM.Commerce.Runtime.Services.Common;

    /// <summary>
    /// Encapsulates functionality related to card types.
    /// </summary>
    internal static class CardTypeHelper
    {
        internal static CardTypeInfo GetCardTypeConfiguration(long channelId, string cardTypeId, RequestContext context)
        {
            if (string.IsNullOrWhiteSpace(cardTypeId))
            {
                throw new ArgumentNullException("cardTypeId");
            }

            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            PagingInfo paging = new PagingInfo
            {
                 // Due to the nature of sql join CARDTYPESVIEW has multiple rows for each card type (one per each number mask).
                 // Since for specific card type configuration will be the same only first row is being used.
                Skip = 0,
                Top = 1
            };

            IEnumerable<CardTypeInfo> cardTypes;
            if (string.IsNullOrWhiteSpace(cardTypeId))
            {
                var cardTypeDataRequest = new GetCardTypeDataRequest(channelId);
                var response = context.Execute<EntityDataServiceResponse<CardTypeInfo>>(cardTypeDataRequest);
                cardTypes = response.EntityCollection;
            }
            else
            {
                GetCardTypeDataRequest getCardTypeRequest = new GetCardTypeDataRequest(channelId, cardTypeId);
                getCardTypeRequest.QueryResultSettings = new QueryResultSettings(paging);
                IEnumerable<CardTypeInfo> infos = context.Execute<EntityDataServiceResponse<CardTypeInfo>>(getCardTypeRequest).EntityCollection;
                cardTypes = infos.AsReadOnly();
            }

            return cardTypes.SingleOrDefault();
        }
        //CS by spriya : DMND0010113 - Synchrony Financing online
        //Updated to extended card type
        internal static AFMRuntimeServices.ExtendedCardType GetPaymentSdkCardType(CardTypeInfo cardTypeConfiguration, string cardType)
        {
            if (string.IsNullOrWhiteSpace(cardType))
            {
                throw new ArgumentException("cardType argument cannot be epmty", "cardType");
            }

            if (cardTypeConfiguration != null && cardTypeConfiguration.CardType == DataModel.CardType.InternationalDebitCard)
            {
                return AFMRuntimeServices.ExtendedCardType.Debit;
            }

            /* Commented for using extended card Type Replacing PaymentSDK.CardType with AFMRuntimeServices.ExtendedCardType
             */
            AFMRuntimeServices.ExtendedCardType paymentSdkType;
            if (!Enum.TryParse(cardType, ignoreCase: true, result: out paymentSdkType))
            {
                return AFMRuntimeServices.ExtendedCardType.Unknown;
            }
            //CE by spriya : DMND0010113 - Synchrony Financing online
            return paymentSdkType;
        }
    }
}
