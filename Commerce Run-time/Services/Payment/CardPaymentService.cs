﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CardPaymentService.cs" company="Microsoft Corporation">
// THIS CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
// OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
// THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
// NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
// </copyright>
// <summary>
//   The card payment service implementation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Handlers;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Retail.Diagnostics;
    using Microsoft.Dynamics.Retail.PaymentSDK.Portable;
    using Microsoft.Dynamics.Retail.PaymentSDK.Portable.Constants;
    using Microsoft.Dynamics.Retail.SDKManager.Portable;
    using PaymentSDK = Microsoft.Dynamics.Retail.PaymentSDK.Portable;
    using AFMRuntimeServices = AFM.Commerce.Runtime.Services.Common;

    /// <summary>
    /// The payment service implementation.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Will be fixed in CTP2")]
    public class CardPaymentService : IOperationRequestHandler
    {
        /// <summary>
        /// The card number mask.
        /// </summary>
        private const string CardNumberMask = "xxxxxxxxxxxx";

        /// <summary>
        /// The locale.
        /// </summary>
        private static string locale;

        /// <summary>
        /// The country region mapper.
        /// </summary>
        private static CountryRegionMapper countryRegionMapper;

        /// <summary>
        /// Gets a collection of operation identifiers supported by this request handler.
        /// </summary>
        public IEnumerable<int> SupportedOperationIds
        {
            get
            {
                return new[]
                {
                    (int)RetailOperation.PayCard
                };
            }
        }

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(CalculatePaymentAmountServiceRequest),
                    typeof(GenerateCardTokenPaymentServiceRequest),
                    typeof(AuthorizePaymentServiceRequest),
                    typeof(AuthorizeTokenizedCardPaymentServiceRequest),
                    typeof(VoidPaymentServiceRequest),
                    typeof(CapturePaymentServiceRequest),
                    typeof(SupportedCardTypesPaymentServiceRequest),
                    typeof(GetCardPaymentPropertiesServiceRequest),
                    typeof(GetChangePaymentServiceRequest)
                };
            }
        }

        /// <summary>
        /// Entry point to Payment service. Takes a Payment service request and returns the result
        /// of the request execution.
        /// </summary>
        /// <param name="request">The Currency service request to execute.</param>
        /// <returns>Result of executing request, or null object for void operations.</returns>
        public Runtime.Messages.Response Execute(Runtime.Messages.Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            if (countryRegionMapper == null)
            {
                countryRegionMapper = new CountryRegionMapper(request.RequestContext);
            }

            Type requestType = request.GetType();
            Runtime.Messages.Response response;

            if (request is PaymentServiceRequestBase)
            {
                // Check the platform for the payment SDK
                SetPaymentSdkPlatform(((PaymentServiceRequestBase)request).Platform);
            }

            if (requestType == typeof(CalculatePaymentAmountServiceRequest))
            {
                response = CalculatePaymentAmount((CalculatePaymentAmountServiceRequest)request);
            }
            else if (requestType == typeof(GetChangePaymentServiceRequest))
            {
                response = GetChange((GetChangePaymentServiceRequest)request);
            }
            else if (requestType == typeof(AuthorizePaymentServiceRequest))
            {
                response = AuthorizePayment((AuthorizePaymentServiceRequest)request);
            }
            else if (requestType == typeof(AuthorizeTokenizedCardPaymentServiceRequest))
            {
                response = AuthorizeTokenizedCardPayment((AuthorizeTokenizedCardPaymentServiceRequest)request);
            }
            else if (requestType == typeof(VoidPaymentServiceRequest))
            {
                response = CancelPayment((VoidPaymentServiceRequest)request);
            }
            else if (requestType == typeof(CapturePaymentServiceRequest))
            {
                response = CapturePayment((CapturePaymentServiceRequest)request);
            }
            else if (requestType == typeof(GenerateCardTokenPaymentServiceRequest))
            {
                response = GenerateCardToken((GenerateCardTokenPaymentServiceRequest)request);
            }
            else if (requestType == typeof(SupportedCardTypesPaymentServiceRequest))
            {
                response = SupportedCardTypes((SupportedCardTypesPaymentServiceRequest)request);
            }
            else if (requestType == typeof(SupportedCardTypesPaymentServiceRequest))
            {
                response = SupportedCardTypes((SupportedCardTypesPaymentServiceRequest)request);
            }
            else if (requestType == typeof(GetCardPaymentPropertiesServiceRequest))
            {
                response = GetAuthorizationPropertiesForReceipt((GetCardPaymentPropertiesServiceRequest)request);
            }
            else
            {
                NetTracer.Error("Unsupported request type for payments");
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }

            NetTracer.Information("Completed Payment.Execute");
            return response;
        }

        /// <summary>
        /// Gets the change.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        /// A response containing the change tender line.
        /// </returns>
        private static GetChangePaymentServiceResponse GetChange(GetChangePaymentServiceRequest request)
        {
            // Change cannot be given in gift cards because it requires manual card number input.
            throw new FeatureNotSupportedException(FeatureNotSupportedErrors.ChangeTenderTypeNotSupported, string.Format("Request '{0}' is not supported. Verify change tender type settings.", request.GetType()));
        }

        /// <summary>
        /// Calculate amount to do be paid.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>A response containing the updated tender line.</returns>
        private static CalculatePaymentAmountServiceResponse CalculatePaymentAmount(CalculatePaymentAmountServiceRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            // no calculation required.
            return new CalculatePaymentAmountServiceResponse(request.TenderLine);
        }

        /// <summary>
        /// Calculate amount to do be paid.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>A response containing the updated tender line.</returns>
        private static GetCardPaymentPropertiesServiceResponse GetAuthorizationPropertiesForReceipt(GetCardPaymentPropertiesServiceRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            string portablePaymentPropertyXmlBlob = string.Empty;
            string accountType = null;
            string approvalCode = null;
            string connectorName = null;
            string providerTransactionId = null;
            string infoMessage = null;
            decimal availableBalance = 0;

            if (request.IsEncryptionCall)
            {
                if (request.IsPortable)
                {
                    portablePaymentPropertyXmlBlob = PortablePaymentProperty.ToPortable(request.AuthorizationXml);
                }
                else
                {
                    portablePaymentPropertyXmlBlob = PortablePaymentProperty.ToLocal(request.AuthorizationXml);
                }
            }
            else
            {
                string authorizationXml = request.AuthorizationXml;
                if (!request.AuthorizationXml.StartsWith("<![CDATA["))
                {
                    authorizationXml = request.AuthorizationXml.Substring(36);
                }

                PaymentProperty[] properties = PaymentProperty.ConvertXMLToPropertyArray(authorizationXml);

                Hashtable propertyTable = PaymentProperty.ConvertToHashtable(properties);

                PaymentProperty property = PaymentProperty.GetPropertyFromHashtable(propertyTable, GenericNamespace.PaymentCard, PaymentCardProperties.AccountType);

                if (property != null)
                {
                    accountType = property.StringValue ?? string.Empty;
                }

                property = PaymentProperty.GetPropertyFromHashtable(propertyTable, GenericNamespace.AuthorizationResponse, AuthorizationResponseProperties.AvailableBalance);

                if (property != null)
                {
                    availableBalance = property.DecimalValue;
                }

                property = PaymentProperty.GetPropertyFromHashtable(propertyTable, GenericNamespace.AuthorizationResponse, AuthorizationResponseProperties.ApprovalCode);

                if (property != null)
                {
                    approvalCode = property.StringValue ?? string.Empty;
                }

                property = PaymentProperty.GetPropertyFromHashtable(propertyTable, GenericNamespace.Connector, ConnectorProperties.ConnectorName);

                if (property != null)
                {
                    connectorName = property.StringValue ?? string.Empty;
                }

                property = PaymentProperty.GetPropertyFromHashtable(propertyTable, GenericNamespace.AuthorizationResponse, AuthorizationResponseProperties.ProviderTransactionId);

                if (property != null)
                {
                    providerTransactionId = property.StringValue ?? string.Empty;
                }

                property = PaymentProperty.GetPropertyFromHashtable(propertyTable, GenericNamespace.AuthorizationResponse, AuthorizationResponseProperties.AuthorizationResult);

                if (property != null)
                {
                    if (property.StringValue.Equals(AuthorizationResult.Success.ToString(), StringComparison.OrdinalIgnoreCase)
                            || property.StringValue.Equals(AuthorizationResult.PartialAuthorization.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        // send APPROVED text if the authorization is approved.
                        infoMessage = "<T:string_6156>";
                    }
                    else
                    {
                        // send DECLINED text if the authorization is declined.
                        infoMessage = "<T:string_6157>";
                    }
                }
            }

            GetCardPaymentPropertiesServiceResponse response = new GetCardPaymentPropertiesServiceResponse(accountType, approvalCode, connectorName, providerTransactionId, availableBalance, infoMessage, portablePaymentPropertyXmlBlob: portablePaymentPropertyXmlBlob);
            return response;
        }

        /// <summary>
        /// Captures the payment.
        /// </summary>
        /// <param name="request">The payment capture request.</param>
        /// <returns>Response to capture payment request. </returns>
        private static CapturePaymentServiceResponse CapturePayment(CapturePaymentServiceRequest request)
        {
            NetTracer.Information("Calling Payment.CapturePayment");
            
            if (request.TenderLine.IsPreProcessed)
            {
                request.TenderLine.Status = TenderLineStatus.Committed;
                return new CapturePaymentServiceResponse(ReEncryptPaymentAuthorizationProperties(request.RequestContext, request.TenderLine, request.Platform));
            }

            Request paymentRequest = new Request
            {
                Locale = locale ?? GetLocale(request.RequestContext),
                Properties = GetPaymentPropertiesForCapture(request.TenderLine, request.PaymentCard.CardNumber).ToArray()
            };

            if (paymentRequest.Properties == null
                || !paymentRequest.Properties.Any())
            {
                NetTracer.Error("Invalid request");
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "Invalid request.");
            }

            PaymentProperty[] updatedPaymentRequestProperties = paymentRequest.Properties;

            string connectorName;
            updatedPaymentRequestProperties = AddConnectorPropertiesByServiceAccountId(request.RequestContext, updatedPaymentRequestProperties, out connectorName);

            paymentRequest.Properties = updatedPaymentRequestProperties;

            // Get payment processor
            IPaymentProcessor processor = CardPaymentService.GetPaymentProcessor(connectorName);

            // Run Capture
            Response response = processor.Capture(paymentRequest);
            VerifyResponseResult("Capture", response, GenericNamespace.CaptureResponse, CaptureResponseProperties.CaptureResult, new List<string>() { CaptureResult.Success.ToString() }, PaymentErrors.UnableToCapturePayment);

            NetTracer.Information("Completed Payment.Capture");
            return new CapturePaymentServiceResponse(GetTenderLineFromCaptureResponse(request));
        }

        /// <summary>
        /// Generates card token.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="GenerateCardTokenPaymentServiceResponse"/>card token response.
        /// </returns>
        /// <exception cref="DataValidationException">Validation exception.</exception>
        private static GenerateCardTokenPaymentServiceResponse GenerateCardToken(GenerateCardTokenPaymentServiceRequest request)
        {
            NetTracer.Information("Calling Payment.GenerateCardToken");

            if (request.TokenizedPaymentCard != null)
            {
                request.TenderLine.Authorization = request.TokenizedPaymentCard.CardTokenInfo.CardToken;
                TenderLine localEncryptedTenderLine = ReEncryptPaymentAuthorizationProperties(request.RequestContext, request.TenderLine, request.Platform);
                request.TokenizedPaymentCard.CardTokenInfo.CardToken = localEncryptedTenderLine.Authorization;
                return new GenerateCardTokenPaymentServiceResponse(localEncryptedTenderLine, request.TokenizedPaymentCard, localEncryptedTenderLine.Authorization);
            }

            Request paymentRequest = new Request { Locale = locale ?? GetLocale(request.RequestContext) };

            string cardTypeId = request.TenderLine.CardTypeId ?? request.PaymentCard.CardTypes;
            CardTypeInfo cardTypeConfiguration = CardTypeHelper.GetCardTypeConfiguration(request.RequestContext.GetPrincipal().ChannelId, cardTypeId, request.RequestContext);

            IList<PaymentProperty> paymentProperties = GetPaymentPropertiesForAuthorizationAndRefund(request.TenderLine, request.PaymentCard, cardTypeConfiguration, isCardTokenRequired: true, allowPartialAuthorization: false); // Since this is a tokenization request, the value of AllowPartialAuthorization flag does not matter. 
            if (paymentProperties == null || !paymentProperties.Any())
            {
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "Invalid request.");
            }

            string connectorName;
            IEnumerable<PaymentProperty> connectorProperties = GetConnectorProperties(request.RequestContext, paymentProperties, out connectorName);
            paymentProperties.AddRange(connectorProperties);
            paymentRequest.Properties = paymentProperties.ToArray();

            // Get payment processor
            IPaymentProcessor processor = CardPaymentService.GetPaymentProcessor(connectorName);

            // Run GenerateCardToken
            Response response = processor.GenerateCardToken(paymentRequest, null);
            VerifyResponseErrors("GenerateCardToken", response, PaymentErrors.UnableToGenerateToken);

            CardTokenInfo cardTokenInfo = new CardTokenInfo();

            PaymentSDK.Hashtable hashTable = PaymentProperty.ConvertToHashtable(response.Properties);

            PaymentProperty property = PaymentProperty.GetPropertyFromHashtable(hashTable, GenericNamespace.PaymentCard, PaymentCardProperties.CardToken);
            cardTokenInfo.CardToken = (property != null) ? property.StringValue : string.Empty;

            property = PaymentProperty.GetPropertyFromHashtable(hashTable, GenericNamespace.PaymentCard, PaymentCardProperties.UniqueCardId);
            cardTokenInfo.UniqueCardId = (property != null) ? property.StringValue : string.Empty;

            property = PaymentProperty.GetPropertyFromHashtable(hashTable, GenericNamespace.MerchantAccount, MerchantAccountProperties.ServiceAccountId);
            cardTokenInfo.ServiceAccountId = (property != null) ? property.StringValue : string.Empty;

            property = PaymentProperty.GetPropertyFromHashtable(hashTable, GenericNamespace.PaymentCard, PaymentCardProperties.Last4Digits);
            cardTokenInfo.MaskedCardNumber = (property != null) ? (CardNumberMask + property.StringValue) : string.Empty;

            string reponsePropertiesBlob = PaymentProperty.ConvertPropertyArrayToXML(response.Properties);

            TokenizedPaymentCard tokenizedPaymentCard = GetTokenizedPaymentCard(request.PaymentCard, cardTokenInfo);

            //NS Developed by muthait
            tokenizedPaymentCard["TranId"] = request.PaymentCard["TranId"];
            tokenizedPaymentCard["IsTestMode"] = request.PaymentCard["IsTestMode"];            
            //NE Developed by muthait

            //N by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
            tokenizedPaymentCard["LineConfirmationGroupId"] = request.PaymentCard["LineConfirmationGroupId"];

            NetTracer.Information("Completed Payment.GenerateCardToken");
            return new GenerateCardTokenPaymentServiceResponse(GetTenderLineForGeneratedCardToken(request, hashTable), tokenizedPaymentCard, reponsePropertiesBlob);
        }

        /// <summary>
        /// Gets the tokenized payment card.
        /// </summary>
        /// <param name="paymentCard">The payment card that was used to generate the card token.</param>
        /// <param name="cardTokenInfo">The card token information.</param>
        /// <returns>An instance of a tokenized payment card.</returns>
        private static TokenizedPaymentCard GetTokenizedPaymentCard(PaymentCard paymentCard, CardTokenInfo cardTokenInfo)
        {
            TokenizedPaymentCard tokenizedPaymentCard = new TokenizedPaymentCard()
            {
                CardTokenInfo = cardTokenInfo,
                CardTypes = paymentCard.CardTypes,
                NameOnCard = paymentCard.NameOnCard,
                ExpirationYear = paymentCard.ExpirationYear,
                ExpirationMonth = paymentCard.ExpirationMonth,
                Address1 = paymentCard.Address1,
                Country = paymentCard.Country,
                Zip = paymentCard.Zip,
                City = paymentCard.City,
                State = paymentCard.State
            };

            return tokenizedPaymentCard;
        }

        /// <summary>
        /// Authorizes the tokenized card payment.
        /// </summary>
        /// <param name="authorizationRequest">The authorization request.</param>
        /// <returns>
        /// A payment authorization response.
        /// </returns>
        private static AuthorizePaymentServiceResponse AuthorizeTokenizedCardPayment(AuthorizeTokenizedCardPaymentServiceRequest authorizationRequest)
        {
            NetTracer.Information("Calling Payment.TokenenizedCardPaymentAuthorize");
            string connectorName;
            PaymentProperty[] tokenResponseProperties = null;
            PaymentProperty[] paymentRequestProperties = null;

            string cardTypeId = authorizationRequest.TenderLine.CardTypeId ?? authorizationRequest.TokenizedPaymentCard.CardTypes;
            CardTypeInfo cardTypeConfiguration = CardTypeHelper.GetCardTypeConfiguration(authorizationRequest.RequestContext.GetPrincipal().ChannelId, cardTypeId, authorizationRequest.RequestContext);
            if (!authorizationRequest.SkipLimitValidation)
            {
                ValidateCardEntry(authorizationRequest.TokenizedPaymentCard, cardTypeConfiguration);
            }

            ValidateTokenInfo(authorizationRequest.TokenizedPaymentCard.CardTokenInfo);
            paymentRequestProperties = GetPaymentPropertiesForAuthorizationWithToken(authorizationRequest.TenderLine, authorizationRequest.TokenizedPaymentCard, authorizationRequest.AllowPartialAuthorization);

            paymentRequestProperties = AddConnectorPropertiesByServiceAccountId(authorizationRequest.RequestContext, paymentRequestProperties, out connectorName);

            tokenResponseProperties = GetTokenResponseProperties(authorizationRequest.TokenResponsePaymentProperties, authorizationRequest.TokenizedPaymentCard);

            //CS Developed by muthait
            PaymentCard paymentCard = new PaymentCard();
            paymentCard["TranId"] = authorizationRequest.TokenizedPaymentCard["TranId"];
            paymentCard["IsTestMode"] = authorizationRequest.TokenizedPaymentCard["IsTestMode"];

            //N by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
            paymentCard["LineConfirmationGroupId"] = authorizationRequest.TokenizedPaymentCard["LineConfirmationGroupId"];

            AuthorizePaymentServiceResponse authorizeResponse = AuthorizePayment(
                authorizationRequest.RequestContext,
                authorizationRequest.TenderLine,
                connectorName,
                paymentRequestProperties,
                tokenResponseProperties,paymentCard);

            //CE Developed by muthait

            return authorizeResponse;
        }

        /// <summary>
        /// Authorizes the payment.
        /// </summary>
        /// <param name="authorizationRequest">The authorization request.</param>
        /// <returns>A payment authorization response.</returns>
        private static AuthorizePaymentServiceResponse AuthorizePayment(AuthorizePaymentServiceRequest authorizationRequest)
        {
            NetTracer.Information("Calling Payment.Authorize");

            if (authorizationRequest.TenderLine.IsPreProcessed)
            {
                return new AuthorizePaymentServiceResponse(ReEncryptPaymentAuthorizationProperties(authorizationRequest.RequestContext, authorizationRequest.TenderLine, authorizationRequest.Platform));
            }

            string connectorName;

            string cardTypeId = authorizationRequest.TenderLine.CardTypeId ?? authorizationRequest.PaymentCard.CardTypes;
            CardTypeInfo cardTypeConfiguration = CardTypeHelper.GetCardTypeConfiguration(authorizationRequest.RequestContext.GetPrincipal().ChannelId, cardTypeId, authorizationRequest.RequestContext);
            if (!authorizationRequest.SkipLimitValidation)
            {
                ValidateCardEntry(authorizationRequest.PaymentCard, cardTypeConfiguration);
            }

            IList<PaymentProperty> paymentProperties = GetPaymentPropertiesForAuthorizationAndRefund(authorizationRequest.TenderLine, authorizationRequest.PaymentCard, cardTypeConfiguration, authorizationRequest.IsCardTokenRequired, authorizationRequest.AllowPartialAuthorization);

            IEnumerable<PaymentProperty> connectorProperties = GetConnectorProperties(authorizationRequest.RequestContext, paymentProperties, out connectorName);
            paymentProperties.AddRange(connectorProperties);
            PaymentProperty[] paymentRequestProperties = paymentProperties.ToArray();

            /// CS by muthait dated 17Oct2014
            AuthorizePaymentServiceResponse authorizeResponse = AuthorizePayment(
                authorizationRequest.RequestContext,
                authorizationRequest.TenderLine,
                connectorName,
                paymentRequestProperties,
                null, authorizationRequest.PaymentCard);

            return authorizeResponse;
        }


        /// CS by muthait dated 17Oct2014
        /// <summary>
        /// Authorizes payment.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="tenderLine">The tender line.</param>
        /// <param name="connectorName">Name of the connector.</param>
        /// <param name="paymentRequestProperties">The payment request properties.</param>
        /// <param name="cardTokenResponsePaymentProperties">The card token response payment properties.</param>
        /// <returns>
        /// The <see cref="AuthorizePaymentServiceResponse"/>.
        /// </returns>
        private static AuthorizePaymentServiceResponse AuthorizePayment(RequestContext context, TenderLine tenderLine, string connectorName, PaymentProperty[] paymentRequestProperties, PaymentProperty[] cardTokenResponsePaymentProperties, PaymentCard paymentCard = null)
        {
            Request paymentRequest = new Request { Locale = locale ?? GetLocale(context) };
            paymentRequest.Properties = paymentRequestProperties;

            // Get payment processor
            IPaymentProcessor processor = CardPaymentService.GetPaymentProcessor(connectorName);
            Response response;

            if (tenderLine.Amount >= 0)
            {
                // Run authorize
                //CS by muthait dated 10Sep2014
                if (paymentCard != null)
                {
                    IEnumerable<PaymentProperty> ashleyEnhancedAuthorizationProperties =
                        GetAshleyEnhancedAuthorizationProperties(paymentCard);
                    if (ashleyEnhancedAuthorizationProperties != null)
                    {
                        var paymentProperties = new List<PaymentProperty>();

                        paymentProperties.AddRange(paymentRequest.Properties);
                        paymentProperties.AddRange(ashleyEnhancedAuthorizationProperties);
                        paymentRequest.Properties = paymentProperties.ToArray();
                    }
                }

                //CE by muthait date 10Sep2014
                response = processor.Authorize(paymentRequest, null);
                VerifyResponseResult("Authorization", response, GenericNamespace.AuthorizationResponse, AuthorizationResponseProperties.AuthorizationResult, new List<string>() { AuthorizationResult.Success.ToString(), AuthorizationResult.PartialAuthorization.ToString() }, PaymentErrors.UnableToAuthorizePayment);

                var cardTokenResponsePaymentPropertiesList = cardTokenResponsePaymentProperties.ToList();
                if (cardTokenResponsePaymentProperties != null && cardTokenResponsePaymentProperties.Any())
                {
                    foreach (var prop in response.Properties)
                    {
                        if (prop.Namespace == "Connector")
                        {
                            if (!cardTokenResponsePaymentPropertiesList.Contains(prop))
                            {
                                cardTokenResponsePaymentPropertiesList.Add(prop);
                            }
                        }
                    }

                    //NS by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
                    //To Display adddress on Customer Credit card screen
                    var addressProperties = paymentRequest.Properties.Where(p => p.Name.Equals(PaymentCardProperties.Phone) || p.Name.Equals(PaymentCardProperties.PostalCode) || p.Name.Equals(PaymentCardProperties.State) ||
                                  p.Name.Equals(PaymentCardProperties.StreetAddress) || p.Name.Equals(PaymentCardProperties.StreetAddress2) || p.Name.Equals(PaymentCardProperties.City) || p.Name.Equals(PaymentCardProperties.Country)).ToList();
                    
                    cardTokenResponsePaymentPropertiesList.AddRange(addressProperties);
                    //NE by RxL

                    PaymentProperty property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.Properties, cardTokenResponsePaymentPropertiesList.ToArray());
                    
                    response.Properties = CombinePaymentProperties(response.Properties, property);
                }
                else
                {
                    response.Properties = CombinePaymentProperties(response.Properties, paymentRequest.Properties);
                }

                NetTracer.Information("Completed Payment.Authorize");
                return new AuthorizePaymentServiceResponse(GetTenderLineFromAuthorizationResponse(tenderLine, response.Properties));
            }
            else
            {
                response = processor.Refund(paymentRequest, null);
                VerifyResponseResult("Refund", response, GenericNamespace.RefundResponse, RefundResponseProperties.RefundResult, new List<string>() { RefundResult.Success.ToString() }, PaymentErrors.UnableToRefundPayment);
                NetTracer.Information("Completed Payment.Refund");
                return new AuthorizePaymentServiceResponse(GetTenderLineFromRefundResponse(tenderLine, response.Properties));
            }
        }

        /// <summary>
        /// Gets the token response properties.
        /// </summary>
        /// <param name="serializedTokenResponseProperties">The serialized token response properties.</param>
        /// <param name="tokenizedPaymentCard">The tokenized payment card.</param>
        /// <returns>
        /// The payment properties that are associated with a token generation response.
        /// </returns>
        private static PaymentProperty[] GetTokenResponseProperties(string serializedTokenResponseProperties, TokenizedPaymentCard tokenizedPaymentCard)
        {
            PaymentProperty[] tokenResponseProperties;

            if (!string.IsNullOrWhiteSpace(serializedTokenResponseProperties))
            {
                tokenResponseProperties = PaymentProperty.ConvertXMLToPropertyArray(serializedTokenResponseProperties);
            }
            else
            {
                // This can happen if the token was created externally, and the token response is not available.
                // In such a case, generate an array of payment properties that closely resembles a token response.
                // This is necessary because Ax requires these values to process the payment of an online order once it is downloaded to Ax.
                tokenResponseProperties = GetSimulatedTokenResponseProperties(tokenizedPaymentCard);
            }

            return tokenResponseProperties;
        }

        /// <summary>
        /// Gets the simulated token response properties. This method should be used when due to some reason, the serialized property blob associated with the token generation response is not available.
        /// In such a case, this method can generate an array of payment properties that closely resembles a token response.
        /// </summary>
        /// <param name="tokenizedPaymentCard">The tokenized payment card.</param>
        /// <returns>
        /// An array of payment properties that resemble a token generation response.
        /// </returns>
        private static PaymentProperty[] GetSimulatedTokenResponseProperties(TokenizedPaymentCard tokenizedPaymentCard)
        {
            List<PaymentProperty> simulatedTokenResponseProperties = new List<PaymentProperty>();

            PaymentProperty property = new PaymentProperty(GenericNamespace.TransactionData, TransactionDataProperties.SupportCardTokenization, true.ToString(), SecurityLevel.None);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.CardType, tokenizedPaymentCard.CardTypes, SecurityLevel.PCI);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.Name, tokenizedPaymentCard.NameOnCard, SecurityLevel.PCI);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.ExpirationYear, tokenizedPaymentCard.ExpirationYear, SecurityLevel.PCI);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.ExpirationMonth, tokenizedPaymentCard.ExpirationMonth, SecurityLevel.PCI);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.StreetAddress, tokenizedPaymentCard.Address1, SecurityLevel.PII);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.PostalCode, tokenizedPaymentCard.Zip, SecurityLevel.None);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.Country, tokenizedPaymentCard.Country, SecurityLevel.None);
            simulatedTokenResponseProperties.Add(property);

            //NS by ralandge 5182: FIN-DJ Credit card address is not populating in AX
            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.City, tokenizedPaymentCard.City, SecurityLevel.None);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.State, tokenizedPaymentCard.State, SecurityLevel.None);
            simulatedTokenResponseProperties.Add(property);
            //NE

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.UniqueCardId, tokenizedPaymentCard.CardTokenInfo.UniqueCardId, SecurityLevel.PCI);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.CardToken, tokenizedPaymentCard.CardTokenInfo.CardToken, SecurityLevel.PCI);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.MerchantAccount, MerchantAccountProperties.ServiceAccountId, tokenizedPaymentCard.CardTokenInfo.ServiceAccountId, SecurityLevel.None);
            simulatedTokenResponseProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.Last4Digits, tokenizedPaymentCard.CardTokenInfo.MaskedCardNumber, SecurityLevel.PCI);
            simulatedTokenResponseProperties.Add(property);

            return simulatedTokenResponseProperties.ToArray();
        }

        /// <summary>
        /// Validates the token information.
        /// </summary>
        /// <param name="cardTokenInfo">The card token information.</param>
        /// <exception cref="DataValidationException">Throw if any of the crucial properties on the card token info object are not set.</exception>
        private static void ValidateTokenInfo(CardTokenInfo cardTokenInfo)
        {
            string validationErrorMessage = string.Empty;
            bool didValidationFail = false;

            if (string.IsNullOrEmpty(cardTokenInfo.ServiceAccountId))
            {
                validationErrorMessage += "A non-empty payment service account identifier, that was used to create the credit card token, must be specified for payment processing to proceed.";
                didValidationFail = true;
            }

            if (string.IsNullOrEmpty(cardTokenInfo.CardToken))
            {
                validationErrorMessage += "The CardToken field must not be empty.";
                didValidationFail = true;
            }

            if (string.IsNullOrEmpty(cardTokenInfo.UniqueCardId))
            {
                validationErrorMessage += "The UniqueCardId field must not be empty.";
                didValidationFail = true;
            }

            if (string.IsNullOrEmpty(cardTokenInfo.MaskedCardNumber))
            {
                validationErrorMessage += "The MaskedCardNumber field must not be empty.";
                didValidationFail = true;
            }

            if (didValidationFail)
            {
                throw new DataValidationException(PaymentErrors.UnableToAuthorizePayment, validationErrorMessage);
            }
        }

        /// <summary>
        /// Verifies response result.
        /// </summary>
        /// <param name="operationName">
        /// The operation name.
        /// </param>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <param name="responseNamespace">
        /// The response namespace.
        /// </param>
        /// <param name="resultProperty">
        /// The result property.
        /// </param>
        /// <param name="expectedValues">
        /// A list of expected values.
        /// </param>
        /// <param name="errorResourceId">
        /// The error resource id.
        /// </param>
        private static void VerifyResponseResult(string operationName, Response response, string responseNamespace, string resultProperty, List<string> expectedValues, string errorResourceId)
        {
            PaymentProperty property = null;

            // Payment connector might of returned no properties
            if (response.Properties != null)
            {
                PaymentSDK.Hashtable hashTable = PaymentProperty.ConvertToHashtable(response.Properties);
                property = PaymentProperty.GetPropertyFromHashtable(
                    hashTable,
                    responseNamespace,
                    resultProperty);
            }

            if (property == null || !expectedValues.Where(s => s.Equals(property.StringValue, StringComparison.OrdinalIgnoreCase)).Any())
            {
                VerifyResponseErrors(operationName, response, errorResourceId);

                // throw generic exception if operation failed but no errors returned.
                throw new PaymentException(errorResourceId, "{0} failed.", operationName);
            }
        }

        /// <summary>
        /// Verifies response errors.
        /// </summary>
        /// <param name="operationName">
        /// The operation name.
        /// </param>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <param name="errorResourceId">
        /// The error resource id.
        /// </param>
        private static void VerifyResponseErrors(string operationName, Response response, string errorResourceId)
        {
            // CD by muthait dated 26Dec2014
            StringBuilder errorsInfo = new StringBuilder();
            //StringBuilder errorsInfo = new StringBuilder("Error returned from the payment connector. See error details below:");

            if (response.Errors == null)
            {
                return;
            }

            // The format is used by clientError() in source\frameworks\retail\channels\apps\framework\core\notificationhandler.ts
            // to parse the error message.  If any format change is made here, update notificationhandler.ts accordingly.
            foreach (PaymentSDK.PaymentError error in response.Errors)
            {
                //errorsInfo.AppendFormat("\nErrorCode: {0}, ErrorMessage: {1}", error.Code, error.Message);
                errorsInfo.AppendFormat(error.Message);
            }

            // CS by muthait dated 26Dec2014
            //string exceptionMessage = string.Format(
            //    CultureInfo.InvariantCulture,
            //    "{0} failed. Error(s): {1}",
            //    operationName,
            //    errorsInfo);
            string exceptionMessage = string.Format(
               CultureInfo.InvariantCulture,
               "{0}",
               errorsInfo);
            var exception = new PaymentException(errorResourceId, exceptionMessage);
            // CE by muthait dated 26Dec2014
            foreach (PaymentSDK.PaymentError error in response.Errors)
            {
                exception.Data.Add(error.Code.ToString(), error.Message);
            }

            throw exception;
        }

        /// <summary>
        /// Cancels the payment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Payment cancellation response.</returns>
        /// <exception cref="DataValidationException">Invalid request.</exception>
        private static VoidPaymentServiceResponse CancelPayment(VoidPaymentServiceRequest request)
        {
            NetTracer.Information("Calling Payment.Cancel");

            if (request.TenderLine.IsPreProcessed)
            {
                // For preprocessed payments, there is no extra step needed for cancelling the payment.
                request.TenderLine.Status = TenderLineStatus.Voided;
                request.TenderLine.IsVoidable = false;
                return new VoidPaymentServiceResponse(ReEncryptPaymentAuthorizationProperties(request.RequestContext, request.TenderLine, request.Platform));
            }
            
            Request paymentRequest = new Request
            {
                Locale = locale ?? GetLocale(request.RequestContext),
                Properties = GetPaymentPropertiesForCancellation(request).ToArray()
            };

            if (paymentRequest.Properties == null || !paymentRequest.Properties.Any())
            {
                NetTracer.Error("Invalid request");
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "Invalid request.");
            }

            PaymentProperty[] updatedPaymentRequestProperties = paymentRequest.Properties;

            string connectorName;
            updatedPaymentRequestProperties = AddConnectorPropertiesByServiceAccountId(request.RequestContext, updatedPaymentRequestProperties, out connectorName);

            paymentRequest.Properties = updatedPaymentRequestProperties;

            // Get payment processor
            IPaymentProcessor processor = CardPaymentService.GetPaymentProcessor(connectorName);

            // Run void
            Response response = processor.Void(paymentRequest);
            VerifyResponseResult("Void", response, GenericNamespace.VoidResponse, VoidResponseProperties.VoidResult, new List<string>() { VoidResult.Success.ToString() }, PaymentErrors.UnableToCancelPayment);
            NetTracer.Information("Completed Payment.Cancel");

            return new VoidPaymentServiceResponse(GetTenderLineForVoid(request));
        }

        //NS by muthait dated 10Sep2014
        private static IEnumerable<PaymentProperty> GetAshleyEnhancedAuthorizationProperties(PaymentCard paymentCard)
        {
            PaymentProperty paymentProperty = new PaymentProperty();
            List<PaymentProperty> paymentProperties = new List<PaymentProperty>();

            //Transaction Id
            if (!string.IsNullOrEmpty(paymentCard["TranId"].ToString()))
            {
                paymentProperty = new PaymentProperty(
                   GenericNamespace.TransactionData,
                   TransactionDataProperties.ExternalInvoiceNumber,
                   paymentCard["TranId"].ToString(),
                   SecurityLevel.None);
                paymentProperties.Add(paymentProperty);
            }

            //Is Test Mode
            //if (!string.IsNullOrEmpty(paymentCard["IsTestMode"].ToString()))
            //{
            //    paymentProperty = new PaymentProperty(
            //       GenericNamespace.TransactionData,
            //       TransactionDataProperties.IsTestMode,
            //       paymentCard["IsTestMode"].ToString(),
            //       SecurityLevel.None);
            //    paymentProperties.Add(paymentProperty);
            //}

            //Transaction Id
            if (paymentCard["LineConfirmationGroupId"] != null)
            {
                paymentProperty = new PaymentProperty(
                   GenericNamespace.TransactionData,
                   "LineConfirmationGroupId",
                   paymentCard["LineConfirmationGroupId"].ToString(),
                   SecurityLevel.None);
                paymentProperties.Add(paymentProperty);
            }
            return paymentProperties;
        }
        //NE by muthait dated 10Sep2014

        /// <summary>
        /// Gets the payment connector properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="paymentProperties">The payment properties.</param>
        /// <param name="connectorName">Name of the connector.</param>
        /// <returns>Payment properties associated with the payment connector.</returns>
        /// <exception cref="ConfigurationException">No payment connectors loaded.</exception>
        /// <exception cref="DataValidationException">
        /// Invalid request for Payment.Authorize currency missing
        /// or
        /// Invalid request for Payment.Authorize CardType missing
        /// or
        /// Connector to service request not found!
        /// </exception>
        private static IEnumerable<PaymentProperty> GetConnectorProperties(RequestContext context, IEnumerable<PaymentProperty> paymentProperties, out string connectorName)
        {
            List<PaymentProperty> enhancedAuthorizationProperties = new List<PaymentProperty>();

            var paymentConnectorConfigs = GetMerchantConnectorInformation(context);
            if (paymentConnectorConfigs == null || paymentConnectorConfigs.Count == 0)
            {
                NetTracer.Error("No payment connectors loaded.");
                throw new ConfigurationException(ConfigurationErrors.PaymentConnectorNotFound, "No payment connectors loaded.");
            }

            // Get currency for request
            string requestCurrency;
            PaymentSDK.Hashtable requestHashtable = PaymentProperty.ConvertToHashtable(paymentProperties.ToArray());
            if (PaymentProperty.GetPropertyValue(requestHashtable, GenericNamespace.TransactionData, TransactionDataProperties.CurrencyCode, out requestCurrency)
                && !string.IsNullOrWhiteSpace(requestCurrency))
            {
                requestCurrency = requestCurrency.ToUpper();
            }
            else
            {
                NetTracer.Error("Invalid request for Payment.Authorize currency missing");
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "Invalid request for Payment.Authorize currency missing");
            }

            // Get cardtype from request
            string requestCardType;
            if (PaymentProperty.GetPropertyValue(requestHashtable, GenericNamespace.PaymentCard, PaymentCardProperties.CardType, out requestCardType)
                && !string.IsNullOrWhiteSpace(requestCardType))
            {
                requestCardType = requestCardType.ToUpper();
            }
            else
            {
                NetTracer.Error("Invalid request for Payment.Authorize CardType missing");
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "Invalid request for Payment.Authorize CardType missing");
            }

            // Find the first connector that supports the currency and cardtype
            connectorName = string.Empty;

            foreach (PaymentConnectorConfiguration paymentConnectorConfig in paymentConnectorConfigs)
            {
                PaymentProperty[] properties = PaymentProperty.ConvertXMLToPropertyArray(paymentConnectorConfig.ConnectorProperties);
                PaymentSDK.Hashtable merchantConnectorInformation = PaymentProperty.ConvertToHashtable(properties);

                string currencies;
                if (PaymentProperty.GetPropertyValue(merchantConnectorInformation, GenericNamespace.MerchantAccount, MerchantAccountProperties.SupportedCurrencies, out currencies)
                    && !string.IsNullOrWhiteSpace(currencies))
                {
                    if (currencies.ToUpper().Contains(requestCurrency))
                    {
                        string cardTypes;
                        if (PaymentProperty.GetPropertyValue(merchantConnectorInformation, GenericNamespace.MerchantAccount, MerchantAccountProperties.SupportedTenderTypes, out cardTypes)
                            && !string.IsNullOrWhiteSpace(cardTypes))
                        {
                            if (cardTypes.ToUpper().Contains(requestCardType))
                            {
                                connectorName = paymentConnectorConfig.Name;
                                enhancedAuthorizationProperties.AddRange(properties);
                                PaymentProperty testModeProperty = new PaymentProperty(
                                    GenericNamespace.TransactionData,
                                    TransactionDataProperties.IsTestMode,
                                    paymentConnectorConfig.IsTestMode.ToString(),
                                    SecurityLevel.None);

                                enhancedAuthorizationProperties.Add(testModeProperty);
                                break;
                            }
                        }
                    }
                }
                else
                {
                    NetTracer.Warning("Connector {0} missing supported currencies property", paymentConnectorConfig.Name);
                }
            }

            if (string.IsNullOrEmpty(connectorName))
            {
                NetTracer.Error("Connector to service request not found!");
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "Connector to service request not found!");
            }

            return enhancedAuthorizationProperties;
        }

        /// <summary>
        /// Adds the merchant specific payment connector properties by service account identifier, and returns updated payment properties.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="paymentRequestProperties">The payment properties of the request.</param>
        /// <param name="connectorName">Name of the connector.</param>
        /// <returns>Updated payment properties.</returns>
        /// <exception cref="DataValidationException">Thrown if the ServiceAccountId was not specified in the request.</exception>
        /// <exception cref="ConfigurationException">Thrown if no payment connectors with the requested ServiceAccountId could be found.</exception>
        private static PaymentProperty[] AddConnectorPropertiesByServiceAccountId(RequestContext context, PaymentProperty[] paymentRequestProperties, out string connectorName)
        {
            connectorName = string.Empty;

            PaymentSDK.Hashtable requestHashtable = PaymentProperty.ConvertToHashtable(paymentRequestProperties);

            string requestedServiceAccountId;
            if (!PaymentProperty.GetPropertyValue(requestHashtable, GenericNamespace.MerchantAccount, MerchantAccountProperties.ServiceAccountId, out requestedServiceAccountId)
                || string.IsNullOrWhiteSpace(requestedServiceAccountId))
            {
                NetTracer.Error("Invalid request missing ServiceAccountId");
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "ServiceAccountId not found.");
            }

            // Need to load correct merchant account details
            List<PaymentProperty> updatedPaymentRequestProperties = paymentRequestProperties.ToList();

            var paymentConnectorConfigs = GetMerchantConnectorInformation(context);
            bool wasRequestedServiceAccountIdFound = false;
            foreach (PaymentConnectorConfiguration connectorConfig in paymentConnectorConfigs)
            {
                PaymentProperty[] connectorProperties = PaymentProperty.ConvertXMLToPropertyArray(connectorConfig.ConnectorProperties);
                PaymentSDK.Hashtable connectorPropertiesHashTable = PaymentProperty.ConvertToHashtable(connectorProperties);
                string foundServiceId;

                if (PaymentProperty.GetPropertyValue(connectorPropertiesHashTable, GenericNamespace.MerchantAccount, MerchantAccountProperties.ServiceAccountId, out foundServiceId)
                    && !string.IsNullOrWhiteSpace(foundServiceId))
                {
                    if (foundServiceId == requestedServiceAccountId)
                    {
                        connectorName = connectorConfig.Name;
                        wasRequestedServiceAccountIdFound = true;
                        foreach (PaymentProperty connectorProperty in connectorProperties)
                        {
                            if (!updatedPaymentRequestProperties.Contains(connectorProperty))
                            {
                                updatedPaymentRequestProperties.Add(connectorProperty);
                            }
                        }

                        break;
                    }
                }
            }

            if (!wasRequestedServiceAccountIdFound)
            {
                string message = string.Format("No payment connector configurations were found that have the requested ServiceAccountId : '{0}'.", requestedServiceAccountId);
                throw new ConfigurationException(ConfigurationErrors.PaymentConnectorNotFound, message);
            }

            PaymentProperty connectorNamePaymentProperty = new PaymentProperty(GenericNamespace.Connector, ConnectorProperties.ConnectorName, connectorName, SecurityLevel.None);

            var previousConnectorNameProperty = updatedPaymentRequestProperties.SingleOrDefault(i => (i.Namespace.Equals(connectorNamePaymentProperty.Namespace) && i.Name.Equals(connectorNamePaymentProperty.Name)));
            if (previousConnectorNameProperty == null)
            {
                updatedPaymentRequestProperties.Add(connectorNamePaymentProperty);
            }
            else
            {
                previousConnectorNameProperty.StringValue = connectorName;
            }

            return updatedPaymentRequestProperties.ToArray();
        }

        /// <summary>
        /// Supported the card types.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Response containing supported card types.</returns>
        private static SupportedCardTypesPaymentServiceResponse SupportedCardTypes(SupportedCardTypesPaymentServiceRequest request)
        {
            NetTracer.Information("Calling Payment.SupportedCardTypes");
            if (request == null || string.IsNullOrWhiteSpace(request.Currency))
            {
                NetTracer.Error("Invalid request");
                throw new DataValidationException(DataValidationErrors.RequiredValueNotFound, "Invalid request.");
            }

            var merchantConnectorInformationList = GetMerchantConnectorInformation(request.RequestContext);
            if (merchantConnectorInformationList == null || !merchantConnectorInformationList.Any())
            {
                NetTracer.Error("No payment connectors loaded.");
                throw new ConfigurationException(ConfigurationErrors.PaymentConnectorNotFound, "No payment connectors loaded.");
            }

            Collection<string> responseCardTypes = new Collection<string>();

            foreach (PaymentConnectorConfiguration item in merchantConnectorInformationList)
            {
                PaymentProperty[] properties = PaymentProperty.ConvertXMLToPropertyArray(item.ConnectorProperties);
                PaymentSDK.Hashtable merchantConnectorInformation = PaymentProperty.ConvertToHashtable(properties);
                string currencies;
                if (PaymentProperty.GetPropertyValue(
                    merchantConnectorInformation,
                    GenericNamespace.MerchantAccount,
                    MerchantAccountProperties.SupportedCurrencies,
                    out currencies) && !string.IsNullOrWhiteSpace(currencies))
                {
                    string requestCurrency = request.Currency.ToUpper();
                    if (currencies.ToUpper().Contains(requestCurrency))
                    {
                        string cardTypes;
                        if (PaymentProperty.GetPropertyValue(
                            merchantConnectorInformation,
                            GenericNamespace.MerchantAccount,
                            MerchantAccountProperties.SupportedTenderTypes,
                            out cardTypes) && !string.IsNullOrWhiteSpace(cardTypes))
                        {
                            string[] splitCardTypes = cardTypes.Split(';');
                            foreach (string cardTypeItem in splitCardTypes)
                            {
                                if (!responseCardTypes.Contains(cardTypeItem))
                                {
                                    responseCardTypes.Add(cardTypeItem);
                                }
                            }
                        }
                        else
                        {
                            NetTracer.Warning("Connector {0} missing supported card types", item.Name);
                        }
                    }
                }
                else
                {
                    NetTracer.Warning("Connector {0} missing supported currencies property", item.Name);
                }
            }

            NetTracer.Information("Completed Payment.SupportedCardTypes");
            return new SupportedCardTypesPaymentServiceResponse(new ReadOnlyCollection<string>(responseCardTypes));
        }

        private static Collection<PaymentConnectorConfiguration> GetMerchantConnectorInformation(RequestContext context)
        {
            var merchantConnectorInformationList = new Collection<PaymentConnectorConfiguration>();

            var connectorConfigs = new Collection<PaymentConnectorConfiguration>();
            if (IsRequestFromTerminal(context))
            {
                if (context.GetTerminal() != null)
                {
                    GetPaymentConnectorDataRequest dataRequest = new GetPaymentConnectorDataRequest(context.GetTerminal().TerminalId);
                    connectorConfigs.Add(context.Execute<SingleEntityDataServiceResponse<PaymentConnectorConfiguration>>(dataRequest).Entity);
                }
            }
            else
            {
                long currentChannelId = context.GetPrincipal().ChannelId;

                // Get all setup merchant payment connector settings for a channel
                GetPaymentConnectorConfigurationDataRequest configurationRequest = new GetPaymentConnectorConfigurationDataRequest(currentChannelId);
                IEnumerable<PaymentConnectorConfiguration> connectors = context.Execute<EntityDataServiceResponse<PaymentConnectorConfiguration>>(configurationRequest).EntityCollection;
                connectorConfigs.AddRange(connectors);
            }

            if (!connectorConfigs.Any())
            {
                throw new ConfigurationException(ConfigurationErrors.PaymentConnectorNotFound, "No payment connectors found.");
            }

            foreach (PaymentConnectorConfiguration item in connectorConfigs)
            {
                if (item == null || string.IsNullOrEmpty(item.Name) || string.IsNullOrEmpty(item.ConnectorProperties))
                {
                    continue;
                }

                NetTracer.Information("Adding merchant information {0}", item.Name);

                // Save merchant data
                merchantConnectorInformationList.Add(item);
            }

            LoadAllSetupConnectors(merchantConnectorInformationList.ToArray());

            return merchantConnectorInformationList;
        }

        /// <summary>
        /// Loads all payment connector assemblies.
        /// </summary>
        /// <param name="paymentConfigs">The payment configurations.</param>
        private static void LoadAllSetupConnectors(params PaymentConnectorConfiguration[] paymentConfigs)
        {
            List<string> connectors = new List<string>();

            foreach (PaymentConnectorConfiguration item in paymentConfigs)
            {
                PaymentProperty[] properties = PaymentProperty.ConvertXMLToPropertyArray(item.ConnectorProperties);
                PaymentSDK.Hashtable merchantConnectorInformation = PaymentProperty.ConvertToHashtable(properties);

                // CRT now only loads the PCL version of the payment connectors
                // Preference is to load the new portable assembly name, else use the old property
                string connectorAssemblyName;
                if (!PaymentProperty.GetPropertyValue(
                    merchantConnectorInformation,
                    GenericNamespace.MerchantAccount,
                    MerchantAccountProperties.PortableAssemblyName,
                    out connectorAssemblyName))
                {
                    // Get old assembly names for connectors
                    PaymentProperty.GetPropertyValue(
                        merchantConnectorInformation,
                        GenericNamespace.MerchantAccount,
                        MerchantAccountProperties.AssemblyName,
                        out connectorAssemblyName);
                }

                if (!string.IsNullOrEmpty(connectorAssemblyName))
                {
                    if (!connectors.Contains(connectorAssemblyName))
                    {
                        NetTracer.Information("Adding assemblies to load {0}", connectorAssemblyName);
                        connectors.Add(connectorAssemblyName);
                    }
                }
            }

            // Load all setup connectors
            if (connectors.Any())
            {
                try
                {
                    NetTracer.Information("Loading connectors");
                    PaymentProcessorManager.Create(connectors.ToArray());
                }
                catch (Exception ex)
                {
                    // Swallow exception so CRT doesn't crash
                    NetTracer.Error(ex, "Failed to load connectors with following error {0}", ex.Message);
                }
            }
        }

        /// <summary>
        /// Determines whether the current request is coming from a terminal.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>
        /// If <c>true</c> the request originated from a terminal.
        /// </returns>
        private static bool IsRequestFromTerminal(RequestContext context)
        {
            return context != null && context.GetPrincipal() != null && context.GetPrincipal().TerminalId > 0;
        }

        /// <summary>
        /// Gets the locale.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>The current locale.</returns>
        private static string GetLocale(RequestContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            GetDefaultLanguageIdDataRequest languageIdRequest = new GetDefaultLanguageIdDataRequest();
            return locale = context.Execute<SingleEntityDataServiceResponse<string>>(languageIdRequest).Entity;
        }

        /// <summary>
        /// Gets the payment properties required for authorization.
        /// </summary>
        /// <param name="tenderLine">The tender line.</param>
        /// <param name="paymentCard">Payment card information.</param>
        /// <param name="cardTypeConfiguration">Card type configuration.</param>
        /// <param name="isCardTokenRequired">Indicates whether a card token is required.</param>
        /// <param name="allowPartialAuthorization">If set to <c>true</c> [then allow partial authorization].</param>
        /// <returns>
        /// A list of payment properties that would be required for a card authorization request.
        /// </returns>
        private static List<PaymentProperty> GetPaymentPropertiesForAuthorizationAndRefund(TenderLine tenderLine, PaymentCard paymentCard, CardTypeInfo cardTypeConfiguration, bool isCardTokenRequired, bool allowPartialAuthorization)
        {
            if (tenderLine == null)
            {
                throw new ArgumentNullException("tenderLine");
            }

            if (paymentCard == null)
            {
                throw new ArgumentNullException("tenderLine", "Payment card info cannot be null inside tender line.");
            }

            List<PaymentProperty> paymentProperties = new List<PaymentProperty>();

            // Industry type.
            PaymentProperty paymentProperty = new PaymentProperty(
                GenericNamespace.TransactionData,
                TransactionDataProperties.IndustryType,
                IndustryType.Retail.ToString(),
                SecurityLevel.None);
            paymentProperties.Add(paymentProperty);

            // Amount.
            paymentProperty = new PaymentProperty(
                GenericNamespace.TransactionData,
                TransactionDataProperties.Amount,
                Math.Abs(tenderLine.Amount), // for refunds request amount must be positive
                SecurityLevel.None);
            paymentProperties.Add(paymentProperty);

            // Currency code.
            paymentProperty = new PaymentProperty(
                GenericNamespace.TransactionData,
                TransactionDataProperties.CurrencyCode,
                tenderLine.Currency,
                SecurityLevel.None);
            paymentProperties.Add(paymentProperty);

            // Is cart tokenization supported?
            paymentProperty = new PaymentProperty(
                GenericNamespace.TransactionData,
                TransactionDataProperties.SupportCardTokenization,
                isCardTokenRequired.ToString(),
                SecurityLevel.None);
            paymentProperties.Add(paymentProperty);

            // Is partial authorization allowed?
            paymentProperty = new PaymentProperty(
                GenericNamespace.TransactionData,
                TransactionDataProperties.AllowPartialAuthorization,
                allowPartialAuthorization.ToString(),
                SecurityLevel.None);
            paymentProperties.Add(paymentProperty);

            // Card number.
            paymentProperty = new PaymentProperty(
                GenericNamespace.PaymentCard,
                PaymentCardProperties.CardNumber,
                paymentCard.CardNumber,
                SecurityLevel.PCI);
            paymentProperties.Add(paymentProperty);

            //NS by muthait dated 10Sep2014
            // Card Token
            if (!string.IsNullOrEmpty(paymentCard["CardToken"] as string))
            {
                paymentProperty = new PaymentProperty(
                  GenericNamespace.PaymentCard,
                  PaymentCardProperties.CardToken,
                  paymentCard["CardToken"].ToString(),
                  SecurityLevel.PCI);
                paymentProperties.Add(paymentProperty);
            }

            //Transaction Id
            if (!string.IsNullOrEmpty(paymentCard["TranId"] as string))
            {
                paymentProperty = new PaymentProperty(
                   GenericNamespace.TransactionData,
                   TransactionDataProperties.ExternalInvoiceNumber,
                   paymentCard["TranId"].ToString(), // for refunds request amount must be positive
                   SecurityLevel.None);
                paymentProperties.Add(paymentProperty);
            }
            //NE by muthait dated 10Sept2014

            if (paymentCard["LineConfirmationGroupId"] != null)
            {
                paymentProperty = new PaymentProperty(
                   GenericNamespace.TransactionData,
                   "LineConfirmationGroupId",
                   paymentCard["LineConfirmationGroupId"].ToString(), 
                   SecurityLevel.None);
                paymentProperties.Add(paymentProperty);
            }

            //CS by spriya : DMND0010113 - Synchrony Financing online
            // Updated to extended Card type.
            AFMRuntimeServices.ExtendedCardType paymentSdkCardType = CardTypeHelper.GetPaymentSdkCardType(cardTypeConfiguration, tenderLine.CardTypeId ?? paymentCard.CardTypes);
            paymentProperty = new PaymentProperty(
                GenericNamespace.PaymentCard,
                PaymentCardProperties.CardType,
                paymentSdkCardType.ToString(),
                SecurityLevel.PCI);
            paymentProperties.Add(paymentProperty);

            // Voice authorization code.
            if (!string.IsNullOrWhiteSpace(paymentCard.VoiceAuthorizationCode))
            {
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.VoiceAuthorizationCode,
                    paymentCard.VoiceAuthorizationCode,
                    SecurityLevel.None);
                paymentProperties.Add(paymentProperty);
            }
            //Updated to extended card type
            if (paymentSdkCardType == AFMRuntimeServices.ExtendedCardType.Debit)
            {
                //CE by spriya : DMND0010113 - Synchrony Financing online
                if (tenderLine.CashBackAmount > 0)
                {
                    // Cashback amount.
                    paymentProperty = new PaymentProperty(
                        GenericNamespace.PaymentCard,
                        PaymentCardProperties.CashBackAmount,
                        tenderLine.CashBackAmount,
                        SecurityLevel.None);
                    paymentProperties.Add(paymentProperty);
                }

                // Encrypted pin.
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.EncryptedPin,
                    paymentCard.EncryptedPin,
                    SecurityLevel.PCI);
                paymentProperties.Add(paymentProperty);

                // Additional security data.
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.AdditionalSecurityData,
                    paymentCard.AdditionalSecurityData,
                    SecurityLevel.PCI);
                paymentProperties.Add(paymentProperty);
            }

            if (!paymentCard.IsSwipe)
            {
                // Card verification value.
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.CardVerificationValue,
                    paymentCard.CCID,
                    SecurityLevel.PCI);
                paymentProperties.Add(paymentProperty);

                // Card expiration year.
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.ExpirationYear,
                    paymentCard.ExpirationYear,
                    SecurityLevel.None);
                paymentProperties.Add(paymentProperty);

                // Card expiration month.
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.ExpirationMonth,
                    paymentCard.ExpirationMonth,
                    SecurityLevel.None);
                paymentProperties.Add(paymentProperty);

                // Card holder name.
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.Name,
                    paymentCard.NameOnCard,
                    SecurityLevel.PII);
                paymentProperties.Add(paymentProperty);

                // Card street address.
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.StreetAddress,
                    paymentCard.Address1,
                    SecurityLevel.PII);
                paymentProperties.Add(paymentProperty);

                // Card postal code.
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.PostalCode,
                    paymentCard.Zip,
                    SecurityLevel.None);
                paymentProperties.Add(paymentProperty);

                //NS by ralandge 5182: FIN-DJ Credit card address is not populating in AX
                // Card city.
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.City,
                    paymentCard.City,
                    SecurityLevel.None);
                paymentProperties.Add(paymentProperty);

                // Card State.
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.State,
                    paymentCard.State,
                    SecurityLevel.None);
                paymentProperties.Add(paymentProperty);
                //NE

                // Card country.
                paymentProperty = GetCountryProperty(paymentCard.Country);
                paymentProperties.Add(paymentProperty);
            }
            else
            {
                // Track1
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.Track1,
                    paymentCard.Track1,
                    SecurityLevel.PCI);
                paymentProperties.Add(paymentProperty);

                // Track2
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.Track2,
                    paymentCard.Track2,
                    SecurityLevel.PCI);
                paymentProperties.Add(paymentProperty);

                // Track3
                paymentProperty = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.Track3,
                    paymentCard.Track3,
                    SecurityLevel.PCI);
                paymentProperties.Add(paymentProperty);
            }

            return paymentProperties;
        }

        /// <summary>
        /// Gets the payment properties for cancel (void).
        /// </summary>
        /// <param name="request">The payment request.</param>
        /// <returns>A list of payment properties that would be required for a card payment capture request.</returns>
        private static IList<PaymentProperty> GetPaymentPropertiesForCancellation(VoidPaymentServiceRequest request)
        {
            List<PaymentProperty> properties = new List<PaymentProperty>();
            PaymentProperty property;

            if (request.PaymentCard != null && !string.IsNullOrEmpty(PaymentCardProperties.CardNumber))
            {
                // Card number.
                property = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.CardNumber,
                    request.PaymentCard.CardNumber,
                    SecurityLevel.PCI);
                properties.Add(property);
            }

            // Currency
            property = new PaymentProperty(
                GenericNamespace.TransactionData,
                TransactionDataProperties.CurrencyCode,
                request.TenderLine.Currency,
                SecurityLevel.None);
            properties.Add(property);
            
            // Extract payment properties from TenderLine.Authorization.
            PaymentProperty[] storedAuthorizationResponseProperties = PaymentProperty.ConvertXMLToPropertyArray(request.TenderLine.Authorization);
            var paymentProperties = CombinePaymentProperties(storedAuthorizationResponseProperties, properties);

            return paymentProperties;
        }

        /// <summary>
        /// Gets the payment properties for payment authorization.
        /// </summary>
        /// <param name="tenderLine">The tender line.</param>
        /// <param name="tokenizedPaymentCard">The tokenized payment card.</param>
        /// <param name="allowPartialAuthorization">If set to <c>true</c> [then allow partial authorization].</param>
        /// <returns>
        /// An array of payment properties required for a valid authorization request.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when tenderLine or tokenizedPaymentCard is null.</exception>
        private static PaymentProperty[] GetPaymentPropertiesForAuthorizationWithToken(TenderLine tenderLine, TokenizedPaymentCard tokenizedPaymentCard, bool allowPartialAuthorization)
        {
            if (tenderLine == null)
            {
                throw new ArgumentNullException("tenderLine");
            }

            if (tokenizedPaymentCard == null)
            {
                throw new ArgumentNullException("tokenizedPaymentCard");
            }

            List<PaymentProperty> paymentProperties = new List<PaymentProperty>();

            PaymentProperty property = new PaymentProperty(GenericNamespace.TransactionData, TransactionDataProperties.Amount, Math.Abs(tenderLine.Amount), SecurityLevel.None); // For refunds request, amount must be positive.
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.TransactionData, TransactionDataProperties.CurrencyCode, tenderLine.Currency, SecurityLevel.None);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.TransactionData, TransactionDataProperties.SupportCardTokenization, true.ToString(), SecurityLevel.None);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.TransactionData, TransactionDataProperties.AllowPartialAuthorization, allowPartialAuthorization.ToString(), SecurityLevel.None);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.CardType, tokenizedPaymentCard.CardTypes, SecurityLevel.PCI);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.Name, tokenizedPaymentCard.NameOnCard, SecurityLevel.PCI);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.ExpirationYear, tokenizedPaymentCard.ExpirationYear, SecurityLevel.PCI);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.ExpirationMonth, tokenizedPaymentCard.ExpirationMonth, SecurityLevel.PCI);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.StreetAddress, tokenizedPaymentCard.Address1, SecurityLevel.PII);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.PostalCode, tokenizedPaymentCard.Zip, SecurityLevel.None);
            paymentProperties.Add(property);

            //NS by ralandge 5182: FIN-DJ Credit card address is not populating in AX
            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.City, tokenizedPaymentCard.City, SecurityLevel.None);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.State, tokenizedPaymentCard.State, SecurityLevel.None);
            paymentProperties.Add(property);
            //NE

            property = GetCountryProperty(tokenizedPaymentCard.Country);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.UniqueCardId, tokenizedPaymentCard.CardTokenInfo.UniqueCardId, SecurityLevel.PCI);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.CardToken, tokenizedPaymentCard.CardTokenInfo.CardToken, SecurityLevel.PCI);
            paymentProperties.Add(property);

            property = new PaymentProperty(GenericNamespace.PaymentCard, PaymentCardProperties.Last4Digits, tokenizedPaymentCard.CardTokenInfo.MaskedCardNumber, SecurityLevel.PCI);
            paymentProperties.Add(property);

            property = new PaymentProperty(
                GenericNamespace.MerchantAccount,
                MerchantAccountProperties.ServiceAccountId,
                tokenizedPaymentCard.CardTokenInfo.ServiceAccountId,
                SecurityLevel.None);
            paymentProperties.Add(property);

            return paymentProperties.ToArray();
        }

        /// <summary>
        /// Gets the payment properties for capture.
        /// </summary>
        /// <param name="tenderLine">The tender line.</param>
        /// <param name="cardNumber">The card number.</param>
        /// <returns>A list of payment properties that would be required for a card payment capture request.</returns>
        private static List<PaymentProperty> GetPaymentPropertiesForCapture(TenderLine tenderLine, string cardNumber)
        {
            List<PaymentProperty> properties = new List<PaymentProperty>();
            PaymentProperty property;

            if (!string.IsNullOrEmpty(cardNumber))
            {
                // Card number.
                property = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.CardNumber,
                    cardNumber,
                    SecurityLevel.PCI);
                properties.Add(property);
            }

            // Amount.
            property = new PaymentProperty(
                GenericNamespace.TransactionData,
                TransactionDataProperties.Amount,
                Math.Abs(tenderLine.Amount), // for refunds request amount must be positive
                SecurityLevel.None);
            properties.Add(property);

            // Currency
            property = new PaymentProperty(
                GenericNamespace.TransactionData,
                TransactionDataProperties.CurrencyCode,
                tenderLine.Currency,
                SecurityLevel.None);
            properties.Add(property);

            // Extract payment properties from TenderLine.Authorization.
            PaymentProperty[] storedAuthorizationResponseProperties = PaymentProperty.ConvertXMLToPropertyArray(tenderLine.Authorization);
            var paymentProperties = CombinePaymentProperties(storedAuthorizationResponseProperties, properties);

            return new List<PaymentProperty>(paymentProperties);
        }

        /// <summary>
        /// Combines the payment properties.
        /// </summary>
        /// <param name="paymentProperties1">First set of payment properties.</param>
        /// <param name="paymentProperty">The payment property.</param>
        /// <returns>
        /// An array which is a combination of the unique elements in two sets of input payment properties.
        /// </returns>
        private static PaymentProperty[] CombinePaymentProperties(IEnumerable<PaymentProperty> paymentProperties1, PaymentProperty paymentProperty)
        {
            return CombinePaymentProperties(paymentProperties1, new[] { paymentProperty });
        }

        /// <summary>
        /// Combines the payment properties.
        /// </summary>
        /// <param name="paymentProperties1">First set of payment properties.</param>
        /// <param name="paymentProperties2">Second set of payment properties.</param>
        /// <returns>An array which is a combination of the unique elements in two sets of input payment properties.</returns>
        private static PaymentProperty[] CombinePaymentProperties(IEnumerable<PaymentProperty> paymentProperties1, IEnumerable<PaymentProperty> paymentProperties2)
        {
            // This logic can be replaced by a hashset to increase performance. 
            // Using HashSet will require changes to PaymentProperty.
            var combinedPaymentProperties = new List<PaymentProperty>(paymentProperties1);

            foreach (var paymentProperty in paymentProperties2)
            {
                if (!combinedPaymentProperties.Contains(paymentProperty))
                {
                    combinedPaymentProperties.Add(paymentProperty);
                }
            }

            return combinedPaymentProperties.ToArray();
        }

        /// <summary>
        /// Gets the tender line from the specified payment properties.
        /// </summary>
        /// <param name="request">Capture request.</param>
        /// <returns>
        /// The tender line.
        /// </returns>
        private static TenderLine GetTenderLineFromCaptureResponse(CapturePaymentServiceRequest request)
        {
            TenderLine tenderLine = request.TenderLine;
            tenderLine.Status = TenderLineStatus.Committed;
            tenderLine.IsVoidable = false; // payment cannot be voided once captured. 
            return tenderLine;
        }

        /// <summary>
        /// Gets the tender line from the specified payment properties.
        /// </summary>
        /// <param name="request">Void request.</param>
        /// <returns>
        /// The tender line.
        /// </returns>
        private static TenderLine GetTenderLineForVoid(VoidPaymentServiceRequest request)
        {
            TenderLine tenderLine = request.TenderLine;
            tenderLine.Status = TenderLineStatus.Voided;
            tenderLine.IsVoidable = false; // payment can not be voided twice.
            return tenderLine;
        }

        /// <summary>
        /// Gets the tender line from the specified payment properties.
        /// </summary>
        /// <param name="request">Request to generate card token.</param>
        /// <param name="hashTable">The payment properties.</param>
        /// <returns>
        /// The tender line.
        /// </returns>
        private static TenderLine GetTenderLineForGeneratedCardToken(GenerateCardTokenPaymentServiceRequest request, PaymentSDK.Hashtable hashTable)
        {
            TenderLine tenderLine = request.TenderLine;

            PaymentProperty property = PaymentProperty.GetPropertyFromHashtable(hashTable, GenericNamespace.PaymentCard, PaymentCardProperties.Last4Digits);
            tenderLine.MaskedCardNumber = (property != null) ? (CardNumberMask + property.StringValue) : string.Empty;

            // Hardware station needs to return the token blob for future use            
            tenderLine.Authorization = PaymentProperty.ConvertPropertyArrayToXML(PaymentProperty.ConvertHashToProperties(hashTable));

            return tenderLine;
        }

        /// <summary>
        /// Gets the tender line from the specified authorization payment properties.
        /// </summary>
        /// <param name="tenderLine">The tender line.</param>
        /// <param name="authorizationProperties">The properties.</param>
        /// <returns>
        /// The updated tender line.
        /// </returns>
        private static TenderLine GetTenderLineFromAuthorizationResponse(TenderLine tenderLine, PaymentProperty[] authorizationProperties)
        {
            tenderLine.Status = TenderLineStatus.PendingCommit;
            tenderLine.IsVoidable = true;

            PaymentSDK.Hashtable hashTable = PaymentProperty.ConvertToHashtable(authorizationProperties);

            PaymentProperty property = PaymentProperty.GetPropertyFromHashtable(hashTable, GenericNamespace.AuthorizationResponse, AuthorizationResponseProperties.ApprovedAmount);
            tenderLine.Amount = (property != null) ? property.DecimalValue : 0.0m;

            property = PaymentProperty.GetPropertyFromHashtable(hashTable, GenericNamespace.AuthorizationResponse, AuthorizationResponseProperties.CashBackAmount);
            tenderLine.CashBackAmount = (property != null) ? property.DecimalValue : 0.0m;

            property = PaymentProperty.GetPropertyFromHashtable(hashTable, GenericNamespace.AuthorizationResponse, AuthorizationResponseProperties.Last4Digits);
            tenderLine.MaskedCardNumber = (property != null) ? (CardNumberMask + property.StringValue) : string.Empty;

            // Some payment connectors may capture in one payment operation.
            property = PaymentProperty.GetPropertyFromHashtable(hashTable, GenericNamespace.CaptureResponse, CaptureResponseProperties.CaptureResult);
            if (property != null && property.StringValue.Equals(CaptureResult.Success.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                // Yes it has been captured, set the tender line status to committed.
                tenderLine.Status = TenderLineStatus.Committed;
            }

            tenderLine.Authorization = PaymentProperty.ConvertPropertyArrayToXML(authorizationProperties);

            return tenderLine;
        }

        /// <summary>
        /// Gets the tender line from the specified refund payment properties.
        /// </summary>
        /// <param name="tenderLine">The tender line.</param>
        /// <param name="refundProperties">The properties.</param>
        /// <returns>
        /// The updated tender line.
        /// </returns>
        private static TenderLine GetTenderLineFromRefundResponse(TenderLine tenderLine, PaymentProperty[] refundProperties)
        {
            tenderLine.Status = TenderLineStatus.Committed; // refund doesn't require capture
            tenderLine.IsVoidable = false; // refund cannot be voided.

            PaymentSDK.Hashtable hashTable = PaymentProperty.ConvertToHashtable(refundProperties);

            PaymentProperty property = PaymentProperty.GetPropertyFromHashtable(hashTable, GenericNamespace.RefundResponse, RefundResponseProperties.Last4Digits);
            tenderLine.MaskedCardNumber = (property != null) ? (CardNumberMask + property.StringValue) : string.Empty;

            return tenderLine;
        }

        /// <summary>
        /// Gets the country property.
        /// </summary>
        /// <param name="countryRegionCode">The country region code.</param>
        /// <returns>Payment property that encapsulates the country code.</returns>
        /// <exception cref="System.ArgumentException">Thrown if countryRegionCode is empty or invalid.</exception>
        /// <exception cref="DataValidationException">Invalid country/region code format.</exception>
        private static PaymentProperty GetCountryProperty(string countryRegionCode)
        {
            if (string.IsNullOrWhiteSpace(countryRegionCode))
            {
                throw new ArgumentException("countryRegionCode is empty.", "countryRegionCode");
            }

            string twoLetterCountryCode;

            if (countryRegionCode.Length == 2)
            {
                twoLetterCountryCode = countryRegionCode;
            }
            else if (countryRegionCode.Length == 3)
            {
                twoLetterCountryCode = countryRegionMapper.ConvertToTwoLetterCountryCode(countryRegionCode);
            }
            else
            {
                throw new DataValidationException(DataValidationErrors.InvalidFormat, "Invalid country/region code format.");
            }

            PaymentProperty property = new PaymentProperty(
                    GenericNamespace.PaymentCard,
                    PaymentCardProperties.Country,
                    twoLetterCountryCode,
                    SecurityLevel.None);
            return property;
        }

        /// <summary>
        /// Fail if card number was manually entered and manual entry is blocked for selected card type.
        /// </summary>
        /// <param name="paymentCardBase">Card information.</param>
        /// <param name="cardTypeInfo">Card type configuration.</param>
        private static void ValidateCardEntry(PaymentCardBase paymentCardBase, CardTypeInfo cardTypeInfo)
        {
            if (!cardTypeInfo.AllowManualInput && !paymentCardBase.IsSwipe)
            {
                throw new PaymentException(PaymentErrors.ManualCardNumberNotAllowed, "The card number for card type '{0}' cannot be manually entered.", cardTypeInfo.TypeId);
            }
        }

        /// <summary>
        /// Re-encrypt the payment authorization properties.
        /// </summary>
        /// <param name="context">Request context.</param>
        /// <param name="tenderline">The tenderline.</param>
        /// <param name="platformString">Platform that is running process.</param>
        /// <returns>The converted tenderline.</returns>
        private static TenderLine ReEncryptPaymentAuthorizationProperties(RequestContext context, TenderLine tenderline, string platformString)
        {
            // Convert tenderline authorizations to local payment SDK settings.
            GetCardPaymentPropertiesServiceRequest propertyRequest = new GetCardPaymentPropertiesServiceRequest(tenderline.Authorization, false, platformString);
            GetCardPaymentPropertiesServiceResponse propertyResponse = context.Execute<GetCardPaymentPropertiesServiceResponse>(propertyRequest);
            tenderline.Authorization = propertyResponse.PortablePaymentPropertyXmlBlob;
            return tenderline;
        }

        /// <summary>
        /// Set the payment SDK runtime platform (Desktop/WindowsPhone etc).
        /// </summary>
        /// <param name="platformString">Which platform.</param>
        private static void SetPaymentSdkPlatform(string platformString)
        {
            if (PaymentSDK.SDKExtensions.Platform == PlatformType.None)
            {
                PaymentSDK.PlatformType platform;
                if (Enum.TryParse<PaymentSDK.PlatformType>(platformString, out platform))
                {
                    PaymentSDK.SDKExtensions.Platform = platform;
                }
                else
                {
                    // Default to desktop if not set by client.
                    PaymentSDK.SDKExtensions.Platform = PlatformType.Desktop;
                }
            }
        }

        /// <summary>
        /// Gets the payment processor by the specified name. Throws an exception if the specified payment processor is not found.
        /// </summary>
        /// <param name="paymentConnectorName">Name of the payment connector.</param>
        /// <returns>The payment processor that was requested.</returns>
        private static IPaymentProcessor GetPaymentProcessor(string paymentConnectorName)
        {
            IPaymentProcessor processor = null;
            try
            {
                processor = PaymentProcessorManager.GetPaymentProcessor(paymentConnectorName);
                if (processor == null)
                {
                    var message = string.Format("The specified payment connector {0} could not be loaded.", paymentConnectorName);
                    ConfigurationException configurationException = new ConfigurationException(ConfigurationErrors.PaymentConnectorNotFound, message);
                    throw configurationException;
                }
            }
            catch (InvalidOperationException exception)
            {
                var message = string.Format("The specified payment connector {0} could not be loaded.", paymentConnectorName);
                ConfigurationException configurationException = new ConfigurationException(ConfigurationErrors.PaymentConnectorNotFound, message, exception);
                throw configurationException;
            }

            return processor;
        }
    }
}