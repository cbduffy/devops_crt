﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Represents an implementation of the store operation service.
    /// </summary>
    public class StoreOperationService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(SaveNonSaleTenderServiceRequest),
                    typeof(SaveDropAndDeclareServiceRequest),
                    typeof(GetNonSaleTenderServiceRequest)
                };
            }
        }

        /// <summary>
        /// Executes the specified service request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;

            if (requestType == typeof(SaveNonSaleTenderServiceRequest))
            {
                response = SaveNonSaleTenderTransactions((SaveNonSaleTenderServiceRequest)request);
            }
            else if (requestType == typeof(GetNonSaleTenderServiceRequest))
            {
                response = GetNonSaleTenderTransactions((GetNonSaleTenderServiceRequest)request);
            }
            else if (requestType == typeof(SaveDropAndDeclareServiceRequest))
            {
                response = SaveDropAndDeclareTransactions((SaveDropAndDeclareServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// Invoke the method to save non sale tender type transactions.
        /// </summary>
        /// <param name="request">Request for non sale tender transactions.</param>
        /// <returns>Returns the non sale tender transactions.</returns>
        private static SaveNonSaleTenderServiceResponse SaveNonSaleTenderTransactions(SaveNonSaleTenderServiceRequest request)
        {
            StoreOperationServiceHelper.ValidateRetailOperationPermission(request);

            var nonSaleTenderDataManager = new NonSaleTenderOperationDataManager(request.RequestContext);

            NonSalesTransaction tenderTransaction = StoreOperationServiceHelper.ConvertToNonSalesTenderTransaction(request.RequestContext, request);

            nonSaleTenderDataManager.Save(tenderTransaction);

            return new SaveNonSaleTenderServiceResponse(tenderTransaction);
        }

        /// <summary>
        /// Invoke the method to save drop and declare transactions.
        /// </summary>
        /// <param name="request">Request context.</param>
        /// <returns>Returns response for save drop and declare.</returns>
        private static SaveDropAndDeclareServiceResponse SaveDropAndDeclareTransactions(SaveDropAndDeclareServiceRequest request)
        {
            StoreOperationServiceHelper.ValidateRetailOperationPermission(request);
            StoreOperationServiceHelper.ValidateTenderDeclarationCountingDifference(request);

            var tenderDropAndDeclareDataManager = new TenderDropAndDeclareDataManager(request.RequestContext);

            DropAndDeclareTransaction tenderDropAndDeclare = StoreOperationServiceHelper.ConvertTenderDropAndDeclareTransaction(request);
            
            tenderDropAndDeclareDataManager.Save(tenderDropAndDeclare);

            return new SaveDropAndDeclareServiceResponse(tenderDropAndDeclare);
        }

        /// <summary>
        /// Invoke the method to get non sale tender transaction list for the given non sale tender type.
        /// </summary>
        /// <param name="request">Request for non sale tender service.</param>
        /// <returns>Returns the response for non sale tender operation get request.</returns>
        private static GetNonSaleTenderServiceResponse GetNonSaleTenderTransactions(GetNonSaleTenderServiceRequest request)
        {
            StoreOperationServiceHelper.ValidateRetailOperationPermission(request);

            var nonSaleTenderDataManager = new NonSaleTenderOperationDataManager(request.RequestContext);

            NonSalesTransaction tenderTransaction = StoreOperationServiceHelper.ConvertToNonSalesTenderTransaction(request.RequestContext, request.ShiftId, request.NonSalesTenderType);

            ReadOnlyCollection<NonSalesTransaction> nonSaleOperationList;

            if (string.IsNullOrWhiteSpace(request.TransactionId))
            {
                nonSaleOperationList = nonSaleTenderDataManager.GetCurrentShiftNonSaleOperations(tenderTransaction);
            }
            else
            {
                nonSaleOperationList = nonSaleTenderDataManager.GetCurrentShiftNonSaleOperations(tenderTransaction, request.TransactionId);
            }

            return new GetNonSaleTenderServiceResponse(nonSaleOperationList);
        }
    }
}
