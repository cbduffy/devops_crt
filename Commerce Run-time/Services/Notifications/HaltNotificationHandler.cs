﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Notifications
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// A simple notification handler that blocks the execution flow.
    /// </summary>
    public sealed class HaltNotificationHandler : INotificationHandler
    {
        /// <summary>
        /// Gets a value indicating whether this notification handler is the default handler to use when no matching 
        /// notification handlers could be found.
        /// </summary>
        public bool IsDefault
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the collection of notification types supported by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedNotificationTypes
        {
            get
            {
                yield return typeof(EmptyOrderDeliveryOptionSetNotification);
                yield return typeof(EmptyProductDeliveryOptionSetNotification);
                yield return typeof(ProductDiscontinuedFromChannelNotification);
                yield return typeof(ReasonCodeRequiredNotification);
                yield return typeof(UnableToResolveListingIdsNotification);
            }
        }

        /// <summary>
        /// Executes the logic within the specified context.
        /// </summary>
        /// <param name="notification">The notification.</param>
        /// <returns>
        /// Action to take by the client code. If <c>true</c> the error is critical and execution must stop. Otherwise, <c>false</c>.
        /// </returns>
        public bool Execute(Notification notification)
        {
            // Halt the flow.
            return true;
        }
    }
}