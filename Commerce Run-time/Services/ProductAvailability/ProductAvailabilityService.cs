﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProductAvailabilityService.cs" company="Microsoft Corporation">
//   Copyright (C) Microsoft Corporation.  All rights reserved.
//  THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
//  OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
//  THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
//  NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
// </copyright>
// <summary>
//   Implementation for product availability service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using Microsoft.Dynamics.Retail.Diagnostics;
    
    /// <summary>
    /// Implementation for product availability service.
    /// </summary>
    public class ProductAvailabilityService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetStoreAvailabilityServiceRequest),
                    typeof(GetItemAvailabilitiesByItemsServiceRequest),
                    typeof(GetItemAvailabilitiesByItemQuantitiesServiceRequest),
                    typeof(GetItemAvailabilitiesByItemWarehousesServiceRequest),
                    typeof(GetItemAvailableQuantitiesByItemsServiceRequest)
                };
            }
        }

        /// <summary>
        /// Entry point to Availability service. Takes a Availability service request and returns the result of the request execution.
        /// </summary>
        /// <param name="request">The Availability service request to execute.</param>
        /// <returns>Result of executing request, or null object for void operations.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;
            if (requestType == typeof(GetItemAvailabilitiesByItemsServiceRequest))
            {
                response = GetItemAvailabilitiesByItems((GetItemAvailabilitiesByItemsServiceRequest)request);
            }
            else if (requestType == typeof(GetItemAvailabilitiesByItemQuantitiesServiceRequest))
            {
                response = GetItemAvailabilitiesByItemQuantities((GetItemAvailabilitiesByItemQuantitiesServiceRequest)request);
            }
            else if (requestType == typeof(GetItemAvailabilitiesByItemWarehousesServiceRequest))
            {
                response = GetItemAvailabilitiesByItemWarehouses((GetItemAvailabilitiesByItemWarehousesServiceRequest)request);
            }
            else if (requestType == typeof(GetItemAvailableQuantitiesByItemsServiceRequest))
            {
                response = GetItemAvailableQuantitiesByItems((GetItemAvailableQuantitiesByItemsServiceRequest)request);
            }
            else if (requestType == typeof(GetStoreAvailabilityServiceRequest))
            {
                response = GetStoreAvailability((GetStoreAvailabilityServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request type '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        private static GetStoreAvailabilityServiceResponse GetStoreAvailability(GetStoreAvailabilityServiceRequest serviceRequest)
        {
            var getStoreAvailabilityRealtimeRequest = new GetStoreAvailabilityRealtimeRequest(serviceRequest.ItemId, serviceRequest.VariantId);
            ReadOnlyCollection<ItemAvailableStore> availabilities = serviceRequest.RequestContext.Execute<EntityDataServiceResponse<ItemAvailableStore>>(getStoreAvailabilityRealtimeRequest).EntityCollection;
            return new GetStoreAvailabilityServiceResponse(availabilities);
        }

        private static GetItemAvailabilitiesByItemsServiceResponse GetItemAvailabilitiesByItems(GetItemAvailabilitiesByItemsServiceRequest request)
        {
            var settings = request.QueryResultSettings ?? new QueryResultSettings();
            IEnumerable<ItemAvailability> itemAvailabilities = new Collection<ItemAvailability>();

            try
            {
                var searchCriteria = new ItemAvailabilitiesQueryCriteria(request.CustomerAccountNumber, request.Items, includeQuantities: false);
                var dataRequest = new GetItemAvailabilitiesDataRequest(searchCriteria, settings);
                itemAvailabilities = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<ItemAvailability>>(dataRequest, request.RequestContext).EntityCollection;
            }
            catch (StorageException exception)
            {
                // supress data service exception in order not to block the order
                // raise the notification instead
                UnableToDetermineQuantityNotification notification = new UnableToDetermineQuantityNotification(request.Items);

                if (request.RequestContext.Runtime.Notify(request.RequestContext, notification))
                {
                    throw new NotificationException(
                        NotificationErrors.StorageError,
                        notification,
                        exception.Message,
                        exception);
                }
            }

            return new GetItemAvailabilitiesByItemsServiceResponse(itemAvailabilities, settings.Paging.TotalNumberOfRecords);
        }

        private static GetItemAvailabilitiesByItemQuantitiesServiceResponse GetItemAvailabilitiesByItemQuantities(GetItemAvailabilitiesByItemQuantitiesServiceRequest request)
        {
            var settings = request.QueryResultSettings ?? new QueryResultSettings();
            IEnumerable<ItemAvailability> itemAvailabilities = new Collection<ItemAvailability>();

            try
            {
                var searchCriteria = new ItemAvailabilitiesQueryCriteria(request.ItemQuantities, request.CustomerAccountNumber, request.MaxWarehousesPerItem);
                var dataRequest = new GetItemAvailabilitiesDataRequest(searchCriteria, settings);
                itemAvailabilities = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<ItemAvailability>>(dataRequest, request.RequestContext).EntityCollection;
            }
            catch (StorageException exception)
            {
                // supress exception in order not to block the order
                // raise the notification instead
                UnableToDetermineQuantityNotification notification = new UnableToDetermineQuantityNotification(request.ItemQuantities.Select(itemQuantity => itemQuantity.GetItem()));

                if (request.RequestContext.Runtime.Notify(request.RequestContext, notification))
                {
                    throw new NotificationException(
                        NotificationErrors.StorageError,
                        notification,
                        exception.Message,
                        exception);
                }
            }

            return new GetItemAvailabilitiesByItemQuantitiesServiceResponse(itemAvailabilities, settings.Paging.TotalNumberOfRecords);
        }

        private static GetItemAvailableQuantitiesByItemsServiceResponse GetItemAvailableQuantitiesByItems(GetItemAvailableQuantitiesByItemsServiceRequest request)
        {
            var settings = request.QueryResultSettings ?? new QueryResultSettings();
            IEnumerable<ItemAvailableQuantity> itemAvailableQuantities = new Collection<ItemAvailableQuantity>();

            try
            {
                var searchCriteria = new ItemAvailabilitiesQueryCriteria(request.CustomerAccountNumber, request.Items, includeQuantities: true);
                var dataRequest = new GetItemAvailabilitiesDataRequest(searchCriteria, settings);
                itemAvailableQuantities = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<ItemAvailableQuantity>>(dataRequest, request.RequestContext).EntityCollection;
            }
            catch (StorageException exception)
            {
                NetTracer.Error(exception, "Error while executing GetItemAvailableQuantitiesByItems");
                
                // supress exception in order not to block the order
                // raise the notification instead
                UnableToDetermineQuantityNotification notification = new UnableToDetermineQuantityNotification(request.Items);

                if (request.RequestContext.Runtime.Notify(request.RequestContext, notification))
                {
                    throw new NotificationException(
                        NotificationErrors.StorageError,
                        notification,
                        exception.Message,
                        exception);
                }
            }

            return new GetItemAvailableQuantitiesByItemsServiceResponse(itemAvailableQuantities, settings.Paging.TotalNumberOfRecords);
        }

        private static GetItemAvailabilitiesByItemWarehousesServiceResponse GetItemAvailabilitiesByItemWarehouses(GetItemAvailabilitiesByItemWarehousesServiceRequest request)
        {
            QueryResultSettings settings = request.QueryResultSettings ?? new QueryResultSettings();
            IEnumerable<ItemAvailability> itemAvailabilities = new Collection<ItemAvailability>();

            try
            {
                var searchCriteria = new ItemAvailabilitiesQueryCriteria(request.ItemWarehouses);
                var dataRequest = new GetItemAvailabilitiesDataRequest(searchCriteria, settings);
                itemAvailabilities = request.RequestContext.Runtime.Execute<EntityDataServiceResponse<ItemAvailability>>(dataRequest, request.RequestContext).EntityCollection;
            }
            catch (StorageException exception)
            {
                // supress exception in order not to block the order
                // raise the notification instead
                UnableToDetermineQuantityForStoresNotification notification = new UnableToDetermineQuantityForStoresNotification(request.ItemWarehouses);

                if (request.RequestContext.Runtime.Notify(request.RequestContext, notification))
                {
                    throw new NotificationException(
                        NotificationErrors.StorageError,
                        notification,
                        exception.Message,
                        exception);
                }
            }

            return new GetItemAvailabilitiesByItemWarehousesServiceResponse(itemAvailabilities, settings.Paging.TotalNumberOfRecords);
        }
    }
}
