﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Implementation for channel service.
    /// </summary>
    public class ChannelService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(UpdateChannelPublishStatusServiceRequest)
                };
            }
        }

        /// <summary>
        /// Entry point to Channel service.
        /// </summary>
        /// <param name="request">The service request to execute.</param>
        /// <returns>Result of executing request, or null object for void operations.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;
            if (requestType == typeof(UpdateChannelPublishStatusServiceRequest))
            {
                response = UpdateChannelPublishStatus((UpdateChannelPublishStatusServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request type '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// Updates the channel publish status.
        /// </summary>
        /// <param name="request">The service request.</param>
        /// <returns>The service response.</returns>
        private static NullResponse UpdateChannelPublishStatus(UpdateChannelPublishStatusServiceRequest request)
        {
            ChannelDataManager manager = new ChannelDataManager(request.RequestContext);
            var channel = manager.GetOnlineChannelById(request.ChannelId, new ColumnSet());
            manager.UpdatePublishStatus(request.ChannelId, request.PublishStatus, request.PublishStatusMessage);

            try
            {
                var updateChannelPublishingStatusRequest = new UpdateChannelPublishingStatusRealtimeRequest(request.ChannelId, request.PublishStatus, request.PublishStatusMessage);
                request.RequestContext.Execute<NullResponse>(updateChannelPublishingStatusRequest);
            }
            catch (CommerceRuntimeException)
            {
                // Revert the link status if it fails to update AX via transaction service.
                manager.UpdatePublishStatus(request.ChannelId, channel.PublishStatus, channel.PublishStatusMessage);
                throw;
            }

            return new NullResponse();
        }
    }
}
