﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Encapuslates the implementation of the logging service.
    /// </summary>
    public class LoggingService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(InsertAuditLogServiceRequest)
                };
            }
        }

        /// <summary>
        /// Execute the service request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        /// The response.
        /// </returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;

            if (requestType == typeof(InsertAuditLogServiceRequest))
            {
                response = InsertAuditLog((InsertAuditLogServiceRequest)request);
            }
            else
            {
                NetTracer.Error("Unsupported request type for logging.");
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// Inserts Audit Log.
        /// </summary>
        /// <param name="request">The audit log request.</param>
        /// <returns>The response.</returns>
        private static NullResponse InsertAuditLog(InsertAuditLogServiceRequest request)
        {
            try
            {
                var context = request.RequestContext;
                long channelId = context.GetChannelId();
                if (channelId == CommercePrincipal.DefaultChannelID && context.GetPrincipal() != null)
                {
                    // LogOn and LogOff events store channel identifier in different places.
                    channelId = context.GetPrincipal().ChannelId;
                }

                if (channelId == CommercePrincipal.DefaultChannelID)
                {
                    // If channel of audit is unknown then logging cannot be done.
                    return new NullResponse();
                }

                Terminal terminal = context.GetTerminal();
                if (terminal == null)
                {
                    // If there is still no clarity of the audit log terminal, that is a requirement, then exit.
                    return new NullResponse();
                }

                string terminalId = terminal.TerminalId;

                var deviceConfigurationDataRequest = new GetDeviceConfigurationDataServiceRequest(channelId, terminalId);
                DeviceConfiguration deviceConfiguration = context.Execute<SingleEntityDataServiceResponse<DeviceConfiguration>>(deviceConfigurationDataRequest).Entity;

                if (deviceConfiguration != null && deviceConfiguration.AuditEnabled)
                {
                    string storeId = string.Empty;
                    if (request.RequestContext.GetOrgUnit() != null)
                    {
                        storeId = request.RequestContext.GetOrgUnit().OrgUnitNumber ?? string.Empty;
                    }

                    if (string.IsNullOrEmpty(storeId))
                    {
                        storeId = deviceConfiguration.InventLocationId ?? string.Empty;
                    }

                    if (string.IsNullOrEmpty(storeId))
                    {
                        GetStoreDataServiceRequest orgUnitRequest = new GetStoreDataServiceRequest(context.GetPrincipal().ChannelId);
                        OrgUnit orgUnit = context.Runtime.Execute<SingleEntityDataServiceResponse<OrgUnit>>(orgUnitRequest, context, skipRequestPipeline: true).Entity;
                        storeId = orgUnit.OrgUnitNumber ?? string.Empty;
                    }

                    var dataRequest = new InsertAuditLogDataRequest(
                        request.Source,
                        request.LogEntry,
                        request.LogLevel,
                        storeId,
                        terminalId,
                        request.DurationInMilliseconds);

                    context.Execute<NullResponse>(dataRequest);
                }
            }
            catch (Exception ex)
            {
                NetTracer.Error(ex, "Unable to write the audit entry to the database.");
            }

            return new NullResponse();
        }
    }
}
