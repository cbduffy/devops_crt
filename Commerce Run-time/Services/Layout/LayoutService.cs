﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// The layout service.
    /// </summary>
    public class LayoutService : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(GetButtonGridsServiceRequest),
                    typeof(GetButtonGridByIdServiceRequest),
                    typeof(GetButtonGridsByIdsServiceRequest)
                };
            }
        }

        /// <summary>
        /// Implementation of button grid service.
        /// </summary>
        /// <param name="request">The request object.</param>
        /// <returns>The response object.</returns>
        public Response Execute(Request request)
        {
            Response response;

            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            ServicesHelper.ValidateInboundRequest(request);
            Type requestType = request.GetType();

            if (requestType == typeof(GetButtonGridByIdServiceRequest))
            {
                response = GetButtonGridById((GetButtonGridByIdServiceRequest)request);
            }
            else if (requestType == typeof(GetButtonGridsServiceRequest))
            {
                response = GetButtonGrids((GetButtonGridsServiceRequest)request);
            }
            else if (requestType == typeof(GetButtonGridsByIdsServiceRequest))
            {
                response = GetButtonGridByIds((GetButtonGridsByIdsServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Request '{0}' is not supported.", request.GetType().FullName));
            }

            return response;
        }

        /// <summary>
        /// Gets a button grid.
        /// </summary>
        /// <param name="request">Request instance.</param>
        /// <returns>Response instance.</returns>
        private static GetButtonGridByIdServiceResponse GetButtonGridById(GetButtonGridByIdServiceRequest request)
        {
            var buttonGridDataManager = new LayoutDataManager(request.RequestContext);

            ButtonGrid buttonGrid = buttonGridDataManager.GetButtonGridById(request.ButtonGridId);

            return new GetButtonGridByIdServiceResponse(buttonGrid);
        }

        /// <summary>
        /// Gets all button grids.
        /// </summary>
        /// <param name="request">Request instance.</param>
        /// <returns>Response instance.</returns>
        private static GetButtonGridsServiceResponse GetButtonGrids(GetButtonGridsServiceRequest request)
        {
            var buttonGridDataManager = new LayoutDataManager(request.RequestContext);

            IEnumerable<ButtonGrid> buttonGrids = buttonGridDataManager.GetButtonGrids(request.ButtonGridQueryResultSettings).Results;
            
            return new GetButtonGridsServiceResponse(buttonGrids);
        }

        /// <summary>
        /// Gets button grids by identifiers.
        /// </summary>
        /// <param name="request">Request instance.</param>
        /// <returns>Response instance.</returns>
        private static GetButtonGridsByIdsServiceResponse GetButtonGridByIds(GetButtonGridsByIdsServiceRequest request)
        {
            var buttonGridDataManager = new LayoutDataManager(request.RequestContext);

            IEnumerable<ButtonGrid> buttonGrids = buttonGridDataManager.GetButtonsGridByIds(request.ButtonGridIds).Results;

            return new GetButtonGridsByIdsServiceResponse(buttonGrids);
        }
    }
}
