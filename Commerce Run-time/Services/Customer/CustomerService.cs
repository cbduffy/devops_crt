﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerService.cs" company="Microsoft Corporation">
// THIS CODE IS MADE AVAILABLE AS IS. MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
// OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
// THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
// NO TECHNICAL SUPPORT IS PROVIDED. YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
// </copyright>
// <summary>
// Customer service class.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Customer Service class.
    /// </summary>
    
    //Class made partial during RTM upgrade
    public partial class CustomerService : IRequestHandler
    {
        /// <summary>
        /// Enumeration of Customer operations used in the Customer Service.
        /// </summary>
        private enum CustomerOperations
        {
            UpdateCustomer = 0,
            CreateCustomerInAX,
            CreateCustomerInCRT,
            CreateAddressInAX,
            UpdateCustomerInAX,
            UpdateCustomerInCRT,
            UpdateAddressInAX,
            DeactivateAddressInAX,
            SendAccountActivationReqest,
            CompleteAccountActivationRequest
        }

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                return new[]
                {
                    typeof(SaveCustomerServiceRequest),
                    typeof(GetCustomersServiceRequest),
                    typeof(CustomersSearchServiceRequest),
                    typeof(GetCustomerGroupsServiceRequest),
                    typeof(SaveCustomerAccountActivationServiceRequest),
                    typeof(ValidateAccountActivationRequestServiceRequest),
                    typeof(GetCustomerBalanceServiceRequest),
                };
            }
        }

        /// <summary>
        /// Executes the service request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>
        /// The response.
        /// </returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestedType = request.GetType();

            if (requestedType == typeof(SaveCustomerServiceRequest))
            {
                return SaveCustomer((SaveCustomerServiceRequest)request);
            }

            if (requestedType == typeof(GetCustomersServiceRequest))
            {
                return GetCustomers((GetCustomersServiceRequest)request);
            }

            if (requestedType == typeof(CustomersSearchServiceRequest))
            {
                return SearchCustomers((CustomersSearchServiceRequest)request);
            }

            if (requestedType == typeof(GetCustomerGroupsServiceRequest))
            {
                return GetCustomerGroups((GetCustomerGroupsServiceRequest)request);
            }

            if (requestedType == typeof(SaveCustomerAccountActivationServiceRequest))
            {
                SaveCustomerAccountActivationServiceRequest saveAccountActivationRequest = (SaveCustomerAccountActivationServiceRequest)request;

                if (saveAccountActivationRequest.Status == 0)
                {
                    return SaveAccountActivationRequest(saveAccountActivationRequest);
                }
                else
                {
                    return CompleteAccountActivationRequest(saveAccountActivationRequest);
                }
            }

            if (requestedType == typeof(ValidateAccountActivationRequestServiceRequest))
            {
                return ValidateAccountActivationRequest((ValidateAccountActivationRequestServiceRequest)request);
            }

            if (requestedType == typeof(GetCustomerBalanceServiceRequest))
            {
                return GetBalance((GetCustomerBalanceServiceRequest)request);
            }

            throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
        }

        /// <summary>
        /// Get customers using the request criteria.
        /// </summary>
        /// <param name="request">Request containing the criteria to retrieve customers for.</param>
        /// <returns>GetCustomersServiceResponse object.</returns>
        /// <remarks>Calling this method with empty criteria (e.g. empty account number or no record id) will lead to performance issues. 
        /// Please use instead SearchCustomers method.
        /// </remarks>
        //Commented on RTM Upgrade
     /*   private static GetCustomersServiceResponse GetCustomers(GetCustomersServiceRequest request)
        {
            ValidateRequest(request);

            var dataServiceRequest = new GetCustomersDataRequest(request.RecordId, request.CustomerAccountNumber, request.PartyRecordId, request.CustomerPartyNumber, request.QueryResultSettings);
            EntityDataServiceResponse<Customer> dataServiceResponse = request.RequestContext.Execute<EntityDataServiceResponse<Customer>>(dataServiceRequest);
            var response = new GetCustomersServiceResponse(dataServiceResponse.EntityCollection);
            return response;
        }
     */
        /// <summary>
        /// Get customers using the request criteria.
        /// </summary>
        /// <param name="request">Request containing the criteria to retrieve customers for.</param>
        /// <returns>CustomersSearchServiceResponse object.</returns>
        private static CustomersSearchServiceResponse SearchCustomers(CustomersSearchServiceRequest request)
        {
            ValidateRequest(request);

            ReadOnlyCollection<GlobalCustomer> customers;

            if (request.Criteria != null && !string.IsNullOrWhiteSpace(request.Criteria.Keyword))
            {
                var dataServiceRequest = new SearchCustomersDataRequest(
                    request.Criteria.Keyword,
                    request.Criteria.SearchOnlyCurrentCompany,
                    request.QueryResultSettings);

                EntityDataServiceResponse<GlobalCustomer> dataServiceResponse = request.RequestContext.Execute<EntityDataServiceResponse<GlobalCustomer>>(dataServiceRequest);
                customers = dataServiceResponse.EntityCollection;
            }
            else
            {
                customers = Enumerable.Empty<GlobalCustomer>().AsReadOnly();
            }

            var response = new CustomersSearchServiceResponse(customers);
            return response;
        }

        /// <summary>
        /// Gets the collection of customer group from customer group table.
        /// </summary>
        /// <param name="request">Request containing the service context.</param>
        /// <returns>Returns the customer groups.</returns>
        private static GetCustomerGroupsServiceResponse GetCustomerGroups(GetCustomerGroupsServiceRequest request)
        {
            var dataServiceRequest = new GetCustomerGroupsDataRequest(request.QueryResultSettings);
            EntityDataServiceResponse<CustomerGroup> dataServiceResponse = request.RequestContext.Execute<EntityDataServiceResponse<CustomerGroup>>(dataServiceRequest);
            return new GetCustomerGroupsServiceResponse(dataServiceResponse.EntityCollection);
        }

        /// <summary>
        /// Save the requested customer.
        /// </summary>
        /// <param name="request">Request contains the customer to save.</param>
        /// <returns>The response.</returns>
        private static SaveCustomerServiceResponse SaveCustomer(SaveCustomerServiceRequest request)
        {
            Customer customer;

            if (request.CustomerToSave.RecordId > 0)
            {
                customer = UpdateCustomer(request);
            }
            else
            {
                customer = CreateCustomer(request, request.RequestContext);
            }

            var response = new SaveCustomerServiceResponse(customer);

            return response;
        }

        /// <summary>
        /// Create a new customer record.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="context">The request context.</param>
        /// <returns>The customer record created.</returns>
        // Commented on RTM Upgrade
       /* private static Customer CreateCustomer(SaveCustomerServiceRequest request, RequestContext context)
        {
            ValidateRequest(request);

            long storeId = GetStoreIdFromChannelConfiguration(context);

            Customer customer;
            if (string.IsNullOrWhiteSpace(request.CustomerToSave.NewCustomerPartyNumber))
            {
                customer = request.CustomerToSave;

                var getCustomerdataServiceRequest = new GetCustomerDataRequest(GetDefaultCustomerAccountNumberFromChannelProperties(context));
                SingleEntityDataServiceResponse<Customer> getCustomerdataServiceResponse = context.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerdataServiceRequest);
                Customer defaultAxCustomer = getCustomerdataServiceResponse.Entity;

                // if there is no default customer specified we can't set the defaults 
                if (defaultAxCustomer != null)
                {
                    customer.CustomerGroup = string.IsNullOrWhiteSpace(customer.CustomerGroup) ? defaultAxCustomer.CustomerGroup : customer.CustomerGroup;
                    customer.CurrencyCode = string.IsNullOrWhiteSpace(customer.CurrencyCode) ? defaultAxCustomer.CurrencyCode : customer.CurrencyCode;
                }

                // save customer in AX
                ExecutionHandler(
                     delegate
                     {
                         var newCustomerRequest = new NewCustomerServiceRequest(customer, storeId);
                         IRequestHandler transactionServiceRequestHandler = context.Runtime.GetRequestHandler(newCustomerRequest.GetType(), ServiceTypes.CustomerTransactionService);
                         SaveCustomerServiceResponse newCustomerResponse = context.Execute<SaveCustomerServiceResponse>(newCustomerRequest, transactionServiceRequestHandler);
                         customer = newCustomerResponse.UpdatedCustomer;
                     },
                     CustomerOperations.CreateCustomerInAX.ToString());

                // No need to save the address in AX since it was created already by the newCustomer call.

                // save customer and addresses in CRT
                ExecutionHandler(
                    delegate
                    {
                        var saveCustomerDataServiceRequest = new CreateOrUpdateCustomerDataRequest(customer);
                        SingleEntityDataServiceResponse<Customer> saveCustomerDataServiceResponse = context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(saveCustomerDataServiceRequest, request.RequestContext);
                        customer = saveCustomerDataServiceResponse.Entity;
                    },
                    CustomerOperations.CreateCustomerInCRT.ToString());
            }
            else
            {
                // Refresh customerData so that the party specific fields are filled
                var getInitCustomerDataServiceRequest = new GetCustomerWithPartyNumberDataRequest(request.CustomerToSave.NewCustomerPartyNumber);
                SingleEntityDataServiceResponse<Customer> getInitCustomerDataServiceResponse = context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(getInitCustomerDataServiceRequest, request.RequestContext);
                customer = getInitCustomerDataServiceResponse.Entity;

                // save customer in AX
                ExecutionHandler(
                     delegate
                     {
                         var newCustomerFromDirectoryPartyServiceRequest = new NewCustomerFromDirectoryPartyServiceRequest(customer, storeId);
                         IRequestHandler transactionServiceRequestHandler = context.Runtime.GetRequestHandler(newCustomerFromDirectoryPartyServiceRequest.GetType(), ServiceTypes.CustomerTransactionService);
                         SaveCustomerServiceResponse saveCustomerServiceResponse = context.Runtime.Execute<SaveCustomerServiceResponse>(newCustomerFromDirectoryPartyServiceRequest, request.RequestContext, transactionServiceRequestHandler);
                         customer = saveCustomerServiceResponse.UpdatedCustomer;
                     },
                     CustomerOperations.CreateCustomerInAX.ToString());

                // save customer and addresses in CRT
                ExecutionHandler(
                    delegate
                    {
                        var saveCustomerDataServiceRequest = new CreateOrUpdateCustomerDataRequest(customer);
                        SingleEntityDataServiceResponse<Customer> saveCustomerDataServiceResponse = context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(saveCustomerDataServiceRequest, request.RequestContext);
                        customer = saveCustomerDataServiceResponse.Entity;
                    },
                    CustomerOperations.CreateCustomerInCRT.ToString());
            }

            var getCustomerDataRequest = new GetCustomerDataRequest(customer.AccountNumber);
            SingleEntityDataServiceResponse<Customer> getCustomerDataResponse = context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerDataRequest, request.RequestContext);
            customer = getCustomerDataResponse.Entity;

            return customer;
        }*/

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Updated customer record.</returns>
       //Commented on RTM Upgrade
       /* private static Customer UpdateCustomer(SaveCustomerServiceRequest request)
        {
            ValidateRequest(request);

            Customer customer;

            // Create customer in current company if the customer is from 3rd party company
            if (string.IsNullOrEmpty(request.CustomerToSave.AccountNumber) && !string.IsNullOrEmpty(request.CustomerToSave.NewCustomerPartyNumber))
            {
                customer = CustomerService.CreateCustomer(request, request.RequestContext);
            }

            // get original copy of customer
            var getCustomerdataServiceRequest = new GetCustomerDataRequest(request.CustomerToSave.AccountNumber);
            SingleEntityDataServiceResponse<Customer> getCustomerdataServiceResponse = request.RequestContext.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerdataServiceRequest);
            Customer databaseStoredCustomer = getCustomerdataServiceResponse.Entity;

            if (databaseStoredCustomer == null)
            {
                throw new DataValidationException(DataValidationErrors.CustomerNotFound, "Customer {0} not found", request.CustomerToSave.RecordId);
            }

            // make sure that the the recordid of the customer retrieved matches the customer record id in the request.
            // this prevents a request from updating the wrong record
            if (databaseStoredCustomer.RecordId != request.CustomerToSave.RecordId)
            {
                throw new DataValidationException(DataValidationErrors.IdMismatch, "Customer RecordId {0} in request does not match the Customer RecordId retrieved from the Channel Db {1} using AccountNumber {2} as the lookup", request.CustomerToSave.RecordId, databaseStoredCustomer.RecordId, request.CustomerToSave.AccountNumber);
            }

            // merge with CRT customer
            customer = DataModelExtensions.MergeDatabaseCustomerIntoInputCustomer(request.CustomerToSave, databaseStoredCustomer);

            ExecutionHandler(
                delegate
                {
                    // update customer in AX
                    var updateCustomerServiceRequest = new SaveCustomerServiceRequest(customer);
                    IRequestHandler transactionServiceRequestHandler = request.RequestContext.Runtime.GetRequestHandler(updateCustomerServiceRequest.GetType(), ServiceTypes.CustomerTransactionService);
                    SaveCustomerServiceResponse updateCustomerServiceResponse = request.RequestContext.Execute<SaveCustomerServiceResponse>(updateCustomerServiceRequest, transactionServiceRequestHandler);
                    customer = updateCustomerServiceResponse.UpdatedCustomer;
                },
                CustomerOperations.UpdateCustomerInAX.ToString());

            // save address in AX
            customer = CustomerService.SaveAddresses(request, customer);

            // save customer and addresses in CRT
            ExecutionHandler(
                delegate
                {
                    var saveCustomerDataServiceRequest = new CreateOrUpdateCustomerDataRequest(customer);
                    SingleEntityDataServiceResponse<Customer> saveCustomerDataServiceResponse = request.RequestContext.Execute<SingleEntityDataServiceResponse<Customer>>(saveCustomerDataServiceRequest);
                    customer = saveCustomerDataServiceResponse.Entity;
                },
                CustomerOperations.UpdateCustomerInCRT.ToString());

            return customer;
        }
        */
        /// <summary>
        /// Save customer account activation request to channel database.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        private static Response SaveAccountActivationRequest(SaveCustomerAccountActivationServiceRequest request)
        {
            ValidateRequest(request);

            ExecutionHandler(
                delegate
                {
                    var dataServiceRequest = new SaveCustomerAccountActivationDataRequest(request.EmailAddress, request.ActivationToken);
                    var dataServiceResponse = request.RequestContext.Execute<NullResponse>(dataServiceRequest);
                },
                CustomerOperations.SendAccountActivationReqest.ToString());

            return new NullResponse();
        }

        /// <summary>
        /// Create customer account and update account activation request status to completed.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        private static Response CompleteAccountActivationRequest(SaveCustomerAccountActivationServiceRequest request)
        {
            ExecutionHandler(
                delegate
                {
                    // Validate the request
                    ValidateRequest(request);
                    Customer customer = null;

                    // Try to locate the customer
                    var searchCustomerDataServiceRequest = new SearchCustomersDataRequest(request.EmailAddress, false, new QueryResultSettings());
                    EntityDataServiceResponse<GlobalCustomer> searchCustomerDataServiceResponse = request.RequestContext.Execute<EntityDataServiceResponse<GlobalCustomer>>(searchCustomerDataServiceRequest);
                    GlobalCustomer globalCustomer = searchCustomerDataServiceResponse.EntityCollection.FirstOrDefault();

                    if (string.IsNullOrEmpty(globalCustomer.AccountNumber))
                    {
                        customer = new Customer() { NewCustomerPartyNumber = globalCustomer.PartyNumber };

                        // Create customer in current company
                        SaveCustomerServiceRequest saveCustomerRequest = new SaveCustomerServiceRequest(customer);
                        CreateCustomer(saveCustomerRequest, request.RequestContext);
                    }
                    else
                    {
                        var getCustomerDataRequest = new GetCustomerDataRequest(globalCustomer.AccountNumber);
                        SingleEntityDataServiceResponse<Customer> getCustomerDataResponse = request.RequestContext.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerDataRequest);
                        customer = getCustomerDataResponse.Entity;
                    }

                    // Update the request status in channel db
                    var completeActivationDataServiceRequest = new CompleteCustomerAccountActivationDataRequest(request.EmailAddress, request.ActivationToken);
                    var completeActivationDataServiceResponse = request.RequestContext.Execute<NullResponse>(completeActivationDataServiceRequest);
                },
                CustomerOperations.CompleteAccountActivationRequest.ToString());

            return new NullResponse();
        }

        /// <summary>
        /// Validate customer account activation request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        private static Response ValidateAccountActivationRequest(ValidateAccountActivationRequestServiceRequest request)
        {
            ValidateRequest(request);

            var dataServiceRequest = new ValidateAccountActivationDataRequest(request.EmailAddress, request.ActivationToken);
            ValidateAccountActivationDataResponse dataServiceResponse = request.RequestContext.Execute<ValidateAccountActivationDataResponse>(dataServiceRequest);
            if (!dataServiceResponse.IsRequestValid)
            {
                throw new DataValidationException(
                    DataValidationErrors.ObjectNotFound,
                    "Invalid request. Email='{0}'; Activation token='{1}'",
                    request.EmailAddress,
                    request.ActivationToken);
            }

            return new NullResponse();
        }

        /// <summary>
        /// Save the customer addresses.
        /// </summary>
        /// <param name="request">This service request.</param>
        /// <param name="customerData">The customer data.</param>
        /// <returns>The updated customer. </returns>
        private static Customer SaveAddresses(Request request, Customer customerData)
        {
            List<Address> savedAddresses = new List<Address>();

            foreach (Address address in customerData.Addresses)
            {
                Address savedAddress = address;
                if (savedAddress == null)
                {
                    continue;
                }

                if (savedAddress.RecordId == 0)
                {
                    ExecutionHandler(
                        delegate
                        {
                            var createAddressRequest = new CreateAddressServiceRequest(customerData, savedAddress);
                            IRequestHandler transactionServiceRequestHandler = request.RequestContext.Runtime.GetRequestHandler(createAddressRequest.GetType(), ServiceTypes.CustomerTransactionService);
                            CreateAddressServiceResponse createAddressResponse = request.RequestContext.Execute<CreateAddressServiceResponse>(createAddressRequest, transactionServiceRequestHandler);
                            savedAddress = createAddressResponse.Address;
                        },
                        CustomerOperations.CreateAddressInAX.ToString());
                }
                else
                {
                    if (address.Deactivate)
                    {
                        long addressId = savedAddress.RecordId;
                        long customerId = customerData.RecordId;

                        ExecutionHandler(
                            delegate
                            {
                                var deactivateAddressRequest = new DeactivateAddressServiceRequest(addressId, customerId);
                                IRequestHandler transactionServiceRequestHandler = request.RequestContext.Runtime.GetRequestHandler(deactivateAddressRequest.GetType(), ServiceTypes.CustomerTransactionService);
                                request.RequestContext.Execute<NullResponse>(deactivateAddressRequest, transactionServiceRequestHandler);
                            },
                            CustomerOperations.DeactivateAddressInAX.ToString());
                    }
                    else
                    {
                        // Get existing address and validate readonly properties before updating the address.
                        GetAddressDataRequest dataServiceRequest = new GetAddressDataRequest(savedAddress.RecordId, customerData.RecordId, new ColumnSet());
                        Address existingAddress = request.RequestContext.Execute<SingleEntityDataServiceResponse<Address>>(dataServiceRequest).Entity;

                        if (existingAddress != null)
                        {
                            Collection<DataValidationFailure> dataValidationFailures = ReadOnlyAttribute.CheckReadOnlyProperties(existingAddress, savedAddress);

                            if (dataValidationFailures.Count > 0)
                            {
                                throw new DataValidationException(DataValidationErrors.AggregateValidationError, dataValidationFailures, string.Format("Validation failures occurred when verifying address for customer number {0} for update.", customerData.AccountNumber));
                            }
                        }

                        ExecutionHandler(
                            delegate
                            {
                                var updateAddressReqeust = new UpdateAddressServiceRequest(customerData, savedAddress);
                                IRequestHandler transactionServiceRequestHandler = request.RequestContext.Runtime.GetRequestHandler(updateAddressReqeust.GetType(), ServiceTypes.CustomerTransactionService);
                                UpdateAddressServiceResponse updateAddressResponse = request.RequestContext.Execute<UpdateAddressServiceResponse>(updateAddressReqeust, transactionServiceRequestHandler);
                                customerData = updateAddressResponse.Customer;
                                savedAddress = updateAddressResponse.Address;
                            },
                            CustomerOperations.UpdateAddressInAX.ToString());
                    }
                }

                savedAddresses.Add(savedAddress);
            }

            customerData.Addresses = savedAddresses;

            return customerData;
        }

        /// <summary>
        /// Gets the balance.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>A customer balance service response.</returns>
        private static GetCustomerBalanceServiceResponse GetBalance(GetCustomerBalanceServiceRequest request)
        {
            if (!request.SearchLocation.HasFlag(SearchLocation.Remote))
            {
                // Since last transaction that was in sync after P-job (anchor) can be retrieved from TS only, 
                // the required search location has to be either Remote or All.
                throw new DataValidationException(
                    DataValidationErrors.InvalidRequest,
                    "Search location requires Remote flag in order to retrive the PendingTransactionsAnchor.");
            }

            // Get balance from HQ
            var getCustomerBalanceRequest = new GetCustomerBalanceServiceRequest(request.AccountNumber);
            IRequestHandler transactionServiceRequestHandler = request.RequestContext.Runtime.GetRequestHandler(getCustomerBalanceRequest.GetType(), ServiceTypes.CustomerTransactionService);
            GetCustomerBalanceServiceResponse getCustomerBalanceResponse = request.RequestContext.Execute<GetCustomerBalanceServiceResponse>(getCustomerBalanceRequest, transactionServiceRequestHandler);
            CustomerBalances customerBalance = getCustomerBalanceResponse.Balance;

            if (request.SearchLocation.HasFlag(SearchLocation.Local))
            {
                // Fetch pending local balance too
                SalesTransactionDataManager salesDatamanager = new SalesTransactionDataManager(request.RequestContext);

                decimal localNonPostedTransactionAmount = salesDatamanager.GetCustomerLocalPendingBalance(
                    request.AccountNumber,
                    customerBalance.PendingTransactionsAnchor);

                customerBalance.PendingBalance = localNonPostedTransactionAmount;

                if (!string.IsNullOrWhiteSpace(request.InvoiceAccountNumber))
                {
                    decimal localInvoiceNonPostedTransactionAmount = salesDatamanager.GetCustomerLocalPendingBalance(
                        request.InvoiceAccountNumber,
                        customerBalance.PendingTransactionsAnchor);

                    customerBalance.InvoiceAccountPendingBalance = localInvoiceNonPostedTransactionAmount;
                }
            }

            return new GetCustomerBalanceServiceResponse(customerBalance);
        }

        #region Helpers

        /// <summary>
        /// Gets the default customer account number from the channel properties.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>The default customer account number.</returns>
        private static string GetDefaultCustomerAccountNumberFromChannelProperties(RequestContext context)
        {
            ChannelConfiguration channelConfig = context.GetChannelConfiguration();

            if (channelConfig.ChannelType == RetailChannelType.RetailStore)
            {
                return context.GetOrgUnit().DefaultCustomerAccount;
            }

            return new ChannelDataManager(context).GetOnlineChannelById(channelConfig.RecordId, new ColumnSet()).DefaultCustomerAccount;
        }

        /// <summary>
        /// Gets the store identifier from the channel configuration.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>The store identifier.</returns>
        private static long GetStoreIdFromChannelConfiguration(RequestContext context)
        {
            ChannelDataManager manager = new ChannelDataManager(context);
            return manager.GetCurrentChannelId();
        }

        /// <summary>
        /// Validate the service request.
        /// </summary>
        /// <param name="request">The request.</param>
        private static void ValidateRequest(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            var saveCustomerServiceRequest = request as SaveCustomerServiceRequest;

            if (saveCustomerServiceRequest != null)
            {
                if (saveCustomerServiceRequest.CustomerToSave == null)
                {
                    throw new ArgumentNullException("CustomerToSave");
                }

                // Skip validation when creating customer from existing dirParty
                if (saveCustomerServiceRequest.CustomerToSave.DirectoryPartyRecordId == 0)
                {
                    if (saveCustomerServiceRequest.CustomerToSave.Addresses == null)
                    {
                        throw new ArgumentNullException("Addresses");
                    }

                    // new customer validation
                    if (saveCustomerServiceRequest.CustomerToSave.RecordId == 0)
                    {
                        int isPrimaryCount = 0;

                        // verify there is one and only one address marked as primary
                        foreach (Address address in saveCustomerServiceRequest.CustomerToSave.Addresses)
                        {
                            if (address.IsPrimary)
                            {
                                isPrimaryCount++;
                            }
                        }

                        if (isPrimaryCount > 1)
                        {
                            throw new ArgumentException("There must be only one address marked as Primary");
                        }
                    }
                }
            }

            var accountActivationRequest = request as SaveCustomerAccountActivationServiceRequest;
            if (accountActivationRequest != null && accountActivationRequest.Status != 0)
            {
                var dataServiceRequest = new ValidateAccountActivationDataRequest(accountActivationRequest.EmailAddress, accountActivationRequest.ActivationToken);
                ValidateAccountActivationDataResponse dataServiceResponse = accountActivationRequest.RequestContext.Execute<ValidateAccountActivationDataResponse>(dataServiceRequest);
                if (!dataServiceResponse.IsRequestValid)
                {
                    throw new DataValidationException(DataValidationErrors.ObjectNotFound, "Invalid request. Email='{0}'; Activation token='{1}'", accountActivationRequest.EmailAddress, accountActivationRequest.ActivationToken);
                }
            }
        }

        /// <summary>
        /// Executions the handler.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="operationType">Type of the operation.</param>
        private static void ExecutionHandler(Action action, string operationType)
        {
            try
            {
                action();
                NetTracer.Information("Operation {0} succeeded", operationType);
            }
            catch (Exception e)
            {
                NetTracer.Error(e, "Operation {0} failed", operationType);
                throw;
            }
        }
        #endregion
    }
}
