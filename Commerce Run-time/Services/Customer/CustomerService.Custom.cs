﻿using AFM.Commerce.Runtime.Services.TransactionService;
using Microsoft.Dynamics.Commerce.Runtime.Data;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.Services.Messages;
using Microsoft.Dynamics.Commerce.Runtime;
using System.Collections.ObjectModel;
using AFMDM = AFM.Commerce.Runtime.Services.DataManagers;
using Microsoft.Dynamics.Commerce.Runtime.TransactionService;
using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
using Microsoft.Dynamics.Commerce.Runtime.Workflow;

namespace Microsoft.Dynamics.Commerce.Runtime.Services
{
    partial class CustomerService
    {
        /// <summary>
        /// Get customers using the request criteria.
        /// </summary>
        /// <param name="request">Request containing the criteria to retrieve customers for.</param>
        /// <returns>GetCustomersServiceResponse object.</returns>
        /// <remarks>Calling this method with empty criteria (e.g. empty account number or no record id) will lead to performance issues. 
        /// Please use instead SearchCustomers method.
        /// </remarks>
        private static GetCustomersServiceResponse GetCustomers(GetCustomersServiceRequest request)
        {
            ValidateRequest(request);
            var dataServiceRequest = new GetCustomersDataRequest(request.RecordId, request.CustomerAccountNumber, request.PartyRecordId, request.CustomerPartyNumber, request.QueryResultSettings);
            EntityDataServiceResponse<Customer> dataServiceResponse = request.RequestContext.Execute<EntityDataServiceResponse<Customer>>(dataServiceRequest);
            //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
            // Iterate through all customers to fetch electronic address information
            ReadOnlyCollection<Customer> customers = dataServiceResponse.EntityCollection;

            AFMDM.AFMCustomerDataManager managerEx = new AFMDM.AFMCustomerDataManager(request.RequestContext);

            foreach (Customer customer in customers)
            {
                managerEx.GetElectronicAddresses(customer);
            }
            //NE - RxL - FDD0041_BMS_Create_Customer_v2.9

            var response = new GetCustomersServiceResponse(dataServiceResponse.EntityCollection);
            return response;
        }

        /// <summary>
        /// Create a new customer record.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The customer record created.</returns>
        private static Customer CreateCustomer(SaveCustomerServiceRequest request, RequestContext context)
        {
            ValidateRequest(request);

            long storeId = GetStoreIdFromChannelConfiguration(context);
            AFMDM.AFMCustomerDataManager managerEx = new AFMDM.AFMCustomerDataManager(request.RequestContext);
            Customer customer;

            if (string.IsNullOrWhiteSpace(request.CustomerToSave.NewCustomerPartyNumber))
            {
                customer = request.CustomerToSave;

                var getCustomerdataServiceRequest = new GetCustomerDataRequest(GetDefaultCustomerAccountNumberFromChannelProperties(context));
                SingleEntityDataServiceResponse<Customer> getCustomerdataServiceResponse = context.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerdataServiceRequest);
                Customer defaultAxCustomer = getCustomerdataServiceResponse.Entity;



                // if there is no default customer specified we can't set the defaults 
                if (defaultAxCustomer != null)
                {
                    customer.CustomerGroup = string.IsNullOrWhiteSpace(customer.CustomerGroup) ? defaultAxCustomer.CustomerGroup : customer.CustomerGroup;
                    customer.CurrencyCode = string.IsNullOrWhiteSpace(customer.CurrencyCode) ? defaultAxCustomer.CurrencyCode : customer.CurrencyCode;
                }

                // save customer in AX
                ExecutionHandler(
                     delegate
                     {
                         var newCustomerRequest = new NewCustomerServiceRequest(customer, storeId);
                         IRequestHandler transactionServiceRequestHandler = context.Runtime.GetRequestHandler(newCustomerRequest.GetType(), ServiceTypes.CustomerTransactionService);
                         SaveCustomerServiceResponse newCustomerResponse = context.Execute<SaveCustomerServiceResponse>(newCustomerRequest, transactionServiceRequestHandler);
                         customer = newCustomerResponse.UpdatedCustomer;
                     },
                     CustomerOperations.CreateCustomerInAX.ToString());

                //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                //Save additional customer information in AX
                ExecutionHandler(
                     delegate
                     {
                         AFMTransactionServiceClient.UpdateCustomerInformation(ref customer, request);
                     },
                     CustomerOperations.CreateCustomerInAX.ToString());
                //NE - RxL - FDD0041_BMS_Create_Customer_v2.9

                // No need to save the address in AX since it was created already by the newCustomer call.

                // save customer and addresses in CRT
                ExecutionHandler(
                    delegate
                    {
                        var saveCustomerDataServiceRequest = new CreateOrUpdateCustomerDataRequest(customer);
                        SingleEntityDataServiceResponse<Customer> saveCustomerDataServiceResponse = context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(saveCustomerDataServiceRequest, request.RequestContext);
                        customer = saveCustomerDataServiceResponse.Entity;
                    },
                    CustomerOperations.CreateCustomerInCRT.ToString());


                //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                //Save additional customer information in CRT
                ExecutionHandler(
                    delegate
                    {
                        customer = managerEx.CreateOrUpdateAdditonalAddressInfo(customer);
                    },
                    CustomerOperations.CreateCustomerInCRT.ToString());
                //NE - RxL - FDD0041_BMS_Create_Customer_v2.9

            }
            else
            {
                // Refresh customerData so that the party specific fields are filled
                var getInitCustomerDataServiceRequest = new GetCustomerWithPartyNumberDataRequest(request.CustomerToSave.NewCustomerPartyNumber);
                SingleEntityDataServiceResponse<Customer> getInitCustomerDataServiceResponse = context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(getInitCustomerDataServiceRequest, request.RequestContext);
                customer = getInitCustomerDataServiceResponse.Entity;

                // save customer in AX
                ExecutionHandler(
                     delegate
                     {
                         var newCustomerFromDirectoryPartyServiceRequest = new NewCustomerFromDirectoryPartyServiceRequest(customer, storeId);
                         IRequestHandler transactionServiceRequestHandler = context.Runtime.GetRequestHandler(newCustomerFromDirectoryPartyServiceRequest.GetType(), ServiceTypes.CustomerTransactionService);
                         SaveCustomerServiceResponse saveCustomerServiceResponse = context.Runtime.Execute<SaveCustomerServiceResponse>(newCustomerFromDirectoryPartyServiceRequest, request.RequestContext, transactionServiceRequestHandler);
                         customer = saveCustomerServiceResponse.UpdatedCustomer;
                     },
                     CustomerOperations.CreateCustomerInAX.ToString());

                //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                //Save additional customer information in AX
                ExecutionHandler(
                     delegate
                     {
                         AFMTransactionServiceClient.UpdateCustomerInformation(ref customer, request);
                     },
                     CustomerOperations.CreateCustomerInAX.ToString());


                //NE - RxL - FDD0041_BMS_Create_Customer_v2.9

                // save customer and addresses in CRT
                ExecutionHandler(
                    delegate
                    {
                        var saveCustomerDataServiceRequest = new CreateOrUpdateCustomerDataRequest(customer);
                        SingleEntityDataServiceResponse<Customer> saveCustomerDataServiceResponse = context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(saveCustomerDataServiceRequest, request.RequestContext);
                        customer = saveCustomerDataServiceResponse.Entity;
                    },
                    CustomerOperations.CreateCustomerInCRT.ToString());

                //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
                //Save additional customer information in CRT
                ExecutionHandler(
                    delegate
                    {
                        customer = managerEx.CreateOrUpdateAdditonalAddressInfo(customer);
                    },
                    CustomerOperations.CreateCustomerInCRT.ToString());
                //NE - RxL - FDD0041_BMS_Create_Customer_v2.9
            }

            var getCustomerDataRequest = new GetCustomerDataRequest(customer.AccountNumber);
            SingleEntityDataServiceResponse<Customer> getCustomerDataResponse = context.Runtime.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerDataRequest, request.RequestContext);
            customer = getCustomerDataResponse.Entity;

            //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
            // Get additional electronic addresses
            customer = managerEx.GetElectronicAddresses(customer);
            //NE - RxL - FDD0041_BMS_Create_Customer_v2.9

            return customer;
        }

        /// <summary>
        /// Updates the customer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Updated customer record.</returns>
        private static Customer UpdateCustomer(SaveCustomerServiceRequest request)
        {
            ValidateRequest(request);

            Customer customer;

            //TO DO spriya upgraded for merge
            AFMDM.AFMCustomerDataManager managerEx = new AFMDM.AFMCustomerDataManager(request.RequestContext);

            // Create customer in current company if the customer is from 3rd party company
            if (string.IsNullOrEmpty(request.CustomerToSave.AccountNumber) && !string.IsNullOrEmpty(request.CustomerToSave.NewCustomerPartyNumber))
            {
                customer = CustomerService.CreateCustomer(request, request.RequestContext);
            }

            // get original copy of customer
            var getCustomerdataServiceRequest = new GetCustomerDataRequest(request.CustomerToSave.AccountNumber);
            SingleEntityDataServiceResponse<Customer> getCustomerdataServiceResponse = request.RequestContext.Execute<SingleEntityDataServiceResponse<Customer>>(getCustomerdataServiceRequest);
            Customer databaseStoredCustomer = getCustomerdataServiceResponse.Entity;

            //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
            // Get additional electronic addresses
            databaseStoredCustomer = managerEx.GetElectronicAddresses(databaseStoredCustomer);
            //NE - RxL - FDD0041_BMS_Create_Customer_v2.9

            if (databaseStoredCustomer == null)
            {
                throw new DataValidationException(DataValidationErrors.CustomerNotFound, "Customer {0} not found", request.CustomerToSave.RecordId);
            }

            // make sure that the the recordid of the customer retrieved matches the customer record id in the request.
            // this prevents a request from updating the wrong record
            if (databaseStoredCustomer.RecordId != request.CustomerToSave.RecordId)
            {
                throw new DataValidationException(DataValidationErrors.IdMismatch, "Customer RecordId {0} in request does not match the Customer RecordId retrieved from the Channel Db {1} using AccountNumber {2} as the lookup", request.CustomerToSave.RecordId, databaseStoredCustomer.RecordId, request.CustomerToSave.AccountNumber);
            }

            //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
            // Call customized MergeDatabaseCustomerIntoInputCustomer method to include extension properties
            //customer = DataModelExtensions.MergeDatabaseCustomerIntoInputCustomer(request.CustomerToSave, databaseStoredCustomer);

            customer = AFM.Commerce.Runtime.Services.Models.DataModelExtensions.MergeDatabaseCustomerIntoInputCustomer(request.CustomerToSave, databaseStoredCustomer);
            ExecutionHandler(
                delegate
                {
                    // update customer in AX
                    var updateCustomerServiceRequest = new SaveCustomerServiceRequest(customer);
                    IRequestHandler transactionServiceRequestHandler = request.RequestContext.Runtime.GetRequestHandler(updateCustomerServiceRequest.GetType(), ServiceTypes.CustomerTransactionService);
                    SaveCustomerServiceResponse updateCustomerServiceResponse = request.RequestContext.Execute<SaveCustomerServiceResponse>(updateCustomerServiceRequest, transactionServiceRequestHandler);
                    customer = updateCustomerServiceResponse.UpdatedCustomer;
                },
                CustomerOperations.UpdateCustomerInAX.ToString());

            // save address in AX
            customer = CustomerService.SaveAddresses(request, customer);

            //Save non primary address information
            foreach (Address address in customer.Addresses)
            {
                ExecutionHandler(
                delegate
                {
                    AFMTransactionServiceClient.UpdateCustomerInformation(ref customer, request, address);
                },
                CustomerOperations.CreateCustomerInAX.ToString());

                // save customer and addresses in CRT
                ExecutionHandler(
                    delegate
                    {
                        var saveCustomerDataServiceRequest = new CreateOrUpdateCustomerDataRequest(customer);
                        SingleEntityDataServiceResponse<Customer> saveCustomerDataServiceResponse = request.RequestContext.Execute<SingleEntityDataServiceResponse<Customer>>(saveCustomerDataServiceRequest);
                        customer = saveCustomerDataServiceResponse.Entity;
                    },
                    CustomerOperations.UpdateCustomerInCRT.ToString());
            }

            //NS - RxL - FDD0041_BMS_Create_Customer_v2.9
            //Save additional customer information in CRT
            ExecutionHandler(
                delegate
                {
                    customer = managerEx.CreateOrUpdateAdditonalAddressInfo(customer);
                },
                CustomerOperations.CreateCustomerInCRT.ToString());
            //NE - RxL - FDD0041_BMS_Create_Customer_v2.9
            return customer;
        }
    }
}
