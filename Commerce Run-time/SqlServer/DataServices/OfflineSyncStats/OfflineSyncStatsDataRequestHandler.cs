﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer
{
    using System;
    using System.Collections.Generic;
    using Messages;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Runtime.Messages;
    using Workflow;

    /// <summary>
    /// Data service for managing offline sync statistics from SQL server database.
    /// </summary>
    public class OfflineSyncStatsSqlServerDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get { yield return typeof(GetOfflineSyncStatsDataServiceRequest); }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>
        /// The outgoing response message.
        /// </returns>
        /// <exception cref="System.NotSupportedException">The request type is not supported.</exception>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;

            if (requestType == typeof(GetOfflineSyncStatsDataServiceRequest))
            {
                response = this.GetOfflineSyncStatsLines((GetOfflineSyncStatsDataServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        private EntityDataServiceResponse<OfflineSyncStatsLine> GetOfflineSyncStatsLines(GetOfflineSyncStatsDataServiceRequest request)
        {
            using (SqlServerDatabaseContext databaseContext = new SqlServerDatabaseContext(request.RequestContext))
            {
                GetOfflineSyncStatsLinesProcedure getOfflineSyncStatsLinesProcedure = new GetOfflineSyncStatsLinesProcedure(request, databaseContext);
                return getOfflineSyncStatsLinesProcedure.Execute();
            }
        }
    }
}