﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Data service for products.
    /// </summary>
    public sealed class ProductSqlServerDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(GetProductPartsDataServiceRequest);
                yield return typeof(GetProductUnitOfMeasureOptionsDataRequest);
                yield return typeof(GetParentKitDataRequest);
                yield return typeof(ProductSearchDataRequest);
                yield return typeof(GetItemsDataRequest); 
                yield return typeof(ProductSearchDataRequest);
                yield return typeof(GetProductCatalogAssociationsDataRequest);
                yield return typeof(GetLinkedProductsDataRequest);
                yield return typeof(GetProductVariantsDataRequest);
                yield return typeof(GetProductKitDataRequest);
            }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            Response response;

            if (request is GetProductPartsDataServiceRequest)
            {
                response = GetProductsParts((GetProductPartsDataServiceRequest)request);
            }
            else if (request is GetProductUnitOfMeasureOptionsDataRequest)
            {
                response = GetProductUnitOfMeasureOptions((GetProductUnitOfMeasureOptionsDataRequest)request);
            }
            else if (request is GetParentKitDataRequest)
            {
                response = GetParentProductKit((GetParentKitDataRequest)request);
            }
            else if (request is GetItemsDataRequest)
            {
                response = GetItems((GetItemsDataRequest)request);
            }
            else if (request is GetProductCatalogAssociationsDataRequest)
            {
                response = GetProductCatalogAssociations((GetProductCatalogAssociationsDataRequest)request);
            }
            else if (request is GetLinkedProductsDataRequest)
            {
                response = GetLinkedProducts((GetLinkedProductsDataRequest)request);
            }
            else if (request is GetProductVariantsDataRequest)
            {
                response = GetProductVariants((GetProductVariantsDataRequest)request);
            }
            else if (request is GetProductKitDataRequest)
            {
                response = GetProductKit((GetProductKitDataRequest)request);
            }
            else
            {
                string message = string.Format("Request type '{0}' is not supported", request.GetType().FullName);
                throw new NotSupportedException(message);
            }

            return response;
        }

        private static EntityDataServiceResponse<ProductVariant> GetProductVariants(GetProductVariantsDataRequest request)
        {
            ItemDataManager itemDataManager = new ItemDataManager(request.RequestContext);
            ReadOnlyCollection<ProductVariant> variants = itemDataManager.GetVariants(request.ItemAndInventoryDimensionIds, request.QueryResultSettings.ColumnSet);
            return new EntityDataServiceResponse<ProductVariant>(variants);
        }

        private static GetLinkedProductsDataResponse GetLinkedProducts(GetLinkedProductsDataRequest request)
        {
            var productDataManager = GetProductDataManager(request.RequestContext);
            return new GetLinkedProductsDataResponse(productDataManager.GetLinkedProducts(request.ProductIds));
        }

        private static GetProductCatalogAssociationsDataResponse GetProductCatalogAssociations(GetProductCatalogAssociationsDataRequest request)
        {
            var productDataManager = GetProductDataManager(request.RequestContext);
            ReadOnlyCollection<ProductCatalogAssociation> catalogAssociations = productDataManager.GetProductCatalogAssociations(request.ProductIds);
            return new GetProductCatalogAssociationsDataResponse(catalogAssociations);
        }

        private static GetParentKitDataResponse GetParentProductKit(GetParentKitDataRequest request)
        {
            ReadOnlyCollection<KitComponent> kitComponents = GetProductDataManager(request.RequestContext).GetParentKitComponentInfo(request.ProductIds);
            return new GetParentKitDataResponse(kitComponents);
        }

        private static GetProductUnitOfMeasureOptionsDataResponse GetProductUnitOfMeasureOptions(GetProductUnitOfMeasureOptionsDataRequest request)
        {
            return GetProductDataManager(request.RequestContext).GetProductUnitofMeasureOptions(request);
        }

        private static GetProductPartsDataServiceResponse GetProductsParts(GetProductPartsDataServiceRequest request)
        {
            return GetProductDataManager(request.RequestContext).GetProductParts(request);
        }

        private static GetProductKitDataResponse GetProductKit(GetProductKitDataRequest request)
        {
            ReadOnlyCollection<KitDefinition> kitDefinitions;
            ReadOnlyCollection<KitComponent> componentAndSubstituteList;
            ReadOnlyCollection<KitConfigToComponentAssociation> configToComponentAssociations;

            GetProductDataManager(request.RequestContext).GetProductKits(
                    request.KitMasterProductIds,
                    out kitDefinitions,
                    out componentAndSubstituteList,
                    out configToComponentAssociations);

            return new GetProductKitDataResponse(kitDefinitions, componentAndSubstituteList, configToComponentAssociations);
        }

        private static GetItemsDataResponse GetItems(GetItemsDataRequest request)
        {
            ItemDataManager itemDataManager = GetItemDataManager(request.RequestContext);
            ReadOnlyCollection<Item> items;

            if (!request.ItemIds.IsNullOrEmpty())
            {
                items = itemDataManager.GetItems(request.ItemIds, request.QueryResultSettings.ColumnSet);
            }
            else if (!request.ProductIds.IsNullOrEmpty())
            {
                items = itemDataManager.GetItems(request.ProductIds, request.QueryResultSettings.ColumnSet);
            }
            else
            {
                items = new ReadOnlyCollection<Item>(new List<Item>());
            }

            return new GetItemsDataResponse(items);
        }

        private static ProductDataManager GetProductDataManager(RequestContext context)
        {
            return new ProductDataManager(context);
        }

        private static ItemDataManager GetItemDataManager(RequestContext context)
        {
            return new ItemDataManager(context);
        }
    }
}