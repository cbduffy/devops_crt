﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer.DataServices
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// The data request handler for sales transaction in SQLServer.
    /// </summary>
    public sealed partial class SalesTransactionSqlServerDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(SaveCartDataRequest);
                yield return typeof(DeleteCartDataRequest);
                yield return typeof(InsertSalesTransactionTablesDataRequest);
                yield return typeof(GetSalesTransactionDataRequest);
                yield return typeof(GetDiscountLinesDataRequest);
                yield return typeof(GetLoyaltyRewardPointLinesDataRequest);
                yield return typeof(GetSalesLinesDataRequest);
            }
        }

        /// <summary>
        /// Gets the sales transaction to be saved.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <returns>The response message.</returns>
        public Response Execute(Request request)
        {
            Response response;

            if (request is SaveCartDataRequest)
            {
                response = SaveSalesTransaction((SaveCartDataRequest)request);
            }
            else if (request is InsertSalesTransactionTablesDataRequest)
            {
                response = InsertSalesTransactionTables((InsertSalesTransactionTablesDataRequest)request);
            }
            else if (request is DeleteCartDataRequest)
            {
                response = DeleteCart((DeleteCartDataRequest)request);
            }
            else if (request is GetSalesTransactionDataRequest)
            {
                response = GetSalesTransaction((GetSalesTransactionDataRequest)request);
            }
            else if (request is GetDiscountLinesDataRequest)
            {
                response = GetDiscountLines((GetDiscountLinesDataRequest)request);
            }
            else if (request is GetLoyaltyRewardPointLinesDataRequest)
            {
                response = GetLoyaltyRewardPointLines((GetLoyaltyRewardPointLinesDataRequest)request);
            }
            else if (request is GetSalesLinesDataRequest)
            {
                response = GetSalesLines((GetSalesLinesDataRequest)request);
            }
            else
            {
                string message = string.Format("Request type '{0}' is not supported", request.GetType().FullName);
                throw new NotSupportedException(message);
            }

            return response;
        }

        private static EntityDataServiceResponse<SalesOrder> GetSalesTransaction(GetSalesTransactionDataRequest request)
        {
            IEnumerable<SalesOrder> transactions = new SalesOrderDataManager(request.RequestContext)
                .SearchSalesOrders(request.QueryResultSettings, request.SearchCriteria);

            return new EntityDataServiceResponse<SalesOrder>(transactions.AsReadOnly());
        }

        private static NullResponse DeleteCart(DeleteCartDataRequest request)
        {
            GetTransactionDataManager(request.RequestContext).DeleteTransactions(request.SalesTransactions);
            return new NullResponse();
        }

        private static NullResponse InsertSalesTransactionTables(InsertSalesTransactionTablesDataRequest request)
        {
            //N by RxL: DMND0026205-FDD-FIN-Multiple Authorizations
            //Call AFMPreInsertSalesTransactionTables to populate Line confirmation Id in Payment trans line
            AFMPreInsertSalesTransactionTables(request);

            ParameterSet parameters = new ParameterSet();
            parameters[DatabaseAccessor.ChannelIdVariableName] = request.RequestContext.GetPrincipal().ChannelId;
            parameters["@tvp_Transaction"] = request.TransactionTable;
            parameters["@tvp_SalesTrans"] = request.LinesTable;
            parameters["@tvp_IncomeExpense"] = request.IncomeExpenseTable;
            parameters["@tvp_MarkupTrans"] = request.MarkupTable;
            parameters["@tvp_PaymentTrans"] = request.PaymentTable;
            parameters["@tvp_TaxTrans"] = request.TaxTable;
            parameters["@tvp_AttributeTrans"] = request.AttributeTable;
            parameters["@tvp_AddressTrans"] = request.AddressTable;
            parameters["@tvp_DiscountTrans"] = request.DiscountTable;
            parameters["@tvp_InfoCodeTrans"] = request.ReasonCodeTable;
            parameters["@tvp_SalesProperties"] = request.PropertiesTable;
            parameters["@tvp_RewardPointTrans"] = request.RewardPointTable;
            parameters["@tvp_AffiliationTrans"] = request.AffiliationsTable;
            parameters["@tvp_CustomerOrderTrans"] = request.CustomerOrderTable;
            parameters["@tvp_InvoiceTrans"] = request.InvoiceTable;

            int errorCode;

            using (var databaseContext = new SqlServerDatabaseContext(request.RequestContext))
            {
                errorCode = databaseContext.ExecuteStoredProcedureNonQuery("InsertSalesOrder", parameters);                
            }

            if (errorCode != (int)DatabaseErrorCodes.Success)
            {
                throw new StorageException(StorageErrors.CriticalError, errorCode, "Unable to save sales order.");
            }           
            
            return new NullResponse();
        }

        private static NullResponse SaveSalesTransaction(SaveCartDataRequest request)
        {
            GetTransactionDataManager(request.RequestContext).SaveTransactions(request.SalesTransactions, request.IgnoreRowVersionCheck);
            return new NullResponse();
        }

        private static EntityDataServiceResponse<DiscountLine> GetDiscountLines(GetDiscountLinesDataRequest request)
        {
            using (SqlServerDatabaseContext databaseContext = new SqlServerDatabaseContext(request.RequestContext))
            {
                GetDiscountLinesProcedure getDiscountLinesProcedure = new GetDiscountLinesProcedure(request, databaseContext);
                return getDiscountLinesProcedure.Execute();
            }
        }

        private static EntityDataServiceResponse<LoyaltyRewardPointLine> GetLoyaltyRewardPointLines(GetLoyaltyRewardPointLinesDataRequest request)
        {
            using (SqlServerDatabaseContext databaseContext = new SqlServerDatabaseContext(request.RequestContext))
            {
                GetLoyaltyRewardPointLinesProcedure getLoyaltyRewardPointLinesProcedure = new GetLoyaltyRewardPointLinesProcedure(request, databaseContext);
                return getLoyaltyRewardPointLinesProcedure.Execute();
            }
        }

        private static EntityDataServiceResponse<SalesLine> GetSalesLines(GetSalesLinesDataRequest request)
        {
            using (SqlServerDatabaseContext databaseContext = new SqlServerDatabaseContext(request.RequestContext))
            {
                GetSalesLinesProcedure getSalesLinesProcedure = new GetSalesLinesProcedure(request, databaseContext);
                return getSalesLinesProcedure.Execute();
            }
        }

        private static SalesTransactionDataManager GetTransactionDataManager(RequestContext context)
        {
            return new SalesTransactionDataManager(context);
        }
    }
}