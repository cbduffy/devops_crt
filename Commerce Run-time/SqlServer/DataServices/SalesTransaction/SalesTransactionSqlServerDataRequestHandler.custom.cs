﻿//N by RxL: DMND0026205-FDD-FIN-Multiple Authorizations

using Microsoft.Dynamics.Commerce.Runtime.Data.Types;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
using System;
using System.Collections.Generic;
namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer.DataServices
{    
    /// <summary>
    /// This partial class contains custom methods required for SalesTransactionSqlServerDataRequestHandler method
    /// </summary>
    public sealed partial class SalesTransactionSqlServerDataRequestHandler
    {
        /// <summary>
        /// This method sets a field value for given row
        /// </summary>
        /// <param name="row">Transaction Table row</param>
        /// <param name="field">Column Name</param>
        /// <param name="value">value</param>
        static void SetField(DataRow row, string field, object value)
        {
            ThrowIf.Null<DataRow>(row, "row");
            ThrowIf.Null<string>(field, "field");
            row[field] = value;
        }

        /// <summary>
        /// This method populates the LineConfirmation Id in Payment Trans line
        /// </summary>
        /// <param name="request">InsertSalesTransactionTablesDataRequest</param>
        private static void AFMPreInsertSalesTransactionTables(InsertSalesTransactionTablesDataRequest request)
        {
            request.PaymentTable.Columns.Add("AFMLINECONFIRMATIONID", typeof(string)).DefaultValue = (object)string.Empty;

            var tederLines = request.RequestContext.GetSalesTransaction().ActiveTenderLines;           

            foreach (DataRow tenderRow in request.PaymentTable.Rows)
            {
                foreach(var tenderLine in tederLines )
                {
                    if (tenderLine.LineNumber == Convert.ToDecimal(tenderRow["LINENUM"]))
                    {
                        //CS by spriya : DMND0010113 - Synchrony Financing online
                       // if (Convert.ToInt32(tenderLine["LineConfirmationGroupId"]) != null && Convert.ToInt32(tenderLine["LineConfirmationGroupId"])>0)
                        SalesTransactionSqlServerDataRequestHandler.SetField(tenderRow, "AFMLINECONFIRMATIONID",
                            (object)(request.TransactionTable.Rows[0]["CHANNELREFERENCEID"].ToString() + (Convert.ToInt32(tenderLine["LineConfirmationGroupId"]).ToString("00") ?? string.Empty)));
                        /*else
                            SalesTransactionSqlServerDataRequestHandler.SetField(tenderRow, "AFMLINECONFIRMATIONID",
                                (object)(request.TransactionTable.Rows[0]["CHANNELREFERENCEID"].ToString() ?? string.Empty));*/
                        //CE by spriya : DMND0010113 - Synchrony Financing online
                    }
                }
            }
        }
    }
}
