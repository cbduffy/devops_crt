﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer.DataServices.ReasonCodes
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Reason code data request handler for SQLServer.
    /// </summary>
    public sealed class ReasonCodeSqlServerDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get 
            {
                yield return typeof(GetReasonCodesDataRequest);
            }
        }

        /// <summary>
        /// Gets the sales transaction to be saved.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <returns>The response message.</returns>
        public Response Execute(Request request)
        {
            Response response;

            if (request is GetReasonCodesDataRequest)
            {
                response = GetReasonCodes((GetReasonCodesDataRequest)request);
            }
            else
            {
                string message = string.Format("Request type '{0}' is not supported", request.GetType().FullName);
                throw new NotSupportedException(message);
            }

            return response;
        }

        private static EntityDataServiceResponse<ReasonCode> GetReasonCodes(GetReasonCodesDataRequest request)
        {
            ReasonCodeDataManager reasonCodeDataManager = new ReasonCodeDataManager(request.RequestContext);
            ReadOnlyCollection<ReasonCode> reasonCodes = reasonCodeDataManager.GetReasonCodes(request.ReasonCodeId, request.QueryResultSettings);

            return new EntityDataServiceResponse<ReasonCode>(reasonCodes);
        }
    }
}