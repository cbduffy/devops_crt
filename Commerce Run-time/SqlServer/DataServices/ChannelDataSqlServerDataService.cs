﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Data.Types;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Channel data services that contains methods to retrieve the information by calling views.
    /// </summary>
    public class ChannelDataSqlServerDataRequestHandler : IRequestHandler
    {
        private const string ChannelCategoryAttributeViewName = "CHANNELCATEGORYATTRIBUTEVIEW";
        private const string SqlParamChannelId = "@channelId";

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(GetDeviceDataServiceRequest);
                yield return typeof(GetChannelCategoryAttributesDataRequest);                
            }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;

            if (requestType == typeof(GetDeviceDataServiceRequest))
            {
                response = GetDevice((GetDeviceDataServiceRequest)request);
            }
            else if (requestType == typeof(GetChannelCategoryAttributesDataRequest))
            {
                response = GetChannelCategoryAttributes((GetChannelCategoryAttributesDataRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }
        
        /// <summary>
        /// Gets the device by device identifier.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        private static SingleEntityDataServiceResponse<Device> GetDevice(GetDeviceDataServiceRequest request)
        {
            DeviceDataManager deviceDataManager = GetDeviceDataManagerInstance(request.RequestContext);
            Device device = deviceDataManager.GetDeviceByDeviceId(request.DeviceNumber, request.IsActivatedDeviceOnly);
            return new SingleEntityDataServiceResponse<Device>(device);
        }

        private static EntityDataServiceResponse<ChannelCategoryAttribute> GetChannelCategoryAttributes(GetChannelCategoryAttributesDataRequest request)
        {
            ReadOnlyCollection<ChannelCategoryAttribute> channelCategoryAttributes;

            var query = new SqlPagedQuery
            {
                Select = request.QueryResultSettings.ColumnSet,
                Paging = request.QueryResultSettings.Paging,
                From = ChannelCategoryAttributeViewName,
                Where = "HOSTCHANNEL = " + SqlParamChannelId,
                IsQueryByPrimaryKey = false
            };

            using (RecordIdTableType categoryRecordIds = new RecordIdTableType(request.CategoryIds, "CATEGORY"))
            using (SqlServerDatabaseContext context = new SqlServerDatabaseContext(request.RequestContext))
            {
                query.Parameters[SqlParamChannelId] = request.ChannelId;
                query.Parameters["@TVP_RECIDTABLETYPE"] = categoryRecordIds;

                channelCategoryAttributes = context.ReadEntity<ChannelCategoryAttribute>(query);
            }

            return new EntityDataServiceResponse<ChannelCategoryAttribute>(channelCategoryAttributes);
        }
        
        /// <summary>
        /// Gets the device data manager instance.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The device data manager.</returns>
        private static DeviceDataManager GetDeviceDataManagerInstance(RequestContext context)
        {
            return new DeviceDataManager(context);
        }
    }
}
