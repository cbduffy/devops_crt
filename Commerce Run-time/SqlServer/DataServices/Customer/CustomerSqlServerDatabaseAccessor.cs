﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer.DataServices
{
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;

    /// <summary>
    /// Customer SQL database accessor class.
    /// </summary>
    public sealed class CustomerSqlServerDatabaseAccessor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerSqlServerDatabaseAccessor"/> class.
        /// </summary>
        /// <param name="context">The request context.</param>
        public CustomerSqlServerDatabaseAccessor(RequestContext context)
        {
            this.Context = context;
        }

        private RequestContext Context { get; set; }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        /// <param name="keyword">Optional record identifier of the customer to retrieve.</param>
        /// <param name="onlyCurrentCompany">If set to <c>true</c> [only current company].</param>
        /// <param name="settings">The query result settings.</param>
        /// <returns>
        /// A collection of customers.
        /// </returns>
        public ReadOnlyCollection<GlobalCustomer> SearchCustomers(string keyword, bool onlyCurrentCompany, QueryResultSettings settings)
        {
            CustomerDataManager dataManager = this.GetDataManagerInstance();
            return dataManager.SearchCustomers(keyword, onlyCurrentCompany, this.Context.GetPrincipal().ChannelId, settings);
        }

        /// <summary>
        /// Gets the customer by account number.
        /// </summary>
        /// <param name="accountNumber">The account number of the customer to retrieved.</param>
        /// <returns>
        /// The customer.
        /// </returns>
        public Customer GetCustomerByAccountNumber(string accountNumber)
        {
            CustomerDataManager dataManager = this.GetDataManagerInstance();
            return dataManager.GetCustomerByAccountNumber(accountNumber);
        }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        /// <param name="recordId">Optional record identifier of the customer to retrieve.</param>
        /// <param name="accountNumber">Optional account number of the customer to retrieve.</param>
        /// <param name="dirPartyRecId">Optional record identifier for the directory party table of the customer to retrieve.</param>
        /// <param name="partyNumber">The party number.</param>
        /// <param name="settings">The query result settings.</param>
        /// <returns>
        /// A collection of customers.
        /// </returns>
        public ReadOnlyCollection<Customer> GetCustomers(long recordId, string accountNumber, long dirPartyRecId, string partyNumber, QueryResultSettings settings)
        {
            CustomerDataManager dataManager = this.GetDataManagerInstance();
            return dataManager.GetCustomers(recordId, accountNumber, dirPartyRecId, partyNumber, settings);
        }

        /// <summary>
        /// Save customer account activation request to channel DB.
        /// </summary>
        /// <param name="email">E-mail address.</param>
        /// <param name="activationToken">Activation token (a GUID).</param>
        public void CompleteCustomerAccountActivationRequest(string email, string activationToken)
        {
            CustomerDataManager dataManager = this.GetDataManagerInstance();
            dataManager.CompleteCustomerAccountActivationRequest(email, activationToken);
        }

        /// <summary>
        /// Creates or updates a customer address.
        /// </summary>
        /// <param name="customer">The customer data.</param>
        /// <returns>Updated customer.</returns>
        public Customer CreateOrUpdateCustomer(Customer customer)
        {
            CustomerDataManager dataManager = this.GetDataManagerInstance();
            return dataManager.CreateOrUpdateCustomer(customer);
        }        

        /// <summary>
        /// Save customer account activation request to channel DB.
        /// </summary>
        /// <param name="email">E-mail address.</param>
        /// <param name="activationToken">Activation token (a GUID).</param>
        public void SaveCustomerAccountActivationRequest(string email, string activationToken)
        {
            CustomerDataManager dataManager = this.GetDataManagerInstance();
            dataManager.SaveCustomerAccountActivationRequest(email, activationToken);
        }

        /// <summary>
        /// Validate the account activation request.
        /// </summary>
        /// <param name="email">E-mail address.</param>
        /// <param name="activationToken">Activation token (a GUID).</param>
        /// <returns>True if the request is valid; otherwise return false.</returns>
        public bool ValidateAccountActivationRequest(string email, string activationToken)
        {
            CustomerDataManager dataManager = this.GetDataManagerInstance();
            return dataManager.ValidateAccountActivationRequest(email, activationToken);
        }

        /// <summary>
        /// Gets the customer data manager instance.
        /// </summary>
        /// <returns>An instance of <see cref="CustomerDataManager"/></returns>
        private CustomerDataManager GetDataManagerInstance()
        {
            return new CustomerDataManager(this.Context);
        }
    }
}
