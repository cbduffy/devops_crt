﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer.DataServices
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Customer data requests handler that retrieves the customer information from underlying data storage.
    /// </summary>
    public class CustomerSqlServerDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(CompleteCustomerAccountActivationDataRequest);
                yield return typeof(CreateOrUpdateCustomerDataRequest);
                yield return typeof(GetCustomerDataRequest);
                yield return typeof(GetCustomersDataRequest);
                yield return typeof(SearchCustomersDataRequest);
                yield return typeof(SaveCustomerAccountActivationDataRequest);
                yield return typeof(ValidateAccountActivationDataRequest);
                yield return typeof(GetCustomerLoyaltyCardsDataRequest);
            }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestedType = request.GetType();
            Response response;

            if (requestedType == typeof(CompleteCustomerAccountActivationDataRequest))
            {
                response = this.CompleteCustomerAccountActivation((CompleteCustomerAccountActivationDataRequest)request);
            }
            else if (requestedType == typeof(CreateOrUpdateCustomerDataRequest))
            {
                response = this.CreateOrUpdateCustomer((CreateOrUpdateCustomerDataRequest)request);
            }
            else if (requestedType == typeof(GetCustomerDataRequest))
            {
                response = this.GetCustomerByAccountNumber((GetCustomerDataRequest)request);
            }
            else if (requestedType == typeof(GetCustomersDataRequest))
            {
                response = this.GetCustomers((GetCustomersDataRequest)request);
            }
            else if (requestedType == typeof(SearchCustomersDataRequest))
            {
                response = this.SearchCustomers((SearchCustomersDataRequest)request);
            }
            else if (requestedType == typeof(SaveCustomerAccountActivationDataRequest))
            {
                response = this.SaveCustomerAccountActivation((SaveCustomerAccountActivationDataRequest)request);
            }
            else if (requestedType == typeof(ValidateAccountActivationDataRequest))
            {
                response = this.ValidateAccountActivation((ValidateAccountActivationDataRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// Gets the customer SQLServer database accessor instance.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>An instance of <see cref="CustomerSqlServerDatabaseAccessor"/></returns>
        private CustomerSqlServerDatabaseAccessor GetSqlDatabaseAccessorInstance(RequestContext context)
        {
            return new CustomerSqlServerDatabaseAccessor(context);
        }

        /// <summary>
        /// Gets the customer by account number.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private SingleEntityDataServiceResponse<Customer> GetCustomerByAccountNumber(GetCustomerDataRequest request)
        {
            CustomerSqlServerDatabaseAccessor databaseAccessor = this.GetSqlDatabaseAccessorInstance(request.RequestContext);
            Customer customer = databaseAccessor.GetCustomerByAccountNumber(request.AccountNumber);

            return new SingleEntityDataServiceResponse<Customer>(customer);
        }

        /// <summary>
        /// Gets the customers.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private EntityDataServiceResponse<Customer> GetCustomers(GetCustomersDataRequest request)
        {
            CustomerSqlServerDatabaseAccessor databaseAccessor = this.GetSqlDatabaseAccessorInstance(request.RequestContext);
            ReadOnlyCollection<Customer> customers = databaseAccessor.GetCustomers(request.RecordId, request.AccountNumber, request.DirPartyRecordId, request.PartyNumber, request.QueryResultSettings);

            return new EntityDataServiceResponse<Customer>(customers);
        }

        /// <summary>
        /// Searches the customers with given search conditions.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private EntityDataServiceResponse<GlobalCustomer> SearchCustomers(SearchCustomersDataRequest request)
        {
            CustomerSqlServerDatabaseAccessor databaseAccessor = this.GetSqlDatabaseAccessorInstance(request.RequestContext);
            ReadOnlyCollection<GlobalCustomer> globalCustomers = databaseAccessor.SearchCustomers(request.Keyword, request.SearchCurrentCompanyOnly, request.QueryResultSettings);

            return new EntityDataServiceResponse<GlobalCustomer>(globalCustomers);
        }

        /// <summary>
        /// Saves customer account activation request to channel DB.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private NullResponse CompleteCustomerAccountActivation(CompleteCustomerAccountActivationDataRequest request)
        {
            CustomerSqlServerDatabaseAccessor databaseAccessor = this.GetSqlDatabaseAccessorInstance(request.RequestContext);
            databaseAccessor.CompleteCustomerAccountActivationRequest(request.Email, request.ActivationToken);

            return new NullResponse();
        }

        /// <summary>
        /// Creates or updates a customer address.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private SingleEntityDataServiceResponse<Customer> CreateOrUpdateCustomer(CreateOrUpdateCustomerDataRequest request)
        {
            CustomerSqlServerDatabaseAccessor databaseAccessor = this.GetSqlDatabaseAccessorInstance(request.RequestContext);
            Customer customer = databaseAccessor.CreateOrUpdateCustomer(request.Customer);

            return new SingleEntityDataServiceResponse<Customer>(customer);
        }

        /// <summary>
        /// Saves the customer account activation request to channel DB.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private NullResponse SaveCustomerAccountActivation(SaveCustomerAccountActivationDataRequest request)
        {
            CustomerSqlServerDatabaseAccessor databaseAccessor = this.GetSqlDatabaseAccessorInstance(request.RequestContext);
            databaseAccessor.SaveCustomerAccountActivationRequest(request.Email, request.ActivationToken);

            return new NullResponse();
        }

        /// <summary>
        /// Validates the account activation request.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private ValidateAccountActivationDataResponse ValidateAccountActivation(ValidateAccountActivationDataRequest request)
        {
            CustomerSqlServerDatabaseAccessor databaseAccessor = this.GetSqlDatabaseAccessorInstance(request.RequestContext);
            bool isValid = databaseAccessor.ValidateAccountActivationRequest(request.Email, request.ActivationToken);

            return new ValidateAccountActivationDataResponse(isValid);
        }
    }
}