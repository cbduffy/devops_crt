﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Pricing and discount data service class.
    /// </summary>
    public class PricingSqlDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(ReadPriceAdjustmentsDataServiceRequest);
                yield return typeof(ReadRetailDiscountsDataServiceRequest);
                yield return typeof(ReadPriceTradeAgreementsDataServiceRequest);
                yield return typeof(ReadDiscountTradeAgreementsDataServiceRequest);
                yield return typeof(GetPeriodicDiscountsDataServiceRequest);
                yield return typeof(GetQuantityDiscountDataServiceRequest);
                yield return typeof(EntityDataServiceRequest<IEnumerable<AffiliationLoyaltyTier>, PriceGroup>);
                yield return typeof(FindPriceAdjustmentsDataServiceRequest);
                yield return typeof(FindTradeAgreementsDataServiceRequest);
            }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;

            if (requestType == typeof(ReadPriceAdjustmentsDataServiceRequest))
            {
                response = this.ReadPriceAdjustments((ReadPriceAdjustmentsDataServiceRequest)request);
            }
            else if (requestType == typeof(ReadRetailDiscountsDataServiceRequest))
            {
                response = this.ReadRetailDiscounts((ReadRetailDiscountsDataServiceRequest)request);
            }
            else if (requestType == typeof(ReadPriceTradeAgreementsDataServiceRequest))
            {
                response = this.ReadPriceTradeAgreements((ReadPriceTradeAgreementsDataServiceRequest)request);
            }
            else if (requestType == typeof(ReadDiscountTradeAgreementsDataServiceRequest))
            {
                response = this.ReadDiscountTradeAgreements((ReadDiscountTradeAgreementsDataServiceRequest)request);
            }
            else if (requestType == typeof(GetPeriodicDiscountsDataServiceRequest))
            {
                response = this.GetPeriodicDiscounts((GetPeriodicDiscountsDataServiceRequest)request);
            }
            else if (requestType == typeof(GetQuantityDiscountDataServiceRequest))
            {
                response = this.GetQuantityDiscountLevelByQuantity((GetQuantityDiscountDataServiceRequest)request);
            }
            else if (requestType == typeof(EntityDataServiceRequest<IEnumerable<AffiliationLoyaltyTier>, PriceGroup>))
            {
                response = this.GetAffiliationPriceGroups((EntityDataServiceRequest<IEnumerable<AffiliationLoyaltyTier>, PriceGroup>)request);
            }
            else if (requestType == typeof(FindPriceAdjustmentsDataServiceRequest))
            {
                response = this.FindPriceAdjustments((FindPriceAdjustmentsDataServiceRequest)request);
            }
            else if (requestType == typeof(FindTradeAgreementsDataServiceRequest))
            {
                response = this.FindTradeAgreements((FindTradeAgreementsDataServiceRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType().ToString()));
            }

            return response;
        }

        private PricingDataManager GetDataManagerInstance(RequestContext context)
        {
            return new PricingDataManager(context);
        }

        private EntityDataServiceResponse<PriceAdjustment, ValidationPeriod> ReadPriceAdjustments(ReadPriceAdjustmentsDataServiceRequest request)
        {
            var pricingDataManager = this.GetDataManagerInstance(request.RequestContext);
            ReadOnlyCollection<ValidationPeriod> validationPeriods;

            var priceAdjustments = pricingDataManager.ReadPriceAdjustments(
                request.ItemUnits, 
                request.PriceGroups as ISet<string>, 
                request.MinActiveDate,
                request.MaxActiveDate, 
                out validationPeriods);

            return new EntityDataServiceResponse<PriceAdjustment, ValidationPeriod>(priceAdjustments, validationPeriods.ToArray());
        }

        private EntityDataServiceResponse<PeriodicDiscount, ValidationPeriod> ReadRetailDiscounts(ReadRetailDiscountsDataServiceRequest request)
        {
            var pricingSqlDatamaanger = this.GetDataManagerInstance(request.RequestContext);
            ReadOnlyCollection<ValidationPeriod> validationPeriods;

            var retailDiscounts = pricingSqlDatamaanger.ReadRetailDiscounts(
                request.ItemUnits, 
                request.PriceGroups as ISet<string>,
                request.MinActiveDate,
                request.MaxActiveDate, 
                request.CurrencyCode, 
                out validationPeriods);

            return new EntityDataServiceResponse<PeriodicDiscount, ValidationPeriod>(retailDiscounts, validationPeriods.ToArray());
        }

        private EntityDataServiceResponse<TradeAgreement> ReadPriceTradeAgreements(ReadPriceTradeAgreementsDataServiceRequest request)
        {
            var pricingSqlDatamaanger = this.GetDataManagerInstance(request.RequestContext);

            var retailPriceTradeAgreements = pricingSqlDatamaanger.ReadPriceTradeAgreements(
                request.ItemId, 
                request.PriceGroups, 
                request.CustomerAccount, 
                request.MinActiveDate,
                request.MaxActiveDate, 
                request.CurrencyCode);

            return new EntityDataServiceResponse<TradeAgreement>(retailPriceTradeAgreements);
        }

        private EntityDataServiceResponse<TradeAgreement> ReadDiscountTradeAgreements(ReadDiscountTradeAgreementsDataServiceRequest request)
        {
            var pricingSqlDataManager = this.GetDataManagerInstance(request.RequestContext);

            var retailDiscountTradeAgreements = pricingSqlDataManager.ReadDiscountTradeAgreements(
                request.ItemId, 
                request.CustomerAccount, 
                request.MinActiveDate,
                request.MaxActiveDate, 
                request.CurrencyCode);

            return new EntityDataServiceResponse<TradeAgreement>(retailDiscountTradeAgreements);
        }

        private EntityDataServiceResponse<PeriodicDiscount> GetPeriodicDiscounts(GetPriceAndDiscountDataServiceRequest request)
        {
            var pricingSqlDataManager = this.GetDataManagerInstance(request.RequestContext);

            var retailPeriodicDiscounts = pricingSqlDataManager.GetPeriodicDiscounts(request.ProductId, request.VariantId, request.PriceGroupIds, request.CurrencyCode, request.MinActiveDate);

            return new EntityDataServiceResponse<PeriodicDiscount>(retailPeriodicDiscounts);
        }

        private EntityDataServiceResponse<QuantityDiscountLevel> GetQuantityDiscountLevelByQuantity(GetQuantityDiscountDataServiceRequest request)
        {
            var pricingSqlDataManager = this.GetDataManagerInstance(request.RequestContext);

            var quantityDiscountLevel = pricingSqlDataManager.GetQuantityDiscountLevelByQuantity(request.OfferId, request.Quantity);

            return new EntityDataServiceResponse<QuantityDiscountLevel>(new ReadOnlyCollection<QuantityDiscountLevel>(new[] { quantityDiscountLevel }));
        }

        private EntityDataServiceResponse<PriceGroup> GetAffiliationPriceGroups(EntityDataServiceRequest<IEnumerable<AffiliationLoyaltyTier>, PriceGroup> request)
        {
            var pricingSqlDataManager = this.GetDataManagerInstance(request.RequestContext);

            var affiliationPriceGroups = pricingSqlDataManager.GetAffiliationPriceGroups(request.RequestParameter);

            return new EntityDataServiceResponse<PriceGroup>(affiliationPriceGroups);
        }

        private EntityDataServiceResponse<PriceAdjustment> FindPriceAdjustments(FindPriceAdjustmentsDataServiceRequest request)
        {
            var pricingSqlDataManager = this.GetDataManagerInstance(request.RequestContext);

            var priceAdjustments = pricingSqlDataManager.FindPriceAdjustments(
                request.ProductId, 
                request.VariantId,
                request.PriceGroupIds, 
                request.CurrencyCode, 
                request.UnitOfMeasure, 
                request.MinActiveDate);

            return new EntityDataServiceResponse<PriceAdjustment>(priceAdjustments);
        }

        private EntityDataServiceResponse<TradeAgreement> FindTradeAgreements(FindTradeAgreementsDataServiceRequest request)
        {
            var pricingSqlDataManager = this.GetDataManagerInstance(request.RequestContext);

            var tradeAgreements = pricingSqlDataManager.FindTradeAgreements(
                request.PriceDiscountType, 
                request.ItemCode,
                request.ItemId.Single(), 
                request.AccountCode, 
                request.AccountRelations, 
                request.UnitId,
                request.CurrencyCode, 
                request.Quantity, 
                request.Variant, 
                request.MinActiveDate);

            return new EntityDataServiceResponse<TradeAgreement>(tradeAgreements);
        }
    }
}
