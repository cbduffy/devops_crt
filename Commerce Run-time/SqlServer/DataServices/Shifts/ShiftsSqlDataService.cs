﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// The shifts SQL server data service.
    /// </summary>
    public class ShiftsSqlDataRequestHandler : IRequestHandler
    {
        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(UpdateShiftStagingTableDataServiceRequest);
                yield return typeof(CreateShiftDataServiceRequest);
                yield return typeof(GetShiftDataDataRequest);
                yield return typeof(DeleteShiftDataServiceRequest);
                yield return typeof(GetShiftRequiredAmountsPerTenderDataRequest);
            }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;

            if (requestType == typeof(UpdateShiftStagingTableDataServiceRequest))
            {
                response = this.UpdateShiftStaging((UpdateShiftStagingTableDataServiceRequest)request);
            }
            else if (requestType == typeof(CreateShiftDataServiceRequest))
            {
                response = this.InsertShiftsStagingTable((CreateShiftDataServiceRequest)request);
            }
            else if (requestType == typeof(GetShiftDataDataRequest))
            {
                response = this.GetShiftData((GetShiftDataDataRequest)request);
            }
            else if (requestType == typeof(DeleteShiftDataServiceRequest))
            {
                response = this.DeleteShiftStagingTable((DeleteShiftDataServiceRequest)request);
            }
            else if (requestType == typeof(GetShiftRequiredAmountsPerTenderDataRequest))
            {
                response = this.GetShiftRequiredAmountsPerTender((GetShiftRequiredAmountsPerTenderDataRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        private Response GetShiftData(GetShiftDataDataRequest request)
        {
            using (SqlServerDatabaseContext databaseContext = new SqlServerDatabaseContext(request.RequestContext))
            {
                GetShiftDataProcedure procedure = new GetShiftDataProcedure(request, databaseContext);
                return procedure.Execute();
            }
        }

        private NullResponse InsertShiftsStagingTable(CreateShiftDataServiceRequest request)
        {
            var databaseAccessor = new ShiftDataManager(request.RequestContext);
            databaseAccessor.Insert(request.Shift);

            return new NullResponse();
        }

        /// <summary>
        /// The data service method to update the shift staging table.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private NullResponse UpdateShiftStaging(UpdateShiftStagingTableDataServiceRequest request)
        {
            var shiftDataManager = this.GetDataManagerInstance(request.RequestContext);
            shiftDataManager.Update(request.Shift);

            return new NullResponse();
        }

        /// <summary>
        /// The data service method to retrieve the required tender declaration amounts of a shift per tender type.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private EntityDataServiceResponse<ShiftTenderLine> GetShiftRequiredAmountsPerTender(GetShiftRequiredAmountsPerTenderDataRequest request)
        {
            ShiftDataManager shiftDataManager = this.GetDataManagerInstance(request.RequestContext);
            ReadOnlyCollection<ShiftTenderLine> shiftTenderLines = shiftDataManager.GetShiftRequiredAmountsPerTender(request.TerminalId, request.ShiftId);

            return new EntityDataServiceResponse<ShiftTenderLine>(shiftTenderLines);
        }

        private ShiftDataManager GetDataManagerInstance(RequestContext context)
        {
            return new ShiftDataManager(context);
        }

        private NullResponse DeleteShiftStagingTable(DeleteShiftDataServiceRequest request)
        {
            var shiftDataManager = this.GetDataManagerInstance(request.RequestContext);
            shiftDataManager.DeleteShiftStaging(request.Shift);

            return new NullResponse();
        }
    }
}
