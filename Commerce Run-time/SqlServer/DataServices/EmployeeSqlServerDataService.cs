﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Messages;
    using Microsoft.Dynamics.Commerce.Runtime.Workflow;

    /// <summary>
    /// Employee data services that contains methods to retrieve the information by calling views.
    /// </summary>
    public class EmployeeSqlServerDataRequestHandler : IRequestHandler
    {
        private const string LockUserSprocName = "INSERTRETAILSTAFFLOGINLOG";
        private const string UnlockUserSprocName = "DELETERETAILSTAFFLOGINLOG";
        private const string ChannelIdParamName = "@CHANNELID";
        private const string StaffIdParamName = "@STAFFID";
        private const string DataAreaIdParamName = "@DATAAREAID";
        private const string ReturnValueParamName = "@RETURN_VALUE";
        private const string TerminalIdParamName = "@TERMINALID";

        /// <summary>
        /// Gets the collection of supported request types by this handler.
        /// </summary>
        public IEnumerable<Type> SupportedRequestTypes
        {
            get
            {
                yield return typeof(LockUserAtLogOnDataRequest);
                yield return typeof(UnLockUserAtLogOffDataRequest);
            }
        }

        /// <summary>
        /// Represents the entry point of the request handler.
        /// </summary>
        /// <param name="request">The incoming request message.</param>
        /// <returns>The outgoing response message.</returns>
        public Response Execute(Request request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            Type requestType = request.GetType();
            Response response;
    
            if (requestType == typeof(LockUserAtLogOnDataRequest))
            {
                response = this.LockUserAtLogOn((LockUserAtLogOnDataRequest)request);
            }
            else if (requestType == typeof(UnLockUserAtLogOffDataRequest))
            {
                response = this.UnLockUserAtLogOff((UnLockUserAtLogOffDataRequest)request);
            }
            else
            {
                throw new NotSupportedException(string.Format("Request '{0}' is not supported.", request.GetType()));
            }

            return response;
        }

        /// <summary>
        /// Unlock the current user.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private SingleEntityDataServiceResponse<bool> UnLockUserAtLogOff(UnLockUserAtLogOffDataRequest request)
        {
            ParameterSet parameters = new ParameterSet();
            parameters[ChannelIdParamName] = request.ChannelId;
            parameters[StaffIdParamName] = request.StaffId;
            parameters[DataAreaIdParamName] = request.DataAreaId;
            ParameterSet outputParameters = new ParameterSet();
            outputParameters[ReturnValueParamName] = 0;

            using (SqlServerDatabaseContext databaseContext = new SqlServerDatabaseContext(request.RequestContext))
            {
                databaseContext.ExecuteStoredProcedureScalar(UnlockUserSprocName, parameters, outputParameters);
            }

            bool result = false;
            if ((int)outputParameters[ReturnValueParamName] == 1)
            {
                result = true;
            }

            return new SingleEntityDataServiceResponse<bool>(result);
        }

        /// <summary>
        /// Lock the current user, so that same user can't log into another terminal until log off from the current terminal.
        /// </summary>
        /// <param name="request">The data service request.</param>
        /// <returns>The data service response.</returns>
        private SingleEntityDataServiceResponse<bool> LockUserAtLogOn(LockUserAtLogOnDataRequest request)
        {
            ParameterSet parameters = new ParameterSet();
            parameters[TerminalIdParamName] = request.TerminalId;
            parameters[ChannelIdParamName] = request.ChannelId;
            parameters[StaffIdParamName] = request.StaffId;
            parameters[DataAreaIdParamName] = request.DataAreaId;
            ParameterSet outputParameters = new ParameterSet();
            outputParameters[ReturnValueParamName] = 0;

            using (SqlServerDatabaseContext databaseContext = new SqlServerDatabaseContext(request.RequestContext))
            {
                databaseContext.ExecuteStoredProcedureScalar(LockUserSprocName, parameters, outputParameters);
            }

            bool result = false;
            if ((int)outputParameters[ReturnValueParamName] == 1)
            {
                result = true;
            }

            return new SingleEntityDataServiceResponse<bool>(result);
        }

        /// <summary>
        /// Gets the data manager instance.
        /// </summary>
        /// <param name="context">The request context.</param>
        /// <returns>An instance of <see cref="EmployeeDataManager"/></returns>
        private EmployeeDataManager GetDataManagerInstance(RequestContext context)
        {
            return new EmployeeDataManager(context);
        }
    }
}
