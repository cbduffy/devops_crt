﻿using AFM.Commerce.Framework.Core.CRTServices.Response;
using AFM.Commerce.Framework.Core.DataManagers;
using AFM.Commerce.Framework.Core.Models;
using Avalara.Ax.CRT;
using Microsoft.Dynamics.Commerce.Runtime.DataModel;
using Microsoft.Dynamics.Commerce.Runtime.DataServices.Messages;
using Microsoft.Dynamics.Retail.Diagnostics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AddressValidationDll.Service.Implementations;
using AddressValidationDll.DataModels;
using AFM.Commerce.Runtime.Services;

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer
{
    public partial class AddressSqlServerDataRequestHandler
    {
        private ValidateAddressDataResponse ValidateAddress(ValidateAddressDataRequest request)
        {
            ThrowIf.Null(request.Address, "address");

            #region Commented
            //Commented by Kishore for Shipping Address Validation
            // string addressLine = request.Address.Street;
            // request.Address.Street = request.Address.Street.Replace(Environment.NewLine, " ");
            //Commented by Kishore for Shipping Address Validation
            #endregion

            if (string.IsNullOrWhiteSpace(request.Address.ThreeLetterISORegionName))
            {
                return CreateFailedValidateAddressDataResponse("ThreeLetterISORegionName");
            }
            //Commented by Kishore Shipping Address Validation
            //Address address = request.Address;

            //CR 306 - Address validation off - CS Developed by spriya Dated 09/14/2015
            if (request.Address.AddressType == AddressType.Invoice)
            {
                var dataManager = new AFMAddressDataManager(request.RequestContext);
                AFMZipCodeMasterData zipCodeMasterData = new AFMZipCodeMasterData(request.Address.City, request.Address.State, request.Address.ZipCode.Substring(0,5),
                    request.Address.ThreeLetterISORegionName, request.Address.AddressType);
                AFMAddressValidationData validatedAddressData = dataManager.GetAddressValidationData(zipCodeMasterData);
                if (validatedAddressData.ServiceByPassFlag)
                {
                    return new ValidateAddressDataResponse(validatedAddressData.IsZipCodeMasterValid, null, null);
                }
            }

            //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
            string[] adressStreet = null;
            if (request.Address.Street.Contains(Environment.NewLine))
            {
                string[] splitchar = { Environment.NewLine };
                adressStreet = request.Address.Street.Split(splitchar, StringSplitOptions.None);
            }
            else
            {
                adressStreet = new string[2];
                adressStreet[0] = request.Address.Street;
                adressStreet[1] = string.Empty;
            }

            AddressValidationDll.DataModels.Address addressValidationAddress = new AddressValidationDll.DataModels.Address()
            {

                City = request.Address.City,
                Country = CustomerConstants.TwoLetterCountryCode,
                State = request.Address.State,
                StreetLine1 = adressStreet[0],
                StreetLine2 = adressStreet[1],
                Type = request.Address.AddressType.ToString(),
                Zip = request.Address.ZipCode
            };
            AddressValidationService addressValidationServiceRequest = new AddressValidationService();
            AddressValidationResponse response = addressValidationServiceRequest.ValidateAddress(addressValidationAddress);
            bool isValid = response.IsValid;
            string errorMsg = response.ErrorMsg;

            //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore

            #region Commented
            //Commented by kishore for Shipping Address Valiadtion
            //CR 306 - Address validation off - CE Developed by spriya Dated 09/14/2015

            //NS Developed by  spriya for AFM_TFS_46529 dated 6/18/2014 
            // avalara code merge start
            
            //Avalara.Ax.CRT.AvaTaxAddressValidateCls avaTaxAddressValidateCls = new AvaTaxAddressValidateCls();
            //string AvaAddressType;
            //IEnumerable<Address> suggestedAddresses = new ObservableCollection<Address>();
            //ValidateAddressDataRequest validateAddressServiceRequest = (ValidateAddressDataRequest)request;
            //GetCRTAvaAddressValidationResponse getCRTAvaAddressValidationResponse = new GetCRTAvaAddressValidationResponse();
            //getCRTAvaAddressValidationResponse = avaTaxAddressValidateCls.ValidateShippingAddress(request, validateAddressServiceRequest.Address, out AvaAddressType, out suggestedAddresses);
             
            //PO Box check
            //if (!string.IsNullOrEmpty(AvaAddressType) && AvaAddressType.ToLower().Contains("p") && validateAddressServiceRequest.Address.AddressType == AddressType.Delivery)
            //{
            //    getCRTAvaAddressValidationResponse.IsValid = false;
            //    getCRTAvaAddressValidationResponse.ErrorMessage = "POERROR";
            //}
            //Commented by kishore for Shipping Address Valiadtion
            #endregion

            //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
            if (response.SuggestedAddress != null)
            {
                if (!string.IsNullOrEmpty(response.SuggestedAddress.Type) && response.SuggestedAddress.Type.ToLower().Contains("p") && addressValidationAddress.Type == AddressType.Delivery.ToString())
                {
                    isValid = false;
                    errorMsg = CustomerConstants.POError;
                }
            }
            //ES - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore

            #region Commented 
            //Commented by Kishore for Shipping Address Validation
            //NS developed by v-hapat for Bug 98006 dated on 11/14/2014            
            //if (getCRTAvaAddressValidationResponse.IsValid)
            //{
            //    if (getCRTAvaAddressValidationResponse.SuggestedAddress != null)
            //    {
            //        if (
            //            request.Address.Street.ToLower() != getCRTAvaAddressValidationResponse.SuggestedAddress.Street.ToLower() ||
            //            request.Address.City.ToLower() != getCRTAvaAddressValidationResponse.SuggestedAddress.City.ToLower() ||
            //            request.Address.State.ToLower() != getCRTAvaAddressValidationResponse.SuggestedAddress.State.ToLower() ||
            //            request.Address.ZipCode.ToLower().Split('-')[0] != getCRTAvaAddressValidationResponse.SuggestedAddress.ZipCode.ToLower().Split('-')[0]
            //            )
            //        {
            //            getCRTAvaAddressValidationResponse.IsValid = false;
            //        }
            //    }
            //}
            //Commented by Kishore for Shipping Address Validation
            #endregion

            //NS - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore
            if (isValid)
            {
                if (response.SuggestedAddress != null && !string.IsNullOrEmpty(response.SuggestedAddress.Zip)
                    && addressValidationAddress.Zip.ToLower() != response.SuggestedAddress.Zip.ToLower())
                {
                    if (addressValidationAddress.Zip.ToLower().Split('-')[0] == response.SuggestedAddress.Zip.ToLower().Split('-')[0])
                    {
                        response.SuggestedAddress.Zip = response.SuggestedAddress.Zip.ToLower().Split('-')[0];
                    }
                }
            }
            return new ValidateAddressDataResponse(isValid, errorMsg, errorMsg);
            //NE - DMND0026203-FDD-Shipping Address Validation - Developed by Kishore

            #region Commented
            //NE developed by v-hapat for Bug 98006 dated on 11/14/2014
            // request.Address.Street = addressLine;
            //return new ValidateAddressDataResponse(getCRTAvaAddressValidationResponse.IsValid, getCRTAvaAddressValidationResponse.ErrorMessage, getCRTAvaAddressValidationResponse.ErrorMessage);
            //return CreateFailedValidateAddressDataResponse(faultAddressComponent);
            // avalara merge end
            #endregion

        }

        /// <summary>
        /// Creates failed <see cref="ValidateAddressDataResponse"/> response.
        /// </summary>
        /// <param name="faultAddressComponent">The failed address component.</param>
        /// <returns>The <see cref="ValidateAddressDataResponse"/> response.</returns>
        private static ValidateAddressDataResponse CreateFailedValidateAddressDataResponse(string faultAddressComponent)
        {
            // If address is not valid, tell the user/client code : which component is the faulty one
            var message = string.Format(CultureInfo.InvariantCulture, @"Incorrect address provided: validate {0} property.", faultAddressComponent);
            NetTracer.Information(message);

            // create the response object and return
            return new ValidateAddressDataResponse(isAddressValid: false, invalidAddressComponentName: faultAddressComponent, errorMessage: message);
        }
    }
}
