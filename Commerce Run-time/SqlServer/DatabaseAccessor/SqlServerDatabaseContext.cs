﻿/*
SAMPLE CODE NOTICE

THIS SAMPLE CODE IS MADE AVAILABLE AS IS.  MICROSOFT MAKES NO WARRANTIES, WHETHER EXPRESS OR IMPLIED, 
OF FITNESS FOR A PARTICULAR PURPOSE, OF ACCURACY OR COMPLETENESS OF RESPONSES, OF RESULTS, OR CONDITIONS OF MERCHANTABILITY.  
THE ENTIRE RISK OF THE USE OR THE RESULTS FROM THE USE OF THIS SAMPLE CODE REMAINS WITH THE USER.  
NO TECHNICAL SUPPORT IS PROVIDED.  YOU MAY NOT DISTRIBUTE THIS CODE UNLESS YOU HAVE A LICENSE AGREEMENT WITH MICROSOFT THAT ALLOWS YOU TO DO SO.
*/

namespace Microsoft.Dynamics.Commerce.Runtime.DataServices.SqlServer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.Dynamics.Commerce.Runtime.Data;
    using Microsoft.Dynamics.Commerce.Runtime.Data.Types;
    using Microsoft.Dynamics.Commerce.Runtime.DataAccess.SqlServer;
    using Microsoft.Dynamics.Commerce.Runtime.DataModel;
    using Microsoft.Dynamics.Retail.Diagnostics;

    /// <summary>
    /// Wraps common operations associated to database access, maintaining single database connection. 
    /// </summary>
    internal class SqlServerDatabaseContext : DatabaseContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SqlServerDatabaseContext"/> class.
        /// </summary>
        /// <param name="requestContext">The request context.</param>
        public SqlServerDatabaseContext(RequestContext requestContext)
            : base(requestContext)
        {
        }

        /// <summary>
        /// Gets the database provider.
        /// </summary>
        private new SqlServerDatabaseProvider DatabaseProvider
        {
            get
            {
                return (SqlServerDatabaseProvider)base.DatabaseProvider;
            }
        }

        /// <summary>
        /// Executes a query that has no output.
        /// </summary>
        /// <param name="query">SQL query to be executed.</param>
        public void ExecuteNonQuery(IDatabaseQuery query)
        {
            try
            {
                using (IDatabaseResult result = this.DatabaseProvider.ExecuteQuery(this.ConnectionManager.Connection, query))
                {
                    ConsumeNonQueryResult(result);
                }
            }
            catch (DatabaseException exception)
            {
                throw new StorageException(StorageErrors.CriticalError, (int)exception.ErrorCode, exception.Message, exception);
            }
        }

        /// <summary>
        /// Executes a query that returns a single result.
        /// </summary>
        /// <typeparam name="T">The scalar type.</typeparam>
        /// <param name="query">The SQL query to be executed.</param>
        /// <returns>The scalar result.</returns>
        public T ExecuteScalar<T>(IDatabaseQuery query)
        {
            try
            {
                using (IDatabaseResult result = this.DatabaseProvider.ExecuteQuery(this.ConnectionManager.Connection, query))
                {
                    result.Read();
                    return result.GetValue<T>(0);
                }
            }
            catch (DatabaseException exception)
            {
                throw new StorageException(StorageErrors.CriticalError, (int)exception.ErrorCode, exception.Message, exception);
            }
        }

        /// <summary>
        /// Executes the stored procedure using the specified parameters.
        /// </summary>
        /// <typeparam name="T">The type of data being returned.</typeparam>
        /// <param name="procedureName">The name of the stored procedures.</param>
        /// <param name="parameters">The set of parameters.</param>
        /// <returns>A collection of <see cref="CommerceEntity"/> objects.</returns>
        public ReadOnlyCollection<T> ExecuteStoredProcedure<T>(string procedureName, ParameterSet parameters)
            where T : CommerceEntity, new()
        {
            return this.ExecuteStoredProcedure<T>(procedureName, parameters, null);
        }

        /// <summary>
        /// Executes the specified stored procedure.
        /// </summary>
        /// <typeparam name="T1">Type of first expected result set.</typeparam>
        /// <typeparam name="T2">Type of second expected result set.</typeparam>
        /// <param name="procedureName">Procedure name to call.</param>
        /// <param name="parameters">Parameters to pass to stored procedure.</param>
        /// <returns>The result of executing the stored procedure.</returns>
        public Tuple<ReadOnlyCollection<T1>, ReadOnlyCollection<T2>> ExecuteStoredProcedure<T1, T2>(string procedureName, ParameterSet parameters)
            where T1 : CommerceEntity, new()
            where T2 : CommerceEntity, new()
        {
            int returnValue = 0;

            return this.ExecuteStoredProcedure<T1, T2>(procedureName, parameters, null, out returnValue);
        }

        /// <summary>
        /// Executes the specified stored procedure.
        /// </summary>
        /// <typeparam name="T1">Type of first expected result set.</typeparam>
        /// <typeparam name="T2">Type of second expected result set.</typeparam>
        /// <param name="procedureName">Procedure name to call.</param>
        /// <param name="parameters">Parameters to pass to stored procedure.</param>
        /// <param name="outputParameters">Output parameters of the stored procedure.</param>
        /// <param name="returnValue">The return value of the stored procedure.</param>
        /// <returns>The result of executing the stored procedure.</returns>
        public Tuple<ReadOnlyCollection<T1>, ReadOnlyCollection<T2>> ExecuteStoredProcedure<T1, T2>(string procedureName, ParameterSet parameters, ParameterSet outputParameters, out int returnValue)
            where T1 : CommerceEntity, new()
            where T2 : CommerceEntity, new()
        {
            var resultTuple = this.ExecuteStoredProcedure<T1, T2, T2, T2, T2, T2, T2, T2>(
                    procedureName, parameters, outputParameters, out returnValue);

            return new Tuple<ReadOnlyCollection<T1>, ReadOnlyCollection<T2>>(resultTuple.Item1, resultTuple.Item2);
        }

        /// <summary>
        /// Executes the specified stored procedure.
        /// </summary>
        /// <typeparam name="T1">Type of first expected result set.</typeparam>
        /// <typeparam name="T2">Type of second expected result set.</typeparam>
        /// <typeparam name="T3">Type of third expected result set.</typeparam>
        /// <typeparam name="T4">Type of fourth expected result set.</typeparam>
        /// <typeparam name="T5">Type of fifth expected result set.</typeparam>
        /// <typeparam name="T6">Type of sixth expected result set.</typeparam>
        /// <typeparam name="T7">Type of seventh expected result set.</typeparam>
        /// <typeparam name="T8">Type of eighth expected result set.</typeparam>
        /// <param name="procedureName">Procedure name to call.</param>
        /// <param name="parameters">Parameters to pass to stored procedure.</param>
        /// <param name="outputParameters">Output parameters of the stored procedure.</param>
        /// <param name="returnValue">The return value of the stored procedure.</param>
        /// <returns>The result of executing the stored procedure.</returns>
        public Tuple<ReadOnlyCollection<T1>, ReadOnlyCollection<T2>, ReadOnlyCollection<T3>, ReadOnlyCollection<T4>, ReadOnlyCollection<T5>, ReadOnlyCollection<T6>, ReadOnlyCollection<T7>, Tuple<ReadOnlyCollection<T8>>>
            ExecuteStoredProcedure<T1, T2, T3, T4, T5, T6, T7, T8>(string procedureName, ParameterSet parameters, ParameterSet outputParameters, out int returnValue)
            where T1 : CommerceEntity, new()
            where T2 : CommerceEntity, new()
            where T3 : CommerceEntity, new()
            where T4 : CommerceEntity, new()
            where T5 : CommerceEntity, new()
            where T6 : CommerceEntity, new()
            where T7 : CommerceEntity, new()
            where T8 : CommerceEntity, new()
        {
            ReadOnlyCollection<T1> resultThe1st = null;
            ReadOnlyCollection<T2> resultThe2nd = null;
            ReadOnlyCollection<T3> resultThe3rd = null;
            ReadOnlyCollection<T4> resultThe4th = null;
            ReadOnlyCollection<T5> resultThe5th = null;
            ReadOnlyCollection<T6> resultThe6th = null;
            ReadOnlyCollection<T7> resultThe7th = null;
            ReadOnlyCollection<T8> resultThe8th = null;

            Action<IDatabaseResult> populateEntitiesCallback = (databaseResult) =>
            {
                resultThe1st = this.CreateEntityListFromResult<T1>(databaseResult).AsReadOnly();

                if (databaseResult.NextResult())
                {
                    resultThe2nd = this.CreateEntityListFromResult<T2>(databaseResult).AsReadOnly();
                }

                if (databaseResult.NextResult())
                {
                    resultThe3rd = this.CreateEntityListFromResult<T3>(databaseResult).AsReadOnly();
                }

                if (databaseResult.NextResult())
                {
                    resultThe4th = this.CreateEntityListFromResult<T4>(databaseResult).AsReadOnly();
                }

                if (databaseResult.NextResult())
                {
                    resultThe5th = this.CreateEntityListFromResult<T5>(databaseResult).AsReadOnly();
                }

                if (databaseResult.NextResult())
                {
                    resultThe6th = this.CreateEntityListFromResult<T6>(databaseResult).AsReadOnly();
                }

                if (databaseResult.NextResult())
                {
                    resultThe7th = this.CreateEntityListFromResult<T7>(databaseResult).AsReadOnly();
                }

                if (databaseResult.NextResult())
                {
                    resultThe8th = this.CreateEntityListFromResult<T8>(databaseResult).AsReadOnly();
                }
            };

            this.ExecuteStoredProcedure(
                procedureName,
                parameters,
                outputParameters,
                populateEntitiesCallback,
                out returnValue);

            var rest = Tuple.Create(resultThe8th.AsReadOnly());
            var resultDataSets = new Tuple<ReadOnlyCollection<T1>, ReadOnlyCollection<T2>, ReadOnlyCollection<T3>, ReadOnlyCollection<T4>, ReadOnlyCollection<T5>, ReadOnlyCollection<T6>, ReadOnlyCollection<T7>, Tuple<ReadOnlyCollection<T8>>>(
                resultThe1st.AsReadOnly(),
                resultThe2nd.AsReadOnly(),
                resultThe3rd.AsReadOnly(),
                resultThe4th.AsReadOnly(),
                resultThe5th.AsReadOnly(),
                resultThe6th.AsReadOnly(),
                resultThe7th.AsReadOnly(),
                rest);

            return resultDataSets;
        }

        /// <summary>
        /// Executes a stored procedure.
        /// </summary>
        /// <param name="procedureName">The procedure name.</param>
        /// <param name="parameters">The set of parameters.</param>
        /// <param name="outputParameters">The set of output parameters.</param>
        /// <param name="resultCallback">The result callback.</param>
        /// <param name="returnValue">The procedure result.</param>
        public void ExecuteStoredProcedure(string procedureName, ParameterSet parameters, ParameterSet outputParameters, Action<IDatabaseResult> resultCallback, out int returnValue)
        {
            ThrowIf.NullOrWhiteSpace(procedureName, "procedureName");
            ThrowIf.Null(parameters, "parameters");

            int? procedureReturnValue;

            try
            {
                this.DatabaseProvider.ExecuteStoredProcedure(
                    this.ConnectionManager.Connection,
                    procedureName,
                    parameters,
                    outputParameters,
                    resultCallback,
                    out procedureReturnValue);
            }
            catch (DatabaseException ex)
            {
                throw new StorageException(
                StorageErrors.CriticalError,
                (int)ex.ErrorCode,
                "Failed to read from the database. See inner exception for details",
                ex);
            }

            returnValue = procedureReturnValue.GetValueOrDefault(0);
        }

        /// <summary>
        /// Executes the specified stored procedure with the specified parameters.
        /// </summary>
        /// <param name="procedureName">The procedure name.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>The error code returned from the stored procedure.</returns>
        public int ExecuteStoredProcedureNonQuery(string procedureName, ParameterSet parameters)
        {
            return this.ExecuteStoredProcedureScalar(procedureName, parameters, null);
        }

        /// <summary>
        /// Executes the stored procedure using the specified parameters and returns the output
        /// of the stored procedure as a dataset.
        /// </summary>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The set of parameters.</param>
        /// <param name="settings">The query result settings.</param>
        /// <returns>A dataset.</returns>
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "There is currently no good reason to call Dispose on a DataSet.")]
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "False positive: This is a parameterized stored proc call.")]
        public DataSet ExecuteStoredProcedureDataSet(string procedureName, ParameterSet parameters, QueryResultSettings settings)
        {
            int resultValue;
            DataSet ds = null;

            Action<IDatabaseResult> callback = result =>
            {
                ds =
                    this.CreateDatasetFromResult(
                        result);
            };

            this.ExecuteStoredProcedure(procedureName, parameters, null, callback, out resultValue);

            return ds;
        }

        /// <summary>
        /// Executes the query with specified parameters.
        /// </summary>
        /// <param name="query">Query to be executed.</param>
        /// <param name="parameters">The set of parameters.</param>
        /// <returns>A dataset.</returns>
        public DataSet ExecuteQueryDataSet(string query, ParameterSet parameters)
        {
            ThrowIf.NullOrWhiteSpace(query, "query");
            ThrowIf.Null(parameters, "parameters");

            DataSet ds;

            using (IDatabaseResult result = ExecuteQuery(new SqlQuery(query, parameters)))
            {
                ds = this.CreateDatasetFromResult(result);
            }

            return ds;
        }

        /// <summary>
        /// Executes the stored procedure using the specified parameters and returns the return value
        /// of the stored procedure as an integer.
        /// </summary>
        /// <param name="procedureName">The name of the stored procedure.</param>
        /// <param name="parameters">The set of parameters.</param>
        /// <param name="outputParameters">The set of output parameters.</param>
        /// <returns>A collection of <see cref="CommerceEntity"/> objects.</returns>
        public int ExecuteStoredProcedureScalar(string procedureName, ParameterSet parameters, ParameterSet outputParameters)
        {
            int returnValue;

            this.ExecuteStoredProcedure(procedureName, parameters, outputParameters, ConsumeNonQueryResult, out returnValue);

            return returnValue;
        }

        /// <summary>
        /// Consume a <see cref="DatabaseResult"/> for non-query operations.
        /// </summary>
        /// <param name="result">The database result.</param>
        private static void ConsumeNonQueryResult(IDatabaseResult result)
        {
            try
            {
                // exauts the results to avoid unexecuted statements
                while (result.Read())
                {
                }
            }
            catch (DatabaseException exception)
            {
                throw new StorageException(
                    StorageErrors.CriticalError,
                    (int)exception.ErrorCode,
                    "Failed to read from the database. See inner exception for details",
                    exception);
            }
        }

        private DataSet CreateDatasetFromResult(IDatabaseResult databaseResult)
        {
            DataSet dataSet = new DataSet();

            try
            {
                do
                {
                    DataTable table = new DataTable();

                    if (databaseResult.Read())
                    {
                        for (int i = 0; i < databaseResult.FieldCount; i++)
                        {
                            object value = databaseResult.GetValue<object>(i);
                            Type type = value == null ? typeof(object) : value.GetType();
                            table.Columns.Add(databaseResult.GetName(i), type);
                        }

                        do
                        {
                            DataRow row = table.NewRow();

                            for (int i = 0; i < databaseResult.FieldCount; i++)
                            {
                                object value = databaseResult.GetValue<object>(i);
                                row[i] = value;
                            }

                            table.Rows.Add(row);
                        }
                        while (databaseResult.Read());
                    }

                    dataSet.Tables.Add(table);
                }
                while (databaseResult.NextResult());
            }
            catch (DatabaseException exception)
            {
                throw new StorageException(
                    StorageErrors.CriticalError,
                    (int)exception.ErrorCode,
                    "Failed to read from the database. See inner exception for details",
                    exception);
            }

            return dataSet;
        }

        private ReadOnlyCollection<T> ExecuteStoredProcedure<T>(string procedureName, ParameterSet parameters, ParameterSet outputParameters)
    where T : CommerceEntity, new()
        {
            int returnValue;
            return this.ExecuteStoredProcedure<T>(procedureName, parameters, outputParameters, out returnValue);
        }

        private ReadOnlyCollection<T> ExecuteStoredProcedure<T>(string procedureName, ParameterSet parameters, ParameterSet outputParameters, out int returnValue)
            where T : CommerceEntity, new()
        {
            return this.ExecuteStoredProcedure<T, T>(procedureName, parameters, outputParameters, out returnValue).Item1;
        }

        private List<T> CreateEntityListFromResult<T>(IDatabaseResult databaseResult) where T : CommerceEntity, new()
        {
            List<T> result = new List<T>();

            try
            {
                while (databaseResult.Read())
                {
                    result.Add(this.CreateEntityFromResult<T>(databaseResult));
                }
            }
            catch (DatabaseException exception)
            {
                throw new StorageException(
                    StorageErrors.CriticalError,
                    (int)exception.ErrorCode,
                    "Failed to read from the database. See inner exception for details",
                    exception);
            }

            return result;
        }

        private T CreateEntityFromResult<T>(IDatabaseResult databaseResult) where T : CommerceEntity, new()
        {
            T entity = new T();
            this.PopulateFromResult(entity, databaseResult);

            return entity;
        }

        private void PopulateFromResult(CommerceEntity entity, IDatabaseResult databaseResult)
        {
            entity.Populate(databaseResult, this.RequestContext.GetChannelConfiguration() == null ? null : this.RequestContext.GetChannelConfiguration().TimeZoneRecords);
        }
    }
}